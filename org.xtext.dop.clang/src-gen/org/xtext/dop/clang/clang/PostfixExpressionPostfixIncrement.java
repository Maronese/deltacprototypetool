/**
 */
package org.xtext.dop.clang.clang;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Postfix Expression Postfix Increment</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getPostfixExpressionPostfixIncrement()
 * @model
 * @generated
 */
public interface PostfixExpressionPostfixIncrement extends PostfixExpressionPostfix
{
} // PostfixExpressionPostfixIncrement
