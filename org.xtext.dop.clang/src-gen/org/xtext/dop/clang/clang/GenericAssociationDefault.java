/**
 */
package org.xtext.dop.clang.clang;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Generic Association Default</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getGenericAssociationDefault()
 * @model
 * @generated
 */
public interface GenericAssociationDefault extends GenericAssociation
{
} // GenericAssociationDefault
