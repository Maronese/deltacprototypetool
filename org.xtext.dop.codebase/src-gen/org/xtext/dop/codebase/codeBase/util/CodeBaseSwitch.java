/**
 */
package org.xtext.dop.codebase.codeBase.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.xtext.dop.codebase.codeBase.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.xtext.dop.codebase.codeBase.CodeBasePackage
 * @generated
 */
public class CodeBaseSwitch<T> extends Switch<T>
{
  /**
   * The cached model package
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static CodeBasePackage modelPackage;

  /**
   * Creates an instance of the switch.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CodeBaseSwitch()
  {
    if (modelPackage == null)
    {
      modelPackage = CodeBasePackage.eINSTANCE;
    }
  }

  /**
   * Checks whether this is a switch for the given package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param ePackage the package in question.
   * @return whether this is a switch for the given package.
   * @generated
   */
  @Override
  protected boolean isSwitchFor(EPackage ePackage)
  {
    return ePackage == modelPackage;
  }

  /**
   * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the first non-null result returned by a <code>caseXXX</code> call.
   * @generated
   */
  @Override
  protected T doSwitch(int classifierID, EObject theEObject)
  {
    switch (classifierID)
    {
      case CodeBasePackage.DELTA_MODULE:
      {
        DeltaModule deltaModule = (DeltaModule)theEObject;
        T result = caseDeltaModule(deltaModule);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CodeBasePackage.FILE_OPERATION:
      {
        FileOperation fileOperation = (FileOperation)theEObject;
        T result = caseFileOperation(fileOperation);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CodeBasePackage.ADD_SOURCE:
      {
        AddSource addSource = (AddSource)theEObject;
        T result = caseAddSource(addSource);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CodeBasePackage.REMOVE_SOURCE:
      {
        RemoveSource removeSource = (RemoveSource)theEObject;
        T result = caseRemoveSource(removeSource);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CodeBasePackage.SOURCE_OPERATION:
      {
        SourceOperation sourceOperation = (SourceOperation)theEObject;
        T result = caseSourceOperation(sourceOperation);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CodeBasePackage.ADDS:
      {
        Adds adds = (Adds)theEObject;
        T result = caseAdds(adds);
        if (result == null) result = caseFileOperation(adds);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CodeBasePackage.REMOVES:
      {
        Removes removes = (Removes)theEObject;
        T result = caseRemoves(removes);
        if (result == null) result = caseFileOperation(removes);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CodeBasePackage.MODIFIES:
      {
        Modifies modifies = (Modifies)theEObject;
        T result = caseModifies(modifies);
        if (result == null) result = caseFileOperation(modifies);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CodeBasePackage.ADD_DECLARATION:
      {
        AddDeclaration addDeclaration = (AddDeclaration)theEObject;
        T result = caseAddDeclaration(addDeclaration);
        if (result == null) result = caseSourceOperation(addDeclaration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CodeBasePackage.ADD_STANDARD_INCLUDE:
      {
        AddStandardInclude addStandardInclude = (AddStandardInclude)theEObject;
        T result = caseAddStandardInclude(addStandardInclude);
        if (result == null) result = caseSourceOperation(addStandardInclude);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CodeBasePackage.ADD_FILE_INCLUDE:
      {
        AddFileInclude addFileInclude = (AddFileInclude)theEObject;
        T result = caseAddFileInclude(addFileInclude);
        if (result == null) result = caseSourceOperation(addFileInclude);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CodeBasePackage.REMOVE_GLOBAL:
      {
        RemoveGlobal removeGlobal = (RemoveGlobal)theEObject;
        T result = caseRemoveGlobal(removeGlobal);
        if (result == null) result = caseSourceOperation(removeGlobal);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CodeBasePackage.REMOVE_STANDARD_INCLUDE:
      {
        RemoveStandardInclude removeStandardInclude = (RemoveStandardInclude)theEObject;
        T result = caseRemoveStandardInclude(removeStandardInclude);
        if (result == null) result = caseSourceOperation(removeStandardInclude);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CodeBasePackage.REMOVE_FILE_INCLUDE:
      {
        RemoveFileInclude removeFileInclude = (RemoveFileInclude)theEObject;
        T result = caseRemoveFileInclude(removeFileInclude);
        if (result == null) result = caseSourceOperation(removeFileInclude);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CodeBasePackage.MODIFY_FUNCTION:
      {
        ModifyFunction modifyFunction = (ModifyFunction)theEObject;
        T result = caseModifyFunction(modifyFunction);
        if (result == null) result = caseSourceOperation(modifyFunction);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      default: return defaultCase(theEObject);
    }
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Delta Module</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Delta Module</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDeltaModule(DeltaModule object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>File Operation</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>File Operation</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFileOperation(FileOperation object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Add Source</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Add Source</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAddSource(AddSource object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Remove Source</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Remove Source</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRemoveSource(RemoveSource object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Source Operation</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Source Operation</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSourceOperation(SourceOperation object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Adds</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Adds</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAdds(Adds object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Removes</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Removes</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRemoves(Removes object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Modifies</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Modifies</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseModifies(Modifies object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Add Declaration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Add Declaration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAddDeclaration(AddDeclaration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Add Standard Include</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Add Standard Include</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAddStandardInclude(AddStandardInclude object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Add File Include</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Add File Include</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAddFileInclude(AddFileInclude object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Remove Global</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Remove Global</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRemoveGlobal(RemoveGlobal object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Remove Standard Include</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Remove Standard Include</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRemoveStandardInclude(RemoveStandardInclude object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Remove File Include</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Remove File Include</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRemoveFileInclude(RemoveFileInclude object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Modify Function</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Modify Function</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseModifyFunction(ModifyFunction object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch, but this is the last case anyway.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject)
   * @generated
   */
  @Override
  public T defaultCase(EObject object)
  {
    return null;
  }

} //CodeBaseSwitch
