/**
 */
package org.xtext.dop.clang.clang;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Expression Sizeof</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.clang.clang.ExpressionSizeof#getContent <em>Content</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.ExpressionSizeof#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getExpressionSizeof()
 * @model
 * @generated
 */
public interface ExpressionSizeof extends Expression
{
  /**
   * Returns the value of the '<em><b>Content</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Content</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Content</em>' containment reference.
   * @see #setContent(Expression)
   * @see org.xtext.dop.clang.clang.ClangPackage#getExpressionSizeof_Content()
   * @model containment="true"
   * @generated
   */
  Expression getContent();

  /**
   * Sets the value of the '{@link org.xtext.dop.clang.clang.ExpressionSizeof#getContent <em>Content</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Content</em>' containment reference.
   * @see #getContent()
   * @generated
   */
  void setContent(Expression value);

  /**
   * Returns the value of the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' containment reference.
   * @see #setType(SpecifiedType)
   * @see org.xtext.dop.clang.clang.ClangPackage#getExpressionSizeof_Type()
   * @model containment="true"
   * @generated
   */
  SpecifiedType getType();

  /**
   * Sets the value of the '{@link org.xtext.dop.clang.clang.ExpressionSizeof#getType <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' containment reference.
   * @see #getType()
   * @generated
   */
  void setType(SpecifiedType value);

} // ExpressionSizeof
