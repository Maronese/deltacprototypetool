/**
 */
package org.xtext.dop.clang.clang;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Storage Class Specifier Extern</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getStorageClassSpecifierExtern()
 * @model
 * @generated
 */
public interface StorageClassSpecifierExtern extends StorageClassSpecifier
{
} // StorageClassSpecifierExtern
