/**
 */
package org.xtext.dop.codebase.codeBase.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.xtext.dop.clang.clang.Source;

import org.xtext.dop.codebase.codeBase.CodeBasePackage;
import org.xtext.dop.codebase.codeBase.Modifies;
import org.xtext.dop.codebase.codeBase.SourceOperation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Modifies</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.codebase.codeBase.impl.ModifiesImpl#getFileName <em>File Name</em>}</li>
 *   <li>{@link org.xtext.dop.codebase.codeBase.impl.ModifiesImpl#getAction <em>Action</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ModifiesImpl extends FileOperationImpl implements Modifies
{
  /**
   * The cached value of the '{@link #getFileName() <em>File Name</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFileName()
   * @generated
   * @ordered
   */
  protected Source fileName;

  /**
   * The cached value of the '{@link #getAction() <em>Action</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAction()
   * @generated
   * @ordered
   */
  protected EList<SourceOperation> action;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ModifiesImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return CodeBasePackage.Literals.MODIFIES;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Source getFileName()
  {
    return fileName;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetFileName(Source newFileName, NotificationChain msgs)
  {
    Source oldFileName = fileName;
    fileName = newFileName;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CodeBasePackage.MODIFIES__FILE_NAME, oldFileName, newFileName);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFileName(Source newFileName)
  {
    if (newFileName != fileName)
    {
      NotificationChain msgs = null;
      if (fileName != null)
        msgs = ((InternalEObject)fileName).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CodeBasePackage.MODIFIES__FILE_NAME, null, msgs);
      if (newFileName != null)
        msgs = ((InternalEObject)newFileName).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CodeBasePackage.MODIFIES__FILE_NAME, null, msgs);
      msgs = basicSetFileName(newFileName, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CodeBasePackage.MODIFIES__FILE_NAME, newFileName, newFileName));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<SourceOperation> getAction()
  {
    if (action == null)
    {
      action = new EObjectContainmentEList<SourceOperation>(SourceOperation.class, this, CodeBasePackage.MODIFIES__ACTION);
    }
    return action;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case CodeBasePackage.MODIFIES__FILE_NAME:
        return basicSetFileName(null, msgs);
      case CodeBasePackage.MODIFIES__ACTION:
        return ((InternalEList<?>)getAction()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case CodeBasePackage.MODIFIES__FILE_NAME:
        return getFileName();
      case CodeBasePackage.MODIFIES__ACTION:
        return getAction();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case CodeBasePackage.MODIFIES__FILE_NAME:
        setFileName((Source)newValue);
        return;
      case CodeBasePackage.MODIFIES__ACTION:
        getAction().clear();
        getAction().addAll((Collection<? extends SourceOperation>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case CodeBasePackage.MODIFIES__FILE_NAME:
        setFileName((Source)null);
        return;
      case CodeBasePackage.MODIFIES__ACTION:
        getAction().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case CodeBasePackage.MODIFIES__FILE_NAME:
        return fileName != null;
      case CodeBasePackage.MODIFIES__ACTION:
        return action != null && !action.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //ModifiesImpl
