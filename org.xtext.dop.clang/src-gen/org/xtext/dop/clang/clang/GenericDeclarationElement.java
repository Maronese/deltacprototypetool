/**
 */
package org.xtext.dop.clang.clang;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Generic Declaration Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.clang.clang.GenericDeclarationElement#getId <em>Id</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.GenericDeclarationElement#getInit <em>Init</em>}</li>
 * </ul>
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getGenericDeclarationElement()
 * @model
 * @generated
 */
public interface GenericDeclarationElement extends EObject
{
  /**
   * Returns the value of the '<em><b>Id</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Id</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Id</em>' containment reference.
   * @see #setId(GenericDeclarationId)
   * @see org.xtext.dop.clang.clang.ClangPackage#getGenericDeclarationElement_Id()
   * @model containment="true"
   * @generated
   */
  GenericDeclarationId getId();

  /**
   * Sets the value of the '{@link org.xtext.dop.clang.clang.GenericDeclarationElement#getId <em>Id</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Id</em>' containment reference.
   * @see #getId()
   * @generated
   */
  void setId(GenericDeclarationId value);

  /**
   * Returns the value of the '<em><b>Init</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Init</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Init</em>' containment reference.
   * @see #setInit(GenericDeclarationInitializer)
   * @see org.xtext.dop.clang.clang.ClangPackage#getGenericDeclarationElement_Init()
   * @model containment="true"
   * @generated
   */
  GenericDeclarationInitializer getInit();

  /**
   * Sets the value of the '{@link org.xtext.dop.clang.clang.GenericDeclarationElement#getInit <em>Init</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Init</em>' containment reference.
   * @see #getInit()
   * @generated
   */
  void setInit(GenericDeclarationInitializer value);

} // GenericDeclarationElement
