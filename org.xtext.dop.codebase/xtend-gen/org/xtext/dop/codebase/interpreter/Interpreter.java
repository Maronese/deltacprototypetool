package org.xtext.dop.codebase.interpreter;

import com.google.common.base.Objects;
import com.google.common.collect.Iterators;
import java.util.Iterator;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.IteratorExtensions;
import org.xtext.dop.clang.clang.CompoundStatement;
import org.xtext.dop.clang.clang.Declaration;
import org.xtext.dop.clang.clang.FileInclude;
import org.xtext.dop.clang.clang.FunctionInitializer;
import org.xtext.dop.clang.clang.GenericSpecifiedDeclaration;
import org.xtext.dop.clang.clang.GlobalName;
import org.xtext.dop.clang.clang.Model;
import org.xtext.dop.clang.clang.Source;
import org.xtext.dop.clang.clang.StandardInclude;
import org.xtext.dop.clang.inspector.Inspector;
import org.xtext.dop.codebase.codeBase.AddDeclaration;
import org.xtext.dop.codebase.codeBase.AddFileInclude;
import org.xtext.dop.codebase.codeBase.AddSource;
import org.xtext.dop.codebase.codeBase.AddStandardInclude;
import org.xtext.dop.codebase.codeBase.Adds;
import org.xtext.dop.codebase.codeBase.DeltaModule;
import org.xtext.dop.codebase.codeBase.FileOperation;
import org.xtext.dop.codebase.codeBase.Modifies;
import org.xtext.dop.codebase.codeBase.ModifyFunction;
import org.xtext.dop.codebase.codeBase.RemoveFileInclude;
import org.xtext.dop.codebase.codeBase.RemoveGlobal;
import org.xtext.dop.codebase.codeBase.RemoveSource;
import org.xtext.dop.codebase.codeBase.RemoveStandardInclude;
import org.xtext.dop.codebase.codeBase.Removes;
import org.xtext.dop.codebase.codeBase.SourceOperation;
import org.xtext.dop.rappresentation.DopResource;
import org.xtext.dop.rappresentation.Project;
import org.xtext.dop.rappresentation.Resource;

@SuppressWarnings("all")
public class Interpreter {
  private Project project;
  
  @Extension
  private Inspector inspector;
  
  public Interpreter(final Project project) {
    this.project = project;
    Inspector _inspector = new Inspector();
    this.inspector = _inspector;
  }
  
  public void interpret(final DeltaModule delta) {
    EList<FileOperation> _body = delta.getBody();
    for (final FileOperation op : _body) {
      boolean _matched = false;
      if (!_matched) {
        if (op instanceof Adds) {
          _matched=true;
          EList<AddSource> _files = ((Adds) op).getFiles();
          for (final AddSource source : _files) {
            {
              Source _fileName = source.getFileName();
              final String fileName = _fileName.getName();
              Model _code = source.getCode();
              final DopResource resource = new DopResource(fileName, _code);
              String _name = delta.getName();
              resource.setCreator(_name);
              this.project.addResource(fileName, resource);
            }
          }
        }
      }
      if (!_matched) {
        if (op instanceof Removes) {
          _matched=true;
          EList<RemoveSource> _files = ((Removes) op).getFiles();
          for (final RemoveSource source : _files) {
            {
              Source _fileName = source.getFileName();
              String _name = _fileName.getName();
              String _plus = ("remove ->" + _name);
              InputOutput.<String>println(_plus);
              Source _fileName_1 = source.getFileName();
              String _name_1 = _fileName_1.getName();
              boolean _removeResource = this.project.removeResource(_name_1);
              InputOutput.<Boolean>println(Boolean.valueOf(_removeResource));
            }
          }
        }
      }
      if (!_matched) {
        if (op instanceof Modifies) {
          _matched=true;
          Source _fileName = ((Modifies) op).getFileName();
          String _name = _fileName.getName();
          final Resource source = this.project.getResource(_name);
          EList<SourceOperation> _action = ((Modifies) op).getAction();
          for (final SourceOperation act : _action) {
            this.interpret(source, act);
          }
        }
      }
    }
  }
  
  public Object interpret(final Resource resource, final SourceOperation op) {
    Object _xblockexpression = null;
    {
      final Model model = resource.getSource();
      Object _switchResult = null;
      boolean _matched = false;
      if (!_matched) {
        if (op instanceof AddDeclaration) {
          _matched=true;
          boolean _xblockexpression_1 = false;
          {
            int i = this.inspector.indexFirstFunction(model);
            boolean _xifexpression = false;
            boolean _or = false;
            Declaration _decl = ((AddDeclaration) op).getDecl();
            TreeIterator<EObject> _eAllContents = _decl.eAllContents();
            Iterator<FunctionInitializer> _filter = Iterators.<FunctionInitializer>filter(_eAllContents, FunctionInitializer.class);
            boolean _isEmpty = IteratorExtensions.isEmpty(_filter);
            boolean _not = (!_isEmpty);
            if (_not) {
              _or = true;
            } else {
              _or = (i == (-1));
            }
            if (_or) {
              EList<Declaration> _declarations = model.getDeclarations();
              Declaration _decl_1 = ((AddDeclaration) op).getDecl();
              _xifexpression = _declarations.add(_decl_1);
            } else {
              EList<Declaration> _declarations_1 = model.getDeclarations();
              Declaration _decl_2 = ((AddDeclaration) op).getDecl();
              _declarations_1.add(i, _decl_2);
            }
            _xblockexpression_1 = _xifexpression;
          }
          _switchResult = Boolean.valueOf(_xblockexpression_1);
        }
      }
      if (!_matched) {
        if (op instanceof AddStandardInclude) {
          _matched=true;
          EList<StandardInclude> _includesStd = model.getIncludesStd();
          StandardInclude _incl = ((AddStandardInclude) op).getIncl();
          _switchResult = Boolean.valueOf(_includesStd.add(_incl));
        }
      }
      if (!_matched) {
        if (op instanceof AddFileInclude) {
          _matched=true;
          EList<FileInclude> _includesFile = model.getIncludesFile();
          FileInclude _incl = ((AddFileInclude) op).getIncl();
          _switchResult = Boolean.valueOf(_includesFile.add(_incl));
        }
      }
      if (!_matched) {
        if (op instanceof RemoveGlobal) {
          _matched=true;
          Declaration target = null;
          EList<Declaration> _declarations = model.getDeclarations();
          for (final Declaration decl : _declarations) {
            {
              TreeIterator<EObject> _eAllContents = decl.eAllContents();
              Iterator<GlobalName> _filter = Iterators.<GlobalName>filter(_eAllContents, GlobalName.class);
              final Function1<GlobalName, Boolean> _function = (GlobalName it) -> {
                String _name = it.getName();
                String _global = ((RemoveGlobal) op).getGlobal();
                return Boolean.valueOf(Objects.equal(_name, _global));
              };
              final GlobalName global = IteratorExtensions.<GlobalName>findFirst(_filter, _function);
              boolean _notEquals = (!Objects.equal(global, null));
              if (_notEquals) {
                target = decl;
                EList<Declaration> _declarations_1 = model.getDeclarations();
                return Boolean.valueOf(_declarations_1.remove(target));
              }
            }
          }
        }
      }
      if (!_matched) {
        if (op instanceof RemoveStandardInclude) {
          _matched=true;
          Integer _xblockexpression_1 = null;
          {
            final StandardInclude include = ((RemoveStandardInclude) op).getIncl();
            Integer _xifexpression = null;
            boolean _isWild = include.isWild();
            if (_isWild) {
              EList<StandardInclude> _includesStd = model.getIncludesStd();
              _includesStd.clear();
            } else {
              Integer _xblockexpression_2 = null;
              {
                EList<StandardInclude> _includesStd_1 = model.getIncludesStd();
                final Function1<StandardInclude, Boolean> _function = (StandardInclude it) -> {
                  Source _std = it.getStd();
                  String _name = _std.getName();
                  Source _std_1 = include.getStd();
                  String _name_1 = _std_1.getName();
                  return Boolean.valueOf(Objects.equal(_name, _name_1));
                };
                final StandardInclude target = IterableExtensions.<StandardInclude>findFirst(_includesStd_1, _function);
                Source _std = include.getStd();
                InputOutput.<Source>println(_std);
                EList<StandardInclude> _includesStd_2 = model.getIncludesStd();
                int _length = ((Object[])Conversions.unwrapArray(_includesStd_2, Object.class)).length;
                InputOutput.<Integer>println(Integer.valueOf(_length));
                InputOutput.<StandardInclude>println(target);
                EList<StandardInclude> _includesStd_3 = model.getIncludesStd();
                final int index = _includesStd_3.indexOf(target);
                InputOutput.<Integer>println(Integer.valueOf(index));
                EList<StandardInclude> _includesStd_4 = model.getIncludesStd();
                _includesStd_4.remove(index);
                EList<StandardInclude> _includesStd_5 = model.getIncludesStd();
                int _length_1 = ((Object[])Conversions.unwrapArray(_includesStd_5, Object.class)).length;
                _xblockexpression_2 = InputOutput.<Integer>println(Integer.valueOf(_length_1));
              }
              _xifexpression = _xblockexpression_2;
            }
            _xblockexpression_1 = _xifexpression;
          }
          _switchResult = _xblockexpression_1;
        }
      }
      if (!_matched) {
        if (op instanceof RemoveFileInclude) {
          _matched=true;
          FileInclude _xblockexpression_1 = null;
          {
            final FileInclude include = ((RemoveFileInclude) op).getIncl();
            FileInclude _xifexpression = null;
            String _file = include.getFile();
            boolean _equals = Objects.equal(_file, "\"*\"");
            if (_equals) {
              EList<FileInclude> _includesFile = model.getIncludesFile();
              _includesFile.clear();
            } else {
              FileInclude _xblockexpression_2 = null;
              {
                EList<FileInclude> _includesFile_1 = model.getIncludesFile();
                final Function1<FileInclude, Boolean> _function = (FileInclude it) -> {
                  Boolean _xblockexpression_3 = null;
                  {
                    String _file_1 = include.getFile();
                    String _plus = (_file_1 + "\n");
                    String _file_2 = it.getFile();
                    String _plus_1 = (_plus + _file_2);
                    InputOutput.<String>println(_plus_1);
                    String _file_3 = it.getFile();
                    String _file_4 = include.getFile();
                    boolean _equals_1 = Objects.equal(_file_3, _file_4);
                    _xblockexpression_3 = InputOutput.<Boolean>println(Boolean.valueOf(_equals_1));
                  }
                  return _xblockexpression_3;
                };
                final FileInclude target = IterableExtensions.<FileInclude>findFirst(_includesFile_1, _function);
                EList<FileInclude> _includesFile_2 = model.getIncludesFile();
                final int index = _includesFile_2.indexOf(target);
                EList<FileInclude> _includesFile_3 = model.getIncludesFile();
                _xblockexpression_2 = _includesFile_3.remove(index);
              }
              _xifexpression = _xblockexpression_2;
            }
            _xblockexpression_1 = _xifexpression;
          }
          _switchResult = _xblockexpression_1;
        }
      }
      if (!_matched) {
        if (op instanceof ModifyFunction) {
          _matched=true;
          Declaration _decl = ((ModifyFunction) op).getDecl();
          TreeIterator<EObject> _eAllContents = _decl.eAllContents();
          Iterator<GlobalName> _filter = Iterators.<GlobalName>filter(_eAllContents, GlobalName.class);
          GlobalName _next = _filter.next();
          final String name = _next.getName();
          InputOutput.<String>println(("name = " + name));
          Declaration _decl_1 = ((ModifyFunction) op).getDecl();
          TreeIterator<EObject> _eAllContents_1 = _decl_1.eAllContents();
          Iterator<FunctionInitializer> _filter_1 = Iterators.<FunctionInitializer>filter(_eAllContents_1, FunctionInitializer.class);
          FunctionInitializer _next_1 = _filter_1.next();
          final CompoundStatement block = _next_1.getBlock();
          InputOutput.<String>println(("block = " + block));
          EList<Declaration> _declarations = model.getDeclarations();
          final Function1<Declaration, Boolean> _function = (Declaration it) -> {
            boolean _and = false;
            boolean _and_1 = false;
            if (!(it instanceof GenericSpecifiedDeclaration)) {
              _and_1 = false;
            } else {
              TreeIterator<EObject> _eAllContents_2 = it.eAllContents();
              Iterator<FunctionInitializer> _filter_2 = Iterators.<FunctionInitializer>filter(_eAllContents_2, FunctionInitializer.class);
              boolean _isEmpty = IteratorExtensions.isEmpty(_filter_2);
              boolean _not = (!_isEmpty);
              _and_1 = _not;
            }
            if (!_and_1) {
              _and = false;
            } else {
              TreeIterator<EObject> _eAllContents_3 = it.eAllContents();
              Iterator<GlobalName> _filter_3 = Iterators.<GlobalName>filter(_eAllContents_3, GlobalName.class);
              GlobalName _next_2 = _filter_3.next();
              String _name = _next_2.getName();
              boolean _equals = Objects.equal(_name, name);
              _and = _equals;
            }
            return Boolean.valueOf(_and);
          };
          final Declaration global = IterableExtensions.<Declaration>findFirst(_declarations, _function);
          TreeIterator<EObject> _eAllContents_2 = global.eAllContents();
          Iterator<FunctionInitializer> _filter_2 = Iterators.<FunctionInitializer>filter(_eAllContents_2, FunctionInitializer.class);
          FunctionInitializer _next_2 = _filter_2.next();
          _next_2.setBlock(block);
        }
      }
      _xblockexpression = _switchResult;
    }
    return _xblockexpression;
  }
}
