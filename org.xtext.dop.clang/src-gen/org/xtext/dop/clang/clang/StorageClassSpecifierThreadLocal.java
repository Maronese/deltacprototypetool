/**
 */
package org.xtext.dop.clang.clang;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Storage Class Specifier Thread Local</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getStorageClassSpecifierThreadLocal()
 * @model
 * @generated
 */
public interface StorageClassSpecifierThreadLocal extends StorageClassSpecifier
{
} // StorageClassSpecifierThreadLocal
