/**
 */
package org.xtext.dop.clang.clang.impl;

import org.eclipse.emf.ecore.EClass;

import org.xtext.dop.clang.clang.ClangPackage;
import org.xtext.dop.clang.clang.JumpStatementBreak;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Jump Statement Break</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class JumpStatementBreakImpl extends JumpStatementImpl implements JumpStatementBreak
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected JumpStatementBreakImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ClangPackage.Literals.JUMP_STATEMENT_BREAK;
  }

} //JumpStatementBreakImpl
