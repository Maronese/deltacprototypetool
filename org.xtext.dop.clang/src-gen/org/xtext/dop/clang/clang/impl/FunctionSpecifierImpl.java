/**
 */
package org.xtext.dop.clang.clang.impl;

import org.eclipse.emf.ecore.EClass;

import org.xtext.dop.clang.clang.ClangPackage;
import org.xtext.dop.clang.clang.FunctionSpecifier;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Function Specifier</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class FunctionSpecifierImpl extends DeclarationSpecifierImpl implements FunctionSpecifier
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected FunctionSpecifierImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ClangPackage.Literals.FUNCTION_SPECIFIER;
  }

} //FunctionSpecifierImpl
