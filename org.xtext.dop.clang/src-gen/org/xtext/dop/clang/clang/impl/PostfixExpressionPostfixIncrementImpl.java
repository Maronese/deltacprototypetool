/**
 */
package org.xtext.dop.clang.clang.impl;

import org.eclipse.emf.ecore.EClass;

import org.xtext.dop.clang.clang.ClangPackage;
import org.xtext.dop.clang.clang.PostfixExpressionPostfixIncrement;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Postfix Expression Postfix Increment</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class PostfixExpressionPostfixIncrementImpl extends PostfixExpressionPostfixImpl implements PostfixExpressionPostfixIncrement
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected PostfixExpressionPostfixIncrementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ClangPackage.Literals.POSTFIX_EXPRESSION_POSTFIX_INCREMENT;
  }

} //PostfixExpressionPostfixIncrementImpl
