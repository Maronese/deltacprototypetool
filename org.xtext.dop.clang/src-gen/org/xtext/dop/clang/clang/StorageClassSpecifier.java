/**
 */
package org.xtext.dop.clang.clang;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Storage Class Specifier</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getStorageClassSpecifier()
 * @model
 * @generated
 */
public interface StorageClassSpecifier extends DeclarationSpecifier
{
} // StorageClassSpecifier
