/*
 * generated by Xtext
 */
package org.xtext.dop.codebase;

import org.eclipse.xtext.junit4.IInjectorProvider;

import com.google.inject.Injector;

public class CodeBaseUiInjectorProvider implements IInjectorProvider {
	
	@Override
	public Injector getInjector() {
		return org.xtext.dop.codebase.ui.internal.CodeBaseActivator.getInstance().getInjector("org.xtext.dop.codebase.CodeBase");
	}
	
}
