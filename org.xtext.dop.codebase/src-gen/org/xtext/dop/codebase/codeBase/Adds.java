/**
 */
package org.xtext.dop.codebase.codeBase;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Adds</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.codebase.codeBase.Adds#getFiles <em>Files</em>}</li>
 * </ul>
 *
 * @see org.xtext.dop.codebase.codeBase.CodeBasePackage#getAdds()
 * @model
 * @generated
 */
public interface Adds extends FileOperation
{
  /**
   * Returns the value of the '<em><b>Files</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.dop.codebase.codeBase.AddSource}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Files</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Files</em>' containment reference list.
   * @see org.xtext.dop.codebase.codeBase.CodeBasePackage#getAdds_Files()
   * @model containment="true"
   * @generated
   */
  EList<AddSource> getFiles();

} // Adds
