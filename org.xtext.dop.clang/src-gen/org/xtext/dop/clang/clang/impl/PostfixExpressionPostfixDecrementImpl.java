/**
 */
package org.xtext.dop.clang.clang.impl;

import org.eclipse.emf.ecore.EClass;

import org.xtext.dop.clang.clang.ClangPackage;
import org.xtext.dop.clang.clang.PostfixExpressionPostfixDecrement;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Postfix Expression Postfix Decrement</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class PostfixExpressionPostfixDecrementImpl extends PostfixExpressionPostfixImpl implements PostfixExpressionPostfixDecrement
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected PostfixExpressionPostfixDecrementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ClangPackage.Literals.POSTFIX_EXPRESSION_POSTFIX_DECREMENT;
  }

} //PostfixExpressionPostfixDecrementImpl
