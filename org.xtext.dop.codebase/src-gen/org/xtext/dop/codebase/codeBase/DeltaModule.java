/**
 */
package org.xtext.dop.codebase.codeBase;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Delta Module</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.codebase.codeBase.DeltaModule#getName <em>Name</em>}</li>
 *   <li>{@link org.xtext.dop.codebase.codeBase.DeltaModule#getBody <em>Body</em>}</li>
 * </ul>
 *
 * @see org.xtext.dop.codebase.codeBase.CodeBasePackage#getDeltaModule()
 * @model
 * @generated
 */
public interface DeltaModule extends EObject
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see org.xtext.dop.codebase.codeBase.CodeBasePackage#getDeltaModule_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link org.xtext.dop.codebase.codeBase.DeltaModule#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Body</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.dop.codebase.codeBase.FileOperation}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Body</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Body</em>' containment reference list.
   * @see org.xtext.dop.codebase.codeBase.CodeBasePackage#getDeltaModule_Body()
   * @model containment="true"
   * @generated
   */
  EList<FileOperation> getBody();

} // DeltaModule
