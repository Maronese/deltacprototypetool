/**
 */
package org.xtext.dop.clang.clang.impl;

import org.eclipse.emf.ecore.EClass;

import org.xtext.dop.clang.clang.ClangPackage;
import org.xtext.dop.clang.clang.StorageClassSpecifier;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Storage Class Specifier</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class StorageClassSpecifierImpl extends DeclarationSpecifierImpl implements StorageClassSpecifier
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected StorageClassSpecifierImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ClangPackage.Literals.STORAGE_CLASS_SPECIFIER;
  }

} //StorageClassSpecifierImpl
