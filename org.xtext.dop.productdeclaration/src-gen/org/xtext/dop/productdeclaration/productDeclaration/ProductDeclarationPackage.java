/**
 */
package org.xtext.dop.productdeclaration.productDeclaration;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.xtext.dop.productdeclaration.productDeclaration.ProductDeclarationFactory
 * @model kind="package"
 * @generated
 */
public interface ProductDeclarationPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "productDeclaration";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.xtext.org/dop/productdeclaration/ProductDeclaration";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "productDeclaration";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  ProductDeclarationPackage eINSTANCE = org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationPackageImpl.init();

  /**
   * The meta object id for the '{@link org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationImpl <em>Product Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationImpl
   * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationPackageImpl#getProductDeclaration()
   * @generated
   */
  int PRODUCT_DECLARATION = 0;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRODUCT_DECLARATION__NAME = 0;

  /**
   * The feature id for the '<em><b>Features</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRODUCT_DECLARATION__FEATURES = 1;

  /**
   * The feature id for the '<em><b>Deltas</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRODUCT_DECLARATION__DELTAS = 2;

  /**
   * The feature id for the '<em><b>Constraints</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRODUCT_DECLARATION__CONSTRAINTS = 3;

  /**
   * The feature id for the '<em><b>Partitions</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRODUCT_DECLARATION__PARTITIONS = 4;

  /**
   * The feature id for the '<em><b>Products</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRODUCT_DECLARATION__PRODUCTS = 5;

  /**
   * The number of structural features of the '<em>Product Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRODUCT_DECLARATION_FEATURE_COUNT = 6;

  /**
   * The meta object id for the '{@link org.xtext.dop.productdeclaration.productDeclaration.impl.FeaturesImpl <em>Features</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.productdeclaration.productDeclaration.impl.FeaturesImpl
   * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationPackageImpl#getFeatures()
   * @generated
   */
  int FEATURES = 1;

  /**
   * The feature id for the '<em><b>List</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FEATURES__LIST = 0;

  /**
   * The number of structural features of the '<em>Features</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FEATURES_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.productdeclaration.productDeclaration.impl.DeltasImpl <em>Deltas</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.productdeclaration.productDeclaration.impl.DeltasImpl
   * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationPackageImpl#getDeltas()
   * @generated
   */
  int DELTAS = 2;

  /**
   * The feature id for the '<em><b>List</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DELTAS__LIST = 0;

  /**
   * The number of structural features of the '<em>Deltas</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DELTAS_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.productdeclaration.productDeclaration.impl.ProductsImpl <em>Products</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ProductsImpl
   * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationPackageImpl#getProducts()
   * @generated
   */
  int PRODUCTS = 3;

  /**
   * The feature id for the '<em><b>List</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRODUCTS__LIST = 0;

  /**
   * The number of structural features of the '<em>Products</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRODUCTS_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.productdeclaration.productDeclaration.impl.ProductImpl <em>Product</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ProductImpl
   * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationPackageImpl#getProduct()
   * @generated
   */
  int PRODUCT = 4;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRODUCT__NAME = 0;

  /**
   * The feature id for the '<em><b>List</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRODUCT__LIST = 1;

  /**
   * The number of structural features of the '<em>Product</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRODUCT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.xtext.dop.productdeclaration.productDeclaration.impl.PartitionsImpl <em>Partitions</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.productdeclaration.productDeclaration.impl.PartitionsImpl
   * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationPackageImpl#getPartitions()
   * @generated
   */
  int PARTITIONS = 5;

  /**
   * The feature id for the '<em><b>List</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARTITIONS__LIST = 0;

  /**
   * The number of structural features of the '<em>Partitions</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARTITIONS_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.productdeclaration.productDeclaration.impl.PartitionImpl <em>Partition</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.productdeclaration.productDeclaration.impl.PartitionImpl
   * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationPackageImpl#getPartition()
   * @generated
   */
  int PARTITION = 6;

  /**
   * The feature id for the '<em><b>Constr</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARTITION__CONSTR = 0;

  /**
   * The number of structural features of the '<em>Partition</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARTITION_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.productdeclaration.productDeclaration.impl.ConstraintImpl <em>Constraint</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ConstraintImpl
   * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationPackageImpl#getConstraint()
   * @generated
   */
  int CONSTRAINT = 7;

  /**
   * The feature id for the '<em><b>Delta</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTRAINT__DELTA = 0;

  /**
   * The feature id for the '<em><b>Expr</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTRAINT__EXPR = 1;

  /**
   * The feature id for the '<em><b>Last</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTRAINT__LAST = 2;

  /**
   * The number of structural features of the '<em>Constraint</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTRAINT_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.xtext.dop.productdeclaration.productDeclaration.impl.ListElemImpl <em>List Elem</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ListElemImpl
   * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationPackageImpl#getListElem()
   * @generated
   */
  int LIST_ELEM = 8;

  /**
   * The feature id for the '<em><b>Elem</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LIST_ELEM__ELEM = 0;

  /**
   * The feature id for the '<em><b>Last</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LIST_ELEM__LAST = 1;

  /**
   * The number of structural features of the '<em>List Elem</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LIST_ELEM_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.xtext.dop.productdeclaration.productDeclaration.impl.ExpressionImpl <em>Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ExpressionImpl
   * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationPackageImpl#getExpression()
   * @generated
   */
  int EXPRESSION = 11;

  /**
   * The number of structural features of the '<em>Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.productdeclaration.productDeclaration.impl.EspressionImpl <em>Espression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.productdeclaration.productDeclaration.impl.EspressionImpl
   * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationPackageImpl#getEspression()
   * @generated
   */
  int ESPRESSION = 12;

  /**
   * The number of structural features of the '<em>Espression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ESPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.productdeclaration.productDeclaration.impl.ElementImpl <em>Element</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ElementImpl
   * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationPackageImpl#getElement()
   * @generated
   */
  int ELEMENT = 9;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ELEMENT__NAME = ESPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Element</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ELEMENT_FEATURE_COUNT = ESPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.productdeclaration.productDeclaration.impl.CoreImpl <em>Core</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.productdeclaration.productDeclaration.impl.CoreImpl
   * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationPackageImpl#getCore()
   * @generated
   */
  int CORE = 10;

  /**
   * The feature id for the '<em><b>Expr</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CORE__EXPR = 0;

  /**
   * The number of structural features of the '<em>Core</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CORE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.productdeclaration.productDeclaration.impl.OrImpl <em>Or</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.productdeclaration.productDeclaration.impl.OrImpl
   * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationPackageImpl#getOr()
   * @generated
   */
  int OR = 13;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OR__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OR__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Or</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OR_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.xtext.dop.productdeclaration.productDeclaration.impl.AndImpl <em>And</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.productdeclaration.productDeclaration.impl.AndImpl
   * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationPackageImpl#getAnd()
   * @generated
   */
  int AND = 14;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AND__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AND__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>And</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AND_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.xtext.dop.productdeclaration.productDeclaration.impl.SubImpl <em>Sub</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.productdeclaration.productDeclaration.impl.SubImpl
   * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationPackageImpl#getSub()
   * @generated
   */
  int SUB = 15;

  /**
   * The feature id for the '<em><b>Expr</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUB__EXPR = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Sub</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUB_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.productdeclaration.productDeclaration.impl.NotImpl <em>Not</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.productdeclaration.productDeclaration.impl.NotImpl
   * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationPackageImpl#getNot()
   * @generated
   */
  int NOT = 16;

  /**
   * The feature id for the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NOT__VALUE = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Not</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NOT_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;


  /**
   * Returns the meta object for class '{@link org.xtext.dop.productdeclaration.productDeclaration.ProductDeclaration <em>Product Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Product Declaration</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.ProductDeclaration
   * @generated
   */
  EClass getProductDeclaration();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.dop.productdeclaration.productDeclaration.ProductDeclaration#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.ProductDeclaration#getName()
   * @see #getProductDeclaration()
   * @generated
   */
  EAttribute getProductDeclaration_Name();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.productdeclaration.productDeclaration.ProductDeclaration#getFeatures <em>Features</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Features</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.ProductDeclaration#getFeatures()
   * @see #getProductDeclaration()
   * @generated
   */
  EReference getProductDeclaration_Features();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.productdeclaration.productDeclaration.ProductDeclaration#getDeltas <em>Deltas</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Deltas</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.ProductDeclaration#getDeltas()
   * @see #getProductDeclaration()
   * @generated
   */
  EReference getProductDeclaration_Deltas();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.productdeclaration.productDeclaration.ProductDeclaration#getConstraints <em>Constraints</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Constraints</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.ProductDeclaration#getConstraints()
   * @see #getProductDeclaration()
   * @generated
   */
  EReference getProductDeclaration_Constraints();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.productdeclaration.productDeclaration.ProductDeclaration#getPartitions <em>Partitions</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Partitions</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.ProductDeclaration#getPartitions()
   * @see #getProductDeclaration()
   * @generated
   */
  EReference getProductDeclaration_Partitions();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.productdeclaration.productDeclaration.ProductDeclaration#getProducts <em>Products</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Products</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.ProductDeclaration#getProducts()
   * @see #getProductDeclaration()
   * @generated
   */
  EReference getProductDeclaration_Products();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.productdeclaration.productDeclaration.Features <em>Features</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Features</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.Features
   * @generated
   */
  EClass getFeatures();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.dop.productdeclaration.productDeclaration.Features#getList <em>List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>List</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.Features#getList()
   * @see #getFeatures()
   * @generated
   */
  EReference getFeatures_List();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.productdeclaration.productDeclaration.Deltas <em>Deltas</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Deltas</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.Deltas
   * @generated
   */
  EClass getDeltas();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.dop.productdeclaration.productDeclaration.Deltas#getList <em>List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>List</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.Deltas#getList()
   * @see #getDeltas()
   * @generated
   */
  EReference getDeltas_List();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.productdeclaration.productDeclaration.Products <em>Products</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Products</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.Products
   * @generated
   */
  EClass getProducts();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.dop.productdeclaration.productDeclaration.Products#getList <em>List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>List</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.Products#getList()
   * @see #getProducts()
   * @generated
   */
  EReference getProducts_List();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.productdeclaration.productDeclaration.Product <em>Product</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Product</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.Product
   * @generated
   */
  EClass getProduct();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.dop.productdeclaration.productDeclaration.Product#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.Product#getName()
   * @see #getProduct()
   * @generated
   */
  EAttribute getProduct_Name();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.dop.productdeclaration.productDeclaration.Product#getList <em>List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>List</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.Product#getList()
   * @see #getProduct()
   * @generated
   */
  EReference getProduct_List();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.productdeclaration.productDeclaration.Partitions <em>Partitions</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Partitions</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.Partitions
   * @generated
   */
  EClass getPartitions();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.dop.productdeclaration.productDeclaration.Partitions#getList <em>List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>List</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.Partitions#getList()
   * @see #getPartitions()
   * @generated
   */
  EReference getPartitions_List();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.productdeclaration.productDeclaration.Partition <em>Partition</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Partition</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.Partition
   * @generated
   */
  EClass getPartition();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.dop.productdeclaration.productDeclaration.Partition#getConstr <em>Constr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Constr</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.Partition#getConstr()
   * @see #getPartition()
   * @generated
   */
  EReference getPartition_Constr();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.productdeclaration.productDeclaration.Constraint <em>Constraint</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Constraint</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.Constraint
   * @generated
   */
  EClass getConstraint();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.productdeclaration.productDeclaration.Constraint#getDelta <em>Delta</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Delta</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.Constraint#getDelta()
   * @see #getConstraint()
   * @generated
   */
  EReference getConstraint_Delta();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.productdeclaration.productDeclaration.Constraint#getExpr <em>Expr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expr</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.Constraint#getExpr()
   * @see #getConstraint()
   * @generated
   */
  EReference getConstraint_Expr();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.dop.productdeclaration.productDeclaration.Constraint#isLast <em>Last</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Last</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.Constraint#isLast()
   * @see #getConstraint()
   * @generated
   */
  EAttribute getConstraint_Last();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.productdeclaration.productDeclaration.ListElem <em>List Elem</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>List Elem</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.ListElem
   * @generated
   */
  EClass getListElem();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.productdeclaration.productDeclaration.ListElem#getElem <em>Elem</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Elem</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.ListElem#getElem()
   * @see #getListElem()
   * @generated
   */
  EReference getListElem_Elem();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.dop.productdeclaration.productDeclaration.ListElem#isLast <em>Last</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Last</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.ListElem#isLast()
   * @see #getListElem()
   * @generated
   */
  EAttribute getListElem_Last();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.productdeclaration.productDeclaration.Element <em>Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Element</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.Element
   * @generated
   */
  EClass getElement();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.dop.productdeclaration.productDeclaration.Element#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.Element#getName()
   * @see #getElement()
   * @generated
   */
  EAttribute getElement_Name();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.productdeclaration.productDeclaration.Core <em>Core</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Core</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.Core
   * @generated
   */
  EClass getCore();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.productdeclaration.productDeclaration.Core#getExpr <em>Expr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expr</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.Core#getExpr()
   * @see #getCore()
   * @generated
   */
  EReference getCore_Expr();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.productdeclaration.productDeclaration.Expression <em>Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Expression</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.Expression
   * @generated
   */
  EClass getExpression();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.productdeclaration.productDeclaration.Espression <em>Espression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Espression</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.Espression
   * @generated
   */
  EClass getEspression();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.productdeclaration.productDeclaration.Or <em>Or</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Or</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.Or
   * @generated
   */
  EClass getOr();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.productdeclaration.productDeclaration.Or#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.Or#getLeft()
   * @see #getOr()
   * @generated
   */
  EReference getOr_Left();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.productdeclaration.productDeclaration.Or#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.Or#getRight()
   * @see #getOr()
   * @generated
   */
  EReference getOr_Right();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.productdeclaration.productDeclaration.And <em>And</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>And</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.And
   * @generated
   */
  EClass getAnd();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.productdeclaration.productDeclaration.And#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.And#getLeft()
   * @see #getAnd()
   * @generated
   */
  EReference getAnd_Left();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.productdeclaration.productDeclaration.And#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.And#getRight()
   * @see #getAnd()
   * @generated
   */
  EReference getAnd_Right();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.productdeclaration.productDeclaration.Sub <em>Sub</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Sub</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.Sub
   * @generated
   */
  EClass getSub();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.productdeclaration.productDeclaration.Sub#getExpr <em>Expr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expr</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.Sub#getExpr()
   * @see #getSub()
   * @generated
   */
  EReference getSub_Expr();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.productdeclaration.productDeclaration.Not <em>Not</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Not</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.Not
   * @generated
   */
  EClass getNot();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.productdeclaration.productDeclaration.Not#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Value</em>'.
   * @see org.xtext.dop.productdeclaration.productDeclaration.Not#getValue()
   * @see #getNot()
   * @generated
   */
  EReference getNot_Value();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  ProductDeclarationFactory getProductDeclarationFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationImpl <em>Product Declaration</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationImpl
     * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationPackageImpl#getProductDeclaration()
     * @generated
     */
    EClass PRODUCT_DECLARATION = eINSTANCE.getProductDeclaration();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PRODUCT_DECLARATION__NAME = eINSTANCE.getProductDeclaration_Name();

    /**
     * The meta object literal for the '<em><b>Features</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PRODUCT_DECLARATION__FEATURES = eINSTANCE.getProductDeclaration_Features();

    /**
     * The meta object literal for the '<em><b>Deltas</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PRODUCT_DECLARATION__DELTAS = eINSTANCE.getProductDeclaration_Deltas();

    /**
     * The meta object literal for the '<em><b>Constraints</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PRODUCT_DECLARATION__CONSTRAINTS = eINSTANCE.getProductDeclaration_Constraints();

    /**
     * The meta object literal for the '<em><b>Partitions</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PRODUCT_DECLARATION__PARTITIONS = eINSTANCE.getProductDeclaration_Partitions();

    /**
     * The meta object literal for the '<em><b>Products</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PRODUCT_DECLARATION__PRODUCTS = eINSTANCE.getProductDeclaration_Products();

    /**
     * The meta object literal for the '{@link org.xtext.dop.productdeclaration.productDeclaration.impl.FeaturesImpl <em>Features</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.productdeclaration.productDeclaration.impl.FeaturesImpl
     * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationPackageImpl#getFeatures()
     * @generated
     */
    EClass FEATURES = eINSTANCE.getFeatures();

    /**
     * The meta object literal for the '<em><b>List</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FEATURES__LIST = eINSTANCE.getFeatures_List();

    /**
     * The meta object literal for the '{@link org.xtext.dop.productdeclaration.productDeclaration.impl.DeltasImpl <em>Deltas</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.productdeclaration.productDeclaration.impl.DeltasImpl
     * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationPackageImpl#getDeltas()
     * @generated
     */
    EClass DELTAS = eINSTANCE.getDeltas();

    /**
     * The meta object literal for the '<em><b>List</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DELTAS__LIST = eINSTANCE.getDeltas_List();

    /**
     * The meta object literal for the '{@link org.xtext.dop.productdeclaration.productDeclaration.impl.ProductsImpl <em>Products</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ProductsImpl
     * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationPackageImpl#getProducts()
     * @generated
     */
    EClass PRODUCTS = eINSTANCE.getProducts();

    /**
     * The meta object literal for the '<em><b>List</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PRODUCTS__LIST = eINSTANCE.getProducts_List();

    /**
     * The meta object literal for the '{@link org.xtext.dop.productdeclaration.productDeclaration.impl.ProductImpl <em>Product</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ProductImpl
     * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationPackageImpl#getProduct()
     * @generated
     */
    EClass PRODUCT = eINSTANCE.getProduct();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PRODUCT__NAME = eINSTANCE.getProduct_Name();

    /**
     * The meta object literal for the '<em><b>List</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PRODUCT__LIST = eINSTANCE.getProduct_List();

    /**
     * The meta object literal for the '{@link org.xtext.dop.productdeclaration.productDeclaration.impl.PartitionsImpl <em>Partitions</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.productdeclaration.productDeclaration.impl.PartitionsImpl
     * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationPackageImpl#getPartitions()
     * @generated
     */
    EClass PARTITIONS = eINSTANCE.getPartitions();

    /**
     * The meta object literal for the '<em><b>List</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PARTITIONS__LIST = eINSTANCE.getPartitions_List();

    /**
     * The meta object literal for the '{@link org.xtext.dop.productdeclaration.productDeclaration.impl.PartitionImpl <em>Partition</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.productdeclaration.productDeclaration.impl.PartitionImpl
     * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationPackageImpl#getPartition()
     * @generated
     */
    EClass PARTITION = eINSTANCE.getPartition();

    /**
     * The meta object literal for the '<em><b>Constr</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PARTITION__CONSTR = eINSTANCE.getPartition_Constr();

    /**
     * The meta object literal for the '{@link org.xtext.dop.productdeclaration.productDeclaration.impl.ConstraintImpl <em>Constraint</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ConstraintImpl
     * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationPackageImpl#getConstraint()
     * @generated
     */
    EClass CONSTRAINT = eINSTANCE.getConstraint();

    /**
     * The meta object literal for the '<em><b>Delta</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CONSTRAINT__DELTA = eINSTANCE.getConstraint_Delta();

    /**
     * The meta object literal for the '<em><b>Expr</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CONSTRAINT__EXPR = eINSTANCE.getConstraint_Expr();

    /**
     * The meta object literal for the '<em><b>Last</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute CONSTRAINT__LAST = eINSTANCE.getConstraint_Last();

    /**
     * The meta object literal for the '{@link org.xtext.dop.productdeclaration.productDeclaration.impl.ListElemImpl <em>List Elem</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ListElemImpl
     * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationPackageImpl#getListElem()
     * @generated
     */
    EClass LIST_ELEM = eINSTANCE.getListElem();

    /**
     * The meta object literal for the '<em><b>Elem</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LIST_ELEM__ELEM = eINSTANCE.getListElem_Elem();

    /**
     * The meta object literal for the '<em><b>Last</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute LIST_ELEM__LAST = eINSTANCE.getListElem_Last();

    /**
     * The meta object literal for the '{@link org.xtext.dop.productdeclaration.productDeclaration.impl.ElementImpl <em>Element</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ElementImpl
     * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationPackageImpl#getElement()
     * @generated
     */
    EClass ELEMENT = eINSTANCE.getElement();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ELEMENT__NAME = eINSTANCE.getElement_Name();

    /**
     * The meta object literal for the '{@link org.xtext.dop.productdeclaration.productDeclaration.impl.CoreImpl <em>Core</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.productdeclaration.productDeclaration.impl.CoreImpl
     * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationPackageImpl#getCore()
     * @generated
     */
    EClass CORE = eINSTANCE.getCore();

    /**
     * The meta object literal for the '<em><b>Expr</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CORE__EXPR = eINSTANCE.getCore_Expr();

    /**
     * The meta object literal for the '{@link org.xtext.dop.productdeclaration.productDeclaration.impl.ExpressionImpl <em>Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ExpressionImpl
     * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationPackageImpl#getExpression()
     * @generated
     */
    EClass EXPRESSION = eINSTANCE.getExpression();

    /**
     * The meta object literal for the '{@link org.xtext.dop.productdeclaration.productDeclaration.impl.EspressionImpl <em>Espression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.productdeclaration.productDeclaration.impl.EspressionImpl
     * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationPackageImpl#getEspression()
     * @generated
     */
    EClass ESPRESSION = eINSTANCE.getEspression();

    /**
     * The meta object literal for the '{@link org.xtext.dop.productdeclaration.productDeclaration.impl.OrImpl <em>Or</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.productdeclaration.productDeclaration.impl.OrImpl
     * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationPackageImpl#getOr()
     * @generated
     */
    EClass OR = eINSTANCE.getOr();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OR__LEFT = eINSTANCE.getOr_Left();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OR__RIGHT = eINSTANCE.getOr_Right();

    /**
     * The meta object literal for the '{@link org.xtext.dop.productdeclaration.productDeclaration.impl.AndImpl <em>And</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.productdeclaration.productDeclaration.impl.AndImpl
     * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationPackageImpl#getAnd()
     * @generated
     */
    EClass AND = eINSTANCE.getAnd();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference AND__LEFT = eINSTANCE.getAnd_Left();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference AND__RIGHT = eINSTANCE.getAnd_Right();

    /**
     * The meta object literal for the '{@link org.xtext.dop.productdeclaration.productDeclaration.impl.SubImpl <em>Sub</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.productdeclaration.productDeclaration.impl.SubImpl
     * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationPackageImpl#getSub()
     * @generated
     */
    EClass SUB = eINSTANCE.getSub();

    /**
     * The meta object literal for the '<em><b>Expr</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SUB__EXPR = eINSTANCE.getSub_Expr();

    /**
     * The meta object literal for the '{@link org.xtext.dop.productdeclaration.productDeclaration.impl.NotImpl <em>Not</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.productdeclaration.productDeclaration.impl.NotImpl
     * @see org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationPackageImpl#getNot()
     * @generated
     */
    EClass NOT = eINSTANCE.getNot();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference NOT__VALUE = eINSTANCE.getNot_Value();

  }

} //ProductDeclarationPackage
