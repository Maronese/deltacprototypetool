/**
 */
package org.xtext.dop.clang.clang;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type Qualifier Const</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getTypeQualifierConst()
 * @model
 * @generated
 */
public interface TypeQualifierConst extends TypeQualifier
{
} // TypeQualifierConst
