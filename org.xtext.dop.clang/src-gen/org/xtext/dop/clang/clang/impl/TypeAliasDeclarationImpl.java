/**
 */
package org.xtext.dop.clang.clang.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.xtext.dop.clang.clang.ClangPackage;
import org.xtext.dop.clang.clang.SpecifiedType;
import org.xtext.dop.clang.clang.TypeAliasDeclaration;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Type Alias Declaration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.clang.clang.impl.TypeAliasDeclarationImpl#getTdecl <em>Tdecl</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TypeAliasDeclarationImpl extends DeclarationImpl implements TypeAliasDeclaration
{
  /**
   * The cached value of the '{@link #getTdecl() <em>Tdecl</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTdecl()
   * @generated
   * @ordered
   */
  protected SpecifiedType tdecl;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TypeAliasDeclarationImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ClangPackage.Literals.TYPE_ALIAS_DECLARATION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SpecifiedType getTdecl()
  {
    return tdecl;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTdecl(SpecifiedType newTdecl, NotificationChain msgs)
  {
    SpecifiedType oldTdecl = tdecl;
    tdecl = newTdecl;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ClangPackage.TYPE_ALIAS_DECLARATION__TDECL, oldTdecl, newTdecl);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTdecl(SpecifiedType newTdecl)
  {
    if (newTdecl != tdecl)
    {
      NotificationChain msgs = null;
      if (tdecl != null)
        msgs = ((InternalEObject)tdecl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ClangPackage.TYPE_ALIAS_DECLARATION__TDECL, null, msgs);
      if (newTdecl != null)
        msgs = ((InternalEObject)newTdecl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ClangPackage.TYPE_ALIAS_DECLARATION__TDECL, null, msgs);
      msgs = basicSetTdecl(newTdecl, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ClangPackage.TYPE_ALIAS_DECLARATION__TDECL, newTdecl, newTdecl));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case ClangPackage.TYPE_ALIAS_DECLARATION__TDECL:
        return basicSetTdecl(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case ClangPackage.TYPE_ALIAS_DECLARATION__TDECL:
        return getTdecl();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case ClangPackage.TYPE_ALIAS_DECLARATION__TDECL:
        setTdecl((SpecifiedType)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case ClangPackage.TYPE_ALIAS_DECLARATION__TDECL:
        setTdecl((SpecifiedType)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case ClangPackage.TYPE_ALIAS_DECLARATION__TDECL:
        return tdecl != null;
    }
    return super.eIsSet(featureID);
  }

} //TypeAliasDeclarationImpl
