/**
 */
package org.xtext.dop.codebase.codeBase;

import org.xtext.dop.clang.clang.Declaration;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Modify Function</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.codebase.codeBase.ModifyFunction#getDecl <em>Decl</em>}</li>
 * </ul>
 *
 * @see org.xtext.dop.codebase.codeBase.CodeBasePackage#getModifyFunction()
 * @model
 * @generated
 */
public interface ModifyFunction extends SourceOperation
{
  /**
   * Returns the value of the '<em><b>Decl</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Decl</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Decl</em>' containment reference.
   * @see #setDecl(Declaration)
   * @see org.xtext.dop.codebase.codeBase.CodeBasePackage#getModifyFunction_Decl()
   * @model containment="true"
   * @generated
   */
  Declaration getDecl();

  /**
   * Sets the value of the '{@link org.xtext.dop.codebase.codeBase.ModifyFunction#getDecl <em>Decl</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Decl</em>' containment reference.
   * @see #getDecl()
   * @generated
   */
  void setDecl(Declaration value);

} // ModifyFunction
