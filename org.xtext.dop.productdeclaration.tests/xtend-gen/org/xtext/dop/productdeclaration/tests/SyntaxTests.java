package org.xtext.dop.productdeclaration.tests;

import com.google.inject.Inject;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.junit4.InjectWith;
import org.eclipse.xtext.junit4.XtextRunner;
import org.eclipse.xtext.junit4.util.ParseHelper;
import org.eclipse.xtext.junit4.validation.ValidationTestHelper;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Extension;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.xtext.dop.productdeclaration.ProductDeclarationInjectorProvider;
import org.xtext.dop.productdeclaration.productDeclaration.ProductDeclaration;
import org.xtext.dop.productdeclaration.productDeclaration.ProductDeclarationPackage;
import org.xtext.dop.productdeclaration.validation.ProductDeclarationValidator;

@RunWith(XtextRunner.class)
@InjectWith(ProductDeclarationInjectorProvider.class)
@SuppressWarnings("all")
public class SyntaxTests {
  @Inject
  @Extension
  private ParseHelper<ProductDeclaration> _parseHelper;
  
  @Inject
  @Extension
  private ValidationTestHelper _validationTestHelper;
  
  @Test
  public void emptyDecl() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("SPL Prova {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Features = {}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Deltas = {}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Constraints {}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Partitions {}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Products {}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      ProductDeclaration _parse = this._parseHelper.parse(_builder);
      this._validationTestHelper.assertNoErrors(_parse);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void exampleDecl() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("SPL Example {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Features = {alpha, beta, gamma, delta}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Deltas = {blue, red, green, black, yellow, orange}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Constraints {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("alpha & beta;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Partitions {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{blue} when (alpha),");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{red} when (alpha & delta),");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{green} when (!beta);");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{black} when (alpha & gamma),");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{yellow} when (beta);");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{orange} when (delta);");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Products {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("base = {alpha, beta};");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("adv = {alpha, beta, gamma};");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("full = {alpha, beta, gamma, delta};");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      ProductDeclaration _parse = this._parseHelper.parse(_builder);
      this._validationTestHelper.assertNoErrors(_parse);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void noLastFeaturesExampleDecl() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("SPL Example {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Features = {alpha, beta, gamma, delta,}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Deltas = {blue, red, green, black, yellow, orange}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Constraints {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("alpha & beta;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Partitions {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{blue} when (alpha),");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{red} when (alpha & delta),");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{green} when (!beta);");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{black} when (alpha & gamma),");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{yellow} when (beta);");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{orange} when (delta);");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Products {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("base = {alpha, beta};");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("adv = {alpha, beta, gamma};");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("full = {alpha, beta, gamma, delta};");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      ProductDeclaration _parse = this._parseHelper.parse(_builder);
      EClass _listElem = ProductDeclarationPackage.eINSTANCE.getListElem();
      this._validationTestHelper.assertError(_parse, _listElem, ProductDeclarationValidator.LIST_ELEM);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void featuresDuplicateExampleDecl() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("SPL Example {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Features = {alpha, beta, beta, gamma, delta}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Deltas = {blue, red, green, black, yellow, orange}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Constraints {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("alpha & beta;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Partitions {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{blue} when (alpha),");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{red} when (alpha & delta),");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{green} when (!beta);");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{black} when (alpha & gamma),");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{yellow} when (beta);");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{orange} when (delta);");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Products {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("base = {alpha, beta};");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("adv = {alpha, beta, gamma};");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("full = {alpha, beta, gamma, delta};");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      ProductDeclaration _parse = this._parseHelper.parse(_builder);
      EClass _element = ProductDeclarationPackage.eINSTANCE.getElement();
      this._validationTestHelper.assertError(_parse, _element, ProductDeclarationValidator.ELEMENT);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void noLastDeltasExampleDecl() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("SPL Example {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Features = {alpha, beta, gamma, delta}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Deltas = {blue, red, green, black, yellow, orange,}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Constraints {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("alpha & beta;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Partitions {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{blue} when (alpha),");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{red} when (alpha & delta),");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{green} when (!beta);");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{black} when (alpha & gamma),");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{yellow} when (beta);");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{orange} when (delta);");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Products {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("base = {alpha, beta};");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("adv = {alpha, beta, gamma};");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("full = {alpha, beta, gamma, delta};");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      ProductDeclaration _parse = this._parseHelper.parse(_builder);
      EClass _listElem = ProductDeclarationPackage.eINSTANCE.getListElem();
      this._validationTestHelper.assertError(_parse, _listElem, ProductDeclarationValidator.LIST_ELEM);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void deltasDuplicateExampleDecl() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("SPL Example {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Features = {alpha, beta, gamma, delta}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Deltas = {blue, blue, red, green, black, yellow, orange}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Constraints {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("alpha & beta;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Partitions {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{blue} when (alpha),");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{red} when (alpha & delta),");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{green} when (!beta);");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{black} when (alpha & gamma),");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{yellow} when (beta);");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{orange} when (delta);");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Products {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("base = {alpha, beta};");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("adv = {alpha, beta, gamma};");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("full = {alpha, beta, gamma, delta};");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      ProductDeclaration _parse = this._parseHelper.parse(_builder);
      EClass _element = ProductDeclarationPackage.eINSTANCE.getElement();
      this._validationTestHelper.assertError(_parse, _element, ProductDeclarationValidator.ELEMENT);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void constraintBaseFeatureNotDeclaredExampleDecl() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("SPL Example {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Features = {alpha, beta, gamma, delta}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Deltas = {blue, red, green, black, yellow, orange}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Constraints {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("alpha & beta & omega;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Partitions {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{blue} when (alpha),");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{red} when (alpha & delta),");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{green} when (!beta);");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{black} when (alpha & gamma),");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{yellow} when (beta);");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{orange} when (delta);");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Products {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("base = {alpha, beta};");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("adv = {alpha, beta, gamma};");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("full = {alpha, beta, gamma, delta};");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      ProductDeclaration _parse = this._parseHelper.parse(_builder);
      EClass _element = ProductDeclarationPackage.eINSTANCE.getElement();
      this._validationTestHelper.assertError(_parse, _element, ProductDeclarationValidator.ELEMENT);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void notLastConstraintExampleDecl() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("SPL Example {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Features = {alpha, beta, gamma, delta}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Deltas = {blue, red, green, black, yellow, orange}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Constraints {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("alpha & beta;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Partitions {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{blue} when (alpha),");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{red} when (alpha & delta),");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{green} when (!beta),;");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{black} when (alpha & gamma),");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{yellow} when (beta);");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{orange} when (delta);");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Products {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("base = {alpha, beta};");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("adv = {alpha, beta, gamma};");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("full = {alpha, beta, gamma, delta};");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      ProductDeclaration _parse = this._parseHelper.parse(_builder);
      EClass _constraint = ProductDeclarationPackage.eINSTANCE.getConstraint();
      this._validationTestHelper.assertError(_parse, _constraint, ProductDeclarationValidator.CONSTRAINT);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void constraintDeltaNotDeclaredExampleDecl() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("SPL Example {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Features = {alpha, beta, gamma, delta}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Deltas = {blue, red, green, black, yellow, orange}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Constraints {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("alpha & beta;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Partitions {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{blue} when (alpha),");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{red} when (alpha & delta),");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{green} when (!beta);");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{whithe} when (alpha & gamma),");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{yellow} when (beta);");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{orange} when (delta);");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Products {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("base = {alpha, beta};");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("adv = {alpha, beta, gamma};");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("full = {alpha, beta, gamma, delta};");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      ProductDeclaration _parse = this._parseHelper.parse(_builder);
      EClass _element = ProductDeclarationPackage.eINSTANCE.getElement();
      this._validationTestHelper.assertError(_parse, _element, ProductDeclarationValidator.ELEMENT);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void constraintFeatureNotDeclaredExampleDecl() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("SPL Example {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Features = {alpha, beta, gamma, delta}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Deltas = {blue, red, green, black, yellow, orange}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Constraints {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("alpha & beta;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Partitions {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{blue} when (alpha),");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{red} when (alpha & delta),");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{green} when (!beta);");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{black} when (alpha & omega),");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{yellow} when (beta);");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{orange} when (delta);");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Products {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("base = {alpha, beta};");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("adv = {alpha, beta, gamma};");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("full = {alpha, beta, gamma, delta};");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      ProductDeclaration _parse = this._parseHelper.parse(_builder);
      EClass _element = ProductDeclarationPackage.eINSTANCE.getElement();
      this._validationTestHelper.assertError(_parse, _element, ProductDeclarationValidator.ELEMENT);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void noLastProductFeatureExampleDecl() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("SPL Example {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Features = {alpha, beta, gamma, delta}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Deltas = {blue, red, green, black, yellow, orange}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Constraints {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("alpha & beta;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Partitions {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{blue} when (alpha),");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{red} when (alpha & delta),");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{green} when (!beta);");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{black} when (alpha & omega),");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{yellow} when (beta);");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{orange} when (delta);");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Products {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("base = {alpha, beta};");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("adv = {alpha, beta, gamma,};");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("full = {alpha, beta, gamma, delta};");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      ProductDeclaration _parse = this._parseHelper.parse(_builder);
      EClass _listElem = ProductDeclarationPackage.eINSTANCE.getListElem();
      this._validationTestHelper.assertError(_parse, _listElem, ProductDeclarationValidator.LIST_ELEM);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void productFeatureDuplicateExampleDecl() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("SPL Example {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Features = {alpha, beta, gamma, delta}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Deltas = {blue, red, green, black, yellow, orange}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Constraints {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("alpha & beta;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Partitions {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{blue} when (alpha),");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{red} when (alpha & delta),");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{green} when (!beta);");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{black} when (alpha & omega),");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{yellow} when (beta);");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{orange} when (delta);");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Products {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("base = {alpha, beta, beta};");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("adv = {alpha, beta, gamma};");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("full = {alpha, beta, gamma, delta};");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      ProductDeclaration _parse = this._parseHelper.parse(_builder);
      EClass _element = ProductDeclarationPackage.eINSTANCE.getElement();
      this._validationTestHelper.assertError(_parse, _element, ProductDeclarationValidator.ELEMENT);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void productDuplicateExampleDecl() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("SPL Example {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Features = {alpha, beta, gamma, delta}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Deltas = {blue, red, green, black, yellow, orange}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Constraints {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("alpha & beta;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Partitions {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{blue} when (alpha),");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{red} when (alpha & delta),");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{green} when (!beta);");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{black} when (alpha & omega),");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{yellow} when (beta);");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{orange} when (delta);");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Products {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("base = {alpha, beta};");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("base = {alpha, beta, gamma};");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("full = {alpha, beta, gamma, delta};");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      ProductDeclaration _parse = this._parseHelper.parse(_builder);
      EClass _product = ProductDeclarationPackage.eINSTANCE.getProduct();
      this._validationTestHelper.assertError(_parse, _product, ProductDeclarationValidator.PRODUCT);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void productFeatureNotDeclaredExampleDecl() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("SPL Example {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Features = {alpha, beta, gamma, delta}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Deltas = {blue, red, green, black, yellow, orange}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Constraints {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("alpha & beta;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Partitions {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{blue} when (alpha),");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{red} when (alpha & delta),");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{green} when (!beta);");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{black} when (alpha & omega),");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{yellow} when (beta);");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{orange} when (delta);");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Products {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("base = {alpha, beta};");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("adv = {alpha, beta, gamma, omega};");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("full = {alpha, beta, gamma, delta};");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      ProductDeclaration _parse = this._parseHelper.parse(_builder);
      EClass _element = ProductDeclarationPackage.eINSTANCE.getElement();
      this._validationTestHelper.assertError(_parse, _element, ProductDeclarationValidator.ELEMENT);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void productFeaturesNotContainsBasicExampleDecl() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("SPL Example {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Features = {alpha, beta, gamma, delta}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Deltas = {blue, red, green, black, yellow, orange}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Constraints {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("alpha & beta;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Partitions {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{blue} when (alpha),");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{red} when (alpha & delta),");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{green} when (!beta);");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{black} when (alpha & omega),");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{yellow} when (beta);");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("{orange} when (delta);");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Products {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("base = {alpha, beta};");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("base = {beta, gamma};");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("full = {alpha, beta, gamma, delta};");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      ProductDeclaration _parse = this._parseHelper.parse(_builder);
      EClass _product = ProductDeclarationPackage.eINSTANCE.getProduct();
      this._validationTestHelper.assertError(_parse, _product, ProductDeclarationValidator.PRODUCT);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
