/**
 */
package org.xtext.dop.productdeclaration.productDeclaration;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Partition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.productdeclaration.productDeclaration.Partition#getConstr <em>Constr</em>}</li>
 * </ul>
 *
 * @see org.xtext.dop.productdeclaration.productDeclaration.ProductDeclarationPackage#getPartition()
 * @model
 * @generated
 */
public interface Partition extends EObject
{
  /**
   * Returns the value of the '<em><b>Constr</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.dop.productdeclaration.productDeclaration.Constraint}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Constr</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Constr</em>' containment reference list.
   * @see org.xtext.dop.productdeclaration.productDeclaration.ProductDeclarationPackage#getPartition_Constr()
   * @model containment="true"
   * @generated
   */
  EList<Constraint> getConstr();

} // Partition
