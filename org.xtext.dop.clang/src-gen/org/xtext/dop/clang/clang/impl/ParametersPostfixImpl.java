/**
 */
package org.xtext.dop.clang.clang.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.xtext.dop.clang.clang.ClangPackage;
import org.xtext.dop.clang.clang.NextParameter;
import org.xtext.dop.clang.clang.Parameter;
import org.xtext.dop.clang.clang.ParametersPostfix;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Parameters Postfix</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.clang.clang.impl.ParametersPostfixImpl#getEls <em>Els</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.impl.ParametersPostfixImpl#getNext <em>Next</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.impl.ParametersPostfixImpl#getMore <em>More</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ParametersPostfixImpl extends MinimalEObjectImpl.Container implements ParametersPostfix
{
  /**
   * The cached value of the '{@link #getEls() <em>Els</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEls()
   * @generated
   * @ordered
   */
  protected EList<Parameter> els;

  /**
   * The cached value of the '{@link #getNext() <em>Next</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNext()
   * @generated
   * @ordered
   */
  protected EList<NextParameter> next;

  /**
   * The default value of the '{@link #getMore() <em>More</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMore()
   * @generated
   * @ordered
   */
  protected static final String MORE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getMore() <em>More</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMore()
   * @generated
   * @ordered
   */
  protected String more = MORE_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ParametersPostfixImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ClangPackage.Literals.PARAMETERS_POSTFIX;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Parameter> getEls()
  {
    if (els == null)
    {
      els = new EObjectContainmentEList<Parameter>(Parameter.class, this, ClangPackage.PARAMETERS_POSTFIX__ELS);
    }
    return els;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<NextParameter> getNext()
  {
    if (next == null)
    {
      next = new EObjectContainmentEList<NextParameter>(NextParameter.class, this, ClangPackage.PARAMETERS_POSTFIX__NEXT);
    }
    return next;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getMore()
  {
    return more;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setMore(String newMore)
  {
    String oldMore = more;
    more = newMore;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ClangPackage.PARAMETERS_POSTFIX__MORE, oldMore, more));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case ClangPackage.PARAMETERS_POSTFIX__ELS:
        return ((InternalEList<?>)getEls()).basicRemove(otherEnd, msgs);
      case ClangPackage.PARAMETERS_POSTFIX__NEXT:
        return ((InternalEList<?>)getNext()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case ClangPackage.PARAMETERS_POSTFIX__ELS:
        return getEls();
      case ClangPackage.PARAMETERS_POSTFIX__NEXT:
        return getNext();
      case ClangPackage.PARAMETERS_POSTFIX__MORE:
        return getMore();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case ClangPackage.PARAMETERS_POSTFIX__ELS:
        getEls().clear();
        getEls().addAll((Collection<? extends Parameter>)newValue);
        return;
      case ClangPackage.PARAMETERS_POSTFIX__NEXT:
        getNext().clear();
        getNext().addAll((Collection<? extends NextParameter>)newValue);
        return;
      case ClangPackage.PARAMETERS_POSTFIX__MORE:
        setMore((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case ClangPackage.PARAMETERS_POSTFIX__ELS:
        getEls().clear();
        return;
      case ClangPackage.PARAMETERS_POSTFIX__NEXT:
        getNext().clear();
        return;
      case ClangPackage.PARAMETERS_POSTFIX__MORE:
        setMore(MORE_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case ClangPackage.PARAMETERS_POSTFIX__ELS:
        return els != null && !els.isEmpty();
      case ClangPackage.PARAMETERS_POSTFIX__NEXT:
        return next != null && !next.isEmpty();
      case ClangPackage.PARAMETERS_POSTFIX__MORE:
        return MORE_EDEFAULT == null ? more != null : !MORE_EDEFAULT.equals(more);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (more: ");
    result.append(more);
    result.append(')');
    return result.toString();
  }

} //ParametersPostfixImpl
