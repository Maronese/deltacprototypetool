/**
 */
package org.xtext.dop.clang.clang.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.xtext.dop.clang.clang.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ClangFactoryImpl extends EFactoryImpl implements ClangFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static ClangFactory init()
  {
    try
    {
      ClangFactory theClangFactory = (ClangFactory)EPackage.Registry.INSTANCE.getEFactory(ClangPackage.eNS_URI);
      if (theClangFactory != null)
      {
        return theClangFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new ClangFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ClangFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case ClangPackage.MODEL: return createModel();
      case ClangPackage.STANDARD_INCLUDE: return createStandardInclude();
      case ClangPackage.FILE_INCLUDE: return createFileInclude();
      case ClangPackage.SOURCE: return createSource();
      case ClangPackage.DECLARATION: return createDeclaration();
      case ClangPackage.TYPE_ALIAS_DECLARATION: return createTypeAliasDeclaration();
      case ClangPackage.GENERIC_SPECIFIED_DECLARATION: return createGenericSpecifiedDeclaration();
      case ClangPackage.GENERIC_DECLARATION_ELEMENTS: return createGenericDeclarationElements();
      case ClangPackage.GENERIC_DECLARATION_ELEMENT: return createGenericDeclarationElement();
      case ClangPackage.GENERIC_DECLARATION_ID: return createGenericDeclarationId();
      case ClangPackage.GENERIC_DECLARATION_INNER_ID: return createGenericDeclarationInnerId();
      case ClangPackage.GENERIC_DECLARATION_ID_POSTFIX: return createGenericDeclarationIdPostfix();
      case ClangPackage.ARRAY_POSTFIX: return createArrayPostfix();
      case ClangPackage.PARAMETERS_POSTFIX: return createParametersPostfix();
      case ClangPackage.NEXT_PARAMETER: return createNextParameter();
      case ClangPackage.PARAMETER: return createParameter();
      case ClangPackage.GENERIC_DECLARATION_INITIALIZER: return createGenericDeclarationInitializer();
      case ClangPackage.SPECIFIED_TYPE: return createSpecifiedType();
      case ClangPackage.STATIC_ASSERT_DECLARATION: return createStaticAssertDeclaration();
      case ClangPackage.SIMPLE_TYPE: return createSimpleType();
      case ClangPackage.BASE_TYPE: return createBaseType();
      case ClangPackage.STRUCT_OR_UNION_TYPE: return createStructOrUnionType();
      case ClangPackage.ENUM_TYPE: return createEnumType();
      case ClangPackage.ENUMERATOR_LIST: return createEnumeratorList();
      case ClangPackage.ENUMERATOR: return createEnumerator();
      case ClangPackage.ENUMERATION_CONSTANT: return createEnumerationConstant();
      case ClangPackage.DECLARATION_SPECIFIER: return createDeclarationSpecifier();
      case ClangPackage.STORAGE_CLASS_SPECIFIER: return createStorageClassSpecifier();
      case ClangPackage.TYPE_QUALIFIER: return createTypeQualifier();
      case ClangPackage.FUNCTION_SPECIFIER: return createFunctionSpecifier();
      case ClangPackage.ALIGNMENT_SPECIFIER: return createAlignmentSpecifier();
      case ClangPackage.EXPRESSION: return createExpression();
      case ClangPackage.ASSIGNMENT_EXPRESSION_ELEMENT: return createAssignmentExpressionElement();
      case ClangPackage.STRING_EXPRESSION: return createStringExpression();
      case ClangPackage.GENERIC_ASSOCIATION: return createGenericAssociation();
      case ClangPackage.POSTFIX_EXPRESSION_POSTFIX: return createPostfixExpressionPostfix();
      case ClangPackage.NEXT_ASIGNEMENT: return createNextAsignement();
      case ClangPackage.COMPOUND_STATEMENT: return createCompoundStatement();
      case ClangPackage.BLOCK_ITEM: return createBlockItem();
      case ClangPackage.STATEMENTS: return createStatements();
      case ClangPackage.LABELED_STATEMENT: return createLabeledStatement();
      case ClangPackage.STATEMENT: return createStatement();
      case ClangPackage.EXPRESSION_STATEMENT: return createExpressionStatement();
      case ClangPackage.SELECTION_STATEMENT: return createSelectionStatement();
      case ClangPackage.ITERATION_STATEMENT: return createIterationStatement();
      case ClangPackage.NEXT_FIELD: return createNextField();
      case ClangPackage.JUMP_STATEMENT: return createJumpStatement();
      case ClangPackage.COMPLETE_NAME: return createCompleteName();
      case ClangPackage.SIMPLE_NAME: return createSimpleName();
      case ClangPackage.GENERIC_DECLARATION0: return createGenericDeclaration0();
      case ClangPackage.GENERIC_DECLARATION1: return createGenericDeclaration1();
      case ClangPackage.GENERIC_DECLARATION2: return createGenericDeclaration2();
      case ClangPackage.GLOBAL_NAME: return createGlobalName();
      case ClangPackage.EXPRESSION_INITIALIZER: return createExpressionInitializer();
      case ClangPackage.FUNCTION_INITIALIZER: return createFunctionInitializer();
      case ClangPackage.ATOMIC: return createAtomic();
      case ClangPackage.BASE_TYPE_VOID: return createBaseTypeVoid();
      case ClangPackage.CUSTOM: return createCustom();
      case ClangPackage.BASE_TYPE_CHAR: return createBaseTypeChar();
      case ClangPackage.BASE_TYPE_SHORT: return createBaseTypeShort();
      case ClangPackage.BASE_TYPE_INT: return createBaseTypeInt();
      case ClangPackage.BASE_TYPE_LONG: return createBaseTypeLong();
      case ClangPackage.BASE_TYPE_FLOAT: return createBaseTypeFloat();
      case ClangPackage.BASE_TYPE_DOUBLE: return createBaseTypeDouble();
      case ClangPackage.BASE_TYPE_SIGNED: return createBaseTypeSigned();
      case ClangPackage.BASE_TYPE_UNSIGNED: return createBaseTypeUnsigned();
      case ClangPackage.BASE_TYPE_BOOL: return createBaseTypeBool();
      case ClangPackage.BASE_TYPE_COMPLEX: return createBaseTypeComplex();
      case ClangPackage.BASE_TYPE_IMAGINARY: return createBaseTypeImaginary();
      case ClangPackage.STORAGE_CLASS_SPECIFIER_EXTERN: return createStorageClassSpecifierExtern();
      case ClangPackage.STORAGE_CLASS_SPECIFIER_STATIC: return createStorageClassSpecifierStatic();
      case ClangPackage.STORAGE_CLASS_SPECIFIER_THREAD_LOCAL: return createStorageClassSpecifierThreadLocal();
      case ClangPackage.STORAGE_CLASS_SPECIFIER_AUTO: return createStorageClassSpecifierAuto();
      case ClangPackage.STORAGE_CLASS_SPECIFIER_REGISTER: return createStorageClassSpecifierRegister();
      case ClangPackage.TYPE_QUALIFIER_CONST: return createTypeQualifierConst();
      case ClangPackage.TYPE_QUALIFIER_VOLATILE: return createTypeQualifierVolatile();
      case ClangPackage.TYPE_QUALIFIER_ATOMIC: return createTypeQualifierAtomic();
      case ClangPackage.FUNCTION_SPECIFIER_INLINE: return createFunctionSpecifierInline();
      case ClangPackage.FUNCTION_SPECIFIER_NO_RETURN: return createFunctionSpecifierNoReturn();
      case ClangPackage.ASSIGNMENT_EXPRESSION: return createAssignmentExpression();
      case ClangPackage.CONDITIONAL_EXPRESSION: return createConditionalExpression();
      case ClangPackage.LOGICAL_OR_EXPRESSION: return createLogicalOrExpression();
      case ClangPackage.LOGICAL_AND_EXPRESSION: return createLogicalAndExpression();
      case ClangPackage.INCLUSIVE_OR_EXPRESSION: return createInclusiveOrExpression();
      case ClangPackage.EXCLUSIVE_OR_EXPRESSION: return createExclusiveOrExpression();
      case ClangPackage.AND_EXPRESSION: return createAndExpression();
      case ClangPackage.EQUALITY_EXPRESSION: return createEqualityExpression();
      case ClangPackage.RELATIONAL_EXPRESSION: return createRelationalExpression();
      case ClangPackage.SHIFT_EXPRESSION: return createShiftExpression();
      case ClangPackage.ADDITIVE_EXPRESSION: return createAdditiveExpression();
      case ClangPackage.MULTIPLICATIVE_EXPRESSION: return createMultiplicativeExpression();
      case ClangPackage.EXPRESSION_SIMPLE: return createExpressionSimple();
      case ClangPackage.EXPRESSION_COMPLEX: return createExpressionComplex();
      case ClangPackage.EXPRESSION_INCREMENT: return createExpressionIncrement();
      case ClangPackage.EXPRESSION_DECREMENT: return createExpressionDecrement();
      case ClangPackage.EXPRESSION_SIZEOF: return createExpressionSizeof();
      case ClangPackage.EXPRESSION_UNARY_OP: return createExpressionUnaryOp();
      case ClangPackage.EXPRESSION_ALIGN: return createExpressionAlign();
      case ClangPackage.EXPRESSION_CAST: return createExpressionCast();
      case ClangPackage.EXPRESSION_IDENTIFIER: return createExpressionIdentifier();
      case ClangPackage.EXPRESSION_STRING: return createExpressionString();
      case ClangPackage.EXPRESSION_INTEGER: return createExpressionInteger();
      case ClangPackage.EXPRESSION_FLOAT: return createExpressionFloat();
      case ClangPackage.EXPRESSION_SUB_EXPRESSION: return createExpressionSubExpression();
      case ClangPackage.EXPRESSION_GENERIC_SELECTION: return createExpressionGenericSelection();
      case ClangPackage.GENERIC_ASSOCIATION_CASE: return createGenericAssociationCase();
      case ClangPackage.GENERIC_ASSOCIATION_DEFAULT: return createGenericAssociationDefault();
      case ClangPackage.POSTFIX_EXPRESSION_POSTFIX_EXPRESSION: return createPostfixExpressionPostfixExpression();
      case ClangPackage.POSTFIX_EXPRESSION_POSTFIX_ARGUMENT_LIST: return createPostfixExpressionPostfixArgumentList();
      case ClangPackage.POSTFIX_EXPRESSION_POSTFIX_ACCESS: return createPostfixExpressionPostfixAccess();
      case ClangPackage.POSTFIX_EXPRESSION_POSTFIX_ACCESS_PTR: return createPostfixExpressionPostfixAccessPtr();
      case ClangPackage.POSTFIX_EXPRESSION_POSTFIX_INCREMENT: return createPostfixExpressionPostfixIncrement();
      case ClangPackage.POSTFIX_EXPRESSION_POSTFIX_DECREMENT: return createPostfixExpressionPostfixDecrement();
      case ClangPackage.STATEMENT_DECLARATION: return createStatementDeclaration();
      case ClangPackage.STATEMENT_INNER_STATEMENT: return createStatementInnerStatement();
      case ClangPackage.LABELED_STATEMENT_CASE: return createLabeledStatementCase();
      case ClangPackage.LABELED_STATEMENT_DEFAULT: return createLabeledStatementDefault();
      case ClangPackage.GO_TO_PREFIX_STATEMENT: return createGoToPrefixStatement();
      case ClangPackage.SELECTION_STATEMENT_IF: return createSelectionStatementIf();
      case ClangPackage.SELECTION_STATEMENT_SWITCH: return createSelectionStatementSwitch();
      case ClangPackage.ITERATION_STATEMENT_WHILE: return createIterationStatementWhile();
      case ClangPackage.ITERATION_STATEMENT_DO: return createIterationStatementDo();
      case ClangPackage.ITERATION_STATEMENT_FOR: return createIterationStatementFor();
      case ClangPackage.JUMP_STATEMENT_GOTO: return createJumpStatementGoto();
      case ClangPackage.JUMP_STATEMENT_CONTINUE: return createJumpStatementContinue();
      case ClangPackage.JUMP_STATEMENT_BREAK: return createJumpStatementBreak();
      case ClangPackage.JUMP_STATEMENT_RETURN: return createJumpStatementReturn();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Model createModel()
  {
    ModelImpl model = new ModelImpl();
    return model;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StandardInclude createStandardInclude()
  {
    StandardIncludeImpl standardInclude = new StandardIncludeImpl();
    return standardInclude;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FileInclude createFileInclude()
  {
    FileIncludeImpl fileInclude = new FileIncludeImpl();
    return fileInclude;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Source createSource()
  {
    SourceImpl source = new SourceImpl();
    return source;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Declaration createDeclaration()
  {
    DeclarationImpl declaration = new DeclarationImpl();
    return declaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TypeAliasDeclaration createTypeAliasDeclaration()
  {
    TypeAliasDeclarationImpl typeAliasDeclaration = new TypeAliasDeclarationImpl();
    return typeAliasDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GenericSpecifiedDeclaration createGenericSpecifiedDeclaration()
  {
    GenericSpecifiedDeclarationImpl genericSpecifiedDeclaration = new GenericSpecifiedDeclarationImpl();
    return genericSpecifiedDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GenericDeclarationElements createGenericDeclarationElements()
  {
    GenericDeclarationElementsImpl genericDeclarationElements = new GenericDeclarationElementsImpl();
    return genericDeclarationElements;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GenericDeclarationElement createGenericDeclarationElement()
  {
    GenericDeclarationElementImpl genericDeclarationElement = new GenericDeclarationElementImpl();
    return genericDeclarationElement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GenericDeclarationId createGenericDeclarationId()
  {
    GenericDeclarationIdImpl genericDeclarationId = new GenericDeclarationIdImpl();
    return genericDeclarationId;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GenericDeclarationInnerId createGenericDeclarationInnerId()
  {
    GenericDeclarationInnerIdImpl genericDeclarationInnerId = new GenericDeclarationInnerIdImpl();
    return genericDeclarationInnerId;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GenericDeclarationIdPostfix createGenericDeclarationIdPostfix()
  {
    GenericDeclarationIdPostfixImpl genericDeclarationIdPostfix = new GenericDeclarationIdPostfixImpl();
    return genericDeclarationIdPostfix;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ArrayPostfix createArrayPostfix()
  {
    ArrayPostfixImpl arrayPostfix = new ArrayPostfixImpl();
    return arrayPostfix;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ParametersPostfix createParametersPostfix()
  {
    ParametersPostfixImpl parametersPostfix = new ParametersPostfixImpl();
    return parametersPostfix;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NextParameter createNextParameter()
  {
    NextParameterImpl nextParameter = new NextParameterImpl();
    return nextParameter;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Parameter createParameter()
  {
    ParameterImpl parameter = new ParameterImpl();
    return parameter;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GenericDeclarationInitializer createGenericDeclarationInitializer()
  {
    GenericDeclarationInitializerImpl genericDeclarationInitializer = new GenericDeclarationInitializerImpl();
    return genericDeclarationInitializer;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SpecifiedType createSpecifiedType()
  {
    SpecifiedTypeImpl specifiedType = new SpecifiedTypeImpl();
    return specifiedType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StaticAssertDeclaration createStaticAssertDeclaration()
  {
    StaticAssertDeclarationImpl staticAssertDeclaration = new StaticAssertDeclarationImpl();
    return staticAssertDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SimpleType createSimpleType()
  {
    SimpleTypeImpl simpleType = new SimpleTypeImpl();
    return simpleType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BaseType createBaseType()
  {
    BaseTypeImpl baseType = new BaseTypeImpl();
    return baseType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StructOrUnionType createStructOrUnionType()
  {
    StructOrUnionTypeImpl structOrUnionType = new StructOrUnionTypeImpl();
    return structOrUnionType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EnumType createEnumType()
  {
    EnumTypeImpl enumType = new EnumTypeImpl();
    return enumType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EnumeratorList createEnumeratorList()
  {
    EnumeratorListImpl enumeratorList = new EnumeratorListImpl();
    return enumeratorList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Enumerator createEnumerator()
  {
    EnumeratorImpl enumerator = new EnumeratorImpl();
    return enumerator;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EnumerationConstant createEnumerationConstant()
  {
    EnumerationConstantImpl enumerationConstant = new EnumerationConstantImpl();
    return enumerationConstant;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DeclarationSpecifier createDeclarationSpecifier()
  {
    DeclarationSpecifierImpl declarationSpecifier = new DeclarationSpecifierImpl();
    return declarationSpecifier;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StorageClassSpecifier createStorageClassSpecifier()
  {
    StorageClassSpecifierImpl storageClassSpecifier = new StorageClassSpecifierImpl();
    return storageClassSpecifier;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TypeQualifier createTypeQualifier()
  {
    TypeQualifierImpl typeQualifier = new TypeQualifierImpl();
    return typeQualifier;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FunctionSpecifier createFunctionSpecifier()
  {
    FunctionSpecifierImpl functionSpecifier = new FunctionSpecifierImpl();
    return functionSpecifier;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AlignmentSpecifier createAlignmentSpecifier()
  {
    AlignmentSpecifierImpl alignmentSpecifier = new AlignmentSpecifierImpl();
    return alignmentSpecifier;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Expression createExpression()
  {
    ExpressionImpl expression = new ExpressionImpl();
    return expression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AssignmentExpressionElement createAssignmentExpressionElement()
  {
    AssignmentExpressionElementImpl assignmentExpressionElement = new AssignmentExpressionElementImpl();
    return assignmentExpressionElement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StringExpression createStringExpression()
  {
    StringExpressionImpl stringExpression = new StringExpressionImpl();
    return stringExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GenericAssociation createGenericAssociation()
  {
    GenericAssociationImpl genericAssociation = new GenericAssociationImpl();
    return genericAssociation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PostfixExpressionPostfix createPostfixExpressionPostfix()
  {
    PostfixExpressionPostfixImpl postfixExpressionPostfix = new PostfixExpressionPostfixImpl();
    return postfixExpressionPostfix;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NextAsignement createNextAsignement()
  {
    NextAsignementImpl nextAsignement = new NextAsignementImpl();
    return nextAsignement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CompoundStatement createCompoundStatement()
  {
    CompoundStatementImpl compoundStatement = new CompoundStatementImpl();
    return compoundStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BlockItem createBlockItem()
  {
    BlockItemImpl blockItem = new BlockItemImpl();
    return blockItem;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Statements createStatements()
  {
    StatementsImpl statements = new StatementsImpl();
    return statements;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LabeledStatement createLabeledStatement()
  {
    LabeledStatementImpl labeledStatement = new LabeledStatementImpl();
    return labeledStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Statement createStatement()
  {
    StatementImpl statement = new StatementImpl();
    return statement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExpressionStatement createExpressionStatement()
  {
    ExpressionStatementImpl expressionStatement = new ExpressionStatementImpl();
    return expressionStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SelectionStatement createSelectionStatement()
  {
    SelectionStatementImpl selectionStatement = new SelectionStatementImpl();
    return selectionStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IterationStatement createIterationStatement()
  {
    IterationStatementImpl iterationStatement = new IterationStatementImpl();
    return iterationStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NextField createNextField()
  {
    NextFieldImpl nextField = new NextFieldImpl();
    return nextField;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public JumpStatement createJumpStatement()
  {
    JumpStatementImpl jumpStatement = new JumpStatementImpl();
    return jumpStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CompleteName createCompleteName()
  {
    CompleteNameImpl completeName = new CompleteNameImpl();
    return completeName;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SimpleName createSimpleName()
  {
    SimpleNameImpl simpleName = new SimpleNameImpl();
    return simpleName;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GenericDeclaration0 createGenericDeclaration0()
  {
    GenericDeclaration0Impl genericDeclaration0 = new GenericDeclaration0Impl();
    return genericDeclaration0;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GenericDeclaration1 createGenericDeclaration1()
  {
    GenericDeclaration1Impl genericDeclaration1 = new GenericDeclaration1Impl();
    return genericDeclaration1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GenericDeclaration2 createGenericDeclaration2()
  {
    GenericDeclaration2Impl genericDeclaration2 = new GenericDeclaration2Impl();
    return genericDeclaration2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GlobalName createGlobalName()
  {
    GlobalNameImpl globalName = new GlobalNameImpl();
    return globalName;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExpressionInitializer createExpressionInitializer()
  {
    ExpressionInitializerImpl expressionInitializer = new ExpressionInitializerImpl();
    return expressionInitializer;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FunctionInitializer createFunctionInitializer()
  {
    FunctionInitializerImpl functionInitializer = new FunctionInitializerImpl();
    return functionInitializer;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Atomic createAtomic()
  {
    AtomicImpl atomic = new AtomicImpl();
    return atomic;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BaseTypeVoid createBaseTypeVoid()
  {
    BaseTypeVoidImpl baseTypeVoid = new BaseTypeVoidImpl();
    return baseTypeVoid;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Custom createCustom()
  {
    CustomImpl custom = new CustomImpl();
    return custom;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BaseTypeChar createBaseTypeChar()
  {
    BaseTypeCharImpl baseTypeChar = new BaseTypeCharImpl();
    return baseTypeChar;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BaseTypeShort createBaseTypeShort()
  {
    BaseTypeShortImpl baseTypeShort = new BaseTypeShortImpl();
    return baseTypeShort;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BaseTypeInt createBaseTypeInt()
  {
    BaseTypeIntImpl baseTypeInt = new BaseTypeIntImpl();
    return baseTypeInt;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BaseTypeLong createBaseTypeLong()
  {
    BaseTypeLongImpl baseTypeLong = new BaseTypeLongImpl();
    return baseTypeLong;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BaseTypeFloat createBaseTypeFloat()
  {
    BaseTypeFloatImpl baseTypeFloat = new BaseTypeFloatImpl();
    return baseTypeFloat;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BaseTypeDouble createBaseTypeDouble()
  {
    BaseTypeDoubleImpl baseTypeDouble = new BaseTypeDoubleImpl();
    return baseTypeDouble;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BaseTypeSigned createBaseTypeSigned()
  {
    BaseTypeSignedImpl baseTypeSigned = new BaseTypeSignedImpl();
    return baseTypeSigned;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BaseTypeUnsigned createBaseTypeUnsigned()
  {
    BaseTypeUnsignedImpl baseTypeUnsigned = new BaseTypeUnsignedImpl();
    return baseTypeUnsigned;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BaseTypeBool createBaseTypeBool()
  {
    BaseTypeBoolImpl baseTypeBool = new BaseTypeBoolImpl();
    return baseTypeBool;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BaseTypeComplex createBaseTypeComplex()
  {
    BaseTypeComplexImpl baseTypeComplex = new BaseTypeComplexImpl();
    return baseTypeComplex;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BaseTypeImaginary createBaseTypeImaginary()
  {
    BaseTypeImaginaryImpl baseTypeImaginary = new BaseTypeImaginaryImpl();
    return baseTypeImaginary;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StorageClassSpecifierExtern createStorageClassSpecifierExtern()
  {
    StorageClassSpecifierExternImpl storageClassSpecifierExtern = new StorageClassSpecifierExternImpl();
    return storageClassSpecifierExtern;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StorageClassSpecifierStatic createStorageClassSpecifierStatic()
  {
    StorageClassSpecifierStaticImpl storageClassSpecifierStatic = new StorageClassSpecifierStaticImpl();
    return storageClassSpecifierStatic;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StorageClassSpecifierThreadLocal createStorageClassSpecifierThreadLocal()
  {
    StorageClassSpecifierThreadLocalImpl storageClassSpecifierThreadLocal = new StorageClassSpecifierThreadLocalImpl();
    return storageClassSpecifierThreadLocal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StorageClassSpecifierAuto createStorageClassSpecifierAuto()
  {
    StorageClassSpecifierAutoImpl storageClassSpecifierAuto = new StorageClassSpecifierAutoImpl();
    return storageClassSpecifierAuto;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StorageClassSpecifierRegister createStorageClassSpecifierRegister()
  {
    StorageClassSpecifierRegisterImpl storageClassSpecifierRegister = new StorageClassSpecifierRegisterImpl();
    return storageClassSpecifierRegister;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TypeQualifierConst createTypeQualifierConst()
  {
    TypeQualifierConstImpl typeQualifierConst = new TypeQualifierConstImpl();
    return typeQualifierConst;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TypeQualifierVolatile createTypeQualifierVolatile()
  {
    TypeQualifierVolatileImpl typeQualifierVolatile = new TypeQualifierVolatileImpl();
    return typeQualifierVolatile;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TypeQualifierAtomic createTypeQualifierAtomic()
  {
    TypeQualifierAtomicImpl typeQualifierAtomic = new TypeQualifierAtomicImpl();
    return typeQualifierAtomic;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FunctionSpecifierInline createFunctionSpecifierInline()
  {
    FunctionSpecifierInlineImpl functionSpecifierInline = new FunctionSpecifierInlineImpl();
    return functionSpecifierInline;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FunctionSpecifierNoReturn createFunctionSpecifierNoReturn()
  {
    FunctionSpecifierNoReturnImpl functionSpecifierNoReturn = new FunctionSpecifierNoReturnImpl();
    return functionSpecifierNoReturn;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AssignmentExpression createAssignmentExpression()
  {
    AssignmentExpressionImpl assignmentExpression = new AssignmentExpressionImpl();
    return assignmentExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ConditionalExpression createConditionalExpression()
  {
    ConditionalExpressionImpl conditionalExpression = new ConditionalExpressionImpl();
    return conditionalExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LogicalOrExpression createLogicalOrExpression()
  {
    LogicalOrExpressionImpl logicalOrExpression = new LogicalOrExpressionImpl();
    return logicalOrExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LogicalAndExpression createLogicalAndExpression()
  {
    LogicalAndExpressionImpl logicalAndExpression = new LogicalAndExpressionImpl();
    return logicalAndExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public InclusiveOrExpression createInclusiveOrExpression()
  {
    InclusiveOrExpressionImpl inclusiveOrExpression = new InclusiveOrExpressionImpl();
    return inclusiveOrExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExclusiveOrExpression createExclusiveOrExpression()
  {
    ExclusiveOrExpressionImpl exclusiveOrExpression = new ExclusiveOrExpressionImpl();
    return exclusiveOrExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AndExpression createAndExpression()
  {
    AndExpressionImpl andExpression = new AndExpressionImpl();
    return andExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EqualityExpression createEqualityExpression()
  {
    EqualityExpressionImpl equalityExpression = new EqualityExpressionImpl();
    return equalityExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RelationalExpression createRelationalExpression()
  {
    RelationalExpressionImpl relationalExpression = new RelationalExpressionImpl();
    return relationalExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ShiftExpression createShiftExpression()
  {
    ShiftExpressionImpl shiftExpression = new ShiftExpressionImpl();
    return shiftExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AdditiveExpression createAdditiveExpression()
  {
    AdditiveExpressionImpl additiveExpression = new AdditiveExpressionImpl();
    return additiveExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MultiplicativeExpression createMultiplicativeExpression()
  {
    MultiplicativeExpressionImpl multiplicativeExpression = new MultiplicativeExpressionImpl();
    return multiplicativeExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExpressionSimple createExpressionSimple()
  {
    ExpressionSimpleImpl expressionSimple = new ExpressionSimpleImpl();
    return expressionSimple;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExpressionComplex createExpressionComplex()
  {
    ExpressionComplexImpl expressionComplex = new ExpressionComplexImpl();
    return expressionComplex;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExpressionIncrement createExpressionIncrement()
  {
    ExpressionIncrementImpl expressionIncrement = new ExpressionIncrementImpl();
    return expressionIncrement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExpressionDecrement createExpressionDecrement()
  {
    ExpressionDecrementImpl expressionDecrement = new ExpressionDecrementImpl();
    return expressionDecrement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExpressionSizeof createExpressionSizeof()
  {
    ExpressionSizeofImpl expressionSizeof = new ExpressionSizeofImpl();
    return expressionSizeof;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExpressionUnaryOp createExpressionUnaryOp()
  {
    ExpressionUnaryOpImpl expressionUnaryOp = new ExpressionUnaryOpImpl();
    return expressionUnaryOp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExpressionAlign createExpressionAlign()
  {
    ExpressionAlignImpl expressionAlign = new ExpressionAlignImpl();
    return expressionAlign;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExpressionCast createExpressionCast()
  {
    ExpressionCastImpl expressionCast = new ExpressionCastImpl();
    return expressionCast;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExpressionIdentifier createExpressionIdentifier()
  {
    ExpressionIdentifierImpl expressionIdentifier = new ExpressionIdentifierImpl();
    return expressionIdentifier;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExpressionString createExpressionString()
  {
    ExpressionStringImpl expressionString = new ExpressionStringImpl();
    return expressionString;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExpressionInteger createExpressionInteger()
  {
    ExpressionIntegerImpl expressionInteger = new ExpressionIntegerImpl();
    return expressionInteger;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExpressionFloat createExpressionFloat()
  {
    ExpressionFloatImpl expressionFloat = new ExpressionFloatImpl();
    return expressionFloat;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExpressionSubExpression createExpressionSubExpression()
  {
    ExpressionSubExpressionImpl expressionSubExpression = new ExpressionSubExpressionImpl();
    return expressionSubExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExpressionGenericSelection createExpressionGenericSelection()
  {
    ExpressionGenericSelectionImpl expressionGenericSelection = new ExpressionGenericSelectionImpl();
    return expressionGenericSelection;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GenericAssociationCase createGenericAssociationCase()
  {
    GenericAssociationCaseImpl genericAssociationCase = new GenericAssociationCaseImpl();
    return genericAssociationCase;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GenericAssociationDefault createGenericAssociationDefault()
  {
    GenericAssociationDefaultImpl genericAssociationDefault = new GenericAssociationDefaultImpl();
    return genericAssociationDefault;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PostfixExpressionPostfixExpression createPostfixExpressionPostfixExpression()
  {
    PostfixExpressionPostfixExpressionImpl postfixExpressionPostfixExpression = new PostfixExpressionPostfixExpressionImpl();
    return postfixExpressionPostfixExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PostfixExpressionPostfixArgumentList createPostfixExpressionPostfixArgumentList()
  {
    PostfixExpressionPostfixArgumentListImpl postfixExpressionPostfixArgumentList = new PostfixExpressionPostfixArgumentListImpl();
    return postfixExpressionPostfixArgumentList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PostfixExpressionPostfixAccess createPostfixExpressionPostfixAccess()
  {
    PostfixExpressionPostfixAccessImpl postfixExpressionPostfixAccess = new PostfixExpressionPostfixAccessImpl();
    return postfixExpressionPostfixAccess;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PostfixExpressionPostfixAccessPtr createPostfixExpressionPostfixAccessPtr()
  {
    PostfixExpressionPostfixAccessPtrImpl postfixExpressionPostfixAccessPtr = new PostfixExpressionPostfixAccessPtrImpl();
    return postfixExpressionPostfixAccessPtr;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PostfixExpressionPostfixIncrement createPostfixExpressionPostfixIncrement()
  {
    PostfixExpressionPostfixIncrementImpl postfixExpressionPostfixIncrement = new PostfixExpressionPostfixIncrementImpl();
    return postfixExpressionPostfixIncrement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PostfixExpressionPostfixDecrement createPostfixExpressionPostfixDecrement()
  {
    PostfixExpressionPostfixDecrementImpl postfixExpressionPostfixDecrement = new PostfixExpressionPostfixDecrementImpl();
    return postfixExpressionPostfixDecrement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StatementDeclaration createStatementDeclaration()
  {
    StatementDeclarationImpl statementDeclaration = new StatementDeclarationImpl();
    return statementDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StatementInnerStatement createStatementInnerStatement()
  {
    StatementInnerStatementImpl statementInnerStatement = new StatementInnerStatementImpl();
    return statementInnerStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LabeledStatementCase createLabeledStatementCase()
  {
    LabeledStatementCaseImpl labeledStatementCase = new LabeledStatementCaseImpl();
    return labeledStatementCase;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LabeledStatementDefault createLabeledStatementDefault()
  {
    LabeledStatementDefaultImpl labeledStatementDefault = new LabeledStatementDefaultImpl();
    return labeledStatementDefault;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GoToPrefixStatement createGoToPrefixStatement()
  {
    GoToPrefixStatementImpl goToPrefixStatement = new GoToPrefixStatementImpl();
    return goToPrefixStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SelectionStatementIf createSelectionStatementIf()
  {
    SelectionStatementIfImpl selectionStatementIf = new SelectionStatementIfImpl();
    return selectionStatementIf;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SelectionStatementSwitch createSelectionStatementSwitch()
  {
    SelectionStatementSwitchImpl selectionStatementSwitch = new SelectionStatementSwitchImpl();
    return selectionStatementSwitch;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IterationStatementWhile createIterationStatementWhile()
  {
    IterationStatementWhileImpl iterationStatementWhile = new IterationStatementWhileImpl();
    return iterationStatementWhile;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IterationStatementDo createIterationStatementDo()
  {
    IterationStatementDoImpl iterationStatementDo = new IterationStatementDoImpl();
    return iterationStatementDo;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IterationStatementFor createIterationStatementFor()
  {
    IterationStatementForImpl iterationStatementFor = new IterationStatementForImpl();
    return iterationStatementFor;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public JumpStatementGoto createJumpStatementGoto()
  {
    JumpStatementGotoImpl jumpStatementGoto = new JumpStatementGotoImpl();
    return jumpStatementGoto;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public JumpStatementContinue createJumpStatementContinue()
  {
    JumpStatementContinueImpl jumpStatementContinue = new JumpStatementContinueImpl();
    return jumpStatementContinue;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public JumpStatementBreak createJumpStatementBreak()
  {
    JumpStatementBreakImpl jumpStatementBreak = new JumpStatementBreakImpl();
    return jumpStatementBreak;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public JumpStatementReturn createJumpStatementReturn()
  {
    JumpStatementReturnImpl jumpStatementReturn = new JumpStatementReturnImpl();
    return jumpStatementReturn;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ClangPackage getClangPackage()
  {
    return (ClangPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static ClangPackage getPackage()
  {
    return ClangPackage.eINSTANCE;
  }

} //ClangFactoryImpl
