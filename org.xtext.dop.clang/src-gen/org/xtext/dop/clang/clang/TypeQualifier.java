/**
 */
package org.xtext.dop.clang.clang;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type Qualifier</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getTypeQualifier()
 * @model
 * @generated
 */
public interface TypeQualifier extends DeclarationSpecifier
{
} // TypeQualifier
