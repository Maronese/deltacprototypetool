/**
 * generated by Xtext
 */
package org.xtext.dop.codebase.scoping;

import org.xtext.dop.clang.scoping.ClangScopeProvider;

/**
 * This class contains custom scoping description.
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#scoping
 * on how and when to use it.
 */
@SuppressWarnings("all")
public class CodeBaseScopeProvider extends ClangScopeProvider {
}
