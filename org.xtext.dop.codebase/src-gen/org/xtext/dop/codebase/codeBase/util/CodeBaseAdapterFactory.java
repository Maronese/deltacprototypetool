/**
 */
package org.xtext.dop.codebase.codeBase.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import org.xtext.dop.codebase.codeBase.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.xtext.dop.codebase.codeBase.CodeBasePackage
 * @generated
 */
public class CodeBaseAdapterFactory extends AdapterFactoryImpl
{
  /**
   * The cached model package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static CodeBasePackage modelPackage;

  /**
   * Creates an instance of the adapter factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CodeBaseAdapterFactory()
  {
    if (modelPackage == null)
    {
      modelPackage = CodeBasePackage.eINSTANCE;
    }
  }

  /**
   * Returns whether this factory is applicable for the type of the object.
   * <!-- begin-user-doc -->
   * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
   * <!-- end-user-doc -->
   * @return whether this factory is applicable for the type of the object.
   * @generated
   */
  @Override
  public boolean isFactoryForType(Object object)
  {
    if (object == modelPackage)
    {
      return true;
    }
    if (object instanceof EObject)
    {
      return ((EObject)object).eClass().getEPackage() == modelPackage;
    }
    return false;
  }

  /**
   * The switch that delegates to the <code>createXXX</code> methods.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected CodeBaseSwitch<Adapter> modelSwitch =
    new CodeBaseSwitch<Adapter>()
    {
      @Override
      public Adapter caseDeltaModule(DeltaModule object)
      {
        return createDeltaModuleAdapter();
      }
      @Override
      public Adapter caseFileOperation(FileOperation object)
      {
        return createFileOperationAdapter();
      }
      @Override
      public Adapter caseAddSource(AddSource object)
      {
        return createAddSourceAdapter();
      }
      @Override
      public Adapter caseRemoveSource(RemoveSource object)
      {
        return createRemoveSourceAdapter();
      }
      @Override
      public Adapter caseSourceOperation(SourceOperation object)
      {
        return createSourceOperationAdapter();
      }
      @Override
      public Adapter caseAdds(Adds object)
      {
        return createAddsAdapter();
      }
      @Override
      public Adapter caseRemoves(Removes object)
      {
        return createRemovesAdapter();
      }
      @Override
      public Adapter caseModifies(Modifies object)
      {
        return createModifiesAdapter();
      }
      @Override
      public Adapter caseAddDeclaration(AddDeclaration object)
      {
        return createAddDeclarationAdapter();
      }
      @Override
      public Adapter caseAddStandardInclude(AddStandardInclude object)
      {
        return createAddStandardIncludeAdapter();
      }
      @Override
      public Adapter caseAddFileInclude(AddFileInclude object)
      {
        return createAddFileIncludeAdapter();
      }
      @Override
      public Adapter caseRemoveGlobal(RemoveGlobal object)
      {
        return createRemoveGlobalAdapter();
      }
      @Override
      public Adapter caseRemoveStandardInclude(RemoveStandardInclude object)
      {
        return createRemoveStandardIncludeAdapter();
      }
      @Override
      public Adapter caseRemoveFileInclude(RemoveFileInclude object)
      {
        return createRemoveFileIncludeAdapter();
      }
      @Override
      public Adapter caseModifyFunction(ModifyFunction object)
      {
        return createModifyFunctionAdapter();
      }
      @Override
      public Adapter defaultCase(EObject object)
      {
        return createEObjectAdapter();
      }
    };

  /**
   * Creates an adapter for the <code>target</code>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param target the object to adapt.
   * @return the adapter for the <code>target</code>.
   * @generated
   */
  @Override
  public Adapter createAdapter(Notifier target)
  {
    return modelSwitch.doSwitch((EObject)target);
  }


  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.codebase.codeBase.DeltaModule <em>Delta Module</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.codebase.codeBase.DeltaModule
   * @generated
   */
  public Adapter createDeltaModuleAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.codebase.codeBase.FileOperation <em>File Operation</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.codebase.codeBase.FileOperation
   * @generated
   */
  public Adapter createFileOperationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.codebase.codeBase.AddSource <em>Add Source</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.codebase.codeBase.AddSource
   * @generated
   */
  public Adapter createAddSourceAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.codebase.codeBase.RemoveSource <em>Remove Source</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.codebase.codeBase.RemoveSource
   * @generated
   */
  public Adapter createRemoveSourceAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.codebase.codeBase.SourceOperation <em>Source Operation</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.codebase.codeBase.SourceOperation
   * @generated
   */
  public Adapter createSourceOperationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.codebase.codeBase.Adds <em>Adds</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.codebase.codeBase.Adds
   * @generated
   */
  public Adapter createAddsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.codebase.codeBase.Removes <em>Removes</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.codebase.codeBase.Removes
   * @generated
   */
  public Adapter createRemovesAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.codebase.codeBase.Modifies <em>Modifies</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.codebase.codeBase.Modifies
   * @generated
   */
  public Adapter createModifiesAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.codebase.codeBase.AddDeclaration <em>Add Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.codebase.codeBase.AddDeclaration
   * @generated
   */
  public Adapter createAddDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.codebase.codeBase.AddStandardInclude <em>Add Standard Include</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.codebase.codeBase.AddStandardInclude
   * @generated
   */
  public Adapter createAddStandardIncludeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.codebase.codeBase.AddFileInclude <em>Add File Include</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.codebase.codeBase.AddFileInclude
   * @generated
   */
  public Adapter createAddFileIncludeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.codebase.codeBase.RemoveGlobal <em>Remove Global</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.codebase.codeBase.RemoveGlobal
   * @generated
   */
  public Adapter createRemoveGlobalAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.codebase.codeBase.RemoveStandardInclude <em>Remove Standard Include</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.codebase.codeBase.RemoveStandardInclude
   * @generated
   */
  public Adapter createRemoveStandardIncludeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.codebase.codeBase.RemoveFileInclude <em>Remove File Include</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.codebase.codeBase.RemoveFileInclude
   * @generated
   */
  public Adapter createRemoveFileIncludeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.codebase.codeBase.ModifyFunction <em>Modify Function</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.codebase.codeBase.ModifyFunction
   * @generated
   */
  public Adapter createModifyFunctionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for the default case.
   * <!-- begin-user-doc -->
   * This default implementation returns null.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @generated
   */
  public Adapter createEObjectAdapter()
  {
    return null;
  }

} //CodeBaseAdapterFactory
