/**
 */
package org.xtext.dop.clang.clang;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Base Type Complex</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getBaseTypeComplex()
 * @model
 * @generated
 */
public interface BaseTypeComplex extends BaseType
{
} // BaseTypeComplex
