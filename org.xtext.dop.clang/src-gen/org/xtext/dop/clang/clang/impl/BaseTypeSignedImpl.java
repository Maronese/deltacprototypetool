/**
 */
package org.xtext.dop.clang.clang.impl;

import org.eclipse.emf.ecore.EClass;

import org.xtext.dop.clang.clang.BaseTypeSigned;
import org.xtext.dop.clang.clang.ClangPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Base Type Signed</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BaseTypeSignedImpl extends BaseTypeImpl implements BaseTypeSigned
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected BaseTypeSignedImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ClangPackage.Literals.BASE_TYPE_SIGNED;
  }

} //BaseTypeSignedImpl
