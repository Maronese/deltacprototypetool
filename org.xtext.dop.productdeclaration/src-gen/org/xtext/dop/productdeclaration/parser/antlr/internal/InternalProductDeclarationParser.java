package org.xtext.dop.productdeclaration.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.xtext.dop.productdeclaration.services.ProductDeclarationGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalProductDeclarationParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'SPL'", "'{'", "'Features'", "'='", "'}'", "'Deltas'", "'Constraints'", "'Partitions'", "'Products'", "';'", "'when'", "'('", "')'", "','", "'|'", "'&'", "'!'"
    };
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int RULE_INT=5;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalProductDeclarationParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalProductDeclarationParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalProductDeclarationParser.tokenNames; }
    public String getGrammarFileName() { return "../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g"; }



     	private ProductDeclarationGrammarAccess grammarAccess;
     	
        public InternalProductDeclarationParser(TokenStream input, ProductDeclarationGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "ProductDeclaration";	
       	}
       	
       	@Override
       	protected ProductDeclarationGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleProductDeclaration"
    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:67:1: entryRuleProductDeclaration returns [EObject current=null] : iv_ruleProductDeclaration= ruleProductDeclaration EOF ;
    public final EObject entryRuleProductDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleProductDeclaration = null;


        try {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:68:2: (iv_ruleProductDeclaration= ruleProductDeclaration EOF )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:69:2: iv_ruleProductDeclaration= ruleProductDeclaration EOF
            {
             newCompositeNode(grammarAccess.getProductDeclarationRule()); 
            pushFollow(FOLLOW_ruleProductDeclaration_in_entryRuleProductDeclaration75);
            iv_ruleProductDeclaration=ruleProductDeclaration();

            state._fsp--;

             current =iv_ruleProductDeclaration; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleProductDeclaration85); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleProductDeclaration"


    // $ANTLR start "ruleProductDeclaration"
    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:76:1: ruleProductDeclaration returns [EObject current=null] : (otherlv_0= 'SPL' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' otherlv_3= 'Features' otherlv_4= '=' otherlv_5= '{' ( (lv_features_6_0= ruleFeatures ) )? otherlv_7= '}' otherlv_8= 'Deltas' otherlv_9= '=' otherlv_10= '{' ( (lv_deltas_11_0= ruleDeltas ) )? otherlv_12= '}' otherlv_13= 'Constraints' otherlv_14= '{' ( (lv_constraints_15_0= ruleCore ) )? otherlv_16= '}' otherlv_17= 'Partitions' otherlv_18= '{' ( (lv_partitions_19_0= rulePartitions ) )? otherlv_20= '}' otherlv_21= 'Products' otherlv_22= '{' ( (lv_products_23_0= ruleProducts ) )? otherlv_24= '}' otherlv_25= '}' ) ;
    public final EObject ruleProductDeclaration() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_18=null;
        Token otherlv_20=null;
        Token otherlv_21=null;
        Token otherlv_22=null;
        Token otherlv_24=null;
        Token otherlv_25=null;
        EObject lv_features_6_0 = null;

        EObject lv_deltas_11_0 = null;

        EObject lv_constraints_15_0 = null;

        EObject lv_partitions_19_0 = null;

        EObject lv_products_23_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:79:28: ( (otherlv_0= 'SPL' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' otherlv_3= 'Features' otherlv_4= '=' otherlv_5= '{' ( (lv_features_6_0= ruleFeatures ) )? otherlv_7= '}' otherlv_8= 'Deltas' otherlv_9= '=' otherlv_10= '{' ( (lv_deltas_11_0= ruleDeltas ) )? otherlv_12= '}' otherlv_13= 'Constraints' otherlv_14= '{' ( (lv_constraints_15_0= ruleCore ) )? otherlv_16= '}' otherlv_17= 'Partitions' otherlv_18= '{' ( (lv_partitions_19_0= rulePartitions ) )? otherlv_20= '}' otherlv_21= 'Products' otherlv_22= '{' ( (lv_products_23_0= ruleProducts ) )? otherlv_24= '}' otherlv_25= '}' ) )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:80:1: (otherlv_0= 'SPL' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' otherlv_3= 'Features' otherlv_4= '=' otherlv_5= '{' ( (lv_features_6_0= ruleFeatures ) )? otherlv_7= '}' otherlv_8= 'Deltas' otherlv_9= '=' otherlv_10= '{' ( (lv_deltas_11_0= ruleDeltas ) )? otherlv_12= '}' otherlv_13= 'Constraints' otherlv_14= '{' ( (lv_constraints_15_0= ruleCore ) )? otherlv_16= '}' otherlv_17= 'Partitions' otherlv_18= '{' ( (lv_partitions_19_0= rulePartitions ) )? otherlv_20= '}' otherlv_21= 'Products' otherlv_22= '{' ( (lv_products_23_0= ruleProducts ) )? otherlv_24= '}' otherlv_25= '}' )
            {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:80:1: (otherlv_0= 'SPL' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' otherlv_3= 'Features' otherlv_4= '=' otherlv_5= '{' ( (lv_features_6_0= ruleFeatures ) )? otherlv_7= '}' otherlv_8= 'Deltas' otherlv_9= '=' otherlv_10= '{' ( (lv_deltas_11_0= ruleDeltas ) )? otherlv_12= '}' otherlv_13= 'Constraints' otherlv_14= '{' ( (lv_constraints_15_0= ruleCore ) )? otherlv_16= '}' otherlv_17= 'Partitions' otherlv_18= '{' ( (lv_partitions_19_0= rulePartitions ) )? otherlv_20= '}' otherlv_21= 'Products' otherlv_22= '{' ( (lv_products_23_0= ruleProducts ) )? otherlv_24= '}' otherlv_25= '}' )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:80:3: otherlv_0= 'SPL' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' otherlv_3= 'Features' otherlv_4= '=' otherlv_5= '{' ( (lv_features_6_0= ruleFeatures ) )? otherlv_7= '}' otherlv_8= 'Deltas' otherlv_9= '=' otherlv_10= '{' ( (lv_deltas_11_0= ruleDeltas ) )? otherlv_12= '}' otherlv_13= 'Constraints' otherlv_14= '{' ( (lv_constraints_15_0= ruleCore ) )? otherlv_16= '}' otherlv_17= 'Partitions' otherlv_18= '{' ( (lv_partitions_19_0= rulePartitions ) )? otherlv_20= '}' otherlv_21= 'Products' otherlv_22= '{' ( (lv_products_23_0= ruleProducts ) )? otherlv_24= '}' otherlv_25= '}'
            {
            otherlv_0=(Token)match(input,11,FOLLOW_11_in_ruleProductDeclaration122); 

                	newLeafNode(otherlv_0, grammarAccess.getProductDeclarationAccess().getSPLKeyword_0());
                
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:84:1: ( (lv_name_1_0= RULE_ID ) )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:85:1: (lv_name_1_0= RULE_ID )
            {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:85:1: (lv_name_1_0= RULE_ID )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:86:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleProductDeclaration139); 

            			newLeafNode(lv_name_1_0, grammarAccess.getProductDeclarationAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getProductDeclarationRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_12_in_ruleProductDeclaration156); 

                	newLeafNode(otherlv_2, grammarAccess.getProductDeclarationAccess().getLeftCurlyBracketKeyword_2());
                
            otherlv_3=(Token)match(input,13,FOLLOW_13_in_ruleProductDeclaration168); 

                	newLeafNode(otherlv_3, grammarAccess.getProductDeclarationAccess().getFeaturesKeyword_3());
                
            otherlv_4=(Token)match(input,14,FOLLOW_14_in_ruleProductDeclaration180); 

                	newLeafNode(otherlv_4, grammarAccess.getProductDeclarationAccess().getEqualsSignKeyword_4());
                
            otherlv_5=(Token)match(input,12,FOLLOW_12_in_ruleProductDeclaration192); 

                	newLeafNode(otherlv_5, grammarAccess.getProductDeclarationAccess().getLeftCurlyBracketKeyword_5());
                
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:118:1: ( (lv_features_6_0= ruleFeatures ) )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==RULE_ID) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:119:1: (lv_features_6_0= ruleFeatures )
                    {
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:119:1: (lv_features_6_0= ruleFeatures )
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:120:3: lv_features_6_0= ruleFeatures
                    {
                     
                    	        newCompositeNode(grammarAccess.getProductDeclarationAccess().getFeaturesFeaturesParserRuleCall_6_0()); 
                    	    
                    pushFollow(FOLLOW_ruleFeatures_in_ruleProductDeclaration213);
                    lv_features_6_0=ruleFeatures();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getProductDeclarationRule());
                    	        }
                           		set(
                           			current, 
                           			"features",
                            		lv_features_6_0, 
                            		"Features");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }

            otherlv_7=(Token)match(input,15,FOLLOW_15_in_ruleProductDeclaration226); 

                	newLeafNode(otherlv_7, grammarAccess.getProductDeclarationAccess().getRightCurlyBracketKeyword_7());
                
            otherlv_8=(Token)match(input,16,FOLLOW_16_in_ruleProductDeclaration238); 

                	newLeafNode(otherlv_8, grammarAccess.getProductDeclarationAccess().getDeltasKeyword_8());
                
            otherlv_9=(Token)match(input,14,FOLLOW_14_in_ruleProductDeclaration250); 

                	newLeafNode(otherlv_9, grammarAccess.getProductDeclarationAccess().getEqualsSignKeyword_9());
                
            otherlv_10=(Token)match(input,12,FOLLOW_12_in_ruleProductDeclaration262); 

                	newLeafNode(otherlv_10, grammarAccess.getProductDeclarationAccess().getLeftCurlyBracketKeyword_10());
                
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:152:1: ( (lv_deltas_11_0= ruleDeltas ) )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==RULE_ID) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:153:1: (lv_deltas_11_0= ruleDeltas )
                    {
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:153:1: (lv_deltas_11_0= ruleDeltas )
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:154:3: lv_deltas_11_0= ruleDeltas
                    {
                     
                    	        newCompositeNode(grammarAccess.getProductDeclarationAccess().getDeltasDeltasParserRuleCall_11_0()); 
                    	    
                    pushFollow(FOLLOW_ruleDeltas_in_ruleProductDeclaration283);
                    lv_deltas_11_0=ruleDeltas();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getProductDeclarationRule());
                    	        }
                           		set(
                           			current, 
                           			"deltas",
                            		lv_deltas_11_0, 
                            		"Deltas");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }

            otherlv_12=(Token)match(input,15,FOLLOW_15_in_ruleProductDeclaration296); 

                	newLeafNode(otherlv_12, grammarAccess.getProductDeclarationAccess().getRightCurlyBracketKeyword_12());
                
            otherlv_13=(Token)match(input,17,FOLLOW_17_in_ruleProductDeclaration308); 

                	newLeafNode(otherlv_13, grammarAccess.getProductDeclarationAccess().getConstraintsKeyword_13());
                
            otherlv_14=(Token)match(input,12,FOLLOW_12_in_ruleProductDeclaration320); 

                	newLeafNode(otherlv_14, grammarAccess.getProductDeclarationAccess().getLeftCurlyBracketKeyword_14());
                
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:182:1: ( (lv_constraints_15_0= ruleCore ) )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==RULE_ID||LA3_0==22||LA3_0==27) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:183:1: (lv_constraints_15_0= ruleCore )
                    {
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:183:1: (lv_constraints_15_0= ruleCore )
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:184:3: lv_constraints_15_0= ruleCore
                    {
                     
                    	        newCompositeNode(grammarAccess.getProductDeclarationAccess().getConstraintsCoreParserRuleCall_15_0()); 
                    	    
                    pushFollow(FOLLOW_ruleCore_in_ruleProductDeclaration341);
                    lv_constraints_15_0=ruleCore();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getProductDeclarationRule());
                    	        }
                           		set(
                           			current, 
                           			"constraints",
                            		lv_constraints_15_0, 
                            		"Core");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }

            otherlv_16=(Token)match(input,15,FOLLOW_15_in_ruleProductDeclaration354); 

                	newLeafNode(otherlv_16, grammarAccess.getProductDeclarationAccess().getRightCurlyBracketKeyword_16());
                
            otherlv_17=(Token)match(input,18,FOLLOW_18_in_ruleProductDeclaration366); 

                	newLeafNode(otherlv_17, grammarAccess.getProductDeclarationAccess().getPartitionsKeyword_17());
                
            otherlv_18=(Token)match(input,12,FOLLOW_12_in_ruleProductDeclaration378); 

                	newLeafNode(otherlv_18, grammarAccess.getProductDeclarationAccess().getLeftCurlyBracketKeyword_18());
                
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:212:1: ( (lv_partitions_19_0= rulePartitions ) )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==12) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:213:1: (lv_partitions_19_0= rulePartitions )
                    {
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:213:1: (lv_partitions_19_0= rulePartitions )
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:214:3: lv_partitions_19_0= rulePartitions
                    {
                     
                    	        newCompositeNode(grammarAccess.getProductDeclarationAccess().getPartitionsPartitionsParserRuleCall_19_0()); 
                    	    
                    pushFollow(FOLLOW_rulePartitions_in_ruleProductDeclaration399);
                    lv_partitions_19_0=rulePartitions();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getProductDeclarationRule());
                    	        }
                           		set(
                           			current, 
                           			"partitions",
                            		lv_partitions_19_0, 
                            		"Partitions");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }

            otherlv_20=(Token)match(input,15,FOLLOW_15_in_ruleProductDeclaration412); 

                	newLeafNode(otherlv_20, grammarAccess.getProductDeclarationAccess().getRightCurlyBracketKeyword_20());
                
            otherlv_21=(Token)match(input,19,FOLLOW_19_in_ruleProductDeclaration424); 

                	newLeafNode(otherlv_21, grammarAccess.getProductDeclarationAccess().getProductsKeyword_21());
                
            otherlv_22=(Token)match(input,12,FOLLOW_12_in_ruleProductDeclaration436); 

                	newLeafNode(otherlv_22, grammarAccess.getProductDeclarationAccess().getLeftCurlyBracketKeyword_22());
                
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:242:1: ( (lv_products_23_0= ruleProducts ) )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==RULE_ID) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:243:1: (lv_products_23_0= ruleProducts )
                    {
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:243:1: (lv_products_23_0= ruleProducts )
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:244:3: lv_products_23_0= ruleProducts
                    {
                     
                    	        newCompositeNode(grammarAccess.getProductDeclarationAccess().getProductsProductsParserRuleCall_23_0()); 
                    	    
                    pushFollow(FOLLOW_ruleProducts_in_ruleProductDeclaration457);
                    lv_products_23_0=ruleProducts();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getProductDeclarationRule());
                    	        }
                           		set(
                           			current, 
                           			"products",
                            		lv_products_23_0, 
                            		"Products");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }

            otherlv_24=(Token)match(input,15,FOLLOW_15_in_ruleProductDeclaration470); 

                	newLeafNode(otherlv_24, grammarAccess.getProductDeclarationAccess().getRightCurlyBracketKeyword_24());
                
            otherlv_25=(Token)match(input,15,FOLLOW_15_in_ruleProductDeclaration482); 

                	newLeafNode(otherlv_25, grammarAccess.getProductDeclarationAccess().getRightCurlyBracketKeyword_25());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleProductDeclaration"


    // $ANTLR start "entryRuleFeatures"
    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:276:1: entryRuleFeatures returns [EObject current=null] : iv_ruleFeatures= ruleFeatures EOF ;
    public final EObject entryRuleFeatures() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeatures = null;


        try {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:277:2: (iv_ruleFeatures= ruleFeatures EOF )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:278:2: iv_ruleFeatures= ruleFeatures EOF
            {
             newCompositeNode(grammarAccess.getFeaturesRule()); 
            pushFollow(FOLLOW_ruleFeatures_in_entryRuleFeatures518);
            iv_ruleFeatures=ruleFeatures();

            state._fsp--;

             current =iv_ruleFeatures; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleFeatures528); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeatures"


    // $ANTLR start "ruleFeatures"
    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:285:1: ruleFeatures returns [EObject current=null] : ( (lv_list_0_0= ruleListElem ) )+ ;
    public final EObject ruleFeatures() throws RecognitionException {
        EObject current = null;

        EObject lv_list_0_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:288:28: ( ( (lv_list_0_0= ruleListElem ) )+ )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:289:1: ( (lv_list_0_0= ruleListElem ) )+
            {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:289:1: ( (lv_list_0_0= ruleListElem ) )+
            int cnt6=0;
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==RULE_ID) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:290:1: (lv_list_0_0= ruleListElem )
            	    {
            	    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:290:1: (lv_list_0_0= ruleListElem )
            	    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:291:3: lv_list_0_0= ruleListElem
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getFeaturesAccess().getListListElemParserRuleCall_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleListElem_in_ruleFeatures573);
            	    lv_list_0_0=ruleListElem();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getFeaturesRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"list",
            	            		lv_list_0_0, 
            	            		"ListElem");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt6 >= 1 ) break loop6;
                        EarlyExitException eee =
                            new EarlyExitException(6, input);
                        throw eee;
                }
                cnt6++;
            } while (true);


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeatures"


    // $ANTLR start "entryRuleDeltas"
    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:315:1: entryRuleDeltas returns [EObject current=null] : iv_ruleDeltas= ruleDeltas EOF ;
    public final EObject entryRuleDeltas() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDeltas = null;


        try {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:316:2: (iv_ruleDeltas= ruleDeltas EOF )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:317:2: iv_ruleDeltas= ruleDeltas EOF
            {
             newCompositeNode(grammarAccess.getDeltasRule()); 
            pushFollow(FOLLOW_ruleDeltas_in_entryRuleDeltas609);
            iv_ruleDeltas=ruleDeltas();

            state._fsp--;

             current =iv_ruleDeltas; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleDeltas619); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDeltas"


    // $ANTLR start "ruleDeltas"
    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:324:1: ruleDeltas returns [EObject current=null] : ( (lv_list_0_0= ruleListElem ) )+ ;
    public final EObject ruleDeltas() throws RecognitionException {
        EObject current = null;

        EObject lv_list_0_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:327:28: ( ( (lv_list_0_0= ruleListElem ) )+ )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:328:1: ( (lv_list_0_0= ruleListElem ) )+
            {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:328:1: ( (lv_list_0_0= ruleListElem ) )+
            int cnt7=0;
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==RULE_ID) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:329:1: (lv_list_0_0= ruleListElem )
            	    {
            	    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:329:1: (lv_list_0_0= ruleListElem )
            	    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:330:3: lv_list_0_0= ruleListElem
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getDeltasAccess().getListListElemParserRuleCall_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleListElem_in_ruleDeltas664);
            	    lv_list_0_0=ruleListElem();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getDeltasRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"list",
            	            		lv_list_0_0, 
            	            		"ListElem");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt7 >= 1 ) break loop7;
                        EarlyExitException eee =
                            new EarlyExitException(7, input);
                        throw eee;
                }
                cnt7++;
            } while (true);


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDeltas"


    // $ANTLR start "entryRuleProducts"
    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:354:1: entryRuleProducts returns [EObject current=null] : iv_ruleProducts= ruleProducts EOF ;
    public final EObject entryRuleProducts() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleProducts = null;


        try {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:355:2: (iv_ruleProducts= ruleProducts EOF )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:356:2: iv_ruleProducts= ruleProducts EOF
            {
             newCompositeNode(grammarAccess.getProductsRule()); 
            pushFollow(FOLLOW_ruleProducts_in_entryRuleProducts700);
            iv_ruleProducts=ruleProducts();

            state._fsp--;

             current =iv_ruleProducts; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleProducts710); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleProducts"


    // $ANTLR start "ruleProducts"
    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:363:1: ruleProducts returns [EObject current=null] : ( (lv_list_0_0= ruleProduct ) )+ ;
    public final EObject ruleProducts() throws RecognitionException {
        EObject current = null;

        EObject lv_list_0_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:366:28: ( ( (lv_list_0_0= ruleProduct ) )+ )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:367:1: ( (lv_list_0_0= ruleProduct ) )+
            {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:367:1: ( (lv_list_0_0= ruleProduct ) )+
            int cnt8=0;
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==RULE_ID) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:368:1: (lv_list_0_0= ruleProduct )
            	    {
            	    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:368:1: (lv_list_0_0= ruleProduct )
            	    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:369:3: lv_list_0_0= ruleProduct
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getProductsAccess().getListProductParserRuleCall_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleProduct_in_ruleProducts755);
            	    lv_list_0_0=ruleProduct();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getProductsRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"list",
            	            		lv_list_0_0, 
            	            		"Product");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt8 >= 1 ) break loop8;
                        EarlyExitException eee =
                            new EarlyExitException(8, input);
                        throw eee;
                }
                cnt8++;
            } while (true);


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleProducts"


    // $ANTLR start "entryRuleProduct"
    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:393:1: entryRuleProduct returns [EObject current=null] : iv_ruleProduct= ruleProduct EOF ;
    public final EObject entryRuleProduct() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleProduct = null;


        try {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:394:2: (iv_ruleProduct= ruleProduct EOF )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:395:2: iv_ruleProduct= ruleProduct EOF
            {
             newCompositeNode(grammarAccess.getProductRule()); 
            pushFollow(FOLLOW_ruleProduct_in_entryRuleProduct791);
            iv_ruleProduct=ruleProduct();

            state._fsp--;

             current =iv_ruleProduct; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleProduct801); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleProduct"


    // $ANTLR start "ruleProduct"
    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:402:1: ruleProduct returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '=' otherlv_2= '{' ( (lv_list_3_0= ruleListElem ) )* otherlv_4= '}' otherlv_5= ';' ) ;
    public final EObject ruleProduct() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        EObject lv_list_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:405:28: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '=' otherlv_2= '{' ( (lv_list_3_0= ruleListElem ) )* otherlv_4= '}' otherlv_5= ';' ) )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:406:1: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '=' otherlv_2= '{' ( (lv_list_3_0= ruleListElem ) )* otherlv_4= '}' otherlv_5= ';' )
            {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:406:1: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '=' otherlv_2= '{' ( (lv_list_3_0= ruleListElem ) )* otherlv_4= '}' otherlv_5= ';' )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:406:2: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '=' otherlv_2= '{' ( (lv_list_3_0= ruleListElem ) )* otherlv_4= '}' otherlv_5= ';'
            {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:406:2: ( (lv_name_0_0= RULE_ID ) )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:407:1: (lv_name_0_0= RULE_ID )
            {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:407:1: (lv_name_0_0= RULE_ID )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:408:3: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleProduct843); 

            			newLeafNode(lv_name_0_0, grammarAccess.getProductAccess().getNameIDTerminalRuleCall_0_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getProductRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_0_0, 
                    		"ID");
            	    

            }


            }

            otherlv_1=(Token)match(input,14,FOLLOW_14_in_ruleProduct860); 

                	newLeafNode(otherlv_1, grammarAccess.getProductAccess().getEqualsSignKeyword_1());
                
            otherlv_2=(Token)match(input,12,FOLLOW_12_in_ruleProduct872); 

                	newLeafNode(otherlv_2, grammarAccess.getProductAccess().getLeftCurlyBracketKeyword_2());
                
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:432:1: ( (lv_list_3_0= ruleListElem ) )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==RULE_ID) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:433:1: (lv_list_3_0= ruleListElem )
            	    {
            	    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:433:1: (lv_list_3_0= ruleListElem )
            	    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:434:3: lv_list_3_0= ruleListElem
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getProductAccess().getListListElemParserRuleCall_3_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleListElem_in_ruleProduct893);
            	    lv_list_3_0=ruleListElem();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getProductRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"list",
            	            		lv_list_3_0, 
            	            		"ListElem");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

            otherlv_4=(Token)match(input,15,FOLLOW_15_in_ruleProduct906); 

                	newLeafNode(otherlv_4, grammarAccess.getProductAccess().getRightCurlyBracketKeyword_4());
                
            otherlv_5=(Token)match(input,20,FOLLOW_20_in_ruleProduct918); 

                	newLeafNode(otherlv_5, grammarAccess.getProductAccess().getSemicolonKeyword_5());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleProduct"


    // $ANTLR start "entryRulePartitions"
    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:466:1: entryRulePartitions returns [EObject current=null] : iv_rulePartitions= rulePartitions EOF ;
    public final EObject entryRulePartitions() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePartitions = null;


        try {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:467:2: (iv_rulePartitions= rulePartitions EOF )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:468:2: iv_rulePartitions= rulePartitions EOF
            {
             newCompositeNode(grammarAccess.getPartitionsRule()); 
            pushFollow(FOLLOW_rulePartitions_in_entryRulePartitions954);
            iv_rulePartitions=rulePartitions();

            state._fsp--;

             current =iv_rulePartitions; 
            match(input,EOF,FOLLOW_EOF_in_entryRulePartitions964); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePartitions"


    // $ANTLR start "rulePartitions"
    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:475:1: rulePartitions returns [EObject current=null] : ( (lv_list_0_0= rulePartition ) )+ ;
    public final EObject rulePartitions() throws RecognitionException {
        EObject current = null;

        EObject lv_list_0_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:478:28: ( ( (lv_list_0_0= rulePartition ) )+ )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:479:1: ( (lv_list_0_0= rulePartition ) )+
            {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:479:1: ( (lv_list_0_0= rulePartition ) )+
            int cnt10=0;
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==12) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:480:1: (lv_list_0_0= rulePartition )
            	    {
            	    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:480:1: (lv_list_0_0= rulePartition )
            	    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:481:3: lv_list_0_0= rulePartition
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getPartitionsAccess().getListPartitionParserRuleCall_0()); 
            	    	    
            	    pushFollow(FOLLOW_rulePartition_in_rulePartitions1009);
            	    lv_list_0_0=rulePartition();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getPartitionsRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"list",
            	            		lv_list_0_0, 
            	            		"Partition");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt10 >= 1 ) break loop10;
                        EarlyExitException eee =
                            new EarlyExitException(10, input);
                        throw eee;
                }
                cnt10++;
            } while (true);


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePartitions"


    // $ANTLR start "entryRulePartition"
    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:505:1: entryRulePartition returns [EObject current=null] : iv_rulePartition= rulePartition EOF ;
    public final EObject entryRulePartition() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePartition = null;


        try {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:506:2: (iv_rulePartition= rulePartition EOF )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:507:2: iv_rulePartition= rulePartition EOF
            {
             newCompositeNode(grammarAccess.getPartitionRule()); 
            pushFollow(FOLLOW_rulePartition_in_entryRulePartition1045);
            iv_rulePartition=rulePartition();

            state._fsp--;

             current =iv_rulePartition; 
            match(input,EOF,FOLLOW_EOF_in_entryRulePartition1055); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePartition"


    // $ANTLR start "rulePartition"
    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:514:1: rulePartition returns [EObject current=null] : ( ( (lv_constr_0_0= ruleConstraint ) )+ otherlv_1= ';' ) ;
    public final EObject rulePartition() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_constr_0_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:517:28: ( ( ( (lv_constr_0_0= ruleConstraint ) )+ otherlv_1= ';' ) )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:518:1: ( ( (lv_constr_0_0= ruleConstraint ) )+ otherlv_1= ';' )
            {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:518:1: ( ( (lv_constr_0_0= ruleConstraint ) )+ otherlv_1= ';' )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:518:2: ( (lv_constr_0_0= ruleConstraint ) )+ otherlv_1= ';'
            {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:518:2: ( (lv_constr_0_0= ruleConstraint ) )+
            int cnt11=0;
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==12) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:519:1: (lv_constr_0_0= ruleConstraint )
            	    {
            	    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:519:1: (lv_constr_0_0= ruleConstraint )
            	    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:520:3: lv_constr_0_0= ruleConstraint
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getPartitionAccess().getConstrConstraintParserRuleCall_0_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleConstraint_in_rulePartition1101);
            	    lv_constr_0_0=ruleConstraint();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getPartitionRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"constr",
            	            		lv_constr_0_0, 
            	            		"Constraint");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt11 >= 1 ) break loop11;
                        EarlyExitException eee =
                            new EarlyExitException(11, input);
                        throw eee;
                }
                cnt11++;
            } while (true);

            otherlv_1=(Token)match(input,20,FOLLOW_20_in_rulePartition1114); 

                	newLeafNode(otherlv_1, grammarAccess.getPartitionAccess().getSemicolonKeyword_1());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePartition"


    // $ANTLR start "entryRuleConstraint"
    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:548:1: entryRuleConstraint returns [EObject current=null] : iv_ruleConstraint= ruleConstraint EOF ;
    public final EObject entryRuleConstraint() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConstraint = null;


        try {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:549:2: (iv_ruleConstraint= ruleConstraint EOF )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:550:2: iv_ruleConstraint= ruleConstraint EOF
            {
             newCompositeNode(grammarAccess.getConstraintRule()); 
            pushFollow(FOLLOW_ruleConstraint_in_entryRuleConstraint1150);
            iv_ruleConstraint=ruleConstraint();

            state._fsp--;

             current =iv_ruleConstraint; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleConstraint1160); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConstraint"


    // $ANTLR start "ruleConstraint"
    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:557:1: ruleConstraint returns [EObject current=null] : (otherlv_0= '{' ( (lv_delta_1_0= ruleElement ) ) otherlv_2= '}' otherlv_3= 'when' otherlv_4= '(' ( (lv_expr_5_0= ruleExpression ) ) otherlv_6= ')' ( (lv_last_7_0= ',' ) )? ) ;
    public final EObject ruleConstraint() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token lv_last_7_0=null;
        EObject lv_delta_1_0 = null;

        EObject lv_expr_5_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:560:28: ( (otherlv_0= '{' ( (lv_delta_1_0= ruleElement ) ) otherlv_2= '}' otherlv_3= 'when' otherlv_4= '(' ( (lv_expr_5_0= ruleExpression ) ) otherlv_6= ')' ( (lv_last_7_0= ',' ) )? ) )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:561:1: (otherlv_0= '{' ( (lv_delta_1_0= ruleElement ) ) otherlv_2= '}' otherlv_3= 'when' otherlv_4= '(' ( (lv_expr_5_0= ruleExpression ) ) otherlv_6= ')' ( (lv_last_7_0= ',' ) )? )
            {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:561:1: (otherlv_0= '{' ( (lv_delta_1_0= ruleElement ) ) otherlv_2= '}' otherlv_3= 'when' otherlv_4= '(' ( (lv_expr_5_0= ruleExpression ) ) otherlv_6= ')' ( (lv_last_7_0= ',' ) )? )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:561:3: otherlv_0= '{' ( (lv_delta_1_0= ruleElement ) ) otherlv_2= '}' otherlv_3= 'when' otherlv_4= '(' ( (lv_expr_5_0= ruleExpression ) ) otherlv_6= ')' ( (lv_last_7_0= ',' ) )?
            {
            otherlv_0=(Token)match(input,12,FOLLOW_12_in_ruleConstraint1197); 

                	newLeafNode(otherlv_0, grammarAccess.getConstraintAccess().getLeftCurlyBracketKeyword_0());
                
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:565:1: ( (lv_delta_1_0= ruleElement ) )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:566:1: (lv_delta_1_0= ruleElement )
            {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:566:1: (lv_delta_1_0= ruleElement )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:567:3: lv_delta_1_0= ruleElement
            {
             
            	        newCompositeNode(grammarAccess.getConstraintAccess().getDeltaElementParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleElement_in_ruleConstraint1218);
            lv_delta_1_0=ruleElement();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getConstraintRule());
            	        }
                   		set(
                   			current, 
                   			"delta",
                    		lv_delta_1_0, 
                    		"Element");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_2=(Token)match(input,15,FOLLOW_15_in_ruleConstraint1230); 

                	newLeafNode(otherlv_2, grammarAccess.getConstraintAccess().getRightCurlyBracketKeyword_2());
                
            otherlv_3=(Token)match(input,21,FOLLOW_21_in_ruleConstraint1242); 

                	newLeafNode(otherlv_3, grammarAccess.getConstraintAccess().getWhenKeyword_3());
                
            otherlv_4=(Token)match(input,22,FOLLOW_22_in_ruleConstraint1254); 

                	newLeafNode(otherlv_4, grammarAccess.getConstraintAccess().getLeftParenthesisKeyword_4());
                
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:595:1: ( (lv_expr_5_0= ruleExpression ) )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:596:1: (lv_expr_5_0= ruleExpression )
            {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:596:1: (lv_expr_5_0= ruleExpression )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:597:3: lv_expr_5_0= ruleExpression
            {
             
            	        newCompositeNode(grammarAccess.getConstraintAccess().getExprExpressionParserRuleCall_5_0()); 
            	    
            pushFollow(FOLLOW_ruleExpression_in_ruleConstraint1275);
            lv_expr_5_0=ruleExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getConstraintRule());
            	        }
                   		set(
                   			current, 
                   			"expr",
                    		lv_expr_5_0, 
                    		"Expression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_6=(Token)match(input,23,FOLLOW_23_in_ruleConstraint1287); 

                	newLeafNode(otherlv_6, grammarAccess.getConstraintAccess().getRightParenthesisKeyword_6());
                
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:617:1: ( (lv_last_7_0= ',' ) )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==24) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:618:1: (lv_last_7_0= ',' )
                    {
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:618:1: (lv_last_7_0= ',' )
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:619:3: lv_last_7_0= ','
                    {
                    lv_last_7_0=(Token)match(input,24,FOLLOW_24_in_ruleConstraint1305); 

                            newLeafNode(lv_last_7_0, grammarAccess.getConstraintAccess().getLastCommaKeyword_7_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getConstraintRule());
                    	        }
                           		setWithLastConsumed(current, "last", true, ",");
                    	    

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConstraint"


    // $ANTLR start "entryRuleListElem"
    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:640:1: entryRuleListElem returns [EObject current=null] : iv_ruleListElem= ruleListElem EOF ;
    public final EObject entryRuleListElem() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleListElem = null;


        try {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:641:2: (iv_ruleListElem= ruleListElem EOF )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:642:2: iv_ruleListElem= ruleListElem EOF
            {
             newCompositeNode(grammarAccess.getListElemRule()); 
            pushFollow(FOLLOW_ruleListElem_in_entryRuleListElem1355);
            iv_ruleListElem=ruleListElem();

            state._fsp--;

             current =iv_ruleListElem; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleListElem1365); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleListElem"


    // $ANTLR start "ruleListElem"
    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:649:1: ruleListElem returns [EObject current=null] : ( ( (lv_elem_0_0= ruleElement ) ) ( (lv_last_1_0= ',' ) )? ) ;
    public final EObject ruleListElem() throws RecognitionException {
        EObject current = null;

        Token lv_last_1_0=null;
        EObject lv_elem_0_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:652:28: ( ( ( (lv_elem_0_0= ruleElement ) ) ( (lv_last_1_0= ',' ) )? ) )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:653:1: ( ( (lv_elem_0_0= ruleElement ) ) ( (lv_last_1_0= ',' ) )? )
            {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:653:1: ( ( (lv_elem_0_0= ruleElement ) ) ( (lv_last_1_0= ',' ) )? )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:653:2: ( (lv_elem_0_0= ruleElement ) ) ( (lv_last_1_0= ',' ) )?
            {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:653:2: ( (lv_elem_0_0= ruleElement ) )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:654:1: (lv_elem_0_0= ruleElement )
            {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:654:1: (lv_elem_0_0= ruleElement )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:655:3: lv_elem_0_0= ruleElement
            {
             
            	        newCompositeNode(grammarAccess.getListElemAccess().getElemElementParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_ruleElement_in_ruleListElem1411);
            lv_elem_0_0=ruleElement();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getListElemRule());
            	        }
                   		set(
                   			current, 
                   			"elem",
                    		lv_elem_0_0, 
                    		"Element");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:671:2: ( (lv_last_1_0= ',' ) )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==24) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:672:1: (lv_last_1_0= ',' )
                    {
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:672:1: (lv_last_1_0= ',' )
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:673:3: lv_last_1_0= ','
                    {
                    lv_last_1_0=(Token)match(input,24,FOLLOW_24_in_ruleListElem1429); 

                            newLeafNode(lv_last_1_0, grammarAccess.getListElemAccess().getLastCommaKeyword_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getListElemRule());
                    	        }
                           		setWithLastConsumed(current, "last", true, ",");
                    	    

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleListElem"


    // $ANTLR start "entryRuleElement"
    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:694:1: entryRuleElement returns [EObject current=null] : iv_ruleElement= ruleElement EOF ;
    public final EObject entryRuleElement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleElement = null;


        try {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:695:2: (iv_ruleElement= ruleElement EOF )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:696:2: iv_ruleElement= ruleElement EOF
            {
             newCompositeNode(grammarAccess.getElementRule()); 
            pushFollow(FOLLOW_ruleElement_in_entryRuleElement1479);
            iv_ruleElement=ruleElement();

            state._fsp--;

             current =iv_ruleElement; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleElement1489); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleElement"


    // $ANTLR start "ruleElement"
    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:703:1: ruleElement returns [EObject current=null] : ( (lv_name_0_0= RULE_ID ) ) ;
    public final EObject ruleElement() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;

         enterRule(); 
            
        try {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:706:28: ( ( (lv_name_0_0= RULE_ID ) ) )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:707:1: ( (lv_name_0_0= RULE_ID ) )
            {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:707:1: ( (lv_name_0_0= RULE_ID ) )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:708:1: (lv_name_0_0= RULE_ID )
            {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:708:1: (lv_name_0_0= RULE_ID )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:709:3: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleElement1530); 

            			newLeafNode(lv_name_0_0, grammarAccess.getElementAccess().getNameIDTerminalRuleCall_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getElementRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_0_0, 
                    		"ID");
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleElement"


    // $ANTLR start "entryRuleCore"
    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:733:1: entryRuleCore returns [EObject current=null] : iv_ruleCore= ruleCore EOF ;
    public final EObject entryRuleCore() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCore = null;


        try {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:734:2: (iv_ruleCore= ruleCore EOF )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:735:2: iv_ruleCore= ruleCore EOF
            {
             newCompositeNode(grammarAccess.getCoreRule()); 
            pushFollow(FOLLOW_ruleCore_in_entryRuleCore1570);
            iv_ruleCore=ruleCore();

            state._fsp--;

             current =iv_ruleCore; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleCore1580); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCore"


    // $ANTLR start "ruleCore"
    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:742:1: ruleCore returns [EObject current=null] : ( ( (lv_expr_0_0= ruleExpression ) ) otherlv_1= ';' ) ;
    public final EObject ruleCore() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_expr_0_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:745:28: ( ( ( (lv_expr_0_0= ruleExpression ) ) otherlv_1= ';' ) )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:746:1: ( ( (lv_expr_0_0= ruleExpression ) ) otherlv_1= ';' )
            {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:746:1: ( ( (lv_expr_0_0= ruleExpression ) ) otherlv_1= ';' )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:746:2: ( (lv_expr_0_0= ruleExpression ) ) otherlv_1= ';'
            {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:746:2: ( (lv_expr_0_0= ruleExpression ) )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:747:1: (lv_expr_0_0= ruleExpression )
            {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:747:1: (lv_expr_0_0= ruleExpression )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:748:3: lv_expr_0_0= ruleExpression
            {
             
            	        newCompositeNode(grammarAccess.getCoreAccess().getExprExpressionParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_ruleExpression_in_ruleCore1626);
            lv_expr_0_0=ruleExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getCoreRule());
            	        }
                   		set(
                   			current, 
                   			"expr",
                    		lv_expr_0_0, 
                    		"Expression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_1=(Token)match(input,20,FOLLOW_20_in_ruleCore1638); 

                	newLeafNode(otherlv_1, grammarAccess.getCoreAccess().getSemicolonKeyword_1());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCore"


    // $ANTLR start "entryRuleExpression"
    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:776:1: entryRuleExpression returns [EObject current=null] : iv_ruleExpression= ruleExpression EOF ;
    public final EObject entryRuleExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpression = null;


        try {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:777:2: (iv_ruleExpression= ruleExpression EOF )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:778:2: iv_ruleExpression= ruleExpression EOF
            {
             newCompositeNode(grammarAccess.getExpressionRule()); 
            pushFollow(FOLLOW_ruleExpression_in_entryRuleExpression1674);
            iv_ruleExpression=ruleExpression();

            state._fsp--;

             current =iv_ruleExpression; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleExpression1684); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpression"


    // $ANTLR start "ruleExpression"
    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:785:1: ruleExpression returns [EObject current=null] : this_Or_0= ruleOr ;
    public final EObject ruleExpression() throws RecognitionException {
        EObject current = null;

        EObject this_Or_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:788:28: (this_Or_0= ruleOr )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:790:5: this_Or_0= ruleOr
            {
             
                    newCompositeNode(grammarAccess.getExpressionAccess().getOrParserRuleCall()); 
                
            pushFollow(FOLLOW_ruleOr_in_ruleExpression1730);
            this_Or_0=ruleOr();

            state._fsp--;

             
                    current = this_Or_0; 
                    afterParserOrEnumRuleCall();
                

            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpression"


    // $ANTLR start "entryRuleOr"
    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:806:1: entryRuleOr returns [EObject current=null] : iv_ruleOr= ruleOr EOF ;
    public final EObject entryRuleOr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOr = null;


        try {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:807:2: (iv_ruleOr= ruleOr EOF )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:808:2: iv_ruleOr= ruleOr EOF
            {
             newCompositeNode(grammarAccess.getOrRule()); 
            pushFollow(FOLLOW_ruleOr_in_entryRuleOr1764);
            iv_ruleOr=ruleOr();

            state._fsp--;

             current =iv_ruleOr; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleOr1774); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOr"


    // $ANTLR start "ruleOr"
    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:815:1: ruleOr returns [EObject current=null] : (this_And_0= ruleAnd ( () otherlv_2= '|' ( (lv_right_3_0= ruleOr ) ) )? ) ;
    public final EObject ruleOr() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_And_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:818:28: ( (this_And_0= ruleAnd ( () otherlv_2= '|' ( (lv_right_3_0= ruleOr ) ) )? ) )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:819:1: (this_And_0= ruleAnd ( () otherlv_2= '|' ( (lv_right_3_0= ruleOr ) ) )? )
            {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:819:1: (this_And_0= ruleAnd ( () otherlv_2= '|' ( (lv_right_3_0= ruleOr ) ) )? )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:820:5: this_And_0= ruleAnd ( () otherlv_2= '|' ( (lv_right_3_0= ruleOr ) ) )?
            {
             
                    newCompositeNode(grammarAccess.getOrAccess().getAndParserRuleCall_0()); 
                
            pushFollow(FOLLOW_ruleAnd_in_ruleOr1821);
            this_And_0=ruleAnd();

            state._fsp--;

             
                    current = this_And_0; 
                    afterParserOrEnumRuleCall();
                
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:828:1: ( () otherlv_2= '|' ( (lv_right_3_0= ruleOr ) ) )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==25) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:828:2: () otherlv_2= '|' ( (lv_right_3_0= ruleOr ) )
                    {
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:828:2: ()
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:829:5: 
                    {

                            current = forceCreateModelElementAndSet(
                                grammarAccess.getOrAccess().getOrLeftAction_1_0(),
                                current);
                        

                    }

                    otherlv_2=(Token)match(input,25,FOLLOW_25_in_ruleOr1842); 

                        	newLeafNode(otherlv_2, grammarAccess.getOrAccess().getVerticalLineKeyword_1_1());
                        
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:838:1: ( (lv_right_3_0= ruleOr ) )
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:839:1: (lv_right_3_0= ruleOr )
                    {
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:839:1: (lv_right_3_0= ruleOr )
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:840:3: lv_right_3_0= ruleOr
                    {
                     
                    	        newCompositeNode(grammarAccess.getOrAccess().getRightOrParserRuleCall_1_2_0()); 
                    	    
                    pushFollow(FOLLOW_ruleOr_in_ruleOr1863);
                    lv_right_3_0=ruleOr();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getOrRule());
                    	        }
                           		set(
                           			current, 
                           			"right",
                            		lv_right_3_0, 
                            		"Or");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOr"


    // $ANTLR start "entryRuleAnd"
    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:864:1: entryRuleAnd returns [EObject current=null] : iv_ruleAnd= ruleAnd EOF ;
    public final EObject entryRuleAnd() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAnd = null;


        try {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:865:2: (iv_ruleAnd= ruleAnd EOF )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:866:2: iv_ruleAnd= ruleAnd EOF
            {
             newCompositeNode(grammarAccess.getAndRule()); 
            pushFollow(FOLLOW_ruleAnd_in_entryRuleAnd1901);
            iv_ruleAnd=ruleAnd();

            state._fsp--;

             current =iv_ruleAnd; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleAnd1911); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAnd"


    // $ANTLR start "ruleAnd"
    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:873:1: ruleAnd returns [EObject current=null] : (this_Primary_0= rulePrimary ( () otherlv_2= '&' ( (lv_right_3_0= ruleAnd ) ) )? ) ;
    public final EObject ruleAnd() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_Primary_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:876:28: ( (this_Primary_0= rulePrimary ( () otherlv_2= '&' ( (lv_right_3_0= ruleAnd ) ) )? ) )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:877:1: (this_Primary_0= rulePrimary ( () otherlv_2= '&' ( (lv_right_3_0= ruleAnd ) ) )? )
            {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:877:1: (this_Primary_0= rulePrimary ( () otherlv_2= '&' ( (lv_right_3_0= ruleAnd ) ) )? )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:878:5: this_Primary_0= rulePrimary ( () otherlv_2= '&' ( (lv_right_3_0= ruleAnd ) ) )?
            {
             
                    newCompositeNode(grammarAccess.getAndAccess().getPrimaryParserRuleCall_0()); 
                
            pushFollow(FOLLOW_rulePrimary_in_ruleAnd1958);
            this_Primary_0=rulePrimary();

            state._fsp--;

             
                    current = this_Primary_0; 
                    afterParserOrEnumRuleCall();
                
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:886:1: ( () otherlv_2= '&' ( (lv_right_3_0= ruleAnd ) ) )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==26) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:886:2: () otherlv_2= '&' ( (lv_right_3_0= ruleAnd ) )
                    {
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:886:2: ()
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:887:5: 
                    {

                            current = forceCreateModelElementAndSet(
                                grammarAccess.getAndAccess().getAndLeftAction_1_0(),
                                current);
                        

                    }

                    otherlv_2=(Token)match(input,26,FOLLOW_26_in_ruleAnd1979); 

                        	newLeafNode(otherlv_2, grammarAccess.getAndAccess().getAmpersandKeyword_1_1());
                        
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:896:1: ( (lv_right_3_0= ruleAnd ) )
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:897:1: (lv_right_3_0= ruleAnd )
                    {
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:897:1: (lv_right_3_0= ruleAnd )
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:898:3: lv_right_3_0= ruleAnd
                    {
                     
                    	        newCompositeNode(grammarAccess.getAndAccess().getRightAndParserRuleCall_1_2_0()); 
                    	    
                    pushFollow(FOLLOW_ruleAnd_in_ruleAnd2000);
                    lv_right_3_0=ruleAnd();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getAndRule());
                    	        }
                           		set(
                           			current, 
                           			"right",
                            		lv_right_3_0, 
                            		"And");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAnd"


    // $ANTLR start "entryRulePrimary"
    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:922:1: entryRulePrimary returns [EObject current=null] : iv_rulePrimary= rulePrimary EOF ;
    public final EObject entryRulePrimary() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePrimary = null;


        try {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:923:2: (iv_rulePrimary= rulePrimary EOF )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:924:2: iv_rulePrimary= rulePrimary EOF
            {
             newCompositeNode(grammarAccess.getPrimaryRule()); 
            pushFollow(FOLLOW_rulePrimary_in_entryRulePrimary2038);
            iv_rulePrimary=rulePrimary();

            state._fsp--;

             current =iv_rulePrimary; 
            match(input,EOF,FOLLOW_EOF_in_entryRulePrimary2048); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePrimary"


    // $ANTLR start "rulePrimary"
    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:931:1: rulePrimary returns [EObject current=null] : (this_Atomic_0= ruleAtomic | ( () otherlv_2= '(' ( (lv_expr_3_0= ruleExpression ) ) otherlv_4= ')' ) | ( () otherlv_6= '!' ( (lv_value_7_0= rulePrimary ) ) ) ) ;
    public final EObject rulePrimary() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject this_Atomic_0 = null;

        EObject lv_expr_3_0 = null;

        EObject lv_value_7_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:934:28: ( (this_Atomic_0= ruleAtomic | ( () otherlv_2= '(' ( (lv_expr_3_0= ruleExpression ) ) otherlv_4= ')' ) | ( () otherlv_6= '!' ( (lv_value_7_0= rulePrimary ) ) ) ) )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:935:1: (this_Atomic_0= ruleAtomic | ( () otherlv_2= '(' ( (lv_expr_3_0= ruleExpression ) ) otherlv_4= ')' ) | ( () otherlv_6= '!' ( (lv_value_7_0= rulePrimary ) ) ) )
            {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:935:1: (this_Atomic_0= ruleAtomic | ( () otherlv_2= '(' ( (lv_expr_3_0= ruleExpression ) ) otherlv_4= ')' ) | ( () otherlv_6= '!' ( (lv_value_7_0= rulePrimary ) ) ) )
            int alt16=3;
            switch ( input.LA(1) ) {
            case RULE_ID:
                {
                alt16=1;
                }
                break;
            case 22:
                {
                alt16=2;
                }
                break;
            case 27:
                {
                alt16=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 16, 0, input);

                throw nvae;
            }

            switch (alt16) {
                case 1 :
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:936:5: this_Atomic_0= ruleAtomic
                    {
                     
                            newCompositeNode(grammarAccess.getPrimaryAccess().getAtomicParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleAtomic_in_rulePrimary2095);
                    this_Atomic_0=ruleAtomic();

                    state._fsp--;

                     
                            current = this_Atomic_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:945:6: ( () otherlv_2= '(' ( (lv_expr_3_0= ruleExpression ) ) otherlv_4= ')' )
                    {
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:945:6: ( () otherlv_2= '(' ( (lv_expr_3_0= ruleExpression ) ) otherlv_4= ')' )
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:945:7: () otherlv_2= '(' ( (lv_expr_3_0= ruleExpression ) ) otherlv_4= ')'
                    {
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:945:7: ()
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:946:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getPrimaryAccess().getSubAction_1_0(),
                                current);
                        

                    }

                    otherlv_2=(Token)match(input,22,FOLLOW_22_in_rulePrimary2122); 

                        	newLeafNode(otherlv_2, grammarAccess.getPrimaryAccess().getLeftParenthesisKeyword_1_1());
                        
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:955:1: ( (lv_expr_3_0= ruleExpression ) )
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:956:1: (lv_expr_3_0= ruleExpression )
                    {
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:956:1: (lv_expr_3_0= ruleExpression )
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:957:3: lv_expr_3_0= ruleExpression
                    {
                     
                    	        newCompositeNode(grammarAccess.getPrimaryAccess().getExprExpressionParserRuleCall_1_2_0()); 
                    	    
                    pushFollow(FOLLOW_ruleExpression_in_rulePrimary2143);
                    lv_expr_3_0=ruleExpression();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getPrimaryRule());
                    	        }
                           		set(
                           			current, 
                           			"expr",
                            		lv_expr_3_0, 
                            		"Expression");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_4=(Token)match(input,23,FOLLOW_23_in_rulePrimary2155); 

                        	newLeafNode(otherlv_4, grammarAccess.getPrimaryAccess().getRightParenthesisKeyword_1_3());
                        

                    }


                    }
                    break;
                case 3 :
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:978:6: ( () otherlv_6= '!' ( (lv_value_7_0= rulePrimary ) ) )
                    {
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:978:6: ( () otherlv_6= '!' ( (lv_value_7_0= rulePrimary ) ) )
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:978:7: () otherlv_6= '!' ( (lv_value_7_0= rulePrimary ) )
                    {
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:978:7: ()
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:979:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getPrimaryAccess().getNotAction_2_0(),
                                current);
                        

                    }

                    otherlv_6=(Token)match(input,27,FOLLOW_27_in_rulePrimary2184); 

                        	newLeafNode(otherlv_6, grammarAccess.getPrimaryAccess().getExclamationMarkKeyword_2_1());
                        
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:988:1: ( (lv_value_7_0= rulePrimary ) )
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:989:1: (lv_value_7_0= rulePrimary )
                    {
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:989:1: (lv_value_7_0= rulePrimary )
                    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:990:3: lv_value_7_0= rulePrimary
                    {
                     
                    	        newCompositeNode(grammarAccess.getPrimaryAccess().getValuePrimaryParserRuleCall_2_2_0()); 
                    	    
                    pushFollow(FOLLOW_rulePrimary_in_rulePrimary2205);
                    lv_value_7_0=rulePrimary();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getPrimaryRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_7_0, 
                            		"Primary");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePrimary"


    // $ANTLR start "entryRuleAtomic"
    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:1014:1: entryRuleAtomic returns [EObject current=null] : iv_ruleAtomic= ruleAtomic EOF ;
    public final EObject entryRuleAtomic() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAtomic = null;


        try {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:1015:2: (iv_ruleAtomic= ruleAtomic EOF )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:1016:2: iv_ruleAtomic= ruleAtomic EOF
            {
             newCompositeNode(grammarAccess.getAtomicRule()); 
            pushFollow(FOLLOW_ruleAtomic_in_entryRuleAtomic2242);
            iv_ruleAtomic=ruleAtomic();

            state._fsp--;

             current =iv_ruleAtomic; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleAtomic2252); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAtomic"


    // $ANTLR start "ruleAtomic"
    // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:1023:1: ruleAtomic returns [EObject current=null] : this_Element_0= ruleElement ;
    public final EObject ruleAtomic() throws RecognitionException {
        EObject current = null;

        EObject this_Element_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:1026:28: (this_Element_0= ruleElement )
            // ../org.xtext.dop.productdeclaration/src-gen/org/xtext/dop/productdeclaration/parser/antlr/internal/InternalProductDeclaration.g:1028:5: this_Element_0= ruleElement
            {
             
                    newCompositeNode(grammarAccess.getAtomicAccess().getElementParserRuleCall()); 
                
            pushFollow(FOLLOW_ruleElement_in_ruleAtomic2298);
            this_Element_0=ruleElement();

            state._fsp--;

             
                    current = this_Element_0; 
                    afterParserOrEnumRuleCall();
                

            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAtomic"

    // Delegated rules


 

    public static final BitSet FOLLOW_ruleProductDeclaration_in_entryRuleProductDeclaration75 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleProductDeclaration85 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_ruleProductDeclaration122 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleProductDeclaration139 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_ruleProductDeclaration156 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_13_in_ruleProductDeclaration168 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_ruleProductDeclaration180 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_ruleProductDeclaration192 = new BitSet(new long[]{0x0000000000008010L});
    public static final BitSet FOLLOW_ruleFeatures_in_ruleProductDeclaration213 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_ruleProductDeclaration226 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_ruleProductDeclaration238 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_ruleProductDeclaration250 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_ruleProductDeclaration262 = new BitSet(new long[]{0x0000000000008010L});
    public static final BitSet FOLLOW_ruleDeltas_in_ruleProductDeclaration283 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_ruleProductDeclaration296 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_ruleProductDeclaration308 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_ruleProductDeclaration320 = new BitSet(new long[]{0x0000000008408010L});
    public static final BitSet FOLLOW_ruleCore_in_ruleProductDeclaration341 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_ruleProductDeclaration354 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_ruleProductDeclaration366 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_ruleProductDeclaration378 = new BitSet(new long[]{0x0000000000009000L});
    public static final BitSet FOLLOW_rulePartitions_in_ruleProductDeclaration399 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_ruleProductDeclaration412 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_ruleProductDeclaration424 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_ruleProductDeclaration436 = new BitSet(new long[]{0x0000000000008010L});
    public static final BitSet FOLLOW_ruleProducts_in_ruleProductDeclaration457 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_ruleProductDeclaration470 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_ruleProductDeclaration482 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFeatures_in_entryRuleFeatures518 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFeatures528 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleListElem_in_ruleFeatures573 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_ruleDeltas_in_entryRuleDeltas609 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDeltas619 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleListElem_in_ruleDeltas664 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_ruleProducts_in_entryRuleProducts700 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleProducts710 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleProduct_in_ruleProducts755 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_ruleProduct_in_entryRuleProduct791 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleProduct801 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleProduct843 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_ruleProduct860 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_ruleProduct872 = new BitSet(new long[]{0x0000000000008010L});
    public static final BitSet FOLLOW_ruleListElem_in_ruleProduct893 = new BitSet(new long[]{0x0000000000008010L});
    public static final BitSet FOLLOW_15_in_ruleProduct906 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_20_in_ruleProduct918 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePartitions_in_entryRulePartitions954 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePartitions964 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePartition_in_rulePartitions1009 = new BitSet(new long[]{0x0000000000001002L});
    public static final BitSet FOLLOW_rulePartition_in_entryRulePartition1045 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePartition1055 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleConstraint_in_rulePartition1101 = new BitSet(new long[]{0x0000000000101000L});
    public static final BitSet FOLLOW_20_in_rulePartition1114 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleConstraint_in_entryRuleConstraint1150 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleConstraint1160 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_ruleConstraint1197 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleElement_in_ruleConstraint1218 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_ruleConstraint1230 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_21_in_ruleConstraint1242 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_22_in_ruleConstraint1254 = new BitSet(new long[]{0x0000000008400010L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleConstraint1275 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_23_in_ruleConstraint1287 = new BitSet(new long[]{0x0000000001000002L});
    public static final BitSet FOLLOW_24_in_ruleConstraint1305 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleListElem_in_entryRuleListElem1355 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleListElem1365 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleElement_in_ruleListElem1411 = new BitSet(new long[]{0x0000000001000002L});
    public static final BitSet FOLLOW_24_in_ruleListElem1429 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleElement_in_entryRuleElement1479 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleElement1489 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleElement1530 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCore_in_entryRuleCore1570 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCore1580 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleCore1626 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_20_in_ruleCore1638 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExpression_in_entryRuleExpression1674 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleExpression1684 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOr_in_ruleExpression1730 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOr_in_entryRuleOr1764 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOr1774 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAnd_in_ruleOr1821 = new BitSet(new long[]{0x0000000002000002L});
    public static final BitSet FOLLOW_25_in_ruleOr1842 = new BitSet(new long[]{0x0000000008400010L});
    public static final BitSet FOLLOW_ruleOr_in_ruleOr1863 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAnd_in_entryRuleAnd1901 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAnd1911 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePrimary_in_ruleAnd1958 = new BitSet(new long[]{0x0000000004000002L});
    public static final BitSet FOLLOW_26_in_ruleAnd1979 = new BitSet(new long[]{0x0000000008400010L});
    public static final BitSet FOLLOW_ruleAnd_in_ruleAnd2000 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePrimary_in_entryRulePrimary2038 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePrimary2048 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAtomic_in_rulePrimary2095 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_rulePrimary2122 = new BitSet(new long[]{0x0000000008400010L});
    public static final BitSet FOLLOW_ruleExpression_in_rulePrimary2143 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_23_in_rulePrimary2155 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_rulePrimary2184 = new BitSet(new long[]{0x0000000008400010L});
    public static final BitSet FOLLOW_rulePrimary_in_rulePrimary2205 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAtomic_in_entryRuleAtomic2242 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAtomic2252 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleElement_in_ruleAtomic2298 = new BitSet(new long[]{0x0000000000000002L});

}