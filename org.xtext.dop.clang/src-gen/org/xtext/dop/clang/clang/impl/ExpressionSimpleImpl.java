/**
 */
package org.xtext.dop.clang.clang.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.xtext.dop.clang.clang.ClangPackage;
import org.xtext.dop.clang.clang.Expression;
import org.xtext.dop.clang.clang.ExpressionSimple;
import org.xtext.dop.clang.clang.PostfixExpressionPostfix;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Expression Simple</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.clang.clang.impl.ExpressionSimpleImpl#getExp <em>Exp</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.impl.ExpressionSimpleImpl#getNext <em>Next</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ExpressionSimpleImpl extends ExpressionImpl implements ExpressionSimple
{
  /**
   * The cached value of the '{@link #getExp() <em>Exp</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExp()
   * @generated
   * @ordered
   */
  protected Expression exp;

  /**
   * The cached value of the '{@link #getNext() <em>Next</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNext()
   * @generated
   * @ordered
   */
  protected EList<PostfixExpressionPostfix> next;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ExpressionSimpleImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ClangPackage.Literals.EXPRESSION_SIMPLE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Expression getExp()
  {
    return exp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetExp(Expression newExp, NotificationChain msgs)
  {
    Expression oldExp = exp;
    exp = newExp;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ClangPackage.EXPRESSION_SIMPLE__EXP, oldExp, newExp);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setExp(Expression newExp)
  {
    if (newExp != exp)
    {
      NotificationChain msgs = null;
      if (exp != null)
        msgs = ((InternalEObject)exp).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ClangPackage.EXPRESSION_SIMPLE__EXP, null, msgs);
      if (newExp != null)
        msgs = ((InternalEObject)newExp).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ClangPackage.EXPRESSION_SIMPLE__EXP, null, msgs);
      msgs = basicSetExp(newExp, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ClangPackage.EXPRESSION_SIMPLE__EXP, newExp, newExp));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<PostfixExpressionPostfix> getNext()
  {
    if (next == null)
    {
      next = new EObjectContainmentEList<PostfixExpressionPostfix>(PostfixExpressionPostfix.class, this, ClangPackage.EXPRESSION_SIMPLE__NEXT);
    }
    return next;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case ClangPackage.EXPRESSION_SIMPLE__EXP:
        return basicSetExp(null, msgs);
      case ClangPackage.EXPRESSION_SIMPLE__NEXT:
        return ((InternalEList<?>)getNext()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case ClangPackage.EXPRESSION_SIMPLE__EXP:
        return getExp();
      case ClangPackage.EXPRESSION_SIMPLE__NEXT:
        return getNext();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case ClangPackage.EXPRESSION_SIMPLE__EXP:
        setExp((Expression)newValue);
        return;
      case ClangPackage.EXPRESSION_SIMPLE__NEXT:
        getNext().clear();
        getNext().addAll((Collection<? extends PostfixExpressionPostfix>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case ClangPackage.EXPRESSION_SIMPLE__EXP:
        setExp((Expression)null);
        return;
      case ClangPackage.EXPRESSION_SIMPLE__NEXT:
        getNext().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case ClangPackage.EXPRESSION_SIMPLE__EXP:
        return exp != null;
      case ClangPackage.EXPRESSION_SIMPLE__NEXT:
        return next != null && !next.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //ExpressionSimpleImpl
