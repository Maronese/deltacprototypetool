/**
 */
package org.xtext.dop.codebase.codeBase;

import org.eclipse.emf.ecore.EObject;

import org.xtext.dop.clang.clang.Source;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Remove Source</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.codebase.codeBase.RemoveSource#getFileName <em>File Name</em>}</li>
 * </ul>
 *
 * @see org.xtext.dop.codebase.codeBase.CodeBasePackage#getRemoveSource()
 * @model
 * @generated
 */
public interface RemoveSource extends EObject
{
  /**
   * Returns the value of the '<em><b>File Name</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>File Name</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>File Name</em>' containment reference.
   * @see #setFileName(Source)
   * @see org.xtext.dop.codebase.codeBase.CodeBasePackage#getRemoveSource_FileName()
   * @model containment="true"
   * @generated
   */
  Source getFileName();

  /**
   * Sets the value of the '{@link org.xtext.dop.codebase.codeBase.RemoveSource#getFileName <em>File Name</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>File Name</em>' containment reference.
   * @see #getFileName()
   * @generated
   */
  void setFileName(Source value);

} // RemoveSource
