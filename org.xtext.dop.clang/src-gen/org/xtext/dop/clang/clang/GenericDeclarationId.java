/**
 */
package org.xtext.dop.clang.clang;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Generic Declaration Id</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.clang.clang.GenericDeclarationId#getInner <em>Inner</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.GenericDeclarationId#getPosts <em>Posts</em>}</li>
 * </ul>
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getGenericDeclarationId()
 * @model
 * @generated
 */
public interface GenericDeclarationId extends EObject
{
  /**
   * Returns the value of the '<em><b>Inner</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Inner</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Inner</em>' containment reference.
   * @see #setInner(GenericDeclarationInnerId)
   * @see org.xtext.dop.clang.clang.ClangPackage#getGenericDeclarationId_Inner()
   * @model containment="true"
   * @generated
   */
  GenericDeclarationInnerId getInner();

  /**
   * Sets the value of the '{@link org.xtext.dop.clang.clang.GenericDeclarationId#getInner <em>Inner</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Inner</em>' containment reference.
   * @see #getInner()
   * @generated
   */
  void setInner(GenericDeclarationInnerId value);

  /**
   * Returns the value of the '<em><b>Posts</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.dop.clang.clang.GenericDeclarationIdPostfix}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Posts</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Posts</em>' containment reference list.
   * @see org.xtext.dop.clang.clang.ClangPackage#getGenericDeclarationId_Posts()
   * @model containment="true"
   * @generated
   */
  EList<GenericDeclarationIdPostfix> getPosts();

} // GenericDeclarationId
