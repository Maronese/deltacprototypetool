/**
 */
package org.xtext.dop.clang.clang.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.xtext.dop.clang.clang.AdditiveExpression;
import org.xtext.dop.clang.clang.AlignmentSpecifier;
import org.xtext.dop.clang.clang.AndExpression;
import org.xtext.dop.clang.clang.ArrayPostfix;
import org.xtext.dop.clang.clang.AssignmentExpression;
import org.xtext.dop.clang.clang.AssignmentExpressionElement;
import org.xtext.dop.clang.clang.Atomic;
import org.xtext.dop.clang.clang.BaseType;
import org.xtext.dop.clang.clang.BaseTypeBool;
import org.xtext.dop.clang.clang.BaseTypeChar;
import org.xtext.dop.clang.clang.BaseTypeComplex;
import org.xtext.dop.clang.clang.BaseTypeDouble;
import org.xtext.dop.clang.clang.BaseTypeFloat;
import org.xtext.dop.clang.clang.BaseTypeImaginary;
import org.xtext.dop.clang.clang.BaseTypeInt;
import org.xtext.dop.clang.clang.BaseTypeLong;
import org.xtext.dop.clang.clang.BaseTypeShort;
import org.xtext.dop.clang.clang.BaseTypeSigned;
import org.xtext.dop.clang.clang.BaseTypeUnsigned;
import org.xtext.dop.clang.clang.BaseTypeVoid;
import org.xtext.dop.clang.clang.BlockItem;
import org.xtext.dop.clang.clang.ClangFactory;
import org.xtext.dop.clang.clang.ClangPackage;
import org.xtext.dop.clang.clang.CompleteName;
import org.xtext.dop.clang.clang.CompoundStatement;
import org.xtext.dop.clang.clang.ConditionalExpression;
import org.xtext.dop.clang.clang.Custom;
import org.xtext.dop.clang.clang.Declaration;
import org.xtext.dop.clang.clang.DeclarationSpecifier;
import org.xtext.dop.clang.clang.EnumType;
import org.xtext.dop.clang.clang.EnumerationConstant;
import org.xtext.dop.clang.clang.Enumerator;
import org.xtext.dop.clang.clang.EnumeratorList;
import org.xtext.dop.clang.clang.EqualityExpression;
import org.xtext.dop.clang.clang.ExclusiveOrExpression;
import org.xtext.dop.clang.clang.Expression;
import org.xtext.dop.clang.clang.ExpressionAlign;
import org.xtext.dop.clang.clang.ExpressionCast;
import org.xtext.dop.clang.clang.ExpressionComplex;
import org.xtext.dop.clang.clang.ExpressionDecrement;
import org.xtext.dop.clang.clang.ExpressionFloat;
import org.xtext.dop.clang.clang.ExpressionGenericSelection;
import org.xtext.dop.clang.clang.ExpressionIdentifier;
import org.xtext.dop.clang.clang.ExpressionIncrement;
import org.xtext.dop.clang.clang.ExpressionInitializer;
import org.xtext.dop.clang.clang.ExpressionInteger;
import org.xtext.dop.clang.clang.ExpressionSimple;
import org.xtext.dop.clang.clang.ExpressionSizeof;
import org.xtext.dop.clang.clang.ExpressionStatement;
import org.xtext.dop.clang.clang.ExpressionString;
import org.xtext.dop.clang.clang.ExpressionSubExpression;
import org.xtext.dop.clang.clang.ExpressionUnaryOp;
import org.xtext.dop.clang.clang.FileInclude;
import org.xtext.dop.clang.clang.FunctionInitializer;
import org.xtext.dop.clang.clang.FunctionSpecifier;
import org.xtext.dop.clang.clang.FunctionSpecifierInline;
import org.xtext.dop.clang.clang.FunctionSpecifierNoReturn;
import org.xtext.dop.clang.clang.GenericAssociation;
import org.xtext.dop.clang.clang.GenericAssociationCase;
import org.xtext.dop.clang.clang.GenericAssociationDefault;
import org.xtext.dop.clang.clang.GenericDeclaration0;
import org.xtext.dop.clang.clang.GenericDeclaration1;
import org.xtext.dop.clang.clang.GenericDeclaration2;
import org.xtext.dop.clang.clang.GenericDeclarationElement;
import org.xtext.dop.clang.clang.GenericDeclarationElements;
import org.xtext.dop.clang.clang.GenericDeclarationId;
import org.xtext.dop.clang.clang.GenericDeclarationIdPostfix;
import org.xtext.dop.clang.clang.GenericDeclarationInitializer;
import org.xtext.dop.clang.clang.GenericDeclarationInnerId;
import org.xtext.dop.clang.clang.GenericSpecifiedDeclaration;
import org.xtext.dop.clang.clang.GlobalName;
import org.xtext.dop.clang.clang.GoToPrefixStatement;
import org.xtext.dop.clang.clang.InclusiveOrExpression;
import org.xtext.dop.clang.clang.IterationStatement;
import org.xtext.dop.clang.clang.IterationStatementDo;
import org.xtext.dop.clang.clang.IterationStatementFor;
import org.xtext.dop.clang.clang.IterationStatementWhile;
import org.xtext.dop.clang.clang.JumpStatement;
import org.xtext.dop.clang.clang.JumpStatementBreak;
import org.xtext.dop.clang.clang.JumpStatementContinue;
import org.xtext.dop.clang.clang.JumpStatementGoto;
import org.xtext.dop.clang.clang.JumpStatementReturn;
import org.xtext.dop.clang.clang.LabeledStatement;
import org.xtext.dop.clang.clang.LabeledStatementCase;
import org.xtext.dop.clang.clang.LabeledStatementDefault;
import org.xtext.dop.clang.clang.LogicalAndExpression;
import org.xtext.dop.clang.clang.LogicalOrExpression;
import org.xtext.dop.clang.clang.Model;
import org.xtext.dop.clang.clang.MultiplicativeExpression;
import org.xtext.dop.clang.clang.NextAsignement;
import org.xtext.dop.clang.clang.NextField;
import org.xtext.dop.clang.clang.NextParameter;
import org.xtext.dop.clang.clang.Parameter;
import org.xtext.dop.clang.clang.ParametersPostfix;
import org.xtext.dop.clang.clang.PostfixExpressionPostfix;
import org.xtext.dop.clang.clang.PostfixExpressionPostfixAccess;
import org.xtext.dop.clang.clang.PostfixExpressionPostfixAccessPtr;
import org.xtext.dop.clang.clang.PostfixExpressionPostfixArgumentList;
import org.xtext.dop.clang.clang.PostfixExpressionPostfixDecrement;
import org.xtext.dop.clang.clang.PostfixExpressionPostfixExpression;
import org.xtext.dop.clang.clang.PostfixExpressionPostfixIncrement;
import org.xtext.dop.clang.clang.RelationalExpression;
import org.xtext.dop.clang.clang.SelectionStatement;
import org.xtext.dop.clang.clang.SelectionStatementIf;
import org.xtext.dop.clang.clang.SelectionStatementSwitch;
import org.xtext.dop.clang.clang.ShiftExpression;
import org.xtext.dop.clang.clang.SimpleName;
import org.xtext.dop.clang.clang.SimpleType;
import org.xtext.dop.clang.clang.Source;
import org.xtext.dop.clang.clang.SpecifiedType;
import org.xtext.dop.clang.clang.StandardInclude;
import org.xtext.dop.clang.clang.Statement;
import org.xtext.dop.clang.clang.StatementDeclaration;
import org.xtext.dop.clang.clang.StatementInnerStatement;
import org.xtext.dop.clang.clang.Statements;
import org.xtext.dop.clang.clang.StaticAssertDeclaration;
import org.xtext.dop.clang.clang.StorageClassSpecifier;
import org.xtext.dop.clang.clang.StorageClassSpecifierAuto;
import org.xtext.dop.clang.clang.StorageClassSpecifierExtern;
import org.xtext.dop.clang.clang.StorageClassSpecifierRegister;
import org.xtext.dop.clang.clang.StorageClassSpecifierStatic;
import org.xtext.dop.clang.clang.StorageClassSpecifierThreadLocal;
import org.xtext.dop.clang.clang.StringExpression;
import org.xtext.dop.clang.clang.StructOrUnionType;
import org.xtext.dop.clang.clang.TypeAliasDeclaration;
import org.xtext.dop.clang.clang.TypeQualifier;
import org.xtext.dop.clang.clang.TypeQualifierAtomic;
import org.xtext.dop.clang.clang.TypeQualifierConst;
import org.xtext.dop.clang.clang.TypeQualifierVolatile;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ClangPackageImpl extends EPackageImpl implements ClangPackage
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass modelEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass standardIncludeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fileIncludeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass sourceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass declarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass typeAliasDeclarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass genericSpecifiedDeclarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass genericDeclarationElementsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass genericDeclarationElementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass genericDeclarationIdEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass genericDeclarationInnerIdEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass genericDeclarationIdPostfixEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass arrayPostfixEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass parametersPostfixEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass nextParameterEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass parameterEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass genericDeclarationInitializerEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass specifiedTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass staticAssertDeclarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass simpleTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass baseTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass structOrUnionTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass enumTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass enumeratorListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass enumeratorEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass enumerationConstantEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass declarationSpecifierEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass storageClassSpecifierEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass typeQualifierEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass functionSpecifierEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass alignmentSpecifierEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass expressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass assignmentExpressionElementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass stringExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass genericAssociationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass postfixExpressionPostfixEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass nextAsignementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass compoundStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass blockItemEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass statementsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass labeledStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass statementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass expressionStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass selectionStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass iterationStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass nextFieldEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass jumpStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass completeNameEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass simpleNameEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass genericDeclaration0EClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass genericDeclaration1EClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass genericDeclaration2EClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass globalNameEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass expressionInitializerEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass functionInitializerEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass atomicEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass baseTypeVoidEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass customEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass baseTypeCharEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass baseTypeShortEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass baseTypeIntEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass baseTypeLongEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass baseTypeFloatEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass baseTypeDoubleEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass baseTypeSignedEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass baseTypeUnsignedEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass baseTypeBoolEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass baseTypeComplexEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass baseTypeImaginaryEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass storageClassSpecifierExternEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass storageClassSpecifierStaticEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass storageClassSpecifierThreadLocalEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass storageClassSpecifierAutoEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass storageClassSpecifierRegisterEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass typeQualifierConstEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass typeQualifierVolatileEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass typeQualifierAtomicEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass functionSpecifierInlineEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass functionSpecifierNoReturnEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass assignmentExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass conditionalExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass logicalOrExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass logicalAndExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass inclusiveOrExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass exclusiveOrExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass andExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass equalityExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass relationalExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass shiftExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass additiveExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass multiplicativeExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass expressionSimpleEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass expressionComplexEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass expressionIncrementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass expressionDecrementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass expressionSizeofEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass expressionUnaryOpEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass expressionAlignEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass expressionCastEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass expressionIdentifierEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass expressionStringEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass expressionIntegerEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass expressionFloatEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass expressionSubExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass expressionGenericSelectionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass genericAssociationCaseEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass genericAssociationDefaultEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass postfixExpressionPostfixExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass postfixExpressionPostfixArgumentListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass postfixExpressionPostfixAccessEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass postfixExpressionPostfixAccessPtrEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass postfixExpressionPostfixIncrementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass postfixExpressionPostfixDecrementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass statementDeclarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass statementInnerStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass labeledStatementCaseEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass labeledStatementDefaultEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass goToPrefixStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass selectionStatementIfEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass selectionStatementSwitchEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass iterationStatementWhileEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass iterationStatementDoEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass iterationStatementForEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass jumpStatementGotoEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass jumpStatementContinueEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass jumpStatementBreakEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass jumpStatementReturnEClass = null;

  /**
   * Creates an instance of the model <b>Package</b>, registered with
   * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
   * package URI value.
   * <p>Note: the correct way to create the package is via the static
   * factory method {@link #init init()}, which also performs
   * initialization of the package, or returns the registered package,
   * if one already exists.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.emf.ecore.EPackage.Registry
   * @see org.xtext.dop.clang.clang.ClangPackage#eNS_URI
   * @see #init()
   * @generated
   */
  private ClangPackageImpl()
  {
    super(eNS_URI, ClangFactory.eINSTANCE);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static boolean isInited = false;

  /**
   * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
   * 
   * <p>This method is used to initialize {@link ClangPackage#eINSTANCE} when that field is accessed.
   * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #eNS_URI
   * @see #createPackageContents()
   * @see #initializePackageContents()
   * @generated
   */
  public static ClangPackage init()
  {
    if (isInited) return (ClangPackage)EPackage.Registry.INSTANCE.getEPackage(ClangPackage.eNS_URI);

    // Obtain or create and register package
    ClangPackageImpl theClangPackage = (ClangPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ClangPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ClangPackageImpl());

    isInited = true;

    // Create package meta-data objects
    theClangPackage.createPackageContents();

    // Initialize created meta-data
    theClangPackage.initializePackageContents();

    // Mark meta-data to indicate it can't be changed
    theClangPackage.freeze();

  
    // Update the registry and return the package
    EPackage.Registry.INSTANCE.put(ClangPackage.eNS_URI, theClangPackage);
    return theClangPackage;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getModel()
  {
    return modelEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getModel_IncludesStd()
  {
    return (EReference)modelEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getModel_IncludesFile()
  {
    return (EReference)modelEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getModel_Declarations()
  {
    return (EReference)modelEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStandardInclude()
  {
    return standardIncludeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStandardInclude_Std()
  {
    return (EReference)standardIncludeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getStandardInclude_Wild()
  {
    return (EAttribute)standardIncludeEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFileInclude()
  {
    return fileIncludeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getFileInclude_File()
  {
    return (EAttribute)fileIncludeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSource()
  {
    return sourceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSource_Name()
  {
    return (EAttribute)sourceEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDeclaration()
  {
    return declarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTypeAliasDeclaration()
  {
    return typeAliasDeclarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTypeAliasDeclaration_Tdecl()
  {
    return (EReference)typeAliasDeclarationEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getGenericSpecifiedDeclaration()
  {
    return genericSpecifiedDeclarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getGenericSpecifiedDeclaration_Specs()
  {
    return (EReference)genericSpecifiedDeclarationEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getGenericSpecifiedDeclaration_Type()
  {
    return (EReference)genericSpecifiedDeclarationEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getGenericSpecifiedDeclaration_Decl()
  {
    return (EReference)genericSpecifiedDeclarationEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getGenericDeclarationElements()
  {
    return genericDeclarationElementsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getGenericDeclarationElements_Els()
  {
    return (EReference)genericDeclarationElementsEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getGenericDeclarationElements_Next()
  {
    return (EReference)genericDeclarationElementsEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getGenericDeclarationElement()
  {
    return genericDeclarationElementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getGenericDeclarationElement_Id()
  {
    return (EReference)genericDeclarationElementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getGenericDeclarationElement_Init()
  {
    return (EReference)genericDeclarationElementEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getGenericDeclarationId()
  {
    return genericDeclarationIdEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getGenericDeclarationId_Inner()
  {
    return (EReference)genericDeclarationIdEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getGenericDeclarationId_Posts()
  {
    return (EReference)genericDeclarationIdEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getGenericDeclarationInnerId()
  {
    return genericDeclarationInnerIdEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getGenericDeclarationInnerId_Inner()
  {
    return (EReference)genericDeclarationInnerIdEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getGenericDeclarationIdPostfix()
  {
    return genericDeclarationIdPostfixEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getGenericDeclarationIdPostfix_Array()
  {
    return (EReference)genericDeclarationIdPostfixEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getGenericDeclarationIdPostfix_Function()
  {
    return (EReference)genericDeclarationIdPostfixEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getArrayPostfix()
  {
    return arrayPostfixEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getArrayPostfix_Star()
  {
    return (EAttribute)arrayPostfixEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getArrayPostfix_Exp()
  {
    return (EReference)arrayPostfixEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getParametersPostfix()
  {
    return parametersPostfixEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getParametersPostfix_Els()
  {
    return (EReference)parametersPostfixEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getParametersPostfix_Next()
  {
    return (EReference)parametersPostfixEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getParametersPostfix_More()
  {
    return (EAttribute)parametersPostfixEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getNextParameter()
  {
    return nextParameterEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getNextParameter_Els()
  {
    return (EReference)nextParameterEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getParameter()
  {
    return parameterEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getParameter_Specs()
  {
    return (EReference)parameterEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getParameter_Type()
  {
    return (EReference)parameterEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getParameter_Id()
  {
    return (EReference)parameterEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getGenericDeclarationInitializer()
  {
    return genericDeclarationInitializerEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSpecifiedType()
  {
    return specifiedTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSpecifiedType_Par()
  {
    return (EReference)specifiedTypeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStaticAssertDeclaration()
  {
    return staticAssertDeclarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStaticAssertDeclaration_Exp()
  {
    return (EReference)staticAssertDeclarationEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getStaticAssertDeclaration_String()
  {
    return (EAttribute)staticAssertDeclarationEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSimpleType()
  {
    return simpleTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBaseType()
  {
    return baseTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStructOrUnionType()
  {
    return structOrUnionTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getStructOrUnionType_Kind()
  {
    return (EAttribute)structOrUnionTypeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStructOrUnionType_Content()
  {
    return (EReference)structOrUnionTypeEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getStructOrUnionType_Name()
  {
    return (EAttribute)structOrUnionTypeEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getEnumType()
  {
    return enumTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getEnumType_Content()
  {
    return (EReference)enumTypeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getEnumType_Name()
  {
    return (EAttribute)enumTypeEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getEnumeratorList()
  {
    return enumeratorListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getEnumeratorList_Elements()
  {
    return (EReference)enumeratorListEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getEnumerator()
  {
    return enumeratorEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getEnumerator_Name()
  {
    return (EReference)enumeratorEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getEnumerator_Exp()
  {
    return (EReference)enumeratorEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getEnumerationConstant()
  {
    return enumerationConstantEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getEnumerationConstant_Name()
  {
    return (EAttribute)enumerationConstantEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDeclarationSpecifier()
  {
    return declarationSpecifierEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStorageClassSpecifier()
  {
    return storageClassSpecifierEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTypeQualifier()
  {
    return typeQualifierEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFunctionSpecifier()
  {
    return functionSpecifierEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAlignmentSpecifier()
  {
    return alignmentSpecifierEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAlignmentSpecifier_Type()
  {
    return (EReference)alignmentSpecifierEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAlignmentSpecifier_Exp()
  {
    return (EReference)alignmentSpecifierEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExpression()
  {
    return expressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAssignmentExpressionElement()
  {
    return assignmentExpressionElementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getAssignmentExpressionElement_Op()
  {
    return (EAttribute)assignmentExpressionElementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAssignmentExpressionElement_Exp()
  {
    return (EReference)assignmentExpressionElementEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStringExpression()
  {
    return stringExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getStringExpression_Name()
  {
    return (EAttribute)stringExpressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getGenericAssociation()
  {
    return genericAssociationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getGenericAssociation_Exp()
  {
    return (EReference)genericAssociationEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPostfixExpressionPostfix()
  {
    return postfixExpressionPostfixEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getNextAsignement()
  {
    return nextAsignementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getNextAsignement_Right()
  {
    return (EReference)nextAsignementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCompoundStatement()
  {
    return compoundStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCompoundStatement_Inner()
  {
    return (EReference)compoundStatementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBlockItem()
  {
    return blockItemEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStatements()
  {
    return statementsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLabeledStatement()
  {
    return labeledStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLabeledStatement_Statement()
  {
    return (EReference)labeledStatementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStatement()
  {
    return statementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExpressionStatement()
  {
    return expressionStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExpressionStatement_Exp()
  {
    return (EReference)expressionStatementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSelectionStatement()
  {
    return selectionStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSelectionStatement_Exp()
  {
    return (EReference)selectionStatementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getIterationStatement()
  {
    return iterationStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIterationStatement_Statement()
  {
    return (EReference)iterationStatementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getNextField()
  {
    return nextFieldEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getNextField_Next()
  {
    return (EReference)nextFieldEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getJumpStatement()
  {
    return jumpStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCompleteName()
  {
    return completeNameEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getCompleteName_Ext()
  {
    return (EAttribute)completeNameEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSimpleName()
  {
    return simpleNameEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getGenericDeclaration0()
  {
    return genericDeclaration0EClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getGenericDeclaration0_Pointers()
  {
    return (EAttribute)genericDeclaration0EClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getGenericDeclaration0_Restrict()
  {
    return (EAttribute)genericDeclaration0EClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getGenericDeclaration1()
  {
    return genericDeclaration1EClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getGenericDeclaration2()
  {
    return genericDeclaration2EClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getGlobalName()
  {
    return globalNameEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getGlobalName_Name()
  {
    return (EAttribute)globalNameEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExpressionInitializer()
  {
    return expressionInitializerEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExpressionInitializer_Exp()
  {
    return (EReference)expressionInitializerEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFunctionInitializer()
  {
    return functionInitializerEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionInitializer_Block()
  {
    return (EReference)functionInitializerEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAtomic()
  {
    return atomicEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAtomic_Type()
  {
    return (EReference)atomicEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBaseTypeVoid()
  {
    return baseTypeVoidEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCustom()
  {
    return customEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getCustom_Id()
  {
    return (EAttribute)customEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBaseTypeChar()
  {
    return baseTypeCharEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBaseTypeShort()
  {
    return baseTypeShortEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBaseTypeInt()
  {
    return baseTypeIntEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBaseTypeLong()
  {
    return baseTypeLongEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBaseTypeFloat()
  {
    return baseTypeFloatEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBaseTypeDouble()
  {
    return baseTypeDoubleEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBaseTypeSigned()
  {
    return baseTypeSignedEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBaseTypeUnsigned()
  {
    return baseTypeUnsignedEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBaseTypeBool()
  {
    return baseTypeBoolEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBaseTypeComplex()
  {
    return baseTypeComplexEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBaseTypeImaginary()
  {
    return baseTypeImaginaryEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStorageClassSpecifierExtern()
  {
    return storageClassSpecifierExternEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStorageClassSpecifierStatic()
  {
    return storageClassSpecifierStaticEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStorageClassSpecifierThreadLocal()
  {
    return storageClassSpecifierThreadLocalEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStorageClassSpecifierAuto()
  {
    return storageClassSpecifierAutoEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStorageClassSpecifierRegister()
  {
    return storageClassSpecifierRegisterEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTypeQualifierConst()
  {
    return typeQualifierConstEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTypeQualifierVolatile()
  {
    return typeQualifierVolatileEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTypeQualifierAtomic()
  {
    return typeQualifierAtomicEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFunctionSpecifierInline()
  {
    return functionSpecifierInlineEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFunctionSpecifierNoReturn()
  {
    return functionSpecifierNoReturnEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAssignmentExpression()
  {
    return assignmentExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAssignmentExpression_Exp()
  {
    return (EReference)assignmentExpressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAssignmentExpression_List()
  {
    return (EReference)assignmentExpressionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getConditionalExpression()
  {
    return conditionalExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConditionalExpression_If()
  {
    return (EReference)conditionalExpressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConditionalExpression_Yes()
  {
    return (EReference)conditionalExpressionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConditionalExpression_No()
  {
    return (EReference)conditionalExpressionEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLogicalOrExpression()
  {
    return logicalOrExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLogicalOrExpression_Left()
  {
    return (EReference)logicalOrExpressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getLogicalOrExpression_Operand()
  {
    return (EAttribute)logicalOrExpressionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLogicalOrExpression_Right()
  {
    return (EReference)logicalOrExpressionEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLogicalAndExpression()
  {
    return logicalAndExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLogicalAndExpression_Left()
  {
    return (EReference)logicalAndExpressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getLogicalAndExpression_Operand()
  {
    return (EAttribute)logicalAndExpressionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLogicalAndExpression_Right()
  {
    return (EReference)logicalAndExpressionEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getInclusiveOrExpression()
  {
    return inclusiveOrExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getInclusiveOrExpression_Left()
  {
    return (EReference)inclusiveOrExpressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getInclusiveOrExpression_Operand()
  {
    return (EAttribute)inclusiveOrExpressionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getInclusiveOrExpression_Right()
  {
    return (EReference)inclusiveOrExpressionEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExclusiveOrExpression()
  {
    return exclusiveOrExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExclusiveOrExpression_Left()
  {
    return (EReference)exclusiveOrExpressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getExclusiveOrExpression_Operand()
  {
    return (EAttribute)exclusiveOrExpressionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExclusiveOrExpression_Right()
  {
    return (EReference)exclusiveOrExpressionEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAndExpression()
  {
    return andExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAndExpression_Left()
  {
    return (EReference)andExpressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getAndExpression_Operand()
  {
    return (EAttribute)andExpressionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAndExpression_Right()
  {
    return (EReference)andExpressionEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getEqualityExpression()
  {
    return equalityExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getEqualityExpression_Left()
  {
    return (EReference)equalityExpressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getEqualityExpression_Operand()
  {
    return (EAttribute)equalityExpressionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getEqualityExpression_Right()
  {
    return (EReference)equalityExpressionEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRelationalExpression()
  {
    return relationalExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRelationalExpression_Left()
  {
    return (EReference)relationalExpressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getRelationalExpression_Operand()
  {
    return (EAttribute)relationalExpressionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRelationalExpression_Right()
  {
    return (EReference)relationalExpressionEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getShiftExpression()
  {
    return shiftExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getShiftExpression_Left()
  {
    return (EReference)shiftExpressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getShiftExpression_Operand()
  {
    return (EAttribute)shiftExpressionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getShiftExpression_Right()
  {
    return (EReference)shiftExpressionEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAdditiveExpression()
  {
    return additiveExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAdditiveExpression_Left()
  {
    return (EReference)additiveExpressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getAdditiveExpression_Operand()
  {
    return (EAttribute)additiveExpressionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAdditiveExpression_Right()
  {
    return (EReference)additiveExpressionEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getMultiplicativeExpression()
  {
    return multiplicativeExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMultiplicativeExpression_Left()
  {
    return (EReference)multiplicativeExpressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getMultiplicativeExpression_Operand()
  {
    return (EAttribute)multiplicativeExpressionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMultiplicativeExpression_Right()
  {
    return (EReference)multiplicativeExpressionEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExpressionSimple()
  {
    return expressionSimpleEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExpressionSimple_Exp()
  {
    return (EReference)expressionSimpleEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExpressionSimple_Next()
  {
    return (EReference)expressionSimpleEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExpressionComplex()
  {
    return expressionComplexEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExpressionComplex_Exps()
  {
    return (EReference)expressionComplexEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExpressionComplex_Next()
  {
    return (EReference)expressionComplexEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExpressionIncrement()
  {
    return expressionIncrementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExpressionIncrement_Content()
  {
    return (EReference)expressionIncrementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExpressionDecrement()
  {
    return expressionDecrementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExpressionDecrement_Content()
  {
    return (EReference)expressionDecrementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExpressionSizeof()
  {
    return expressionSizeofEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExpressionSizeof_Content()
  {
    return (EReference)expressionSizeofEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExpressionSizeof_Type()
  {
    return (EReference)expressionSizeofEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExpressionUnaryOp()
  {
    return expressionUnaryOpEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getExpressionUnaryOp_Op()
  {
    return (EAttribute)expressionUnaryOpEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExpressionUnaryOp_Content()
  {
    return (EReference)expressionUnaryOpEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExpressionAlign()
  {
    return expressionAlignEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExpressionAlign_Type()
  {
    return (EReference)expressionAlignEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExpressionCast()
  {
    return expressionCastEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExpressionCast_Type()
  {
    return (EReference)expressionCastEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExpressionCast_Exp()
  {
    return (EReference)expressionCastEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExpressionIdentifier()
  {
    return expressionIdentifierEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getExpressionIdentifier_Name()
  {
    return (EAttribute)expressionIdentifierEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExpressionString()
  {
    return expressionStringEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExpressionString_Value()
  {
    return (EReference)expressionStringEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExpressionInteger()
  {
    return expressionIntegerEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getExpressionInteger_Value()
  {
    return (EAttribute)expressionIntegerEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExpressionFloat()
  {
    return expressionFloatEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getExpressionFloat_Value()
  {
    return (EAttribute)expressionFloatEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExpressionSubExpression()
  {
    return expressionSubExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExpressionSubExpression_Par()
  {
    return (EReference)expressionSubExpressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExpressionGenericSelection()
  {
    return expressionGenericSelectionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExpressionGenericSelection_Left()
  {
    return (EReference)expressionGenericSelectionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExpressionGenericSelection_Right()
  {
    return (EReference)expressionGenericSelectionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getGenericAssociationCase()
  {
    return genericAssociationCaseEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getGenericAssociationCase_Type()
  {
    return (EReference)genericAssociationCaseEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getGenericAssociationDefault()
  {
    return genericAssociationDefaultEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPostfixExpressionPostfixExpression()
  {
    return postfixExpressionPostfixExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPostfixExpressionPostfixExpression_Value()
  {
    return (EReference)postfixExpressionPostfixExpressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPostfixExpressionPostfixArgumentList()
  {
    return postfixExpressionPostfixArgumentListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPostfixExpressionPostfixArgumentList_Left()
  {
    return (EReference)postfixExpressionPostfixArgumentListEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPostfixExpressionPostfixArgumentList_Next()
  {
    return (EReference)postfixExpressionPostfixArgumentListEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPostfixExpressionPostfixAccess()
  {
    return postfixExpressionPostfixAccessEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getPostfixExpressionPostfixAccess_Value()
  {
    return (EAttribute)postfixExpressionPostfixAccessEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPostfixExpressionPostfixAccessPtr()
  {
    return postfixExpressionPostfixAccessPtrEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getPostfixExpressionPostfixAccessPtr_Value()
  {
    return (EAttribute)postfixExpressionPostfixAccessPtrEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPostfixExpressionPostfixIncrement()
  {
    return postfixExpressionPostfixIncrementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPostfixExpressionPostfixDecrement()
  {
    return postfixExpressionPostfixDecrementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStatementDeclaration()
  {
    return statementDeclarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStatementDeclaration_Decl()
  {
    return (EReference)statementDeclarationEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStatementInnerStatement()
  {
    return statementInnerStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStatementInnerStatement_Statement()
  {
    return (EReference)statementInnerStatementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLabeledStatementCase()
  {
    return labeledStatementCaseEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLabeledStatementCase_Exp()
  {
    return (EReference)labeledStatementCaseEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLabeledStatementDefault()
  {
    return labeledStatementDefaultEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getGoToPrefixStatement()
  {
    return goToPrefixStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getGoToPrefixStatement_Goto()
  {
    return (EAttribute)goToPrefixStatementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getGoToPrefixStatement_Statement()
  {
    return (EReference)goToPrefixStatementEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSelectionStatementIf()
  {
    return selectionStatementIfEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSelectionStatementIf_Then_statement()
  {
    return (EReference)selectionStatementIfEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSelectionStatementIf_Else_statement()
  {
    return (EReference)selectionStatementIfEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSelectionStatementSwitch()
  {
    return selectionStatementSwitchEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSelectionStatementSwitch_Content()
  {
    return (EReference)selectionStatementSwitchEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getIterationStatementWhile()
  {
    return iterationStatementWhileEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIterationStatementWhile_Exp()
  {
    return (EReference)iterationStatementWhileEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getIterationStatementDo()
  {
    return iterationStatementDoEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIterationStatementDo_Exp()
  {
    return (EReference)iterationStatementDoEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getIterationStatementFor()
  {
    return iterationStatementForEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIterationStatementFor_Field1()
  {
    return (EReference)iterationStatementForEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIterationStatementFor_Decl()
  {
    return (EReference)iterationStatementForEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIterationStatementFor_Field2()
  {
    return (EReference)iterationStatementForEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIterationStatementFor_Field3()
  {
    return (EReference)iterationStatementForEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIterationStatementFor_Next()
  {
    return (EReference)iterationStatementForEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getJumpStatementGoto()
  {
    return jumpStatementGotoEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getJumpStatementGoto_Name()
  {
    return (EAttribute)jumpStatementGotoEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getJumpStatementContinue()
  {
    return jumpStatementContinueEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getJumpStatementBreak()
  {
    return jumpStatementBreakEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getJumpStatementReturn()
  {
    return jumpStatementReturnEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getJumpStatementReturn_Exp()
  {
    return (EReference)jumpStatementReturnEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ClangFactory getClangFactory()
  {
    return (ClangFactory)getEFactoryInstance();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isCreated = false;

  /**
   * Creates the meta-model objects for the package.  This method is
   * guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void createPackageContents()
  {
    if (isCreated) return;
    isCreated = true;

    // Create classes and their features
    modelEClass = createEClass(MODEL);
    createEReference(modelEClass, MODEL__INCLUDES_STD);
    createEReference(modelEClass, MODEL__INCLUDES_FILE);
    createEReference(modelEClass, MODEL__DECLARATIONS);

    standardIncludeEClass = createEClass(STANDARD_INCLUDE);
    createEReference(standardIncludeEClass, STANDARD_INCLUDE__STD);
    createEAttribute(standardIncludeEClass, STANDARD_INCLUDE__WILD);

    fileIncludeEClass = createEClass(FILE_INCLUDE);
    createEAttribute(fileIncludeEClass, FILE_INCLUDE__FILE);

    sourceEClass = createEClass(SOURCE);
    createEAttribute(sourceEClass, SOURCE__NAME);

    declarationEClass = createEClass(DECLARATION);

    typeAliasDeclarationEClass = createEClass(TYPE_ALIAS_DECLARATION);
    createEReference(typeAliasDeclarationEClass, TYPE_ALIAS_DECLARATION__TDECL);

    genericSpecifiedDeclarationEClass = createEClass(GENERIC_SPECIFIED_DECLARATION);
    createEReference(genericSpecifiedDeclarationEClass, GENERIC_SPECIFIED_DECLARATION__SPECS);
    createEReference(genericSpecifiedDeclarationEClass, GENERIC_SPECIFIED_DECLARATION__TYPE);
    createEReference(genericSpecifiedDeclarationEClass, GENERIC_SPECIFIED_DECLARATION__DECL);

    genericDeclarationElementsEClass = createEClass(GENERIC_DECLARATION_ELEMENTS);
    createEReference(genericDeclarationElementsEClass, GENERIC_DECLARATION_ELEMENTS__ELS);
    createEReference(genericDeclarationElementsEClass, GENERIC_DECLARATION_ELEMENTS__NEXT);

    genericDeclarationElementEClass = createEClass(GENERIC_DECLARATION_ELEMENT);
    createEReference(genericDeclarationElementEClass, GENERIC_DECLARATION_ELEMENT__ID);
    createEReference(genericDeclarationElementEClass, GENERIC_DECLARATION_ELEMENT__INIT);

    genericDeclarationIdEClass = createEClass(GENERIC_DECLARATION_ID);
    createEReference(genericDeclarationIdEClass, GENERIC_DECLARATION_ID__INNER);
    createEReference(genericDeclarationIdEClass, GENERIC_DECLARATION_ID__POSTS);

    genericDeclarationInnerIdEClass = createEClass(GENERIC_DECLARATION_INNER_ID);
    createEReference(genericDeclarationInnerIdEClass, GENERIC_DECLARATION_INNER_ID__INNER);

    genericDeclarationIdPostfixEClass = createEClass(GENERIC_DECLARATION_ID_POSTFIX);
    createEReference(genericDeclarationIdPostfixEClass, GENERIC_DECLARATION_ID_POSTFIX__ARRAY);
    createEReference(genericDeclarationIdPostfixEClass, GENERIC_DECLARATION_ID_POSTFIX__FUNCTION);

    arrayPostfixEClass = createEClass(ARRAY_POSTFIX);
    createEAttribute(arrayPostfixEClass, ARRAY_POSTFIX__STAR);
    createEReference(arrayPostfixEClass, ARRAY_POSTFIX__EXP);

    parametersPostfixEClass = createEClass(PARAMETERS_POSTFIX);
    createEReference(parametersPostfixEClass, PARAMETERS_POSTFIX__ELS);
    createEReference(parametersPostfixEClass, PARAMETERS_POSTFIX__NEXT);
    createEAttribute(parametersPostfixEClass, PARAMETERS_POSTFIX__MORE);

    nextParameterEClass = createEClass(NEXT_PARAMETER);
    createEReference(nextParameterEClass, NEXT_PARAMETER__ELS);

    parameterEClass = createEClass(PARAMETER);
    createEReference(parameterEClass, PARAMETER__SPECS);
    createEReference(parameterEClass, PARAMETER__TYPE);
    createEReference(parameterEClass, PARAMETER__ID);

    genericDeclarationInitializerEClass = createEClass(GENERIC_DECLARATION_INITIALIZER);

    specifiedTypeEClass = createEClass(SPECIFIED_TYPE);
    createEReference(specifiedTypeEClass, SPECIFIED_TYPE__PAR);

    staticAssertDeclarationEClass = createEClass(STATIC_ASSERT_DECLARATION);
    createEReference(staticAssertDeclarationEClass, STATIC_ASSERT_DECLARATION__EXP);
    createEAttribute(staticAssertDeclarationEClass, STATIC_ASSERT_DECLARATION__STRING);

    simpleTypeEClass = createEClass(SIMPLE_TYPE);

    baseTypeEClass = createEClass(BASE_TYPE);

    structOrUnionTypeEClass = createEClass(STRUCT_OR_UNION_TYPE);
    createEAttribute(structOrUnionTypeEClass, STRUCT_OR_UNION_TYPE__KIND);
    createEReference(structOrUnionTypeEClass, STRUCT_OR_UNION_TYPE__CONTENT);
    createEAttribute(structOrUnionTypeEClass, STRUCT_OR_UNION_TYPE__NAME);

    enumTypeEClass = createEClass(ENUM_TYPE);
    createEReference(enumTypeEClass, ENUM_TYPE__CONTENT);
    createEAttribute(enumTypeEClass, ENUM_TYPE__NAME);

    enumeratorListEClass = createEClass(ENUMERATOR_LIST);
    createEReference(enumeratorListEClass, ENUMERATOR_LIST__ELEMENTS);

    enumeratorEClass = createEClass(ENUMERATOR);
    createEReference(enumeratorEClass, ENUMERATOR__NAME);
    createEReference(enumeratorEClass, ENUMERATOR__EXP);

    enumerationConstantEClass = createEClass(ENUMERATION_CONSTANT);
    createEAttribute(enumerationConstantEClass, ENUMERATION_CONSTANT__NAME);

    declarationSpecifierEClass = createEClass(DECLARATION_SPECIFIER);

    storageClassSpecifierEClass = createEClass(STORAGE_CLASS_SPECIFIER);

    typeQualifierEClass = createEClass(TYPE_QUALIFIER);

    functionSpecifierEClass = createEClass(FUNCTION_SPECIFIER);

    alignmentSpecifierEClass = createEClass(ALIGNMENT_SPECIFIER);
    createEReference(alignmentSpecifierEClass, ALIGNMENT_SPECIFIER__TYPE);
    createEReference(alignmentSpecifierEClass, ALIGNMENT_SPECIFIER__EXP);

    expressionEClass = createEClass(EXPRESSION);

    assignmentExpressionElementEClass = createEClass(ASSIGNMENT_EXPRESSION_ELEMENT);
    createEAttribute(assignmentExpressionElementEClass, ASSIGNMENT_EXPRESSION_ELEMENT__OP);
    createEReference(assignmentExpressionElementEClass, ASSIGNMENT_EXPRESSION_ELEMENT__EXP);

    stringExpressionEClass = createEClass(STRING_EXPRESSION);
    createEAttribute(stringExpressionEClass, STRING_EXPRESSION__NAME);

    genericAssociationEClass = createEClass(GENERIC_ASSOCIATION);
    createEReference(genericAssociationEClass, GENERIC_ASSOCIATION__EXP);

    postfixExpressionPostfixEClass = createEClass(POSTFIX_EXPRESSION_POSTFIX);

    nextAsignementEClass = createEClass(NEXT_ASIGNEMENT);
    createEReference(nextAsignementEClass, NEXT_ASIGNEMENT__RIGHT);

    compoundStatementEClass = createEClass(COMPOUND_STATEMENT);
    createEReference(compoundStatementEClass, COMPOUND_STATEMENT__INNER);

    blockItemEClass = createEClass(BLOCK_ITEM);

    statementsEClass = createEClass(STATEMENTS);

    labeledStatementEClass = createEClass(LABELED_STATEMENT);
    createEReference(labeledStatementEClass, LABELED_STATEMENT__STATEMENT);

    statementEClass = createEClass(STATEMENT);

    expressionStatementEClass = createEClass(EXPRESSION_STATEMENT);
    createEReference(expressionStatementEClass, EXPRESSION_STATEMENT__EXP);

    selectionStatementEClass = createEClass(SELECTION_STATEMENT);
    createEReference(selectionStatementEClass, SELECTION_STATEMENT__EXP);

    iterationStatementEClass = createEClass(ITERATION_STATEMENT);
    createEReference(iterationStatementEClass, ITERATION_STATEMENT__STATEMENT);

    nextFieldEClass = createEClass(NEXT_FIELD);
    createEReference(nextFieldEClass, NEXT_FIELD__NEXT);

    jumpStatementEClass = createEClass(JUMP_STATEMENT);

    completeNameEClass = createEClass(COMPLETE_NAME);
    createEAttribute(completeNameEClass, COMPLETE_NAME__EXT);

    simpleNameEClass = createEClass(SIMPLE_NAME);

    genericDeclaration0EClass = createEClass(GENERIC_DECLARATION0);
    createEAttribute(genericDeclaration0EClass, GENERIC_DECLARATION0__POINTERS);
    createEAttribute(genericDeclaration0EClass, GENERIC_DECLARATION0__RESTRICT);

    genericDeclaration1EClass = createEClass(GENERIC_DECLARATION1);

    genericDeclaration2EClass = createEClass(GENERIC_DECLARATION2);

    globalNameEClass = createEClass(GLOBAL_NAME);
    createEAttribute(globalNameEClass, GLOBAL_NAME__NAME);

    expressionInitializerEClass = createEClass(EXPRESSION_INITIALIZER);
    createEReference(expressionInitializerEClass, EXPRESSION_INITIALIZER__EXP);

    functionInitializerEClass = createEClass(FUNCTION_INITIALIZER);
    createEReference(functionInitializerEClass, FUNCTION_INITIALIZER__BLOCK);

    atomicEClass = createEClass(ATOMIC);
    createEReference(atomicEClass, ATOMIC__TYPE);

    baseTypeVoidEClass = createEClass(BASE_TYPE_VOID);

    customEClass = createEClass(CUSTOM);
    createEAttribute(customEClass, CUSTOM__ID);

    baseTypeCharEClass = createEClass(BASE_TYPE_CHAR);

    baseTypeShortEClass = createEClass(BASE_TYPE_SHORT);

    baseTypeIntEClass = createEClass(BASE_TYPE_INT);

    baseTypeLongEClass = createEClass(BASE_TYPE_LONG);

    baseTypeFloatEClass = createEClass(BASE_TYPE_FLOAT);

    baseTypeDoubleEClass = createEClass(BASE_TYPE_DOUBLE);

    baseTypeSignedEClass = createEClass(BASE_TYPE_SIGNED);

    baseTypeUnsignedEClass = createEClass(BASE_TYPE_UNSIGNED);

    baseTypeBoolEClass = createEClass(BASE_TYPE_BOOL);

    baseTypeComplexEClass = createEClass(BASE_TYPE_COMPLEX);

    baseTypeImaginaryEClass = createEClass(BASE_TYPE_IMAGINARY);

    storageClassSpecifierExternEClass = createEClass(STORAGE_CLASS_SPECIFIER_EXTERN);

    storageClassSpecifierStaticEClass = createEClass(STORAGE_CLASS_SPECIFIER_STATIC);

    storageClassSpecifierThreadLocalEClass = createEClass(STORAGE_CLASS_SPECIFIER_THREAD_LOCAL);

    storageClassSpecifierAutoEClass = createEClass(STORAGE_CLASS_SPECIFIER_AUTO);

    storageClassSpecifierRegisterEClass = createEClass(STORAGE_CLASS_SPECIFIER_REGISTER);

    typeQualifierConstEClass = createEClass(TYPE_QUALIFIER_CONST);

    typeQualifierVolatileEClass = createEClass(TYPE_QUALIFIER_VOLATILE);

    typeQualifierAtomicEClass = createEClass(TYPE_QUALIFIER_ATOMIC);

    functionSpecifierInlineEClass = createEClass(FUNCTION_SPECIFIER_INLINE);

    functionSpecifierNoReturnEClass = createEClass(FUNCTION_SPECIFIER_NO_RETURN);

    assignmentExpressionEClass = createEClass(ASSIGNMENT_EXPRESSION);
    createEReference(assignmentExpressionEClass, ASSIGNMENT_EXPRESSION__EXP);
    createEReference(assignmentExpressionEClass, ASSIGNMENT_EXPRESSION__LIST);

    conditionalExpressionEClass = createEClass(CONDITIONAL_EXPRESSION);
    createEReference(conditionalExpressionEClass, CONDITIONAL_EXPRESSION__IF);
    createEReference(conditionalExpressionEClass, CONDITIONAL_EXPRESSION__YES);
    createEReference(conditionalExpressionEClass, CONDITIONAL_EXPRESSION__NO);

    logicalOrExpressionEClass = createEClass(LOGICAL_OR_EXPRESSION);
    createEReference(logicalOrExpressionEClass, LOGICAL_OR_EXPRESSION__LEFT);
    createEAttribute(logicalOrExpressionEClass, LOGICAL_OR_EXPRESSION__OPERAND);
    createEReference(logicalOrExpressionEClass, LOGICAL_OR_EXPRESSION__RIGHT);

    logicalAndExpressionEClass = createEClass(LOGICAL_AND_EXPRESSION);
    createEReference(logicalAndExpressionEClass, LOGICAL_AND_EXPRESSION__LEFT);
    createEAttribute(logicalAndExpressionEClass, LOGICAL_AND_EXPRESSION__OPERAND);
    createEReference(logicalAndExpressionEClass, LOGICAL_AND_EXPRESSION__RIGHT);

    inclusiveOrExpressionEClass = createEClass(INCLUSIVE_OR_EXPRESSION);
    createEReference(inclusiveOrExpressionEClass, INCLUSIVE_OR_EXPRESSION__LEFT);
    createEAttribute(inclusiveOrExpressionEClass, INCLUSIVE_OR_EXPRESSION__OPERAND);
    createEReference(inclusiveOrExpressionEClass, INCLUSIVE_OR_EXPRESSION__RIGHT);

    exclusiveOrExpressionEClass = createEClass(EXCLUSIVE_OR_EXPRESSION);
    createEReference(exclusiveOrExpressionEClass, EXCLUSIVE_OR_EXPRESSION__LEFT);
    createEAttribute(exclusiveOrExpressionEClass, EXCLUSIVE_OR_EXPRESSION__OPERAND);
    createEReference(exclusiveOrExpressionEClass, EXCLUSIVE_OR_EXPRESSION__RIGHT);

    andExpressionEClass = createEClass(AND_EXPRESSION);
    createEReference(andExpressionEClass, AND_EXPRESSION__LEFT);
    createEAttribute(andExpressionEClass, AND_EXPRESSION__OPERAND);
    createEReference(andExpressionEClass, AND_EXPRESSION__RIGHT);

    equalityExpressionEClass = createEClass(EQUALITY_EXPRESSION);
    createEReference(equalityExpressionEClass, EQUALITY_EXPRESSION__LEFT);
    createEAttribute(equalityExpressionEClass, EQUALITY_EXPRESSION__OPERAND);
    createEReference(equalityExpressionEClass, EQUALITY_EXPRESSION__RIGHT);

    relationalExpressionEClass = createEClass(RELATIONAL_EXPRESSION);
    createEReference(relationalExpressionEClass, RELATIONAL_EXPRESSION__LEFT);
    createEAttribute(relationalExpressionEClass, RELATIONAL_EXPRESSION__OPERAND);
    createEReference(relationalExpressionEClass, RELATIONAL_EXPRESSION__RIGHT);

    shiftExpressionEClass = createEClass(SHIFT_EXPRESSION);
    createEReference(shiftExpressionEClass, SHIFT_EXPRESSION__LEFT);
    createEAttribute(shiftExpressionEClass, SHIFT_EXPRESSION__OPERAND);
    createEReference(shiftExpressionEClass, SHIFT_EXPRESSION__RIGHT);

    additiveExpressionEClass = createEClass(ADDITIVE_EXPRESSION);
    createEReference(additiveExpressionEClass, ADDITIVE_EXPRESSION__LEFT);
    createEAttribute(additiveExpressionEClass, ADDITIVE_EXPRESSION__OPERAND);
    createEReference(additiveExpressionEClass, ADDITIVE_EXPRESSION__RIGHT);

    multiplicativeExpressionEClass = createEClass(MULTIPLICATIVE_EXPRESSION);
    createEReference(multiplicativeExpressionEClass, MULTIPLICATIVE_EXPRESSION__LEFT);
    createEAttribute(multiplicativeExpressionEClass, MULTIPLICATIVE_EXPRESSION__OPERAND);
    createEReference(multiplicativeExpressionEClass, MULTIPLICATIVE_EXPRESSION__RIGHT);

    expressionSimpleEClass = createEClass(EXPRESSION_SIMPLE);
    createEReference(expressionSimpleEClass, EXPRESSION_SIMPLE__EXP);
    createEReference(expressionSimpleEClass, EXPRESSION_SIMPLE__NEXT);

    expressionComplexEClass = createEClass(EXPRESSION_COMPLEX);
    createEReference(expressionComplexEClass, EXPRESSION_COMPLEX__EXPS);
    createEReference(expressionComplexEClass, EXPRESSION_COMPLEX__NEXT);

    expressionIncrementEClass = createEClass(EXPRESSION_INCREMENT);
    createEReference(expressionIncrementEClass, EXPRESSION_INCREMENT__CONTENT);

    expressionDecrementEClass = createEClass(EXPRESSION_DECREMENT);
    createEReference(expressionDecrementEClass, EXPRESSION_DECREMENT__CONTENT);

    expressionSizeofEClass = createEClass(EXPRESSION_SIZEOF);
    createEReference(expressionSizeofEClass, EXPRESSION_SIZEOF__CONTENT);
    createEReference(expressionSizeofEClass, EXPRESSION_SIZEOF__TYPE);

    expressionUnaryOpEClass = createEClass(EXPRESSION_UNARY_OP);
    createEAttribute(expressionUnaryOpEClass, EXPRESSION_UNARY_OP__OP);
    createEReference(expressionUnaryOpEClass, EXPRESSION_UNARY_OP__CONTENT);

    expressionAlignEClass = createEClass(EXPRESSION_ALIGN);
    createEReference(expressionAlignEClass, EXPRESSION_ALIGN__TYPE);

    expressionCastEClass = createEClass(EXPRESSION_CAST);
    createEReference(expressionCastEClass, EXPRESSION_CAST__TYPE);
    createEReference(expressionCastEClass, EXPRESSION_CAST__EXP);

    expressionIdentifierEClass = createEClass(EXPRESSION_IDENTIFIER);
    createEAttribute(expressionIdentifierEClass, EXPRESSION_IDENTIFIER__NAME);

    expressionStringEClass = createEClass(EXPRESSION_STRING);
    createEReference(expressionStringEClass, EXPRESSION_STRING__VALUE);

    expressionIntegerEClass = createEClass(EXPRESSION_INTEGER);
    createEAttribute(expressionIntegerEClass, EXPRESSION_INTEGER__VALUE);

    expressionFloatEClass = createEClass(EXPRESSION_FLOAT);
    createEAttribute(expressionFloatEClass, EXPRESSION_FLOAT__VALUE);

    expressionSubExpressionEClass = createEClass(EXPRESSION_SUB_EXPRESSION);
    createEReference(expressionSubExpressionEClass, EXPRESSION_SUB_EXPRESSION__PAR);

    expressionGenericSelectionEClass = createEClass(EXPRESSION_GENERIC_SELECTION);
    createEReference(expressionGenericSelectionEClass, EXPRESSION_GENERIC_SELECTION__LEFT);
    createEReference(expressionGenericSelectionEClass, EXPRESSION_GENERIC_SELECTION__RIGHT);

    genericAssociationCaseEClass = createEClass(GENERIC_ASSOCIATION_CASE);
    createEReference(genericAssociationCaseEClass, GENERIC_ASSOCIATION_CASE__TYPE);

    genericAssociationDefaultEClass = createEClass(GENERIC_ASSOCIATION_DEFAULT);

    postfixExpressionPostfixExpressionEClass = createEClass(POSTFIX_EXPRESSION_POSTFIX_EXPRESSION);
    createEReference(postfixExpressionPostfixExpressionEClass, POSTFIX_EXPRESSION_POSTFIX_EXPRESSION__VALUE);

    postfixExpressionPostfixArgumentListEClass = createEClass(POSTFIX_EXPRESSION_POSTFIX_ARGUMENT_LIST);
    createEReference(postfixExpressionPostfixArgumentListEClass, POSTFIX_EXPRESSION_POSTFIX_ARGUMENT_LIST__LEFT);
    createEReference(postfixExpressionPostfixArgumentListEClass, POSTFIX_EXPRESSION_POSTFIX_ARGUMENT_LIST__NEXT);

    postfixExpressionPostfixAccessEClass = createEClass(POSTFIX_EXPRESSION_POSTFIX_ACCESS);
    createEAttribute(postfixExpressionPostfixAccessEClass, POSTFIX_EXPRESSION_POSTFIX_ACCESS__VALUE);

    postfixExpressionPostfixAccessPtrEClass = createEClass(POSTFIX_EXPRESSION_POSTFIX_ACCESS_PTR);
    createEAttribute(postfixExpressionPostfixAccessPtrEClass, POSTFIX_EXPRESSION_POSTFIX_ACCESS_PTR__VALUE);

    postfixExpressionPostfixIncrementEClass = createEClass(POSTFIX_EXPRESSION_POSTFIX_INCREMENT);

    postfixExpressionPostfixDecrementEClass = createEClass(POSTFIX_EXPRESSION_POSTFIX_DECREMENT);

    statementDeclarationEClass = createEClass(STATEMENT_DECLARATION);
    createEReference(statementDeclarationEClass, STATEMENT_DECLARATION__DECL);

    statementInnerStatementEClass = createEClass(STATEMENT_INNER_STATEMENT);
    createEReference(statementInnerStatementEClass, STATEMENT_INNER_STATEMENT__STATEMENT);

    labeledStatementCaseEClass = createEClass(LABELED_STATEMENT_CASE);
    createEReference(labeledStatementCaseEClass, LABELED_STATEMENT_CASE__EXP);

    labeledStatementDefaultEClass = createEClass(LABELED_STATEMENT_DEFAULT);

    goToPrefixStatementEClass = createEClass(GO_TO_PREFIX_STATEMENT);
    createEAttribute(goToPrefixStatementEClass, GO_TO_PREFIX_STATEMENT__GOTO);
    createEReference(goToPrefixStatementEClass, GO_TO_PREFIX_STATEMENT__STATEMENT);

    selectionStatementIfEClass = createEClass(SELECTION_STATEMENT_IF);
    createEReference(selectionStatementIfEClass, SELECTION_STATEMENT_IF__THEN_STATEMENT);
    createEReference(selectionStatementIfEClass, SELECTION_STATEMENT_IF__ELSE_STATEMENT);

    selectionStatementSwitchEClass = createEClass(SELECTION_STATEMENT_SWITCH);
    createEReference(selectionStatementSwitchEClass, SELECTION_STATEMENT_SWITCH__CONTENT);

    iterationStatementWhileEClass = createEClass(ITERATION_STATEMENT_WHILE);
    createEReference(iterationStatementWhileEClass, ITERATION_STATEMENT_WHILE__EXP);

    iterationStatementDoEClass = createEClass(ITERATION_STATEMENT_DO);
    createEReference(iterationStatementDoEClass, ITERATION_STATEMENT_DO__EXP);

    iterationStatementForEClass = createEClass(ITERATION_STATEMENT_FOR);
    createEReference(iterationStatementForEClass, ITERATION_STATEMENT_FOR__FIELD1);
    createEReference(iterationStatementForEClass, ITERATION_STATEMENT_FOR__DECL);
    createEReference(iterationStatementForEClass, ITERATION_STATEMENT_FOR__FIELD2);
    createEReference(iterationStatementForEClass, ITERATION_STATEMENT_FOR__FIELD3);
    createEReference(iterationStatementForEClass, ITERATION_STATEMENT_FOR__NEXT);

    jumpStatementGotoEClass = createEClass(JUMP_STATEMENT_GOTO);
    createEAttribute(jumpStatementGotoEClass, JUMP_STATEMENT_GOTO__NAME);

    jumpStatementContinueEClass = createEClass(JUMP_STATEMENT_CONTINUE);

    jumpStatementBreakEClass = createEClass(JUMP_STATEMENT_BREAK);

    jumpStatementReturnEClass = createEClass(JUMP_STATEMENT_RETURN);
    createEReference(jumpStatementReturnEClass, JUMP_STATEMENT_RETURN__EXP);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isInitialized = false;

  /**
   * Complete the initialization of the package and its meta-model.  This
   * method is guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void initializePackageContents()
  {
    if (isInitialized) return;
    isInitialized = true;

    // Initialize package
    setName(eNAME);
    setNsPrefix(eNS_PREFIX);
    setNsURI(eNS_URI);

    // Create type parameters

    // Set bounds for type parameters

    // Add supertypes to classes
    typeAliasDeclarationEClass.getESuperTypes().add(this.getDeclaration());
    genericSpecifiedDeclarationEClass.getESuperTypes().add(this.getDeclaration());
    staticAssertDeclarationEClass.getESuperTypes().add(this.getDeclaration());
    baseTypeEClass.getESuperTypes().add(this.getSimpleType());
    structOrUnionTypeEClass.getESuperTypes().add(this.getSimpleType());
    enumTypeEClass.getESuperTypes().add(this.getSimpleType());
    storageClassSpecifierEClass.getESuperTypes().add(this.getDeclarationSpecifier());
    typeQualifierEClass.getESuperTypes().add(this.getDeclarationSpecifier());
    functionSpecifierEClass.getESuperTypes().add(this.getDeclarationSpecifier());
    alignmentSpecifierEClass.getESuperTypes().add(this.getDeclarationSpecifier());
    compoundStatementEClass.getESuperTypes().add(this.getStatements());
    statementsEClass.getESuperTypes().add(this.getStatement());
    labeledStatementEClass.getESuperTypes().add(this.getStatements());
    expressionStatementEClass.getESuperTypes().add(this.getStatements());
    selectionStatementEClass.getESuperTypes().add(this.getStatements());
    iterationStatementEClass.getESuperTypes().add(this.getStatements());
    jumpStatementEClass.getESuperTypes().add(this.getStatements());
    completeNameEClass.getESuperTypes().add(this.getSource());
    simpleNameEClass.getESuperTypes().add(this.getSource());
    genericDeclaration0EClass.getESuperTypes().add(this.getGenericDeclarationId());
    genericDeclaration1EClass.getESuperTypes().add(this.getGenericDeclarationId());
    genericDeclaration2EClass.getESuperTypes().add(this.getGenericDeclarationId());
    globalNameEClass.getESuperTypes().add(this.getGenericDeclarationInnerId());
    expressionInitializerEClass.getESuperTypes().add(this.getGenericDeclarationInitializer());
    functionInitializerEClass.getESuperTypes().add(this.getGenericDeclarationInitializer());
    atomicEClass.getESuperTypes().add(this.getSimpleType());
    baseTypeVoidEClass.getESuperTypes().add(this.getBaseType());
    customEClass.getESuperTypes().add(this.getBaseType());
    baseTypeCharEClass.getESuperTypes().add(this.getBaseType());
    baseTypeShortEClass.getESuperTypes().add(this.getBaseType());
    baseTypeIntEClass.getESuperTypes().add(this.getBaseType());
    baseTypeLongEClass.getESuperTypes().add(this.getBaseType());
    baseTypeFloatEClass.getESuperTypes().add(this.getBaseType());
    baseTypeDoubleEClass.getESuperTypes().add(this.getBaseType());
    baseTypeSignedEClass.getESuperTypes().add(this.getBaseType());
    baseTypeUnsignedEClass.getESuperTypes().add(this.getBaseType());
    baseTypeBoolEClass.getESuperTypes().add(this.getBaseType());
    baseTypeComplexEClass.getESuperTypes().add(this.getBaseType());
    baseTypeImaginaryEClass.getESuperTypes().add(this.getBaseType());
    storageClassSpecifierExternEClass.getESuperTypes().add(this.getStorageClassSpecifier());
    storageClassSpecifierStaticEClass.getESuperTypes().add(this.getStorageClassSpecifier());
    storageClassSpecifierThreadLocalEClass.getESuperTypes().add(this.getStorageClassSpecifier());
    storageClassSpecifierAutoEClass.getESuperTypes().add(this.getStorageClassSpecifier());
    storageClassSpecifierRegisterEClass.getESuperTypes().add(this.getStorageClassSpecifier());
    typeQualifierConstEClass.getESuperTypes().add(this.getTypeQualifier());
    typeQualifierVolatileEClass.getESuperTypes().add(this.getTypeQualifier());
    typeQualifierAtomicEClass.getESuperTypes().add(this.getTypeQualifier());
    functionSpecifierInlineEClass.getESuperTypes().add(this.getFunctionSpecifier());
    functionSpecifierNoReturnEClass.getESuperTypes().add(this.getFunctionSpecifier());
    assignmentExpressionEClass.getESuperTypes().add(this.getExpression());
    conditionalExpressionEClass.getESuperTypes().add(this.getExpression());
    logicalOrExpressionEClass.getESuperTypes().add(this.getExpression());
    logicalAndExpressionEClass.getESuperTypes().add(this.getExpression());
    inclusiveOrExpressionEClass.getESuperTypes().add(this.getExpression());
    exclusiveOrExpressionEClass.getESuperTypes().add(this.getExpression());
    andExpressionEClass.getESuperTypes().add(this.getExpression());
    equalityExpressionEClass.getESuperTypes().add(this.getExpression());
    relationalExpressionEClass.getESuperTypes().add(this.getExpression());
    shiftExpressionEClass.getESuperTypes().add(this.getExpression());
    additiveExpressionEClass.getESuperTypes().add(this.getExpression());
    multiplicativeExpressionEClass.getESuperTypes().add(this.getExpression());
    expressionSimpleEClass.getESuperTypes().add(this.getExpression());
    expressionComplexEClass.getESuperTypes().add(this.getExpression());
    expressionIncrementEClass.getESuperTypes().add(this.getExpression());
    expressionDecrementEClass.getESuperTypes().add(this.getExpression());
    expressionSizeofEClass.getESuperTypes().add(this.getExpression());
    expressionUnaryOpEClass.getESuperTypes().add(this.getExpression());
    expressionAlignEClass.getESuperTypes().add(this.getExpression());
    expressionCastEClass.getESuperTypes().add(this.getExpression());
    expressionIdentifierEClass.getESuperTypes().add(this.getExpression());
    expressionStringEClass.getESuperTypes().add(this.getExpression());
    expressionIntegerEClass.getESuperTypes().add(this.getExpression());
    expressionFloatEClass.getESuperTypes().add(this.getExpression());
    expressionSubExpressionEClass.getESuperTypes().add(this.getExpression());
    expressionGenericSelectionEClass.getESuperTypes().add(this.getExpression());
    genericAssociationCaseEClass.getESuperTypes().add(this.getGenericAssociation());
    genericAssociationDefaultEClass.getESuperTypes().add(this.getGenericAssociation());
    postfixExpressionPostfixExpressionEClass.getESuperTypes().add(this.getPostfixExpressionPostfix());
    postfixExpressionPostfixArgumentListEClass.getESuperTypes().add(this.getPostfixExpressionPostfix());
    postfixExpressionPostfixAccessEClass.getESuperTypes().add(this.getPostfixExpressionPostfix());
    postfixExpressionPostfixAccessPtrEClass.getESuperTypes().add(this.getPostfixExpressionPostfix());
    postfixExpressionPostfixIncrementEClass.getESuperTypes().add(this.getPostfixExpressionPostfix());
    postfixExpressionPostfixDecrementEClass.getESuperTypes().add(this.getPostfixExpressionPostfix());
    statementDeclarationEClass.getESuperTypes().add(this.getBlockItem());
    statementInnerStatementEClass.getESuperTypes().add(this.getBlockItem());
    labeledStatementCaseEClass.getESuperTypes().add(this.getLabeledStatement());
    labeledStatementDefaultEClass.getESuperTypes().add(this.getLabeledStatement());
    goToPrefixStatementEClass.getESuperTypes().add(this.getStatement());
    selectionStatementIfEClass.getESuperTypes().add(this.getSelectionStatement());
    selectionStatementSwitchEClass.getESuperTypes().add(this.getSelectionStatement());
    iterationStatementWhileEClass.getESuperTypes().add(this.getIterationStatement());
    iterationStatementDoEClass.getESuperTypes().add(this.getIterationStatement());
    iterationStatementForEClass.getESuperTypes().add(this.getIterationStatement());
    jumpStatementGotoEClass.getESuperTypes().add(this.getJumpStatement());
    jumpStatementContinueEClass.getESuperTypes().add(this.getJumpStatement());
    jumpStatementBreakEClass.getESuperTypes().add(this.getJumpStatement());
    jumpStatementReturnEClass.getESuperTypes().add(this.getJumpStatement());

    // Initialize classes and features; add operations and parameters
    initEClass(modelEClass, Model.class, "Model", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getModel_IncludesStd(), this.getStandardInclude(), null, "includesStd", null, 0, -1, Model.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getModel_IncludesFile(), this.getFileInclude(), null, "includesFile", null, 0, -1, Model.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getModel_Declarations(), this.getDeclaration(), null, "declarations", null, 0, -1, Model.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(standardIncludeEClass, StandardInclude.class, "StandardInclude", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getStandardInclude_Std(), this.getSource(), null, "std", null, 0, 1, StandardInclude.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getStandardInclude_Wild(), ecorePackage.getEBoolean(), "wild", null, 0, 1, StandardInclude.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(fileIncludeEClass, FileInclude.class, "FileInclude", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getFileInclude_File(), ecorePackage.getEString(), "file", null, 0, 1, FileInclude.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(sourceEClass, Source.class, "Source", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getSource_Name(), ecorePackage.getEString(), "name", null, 0, 1, Source.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(declarationEClass, Declaration.class, "Declaration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(typeAliasDeclarationEClass, TypeAliasDeclaration.class, "TypeAliasDeclaration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getTypeAliasDeclaration_Tdecl(), this.getSpecifiedType(), null, "tdecl", null, 0, 1, TypeAliasDeclaration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(genericSpecifiedDeclarationEClass, GenericSpecifiedDeclaration.class, "GenericSpecifiedDeclaration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getGenericSpecifiedDeclaration_Specs(), this.getDeclarationSpecifier(), null, "specs", null, 0, -1, GenericSpecifiedDeclaration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getGenericSpecifiedDeclaration_Type(), this.getSimpleType(), null, "type", null, 0, -1, GenericSpecifiedDeclaration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getGenericSpecifiedDeclaration_Decl(), this.getGenericDeclarationElements(), null, "decl", null, 0, 1, GenericSpecifiedDeclaration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(genericDeclarationElementsEClass, GenericDeclarationElements.class, "GenericDeclarationElements", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getGenericDeclarationElements_Els(), this.getGenericDeclarationElement(), null, "els", null, 0, -1, GenericDeclarationElements.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getGenericDeclarationElements_Next(), this.getGenericDeclarationElements(), null, "next", null, 0, 1, GenericDeclarationElements.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(genericDeclarationElementEClass, GenericDeclarationElement.class, "GenericDeclarationElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getGenericDeclarationElement_Id(), this.getGenericDeclarationId(), null, "id", null, 0, 1, GenericDeclarationElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getGenericDeclarationElement_Init(), this.getGenericDeclarationInitializer(), null, "init", null, 0, 1, GenericDeclarationElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(genericDeclarationIdEClass, GenericDeclarationId.class, "GenericDeclarationId", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getGenericDeclarationId_Inner(), this.getGenericDeclarationInnerId(), null, "inner", null, 0, 1, GenericDeclarationId.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getGenericDeclarationId_Posts(), this.getGenericDeclarationIdPostfix(), null, "posts", null, 0, -1, GenericDeclarationId.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(genericDeclarationInnerIdEClass, GenericDeclarationInnerId.class, "GenericDeclarationInnerId", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getGenericDeclarationInnerId_Inner(), this.getGenericDeclarationId(), null, "inner", null, 0, 1, GenericDeclarationInnerId.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(genericDeclarationIdPostfixEClass, GenericDeclarationIdPostfix.class, "GenericDeclarationIdPostfix", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getGenericDeclarationIdPostfix_Array(), this.getArrayPostfix(), null, "array", null, 0, 1, GenericDeclarationIdPostfix.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getGenericDeclarationIdPostfix_Function(), this.getParametersPostfix(), null, "function", null, 0, 1, GenericDeclarationIdPostfix.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(arrayPostfixEClass, ArrayPostfix.class, "ArrayPostfix", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getArrayPostfix_Star(), ecorePackage.getEString(), "star", null, 0, 1, ArrayPostfix.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getArrayPostfix_Exp(), this.getExpression(), null, "exp", null, 0, 1, ArrayPostfix.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(parametersPostfixEClass, ParametersPostfix.class, "ParametersPostfix", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getParametersPostfix_Els(), this.getParameter(), null, "els", null, 0, -1, ParametersPostfix.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getParametersPostfix_Next(), this.getNextParameter(), null, "next", null, 0, -1, ParametersPostfix.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getParametersPostfix_More(), ecorePackage.getEString(), "more", null, 0, 1, ParametersPostfix.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(nextParameterEClass, NextParameter.class, "NextParameter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getNextParameter_Els(), this.getParameter(), null, "els", null, 0, -1, NextParameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(parameterEClass, Parameter.class, "Parameter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getParameter_Specs(), this.getDeclarationSpecifier(), null, "specs", null, 0, -1, Parameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getParameter_Type(), this.getSimpleType(), null, "type", null, 0, -1, Parameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getParameter_Id(), this.getGenericDeclarationId(), null, "id", null, 0, 1, Parameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(genericDeclarationInitializerEClass, GenericDeclarationInitializer.class, "GenericDeclarationInitializer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(specifiedTypeEClass, SpecifiedType.class, "SpecifiedType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getSpecifiedType_Par(), this.getParameter(), null, "par", null, 0, 1, SpecifiedType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(staticAssertDeclarationEClass, StaticAssertDeclaration.class, "StaticAssertDeclaration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getStaticAssertDeclaration_Exp(), this.getExpression(), null, "exp", null, 0, 1, StaticAssertDeclaration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getStaticAssertDeclaration_String(), ecorePackage.getEString(), "string", null, 0, 1, StaticAssertDeclaration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(simpleTypeEClass, SimpleType.class, "SimpleType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(baseTypeEClass, BaseType.class, "BaseType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(structOrUnionTypeEClass, StructOrUnionType.class, "StructOrUnionType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getStructOrUnionType_Kind(), ecorePackage.getEString(), "kind", null, 0, 1, StructOrUnionType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getStructOrUnionType_Content(), this.getGenericSpecifiedDeclaration(), null, "content", null, 0, -1, StructOrUnionType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getStructOrUnionType_Name(), ecorePackage.getEString(), "name", null, 0, 1, StructOrUnionType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(enumTypeEClass, EnumType.class, "EnumType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getEnumType_Content(), this.getEnumeratorList(), null, "content", null, 0, 1, EnumType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getEnumType_Name(), ecorePackage.getEString(), "name", null, 0, 1, EnumType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(enumeratorListEClass, EnumeratorList.class, "EnumeratorList", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getEnumeratorList_Elements(), this.getEnumerator(), null, "elements", null, 0, -1, EnumeratorList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(enumeratorEClass, Enumerator.class, "Enumerator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getEnumerator_Name(), this.getEnumerationConstant(), null, "name", null, 0, 1, Enumerator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getEnumerator_Exp(), this.getExpression(), null, "exp", null, 0, 1, Enumerator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(enumerationConstantEClass, EnumerationConstant.class, "EnumerationConstant", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getEnumerationConstant_Name(), ecorePackage.getEString(), "name", null, 0, 1, EnumerationConstant.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(declarationSpecifierEClass, DeclarationSpecifier.class, "DeclarationSpecifier", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(storageClassSpecifierEClass, StorageClassSpecifier.class, "StorageClassSpecifier", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(typeQualifierEClass, TypeQualifier.class, "TypeQualifier", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(functionSpecifierEClass, FunctionSpecifier.class, "FunctionSpecifier", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(alignmentSpecifierEClass, AlignmentSpecifier.class, "AlignmentSpecifier", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getAlignmentSpecifier_Type(), this.getSimpleType(), null, "type", null, 0, 1, AlignmentSpecifier.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getAlignmentSpecifier_Exp(), this.getExpression(), null, "exp", null, 0, 1, AlignmentSpecifier.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(expressionEClass, Expression.class, "Expression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(assignmentExpressionElementEClass, AssignmentExpressionElement.class, "AssignmentExpressionElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getAssignmentExpressionElement_Op(), ecorePackage.getEString(), "op", null, 0, 1, AssignmentExpressionElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getAssignmentExpressionElement_Exp(), this.getExpression(), null, "exp", null, 0, 1, AssignmentExpressionElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(stringExpressionEClass, StringExpression.class, "StringExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getStringExpression_Name(), ecorePackage.getEString(), "name", null, 0, 1, StringExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(genericAssociationEClass, GenericAssociation.class, "GenericAssociation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getGenericAssociation_Exp(), this.getExpression(), null, "exp", null, 0, 1, GenericAssociation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(postfixExpressionPostfixEClass, PostfixExpressionPostfix.class, "PostfixExpressionPostfix", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(nextAsignementEClass, NextAsignement.class, "NextAsignement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getNextAsignement_Right(), this.getExpression(), null, "right", null, 0, -1, NextAsignement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(compoundStatementEClass, CompoundStatement.class, "CompoundStatement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getCompoundStatement_Inner(), this.getBlockItem(), null, "inner", null, 0, -1, CompoundStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(blockItemEClass, BlockItem.class, "BlockItem", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(statementsEClass, Statements.class, "Statements", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(labeledStatementEClass, LabeledStatement.class, "LabeledStatement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getLabeledStatement_Statement(), this.getStatement(), null, "statement", null, 0, 1, LabeledStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(statementEClass, Statement.class, "Statement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(expressionStatementEClass, ExpressionStatement.class, "ExpressionStatement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getExpressionStatement_Exp(), this.getExpression(), null, "exp", null, 0, 1, ExpressionStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(selectionStatementEClass, SelectionStatement.class, "SelectionStatement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getSelectionStatement_Exp(), this.getExpression(), null, "exp", null, 0, 1, SelectionStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(iterationStatementEClass, IterationStatement.class, "IterationStatement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getIterationStatement_Statement(), this.getStatement(), null, "statement", null, 0, 1, IterationStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(nextFieldEClass, NextField.class, "NextField", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getNextField_Next(), this.getExpression(), null, "next", null, 0, 1, NextField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(jumpStatementEClass, JumpStatement.class, "JumpStatement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(completeNameEClass, CompleteName.class, "CompleteName", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getCompleteName_Ext(), ecorePackage.getEString(), "ext", null, 0, 1, CompleteName.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(simpleNameEClass, SimpleName.class, "SimpleName", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(genericDeclaration0EClass, GenericDeclaration0.class, "GenericDeclaration0", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getGenericDeclaration0_Pointers(), ecorePackage.getEString(), "pointers", null, 0, -1, GenericDeclaration0.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getGenericDeclaration0_Restrict(), ecorePackage.getEString(), "restrict", null, 0, 1, GenericDeclaration0.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(genericDeclaration1EClass, GenericDeclaration1.class, "GenericDeclaration1", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(genericDeclaration2EClass, GenericDeclaration2.class, "GenericDeclaration2", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(globalNameEClass, GlobalName.class, "GlobalName", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getGlobalName_Name(), ecorePackage.getEString(), "name", null, 0, 1, GlobalName.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(expressionInitializerEClass, ExpressionInitializer.class, "ExpressionInitializer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getExpressionInitializer_Exp(), this.getExpression(), null, "exp", null, 0, 1, ExpressionInitializer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(functionInitializerEClass, FunctionInitializer.class, "FunctionInitializer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getFunctionInitializer_Block(), this.getCompoundStatement(), null, "block", null, 0, 1, FunctionInitializer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(atomicEClass, Atomic.class, "Atomic", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getAtomic_Type(), this.getSimpleType(), null, "type", null, 0, 1, Atomic.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(baseTypeVoidEClass, BaseTypeVoid.class, "BaseTypeVoid", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(customEClass, Custom.class, "Custom", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getCustom_Id(), ecorePackage.getEString(), "id", null, 0, 1, Custom.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(baseTypeCharEClass, BaseTypeChar.class, "BaseTypeChar", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(baseTypeShortEClass, BaseTypeShort.class, "BaseTypeShort", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(baseTypeIntEClass, BaseTypeInt.class, "BaseTypeInt", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(baseTypeLongEClass, BaseTypeLong.class, "BaseTypeLong", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(baseTypeFloatEClass, BaseTypeFloat.class, "BaseTypeFloat", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(baseTypeDoubleEClass, BaseTypeDouble.class, "BaseTypeDouble", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(baseTypeSignedEClass, BaseTypeSigned.class, "BaseTypeSigned", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(baseTypeUnsignedEClass, BaseTypeUnsigned.class, "BaseTypeUnsigned", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(baseTypeBoolEClass, BaseTypeBool.class, "BaseTypeBool", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(baseTypeComplexEClass, BaseTypeComplex.class, "BaseTypeComplex", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(baseTypeImaginaryEClass, BaseTypeImaginary.class, "BaseTypeImaginary", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(storageClassSpecifierExternEClass, StorageClassSpecifierExtern.class, "StorageClassSpecifierExtern", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(storageClassSpecifierStaticEClass, StorageClassSpecifierStatic.class, "StorageClassSpecifierStatic", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(storageClassSpecifierThreadLocalEClass, StorageClassSpecifierThreadLocal.class, "StorageClassSpecifierThreadLocal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(storageClassSpecifierAutoEClass, StorageClassSpecifierAuto.class, "StorageClassSpecifierAuto", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(storageClassSpecifierRegisterEClass, StorageClassSpecifierRegister.class, "StorageClassSpecifierRegister", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(typeQualifierConstEClass, TypeQualifierConst.class, "TypeQualifierConst", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(typeQualifierVolatileEClass, TypeQualifierVolatile.class, "TypeQualifierVolatile", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(typeQualifierAtomicEClass, TypeQualifierAtomic.class, "TypeQualifierAtomic", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(functionSpecifierInlineEClass, FunctionSpecifierInline.class, "FunctionSpecifierInline", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(functionSpecifierNoReturnEClass, FunctionSpecifierNoReturn.class, "FunctionSpecifierNoReturn", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(assignmentExpressionEClass, AssignmentExpression.class, "AssignmentExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getAssignmentExpression_Exp(), this.getExpression(), null, "exp", null, 0, 1, AssignmentExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getAssignmentExpression_List(), this.getAssignmentExpressionElement(), null, "list", null, 0, -1, AssignmentExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(conditionalExpressionEClass, ConditionalExpression.class, "ConditionalExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getConditionalExpression_If(), this.getExpression(), null, "if", null, 0, 1, ConditionalExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getConditionalExpression_Yes(), this.getExpression(), null, "yes", null, 0, 1, ConditionalExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getConditionalExpression_No(), this.getExpression(), null, "no", null, 0, 1, ConditionalExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(logicalOrExpressionEClass, LogicalOrExpression.class, "LogicalOrExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getLogicalOrExpression_Left(), this.getExpression(), null, "left", null, 0, 1, LogicalOrExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getLogicalOrExpression_Operand(), ecorePackage.getEString(), "operand", null, 0, 1, LogicalOrExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getLogicalOrExpression_Right(), this.getExpression(), null, "right", null, 0, 1, LogicalOrExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(logicalAndExpressionEClass, LogicalAndExpression.class, "LogicalAndExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getLogicalAndExpression_Left(), this.getExpression(), null, "left", null, 0, 1, LogicalAndExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getLogicalAndExpression_Operand(), ecorePackage.getEString(), "operand", null, 0, 1, LogicalAndExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getLogicalAndExpression_Right(), this.getExpression(), null, "right", null, 0, 1, LogicalAndExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(inclusiveOrExpressionEClass, InclusiveOrExpression.class, "InclusiveOrExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getInclusiveOrExpression_Left(), this.getExpression(), null, "left", null, 0, 1, InclusiveOrExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getInclusiveOrExpression_Operand(), ecorePackage.getEString(), "operand", null, 0, 1, InclusiveOrExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getInclusiveOrExpression_Right(), this.getExpression(), null, "right", null, 0, 1, InclusiveOrExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(exclusiveOrExpressionEClass, ExclusiveOrExpression.class, "ExclusiveOrExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getExclusiveOrExpression_Left(), this.getExpression(), null, "left", null, 0, 1, ExclusiveOrExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getExclusiveOrExpression_Operand(), ecorePackage.getEString(), "operand", null, 0, 1, ExclusiveOrExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getExclusiveOrExpression_Right(), this.getExpression(), null, "right", null, 0, 1, ExclusiveOrExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(andExpressionEClass, AndExpression.class, "AndExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getAndExpression_Left(), this.getExpression(), null, "left", null, 0, 1, AndExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getAndExpression_Operand(), ecorePackage.getEString(), "operand", null, 0, 1, AndExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getAndExpression_Right(), this.getExpression(), null, "right", null, 0, 1, AndExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(equalityExpressionEClass, EqualityExpression.class, "EqualityExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getEqualityExpression_Left(), this.getExpression(), null, "left", null, 0, 1, EqualityExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getEqualityExpression_Operand(), ecorePackage.getEString(), "operand", null, 0, 1, EqualityExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getEqualityExpression_Right(), this.getExpression(), null, "right", null, 0, 1, EqualityExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(relationalExpressionEClass, RelationalExpression.class, "RelationalExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getRelationalExpression_Left(), this.getExpression(), null, "left", null, 0, 1, RelationalExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getRelationalExpression_Operand(), ecorePackage.getEString(), "operand", null, 0, 1, RelationalExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getRelationalExpression_Right(), this.getExpression(), null, "right", null, 0, 1, RelationalExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(shiftExpressionEClass, ShiftExpression.class, "ShiftExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getShiftExpression_Left(), this.getExpression(), null, "left", null, 0, 1, ShiftExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getShiftExpression_Operand(), ecorePackage.getEString(), "operand", null, 0, 1, ShiftExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getShiftExpression_Right(), this.getExpression(), null, "right", null, 0, 1, ShiftExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(additiveExpressionEClass, AdditiveExpression.class, "AdditiveExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getAdditiveExpression_Left(), this.getExpression(), null, "left", null, 0, 1, AdditiveExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getAdditiveExpression_Operand(), ecorePackage.getEString(), "operand", null, 0, 1, AdditiveExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getAdditiveExpression_Right(), this.getExpression(), null, "right", null, 0, 1, AdditiveExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(multiplicativeExpressionEClass, MultiplicativeExpression.class, "MultiplicativeExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getMultiplicativeExpression_Left(), this.getExpression(), null, "left", null, 0, 1, MultiplicativeExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getMultiplicativeExpression_Operand(), ecorePackage.getEString(), "operand", null, 0, 1, MultiplicativeExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getMultiplicativeExpression_Right(), this.getExpression(), null, "right", null, 0, 1, MultiplicativeExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(expressionSimpleEClass, ExpressionSimple.class, "ExpressionSimple", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getExpressionSimple_Exp(), this.getExpression(), null, "exp", null, 0, 1, ExpressionSimple.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getExpressionSimple_Next(), this.getPostfixExpressionPostfix(), null, "next", null, 0, -1, ExpressionSimple.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(expressionComplexEClass, ExpressionComplex.class, "ExpressionComplex", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getExpressionComplex_Exps(), this.getExpression(), null, "exps", null, 0, -1, ExpressionComplex.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getExpressionComplex_Next(), this.getPostfixExpressionPostfix(), null, "next", null, 0, -1, ExpressionComplex.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(expressionIncrementEClass, ExpressionIncrement.class, "ExpressionIncrement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getExpressionIncrement_Content(), this.getExpression(), null, "content", null, 0, 1, ExpressionIncrement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(expressionDecrementEClass, ExpressionDecrement.class, "ExpressionDecrement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getExpressionDecrement_Content(), this.getExpression(), null, "content", null, 0, 1, ExpressionDecrement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(expressionSizeofEClass, ExpressionSizeof.class, "ExpressionSizeof", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getExpressionSizeof_Content(), this.getExpression(), null, "content", null, 0, 1, ExpressionSizeof.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getExpressionSizeof_Type(), this.getSpecifiedType(), null, "type", null, 0, 1, ExpressionSizeof.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(expressionUnaryOpEClass, ExpressionUnaryOp.class, "ExpressionUnaryOp", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getExpressionUnaryOp_Op(), ecorePackage.getEString(), "op", null, 0, 1, ExpressionUnaryOp.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getExpressionUnaryOp_Content(), this.getExpression(), null, "content", null, 0, 1, ExpressionUnaryOp.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(expressionAlignEClass, ExpressionAlign.class, "ExpressionAlign", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getExpressionAlign_Type(), this.getSpecifiedType(), null, "type", null, 0, 1, ExpressionAlign.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(expressionCastEClass, ExpressionCast.class, "ExpressionCast", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getExpressionCast_Type(), this.getSpecifiedType(), null, "type", null, 0, 1, ExpressionCast.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getExpressionCast_Exp(), this.getExpression(), null, "exp", null, 0, 1, ExpressionCast.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(expressionIdentifierEClass, ExpressionIdentifier.class, "ExpressionIdentifier", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getExpressionIdentifier_Name(), ecorePackage.getEString(), "name", null, 0, 1, ExpressionIdentifier.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(expressionStringEClass, ExpressionString.class, "ExpressionString", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getExpressionString_Value(), this.getStringExpression(), null, "value", null, 0, 1, ExpressionString.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(expressionIntegerEClass, ExpressionInteger.class, "ExpressionInteger", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getExpressionInteger_Value(), ecorePackage.getEString(), "value", null, 0, 1, ExpressionInteger.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(expressionFloatEClass, ExpressionFloat.class, "ExpressionFloat", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getExpressionFloat_Value(), ecorePackage.getEString(), "value", null, 0, 1, ExpressionFloat.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(expressionSubExpressionEClass, ExpressionSubExpression.class, "ExpressionSubExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getExpressionSubExpression_Par(), this.getExpression(), null, "par", null, 0, 1, ExpressionSubExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(expressionGenericSelectionEClass, ExpressionGenericSelection.class, "ExpressionGenericSelection", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getExpressionGenericSelection_Left(), this.getExpression(), null, "left", null, 0, 1, ExpressionGenericSelection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getExpressionGenericSelection_Right(), this.getGenericAssociation(), null, "right", null, 0, -1, ExpressionGenericSelection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(genericAssociationCaseEClass, GenericAssociationCase.class, "GenericAssociationCase", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getGenericAssociationCase_Type(), this.getSpecifiedType(), null, "type", null, 0, 1, GenericAssociationCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(genericAssociationDefaultEClass, GenericAssociationDefault.class, "GenericAssociationDefault", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(postfixExpressionPostfixExpressionEClass, PostfixExpressionPostfixExpression.class, "PostfixExpressionPostfixExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getPostfixExpressionPostfixExpression_Value(), this.getExpression(), null, "value", null, 0, 1, PostfixExpressionPostfixExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(postfixExpressionPostfixArgumentListEClass, PostfixExpressionPostfixArgumentList.class, "PostfixExpressionPostfixArgumentList", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getPostfixExpressionPostfixArgumentList_Left(), this.getExpression(), null, "left", null, 0, -1, PostfixExpressionPostfixArgumentList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getPostfixExpressionPostfixArgumentList_Next(), this.getNextAsignement(), null, "next", null, 0, -1, PostfixExpressionPostfixArgumentList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(postfixExpressionPostfixAccessEClass, PostfixExpressionPostfixAccess.class, "PostfixExpressionPostfixAccess", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getPostfixExpressionPostfixAccess_Value(), ecorePackage.getEString(), "value", null, 0, 1, PostfixExpressionPostfixAccess.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(postfixExpressionPostfixAccessPtrEClass, PostfixExpressionPostfixAccessPtr.class, "PostfixExpressionPostfixAccessPtr", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getPostfixExpressionPostfixAccessPtr_Value(), ecorePackage.getEString(), "value", null, 0, 1, PostfixExpressionPostfixAccessPtr.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(postfixExpressionPostfixIncrementEClass, PostfixExpressionPostfixIncrement.class, "PostfixExpressionPostfixIncrement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(postfixExpressionPostfixDecrementEClass, PostfixExpressionPostfixDecrement.class, "PostfixExpressionPostfixDecrement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(statementDeclarationEClass, StatementDeclaration.class, "StatementDeclaration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getStatementDeclaration_Decl(), this.getDeclaration(), null, "decl", null, 0, 1, StatementDeclaration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(statementInnerStatementEClass, StatementInnerStatement.class, "StatementInnerStatement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getStatementInnerStatement_Statement(), this.getStatement(), null, "statement", null, 0, 1, StatementInnerStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(labeledStatementCaseEClass, LabeledStatementCase.class, "LabeledStatementCase", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getLabeledStatementCase_Exp(), this.getExpression(), null, "exp", null, 0, 1, LabeledStatementCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(labeledStatementDefaultEClass, LabeledStatementDefault.class, "LabeledStatementDefault", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(goToPrefixStatementEClass, GoToPrefixStatement.class, "GoToPrefixStatement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getGoToPrefixStatement_Goto(), ecorePackage.getEString(), "goto", null, 0, 1, GoToPrefixStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getGoToPrefixStatement_Statement(), this.getStatement(), null, "statement", null, 0, 1, GoToPrefixStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(selectionStatementIfEClass, SelectionStatementIf.class, "SelectionStatementIf", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getSelectionStatementIf_Then_statement(), this.getStatement(), null, "then_statement", null, 0, 1, SelectionStatementIf.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getSelectionStatementIf_Else_statement(), this.getStatement(), null, "else_statement", null, 0, 1, SelectionStatementIf.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(selectionStatementSwitchEClass, SelectionStatementSwitch.class, "SelectionStatementSwitch", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getSelectionStatementSwitch_Content(), this.getLabeledStatement(), null, "content", null, 0, -1, SelectionStatementSwitch.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(iterationStatementWhileEClass, IterationStatementWhile.class, "IterationStatementWhile", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getIterationStatementWhile_Exp(), this.getExpression(), null, "exp", null, 0, 1, IterationStatementWhile.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(iterationStatementDoEClass, IterationStatementDo.class, "IterationStatementDo", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getIterationStatementDo_Exp(), this.getExpression(), null, "exp", null, 0, 1, IterationStatementDo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(iterationStatementForEClass, IterationStatementFor.class, "IterationStatementFor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getIterationStatementFor_Field1(), this.getExpressionStatement(), null, "field1", null, 0, 1, IterationStatementFor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getIterationStatementFor_Decl(), this.getDeclaration(), null, "decl", null, 0, 1, IterationStatementFor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getIterationStatementFor_Field2(), this.getExpressionStatement(), null, "field2", null, 0, 1, IterationStatementFor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getIterationStatementFor_Field3(), this.getExpression(), null, "field3", null, 0, 1, IterationStatementFor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getIterationStatementFor_Next(), this.getNextField(), null, "next", null, 0, -1, IterationStatementFor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(jumpStatementGotoEClass, JumpStatementGoto.class, "JumpStatementGoto", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getJumpStatementGoto_Name(), ecorePackage.getEString(), "name", null, 0, 1, JumpStatementGoto.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(jumpStatementContinueEClass, JumpStatementContinue.class, "JumpStatementContinue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(jumpStatementBreakEClass, JumpStatementBreak.class, "JumpStatementBreak", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(jumpStatementReturnEClass, JumpStatementReturn.class, "JumpStatementReturn", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getJumpStatementReturn_Exp(), this.getExpression(), null, "exp", null, 0, 1, JumpStatementReturn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    // Create resource
    createResource(eNS_URI);
  }

} //ClangPackageImpl
