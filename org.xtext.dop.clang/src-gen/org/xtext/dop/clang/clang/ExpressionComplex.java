/**
 */
package org.xtext.dop.clang.clang;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Expression Complex</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.clang.clang.ExpressionComplex#getExps <em>Exps</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.ExpressionComplex#getNext <em>Next</em>}</li>
 * </ul>
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getExpressionComplex()
 * @model
 * @generated
 */
public interface ExpressionComplex extends Expression
{
  /**
   * Returns the value of the '<em><b>Exps</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.dop.clang.clang.Expression}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Exps</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Exps</em>' containment reference list.
   * @see org.xtext.dop.clang.clang.ClangPackage#getExpressionComplex_Exps()
   * @model containment="true"
   * @generated
   */
  EList<Expression> getExps();

  /**
   * Returns the value of the '<em><b>Next</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.dop.clang.clang.PostfixExpressionPostfix}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Next</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Next</em>' containment reference list.
   * @see org.xtext.dop.clang.clang.ClangPackage#getExpressionComplex_Next()
   * @model containment="true"
   * @generated
   */
  EList<PostfixExpressionPostfix> getNext();

} // ExpressionComplex
