package org.xtext.dop.productdeclaration.tests

import org.eclipse.xtext.junit4.InjectWith
import org.junit.runner.RunWith
import org.eclipse.xtext.junit4.XtextRunner
import org.xtext.dop.productdeclaration.ProductDeclarationInjectorProvider
import com.google.inject.Inject
import org.eclipse.xtext.junit4.validation.ValidationTestHelper
import org.eclipse.xtext.junit4.util.ParseHelper
import org.xtext.dop.productdeclaration.productDeclaration.ProductDeclaration
import org.junit.Test
import org.xtext.dop.productdeclaration.validation.ProductDeclarationValidator
import org.xtext.dop.productdeclaration.productDeclaration.ProductDeclarationPackage

@RunWith(typeof(XtextRunner))
@InjectWith(typeof(ProductDeclarationInjectorProvider))

class SyntaxTests {
	
	@Inject extension ParseHelper<ProductDeclaration>
	@Inject extension ValidationTestHelper
	
	@Test def emptyDecl() {
		'''
		SPL Prova {
			Features = {}
			Deltas = {}
			Constraints {}
			Partitions {}
			Products {}
		}
		'''.parse.assertNoErrors
	}
	
	@Test def exampleDecl() {
		'''
		SPL Example {
			Features = {alpha, beta, gamma, delta}
			Deltas = {blue, red, green, black, yellow, orange}
			Constraints {
				alpha & beta;
			}
			Partitions {
				{blue} when (alpha),
				{red} when (alpha & delta),
				{green} when (!beta);
				
				{black} when (alpha & gamma),
				{yellow} when (beta);
				
				{orange} when (delta);
			}
			Products {
				base = {alpha, beta};
				adv = {alpha, beta, gamma};
				full = {alpha, beta, gamma, delta};
			}
		}
		'''.parse.assertNoErrors
	}
	
	@Test def noLastFeaturesExampleDecl() {
		'''
		SPL Example {
			Features = {alpha, beta, gamma, delta,}
			Deltas = {blue, red, green, black, yellow, orange}
			Constraints {
				alpha & beta;
			}
			Partitions {
				{blue} when (alpha),
				{red} when (alpha & delta),
				{green} when (!beta);
				
				{black} when (alpha & gamma),
				{yellow} when (beta);
				
				{orange} when (delta);
			}
			Products {
				base = {alpha, beta};
				adv = {alpha, beta, gamma};
				full = {alpha, beta, gamma, delta};
			}
		}
		'''.parse.assertError(ProductDeclarationPackage::eINSTANCE.listElem, ProductDeclarationValidator::LIST_ELEM)
	}
	
	@Test def featuresDuplicateExampleDecl() {
		'''
		SPL Example {
			Features = {alpha, beta, beta, gamma, delta}
			Deltas = {blue, red, green, black, yellow, orange}
			Constraints {
				alpha & beta;
			}
			Partitions {
				{blue} when (alpha),
				{red} when (alpha & delta),
				{green} when (!beta);
				
				{black} when (alpha & gamma),
				{yellow} when (beta);
				
				{orange} when (delta);
			}
			Products {
				base = {alpha, beta};
				adv = {alpha, beta, gamma};
				full = {alpha, beta, gamma, delta};
			}
		}
		'''.parse.assertError(ProductDeclarationPackage::eINSTANCE.element, ProductDeclarationValidator::ELEMENT)
	}
	
	@Test def noLastDeltasExampleDecl() {
		'''
		SPL Example {
			Features = {alpha, beta, gamma, delta}
			Deltas = {blue, red, green, black, yellow, orange,}
			Constraints {
				alpha & beta;
			}
			Partitions {
				{blue} when (alpha),
				{red} when (alpha & delta),
				{green} when (!beta);
				
				{black} when (alpha & gamma),
				{yellow} when (beta);
				
				{orange} when (delta);
			}
			Products {
				base = {alpha, beta};
				adv = {alpha, beta, gamma};
				full = {alpha, beta, gamma, delta};
			}
		}
		'''.parse.assertError(ProductDeclarationPackage::eINSTANCE.listElem, ProductDeclarationValidator::LIST_ELEM)
	}
	
	@Test def deltasDuplicateExampleDecl() {
		'''
		SPL Example {
			Features = {alpha, beta, gamma, delta}
			Deltas = {blue, blue, red, green, black, yellow, orange}
			Constraints {
				alpha & beta;
			}
			Partitions {
				{blue} when (alpha),
				{red} when (alpha & delta),
				{green} when (!beta);
				
				{black} when (alpha & gamma),
				{yellow} when (beta);
				
				{orange} when (delta);
			}
			Products {
				base = {alpha, beta};
				adv = {alpha, beta, gamma};
				full = {alpha, beta, gamma, delta};
			}
		}
		'''.parse.assertError(ProductDeclarationPackage::eINSTANCE.element, ProductDeclarationValidator::ELEMENT)
	}
	
	@Test def constraintBaseFeatureNotDeclaredExampleDecl() {
		'''
		SPL Example {
			Features = {alpha, beta, gamma, delta}
			Deltas = {blue, red, green, black, yellow, orange}
			Constraints {
				alpha & beta & omega;
			}
			Partitions {
				{blue} when (alpha),
				{red} when (alpha & delta),
				{green} when (!beta);
				
				{black} when (alpha & gamma),
				{yellow} when (beta);
				
				{orange} when (delta);
			}
			Products {
				base = {alpha, beta};
				adv = {alpha, beta, gamma};
				full = {alpha, beta, gamma, delta};
			}
		}
		'''.parse.assertError(ProductDeclarationPackage::eINSTANCE.element, ProductDeclarationValidator::ELEMENT)
	}
	
	@Test def notLastConstraintExampleDecl() {
		'''
		SPL Example {
			Features = {alpha, beta, gamma, delta}
			Deltas = {blue, red, green, black, yellow, orange}
			Constraints {
				alpha & beta;
			}
			Partitions {
				{blue} when (alpha),
				{red} when (alpha & delta),
				{green} when (!beta),;
				
				{black} when (alpha & gamma),
				{yellow} when (beta);
				
				{orange} when (delta);
			}
			Products {
				base = {alpha, beta};
				adv = {alpha, beta, gamma};
				full = {alpha, beta, gamma, delta};
			}
		}
		'''.parse.assertError(ProductDeclarationPackage::eINSTANCE.constraint, ProductDeclarationValidator::CONSTRAINT)
	}
	
	@Test def constraintDeltaNotDeclaredExampleDecl() {
		'''
		SPL Example {
			Features = {alpha, beta, gamma, delta}
			Deltas = {blue, red, green, black, yellow, orange}
			Constraints {
				alpha & beta;
			}
			Partitions {
				{blue} when (alpha),
				{red} when (alpha & delta),
				{green} when (!beta);
				
				{whithe} when (alpha & gamma),
				{yellow} when (beta);
				
				{orange} when (delta);
			}
			Products {
				base = {alpha, beta};
				adv = {alpha, beta, gamma};
				full = {alpha, beta, gamma, delta};
			}
		}
		'''.parse.assertError(ProductDeclarationPackage::eINSTANCE.element, ProductDeclarationValidator::ELEMENT)
	}
	
	@Test def constraintFeatureNotDeclaredExampleDecl() {
		'''
		SPL Example {
			Features = {alpha, beta, gamma, delta}
			Deltas = {blue, red, green, black, yellow, orange}
			Constraints {
				alpha & beta;
			}
			Partitions {
				{blue} when (alpha),
				{red} when (alpha & delta),
				{green} when (!beta);
				
				{black} when (alpha & omega),
				{yellow} when (beta);
				
				{orange} when (delta);
			}
			Products {
				base = {alpha, beta};
				adv = {alpha, beta, gamma};
				full = {alpha, beta, gamma, delta};
			}
		}
		'''.parse.assertError(ProductDeclarationPackage::eINSTANCE.element, ProductDeclarationValidator::ELEMENT)
	}
	
	@Test def noLastProductFeatureExampleDecl() {
		'''
		SPL Example {
			Features = {alpha, beta, gamma, delta}
			Deltas = {blue, red, green, black, yellow, orange}
			Constraints {
				alpha & beta;
			}
			Partitions {
				{blue} when (alpha),
				{red} when (alpha & delta),
				{green} when (!beta);
				
				{black} when (alpha & omega),
				{yellow} when (beta);
				
				{orange} when (delta);
			}
			Products {
				base = {alpha, beta};
				adv = {alpha, beta, gamma,};
				full = {alpha, beta, gamma, delta};
			}
		}
		'''.parse.assertError(ProductDeclarationPackage::eINSTANCE.listElem, ProductDeclarationValidator::LIST_ELEM)
	}
	
	@Test def productFeatureDuplicateExampleDecl() {
		'''
		SPL Example {
			Features = {alpha, beta, gamma, delta}
			Deltas = {blue, red, green, black, yellow, orange}
			Constraints {
				alpha & beta;
			}
			Partitions {
				{blue} when (alpha),
				{red} when (alpha & delta),
				{green} when (!beta);
				
				{black} when (alpha & omega),
				{yellow} when (beta);
				
				{orange} when (delta);
			}
			Products {
				base = {alpha, beta, beta};
				adv = {alpha, beta, gamma};
				full = {alpha, beta, gamma, delta};
			}
		}
		'''.parse.assertError(ProductDeclarationPackage::eINSTANCE.element, ProductDeclarationValidator::ELEMENT)
	}
	
	@Test def productDuplicateExampleDecl() {
		'''
		SPL Example {
			Features = {alpha, beta, gamma, delta}
			Deltas = {blue, red, green, black, yellow, orange}
			Constraints {
				alpha & beta;
			}
			Partitions {
				{blue} when (alpha),
				{red} when (alpha & delta),
				{green} when (!beta);
				
				{black} when (alpha & omega),
				{yellow} when (beta);
				
				{orange} when (delta);
			}
			Products {
				base = {alpha, beta};
				base = {alpha, beta, gamma};
				full = {alpha, beta, gamma, delta};
			}
		}
		'''.parse.assertError(ProductDeclarationPackage::eINSTANCE.product, ProductDeclarationValidator::PRODUCT)
	}
	
	@Test def productFeatureNotDeclaredExampleDecl() {
		'''
		SPL Example {
			Features = {alpha, beta, gamma, delta}
			Deltas = {blue, red, green, black, yellow, orange}
			Constraints {
				alpha & beta;
			}
			Partitions {
				{blue} when (alpha),
				{red} when (alpha & delta),
				{green} when (!beta);
				
				{black} when (alpha & omega),
				{yellow} when (beta);
				
				{orange} when (delta);
			}
			Products {
				base = {alpha, beta};
				adv = {alpha, beta, gamma, omega};
				full = {alpha, beta, gamma, delta};
			}
		}
		'''.parse.assertError(ProductDeclarationPackage::eINSTANCE.element, ProductDeclarationValidator::ELEMENT)
	}
	
	@Test def productFeaturesNotContainsBasicExampleDecl() {
		'''
		SPL Example {
			Features = {alpha, beta, gamma, delta}
			Deltas = {blue, red, green, black, yellow, orange}
			Constraints {
				alpha & beta;
			}
			Partitions {
				{blue} when (alpha),
				{red} when (alpha & delta),
				{green} when (!beta);
				
				{black} when (alpha & omega),
				{yellow} when (beta);
				
				{orange} when (delta);
			}
			Products {
				base = {alpha, beta};
				base = {beta, gamma};
				full = {alpha, beta, gamma, delta};
			}
		}
		'''.parse.assertError(ProductDeclarationPackage::eINSTANCE.product, ProductDeclarationValidator::PRODUCT)
	}
	
}