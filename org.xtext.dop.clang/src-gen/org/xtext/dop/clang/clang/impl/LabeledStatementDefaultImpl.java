/**
 */
package org.xtext.dop.clang.clang.impl;

import org.eclipse.emf.ecore.EClass;

import org.xtext.dop.clang.clang.ClangPackage;
import org.xtext.dop.clang.clang.LabeledStatementDefault;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Labeled Statement Default</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class LabeledStatementDefaultImpl extends LabeledStatementImpl implements LabeledStatementDefault
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected LabeledStatementDefaultImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ClangPackage.Literals.LABELED_STATEMENT_DEFAULT;
  }

} //LabeledStatementDefaultImpl
