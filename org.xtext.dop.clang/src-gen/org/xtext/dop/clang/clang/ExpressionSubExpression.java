/**
 */
package org.xtext.dop.clang.clang;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Expression Sub Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.clang.clang.ExpressionSubExpression#getPar <em>Par</em>}</li>
 * </ul>
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getExpressionSubExpression()
 * @model
 * @generated
 */
public interface ExpressionSubExpression extends Expression
{
  /**
   * Returns the value of the '<em><b>Par</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Par</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Par</em>' containment reference.
   * @see #setPar(Expression)
   * @see org.xtext.dop.clang.clang.ClangPackage#getExpressionSubExpression_Par()
   * @model containment="true"
   * @generated
   */
  Expression getPar();

  /**
   * Sets the value of the '{@link org.xtext.dop.clang.clang.ExpressionSubExpression#getPar <em>Par</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Par</em>' containment reference.
   * @see #getPar()
   * @generated
   */
  void setPar(Expression value);

} // ExpressionSubExpression
