package org.xtext.dop.clang.ui.contentassist.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.Lexer;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalClangLexer extends Lexer {
    public static final int RULE_A=105;
    public static final int RULE_STRUCT=4;
    public static final int RULE_DEC_OP=72;
    public static final int RULE_ATOMIC=43;
    public static final int RULE_REGISTER=63;
    public static final int RULE_G_OP=20;
    public static final int RULE_NOT_OP=32;
    public static final int RULE_WHILE=82;
    public static final int RULE_FUNC_NAME=100;
    public static final int RULE_RETURN=88;
    public static final int RULE_LAND_OP=95;
    public static final int RULE_SUB_ASSIGN=11;
    public static final int RULE_INLINE=66;
    public static final int RULE_ASSIGN=6;
    public static final int RULE_COLON=70;
    public static final int RULE_CUSTOM=93;
    public static final int RULE_L_OP=19;
    public static final int RULE_TILDE=31;
    public static final int RULE_HP=107;
    public static final int RULE_AUTO=62;
    public static final int RULE_INT=47;
    public static final int RULE_ML_COMMENT=115;
    public static final int RULE_RESTRICT=91;
    public static final int RULE_LBRACKET=40;
    public static final int RULE_MUL_ASSIGN=7;
    public static final int RULE_IMAGINARY=55;
    public static final int RULE_ENUM=58;
    public static final int RULE_MINUS_OP=24;
    public static final int RULE_MULT_OP=25;
    public static final int RULE_I_CONSTANT=98;
    public static final int RULE_COMPLEX=54;
    public static final int RULE_IS=111;
    public static final int RULE_CONST=64;
    public static final int RULE_VOLATILE=65;
    public static final int RULE_DEFAULT=76;
    public static final int RULE_IF=79;
    public static final int RULE_ELLIPSIS=92;
    public static final int RULE_DOT=34;
    public static final int RULE_DIV_OP=26;
    public static final int RULE_AND_OP=30;
    public static final int RULE_LEFT_ASSIGN=12;
    public static final int RULE_SIGNED=51;
    public static final int RULE_STATIC_ASSERT=42;
    public static final int RULE_EQ_OP=17;
    public static final int RULE_UNSIGNED=52;
    public static final int RULE_ALIGNAS=68;
    public static final int RULE_STATIC=60;
    public static final int RULE_LEFT_OP=28;
    public static final int RULE_CONTINUE=86;
    public static final int RULE_BOOL=53;
    public static final int RULE_AND_ASSIGN=14;
    public static final int RULE_O=101;
    public static final int RULE_SWITCH=81;
    public static final int RULE_P=109;
    public static final int RULE_L=104;
    public static final int RULE_OR_OP=96;
    public static final int RULE_FLOAT=49;
    public static final int RULE_F_CONSTANT=99;
    public static final int RULE_H=106;
    public static final int RULE_E=108;
    public static final int RULE_INCLUDE=33;
    public static final int RULE_D=102;
    public static final int RULE_THREAD_LOCAL=61;
    public static final int RULE_NORETURN=67;
    public static final int RULE_DQUOTE=118;
    public static final int RULE_CP=112;
    public static final int RULE_LPAREN=38;
    public static final int RULE_EXTERN=59;
    public static final int RULE_GE_OP=22;
    public static final int RULE_CHAR=45;
    public static final int RULE_SP=113;
    public static final int RULE_MOD_ASSIGN=9;
    public static final int RULE_COMMA=37;
    public static final int RULE_SHORT=46;
    public static final int RULE_RCBRACKET=57;
    public static final int RULE_CASE=78;
    public static final int RULE_XOR_ASSIGN=15;
    public static final int RULE_DO=83;
    public static final int RULE_INC_OP=71;
    public static final int RULE_UNION=5;
    public static final int RULE_SEMICOLON=36;
    public static final int RULE_ELSE=80;
    public static final int RULE_TYPEDEF=35;
    public static final int RULE_LCBRACKET=56;
    public static final int RULE_OR_ASSIGN=16;
    public static final int RULE_NE_OP=18;
    public static final int RULE_IDENTIFIER=90;
    public static final int RULE_XOR_OP=97;
    public static final int RULE_ALIGNOF=74;
    public static final int RULE_PTR_OP=77;
    public static final int RULE_ES=114;
    public static final int RULE_STRING_LITERAL=89;
    public static final int RULE_SL_COMMENT=116;
    public static final int RULE_LOR_OP=94;
    public static final int RULE_DOUBLE=50;
    public static final int RULE_BREAK=87;
    public static final int RULE_ADD_ASSIGN=10;
    public static final int RULE_Q_OP=69;
    public static final int RULE_FOR=84;
    public static final int EOF=-1;
    public static final int RULE_ADD_OP=23;
    public static final int RULE_VOID=44;
    public static final int RULE_SIZEOF=73;
    public static final int RULE_GENERIC=75;
    public static final int RULE_GOTO=85;
    public static final int RULE_WS=117;
    public static final int RULE_RIGHT_ASSIGN=13;
    public static final int RULE_FS=110;
    public static final int RULE_DIV_ASSIGN=8;
    public static final int RULE_RIGHT_OP=29;
    public static final int RULE_RPAREN=39;
    public static final int RULE_MOD_OP=27;
    public static final int RULE_NZ=103;
    public static final int RULE_LE_OP=21;
    public static final int RULE_LONG=48;
    public static final int RULE_RBRACKET=41;

    // delegates
    // delegators

    public InternalClangLexer() {;} 
    public InternalClangLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public InternalClangLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g"; }

    // $ANTLR start "RULE_O"
    public final void mRULE_O() throws RecognitionException {
        try {
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17672:17: ( '0' .. '7' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17672:19: '0' .. '7'
            {
            matchRange('0','7'); 

            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_O"

    // $ANTLR start "RULE_D"
    public final void mRULE_D() throws RecognitionException {
        try {
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17674:17: ( '0' .. '9' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17674:19: '0' .. '9'
            {
            matchRange('0','9'); 

            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_D"

    // $ANTLR start "RULE_NZ"
    public final void mRULE_NZ() throws RecognitionException {
        try {
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17676:18: ( '1' .. '9' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17676:20: '1' .. '9'
            {
            matchRange('1','9'); 

            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_NZ"

    // $ANTLR start "RULE_L"
    public final void mRULE_L() throws RecognitionException {
        try {
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17678:17: ( ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17678:19: ( 'a' .. 'z' | 'A' .. 'Z' | '_' )
            {
            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_L"

    // $ANTLR start "RULE_A"
    public final void mRULE_A() throws RecognitionException {
        try {
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17680:17: ( ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' ) )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17680:19: ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )
            {
            if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_A"

    // $ANTLR start "RULE_H"
    public final void mRULE_H() throws RecognitionException {
        try {
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17682:17: ( ( 'a' .. 'f' | 'A' .. 'F' | '0' .. '9' ) )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17682:19: ( 'a' .. 'f' | 'A' .. 'F' | '0' .. '9' )
            {
            if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='F')||(input.LA(1)>='a' && input.LA(1)<='f') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_H"

    // $ANTLR start "RULE_HP"
    public final void mRULE_HP() throws RecognitionException {
        try {
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17684:18: ( ( '0x' | '0X' ) )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17684:20: ( '0x' | '0X' )
            {
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17684:20: ( '0x' | '0X' )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0=='0') ) {
                int LA1_1 = input.LA(2);

                if ( (LA1_1=='x') ) {
                    alt1=1;
                }
                else if ( (LA1_1=='X') ) {
                    alt1=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 1, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17684:21: '0x'
                    {
                    match("0x"); 


                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17684:26: '0X'
                    {
                    match("0X"); 


                    }
                    break;

            }


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_HP"

    // $ANTLR start "RULE_E"
    public final void mRULE_E() throws RecognitionException {
        try {
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17686:17: ( ( 'E' | 'e' ) ( '+' | '-' )? ( RULE_D )+ )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17686:19: ( 'E' | 'e' ) ( '+' | '-' )? ( RULE_D )+
            {
            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17686:29: ( '+' | '-' )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0=='+'||LA2_0=='-') ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:
                    {
                    if ( input.LA(1)=='+'||input.LA(1)=='-' ) {
                        input.consume();

                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;}


                    }
                    break;

            }

            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17686:40: ( RULE_D )+
            int cnt3=0;
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( ((LA3_0>='0' && LA3_0<='9')) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17686:40: RULE_D
            	    {
            	    mRULE_D(); 

            	    }
            	    break;

            	default :
            	    if ( cnt3 >= 1 ) break loop3;
                        EarlyExitException eee =
                            new EarlyExitException(3, input);
                        throw eee;
                }
                cnt3++;
            } while (true);


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_E"

    // $ANTLR start "RULE_P"
    public final void mRULE_P() throws RecognitionException {
        try {
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17688:17: ( ( 'P' | 'p' ) ( '+' | '-' )? ( RULE_D )+ )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17688:19: ( 'P' | 'p' ) ( '+' | '-' )? ( RULE_D )+
            {
            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17688:29: ( '+' | '-' )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0=='+'||LA4_0=='-') ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:
                    {
                    if ( input.LA(1)=='+'||input.LA(1)=='-' ) {
                        input.consume();

                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;}


                    }
                    break;

            }

            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17688:40: ( RULE_D )+
            int cnt5=0;
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( ((LA5_0>='0' && LA5_0<='9')) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17688:40: RULE_D
            	    {
            	    mRULE_D(); 

            	    }
            	    break;

            	default :
            	    if ( cnt5 >= 1 ) break loop5;
                        EarlyExitException eee =
                            new EarlyExitException(5, input);
                        throw eee;
                }
                cnt5++;
            } while (true);


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_P"

    // $ANTLR start "RULE_FS"
    public final void mRULE_FS() throws RecognitionException {
        try {
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17690:18: ( ( 'f' | 'F' | 'l' | 'L' ) )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17690:20: ( 'f' | 'F' | 'l' | 'L' )
            {
            if ( input.LA(1)=='F'||input.LA(1)=='L'||input.LA(1)=='f'||input.LA(1)=='l' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_FS"

    // $ANTLR start "RULE_IS"
    public final void mRULE_IS() throws RecognitionException {
        try {
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17692:18: ( ( ( 'u' | 'U' ) ( 'l' | 'L' | 'll' | 'LL' )? | ( 'l' | 'L' | 'll' | 'LL' ) ( 'u' | 'U' )? ) )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17692:20: ( ( 'u' | 'U' ) ( 'l' | 'L' | 'll' | 'LL' )? | ( 'l' | 'L' | 'll' | 'LL' ) ( 'u' | 'U' )? )
            {
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17692:20: ( ( 'u' | 'U' ) ( 'l' | 'L' | 'll' | 'LL' )? | ( 'l' | 'L' | 'll' | 'LL' ) ( 'u' | 'U' )? )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0=='U'||LA9_0=='u') ) {
                alt9=1;
            }
            else if ( (LA9_0=='L'||LA9_0=='l') ) {
                alt9=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }
            switch (alt9) {
                case 1 :
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17692:21: ( 'u' | 'U' ) ( 'l' | 'L' | 'll' | 'LL' )?
                    {
                    if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                        input.consume();

                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;}

                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17692:31: ( 'l' | 'L' | 'll' | 'LL' )?
                    int alt6=5;
                    int LA6_0 = input.LA(1);

                    if ( (LA6_0=='l') ) {
                        int LA6_1 = input.LA(2);

                        if ( (LA6_1=='l') ) {
                            alt6=3;
                        }
                    }
                    else if ( (LA6_0=='L') ) {
                        int LA6_2 = input.LA(2);

                        if ( (LA6_2=='L') ) {
                            alt6=4;
                        }
                    }
                    switch (alt6) {
                        case 1 :
                            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17692:32: 'l'
                            {
                            match('l'); 

                            }
                            break;
                        case 2 :
                            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17692:36: 'L'
                            {
                            match('L'); 

                            }
                            break;
                        case 3 :
                            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17692:40: 'll'
                            {
                            match("ll"); 


                            }
                            break;
                        case 4 :
                            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17692:45: 'LL'
                            {
                            match("LL"); 


                            }
                            break;

                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17692:52: ( 'l' | 'L' | 'll' | 'LL' ) ( 'u' | 'U' )?
                    {
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17692:52: ( 'l' | 'L' | 'll' | 'LL' )
                    int alt7=4;
                    int LA7_0 = input.LA(1);

                    if ( (LA7_0=='l') ) {
                        int LA7_1 = input.LA(2);

                        if ( (LA7_1=='l') ) {
                            alt7=3;
                        }
                        else {
                            alt7=1;}
                    }
                    else if ( (LA7_0=='L') ) {
                        int LA7_2 = input.LA(2);

                        if ( (LA7_2=='L') ) {
                            alt7=4;
                        }
                        else {
                            alt7=2;}
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 7, 0, input);

                        throw nvae;
                    }
                    switch (alt7) {
                        case 1 :
                            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17692:53: 'l'
                            {
                            match('l'); 

                            }
                            break;
                        case 2 :
                            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17692:57: 'L'
                            {
                            match('L'); 

                            }
                            break;
                        case 3 :
                            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17692:61: 'll'
                            {
                            match("ll"); 


                            }
                            break;
                        case 4 :
                            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17692:66: 'LL'
                            {
                            match("LL"); 


                            }
                            break;

                    }

                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17692:72: ( 'u' | 'U' )?
                    int alt8=2;
                    int LA8_0 = input.LA(1);

                    if ( (LA8_0=='U'||LA8_0=='u') ) {
                        alt8=1;
                    }
                    switch (alt8) {
                        case 1 :
                            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:
                            {
                            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                                input.consume();

                            }
                            else {
                                MismatchedSetException mse = new MismatchedSetException(null,input);
                                recover(mse);
                                throw mse;}


                            }
                            break;

                    }


                    }
                    break;

            }


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_IS"

    // $ANTLR start "RULE_CP"
    public final void mRULE_CP() throws RecognitionException {
        try {
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17694:18: ( ( 'u' | 'U' | 'L' ) )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17694:20: ( 'u' | 'U' | 'L' )
            {
            if ( input.LA(1)=='L'||input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_CP"

    // $ANTLR start "RULE_SP"
    public final void mRULE_SP() throws RecognitionException {
        try {
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17696:18: ( ( 'u8' | 'u' | 'U' | 'L' ) )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17696:20: ( 'u8' | 'u' | 'U' | 'L' )
            {
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17696:20: ( 'u8' | 'u' | 'U' | 'L' )
            int alt10=4;
            switch ( input.LA(1) ) {
            case 'u':
                {
                int LA10_1 = input.LA(2);

                if ( (LA10_1=='8') ) {
                    alt10=1;
                }
                else {
                    alt10=2;}
                }
                break;
            case 'U':
                {
                alt10=3;
                }
                break;
            case 'L':
                {
                alt10=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }

            switch (alt10) {
                case 1 :
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17696:21: 'u8'
                    {
                    match("u8"); 


                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17696:26: 'u'
                    {
                    match('u'); 

                    }
                    break;
                case 3 :
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17696:30: 'U'
                    {
                    match('U'); 

                    }
                    break;
                case 4 :
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17696:34: 'L'
                    {
                    match('L'); 

                    }
                    break;

            }


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_SP"

    // $ANTLR start "RULE_ES"
    public final void mRULE_ES() throws RecognitionException {
        try {
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17698:18: ( '\\\\' ( '\\'' | '\"' | '?' | '\\\\' | 'a' | 'b' | 'f' | 'n' | 'r' | 't' | 'v' | ( RULE_O | RULE_O RULE_O | RULE_O RULE_O RULE_O ) | 'x' ( RULE_H )+ ) )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17698:20: '\\\\' ( '\\'' | '\"' | '?' | '\\\\' | 'a' | 'b' | 'f' | 'n' | 'r' | 't' | 'v' | ( RULE_O | RULE_O RULE_O | RULE_O RULE_O RULE_O ) | 'x' ( RULE_H )+ )
            {
            match('\\'); 
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17698:25: ( '\\'' | '\"' | '?' | '\\\\' | 'a' | 'b' | 'f' | 'n' | 'r' | 't' | 'v' | ( RULE_O | RULE_O RULE_O | RULE_O RULE_O RULE_O ) | 'x' ( RULE_H )+ )
            int alt13=13;
            switch ( input.LA(1) ) {
            case '\'':
                {
                alt13=1;
                }
                break;
            case '\"':
                {
                alt13=2;
                }
                break;
            case '?':
                {
                alt13=3;
                }
                break;
            case '\\':
                {
                alt13=4;
                }
                break;
            case 'a':
                {
                alt13=5;
                }
                break;
            case 'b':
                {
                alt13=6;
                }
                break;
            case 'f':
                {
                alt13=7;
                }
                break;
            case 'n':
                {
                alt13=8;
                }
                break;
            case 'r':
                {
                alt13=9;
                }
                break;
            case 't':
                {
                alt13=10;
                }
                break;
            case 'v':
                {
                alt13=11;
                }
                break;
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
                {
                alt13=12;
                }
                break;
            case 'x':
                {
                alt13=13;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;
            }

            switch (alt13) {
                case 1 :
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17698:26: '\\''
                    {
                    match('\''); 

                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17698:31: '\"'
                    {
                    match('\"'); 

                    }
                    break;
                case 3 :
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17698:35: '?'
                    {
                    match('?'); 

                    }
                    break;
                case 4 :
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17698:39: '\\\\'
                    {
                    match('\\'); 

                    }
                    break;
                case 5 :
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17698:44: 'a'
                    {
                    match('a'); 

                    }
                    break;
                case 6 :
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17698:48: 'b'
                    {
                    match('b'); 

                    }
                    break;
                case 7 :
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17698:52: 'f'
                    {
                    match('f'); 

                    }
                    break;
                case 8 :
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17698:56: 'n'
                    {
                    match('n'); 

                    }
                    break;
                case 9 :
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17698:60: 'r'
                    {
                    match('r'); 

                    }
                    break;
                case 10 :
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17698:64: 't'
                    {
                    match('t'); 

                    }
                    break;
                case 11 :
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17698:68: 'v'
                    {
                    match('v'); 

                    }
                    break;
                case 12 :
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17698:72: ( RULE_O | RULE_O RULE_O | RULE_O RULE_O RULE_O )
                    {
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17698:72: ( RULE_O | RULE_O RULE_O | RULE_O RULE_O RULE_O )
                    int alt11=3;
                    int LA11_0 = input.LA(1);

                    if ( ((LA11_0>='0' && LA11_0<='7')) ) {
                        int LA11_1 = input.LA(2);

                        if ( ((LA11_1>='0' && LA11_1<='7')) ) {
                            int LA11_3 = input.LA(3);

                            if ( ((LA11_3>='0' && LA11_3<='7')) ) {
                                alt11=3;
                            }
                            else {
                                alt11=2;}
                        }
                        else {
                            alt11=1;}
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 11, 0, input);

                        throw nvae;
                    }
                    switch (alt11) {
                        case 1 :
                            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17698:73: RULE_O
                            {
                            mRULE_O(); 

                            }
                            break;
                        case 2 :
                            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17698:80: RULE_O RULE_O
                            {
                            mRULE_O(); 
                            mRULE_O(); 

                            }
                            break;
                        case 3 :
                            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17698:94: RULE_O RULE_O RULE_O
                            {
                            mRULE_O(); 
                            mRULE_O(); 
                            mRULE_O(); 

                            }
                            break;

                    }


                    }
                    break;
                case 13 :
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17698:116: 'x' ( RULE_H )+
                    {
                    match('x'); 
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17698:120: ( RULE_H )+
                    int cnt12=0;
                    loop12:
                    do {
                        int alt12=2;
                        int LA12_0 = input.LA(1);

                        if ( ((LA12_0>='0' && LA12_0<='9')||(LA12_0>='A' && LA12_0<='F')||(LA12_0>='a' && LA12_0<='f')) ) {
                            alt12=1;
                        }


                        switch (alt12) {
                    	case 1 :
                    	    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17698:120: RULE_H
                    	    {
                    	    mRULE_H(); 

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt12 >= 1 ) break loop12;
                                EarlyExitException eee =
                                    new EarlyExitException(12, input);
                                throw eee;
                        }
                        cnt12++;
                    } while (true);


                    }
                    break;

            }


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_ES"

    // $ANTLR start "RULE_ML_COMMENT"
    public final void mRULE_ML_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_ML_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17700:17: ( '/*' ( options {greedy=false; } : . )* '*/' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17700:19: '/*' ( options {greedy=false; } : . )* '*/'
            {
            match("/*"); 

            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17700:24: ( options {greedy=false; } : . )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( (LA14_0=='*') ) {
                    int LA14_1 = input.LA(2);

                    if ( (LA14_1=='/') ) {
                        alt14=2;
                    }
                    else if ( ((LA14_1>='\u0000' && LA14_1<='.')||(LA14_1>='0' && LA14_1<='\uFFFF')) ) {
                        alt14=1;
                    }


                }
                else if ( ((LA14_0>='\u0000' && LA14_0<=')')||(LA14_0>='+' && LA14_0<='\uFFFF')) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17700:52: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);

            match("*/"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ML_COMMENT"

    // $ANTLR start "RULE_SL_COMMENT"
    public final void mRULE_SL_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_SL_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17702:17: ( '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )? )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17702:19: '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )?
            {
            match("//"); 

            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17702:24: (~ ( ( '\\n' | '\\r' ) ) )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( ((LA15_0>='\u0000' && LA15_0<='\t')||(LA15_0>='\u000B' && LA15_0<='\f')||(LA15_0>='\u000E' && LA15_0<='\uFFFF')) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17702:24: ~ ( ( '\\n' | '\\r' ) )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='\uFFFF') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17702:40: ( ( '\\r' )? '\\n' )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0=='\n'||LA17_0=='\r') ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17702:41: ( '\\r' )? '\\n'
                    {
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17702:41: ( '\\r' )?
                    int alt16=2;
                    int LA16_0 = input.LA(1);

                    if ( (LA16_0=='\r') ) {
                        alt16=1;
                    }
                    switch (alt16) {
                        case 1 :
                            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17702:41: '\\r'
                            {
                            match('\r'); 

                            }
                            break;

                    }

                    match('\n'); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SL_COMMENT"

    // $ANTLR start "RULE_WS"
    public final void mRULE_WS() throws RecognitionException {
        try {
            int _type = RULE_WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17704:9: ( ( ' ' | '\\t' | '\\b' | '\\n' | '\\f' | '\\r' ) )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17704:11: ( ' ' | '\\t' | '\\b' | '\\n' | '\\f' | '\\r' )
            {
            if ( (input.LA(1)>='\b' && input.LA(1)<='\n')||(input.LA(1)>='\f' && input.LA(1)<='\r')||input.LA(1)==' ' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_WS"

    // $ANTLR start "RULE_AUTO"
    public final void mRULE_AUTO() throws RecognitionException {
        try {
            int _type = RULE_AUTO;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17706:11: ( 'auto' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17706:13: 'auto'
            {
            match("auto"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_AUTO"

    // $ANTLR start "RULE_BREAK"
    public final void mRULE_BREAK() throws RecognitionException {
        try {
            int _type = RULE_BREAK;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17708:12: ( 'break' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17708:14: 'break'
            {
            match("break"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_BREAK"

    // $ANTLR start "RULE_CASE"
    public final void mRULE_CASE() throws RecognitionException {
        try {
            int _type = RULE_CASE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17710:11: ( 'case' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17710:13: 'case'
            {
            match("case"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_CASE"

    // $ANTLR start "RULE_CHAR"
    public final void mRULE_CHAR() throws RecognitionException {
        try {
            int _type = RULE_CHAR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17712:11: ( 'char' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17712:13: 'char'
            {
            match("char"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_CHAR"

    // $ANTLR start "RULE_CONST"
    public final void mRULE_CONST() throws RecognitionException {
        try {
            int _type = RULE_CONST;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17714:12: ( 'const' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17714:14: 'const'
            {
            match("const"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_CONST"

    // $ANTLR start "RULE_CONTINUE"
    public final void mRULE_CONTINUE() throws RecognitionException {
        try {
            int _type = RULE_CONTINUE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17716:15: ( 'continue' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17716:17: 'continue'
            {
            match("continue"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_CONTINUE"

    // $ANTLR start "RULE_DEFAULT"
    public final void mRULE_DEFAULT() throws RecognitionException {
        try {
            int _type = RULE_DEFAULT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17718:14: ( 'default' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17718:16: 'default'
            {
            match("default"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_DEFAULT"

    // $ANTLR start "RULE_DO"
    public final void mRULE_DO() throws RecognitionException {
        try {
            int _type = RULE_DO;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17720:9: ( 'do' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17720:11: 'do'
            {
            match("do"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_DO"

    // $ANTLR start "RULE_DOUBLE"
    public final void mRULE_DOUBLE() throws RecognitionException {
        try {
            int _type = RULE_DOUBLE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17722:13: ( 'double' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17722:15: 'double'
            {
            match("double"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_DOUBLE"

    // $ANTLR start "RULE_ELSE"
    public final void mRULE_ELSE() throws RecognitionException {
        try {
            int _type = RULE_ELSE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17724:11: ( 'else' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17724:13: 'else'
            {
            match("else"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ELSE"

    // $ANTLR start "RULE_ENUM"
    public final void mRULE_ENUM() throws RecognitionException {
        try {
            int _type = RULE_ENUM;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17726:11: ( 'enum' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17726:13: 'enum'
            {
            match("enum"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ENUM"

    // $ANTLR start "RULE_EXTERN"
    public final void mRULE_EXTERN() throws RecognitionException {
        try {
            int _type = RULE_EXTERN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17728:13: ( 'extern' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17728:15: 'extern'
            {
            match("extern"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_EXTERN"

    // $ANTLR start "RULE_FLOAT"
    public final void mRULE_FLOAT() throws RecognitionException {
        try {
            int _type = RULE_FLOAT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17730:12: ( 'float' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17730:14: 'float'
            {
            match("float"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_FLOAT"

    // $ANTLR start "RULE_FOR"
    public final void mRULE_FOR() throws RecognitionException {
        try {
            int _type = RULE_FOR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17732:10: ( 'for' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17732:12: 'for'
            {
            match("for"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_FOR"

    // $ANTLR start "RULE_GOTO"
    public final void mRULE_GOTO() throws RecognitionException {
        try {
            int _type = RULE_GOTO;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17734:11: ( 'goto' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17734:13: 'goto'
            {
            match("goto"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_GOTO"

    // $ANTLR start "RULE_IF"
    public final void mRULE_IF() throws RecognitionException {
        try {
            int _type = RULE_IF;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17736:9: ( 'if' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17736:11: 'if'
            {
            match("if"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_IF"

    // $ANTLR start "RULE_INLINE"
    public final void mRULE_INLINE() throws RecognitionException {
        try {
            int _type = RULE_INLINE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17738:13: ( 'inline' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17738:15: 'inline'
            {
            match("inline"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_INLINE"

    // $ANTLR start "RULE_INT"
    public final void mRULE_INT() throws RecognitionException {
        try {
            int _type = RULE_INT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17740:10: ( 'int' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17740:12: 'int'
            {
            match("int"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_INT"

    // $ANTLR start "RULE_LONG"
    public final void mRULE_LONG() throws RecognitionException {
        try {
            int _type = RULE_LONG;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17742:11: ( 'long' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17742:13: 'long'
            {
            match("long"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_LONG"

    // $ANTLR start "RULE_REGISTER"
    public final void mRULE_REGISTER() throws RecognitionException {
        try {
            int _type = RULE_REGISTER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17744:15: ( 'register' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17744:17: 'register'
            {
            match("register"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_REGISTER"

    // $ANTLR start "RULE_RESTRICT"
    public final void mRULE_RESTRICT() throws RecognitionException {
        try {
            int _type = RULE_RESTRICT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17746:15: ( 'restrict' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17746:17: 'restrict'
            {
            match("restrict"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_RESTRICT"

    // $ANTLR start "RULE_RETURN"
    public final void mRULE_RETURN() throws RecognitionException {
        try {
            int _type = RULE_RETURN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17748:13: ( 'return' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17748:15: 'return'
            {
            match("return"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_RETURN"

    // $ANTLR start "RULE_SHORT"
    public final void mRULE_SHORT() throws RecognitionException {
        try {
            int _type = RULE_SHORT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17750:12: ( 'short' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17750:14: 'short'
            {
            match("short"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SHORT"

    // $ANTLR start "RULE_SIGNED"
    public final void mRULE_SIGNED() throws RecognitionException {
        try {
            int _type = RULE_SIGNED;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17752:13: ( 'signed' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17752:15: 'signed'
            {
            match("signed"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SIGNED"

    // $ANTLR start "RULE_SIZEOF"
    public final void mRULE_SIZEOF() throws RecognitionException {
        try {
            int _type = RULE_SIZEOF;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17754:13: ( 'sizeof' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17754:15: 'sizeof'
            {
            match("sizeof"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SIZEOF"

    // $ANTLR start "RULE_STATIC"
    public final void mRULE_STATIC() throws RecognitionException {
        try {
            int _type = RULE_STATIC;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17756:13: ( 'static' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17756:15: 'static'
            {
            match("static"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_STATIC"

    // $ANTLR start "RULE_STRUCT"
    public final void mRULE_STRUCT() throws RecognitionException {
        try {
            int _type = RULE_STRUCT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17758:13: ( 'struct' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17758:15: 'struct'
            {
            match("struct"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_STRUCT"

    // $ANTLR start "RULE_SWITCH"
    public final void mRULE_SWITCH() throws RecognitionException {
        try {
            int _type = RULE_SWITCH;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17760:13: ( 'switch' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17760:15: 'switch'
            {
            match("switch"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SWITCH"

    // $ANTLR start "RULE_TYPEDEF"
    public final void mRULE_TYPEDEF() throws RecognitionException {
        try {
            int _type = RULE_TYPEDEF;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17762:14: ( 'typedef' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17762:16: 'typedef'
            {
            match("typedef"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_TYPEDEF"

    // $ANTLR start "RULE_UNION"
    public final void mRULE_UNION() throws RecognitionException {
        try {
            int _type = RULE_UNION;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17764:12: ( 'union' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17764:14: 'union'
            {
            match("union"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_UNION"

    // $ANTLR start "RULE_UNSIGNED"
    public final void mRULE_UNSIGNED() throws RecognitionException {
        try {
            int _type = RULE_UNSIGNED;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17766:15: ( 'unsigned' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17766:17: 'unsigned'
            {
            match("unsigned"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_UNSIGNED"

    // $ANTLR start "RULE_VOID"
    public final void mRULE_VOID() throws RecognitionException {
        try {
            int _type = RULE_VOID;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17768:11: ( 'void' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17768:13: 'void'
            {
            match("void"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_VOID"

    // $ANTLR start "RULE_VOLATILE"
    public final void mRULE_VOLATILE() throws RecognitionException {
        try {
            int _type = RULE_VOLATILE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17770:15: ( 'volatile' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17770:17: 'volatile'
            {
            match("volatile"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_VOLATILE"

    // $ANTLR start "RULE_WHILE"
    public final void mRULE_WHILE() throws RecognitionException {
        try {
            int _type = RULE_WHILE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17772:12: ( 'while' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17772:14: 'while'
            {
            match("while"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_WHILE"

    // $ANTLR start "RULE_ALIGNAS"
    public final void mRULE_ALIGNAS() throws RecognitionException {
        try {
            int _type = RULE_ALIGNAS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17774:14: ( '_Alignas' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17774:16: '_Alignas'
            {
            match("_Alignas"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ALIGNAS"

    // $ANTLR start "RULE_ALIGNOF"
    public final void mRULE_ALIGNOF() throws RecognitionException {
        try {
            int _type = RULE_ALIGNOF;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17776:14: ( '_Alignof' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17776:16: '_Alignof'
            {
            match("_Alignof"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ALIGNOF"

    // $ANTLR start "RULE_ATOMIC"
    public final void mRULE_ATOMIC() throws RecognitionException {
        try {
            int _type = RULE_ATOMIC;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17778:13: ( '_Atomic' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17778:15: '_Atomic'
            {
            match("_Atomic"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ATOMIC"

    // $ANTLR start "RULE_BOOL"
    public final void mRULE_BOOL() throws RecognitionException {
        try {
            int _type = RULE_BOOL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17780:11: ( '_Bool' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17780:13: '_Bool'
            {
            match("_Bool"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_BOOL"

    // $ANTLR start "RULE_COMPLEX"
    public final void mRULE_COMPLEX() throws RecognitionException {
        try {
            int _type = RULE_COMPLEX;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17782:14: ( '_Complex' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17782:16: '_Complex'
            {
            match("_Complex"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_COMPLEX"

    // $ANTLR start "RULE_GENERIC"
    public final void mRULE_GENERIC() throws RecognitionException {
        try {
            int _type = RULE_GENERIC;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17784:14: ( '_Generic' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17784:16: '_Generic'
            {
            match("_Generic"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_GENERIC"

    // $ANTLR start "RULE_IMAGINARY"
    public final void mRULE_IMAGINARY() throws RecognitionException {
        try {
            int _type = RULE_IMAGINARY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17786:16: ( '_Imaginary' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17786:18: '_Imaginary'
            {
            match("_Imaginary"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_IMAGINARY"

    // $ANTLR start "RULE_NORETURN"
    public final void mRULE_NORETURN() throws RecognitionException {
        try {
            int _type = RULE_NORETURN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17788:15: ( '_Noreturn' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17788:17: '_Noreturn'
            {
            match("_Noreturn"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_NORETURN"

    // $ANTLR start "RULE_STATIC_ASSERT"
    public final void mRULE_STATIC_ASSERT() throws RecognitionException {
        try {
            int _type = RULE_STATIC_ASSERT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17790:20: ( '_Static_assert' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17790:22: '_Static_assert'
            {
            match("_Static_assert"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_STATIC_ASSERT"

    // $ANTLR start "RULE_THREAD_LOCAL"
    public final void mRULE_THREAD_LOCAL() throws RecognitionException {
        try {
            int _type = RULE_THREAD_LOCAL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17792:19: ( '_Thread_local' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17792:21: '_Thread_local'
            {
            match("_Thread_local"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_THREAD_LOCAL"

    // $ANTLR start "RULE_FUNC_NAME"
    public final void mRULE_FUNC_NAME() throws RecognitionException {
        try {
            int _type = RULE_FUNC_NAME;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17794:16: ( '__func__' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17794:18: '__func__'
            {
            match("__func__"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_FUNC_NAME"

    // $ANTLR start "RULE_IDENTIFIER"
    public final void mRULE_IDENTIFIER() throws RecognitionException {
        try {
            int _type = RULE_IDENTIFIER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17796:17: ( RULE_L ( RULE_A )* )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17796:19: RULE_L ( RULE_A )*
            {
            mRULE_L(); 
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17796:26: ( RULE_A )*
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( ((LA18_0>='0' && LA18_0<='9')||(LA18_0>='A' && LA18_0<='Z')||LA18_0=='_'||(LA18_0>='a' && LA18_0<='z')) ) {
                    alt18=1;
                }


                switch (alt18) {
            	case 1 :
            	    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17796:26: RULE_A
            	    {
            	    mRULE_A(); 

            	    }
            	    break;

            	default :
            	    break loop18;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_IDENTIFIER"

    // $ANTLR start "RULE_CUSTOM"
    public final void mRULE_CUSTOM() throws RecognitionException {
        try {
            int _type = RULE_CUSTOM;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17798:13: ( '$' RULE_L ( RULE_A )* )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17798:15: '$' RULE_L ( RULE_A )*
            {
            match('$'); 
            mRULE_L(); 
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17798:26: ( RULE_A )*
            loop19:
            do {
                int alt19=2;
                int LA19_0 = input.LA(1);

                if ( ((LA19_0>='0' && LA19_0<='9')||(LA19_0>='A' && LA19_0<='Z')||LA19_0=='_'||(LA19_0>='a' && LA19_0<='z')) ) {
                    alt19=1;
                }


                switch (alt19) {
            	case 1 :
            	    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17798:26: RULE_A
            	    {
            	    mRULE_A(); 

            	    }
            	    break;

            	default :
            	    break loop19;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_CUSTOM"

    // $ANTLR start "RULE_I_CONSTANT"
    public final void mRULE_I_CONSTANT() throws RecognitionException {
        try {
            int _type = RULE_I_CONSTANT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17800:17: ( ( RULE_HP ( RULE_H )+ ( RULE_IS )? | RULE_NZ ( RULE_D )* ( RULE_IS )? | '0' ( RULE_O )* ( RULE_IS )? | ( RULE_CP )? '\\'' (~ ( ( '\\'' | '\\\\' | '\\n' ) ) | RULE_ES )+ '\\'' ) )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17800:19: ( RULE_HP ( RULE_H )+ ( RULE_IS )? | RULE_NZ ( RULE_D )* ( RULE_IS )? | '0' ( RULE_O )* ( RULE_IS )? | ( RULE_CP )? '\\'' (~ ( ( '\\'' | '\\\\' | '\\n' ) ) | RULE_ES )+ '\\'' )
            {
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17800:19: ( RULE_HP ( RULE_H )+ ( RULE_IS )? | RULE_NZ ( RULE_D )* ( RULE_IS )? | '0' ( RULE_O )* ( RULE_IS )? | ( RULE_CP )? '\\'' (~ ( ( '\\'' | '\\\\' | '\\n' ) ) | RULE_ES )+ '\\'' )
            int alt28=4;
            switch ( input.LA(1) ) {
            case '0':
                {
                int LA28_1 = input.LA(2);

                if ( (LA28_1=='X'||LA28_1=='x') ) {
                    alt28=1;
                }
                else {
                    alt28=3;}
                }
                break;
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                {
                alt28=2;
                }
                break;
            case '\'':
            case 'L':
            case 'U':
            case 'u':
                {
                alt28=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 28, 0, input);

                throw nvae;
            }

            switch (alt28) {
                case 1 :
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17800:20: RULE_HP ( RULE_H )+ ( RULE_IS )?
                    {
                    mRULE_HP(); 
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17800:28: ( RULE_H )+
                    int cnt20=0;
                    loop20:
                    do {
                        int alt20=2;
                        int LA20_0 = input.LA(1);

                        if ( ((LA20_0>='0' && LA20_0<='9')||(LA20_0>='A' && LA20_0<='F')||(LA20_0>='a' && LA20_0<='f')) ) {
                            alt20=1;
                        }


                        switch (alt20) {
                    	case 1 :
                    	    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17800:28: RULE_H
                    	    {
                    	    mRULE_H(); 

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt20 >= 1 ) break loop20;
                                EarlyExitException eee =
                                    new EarlyExitException(20, input);
                                throw eee;
                        }
                        cnt20++;
                    } while (true);

                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17800:36: ( RULE_IS )?
                    int alt21=2;
                    int LA21_0 = input.LA(1);

                    if ( (LA21_0=='L'||LA21_0=='U'||LA21_0=='l'||LA21_0=='u') ) {
                        alt21=1;
                    }
                    switch (alt21) {
                        case 1 :
                            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17800:36: RULE_IS
                            {
                            mRULE_IS(); 

                            }
                            break;

                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17800:45: RULE_NZ ( RULE_D )* ( RULE_IS )?
                    {
                    mRULE_NZ(); 
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17800:53: ( RULE_D )*
                    loop22:
                    do {
                        int alt22=2;
                        int LA22_0 = input.LA(1);

                        if ( ((LA22_0>='0' && LA22_0<='9')) ) {
                            alt22=1;
                        }


                        switch (alt22) {
                    	case 1 :
                    	    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17800:53: RULE_D
                    	    {
                    	    mRULE_D(); 

                    	    }
                    	    break;

                    	default :
                    	    break loop22;
                        }
                    } while (true);

                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17800:61: ( RULE_IS )?
                    int alt23=2;
                    int LA23_0 = input.LA(1);

                    if ( (LA23_0=='L'||LA23_0=='U'||LA23_0=='l'||LA23_0=='u') ) {
                        alt23=1;
                    }
                    switch (alt23) {
                        case 1 :
                            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17800:61: RULE_IS
                            {
                            mRULE_IS(); 

                            }
                            break;

                    }


                    }
                    break;
                case 3 :
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17800:70: '0' ( RULE_O )* ( RULE_IS )?
                    {
                    match('0'); 
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17800:74: ( RULE_O )*
                    loop24:
                    do {
                        int alt24=2;
                        int LA24_0 = input.LA(1);

                        if ( ((LA24_0>='0' && LA24_0<='7')) ) {
                            alt24=1;
                        }


                        switch (alt24) {
                    	case 1 :
                    	    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17800:74: RULE_O
                    	    {
                    	    mRULE_O(); 

                    	    }
                    	    break;

                    	default :
                    	    break loop24;
                        }
                    } while (true);

                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17800:82: ( RULE_IS )?
                    int alt25=2;
                    int LA25_0 = input.LA(1);

                    if ( (LA25_0=='L'||LA25_0=='U'||LA25_0=='l'||LA25_0=='u') ) {
                        alt25=1;
                    }
                    switch (alt25) {
                        case 1 :
                            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17800:82: RULE_IS
                            {
                            mRULE_IS(); 

                            }
                            break;

                    }


                    }
                    break;
                case 4 :
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17800:91: ( RULE_CP )? '\\'' (~ ( ( '\\'' | '\\\\' | '\\n' ) ) | RULE_ES )+ '\\''
                    {
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17800:91: ( RULE_CP )?
                    int alt26=2;
                    int LA26_0 = input.LA(1);

                    if ( (LA26_0=='L'||LA26_0=='U'||LA26_0=='u') ) {
                        alt26=1;
                    }
                    switch (alt26) {
                        case 1 :
                            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17800:91: RULE_CP
                            {
                            mRULE_CP(); 

                            }
                            break;

                    }

                    match('\''); 
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17800:105: (~ ( ( '\\'' | '\\\\' | '\\n' ) ) | RULE_ES )+
                    int cnt27=0;
                    loop27:
                    do {
                        int alt27=3;
                        int LA27_0 = input.LA(1);

                        if ( ((LA27_0>='\u0000' && LA27_0<='\t')||(LA27_0>='\u000B' && LA27_0<='&')||(LA27_0>='(' && LA27_0<='[')||(LA27_0>=']' && LA27_0<='\uFFFF')) ) {
                            alt27=1;
                        }
                        else if ( (LA27_0=='\\') ) {
                            alt27=2;
                        }


                        switch (alt27) {
                    	case 1 :
                    	    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17800:106: ~ ( ( '\\'' | '\\\\' | '\\n' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='&')||(input.LA(1)>='(' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;
                    	case 2 :
                    	    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17800:126: RULE_ES
                    	    {
                    	    mRULE_ES(); 

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt27 >= 1 ) break loop27;
                                EarlyExitException eee =
                                    new EarlyExitException(27, input);
                                throw eee;
                        }
                        cnt27++;
                    } while (true);

                    match('\''); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_I_CONSTANT"

    // $ANTLR start "RULE_F_CONSTANT"
    public final void mRULE_F_CONSTANT() throws RecognitionException {
        try {
            int _type = RULE_F_CONSTANT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17802:17: ( ( ( RULE_D )+ RULE_E ( RULE_FS )? | ( RULE_D )* '.' ( RULE_D )+ ( RULE_E )? ( RULE_FS )? | ( RULE_D )+ '.' ( RULE_E )? ( RULE_FS )? | RULE_HP ( RULE_H )+ RULE_P ( RULE_FS )? | RULE_HP ( RULE_H )* '.' ( RULE_H )+ RULE_P ( RULE_FS )? | RULE_HP ( RULE_H )+ '.' RULE_P ( RULE_FS )? ) )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17802:19: ( ( RULE_D )+ RULE_E ( RULE_FS )? | ( RULE_D )* '.' ( RULE_D )+ ( RULE_E )? ( RULE_FS )? | ( RULE_D )+ '.' ( RULE_E )? ( RULE_FS )? | RULE_HP ( RULE_H )+ RULE_P ( RULE_FS )? | RULE_HP ( RULE_H )* '.' ( RULE_H )+ RULE_P ( RULE_FS )? | RULE_HP ( RULE_H )+ '.' RULE_P ( RULE_FS )? )
            {
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17802:19: ( ( RULE_D )+ RULE_E ( RULE_FS )? | ( RULE_D )* '.' ( RULE_D )+ ( RULE_E )? ( RULE_FS )? | ( RULE_D )+ '.' ( RULE_E )? ( RULE_FS )? | RULE_HP ( RULE_H )+ RULE_P ( RULE_FS )? | RULE_HP ( RULE_H )* '.' ( RULE_H )+ RULE_P ( RULE_FS )? | RULE_HP ( RULE_H )+ '.' RULE_P ( RULE_FS )? )
            int alt45=6;
            alt45 = dfa45.predict(input);
            switch (alt45) {
                case 1 :
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17802:20: ( RULE_D )+ RULE_E ( RULE_FS )?
                    {
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17802:20: ( RULE_D )+
                    int cnt29=0;
                    loop29:
                    do {
                        int alt29=2;
                        int LA29_0 = input.LA(1);

                        if ( ((LA29_0>='0' && LA29_0<='9')) ) {
                            alt29=1;
                        }


                        switch (alt29) {
                    	case 1 :
                    	    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17802:20: RULE_D
                    	    {
                    	    mRULE_D(); 

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt29 >= 1 ) break loop29;
                                EarlyExitException eee =
                                    new EarlyExitException(29, input);
                                throw eee;
                        }
                        cnt29++;
                    } while (true);

                    mRULE_E(); 
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17802:35: ( RULE_FS )?
                    int alt30=2;
                    int LA30_0 = input.LA(1);

                    if ( (LA30_0=='F'||LA30_0=='L'||LA30_0=='f'||LA30_0=='l') ) {
                        alt30=1;
                    }
                    switch (alt30) {
                        case 1 :
                            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17802:35: RULE_FS
                            {
                            mRULE_FS(); 

                            }
                            break;

                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17802:44: ( RULE_D )* '.' ( RULE_D )+ ( RULE_E )? ( RULE_FS )?
                    {
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17802:44: ( RULE_D )*
                    loop31:
                    do {
                        int alt31=2;
                        int LA31_0 = input.LA(1);

                        if ( ((LA31_0>='0' && LA31_0<='9')) ) {
                            alt31=1;
                        }


                        switch (alt31) {
                    	case 1 :
                    	    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17802:44: RULE_D
                    	    {
                    	    mRULE_D(); 

                    	    }
                    	    break;

                    	default :
                    	    break loop31;
                        }
                    } while (true);

                    match('.'); 
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17802:56: ( RULE_D )+
                    int cnt32=0;
                    loop32:
                    do {
                        int alt32=2;
                        int LA32_0 = input.LA(1);

                        if ( ((LA32_0>='0' && LA32_0<='9')) ) {
                            alt32=1;
                        }


                        switch (alt32) {
                    	case 1 :
                    	    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17802:56: RULE_D
                    	    {
                    	    mRULE_D(); 

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt32 >= 1 ) break loop32;
                                EarlyExitException eee =
                                    new EarlyExitException(32, input);
                                throw eee;
                        }
                        cnt32++;
                    } while (true);

                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17802:64: ( RULE_E )?
                    int alt33=2;
                    int LA33_0 = input.LA(1);

                    if ( (LA33_0=='E'||LA33_0=='e') ) {
                        alt33=1;
                    }
                    switch (alt33) {
                        case 1 :
                            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17802:64: RULE_E
                            {
                            mRULE_E(); 

                            }
                            break;

                    }

                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17802:72: ( RULE_FS )?
                    int alt34=2;
                    int LA34_0 = input.LA(1);

                    if ( (LA34_0=='F'||LA34_0=='L'||LA34_0=='f'||LA34_0=='l') ) {
                        alt34=1;
                    }
                    switch (alt34) {
                        case 1 :
                            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17802:72: RULE_FS
                            {
                            mRULE_FS(); 

                            }
                            break;

                    }


                    }
                    break;
                case 3 :
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17802:81: ( RULE_D )+ '.' ( RULE_E )? ( RULE_FS )?
                    {
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17802:81: ( RULE_D )+
                    int cnt35=0;
                    loop35:
                    do {
                        int alt35=2;
                        int LA35_0 = input.LA(1);

                        if ( ((LA35_0>='0' && LA35_0<='9')) ) {
                            alt35=1;
                        }


                        switch (alt35) {
                    	case 1 :
                    	    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17802:81: RULE_D
                    	    {
                    	    mRULE_D(); 

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt35 >= 1 ) break loop35;
                                EarlyExitException eee =
                                    new EarlyExitException(35, input);
                                throw eee;
                        }
                        cnt35++;
                    } while (true);

                    match('.'); 
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17802:93: ( RULE_E )?
                    int alt36=2;
                    int LA36_0 = input.LA(1);

                    if ( (LA36_0=='E'||LA36_0=='e') ) {
                        alt36=1;
                    }
                    switch (alt36) {
                        case 1 :
                            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17802:93: RULE_E
                            {
                            mRULE_E(); 

                            }
                            break;

                    }

                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17802:101: ( RULE_FS )?
                    int alt37=2;
                    int LA37_0 = input.LA(1);

                    if ( (LA37_0=='F'||LA37_0=='L'||LA37_0=='f'||LA37_0=='l') ) {
                        alt37=1;
                    }
                    switch (alt37) {
                        case 1 :
                            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17802:101: RULE_FS
                            {
                            mRULE_FS(); 

                            }
                            break;

                    }


                    }
                    break;
                case 4 :
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17802:110: RULE_HP ( RULE_H )+ RULE_P ( RULE_FS )?
                    {
                    mRULE_HP(); 
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17802:118: ( RULE_H )+
                    int cnt38=0;
                    loop38:
                    do {
                        int alt38=2;
                        int LA38_0 = input.LA(1);

                        if ( ((LA38_0>='0' && LA38_0<='9')||(LA38_0>='A' && LA38_0<='F')||(LA38_0>='a' && LA38_0<='f')) ) {
                            alt38=1;
                        }


                        switch (alt38) {
                    	case 1 :
                    	    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17802:118: RULE_H
                    	    {
                    	    mRULE_H(); 

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt38 >= 1 ) break loop38;
                                EarlyExitException eee =
                                    new EarlyExitException(38, input);
                                throw eee;
                        }
                        cnt38++;
                    } while (true);

                    mRULE_P(); 
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17802:133: ( RULE_FS )?
                    int alt39=2;
                    int LA39_0 = input.LA(1);

                    if ( (LA39_0=='F'||LA39_0=='L'||LA39_0=='f'||LA39_0=='l') ) {
                        alt39=1;
                    }
                    switch (alt39) {
                        case 1 :
                            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17802:133: RULE_FS
                            {
                            mRULE_FS(); 

                            }
                            break;

                    }


                    }
                    break;
                case 5 :
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17802:142: RULE_HP ( RULE_H )* '.' ( RULE_H )+ RULE_P ( RULE_FS )?
                    {
                    mRULE_HP(); 
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17802:150: ( RULE_H )*
                    loop40:
                    do {
                        int alt40=2;
                        int LA40_0 = input.LA(1);

                        if ( ((LA40_0>='0' && LA40_0<='9')||(LA40_0>='A' && LA40_0<='F')||(LA40_0>='a' && LA40_0<='f')) ) {
                            alt40=1;
                        }


                        switch (alt40) {
                    	case 1 :
                    	    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17802:150: RULE_H
                    	    {
                    	    mRULE_H(); 

                    	    }
                    	    break;

                    	default :
                    	    break loop40;
                        }
                    } while (true);

                    match('.'); 
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17802:162: ( RULE_H )+
                    int cnt41=0;
                    loop41:
                    do {
                        int alt41=2;
                        int LA41_0 = input.LA(1);

                        if ( ((LA41_0>='0' && LA41_0<='9')||(LA41_0>='A' && LA41_0<='F')||(LA41_0>='a' && LA41_0<='f')) ) {
                            alt41=1;
                        }


                        switch (alt41) {
                    	case 1 :
                    	    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17802:162: RULE_H
                    	    {
                    	    mRULE_H(); 

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt41 >= 1 ) break loop41;
                                EarlyExitException eee =
                                    new EarlyExitException(41, input);
                                throw eee;
                        }
                        cnt41++;
                    } while (true);

                    mRULE_P(); 
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17802:177: ( RULE_FS )?
                    int alt42=2;
                    int LA42_0 = input.LA(1);

                    if ( (LA42_0=='F'||LA42_0=='L'||LA42_0=='f'||LA42_0=='l') ) {
                        alt42=1;
                    }
                    switch (alt42) {
                        case 1 :
                            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17802:177: RULE_FS
                            {
                            mRULE_FS(); 

                            }
                            break;

                    }


                    }
                    break;
                case 6 :
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17802:186: RULE_HP ( RULE_H )+ '.' RULE_P ( RULE_FS )?
                    {
                    mRULE_HP(); 
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17802:194: ( RULE_H )+
                    int cnt43=0;
                    loop43:
                    do {
                        int alt43=2;
                        int LA43_0 = input.LA(1);

                        if ( ((LA43_0>='0' && LA43_0<='9')||(LA43_0>='A' && LA43_0<='F')||(LA43_0>='a' && LA43_0<='f')) ) {
                            alt43=1;
                        }


                        switch (alt43) {
                    	case 1 :
                    	    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17802:194: RULE_H
                    	    {
                    	    mRULE_H(); 

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt43 >= 1 ) break loop43;
                                EarlyExitException eee =
                                    new EarlyExitException(43, input);
                                throw eee;
                        }
                        cnt43++;
                    } while (true);

                    match('.'); 
                    mRULE_P(); 
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17802:213: ( RULE_FS )?
                    int alt44=2;
                    int LA44_0 = input.LA(1);

                    if ( (LA44_0=='F'||LA44_0=='L'||LA44_0=='f'||LA44_0=='l') ) {
                        alt44=1;
                    }
                    switch (alt44) {
                        case 1 :
                            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17802:213: RULE_FS
                            {
                            mRULE_FS(); 

                            }
                            break;

                    }


                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_F_CONSTANT"

    // $ANTLR start "RULE_STRING_LITERAL"
    public final void mRULE_STRING_LITERAL() throws RecognitionException {
        try {
            int _type = RULE_STRING_LITERAL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17804:21: ( ( ( RULE_SP )? '\"' (~ ( ( '\"' | '\\\\' | '\\n' ) ) | RULE_ES )* '\"' )+ )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17804:23: ( ( RULE_SP )? '\"' (~ ( ( '\"' | '\\\\' | '\\n' ) ) | RULE_ES )* '\"' )+
            {
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17804:23: ( ( RULE_SP )? '\"' (~ ( ( '\"' | '\\\\' | '\\n' ) ) | RULE_ES )* '\"' )+
            int cnt48=0;
            loop48:
            do {
                int alt48=2;
                int LA48_0 = input.LA(1);

                if ( (LA48_0=='\"'||LA48_0=='L'||LA48_0=='U'||LA48_0=='u') ) {
                    alt48=1;
                }


                switch (alt48) {
            	case 1 :
            	    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17804:24: ( RULE_SP )? '\"' (~ ( ( '\"' | '\\\\' | '\\n' ) ) | RULE_ES )* '\"'
            	    {
            	    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17804:24: ( RULE_SP )?
            	    int alt46=2;
            	    int LA46_0 = input.LA(1);

            	    if ( (LA46_0=='L'||LA46_0=='U'||LA46_0=='u') ) {
            	        alt46=1;
            	    }
            	    switch (alt46) {
            	        case 1 :
            	            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17804:24: RULE_SP
            	            {
            	            mRULE_SP(); 

            	            }
            	            break;

            	    }

            	    match('\"'); 
            	    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17804:37: (~ ( ( '\"' | '\\\\' | '\\n' ) ) | RULE_ES )*
            	    loop47:
            	    do {
            	        int alt47=3;
            	        int LA47_0 = input.LA(1);

            	        if ( ((LA47_0>='\u0000' && LA47_0<='\t')||(LA47_0>='\u000B' && LA47_0<='!')||(LA47_0>='#' && LA47_0<='[')||(LA47_0>=']' && LA47_0<='\uFFFF')) ) {
            	            alt47=1;
            	        }
            	        else if ( (LA47_0=='\\') ) {
            	            alt47=2;
            	        }


            	        switch (alt47) {
            	    	case 1 :
            	    	    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17804:38: ~ ( ( '\"' | '\\\\' | '\\n' ) )
            	    	    {
            	    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='!')||(input.LA(1)>='#' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
            	    	        input.consume();

            	    	    }
            	    	    else {
            	    	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	    	        recover(mse);
            	    	        throw mse;}


            	    	    }
            	    	    break;
            	    	case 2 :
            	    	    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17804:57: RULE_ES
            	    	    {
            	    	    mRULE_ES(); 

            	    	    }
            	    	    break;

            	    	default :
            	    	    break loop47;
            	        }
            	    } while (true);

            	    match('\"'); 

            	    }
            	    break;

            	default :
            	    if ( cnt48 >= 1 ) break loop48;
                        EarlyExitException eee =
                            new EarlyExitException(48, input);
                        throw eee;
                }
                cnt48++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_STRING_LITERAL"

    // $ANTLR start "RULE_ELLIPSIS"
    public final void mRULE_ELLIPSIS() throws RecognitionException {
        try {
            int _type = RULE_ELLIPSIS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17806:15: ( '...' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17806:17: '...'
            {
            match("..."); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ELLIPSIS"

    // $ANTLR start "RULE_ASSIGN"
    public final void mRULE_ASSIGN() throws RecognitionException {
        try {
            int _type = RULE_ASSIGN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17808:13: ( '=' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17808:15: '='
            {
            match('='); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ASSIGN"

    // $ANTLR start "RULE_RIGHT_ASSIGN"
    public final void mRULE_RIGHT_ASSIGN() throws RecognitionException {
        try {
            int _type = RULE_RIGHT_ASSIGN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17810:19: ( '>>=' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17810:21: '>>='
            {
            match(">>="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_RIGHT_ASSIGN"

    // $ANTLR start "RULE_LEFT_ASSIGN"
    public final void mRULE_LEFT_ASSIGN() throws RecognitionException {
        try {
            int _type = RULE_LEFT_ASSIGN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17812:18: ( '<<=' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17812:20: '<<='
            {
            match("<<="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_LEFT_ASSIGN"

    // $ANTLR start "RULE_ADD_ASSIGN"
    public final void mRULE_ADD_ASSIGN() throws RecognitionException {
        try {
            int _type = RULE_ADD_ASSIGN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17814:17: ( '+=' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17814:19: '+='
            {
            match("+="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ADD_ASSIGN"

    // $ANTLR start "RULE_SUB_ASSIGN"
    public final void mRULE_SUB_ASSIGN() throws RecognitionException {
        try {
            int _type = RULE_SUB_ASSIGN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17816:17: ( '-=' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17816:19: '-='
            {
            match("-="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SUB_ASSIGN"

    // $ANTLR start "RULE_MUL_ASSIGN"
    public final void mRULE_MUL_ASSIGN() throws RecognitionException {
        try {
            int _type = RULE_MUL_ASSIGN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17818:17: ( '*=' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17818:19: '*='
            {
            match("*="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_MUL_ASSIGN"

    // $ANTLR start "RULE_DIV_ASSIGN"
    public final void mRULE_DIV_ASSIGN() throws RecognitionException {
        try {
            int _type = RULE_DIV_ASSIGN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17820:17: ( '/=' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17820:19: '/='
            {
            match("/="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_DIV_ASSIGN"

    // $ANTLR start "RULE_MOD_ASSIGN"
    public final void mRULE_MOD_ASSIGN() throws RecognitionException {
        try {
            int _type = RULE_MOD_ASSIGN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17822:17: ( '%=' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17822:19: '%='
            {
            match("%="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_MOD_ASSIGN"

    // $ANTLR start "RULE_AND_ASSIGN"
    public final void mRULE_AND_ASSIGN() throws RecognitionException {
        try {
            int _type = RULE_AND_ASSIGN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17824:17: ( '&=' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17824:19: '&='
            {
            match("&="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_AND_ASSIGN"

    // $ANTLR start "RULE_XOR_ASSIGN"
    public final void mRULE_XOR_ASSIGN() throws RecognitionException {
        try {
            int _type = RULE_XOR_ASSIGN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17826:17: ( '^=' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17826:19: '^='
            {
            match("^="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_XOR_ASSIGN"

    // $ANTLR start "RULE_OR_ASSIGN"
    public final void mRULE_OR_ASSIGN() throws RecognitionException {
        try {
            int _type = RULE_OR_ASSIGN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17828:16: ( '|=' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17828:18: '|='
            {
            match("|="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_OR_ASSIGN"

    // $ANTLR start "RULE_RIGHT_OP"
    public final void mRULE_RIGHT_OP() throws RecognitionException {
        try {
            int _type = RULE_RIGHT_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17830:15: ( '>>' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17830:17: '>>'
            {
            match(">>"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_RIGHT_OP"

    // $ANTLR start "RULE_LEFT_OP"
    public final void mRULE_LEFT_OP() throws RecognitionException {
        try {
            int _type = RULE_LEFT_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17832:14: ( '<<' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17832:16: '<<'
            {
            match("<<"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_LEFT_OP"

    // $ANTLR start "RULE_INC_OP"
    public final void mRULE_INC_OP() throws RecognitionException {
        try {
            int _type = RULE_INC_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17834:13: ( '++' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17834:15: '++'
            {
            match("++"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_INC_OP"

    // $ANTLR start "RULE_DEC_OP"
    public final void mRULE_DEC_OP() throws RecognitionException {
        try {
            int _type = RULE_DEC_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17836:13: ( '--' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17836:15: '--'
            {
            match("--"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_DEC_OP"

    // $ANTLR start "RULE_PTR_OP"
    public final void mRULE_PTR_OP() throws RecognitionException {
        try {
            int _type = RULE_PTR_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17838:13: ( '->' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17838:15: '->'
            {
            match("->"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_PTR_OP"

    // $ANTLR start "RULE_LAND_OP"
    public final void mRULE_LAND_OP() throws RecognitionException {
        try {
            int _type = RULE_LAND_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17840:14: ( '&&' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17840:16: '&&'
            {
            match("&&"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_LAND_OP"

    // $ANTLR start "RULE_LOR_OP"
    public final void mRULE_LOR_OP() throws RecognitionException {
        try {
            int _type = RULE_LOR_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17842:13: ( '||' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17842:15: '||'
            {
            match("||"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_LOR_OP"

    // $ANTLR start "RULE_LE_OP"
    public final void mRULE_LE_OP() throws RecognitionException {
        try {
            int _type = RULE_LE_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17844:12: ( '<=' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17844:14: '<='
            {
            match("<="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_LE_OP"

    // $ANTLR start "RULE_GE_OP"
    public final void mRULE_GE_OP() throws RecognitionException {
        try {
            int _type = RULE_GE_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17846:12: ( '>=' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17846:14: '>='
            {
            match(">="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_GE_OP"

    // $ANTLR start "RULE_EQ_OP"
    public final void mRULE_EQ_OP() throws RecognitionException {
        try {
            int _type = RULE_EQ_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17848:12: ( '==' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17848:14: '=='
            {
            match("=="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_EQ_OP"

    // $ANTLR start "RULE_NE_OP"
    public final void mRULE_NE_OP() throws RecognitionException {
        try {
            int _type = RULE_NE_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17850:12: ( '!=' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17850:14: '!='
            {
            match("!="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_NE_OP"

    // $ANTLR start "RULE_SEMICOLON"
    public final void mRULE_SEMICOLON() throws RecognitionException {
        try {
            int _type = RULE_SEMICOLON;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17852:16: ( ';' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17852:18: ';'
            {
            match(';'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SEMICOLON"

    // $ANTLR start "RULE_LCBRACKET"
    public final void mRULE_LCBRACKET() throws RecognitionException {
        try {
            int _type = RULE_LCBRACKET;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17854:16: ( ( '{' | '<%' ) )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17854:18: ( '{' | '<%' )
            {
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17854:18: ( '{' | '<%' )
            int alt49=2;
            int LA49_0 = input.LA(1);

            if ( (LA49_0=='{') ) {
                alt49=1;
            }
            else if ( (LA49_0=='<') ) {
                alt49=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 49, 0, input);

                throw nvae;
            }
            switch (alt49) {
                case 1 :
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17854:19: '{'
                    {
                    match('{'); 

                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17854:23: '<%'
                    {
                    match("<%"); 


                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_LCBRACKET"

    // $ANTLR start "RULE_RCBRACKET"
    public final void mRULE_RCBRACKET() throws RecognitionException {
        try {
            int _type = RULE_RCBRACKET;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17856:16: ( ( '}' | '%>' ) )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17856:18: ( '}' | '%>' )
            {
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17856:18: ( '}' | '%>' )
            int alt50=2;
            int LA50_0 = input.LA(1);

            if ( (LA50_0=='}') ) {
                alt50=1;
            }
            else if ( (LA50_0=='%') ) {
                alt50=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 50, 0, input);

                throw nvae;
            }
            switch (alt50) {
                case 1 :
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17856:19: '}'
                    {
                    match('}'); 

                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17856:23: '%>'
                    {
                    match("%>"); 


                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_RCBRACKET"

    // $ANTLR start "RULE_COMMA"
    public final void mRULE_COMMA() throws RecognitionException {
        try {
            int _type = RULE_COMMA;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17858:12: ( ',' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17858:14: ','
            {
            match(','); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_COMMA"

    // $ANTLR start "RULE_COLON"
    public final void mRULE_COLON() throws RecognitionException {
        try {
            int _type = RULE_COLON;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17860:12: ( ':' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17860:14: ':'
            {
            match(':'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_COLON"

    // $ANTLR start "RULE_LPAREN"
    public final void mRULE_LPAREN() throws RecognitionException {
        try {
            int _type = RULE_LPAREN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17862:13: ( '(' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17862:15: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_LPAREN"

    // $ANTLR start "RULE_RPAREN"
    public final void mRULE_RPAREN() throws RecognitionException {
        try {
            int _type = RULE_RPAREN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17864:13: ( ')' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17864:15: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_RPAREN"

    // $ANTLR start "RULE_LBRACKET"
    public final void mRULE_LBRACKET() throws RecognitionException {
        try {
            int _type = RULE_LBRACKET;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17866:15: ( ( '[' | '<:' ) )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17866:17: ( '[' | '<:' )
            {
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17866:17: ( '[' | '<:' )
            int alt51=2;
            int LA51_0 = input.LA(1);

            if ( (LA51_0=='[') ) {
                alt51=1;
            }
            else if ( (LA51_0=='<') ) {
                alt51=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 51, 0, input);

                throw nvae;
            }
            switch (alt51) {
                case 1 :
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17866:18: '['
                    {
                    match('['); 

                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17866:22: '<:'
                    {
                    match("<:"); 


                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_LBRACKET"

    // $ANTLR start "RULE_RBRACKET"
    public final void mRULE_RBRACKET() throws RecognitionException {
        try {
            int _type = RULE_RBRACKET;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17868:15: ( ( ']' | ':>' ) )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17868:17: ( ']' | ':>' )
            {
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17868:17: ( ']' | ':>' )
            int alt52=2;
            int LA52_0 = input.LA(1);

            if ( (LA52_0==']') ) {
                alt52=1;
            }
            else if ( (LA52_0==':') ) {
                alt52=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 52, 0, input);

                throw nvae;
            }
            switch (alt52) {
                case 1 :
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17868:18: ']'
                    {
                    match(']'); 

                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17868:22: ':>'
                    {
                    match(":>"); 


                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_RBRACKET"

    // $ANTLR start "RULE_DOT"
    public final void mRULE_DOT() throws RecognitionException {
        try {
            int _type = RULE_DOT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17870:10: ( '.' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17870:12: '.'
            {
            match('.'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_DOT"

    // $ANTLR start "RULE_AND_OP"
    public final void mRULE_AND_OP() throws RecognitionException {
        try {
            int _type = RULE_AND_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17872:13: ( '&' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17872:15: '&'
            {
            match('&'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_AND_OP"

    // $ANTLR start "RULE_NOT_OP"
    public final void mRULE_NOT_OP() throws RecognitionException {
        try {
            int _type = RULE_NOT_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17874:13: ( '!' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17874:15: '!'
            {
            match('!'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_NOT_OP"

    // $ANTLR start "RULE_TILDE"
    public final void mRULE_TILDE() throws RecognitionException {
        try {
            int _type = RULE_TILDE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17876:12: ( '~' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17876:14: '~'
            {
            match('~'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_TILDE"

    // $ANTLR start "RULE_MINUS_OP"
    public final void mRULE_MINUS_OP() throws RecognitionException {
        try {
            int _type = RULE_MINUS_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17878:15: ( '-' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17878:17: '-'
            {
            match('-'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_MINUS_OP"

    // $ANTLR start "RULE_ADD_OP"
    public final void mRULE_ADD_OP() throws RecognitionException {
        try {
            int _type = RULE_ADD_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17880:13: ( '+' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17880:15: '+'
            {
            match('+'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ADD_OP"

    // $ANTLR start "RULE_MULT_OP"
    public final void mRULE_MULT_OP() throws RecognitionException {
        try {
            int _type = RULE_MULT_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17882:14: ( '*' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17882:16: '*'
            {
            match('*'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_MULT_OP"

    // $ANTLR start "RULE_DIV_OP"
    public final void mRULE_DIV_OP() throws RecognitionException {
        try {
            int _type = RULE_DIV_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17884:13: ( '/' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17884:15: '/'
            {
            match('/'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_DIV_OP"

    // $ANTLR start "RULE_MOD_OP"
    public final void mRULE_MOD_OP() throws RecognitionException {
        try {
            int _type = RULE_MOD_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17886:13: ( '%' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17886:15: '%'
            {
            match('%'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_MOD_OP"

    // $ANTLR start "RULE_L_OP"
    public final void mRULE_L_OP() throws RecognitionException {
        try {
            int _type = RULE_L_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17888:11: ( '<' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17888:13: '<'
            {
            match('<'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_L_OP"

    // $ANTLR start "RULE_G_OP"
    public final void mRULE_G_OP() throws RecognitionException {
        try {
            int _type = RULE_G_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17890:11: ( '>' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17890:13: '>'
            {
            match('>'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_G_OP"

    // $ANTLR start "RULE_XOR_OP"
    public final void mRULE_XOR_OP() throws RecognitionException {
        try {
            int _type = RULE_XOR_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17892:13: ( '^' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17892:15: '^'
            {
            match('^'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_XOR_OP"

    // $ANTLR start "RULE_OR_OP"
    public final void mRULE_OR_OP() throws RecognitionException {
        try {
            int _type = RULE_OR_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17894:12: ( '|' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17894:14: '|'
            {
            match('|'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_OR_OP"

    // $ANTLR start "RULE_Q_OP"
    public final void mRULE_Q_OP() throws RecognitionException {
        try {
            int _type = RULE_Q_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17896:11: ( '?' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17896:13: '?'
            {
            match('?'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_Q_OP"

    // $ANTLR start "RULE_INCLUDE"
    public final void mRULE_INCLUDE() throws RecognitionException {
        try {
            int _type = RULE_INCLUDE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17898:14: ( '#include' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17898:16: '#include'
            {
            match("#include"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_INCLUDE"

    // $ANTLR start "RULE_DQUOTE"
    public final void mRULE_DQUOTE() throws RecognitionException {
        try {
            int _type = RULE_DQUOTE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17900:13: ( '\"' )
            // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:17900:15: '\"'
            {
            match('\"'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_DQUOTE"

    public void mTokens() throws RecognitionException {
        // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:8: ( RULE_ML_COMMENT | RULE_SL_COMMENT | RULE_WS | RULE_AUTO | RULE_BREAK | RULE_CASE | RULE_CHAR | RULE_CONST | RULE_CONTINUE | RULE_DEFAULT | RULE_DO | RULE_DOUBLE | RULE_ELSE | RULE_ENUM | RULE_EXTERN | RULE_FLOAT | RULE_FOR | RULE_GOTO | RULE_IF | RULE_INLINE | RULE_INT | RULE_LONG | RULE_REGISTER | RULE_RESTRICT | RULE_RETURN | RULE_SHORT | RULE_SIGNED | RULE_SIZEOF | RULE_STATIC | RULE_STRUCT | RULE_SWITCH | RULE_TYPEDEF | RULE_UNION | RULE_UNSIGNED | RULE_VOID | RULE_VOLATILE | RULE_WHILE | RULE_ALIGNAS | RULE_ALIGNOF | RULE_ATOMIC | RULE_BOOL | RULE_COMPLEX | RULE_GENERIC | RULE_IMAGINARY | RULE_NORETURN | RULE_STATIC_ASSERT | RULE_THREAD_LOCAL | RULE_FUNC_NAME | RULE_IDENTIFIER | RULE_CUSTOM | RULE_I_CONSTANT | RULE_F_CONSTANT | RULE_STRING_LITERAL | RULE_ELLIPSIS | RULE_ASSIGN | RULE_RIGHT_ASSIGN | RULE_LEFT_ASSIGN | RULE_ADD_ASSIGN | RULE_SUB_ASSIGN | RULE_MUL_ASSIGN | RULE_DIV_ASSIGN | RULE_MOD_ASSIGN | RULE_AND_ASSIGN | RULE_XOR_ASSIGN | RULE_OR_ASSIGN | RULE_RIGHT_OP | RULE_LEFT_OP | RULE_INC_OP | RULE_DEC_OP | RULE_PTR_OP | RULE_LAND_OP | RULE_LOR_OP | RULE_LE_OP | RULE_GE_OP | RULE_EQ_OP | RULE_NE_OP | RULE_SEMICOLON | RULE_LCBRACKET | RULE_RCBRACKET | RULE_COMMA | RULE_COLON | RULE_LPAREN | RULE_RPAREN | RULE_LBRACKET | RULE_RBRACKET | RULE_DOT | RULE_AND_OP | RULE_NOT_OP | RULE_TILDE | RULE_MINUS_OP | RULE_ADD_OP | RULE_MULT_OP | RULE_DIV_OP | RULE_MOD_OP | RULE_L_OP | RULE_G_OP | RULE_XOR_OP | RULE_OR_OP | RULE_Q_OP | RULE_INCLUDE | RULE_DQUOTE )
        int alt53=101;
        alt53 = dfa53.predict(input);
        switch (alt53) {
            case 1 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:10: RULE_ML_COMMENT
                {
                mRULE_ML_COMMENT(); 

                }
                break;
            case 2 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:26: RULE_SL_COMMENT
                {
                mRULE_SL_COMMENT(); 

                }
                break;
            case 3 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:42: RULE_WS
                {
                mRULE_WS(); 

                }
                break;
            case 4 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:50: RULE_AUTO
                {
                mRULE_AUTO(); 

                }
                break;
            case 5 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:60: RULE_BREAK
                {
                mRULE_BREAK(); 

                }
                break;
            case 6 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:71: RULE_CASE
                {
                mRULE_CASE(); 

                }
                break;
            case 7 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:81: RULE_CHAR
                {
                mRULE_CHAR(); 

                }
                break;
            case 8 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:91: RULE_CONST
                {
                mRULE_CONST(); 

                }
                break;
            case 9 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:102: RULE_CONTINUE
                {
                mRULE_CONTINUE(); 

                }
                break;
            case 10 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:116: RULE_DEFAULT
                {
                mRULE_DEFAULT(); 

                }
                break;
            case 11 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:129: RULE_DO
                {
                mRULE_DO(); 

                }
                break;
            case 12 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:137: RULE_DOUBLE
                {
                mRULE_DOUBLE(); 

                }
                break;
            case 13 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:149: RULE_ELSE
                {
                mRULE_ELSE(); 

                }
                break;
            case 14 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:159: RULE_ENUM
                {
                mRULE_ENUM(); 

                }
                break;
            case 15 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:169: RULE_EXTERN
                {
                mRULE_EXTERN(); 

                }
                break;
            case 16 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:181: RULE_FLOAT
                {
                mRULE_FLOAT(); 

                }
                break;
            case 17 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:192: RULE_FOR
                {
                mRULE_FOR(); 

                }
                break;
            case 18 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:201: RULE_GOTO
                {
                mRULE_GOTO(); 

                }
                break;
            case 19 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:211: RULE_IF
                {
                mRULE_IF(); 

                }
                break;
            case 20 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:219: RULE_INLINE
                {
                mRULE_INLINE(); 

                }
                break;
            case 21 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:231: RULE_INT
                {
                mRULE_INT(); 

                }
                break;
            case 22 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:240: RULE_LONG
                {
                mRULE_LONG(); 

                }
                break;
            case 23 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:250: RULE_REGISTER
                {
                mRULE_REGISTER(); 

                }
                break;
            case 24 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:264: RULE_RESTRICT
                {
                mRULE_RESTRICT(); 

                }
                break;
            case 25 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:278: RULE_RETURN
                {
                mRULE_RETURN(); 

                }
                break;
            case 26 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:290: RULE_SHORT
                {
                mRULE_SHORT(); 

                }
                break;
            case 27 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:301: RULE_SIGNED
                {
                mRULE_SIGNED(); 

                }
                break;
            case 28 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:313: RULE_SIZEOF
                {
                mRULE_SIZEOF(); 

                }
                break;
            case 29 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:325: RULE_STATIC
                {
                mRULE_STATIC(); 

                }
                break;
            case 30 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:337: RULE_STRUCT
                {
                mRULE_STRUCT(); 

                }
                break;
            case 31 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:349: RULE_SWITCH
                {
                mRULE_SWITCH(); 

                }
                break;
            case 32 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:361: RULE_TYPEDEF
                {
                mRULE_TYPEDEF(); 

                }
                break;
            case 33 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:374: RULE_UNION
                {
                mRULE_UNION(); 

                }
                break;
            case 34 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:385: RULE_UNSIGNED
                {
                mRULE_UNSIGNED(); 

                }
                break;
            case 35 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:399: RULE_VOID
                {
                mRULE_VOID(); 

                }
                break;
            case 36 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:409: RULE_VOLATILE
                {
                mRULE_VOLATILE(); 

                }
                break;
            case 37 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:423: RULE_WHILE
                {
                mRULE_WHILE(); 

                }
                break;
            case 38 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:434: RULE_ALIGNAS
                {
                mRULE_ALIGNAS(); 

                }
                break;
            case 39 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:447: RULE_ALIGNOF
                {
                mRULE_ALIGNOF(); 

                }
                break;
            case 40 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:460: RULE_ATOMIC
                {
                mRULE_ATOMIC(); 

                }
                break;
            case 41 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:472: RULE_BOOL
                {
                mRULE_BOOL(); 

                }
                break;
            case 42 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:482: RULE_COMPLEX
                {
                mRULE_COMPLEX(); 

                }
                break;
            case 43 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:495: RULE_GENERIC
                {
                mRULE_GENERIC(); 

                }
                break;
            case 44 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:508: RULE_IMAGINARY
                {
                mRULE_IMAGINARY(); 

                }
                break;
            case 45 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:523: RULE_NORETURN
                {
                mRULE_NORETURN(); 

                }
                break;
            case 46 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:537: RULE_STATIC_ASSERT
                {
                mRULE_STATIC_ASSERT(); 

                }
                break;
            case 47 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:556: RULE_THREAD_LOCAL
                {
                mRULE_THREAD_LOCAL(); 

                }
                break;
            case 48 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:574: RULE_FUNC_NAME
                {
                mRULE_FUNC_NAME(); 

                }
                break;
            case 49 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:589: RULE_IDENTIFIER
                {
                mRULE_IDENTIFIER(); 

                }
                break;
            case 50 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:605: RULE_CUSTOM
                {
                mRULE_CUSTOM(); 

                }
                break;
            case 51 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:617: RULE_I_CONSTANT
                {
                mRULE_I_CONSTANT(); 

                }
                break;
            case 52 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:633: RULE_F_CONSTANT
                {
                mRULE_F_CONSTANT(); 

                }
                break;
            case 53 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:649: RULE_STRING_LITERAL
                {
                mRULE_STRING_LITERAL(); 

                }
                break;
            case 54 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:669: RULE_ELLIPSIS
                {
                mRULE_ELLIPSIS(); 

                }
                break;
            case 55 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:683: RULE_ASSIGN
                {
                mRULE_ASSIGN(); 

                }
                break;
            case 56 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:695: RULE_RIGHT_ASSIGN
                {
                mRULE_RIGHT_ASSIGN(); 

                }
                break;
            case 57 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:713: RULE_LEFT_ASSIGN
                {
                mRULE_LEFT_ASSIGN(); 

                }
                break;
            case 58 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:730: RULE_ADD_ASSIGN
                {
                mRULE_ADD_ASSIGN(); 

                }
                break;
            case 59 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:746: RULE_SUB_ASSIGN
                {
                mRULE_SUB_ASSIGN(); 

                }
                break;
            case 60 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:762: RULE_MUL_ASSIGN
                {
                mRULE_MUL_ASSIGN(); 

                }
                break;
            case 61 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:778: RULE_DIV_ASSIGN
                {
                mRULE_DIV_ASSIGN(); 

                }
                break;
            case 62 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:794: RULE_MOD_ASSIGN
                {
                mRULE_MOD_ASSIGN(); 

                }
                break;
            case 63 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:810: RULE_AND_ASSIGN
                {
                mRULE_AND_ASSIGN(); 

                }
                break;
            case 64 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:826: RULE_XOR_ASSIGN
                {
                mRULE_XOR_ASSIGN(); 

                }
                break;
            case 65 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:842: RULE_OR_ASSIGN
                {
                mRULE_OR_ASSIGN(); 

                }
                break;
            case 66 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:857: RULE_RIGHT_OP
                {
                mRULE_RIGHT_OP(); 

                }
                break;
            case 67 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:871: RULE_LEFT_OP
                {
                mRULE_LEFT_OP(); 

                }
                break;
            case 68 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:884: RULE_INC_OP
                {
                mRULE_INC_OP(); 

                }
                break;
            case 69 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:896: RULE_DEC_OP
                {
                mRULE_DEC_OP(); 

                }
                break;
            case 70 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:908: RULE_PTR_OP
                {
                mRULE_PTR_OP(); 

                }
                break;
            case 71 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:920: RULE_LAND_OP
                {
                mRULE_LAND_OP(); 

                }
                break;
            case 72 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:933: RULE_LOR_OP
                {
                mRULE_LOR_OP(); 

                }
                break;
            case 73 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:945: RULE_LE_OP
                {
                mRULE_LE_OP(); 

                }
                break;
            case 74 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:956: RULE_GE_OP
                {
                mRULE_GE_OP(); 

                }
                break;
            case 75 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:967: RULE_EQ_OP
                {
                mRULE_EQ_OP(); 

                }
                break;
            case 76 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:978: RULE_NE_OP
                {
                mRULE_NE_OP(); 

                }
                break;
            case 77 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:989: RULE_SEMICOLON
                {
                mRULE_SEMICOLON(); 

                }
                break;
            case 78 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:1004: RULE_LCBRACKET
                {
                mRULE_LCBRACKET(); 

                }
                break;
            case 79 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:1019: RULE_RCBRACKET
                {
                mRULE_RCBRACKET(); 

                }
                break;
            case 80 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:1034: RULE_COMMA
                {
                mRULE_COMMA(); 

                }
                break;
            case 81 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:1045: RULE_COLON
                {
                mRULE_COLON(); 

                }
                break;
            case 82 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:1056: RULE_LPAREN
                {
                mRULE_LPAREN(); 

                }
                break;
            case 83 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:1068: RULE_RPAREN
                {
                mRULE_RPAREN(); 

                }
                break;
            case 84 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:1080: RULE_LBRACKET
                {
                mRULE_LBRACKET(); 

                }
                break;
            case 85 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:1094: RULE_RBRACKET
                {
                mRULE_RBRACKET(); 

                }
                break;
            case 86 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:1108: RULE_DOT
                {
                mRULE_DOT(); 

                }
                break;
            case 87 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:1117: RULE_AND_OP
                {
                mRULE_AND_OP(); 

                }
                break;
            case 88 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:1129: RULE_NOT_OP
                {
                mRULE_NOT_OP(); 

                }
                break;
            case 89 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:1141: RULE_TILDE
                {
                mRULE_TILDE(); 

                }
                break;
            case 90 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:1152: RULE_MINUS_OP
                {
                mRULE_MINUS_OP(); 

                }
                break;
            case 91 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:1166: RULE_ADD_OP
                {
                mRULE_ADD_OP(); 

                }
                break;
            case 92 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:1178: RULE_MULT_OP
                {
                mRULE_MULT_OP(); 

                }
                break;
            case 93 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:1191: RULE_DIV_OP
                {
                mRULE_DIV_OP(); 

                }
                break;
            case 94 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:1203: RULE_MOD_OP
                {
                mRULE_MOD_OP(); 

                }
                break;
            case 95 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:1215: RULE_L_OP
                {
                mRULE_L_OP(); 

                }
                break;
            case 96 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:1225: RULE_G_OP
                {
                mRULE_G_OP(); 

                }
                break;
            case 97 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:1235: RULE_XOR_OP
                {
                mRULE_XOR_OP(); 

                }
                break;
            case 98 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:1247: RULE_OR_OP
                {
                mRULE_OR_OP(); 

                }
                break;
            case 99 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:1258: RULE_Q_OP
                {
                mRULE_Q_OP(); 

                }
                break;
            case 100 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:1268: RULE_INCLUDE
                {
                mRULE_INCLUDE(); 

                }
                break;
            case 101 :
                // ../org.xtext.dop.clang.ui/src-gen/org/xtext/dop/clang/ui/contentassist/antlr/internal/InternalClang.g:1:1281: RULE_DQUOTE
                {
                mRULE_DQUOTE(); 

                }
                break;

        }

    }


    protected DFA45 dfa45 = new DFA45(this);
    protected DFA53 dfa53 = new DFA53(this);
    static final String DFA45_eotS =
        "\6\uffff\1\12\7\uffff";
    static final String DFA45_eofS =
        "\16\uffff";
    static final String DFA45_minS =
        "\2\56\1\uffff\3\56\1\60\1\uffff\1\56\2\uffff\1\60\2\uffff";
    static final String DFA45_maxS =
        "\1\71\1\170\1\uffff\1\145\2\146\1\71\1\uffff\1\160\2\uffff\1\160"+
        "\2\uffff";
    static final String DFA45_acceptS =
        "\2\uffff\1\2\4\uffff\1\1\1\uffff\1\5\1\3\1\uffff\1\4\1\6";
    static final String DFA45_specialS =
        "\16\uffff}>";
    static final String[] DFA45_transitionS = {
            "\1\2\1\uffff\1\1\11\3",
            "\1\6\1\uffff\12\3\13\uffff\1\7\22\uffff\1\5\14\uffff\1\7\22"+
            "\uffff\1\4",
            "",
            "\1\6\1\uffff\12\3\13\uffff\1\7\37\uffff\1\7",
            "\1\11\1\uffff\12\10\7\uffff\6\10\32\uffff\6\10",
            "\1\11\1\uffff\12\10\7\uffff\6\10\32\uffff\6\10",
            "\12\2",
            "",
            "\1\13\1\uffff\12\10\7\uffff\6\10\11\uffff\1\14\20\uffff\6"+
            "\10\11\uffff\1\14",
            "",
            "",
            "\12\11\7\uffff\6\11\11\uffff\1\15\20\uffff\6\11\11\uffff\1"+
            "\15",
            "",
            ""
    };

    static final short[] DFA45_eot = DFA.unpackEncodedString(DFA45_eotS);
    static final short[] DFA45_eof = DFA.unpackEncodedString(DFA45_eofS);
    static final char[] DFA45_min = DFA.unpackEncodedStringToUnsignedChars(DFA45_minS);
    static final char[] DFA45_max = DFA.unpackEncodedStringToUnsignedChars(DFA45_maxS);
    static final short[] DFA45_accept = DFA.unpackEncodedString(DFA45_acceptS);
    static final short[] DFA45_special = DFA.unpackEncodedString(DFA45_specialS);
    static final short[][] DFA45_transition;

    static {
        int numStates = DFA45_transitionS.length;
        DFA45_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA45_transition[i] = DFA.unpackEncodedString(DFA45_transitionS[i]);
        }
    }

    class DFA45 extends DFA {

        public DFA45(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 45;
            this.eot = DFA45_eot;
            this.eof = DFA45_eof;
            this.min = DFA45_min;
            this.max = DFA45_max;
            this.accept = DFA45_accept;
            this.special = DFA45_special;
            this.transition = DFA45_transition;
        }
        public String getDescription() {
            return "17802:19: ( ( RULE_D )+ RULE_E ( RULE_FS )? | ( RULE_D )* '.' ( RULE_D )+ ( RULE_E )? ( RULE_FS )? | ( RULE_D )+ '.' ( RULE_E )? ( RULE_FS )? | RULE_HP ( RULE_H )+ RULE_P ( RULE_FS )? | RULE_HP ( RULE_H )* '.' ( RULE_H )+ RULE_P ( RULE_FS )? | RULE_HP ( RULE_H )+ '.' RULE_P ( RULE_FS )? )";
        }
    }
    static final String DFA53_eotS =
        "\1\uffff\1\66\1\uffff\21\27\1\uffff\2\30\2\uffff\1\141\1\27\1\142"+
        "\1\144\1\147\1\152\1\155\1\161\1\163\1\165\1\170\1\172\1\175\1\177"+
        "\4\uffff\1\u0080\13\uffff\6\27\1\u0088\6\27\1\u008f\12\27\1\uffff"+
        "\13\27\2\uffff\1\30\1\uffff\1\30\5\uffff\1\u00ae\2\uffff\1\u00b0"+
        "\30\uffff\7\27\1\uffff\4\27\1\u00bd\1\27\1\uffff\1\27\1\u00c0\32"+
        "\27\1\30\4\uffff\1\u00db\1\27\1\u00dd\1\u00de\4\27\1\u00e3\1\u00e4"+
        "\2\27\1\uffff\1\u00e7\1\27\1\uffff\1\u00e9\14\27\1\u00f6\14\27\1"+
        "\uffff\1\u0103\2\uffff\1\u0104\3\27\2\uffff\1\27\1\u0109\1\uffff"+
        "\1\27\1\uffff\3\27\1\u010e\6\27\1\u0115\1\27\1\uffff\1\27\1\u0118"+
        "\2\27\1\u011b\7\27\2\uffff\2\27\1\u0125\1\u0126\1\uffff\1\u0127"+
        "\2\27\1\u012a\1\uffff\1\u012b\1\u012c\1\u012d\1\u012e\1\u012f\1"+
        "\27\1\uffff\2\27\1\uffff\2\27\1\uffff\10\27\1\u013e\3\uffff\2\27"+
        "\6\uffff\1\u0141\4\27\1\u0146\7\27\1\u014e\1\uffff\1\u014f\1\u0150"+
        "\1\uffff\1\u0151\1\u0152\1\u0153\1\u0154\1\uffff\1\u0155\1\u0156"+
        "\4\27\1\u015b\11\uffff\1\27\1\u015d\2\27\1\uffff\1\u0160\1\uffff"+
        "\2\27\1\uffff\5\27\1\u0168\1\u0169\2\uffff";
    static final String DFA53_eofS =
        "\u016a\uffff";
    static final String DFA53_minS =
        "\1\10\1\52\1\uffff\1\165\1\162\1\141\1\145\2\154\1\157\1\146\1"+
        "\157\1\145\1\150\1\171\1\42\1\157\1\150\1\101\1\42\1\uffff\2\56"+
        "\2\uffff\1\56\1\42\1\0\2\75\1\45\1\53\1\55\2\75\1\46\3\75\4\uffff"+
        "\1\76\13\uffff\1\164\1\145\1\163\1\141\1\156\1\146\1\60\1\163\1"+
        "\165\1\164\1\157\1\162\1\164\1\60\1\154\1\156\1\147\1\157\1\147"+
        "\1\141\1\151\1\160\1\151\1\42\1\uffff\2\151\1\154\2\157\1\145\1"+
        "\155\1\157\1\164\1\150\1\146\3\56\1\uffff\1\56\5\uffff\1\75\2\uffff"+
        "\1\75\30\uffff\1\157\1\141\1\145\1\162\1\163\1\141\1\142\1\uffff"+
        "\1\145\1\155\1\145\1\141\1\60\1\157\1\uffff\1\151\1\60\1\147\1\151"+
        "\1\164\1\165\1\162\1\156\1\145\1\164\1\165\1\164\1\145\1\157\1\151"+
        "\1\144\1\141\1\154\1\151\2\157\1\155\1\156\1\141\1\162\1\141\1\162"+
        "\1\165\1\56\4\uffff\1\60\1\153\2\60\1\164\1\151\1\165\1\154\2\60"+
        "\1\162\1\164\1\uffff\1\60\1\156\1\uffff\1\60\1\163\2\162\1\164\1"+
        "\145\1\157\1\151\2\143\1\144\1\156\1\147\1\60\1\164\1\145\1\147"+
        "\1\155\1\154\1\160\1\145\1\147\1\145\1\164\1\145\1\156\1\uffff\1"+
        "\60\2\uffff\1\60\1\156\1\154\1\145\2\uffff\1\156\1\60\1\uffff\1"+
        "\145\1\uffff\1\164\1\151\1\156\1\60\1\144\1\146\1\143\1\164\1\150"+
        "\1\145\1\60\1\156\1\uffff\1\151\1\60\1\156\1\151\1\60\1\154\1\162"+
        "\1\151\1\164\1\151\1\141\1\143\2\uffff\1\165\1\164\2\60\1\uffff"+
        "\1\60\1\145\1\143\1\60\1\uffff\5\60\1\146\1\uffff\1\145\1\154\1"+
        "\uffff\1\141\1\143\1\uffff\1\145\1\151\1\156\1\165\1\143\1\144\1"+
        "\137\1\145\1\60\3\uffff\1\162\1\164\6\uffff\1\60\1\144\1\145\1\163"+
        "\1\146\1\60\1\170\1\143\1\141\1\162\3\137\1\60\1\uffff\2\60\1\uffff"+
        "\4\60\1\uffff\2\60\1\162\1\156\1\141\1\154\1\60\11\uffff\1\171\1"+
        "\60\1\163\1\157\1\uffff\1\60\1\uffff\1\163\1\143\1\uffff\1\145\1"+
        "\141\1\162\1\154\1\164\2\60\2\uffff";
    static final String DFA53_maxS =
        "\1\176\1\75\1\uffff\1\165\1\162\2\157\1\170\2\157\1\156\1\157\1"+
        "\145\1\167\1\171\1\156\1\157\1\150\1\137\1\47\1\uffff\1\170\1\145"+
        "\2\uffff\1\71\1\47\1\uffff\1\75\1\76\2\75\1\76\1\75\1\76\2\75\1"+
        "\174\1\75\4\uffff\1\76\13\uffff\1\164\1\145\1\163\1\141\1\156\1"+
        "\146\1\172\1\163\1\165\1\164\1\157\1\162\1\164\1\172\1\164\1\156"+
        "\1\164\1\157\1\172\1\162\1\151\1\160\1\163\1\42\1\uffff\1\154\1"+
        "\151\1\164\2\157\1\145\1\155\1\157\1\164\1\150\3\146\1\145\1\uffff"+
        "\1\145\5\uffff\1\75\2\uffff\1\75\30\uffff\1\157\1\141\1\145\1\162"+
        "\1\164\1\141\1\142\1\uffff\1\145\1\155\1\145\1\141\1\172\1\157\1"+
        "\uffff\1\151\1\172\1\147\1\151\1\164\1\165\1\162\1\156\1\145\1\164"+
        "\1\165\1\164\1\145\1\157\1\151\1\144\1\141\1\154\1\151\2\157\1\155"+
        "\1\156\1\141\1\162\1\141\1\162\1\165\1\160\4\uffff\1\172\1\153\2"+
        "\172\1\164\1\151\1\165\1\154\2\172\1\162\1\164\1\uffff\1\172\1\156"+
        "\1\uffff\1\172\1\163\2\162\1\164\1\145\1\157\1\151\2\143\1\144\1"+
        "\156\1\147\1\172\1\164\1\145\1\147\1\155\1\154\1\160\1\145\1\147"+
        "\1\145\1\164\1\145\1\156\1\uffff\1\172\2\uffff\1\172\1\156\1\154"+
        "\1\145\2\uffff\1\156\1\172\1\uffff\1\145\1\uffff\1\164\1\151\1\156"+
        "\1\172\1\144\1\146\1\143\1\164\1\150\1\145\1\172\1\156\1\uffff\1"+
        "\151\1\172\1\156\1\151\1\172\1\154\1\162\1\151\1\164\1\151\1\141"+
        "\1\143\2\uffff\1\165\1\164\2\172\1\uffff\1\172\1\145\1\143\1\172"+
        "\1\uffff\5\172\1\146\1\uffff\1\145\1\154\1\uffff\1\157\1\143\1\uffff"+
        "\1\145\1\151\1\156\1\165\1\143\1\144\1\137\1\145\1\172\3\uffff\1"+
        "\162\1\164\6\uffff\1\172\1\144\1\145\1\163\1\146\1\172\1\170\1\143"+
        "\1\141\1\162\3\137\1\172\1\uffff\2\172\1\uffff\4\172\1\uffff\2\172"+
        "\1\162\1\156\1\141\1\154\1\172\11\uffff\1\171\1\172\1\163\1\157"+
        "\1\uffff\1\172\1\uffff\1\163\1\143\1\uffff\1\145\1\141\1\162\1\154"+
        "\1\164\2\172\2\uffff";
    static final String DFA53_acceptS =
        "\2\uffff\1\3\21\uffff\1\62\2\uffff\1\61\1\63\16\uffff\1\115\1\116"+
        "\1\117\1\120\1\uffff\1\122\1\123\1\124\1\125\1\131\1\143\1\144\1"+
        "\1\1\2\1\75\1\135\30\uffff\1\65\16\uffff\1\64\1\uffff\1\66\1\126"+
        "\1\145\1\113\1\67\1\uffff\1\112\1\140\1\uffff\1\111\1\137\1\72\1"+
        "\104\1\133\1\73\1\105\1\106\1\132\1\74\1\134\1\76\1\136\1\77\1\107"+
        "\1\127\1\100\1\141\1\101\1\110\1\142\1\114\1\130\1\121\7\uffff\1"+
        "\13\6\uffff\1\23\35\uffff\1\70\1\102\1\71\1\103\14\uffff\1\21\2"+
        "\uffff\1\25\32\uffff\1\4\1\uffff\1\6\1\7\4\uffff\1\15\1\16\2\uffff"+
        "\1\22\1\uffff\1\26\14\uffff\1\43\14\uffff\1\5\1\10\4\uffff\1\20"+
        "\4\uffff\1\32\6\uffff\1\41\2\uffff\1\45\2\uffff\1\51\11\uffff\1"+
        "\14\1\17\1\24\2\uffff\1\31\1\33\1\34\1\35\1\36\1\37\16\uffff\1\12"+
        "\2\uffff\1\40\4\uffff\1\50\7\uffff\1\11\1\27\1\30\1\42\1\44\1\46"+
        "\1\47\1\52\1\53\4\uffff\1\60\1\uffff\1\55\2\uffff\1\54\7\uffff\1"+
        "\57\1\56";
    static final String DFA53_specialS =
        "\33\uffff\1\0\u014e\uffff}>";
    static final String[] DFA53_transitionS = {
            "\3\2\1\uffff\2\2\22\uffff\1\2\1\46\1\33\1\62\1\24\1\42\1\43"+
            "\1\30\1\54\1\55\1\41\1\37\1\52\1\40\1\31\1\1\1\25\11\26\1\53"+
            "\1\47\1\36\1\34\1\35\1\61\1\uffff\13\27\1\32\10\27\1\23\5\27"+
            "\1\56\1\uffff\1\57\1\44\1\22\1\uffff\1\3\1\4\1\5\1\6\1\7\1\10"+
            "\1\11\1\27\1\12\2\27\1\13\5\27\1\14\1\15\1\16\1\17\1\20\1\21"+
            "\3\27\1\50\1\45\1\51\1\60",
            "\1\63\4\uffff\1\64\15\uffff\1\65",
            "",
            "\1\67",
            "\1\70",
            "\1\71\6\uffff\1\72\6\uffff\1\73",
            "\1\74\11\uffff\1\75",
            "\1\76\1\uffff\1\77\11\uffff\1\100",
            "\1\101\2\uffff\1\102",
            "\1\103",
            "\1\104\7\uffff\1\105",
            "\1\106",
            "\1\107",
            "\1\110\1\111\12\uffff\1\112\2\uffff\1\113",
            "\1\114",
            "\1\117\4\uffff\1\30\20\uffff\1\116\65\uffff\1\115",
            "\1\120",
            "\1\121",
            "\1\122\1\123\1\124\3\uffff\1\125\1\uffff\1\126\4\uffff\1\127"+
            "\4\uffff\1\130\1\131\12\uffff\1\132",
            "\1\117\4\uffff\1\30",
            "",
            "\1\136\1\uffff\10\135\2\136\13\uffff\1\136\22\uffff\1\134"+
            "\14\uffff\1\136\22\uffff\1\133",
            "\1\136\1\uffff\12\137\13\uffff\1\136\37\uffff\1\136",
            "",
            "",
            "\1\140\1\uffff\12\136",
            "\1\117\4\uffff\1\30",
            "\12\117\1\uffff\ufff5\117",
            "\1\143",
            "\1\146\1\145",
            "\1\50\24\uffff\1\56\1\uffff\1\150\1\151",
            "\1\154\21\uffff\1\153",
            "\1\157\17\uffff\1\156\1\160",
            "\1\162",
            "\1\164\1\51",
            "\1\167\26\uffff\1\166",
            "\1\171",
            "\1\173\76\uffff\1\174",
            "\1\176",
            "",
            "",
            "",
            "",
            "\1\57",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\u0081",
            "\1\u0082",
            "\1\u0083",
            "\1\u0084",
            "\1\u0085",
            "\1\u0086",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\24\27\1\u0087\5"+
            "\27",
            "\1\u0089",
            "\1\u008a",
            "\1\u008b",
            "\1\u008c",
            "\1\u008d",
            "\1\u008e",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "\1\u0090\7\uffff\1\u0091",
            "\1\u0092",
            "\1\u0093\13\uffff\1\u0094\1\u0095",
            "\1\u0096",
            "\1\u0097\22\uffff\1\u0098",
            "\1\u0099\20\uffff\1\u009a",
            "\1\u009b",
            "\1\u009c",
            "\1\u009d\11\uffff\1\u009e",
            "\1\117",
            "",
            "\1\u009f\2\uffff\1\u00a0",
            "\1\u00a1",
            "\1\u00a2\7\uffff\1\u00a3",
            "\1\u00a4",
            "\1\u00a5",
            "\1\u00a6",
            "\1\u00a7",
            "\1\u00a8",
            "\1\u00a9",
            "\1\u00aa",
            "\1\u00ab",
            "\1\136\1\uffff\12\u00ac\7\uffff\6\u00ac\32\uffff\6\u00ac",
            "\1\136\1\uffff\12\u00ac\7\uffff\6\u00ac\32\uffff\6\u00ac",
            "\1\136\1\uffff\10\135\2\136\13\uffff\1\136\37\uffff\1\136",
            "",
            "\1\136\1\uffff\12\137\13\uffff\1\136\37\uffff\1\136",
            "",
            "",
            "",
            "",
            "",
            "\1\u00ad",
            "",
            "",
            "\1\u00af",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\u00b1",
            "\1\u00b2",
            "\1\u00b3",
            "\1\u00b4",
            "\1\u00b5\1\u00b6",
            "\1\u00b7",
            "\1\u00b8",
            "",
            "\1\u00b9",
            "\1\u00ba",
            "\1\u00bb",
            "\1\u00bc",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "\1\u00be",
            "",
            "\1\u00bf",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "\1\u00c1",
            "\1\u00c2",
            "\1\u00c3",
            "\1\u00c4",
            "\1\u00c5",
            "\1\u00c6",
            "\1\u00c7",
            "\1\u00c8",
            "\1\u00c9",
            "\1\u00ca",
            "\1\u00cb",
            "\1\u00cc",
            "\1\u00cd",
            "\1\u00ce",
            "\1\u00cf",
            "\1\u00d0",
            "\1\u00d1",
            "\1\u00d2",
            "\1\u00d3",
            "\1\u00d4",
            "\1\u00d5",
            "\1\u00d6",
            "\1\u00d7",
            "\1\u00d8",
            "\1\u00d9",
            "\1\u00da",
            "\1\136\1\uffff\12\u00ac\7\uffff\6\u00ac\11\uffff\1\136\20"+
            "\uffff\6\u00ac\11\uffff\1\136",
            "",
            "",
            "",
            "",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "\1\u00dc",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "\1\u00df",
            "\1\u00e0",
            "\1\u00e1",
            "\1\u00e2",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "\1\u00e5",
            "\1\u00e6",
            "",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "\1\u00e8",
            "",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "\1\u00ea",
            "\1\u00eb",
            "\1\u00ec",
            "\1\u00ed",
            "\1\u00ee",
            "\1\u00ef",
            "\1\u00f0",
            "\1\u00f1",
            "\1\u00f2",
            "\1\u00f3",
            "\1\u00f4",
            "\1\u00f5",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "\1\u00f7",
            "\1\u00f8",
            "\1\u00f9",
            "\1\u00fa",
            "\1\u00fb",
            "\1\u00fc",
            "\1\u00fd",
            "\1\u00fe",
            "\1\u00ff",
            "\1\u0100",
            "\1\u0101",
            "\1\u0102",
            "",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "",
            "",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "\1\u0105",
            "\1\u0106",
            "\1\u0107",
            "",
            "",
            "\1\u0108",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "",
            "\1\u010a",
            "",
            "\1\u010b",
            "\1\u010c",
            "\1\u010d",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "\1\u010f",
            "\1\u0110",
            "\1\u0111",
            "\1\u0112",
            "\1\u0113",
            "\1\u0114",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "\1\u0116",
            "",
            "\1\u0117",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "\1\u0119",
            "\1\u011a",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "\1\u011c",
            "\1\u011d",
            "\1\u011e",
            "\1\u011f",
            "\1\u0120",
            "\1\u0121",
            "\1\u0122",
            "",
            "",
            "\1\u0123",
            "\1\u0124",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "\1\u0128",
            "\1\u0129",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "\1\u0130",
            "",
            "\1\u0131",
            "\1\u0132",
            "",
            "\1\u0133\15\uffff\1\u0134",
            "\1\u0135",
            "",
            "\1\u0136",
            "\1\u0137",
            "\1\u0138",
            "\1\u0139",
            "\1\u013a",
            "\1\u013b",
            "\1\u013c",
            "\1\u013d",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "",
            "",
            "",
            "\1\u013f",
            "\1\u0140",
            "",
            "",
            "",
            "",
            "",
            "",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "\1\u0142",
            "\1\u0143",
            "\1\u0144",
            "\1\u0145",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "\1\u0147",
            "\1\u0148",
            "\1\u0149",
            "\1\u014a",
            "\1\u014b",
            "\1\u014c",
            "\1\u014d",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "\1\u0157",
            "\1\u0158",
            "\1\u0159",
            "\1\u015a",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\u015c",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "\1\u015e",
            "\1\u015f",
            "",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "",
            "\1\u0161",
            "\1\u0162",
            "",
            "\1\u0163",
            "\1\u0164",
            "\1\u0165",
            "\1\u0166",
            "\1\u0167",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "\12\27\7\uffff\32\27\4\uffff\1\27\1\uffff\32\27",
            "",
            ""
    };

    static final short[] DFA53_eot = DFA.unpackEncodedString(DFA53_eotS);
    static final short[] DFA53_eof = DFA.unpackEncodedString(DFA53_eofS);
    static final char[] DFA53_min = DFA.unpackEncodedStringToUnsignedChars(DFA53_minS);
    static final char[] DFA53_max = DFA.unpackEncodedStringToUnsignedChars(DFA53_maxS);
    static final short[] DFA53_accept = DFA.unpackEncodedString(DFA53_acceptS);
    static final short[] DFA53_special = DFA.unpackEncodedString(DFA53_specialS);
    static final short[][] DFA53_transition;

    static {
        int numStates = DFA53_transitionS.length;
        DFA53_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA53_transition[i] = DFA.unpackEncodedString(DFA53_transitionS[i]);
        }
    }

    class DFA53 extends DFA {

        public DFA53(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 53;
            this.eot = DFA53_eot;
            this.eof = DFA53_eof;
            this.min = DFA53_min;
            this.max = DFA53_max;
            this.accept = DFA53_accept;
            this.special = DFA53_special;
            this.transition = DFA53_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( RULE_ML_COMMENT | RULE_SL_COMMENT | RULE_WS | RULE_AUTO | RULE_BREAK | RULE_CASE | RULE_CHAR | RULE_CONST | RULE_CONTINUE | RULE_DEFAULT | RULE_DO | RULE_DOUBLE | RULE_ELSE | RULE_ENUM | RULE_EXTERN | RULE_FLOAT | RULE_FOR | RULE_GOTO | RULE_IF | RULE_INLINE | RULE_INT | RULE_LONG | RULE_REGISTER | RULE_RESTRICT | RULE_RETURN | RULE_SHORT | RULE_SIGNED | RULE_SIZEOF | RULE_STATIC | RULE_STRUCT | RULE_SWITCH | RULE_TYPEDEF | RULE_UNION | RULE_UNSIGNED | RULE_VOID | RULE_VOLATILE | RULE_WHILE | RULE_ALIGNAS | RULE_ALIGNOF | RULE_ATOMIC | RULE_BOOL | RULE_COMPLEX | RULE_GENERIC | RULE_IMAGINARY | RULE_NORETURN | RULE_STATIC_ASSERT | RULE_THREAD_LOCAL | RULE_FUNC_NAME | RULE_IDENTIFIER | RULE_CUSTOM | RULE_I_CONSTANT | RULE_F_CONSTANT | RULE_STRING_LITERAL | RULE_ELLIPSIS | RULE_ASSIGN | RULE_RIGHT_ASSIGN | RULE_LEFT_ASSIGN | RULE_ADD_ASSIGN | RULE_SUB_ASSIGN | RULE_MUL_ASSIGN | RULE_DIV_ASSIGN | RULE_MOD_ASSIGN | RULE_AND_ASSIGN | RULE_XOR_ASSIGN | RULE_OR_ASSIGN | RULE_RIGHT_OP | RULE_LEFT_OP | RULE_INC_OP | RULE_DEC_OP | RULE_PTR_OP | RULE_LAND_OP | RULE_LOR_OP | RULE_LE_OP | RULE_GE_OP | RULE_EQ_OP | RULE_NE_OP | RULE_SEMICOLON | RULE_LCBRACKET | RULE_RCBRACKET | RULE_COMMA | RULE_COLON | RULE_LPAREN | RULE_RPAREN | RULE_LBRACKET | RULE_RBRACKET | RULE_DOT | RULE_AND_OP | RULE_NOT_OP | RULE_TILDE | RULE_MINUS_OP | RULE_ADD_OP | RULE_MULT_OP | RULE_DIV_OP | RULE_MOD_OP | RULE_L_OP | RULE_G_OP | RULE_XOR_OP | RULE_OR_OP | RULE_Q_OP | RULE_INCLUDE | RULE_DQUOTE );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            IntStream input = _input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA53_27 = input.LA(1);

                        s = -1;
                        if ( ((LA53_27>='\u0000' && LA53_27<='\t')||(LA53_27>='\u000B' && LA53_27<='\uFFFF')) ) {s = 79;}

                        else s = 98;

                        if ( s>=0 ) return s;
                        break;
            }
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 53, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

}