/**
 */
package org.xtext.dop.clang.clang;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.clang.clang.Parameter#getSpecs <em>Specs</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.Parameter#getType <em>Type</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.Parameter#getId <em>Id</em>}</li>
 * </ul>
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getParameter()
 * @model
 * @generated
 */
public interface Parameter extends EObject
{
  /**
   * Returns the value of the '<em><b>Specs</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.dop.clang.clang.DeclarationSpecifier}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Specs</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Specs</em>' containment reference list.
   * @see org.xtext.dop.clang.clang.ClangPackage#getParameter_Specs()
   * @model containment="true"
   * @generated
   */
  EList<DeclarationSpecifier> getSpecs();

  /**
   * Returns the value of the '<em><b>Type</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.dop.clang.clang.SimpleType}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' containment reference list.
   * @see org.xtext.dop.clang.clang.ClangPackage#getParameter_Type()
   * @model containment="true"
   * @generated
   */
  EList<SimpleType> getType();

  /**
   * Returns the value of the '<em><b>Id</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Id</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Id</em>' containment reference.
   * @see #setId(GenericDeclarationId)
   * @see org.xtext.dop.clang.clang.ClangPackage#getParameter_Id()
   * @model containment="true"
   * @generated
   */
  GenericDeclarationId getId();

  /**
   * Sets the value of the '{@link org.xtext.dop.clang.clang.Parameter#getId <em>Id</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Id</em>' containment reference.
   * @see #getId()
   * @generated
   */
  void setId(GenericDeclarationId value);

} // Parameter
