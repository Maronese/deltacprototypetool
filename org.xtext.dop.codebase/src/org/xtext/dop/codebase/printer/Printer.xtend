package org.xtext.dop.codebase.printer

import org.xtext.dop.clang.clang.AdditiveExpression
import org.xtext.dop.clang.clang.AlignmentSpecifier
import org.xtext.dop.clang.clang.AndExpression
import org.xtext.dop.clang.clang.ArrayPostfix
import org.xtext.dop.clang.clang.AssignmentExpression
import org.xtext.dop.clang.clang.AssignmentExpressionElement
import org.xtext.dop.clang.clang.Atomic
import org.xtext.dop.clang.clang.BaseTypeBool
import org.xtext.dop.clang.clang.BaseTypeChar
import org.xtext.dop.clang.clang.BaseTypeComplex
import org.xtext.dop.clang.clang.BaseTypeDouble
import org.xtext.dop.clang.clang.BaseTypeFloat
import org.xtext.dop.clang.clang.BaseTypeImaginary
import org.xtext.dop.clang.clang.BaseTypeInt
import org.xtext.dop.clang.clang.BaseTypeLong
import org.xtext.dop.clang.clang.BaseTypeShort
import org.xtext.dop.clang.clang.BaseTypeSigned
import org.xtext.dop.clang.clang.BaseTypeUnsigned
import org.xtext.dop.clang.clang.BaseTypeVoid
import org.xtext.dop.clang.clang.BlockItem
import org.xtext.dop.clang.clang.CompoundStatement
import org.xtext.dop.clang.clang.ConditionalExpression
import org.xtext.dop.clang.clang.Custom
import org.xtext.dop.clang.clang.Declaration
import org.xtext.dop.clang.clang.DeclarationSpecifier
import org.xtext.dop.clang.clang.EnumType
import org.xtext.dop.clang.clang.EqualityExpression
import org.xtext.dop.clang.clang.ExclusiveOrExpression
import org.xtext.dop.clang.clang.Expression
import org.xtext.dop.clang.clang.ExpressionAlign
import org.xtext.dop.clang.clang.ExpressionCast
import org.xtext.dop.clang.clang.ExpressionComplex
import org.xtext.dop.clang.clang.ExpressionDecrement
import org.xtext.dop.clang.clang.ExpressionFloat
import org.xtext.dop.clang.clang.ExpressionGenericSelection
import org.xtext.dop.clang.clang.ExpressionIdentifier
import org.xtext.dop.clang.clang.ExpressionIncrement
import org.xtext.dop.clang.clang.ExpressionInitializer
import org.xtext.dop.clang.clang.ExpressionInteger
import org.xtext.dop.clang.clang.ExpressionSimple
import org.xtext.dop.clang.clang.ExpressionSizeof
import org.xtext.dop.clang.clang.ExpressionStatement
import org.xtext.dop.clang.clang.ExpressionString
import org.xtext.dop.clang.clang.ExpressionSubExpression
import org.xtext.dop.clang.clang.ExpressionUnaryOp
import org.xtext.dop.clang.clang.FunctionInitializer
import org.xtext.dop.clang.clang.FunctionSpecifierInline
import org.xtext.dop.clang.clang.FunctionSpecifierNoReturn
import org.xtext.dop.clang.clang.GenericAssociation
import org.xtext.dop.clang.clang.GenericAssociationCase
import org.xtext.dop.clang.clang.GenericAssociationDefault
import org.xtext.dop.clang.clang.GenericDeclaration0
import org.xtext.dop.clang.clang.GenericDeclaration1
import org.xtext.dop.clang.clang.GenericDeclaration2
import org.xtext.dop.clang.clang.GenericDeclarationElement
import org.xtext.dop.clang.clang.GenericDeclarationElements
import org.xtext.dop.clang.clang.GenericDeclarationId
import org.xtext.dop.clang.clang.GenericDeclarationIdPostfix
import org.xtext.dop.clang.clang.GenericDeclarationInitializer
import org.xtext.dop.clang.clang.GenericDeclarationInnerId
import org.xtext.dop.clang.clang.GenericSpecifiedDeclaration
import org.xtext.dop.clang.clang.GlobalName
import org.xtext.dop.clang.clang.GoToPrefixStatement
import org.xtext.dop.clang.clang.InclusiveOrExpression
import org.xtext.dop.clang.clang.IterationStatementDo
import org.xtext.dop.clang.clang.IterationStatementFor
import org.xtext.dop.clang.clang.IterationStatementWhile
import org.xtext.dop.clang.clang.JumpStatementBreak
import org.xtext.dop.clang.clang.JumpStatementContinue
import org.xtext.dop.clang.clang.JumpStatementGoto
import org.xtext.dop.clang.clang.JumpStatementReturn
import org.xtext.dop.clang.clang.LabeledStatementCase
import org.xtext.dop.clang.clang.LabeledStatementDefault
import org.xtext.dop.clang.clang.LogicalAndExpression
import org.xtext.dop.clang.clang.LogicalOrExpression
import org.xtext.dop.clang.clang.Model
import org.xtext.dop.clang.clang.MultiplicativeExpression
import org.xtext.dop.clang.clang.Parameter
import org.xtext.dop.clang.clang.ParametersPostfix
import org.xtext.dop.clang.clang.PostfixExpressionPostfix
import org.xtext.dop.clang.clang.PostfixExpressionPostfixAccess
import org.xtext.dop.clang.clang.PostfixExpressionPostfixAccessPtr
import org.xtext.dop.clang.clang.PostfixExpressionPostfixArgumentList
import org.xtext.dop.clang.clang.PostfixExpressionPostfixDecrement
import org.xtext.dop.clang.clang.PostfixExpressionPostfixExpression
import org.xtext.dop.clang.clang.PostfixExpressionPostfixIncrement
import org.xtext.dop.clang.clang.RelationalExpression
import org.xtext.dop.clang.clang.SelectionStatementIf
import org.xtext.dop.clang.clang.SelectionStatementSwitch
import org.xtext.dop.clang.clang.ShiftExpression
import org.xtext.dop.clang.clang.SimpleType
import org.xtext.dop.clang.clang.SpecifiedType
import org.xtext.dop.clang.clang.Statement
import org.xtext.dop.clang.clang.StatementDeclaration
import org.xtext.dop.clang.clang.StatementInnerStatement
import org.xtext.dop.clang.clang.StaticAssertDeclaration
import org.xtext.dop.clang.clang.StorageClassSpecifierAuto
import org.xtext.dop.clang.clang.StorageClassSpecifierExtern
import org.xtext.dop.clang.clang.StorageClassSpecifierRegister
import org.xtext.dop.clang.clang.StorageClassSpecifierStatic
import org.xtext.dop.clang.clang.StorageClassSpecifierThreadLocal
import org.xtext.dop.clang.clang.StringExpression
import org.xtext.dop.clang.clang.StructOrUnionType
import org.xtext.dop.clang.clang.TypeAliasDeclaration
import org.xtext.dop.clang.clang.TypeQualifierAtomic
import org.xtext.dop.clang.clang.TypeQualifierConst
import org.xtext.dop.clang.clang.TypeQualifierVolatile
import org.xtext.dop.clang.clang.NextParameter
import org.xtext.dop.clang.clang.Source
import org.xtext.dop.clang.clang.SimpleName
import org.xtext.dop.clang.clang.CompleteName
import org.xtext.dop.clang.clang.NextAsignement
import org.xtext.dop.clang.clang.NextField
import org.xtext.dop.clang.clang.StandardInclude
import org.xtext.dop.clang.clang.FileInclude
import org.xtext.dop.rappresentation.Resource

class Printer {
	
	int tab
	Resource resource
	
	new(Resource resource) {
		this.resource = resource
	}
	
	def print() {
		val model = resource.getSource
		var res = model.includesStd.map[it.print].join("\n")
		res += "\n\n"+model.includesFile.map[it.print].join("\n")
		res += "#include \""+resource.getName+"_prototypes.h\""
		res += "\n\n"+model.declarations.map[it.print].join("\n")
		return res
	}
	
	//preprocessor
	
	def String print(StandardInclude include) {
		//println("#include <"+include.std.print+">")
		"#include <"+include.std.print+">"
	}
	
	def String print(FileInclude include) {
		"#include "+include.file
	}
	
	def String print(Source source) {
		switch source {
			SimpleName:
				source.name
			CompleteName:
				source.name+"."+(source as CompleteName).ext
		}
	}
	
	//declarations
	
	def String print(Declaration decl) {
		switch decl {
			GenericSpecifiedDeclaration:
				{
					val item = decl as GenericSpecifiedDeclaration
					var res = ""
					if(!item.specs.empty) res += item.specs.map[it.print].join(" ")+" "
					return res+item.type.map[it.print].join(" ")+" "+item.decl.print+";"
				}
			TypeAliasDeclaration:
				{
					val item = decl as TypeAliasDeclaration
					"typedef "+item.tdecl.print+";"
				}
			StaticAssertDeclaration:
				{
					val item = decl as StaticAssertDeclaration
					"_Static_assert ("+item.exp.print+", "+item.string+");"
				}
		}
	}
	def String print(GenericDeclarationElements gen) {
		val res = gen.els.map[it.print].join(" ")
		if(gen.next!=null) return res+", "+gen.next.print
		return res
	}
	def print(GenericDeclarationElement gen) {
		val res = gen.id.print
		if(gen.init!=null) return res+" "+gen.init.print
		else return res
	}
	def print(SpecifiedType spec) {
		spec.par.print
	}
	def String print(GenericDeclarationId gen) {
		switch gen {
			GenericDeclaration0:
				{
					val item = gen as GenericDeclaration0
					var res = item.pointers.join+" "
					if(item.restrict!=null) res+=item.restrict+" "
					if(item.inner!=null) res+=item.inner.print+" "
					res+item.posts.map[it.print].join
				}
			GenericDeclaration1:
				{
					val item = gen as GenericDeclaration1
					item.inner.print+" "+item.posts.map[it.print].join
				}
			GenericDeclaration2:
				{
					val item = gen as GenericDeclaration2
					item.posts.map[it.print].join+" "+item.inner.print
				}
		}
	}
	def print(GenericDeclarationInnerId gen) {
		if(gen instanceof GlobalName) return gen.name
		else return "("+gen.inner.print+")"
	}
	def print(GenericDeclarationIdPostfix gen) {
		if(gen.array!=null) return gen.array.print
		else return gen.function.print
	}
	def print(ArrayPostfix arr){
		var res = "["
		if(arr.star!=null) res += arr.star
		if(arr.exp!=null) return res+arr.exp.print+"]"
		else return res+"]"
	}
	def print(ParametersPostfix par) {
		var ret = "("
		if(!par.els.isEmpty) ret+=par.els.map[it.print].join(" ")
		if(!par.next.isEmpty) ret+=par.next.map[it.print].join
		if(par.more!=null) ret+="..."
		return ret+")"
	}
	def print(NextParameter next) {
		", "+next.els.map[it.print].join
	}
	def print(Parameter par) {
		var res =par.specs.map[it.print].join(" ")+" "+par.type.map[it.print].join(" ")
		if(par.id!=null) return res+" "+par.id.print
		else return res
	}
	def print(GenericDeclarationInitializer gen) {
		switch gen {
			ExpressionInitializer:
				{
					val item = gen as ExpressionInitializer
					" = "+item.exp.print
				}
			FunctionInitializer:
				{
					val item = gen as FunctionInitializer
					if (item.block!=null) return " "+item.block.print
					else return ""//per i prototype
				}
		}
	}
	
	//simple type
	
	def print(SimpleType type) {
		switch type {
			BaseTypeVoid:
				"void"
			Custom:
				{
					val item = type as Custom
					item.id.substring(1) //elimina $
				}
			BaseTypeChar:
				"char"
			BaseTypeShort:
				"shot"
			BaseTypeInt:
				"int"
			BaseTypeLong:
				"long"
			BaseTypeFloat:
				"float"
			BaseTypeDouble:
				"double"
			BaseTypeSigned:
				"signed"
			BaseTypeUnsigned:
				"unsigned"
			BaseTypeBool:
				"_Bool"
			BaseTypeComplex:
				"_Complex"
			BaseTypeImaginary:
				"_Immaginary"
			StructOrUnionType:
				{
					val item = type as StructOrUnionType
					var res = item.kind+" "
					if(item.content==null) return res+item.name
					if(item.name!=null) res+=item.name+" "
					"{\n"+item.content.map[it.print].join+"\n}\n"
				}
			EnumType:
				{
					val item = type as EnumType
					var res = "enum "
					//todo
					""
				}
			Atomic:
				{
					val item = type as Atomic
					"_Atomic("+item.type+")"
				}
		}
	}
	
	//Specifiers
	
	def print(DeclarationSpecifier spec) {
		switch spec {
			StorageClassSpecifierExtern:
				"extern"
			StorageClassSpecifierStatic:
				"static"
			StorageClassSpecifierThreadLocal:
				"_Thread_local"
			StorageClassSpecifierAuto:
				"auto"
			StorageClassSpecifierRegister:
				"register"
			TypeQualifierConst:
				"const"
			TypeQualifierVolatile:
				"volatile"
			TypeQualifierAtomic:
				"_Atomic"
			FunctionSpecifierInline:
				"inline"
			FunctionSpecifierNoReturn:
				"_Noreturn"
			AlignmentSpecifier:
				{
					val item = spec as AlignmentSpecifier
					if(item.type!=null) return "_Alignas("+item.type.print+")"
					else return "_Alignas("+item.exp.print+")"
				}
		}
	}
	
	//Expressions
	
	def String print(Expression expr) {
		switch expr {
			AssignmentExpression: expr.exp.print+expr.list.map[it.print].join
			ConditionalExpression: expr.getIf.print+"? "+expr.yes.print+": "+expr.no.print
			LogicalOrExpression: expr.left.print+"||"+expr.right.print
			LogicalAndExpression: expr.left.print+"&&"+expr.right.print
			InclusiveOrExpression: expr.left.print+"|"+expr.right.print
			ExclusiveOrExpression: expr.left.print+"^"+expr.right.print
			AndExpression: expr.left.print+"&"+expr.right.print
			EqualityExpression: expr.left.print+expr.operand+expr.right.print
			RelationalExpression: expr.left.print+expr.operand+expr.right.print
			ShiftExpression: expr.left.print+expr.operand+expr.right.print
			AdditiveExpression: expr.left.print+expr.operand+expr.right.print
			MultiplicativeExpression: expr.left.print+expr.operand+expr.right.print
			ExpressionSimple:
				{
					val item = (expr as ExpressionSimple)
					item.exp.print+item.next.map[it.print].join
				}
			ExpressionComplex:
				{
					val item = (expr as ExpressionComplex)
					"{"+item.exps.map[it.print].join(',')+"}"+item.next.map[it.print].join
				}
			ExpressionIncrement:
				{
					val item = (expr as ExpressionIncrement)
					"++"+item.content.print
				}
			ExpressionDecrement:
				{
					val item = (expr as ExpressionDecrement)
					"--"+item.content.print
				}
			ExpressionSizeof:
				{
					val item = (expr as ExpressionSizeof)
					"sizeof("+item.content.print+item.type.print+")"
				}
			ExpressionUnaryOp:
				{
					val item = (expr as ExpressionUnaryOp)
					item.op+item.content.print
				}
			ExpressionAlign:
				{
					val item = (expr as ExpressionAlign)
					"alignof("+item.type.print+")"
				}
			ExpressionCast:
				{
					val item = (expr as ExpressionCast)
					"("+item.type.print+")"+item.exp.print
				}
			ExpressionIdentifier:
				{
					val item = expr as ExpressionIdentifier
					item.name
				}
			ExpressionString:
				{
					val item = expr as ExpressionString
					item.value.print
				}
			ExpressionInteger:
				{
					val item = expr as ExpressionInteger
					item.value
				}
			ExpressionFloat:
				{
					val item = expr as ExpressionFloat
					item.value
				}
			ExpressionSubExpression:
				{
					val item = expr as ExpressionSubExpression
					"("+item.par.print+")"
				}
			ExpressionGenericSelection:
				{
					val item = expr as ExpressionGenericSelection
					//"generic("+item.left.print+item.right.map[','+it.print].join+")"
					""
				}
		}
	}
	def print(AssignmentExpressionElement expr) {
		expr.op+expr.exp.print
	}
	def print(NextAsignement expr) {
		expr.right.map[it.print].join
	}
	def print(StringExpression expr) {
		expr.name
	}
	def print(GenericAssociation expr) {
		switch expr {
			GenericAssociationCase:
				{
					val item = expr as GenericAssociationCase
					item.type.print+": "+item.exp.print
				}
			GenericAssociationDefault:
				"default: "+expr.exp.print
		}
	}
	def print(PostfixExpressionPostfix expr) {
		switch expr {
			PostfixExpressionPostfixExpression:
				{
					val item = expr as PostfixExpressionPostfixExpression
					"["+item.value.print+"]"
				}
			PostfixExpressionPostfixArgumentList:
				{
					val item = expr as PostfixExpressionPostfixArgumentList
					//todo
					""
				}
			PostfixExpressionPostfixAccess:
				{
					val item = expr as PostfixExpressionPostfixAccess
					"."+item.value
				}
			PostfixExpressionPostfixAccessPtr:
				{
					val item = expr as PostfixExpressionPostfixAccessPtr
					"->"+item.value
				}
			PostfixExpressionPostfixIncrement:
				"++"
			PostfixExpressionPostfixDecrement:
				"--"
		}
	}
	
	//Statement
	
	def String print(Statement stat) {
		switch stat {
			GoToPrefixStatement:
				{
					val item = stat as GoToPrefixStatement
					item.goto+": "+item.statement.print
				}
			CompoundStatement:
				{
					val item = stat as CompoundStatement
					var ret = " {\n"
					tab++
					ret+=item.inner.map[it.print].join
					tab--
					ret+="}\n"
				}
			LabeledStatementCase:
				{
					val item = stat as LabeledStatementCase
					"case "+item.exp.print+": "+item.statement.print
				}
			LabeledStatementDefault:
				{
					val item = stat as LabeledStatementDefault
					"default: "+item.statement.print
				}
			ExpressionStatement:
				{
					val item = stat as ExpressionStatement
					if(item.exp==null) return ";\n"
					else item.exp.print+";\n"
				}
			SelectionStatementIf:
				{
					val item = stat as SelectionStatementIf
					"if("+item.exp.print+")"+item.then_statement.print+"\nelse"+item.else_statement.print
				}
			SelectionStatementSwitch:
				{
					val item = stat as SelectionStatementSwitch
					"switch("+item.exp.print+") {"item.content.map[it.print].join
				}
			IterationStatementWhile:
				{
					val item = stat as IterationStatementWhile
					"while("+item.exp.print+") "+item.statement.print
				}
			IterationStatementDo:
				{
					val item = stat as IterationStatementDo
					"do"+item.statement.print+"while("+item.exp.print+")"
				}
			IterationStatementFor:
				{
					val item = stat as IterationStatementFor
					var res = "for("
					if(item.field1==null&&item.decl!=null) res+=item.decl.print
					else if(item.field1!=null&&item.decl==null) res+=item.field1.print
					if(item.field2!=null) res+=item.field2.print
					if(item.field3!=null) res+=item.field3.print
					if(item.next!=null) res+=item.next.map[it.print].join(",")
					res+=")"+item.statement.print //modificare field3
				}
			JumpStatementGoto:
				{
					val item = stat as JumpStatementGoto
					"goto "+item.name+";\n"
				}
			JumpStatementReturn:
				{
					val item = stat as JumpStatementReturn
					if(item.exp==null) return "return;\n"
					else return "return "+item.exp.print+";\n"
				}
			JumpStatementContinue:
				"continue;\n"
			JumpStatementBreak:
				"break;\n"
		}
	}
	def print(NextField field) {
		field.next.print
	}
	def print(BlockItem block) {
		switch block {
			StatementDeclaration:
				{
					val item = block as StatementDeclaration
					
					item.decl.print+"\n".indent
				}
			StatementInnerStatement:
				{
					val item = block as StatementInnerStatement
					item.statement.print
				}
		}
	}
	
	def indent(String str) {
		var res = ""
		for(var i=0; i<tab; i++) {
			res+="\t"
		}
		res+str
	}
}