/**
 */
package org.xtext.dop.codebase.codeBase.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.xtext.dop.codebase.codeBase.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CodeBaseFactoryImpl extends EFactoryImpl implements CodeBaseFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static CodeBaseFactory init()
  {
    try
    {
      CodeBaseFactory theCodeBaseFactory = (CodeBaseFactory)EPackage.Registry.INSTANCE.getEFactory(CodeBasePackage.eNS_URI);
      if (theCodeBaseFactory != null)
      {
        return theCodeBaseFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new CodeBaseFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CodeBaseFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case CodeBasePackage.DELTA_MODULE: return createDeltaModule();
      case CodeBasePackage.FILE_OPERATION: return createFileOperation();
      case CodeBasePackage.ADD_SOURCE: return createAddSource();
      case CodeBasePackage.REMOVE_SOURCE: return createRemoveSource();
      case CodeBasePackage.SOURCE_OPERATION: return createSourceOperation();
      case CodeBasePackage.ADDS: return createAdds();
      case CodeBasePackage.REMOVES: return createRemoves();
      case CodeBasePackage.MODIFIES: return createModifies();
      case CodeBasePackage.ADD_DECLARATION: return createAddDeclaration();
      case CodeBasePackage.ADD_STANDARD_INCLUDE: return createAddStandardInclude();
      case CodeBasePackage.ADD_FILE_INCLUDE: return createAddFileInclude();
      case CodeBasePackage.REMOVE_GLOBAL: return createRemoveGlobal();
      case CodeBasePackage.REMOVE_STANDARD_INCLUDE: return createRemoveStandardInclude();
      case CodeBasePackage.REMOVE_FILE_INCLUDE: return createRemoveFileInclude();
      case CodeBasePackage.MODIFY_FUNCTION: return createModifyFunction();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DeltaModule createDeltaModule()
  {
    DeltaModuleImpl deltaModule = new DeltaModuleImpl();
    return deltaModule;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FileOperation createFileOperation()
  {
    FileOperationImpl fileOperation = new FileOperationImpl();
    return fileOperation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AddSource createAddSource()
  {
    AddSourceImpl addSource = new AddSourceImpl();
    return addSource;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RemoveSource createRemoveSource()
  {
    RemoveSourceImpl removeSource = new RemoveSourceImpl();
    return removeSource;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SourceOperation createSourceOperation()
  {
    SourceOperationImpl sourceOperation = new SourceOperationImpl();
    return sourceOperation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Adds createAdds()
  {
    AddsImpl adds = new AddsImpl();
    return adds;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Removes createRemoves()
  {
    RemovesImpl removes = new RemovesImpl();
    return removes;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Modifies createModifies()
  {
    ModifiesImpl modifies = new ModifiesImpl();
    return modifies;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AddDeclaration createAddDeclaration()
  {
    AddDeclarationImpl addDeclaration = new AddDeclarationImpl();
    return addDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AddStandardInclude createAddStandardInclude()
  {
    AddStandardIncludeImpl addStandardInclude = new AddStandardIncludeImpl();
    return addStandardInclude;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AddFileInclude createAddFileInclude()
  {
    AddFileIncludeImpl addFileInclude = new AddFileIncludeImpl();
    return addFileInclude;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RemoveGlobal createRemoveGlobal()
  {
    RemoveGlobalImpl removeGlobal = new RemoveGlobalImpl();
    return removeGlobal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RemoveStandardInclude createRemoveStandardInclude()
  {
    RemoveStandardIncludeImpl removeStandardInclude = new RemoveStandardIncludeImpl();
    return removeStandardInclude;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RemoveFileInclude createRemoveFileInclude()
  {
    RemoveFileIncludeImpl removeFileInclude = new RemoveFileIncludeImpl();
    return removeFileInclude;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ModifyFunction createModifyFunction()
  {
    ModifyFunctionImpl modifyFunction = new ModifyFunctionImpl();
    return modifyFunction;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CodeBasePackage getCodeBasePackage()
  {
    return (CodeBasePackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static CodeBasePackage getPackage()
  {
    return CodeBasePackage.eINSTANCE;
  }

} //CodeBaseFactoryImpl
