/**
 */
package org.xtext.dop.clang.clang.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;

import org.xtext.dop.clang.clang.ClangPackage;
import org.xtext.dop.clang.clang.GenericDeclaration0;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Generic Declaration0</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.clang.clang.impl.GenericDeclaration0Impl#getPointers <em>Pointers</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.impl.GenericDeclaration0Impl#getRestrict <em>Restrict</em>}</li>
 * </ul>
 *
 * @generated
 */
public class GenericDeclaration0Impl extends GenericDeclarationIdImpl implements GenericDeclaration0
{
  /**
   * The cached value of the '{@link #getPointers() <em>Pointers</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPointers()
   * @generated
   * @ordered
   */
  protected EList<String> pointers;

  /**
   * The default value of the '{@link #getRestrict() <em>Restrict</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRestrict()
   * @generated
   * @ordered
   */
  protected static final String RESTRICT_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getRestrict() <em>Restrict</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRestrict()
   * @generated
   * @ordered
   */
  protected String restrict = RESTRICT_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected GenericDeclaration0Impl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ClangPackage.Literals.GENERIC_DECLARATION0;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getPointers()
  {
    if (pointers == null)
    {
      pointers = new EDataTypeEList<String>(String.class, this, ClangPackage.GENERIC_DECLARATION0__POINTERS);
    }
    return pointers;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getRestrict()
  {
    return restrict;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRestrict(String newRestrict)
  {
    String oldRestrict = restrict;
    restrict = newRestrict;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ClangPackage.GENERIC_DECLARATION0__RESTRICT, oldRestrict, restrict));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case ClangPackage.GENERIC_DECLARATION0__POINTERS:
        return getPointers();
      case ClangPackage.GENERIC_DECLARATION0__RESTRICT:
        return getRestrict();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case ClangPackage.GENERIC_DECLARATION0__POINTERS:
        getPointers().clear();
        getPointers().addAll((Collection<? extends String>)newValue);
        return;
      case ClangPackage.GENERIC_DECLARATION0__RESTRICT:
        setRestrict((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case ClangPackage.GENERIC_DECLARATION0__POINTERS:
        getPointers().clear();
        return;
      case ClangPackage.GENERIC_DECLARATION0__RESTRICT:
        setRestrict(RESTRICT_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case ClangPackage.GENERIC_DECLARATION0__POINTERS:
        return pointers != null && !pointers.isEmpty();
      case ClangPackage.GENERIC_DECLARATION0__RESTRICT:
        return RESTRICT_EDEFAULT == null ? restrict != null : !RESTRICT_EDEFAULT.equals(restrict);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (pointers: ");
    result.append(pointers);
    result.append(", restrict: ");
    result.append(restrict);
    result.append(')');
    return result.toString();
  }

} //GenericDeclaration0Impl
