package org.xtext.dop.clang.inspector

import org.xtext.dop.clang.clang.Model
import org.xtext.dop.clang.clang.GenericSpecifiedDeclaration
import org.xtext.dop.clang.clang.FunctionInitializer

class Inspector {
	
	def indexFirstFunction(Model model) {
		val target = model.declarations.findFirst[
				it instanceof GenericSpecifiedDeclaration &&				//è una generic declaration
				!it.eAllContents.filter(typeof(FunctionInitializer)).empty 	//è un metodo
			]
		//println("target: "+target)
		val index = model.declarations.indexOf(target)
		//println("index: "+index)
		//if(index==-1) return 0
		return index
	}
}