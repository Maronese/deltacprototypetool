/**
 */
package org.xtext.dop.codebase.codeBase;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Source Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.dop.codebase.codeBase.CodeBasePackage#getSourceOperation()
 * @model
 * @generated
 */
public interface SourceOperation extends EObject
{
} // SourceOperation
