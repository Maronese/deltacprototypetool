/**
 */
package org.xtext.dop.clang.clang.impl;

import org.eclipse.emf.ecore.EClass;

import org.xtext.dop.clang.clang.ClangPackage;
import org.xtext.dop.clang.clang.GenericDeclaration1;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Generic Declaration1</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class GenericDeclaration1Impl extends GenericDeclarationIdImpl implements GenericDeclaration1
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected GenericDeclaration1Impl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ClangPackage.Literals.GENERIC_DECLARATION1;
  }

} //GenericDeclaration1Impl
