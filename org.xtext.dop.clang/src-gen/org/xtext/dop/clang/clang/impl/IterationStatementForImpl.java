/**
 */
package org.xtext.dop.clang.clang.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.xtext.dop.clang.clang.ClangPackage;
import org.xtext.dop.clang.clang.Declaration;
import org.xtext.dop.clang.clang.Expression;
import org.xtext.dop.clang.clang.ExpressionStatement;
import org.xtext.dop.clang.clang.IterationStatementFor;
import org.xtext.dop.clang.clang.NextField;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Iteration Statement For</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.clang.clang.impl.IterationStatementForImpl#getField1 <em>Field1</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.impl.IterationStatementForImpl#getDecl <em>Decl</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.impl.IterationStatementForImpl#getField2 <em>Field2</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.impl.IterationStatementForImpl#getField3 <em>Field3</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.impl.IterationStatementForImpl#getNext <em>Next</em>}</li>
 * </ul>
 *
 * @generated
 */
public class IterationStatementForImpl extends IterationStatementImpl implements IterationStatementFor
{
  /**
   * The cached value of the '{@link #getField1() <em>Field1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getField1()
   * @generated
   * @ordered
   */
  protected ExpressionStatement field1;

  /**
   * The cached value of the '{@link #getDecl() <em>Decl</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDecl()
   * @generated
   * @ordered
   */
  protected Declaration decl;

  /**
   * The cached value of the '{@link #getField2() <em>Field2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getField2()
   * @generated
   * @ordered
   */
  protected ExpressionStatement field2;

  /**
   * The cached value of the '{@link #getField3() <em>Field3</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getField3()
   * @generated
   * @ordered
   */
  protected Expression field3;

  /**
   * The cached value of the '{@link #getNext() <em>Next</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNext()
   * @generated
   * @ordered
   */
  protected EList<NextField> next;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected IterationStatementForImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ClangPackage.Literals.ITERATION_STATEMENT_FOR;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExpressionStatement getField1()
  {
    return field1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetField1(ExpressionStatement newField1, NotificationChain msgs)
  {
    ExpressionStatement oldField1 = field1;
    field1 = newField1;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ClangPackage.ITERATION_STATEMENT_FOR__FIELD1, oldField1, newField1);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setField1(ExpressionStatement newField1)
  {
    if (newField1 != field1)
    {
      NotificationChain msgs = null;
      if (field1 != null)
        msgs = ((InternalEObject)field1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ClangPackage.ITERATION_STATEMENT_FOR__FIELD1, null, msgs);
      if (newField1 != null)
        msgs = ((InternalEObject)newField1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ClangPackage.ITERATION_STATEMENT_FOR__FIELD1, null, msgs);
      msgs = basicSetField1(newField1, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ClangPackage.ITERATION_STATEMENT_FOR__FIELD1, newField1, newField1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Declaration getDecl()
  {
    return decl;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDecl(Declaration newDecl, NotificationChain msgs)
  {
    Declaration oldDecl = decl;
    decl = newDecl;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ClangPackage.ITERATION_STATEMENT_FOR__DECL, oldDecl, newDecl);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDecl(Declaration newDecl)
  {
    if (newDecl != decl)
    {
      NotificationChain msgs = null;
      if (decl != null)
        msgs = ((InternalEObject)decl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ClangPackage.ITERATION_STATEMENT_FOR__DECL, null, msgs);
      if (newDecl != null)
        msgs = ((InternalEObject)newDecl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ClangPackage.ITERATION_STATEMENT_FOR__DECL, null, msgs);
      msgs = basicSetDecl(newDecl, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ClangPackage.ITERATION_STATEMENT_FOR__DECL, newDecl, newDecl));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExpressionStatement getField2()
  {
    return field2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetField2(ExpressionStatement newField2, NotificationChain msgs)
  {
    ExpressionStatement oldField2 = field2;
    field2 = newField2;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ClangPackage.ITERATION_STATEMENT_FOR__FIELD2, oldField2, newField2);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setField2(ExpressionStatement newField2)
  {
    if (newField2 != field2)
    {
      NotificationChain msgs = null;
      if (field2 != null)
        msgs = ((InternalEObject)field2).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ClangPackage.ITERATION_STATEMENT_FOR__FIELD2, null, msgs);
      if (newField2 != null)
        msgs = ((InternalEObject)newField2).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ClangPackage.ITERATION_STATEMENT_FOR__FIELD2, null, msgs);
      msgs = basicSetField2(newField2, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ClangPackage.ITERATION_STATEMENT_FOR__FIELD2, newField2, newField2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Expression getField3()
  {
    return field3;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetField3(Expression newField3, NotificationChain msgs)
  {
    Expression oldField3 = field3;
    field3 = newField3;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ClangPackage.ITERATION_STATEMENT_FOR__FIELD3, oldField3, newField3);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setField3(Expression newField3)
  {
    if (newField3 != field3)
    {
      NotificationChain msgs = null;
      if (field3 != null)
        msgs = ((InternalEObject)field3).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ClangPackage.ITERATION_STATEMENT_FOR__FIELD3, null, msgs);
      if (newField3 != null)
        msgs = ((InternalEObject)newField3).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ClangPackage.ITERATION_STATEMENT_FOR__FIELD3, null, msgs);
      msgs = basicSetField3(newField3, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ClangPackage.ITERATION_STATEMENT_FOR__FIELD3, newField3, newField3));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<NextField> getNext()
  {
    if (next == null)
    {
      next = new EObjectContainmentEList<NextField>(NextField.class, this, ClangPackage.ITERATION_STATEMENT_FOR__NEXT);
    }
    return next;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case ClangPackage.ITERATION_STATEMENT_FOR__FIELD1:
        return basicSetField1(null, msgs);
      case ClangPackage.ITERATION_STATEMENT_FOR__DECL:
        return basicSetDecl(null, msgs);
      case ClangPackage.ITERATION_STATEMENT_FOR__FIELD2:
        return basicSetField2(null, msgs);
      case ClangPackage.ITERATION_STATEMENT_FOR__FIELD3:
        return basicSetField3(null, msgs);
      case ClangPackage.ITERATION_STATEMENT_FOR__NEXT:
        return ((InternalEList<?>)getNext()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case ClangPackage.ITERATION_STATEMENT_FOR__FIELD1:
        return getField1();
      case ClangPackage.ITERATION_STATEMENT_FOR__DECL:
        return getDecl();
      case ClangPackage.ITERATION_STATEMENT_FOR__FIELD2:
        return getField2();
      case ClangPackage.ITERATION_STATEMENT_FOR__FIELD3:
        return getField3();
      case ClangPackage.ITERATION_STATEMENT_FOR__NEXT:
        return getNext();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case ClangPackage.ITERATION_STATEMENT_FOR__FIELD1:
        setField1((ExpressionStatement)newValue);
        return;
      case ClangPackage.ITERATION_STATEMENT_FOR__DECL:
        setDecl((Declaration)newValue);
        return;
      case ClangPackage.ITERATION_STATEMENT_FOR__FIELD2:
        setField2((ExpressionStatement)newValue);
        return;
      case ClangPackage.ITERATION_STATEMENT_FOR__FIELD3:
        setField3((Expression)newValue);
        return;
      case ClangPackage.ITERATION_STATEMENT_FOR__NEXT:
        getNext().clear();
        getNext().addAll((Collection<? extends NextField>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case ClangPackage.ITERATION_STATEMENT_FOR__FIELD1:
        setField1((ExpressionStatement)null);
        return;
      case ClangPackage.ITERATION_STATEMENT_FOR__DECL:
        setDecl((Declaration)null);
        return;
      case ClangPackage.ITERATION_STATEMENT_FOR__FIELD2:
        setField2((ExpressionStatement)null);
        return;
      case ClangPackage.ITERATION_STATEMENT_FOR__FIELD3:
        setField3((Expression)null);
        return;
      case ClangPackage.ITERATION_STATEMENT_FOR__NEXT:
        getNext().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case ClangPackage.ITERATION_STATEMENT_FOR__FIELD1:
        return field1 != null;
      case ClangPackage.ITERATION_STATEMENT_FOR__DECL:
        return decl != null;
      case ClangPackage.ITERATION_STATEMENT_FOR__FIELD2:
        return field2 != null;
      case ClangPackage.ITERATION_STATEMENT_FOR__FIELD3:
        return field3 != null;
      case ClangPackage.ITERATION_STATEMENT_FOR__NEXT:
        return next != null && !next.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //IterationStatementForImpl
