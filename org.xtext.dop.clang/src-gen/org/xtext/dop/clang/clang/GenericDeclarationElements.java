/**
 */
package org.xtext.dop.clang.clang;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Generic Declaration Elements</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.clang.clang.GenericDeclarationElements#getEls <em>Els</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.GenericDeclarationElements#getNext <em>Next</em>}</li>
 * </ul>
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getGenericDeclarationElements()
 * @model
 * @generated
 */
public interface GenericDeclarationElements extends EObject
{
  /**
   * Returns the value of the '<em><b>Els</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.dop.clang.clang.GenericDeclarationElement}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Els</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Els</em>' containment reference list.
   * @see org.xtext.dop.clang.clang.ClangPackage#getGenericDeclarationElements_Els()
   * @model containment="true"
   * @generated
   */
  EList<GenericDeclarationElement> getEls();

  /**
   * Returns the value of the '<em><b>Next</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Next</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Next</em>' containment reference.
   * @see #setNext(GenericDeclarationElements)
   * @see org.xtext.dop.clang.clang.ClangPackage#getGenericDeclarationElements_Next()
   * @model containment="true"
   * @generated
   */
  GenericDeclarationElements getNext();

  /**
   * Sets the value of the '{@link org.xtext.dop.clang.clang.GenericDeclarationElements#getNext <em>Next</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Next</em>' containment reference.
   * @see #getNext()
   * @generated
   */
  void setNext(GenericDeclarationElements value);

} // GenericDeclarationElements
