package org.xtext.dop.codebase.tests

import org.eclipse.xtext.junit4.XtextRunner
import org.junit.runner.RunWith
import org.eclipse.xtext.junit4.InjectWith
import com.google.inject.Inject
import org.eclipse.xtext.junit4.util.ParseHelper
import org.xtext.dop.codebase.CodeBaseInjectorProvider
import org.xtext.dop.codebase.codeBase.DeltaModule
import org.eclipse.xtext.junit4.validation.ValidationTestHelper
import org.junit.Test

@RunWith(typeof(XtextRunner))
@InjectWith(typeof(CodeBaseInjectorProvider))

class CodeBaseSyntaxTests {
	
	@Inject extension ParseHelper<DeltaModule>
	@Inject extension ValidationTestHelper
	
	@Test
	def void emptyDelta() {
		'''delta test0 {}'''.parse.assertNoErrors
	}
	
	@Test
	def void addEmptySourceDelta() {
		'''
		delta test0 {
			adds {
				source {}
			}
		}
		'''.parse.assertNoErrors
	}
	
	@Test
	def void addMoreEmptySourceDelta() {
		'''
		delta test0 {
			adds {
				source0 {}
				source1 {}
			}
		}
		'''.parse.assertNoErrors
	}
	
	@Test
	def void multipleAddsSourceDelta() {
		'''
		delta test0 {
			adds {
				source0 {}
				source1 {}
			}
			adds {
				source2 {}
			}
		}
		'''.parse.assertNoErrors
	}
	
	@Test
	def void removesEmptySourceDelta() {
		'''
		delta test0 {
			removes {
				source;
			}
		}
		'''.parse.assertNoErrors
	}
	
	@Test
	def void removesMoreEmptySourceDelta() {
		'''
		delta test0 {
			removes {
				source0;
				source1;
			}
		}
		'''.parse.assertNoErrors
	}
	
	@Test
	def void multipleRemovesSourceDelta() {
		'''
		delta test0 {
			removes {
				source0;
				source1;
			}
			removes {
				source2;
			}
		}
		'''.parse.assertNoErrors
	}
	
	@Test
	def void addExample0SourceTest() {
		'''
		delta test0 {
			adds {
				source.c {
					struct s{
						int fill_pointer;
						char vettore[20];
					} stack;
					
					void print_stack(struct stack s);
					
					void print_stack(struct stack s) {
						int i, top;
						top = s.fill_pointer;
						for(i=top; i>=0; i++);
							printf("%c \n", s.vettore[i]);
					};
				}
			}
		}
		'''.parse.assertNoErrors
	}
	
	@Test
	def void addExample1SourceTest() {
		'''
		delta test0 {
			adds {
				source.c {
					int MAX = 20;
					
					int main() {
						char s1[MAX], s2[MAX];
						int dim;
						strcpy(s1, "");
						dim = strlen(s1);
						printf("\n s1 vale %s, la sua lunghezza e` %d\n", s1, dim);
						strcpy(s1, "ciao");
						dim = strlen(s1);
						strcpy(s2, s1);
						strcat(s2, " Beppe!");
						dim = strlen(s2);
					};
				}
			}
		}
		'''.parse.assertNoErrors
	}
	
	@Test
	def void addExample2SourceTest() {
		'''
		delta test {
			adds {
				source0.c {
					void somma(int vect[10]) {
						int i=0;
						while (i<10){
							vect[i] = vect[i]+10;
							i++;
						}
					};
					
					int main () {
						int v[10];
						int i=0;
						for (; i<10; i++) v[i] = i;
						somma(v);
						for (i=0; i<10; i++) printf("\n v[%d] = %d", i, v[i]);
					};
				}
				
				source1.c {
					int N = 24;
					
					typedef struct _nodo {
						int dato;
						char nome[N];
						struct _nodo *next;
						struct _nodo *prev;
					} nodo;
					
					typedef struct _nodo * lista;
					
					struct _nodo * crea_nodo(int num, char nome[N]) {
						//int size = sizeof(int); crea problemi la sizeof
						struct _nodo * result = (struct _nodo *)malloc(size);
						result->dato = num;
						strcpy(result->nome, nome);
						result->next = NULL;
						result->prev = NULL;
						return result;
					};
				}
			}
		}
		'''.parse.assertNoErrors
	}
	
	@Test
	def void modifieAddsDelta() {
		'''
		delta test0 {
			modifies source1.c {
				adds typedef struct _nodo {
						int dato;
						char nome[N];
						struct _nodo *next;
						struct _nodo *prev;
					} nodo;
					
				adds typedef struct _nodo * lista;
			}
		}
		'''.parse.assertNoErrors
	}
	
	@Test
	def void modifieRemovesDelta() {
		'''
		delta test0 {
			modifies source1.c {
				removes lista;
				removes main;
			}
		}
		'''.parse.assertNoErrors
	}
	
	@Test
	def void modifieDelta() {
		'''
		delta test0 {
			modifies source1.c {
				modifies typedef struct tree_node * lista;
				modifies struct tree_node* clone(struct tree_node*);
			}
		}
		'''.parse.assertNoErrors
	}
	
	@Test
	def void addStandardInclude() {
		'''
		delta test {
			modifies source.c {
				adds #include <stdio.h>;
				adds #include <stdlib.h>;
			}
		}
		'''.parse.assertNoErrors
	}
	
	@Test
	def void addFileInclude() {
		'''
		delta test {
			modifies source.c {
				adds #include "header0.h";
				adds #include "header1.h";
			}
		}
		'''.parse.assertNoErrors
	}
	
	@Test
	def void removeStandardInclude() {
		'''
		delta test {
			modifies source.c {
				removes #include <stdio.h>;
				removes #include <stdlib.h>;
			}
		}
		'''.parse.assertNoErrors
	}
	
	@Test
	def void revmoveFileInclude() {
		'''
		delta test {
			modifies source.c {
				removes #include "header0.h";
				removes #include "header1.h";
			}
		}
		'''.parse.assertNoErrors
	}
}