/**
 */
package org.xtext.dop.clang.clang.impl;

import org.eclipse.emf.ecore.EClass;

import org.xtext.dop.clang.clang.BaseTypeLong;
import org.xtext.dop.clang.clang.ClangPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Base Type Long</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BaseTypeLongImpl extends BaseTypeImpl implements BaseTypeLong
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected BaseTypeLongImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ClangPackage.Literals.BASE_TYPE_LONG;
  }

} //BaseTypeLongImpl
