/**
 */
package org.xtext.dop.clang.clang;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Generic Association Case</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.clang.clang.GenericAssociationCase#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getGenericAssociationCase()
 * @model
 * @generated
 */
public interface GenericAssociationCase extends GenericAssociation
{
  /**
   * Returns the value of the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' containment reference.
   * @see #setType(SpecifiedType)
   * @see org.xtext.dop.clang.clang.ClangPackage#getGenericAssociationCase_Type()
   * @model containment="true"
   * @generated
   */
  SpecifiedType getType();

  /**
   * Sets the value of the '{@link org.xtext.dop.clang.clang.GenericAssociationCase#getType <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' containment reference.
   * @see #getType()
   * @generated
   */
  void setType(SpecifiedType value);

} // GenericAssociationCase
