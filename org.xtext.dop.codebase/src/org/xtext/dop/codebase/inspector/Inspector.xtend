package org.xtext.dop.codebase.inspector

import java.util.HashSet
import java.util.Set
import org.eclipse.emf.ecore.resource.Resource
import org.xtext.dop.clang.clang.CompleteName
import org.xtext.dop.clang.clang.SimpleName
import org.xtext.dop.clang.clang.Source
import org.xtext.dop.codebase.codeBase.AddSource
import org.xtext.dop.codebase.codeBase.Adds
import org.xtext.dop.codebase.codeBase.DeltaModule
import org.xtext.dop.codebase.codeBase.FileOperation
import org.xtext.dop.codebase.codeBase.Modifies
import org.xtext.dop.codebase.codeBase.RemoveSource
import org.xtext.dop.codebase.codeBase.Removes

class Inspector {
	
	//val Set<String> set = new HashSet<String>
	
	def inspect(Resource resource) {
		deals(resource.allContents.filter(typeof(DeltaModule)).next)
	}
	
	private def deals(DeltaModule delta) {
		val Set<String> set = new HashSet<String>
		for(FileOperation op: delta.body) {
			switch op {
				Adds:
					for(AddSource source: (op as Adds).files) set.add(source.fileName.getSource)
				Removes:
					for(RemoveSource source: (op as Removes).files) set.add(source.fileName.getSource)
				Modifies:
					set.add((op as Modifies).fileName.getSource)
			}
		}
		set
	}
	
	private def getSource(Source s) {
		switch s {
			SimpleName:
				s.name
			CompleteName:
				s.name+'.'+(s as CompleteName).ext
		}
	}

	
}