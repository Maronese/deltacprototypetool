package org.xtext.dop.clang.tests


import com.google.inject.Inject
import org.eclipse.xtext.junit4.InjectWith
import org.eclipse.xtext.junit4.XtextRunner
import org.eclipse.xtext.junit4.util.ParseHelper
import org.eclipse.xtext.junit4.validation.ValidationTestHelper
import org.junit.Test
import org.junit.runner.RunWith
import org.xtext.dop.clang.ClangInjectorProvider
import org.xtext.dop.clang.clang.Model

@RunWith(typeof(XtextRunner))
@InjectWith(typeof(ClangInjectorProvider))

class CSyntaxTests {
	
	@Inject extension ParseHelper<Model>
	@Inject extension ValidationTestHelper
	
	/*************global declaration**************/
	
	@Test
	def void varDeclCTest() {
		'''
		int i;
		'''.parse.assertNoErrors
	}
	
	@Test
	def void varAssingCTest() {
		'''
		int i = 5;
		'''.parse.assertNoErrors
	}
	
	@Test
	def void array0DeclCTest() {
		'''
		int i[5];
		'''.parse.assertNoErrors
	}
	
	@Test
	def void array1DeclCTest() {
		'''
		int matrix[5][5];
		'''.parse.assertNoErrors
	}
	
	@Test
	def void arrayAssingCTest() {
		'''
		int i[3] = {0, 1, 2};
		'''.parse.assertNoErrors
	}
	
	@Test
	def void typedefCTest() {
		'''
		typedef int** matrix;
		'''.parse.assertNoErrors
	}
	
	@Test
	def void struct0DeclCTest() {
		'''
		struct node {
			int data;
			struct node* next;
		} list;
		'''.parse.assertNoErrors
	}
	
	@Test
	def void struct1DeclCTest() {
		'''
		struct node {
			int data;
			struct node* next;
		} list, note;
		'''.parse.assertNoErrors
	}
	
	@Test
	def void unionDeclCTest() {
		'''
		union {
			int campo1;
			float campo2;
			char campo3[10];
		} vrb;
		'''.parse.assertNoErrors
	}
	
	@Test
	def void enumDeclCTest() {
		'''
		enum season {
			winter,
			spring,
			summer,
			autumn,
		} b;
		'''.parse.assertNoErrors
	}
	
	@Test
	def void typedefStructCTest() {
		'''
		typedef struct node {
			int data;
			struct node* next;
		} list;
		'''.parse.assertNoErrors
	}
	
	@Test
	def void functDecl0CTest() {
		'''
		extern void proc();
		'''.parse.assertNoErrors
	}
	
	@Test
	def void functDecl1CTest() {
		'''
		void proc(int* a, char b);
		'''.parse.assertNoErrors
	}
	
	@Test
	def void functDecl2CTest() {
		'''
		struct c proc(int* a, struct r);
		'''.parse.assertNoErrors
	}
	
	@Test// probably not defined rule for custom type
	def void functDecl3CTest() {
		'''
		$mat proc(int a, struct r);
		'''.parse.assertNoErrors
	}
	
	@Test
	def void functPoint0DeclCTest() {
		'''
		int (*f)();
		'''.parse.assertNoErrors
	}
	
	@Test
	def void functPoint1DeclCTest() {
		'''
		int (*f)(int, char*);
		'''.parse.assertNoErrors
	}
	
	@Test
	def void functPoint2DeclCTest() {
		'''
		int *(*f)(int);
		'''.parse.assertNoErrors
	}
	
	@Test
	def void functPoint3DeclCTest() {
		'''
		int (*(*f)(int))(double);
		'''.parse.assertNoErrors
	}
	
	@Test
	def void functPoint4DeclCTest() {
		'''
		int *(*(*f)(int))(double);
		'''.parse.assertNoErrors
	}
	
	/***************statement***************/
	
	@Test// we are forced to end a declaration with ;
	def void emptyMainCTest() {
		'''
		int main(){};
		'''.parse.assertNoErrors
	}
	
	/* @Test// i don't know if it is or not allowed in some c standard, but generally c compilers allow it
	def void main0CTest() {
		'''
		main(){
			;
		}
		'''.parse.assertNoErrors
	}*/
	
	@Test//it should be allowed
	def void main1CTest() {
		'''
		int main(void){
			;
		};
		'''.parse.assertNoErrors
	}
	
	@Test//it should be allowed
	def void main2CTest() {
		'''
		int main(int argc, char *argv[]){
			;
		};
		'''.parse.assertNoErrors
	}
	
	@Test// compound statement in declaration end with }; NB 4 other test i use };
	def void varMainCTest() {
		'''
		int main(){
			int c=9;
			char l;
		};
		'''.parse.assertNoErrors
	}
	
	@Test// custom type problem
	def void customVarDeclMainCTest() {
		'''
		typedef char[128] string;
		int main(){
			$string s;
		};
		'''.parse.assertNoErrors
	}
	
	@Test
	def void compStatMainCTest() {
		'''
		int main(){
			{
				int a;
			}
		};
		'''.parse.assertNoErrors
	}
	
	@Test
	def void returnStatMainCTest() {
		'''
		int main(){
			return 6;
		};
		'''.parse.assertNoErrors
	}
	
	
	
	@Test
	def void castStatMainCTest() {
		'''
		double swap(int i){
			return (double***)i;
		};
		'''.parse.assertNoErrors
	}
	
	@Test //parentesis expression problem
	def void expr0StatMainCTest() {
		'''
		int main(){
			double d = (13+9);
		};
		'''.parse.assertNoErrors
	}
	
	@Test
	def void ifStatMainCTest() {
		'''
		int swap(int a){
			if(a) return 0;
			return 1;
		};
		'''.parse.assertNoErrors
	}
	
	@Test
	def void ifelStatMainCTest() {
		'''
		int swap(int a){
			if(a) return 0;
			else return 1;
		};
		'''.parse.assertNoErrors
	}
	
	@Test
	def void switchStatMainCTest() {
		'''
		int swap(int a){
			switch(a){
				case 0:
					return 1;
				default:
					return 0;
			}
		};
		'''.parse.assertNoErrors
	}
	
	@Test
	def void for0StatMainCTest() {
		'''
		void loop(int a){
			for(i=0; i<a; i++);
		};
		'''.parse.assertNoErrors
	}
	
	@Test //probably allowed in C11 (not in old c version)
	def void for1StatMainCTest() {
		'''
		void loop(int a){
			for(int i=0; i<a; i++);
		};
		'''.parse.assertNoErrors
	}
	
	@Test
	def void while0StatMainCTest() {
		'''
		void loop(int a){
			while(i<a);
		};
		'''.parse.assertNoErrors
	}
	
	@Test
	def void do0StatMainCTest() {
		'''
		void loop(int a){
			do{;}while(i<a);
		};
		'''.parse.assertNoErrors
	}
	
	@Test
	def void funct0StatMainCTest() {
		'''
		void loop(int a){
			funct();
		};
		'''.parse.assertNoErrors
	}
	
	@Test
	def void funct1StatMainCTest() {
		'''
		void loop(int a){
			funct(a, b);
		};
		'''.parse.assertNoErrors
	}
	
	@Test
	def void funct2StatMainCTest() {
		'''
		void loop(int a){
			a = funct();
		};
		'''.parse.assertNoErrors
	}
	
	@Test
	def void label0StatMainCTest() {
		'''
		int main(){
			l1:;
		};
		'''.parse.assertNoErrors
	}
	
	@Test
	def void label1StatMainCTest() {
		'''
		int main(){
			l1:l2: return;
		};
		'''.parse.assertNoErrors
	}
	
	@Test
	def void gotoStatMainCTest() {
		'''
		int main(){
			goto l1;
		};
		'''.parse.assertNoErrors
	}
	
	@Test
	def void breakStatMainCTest() {
		'''
		int main(){
			break;
		};
		'''.parse.assertNoErrors
	}
	
	@Test
	def void return1StatMainCTest() {
		'''
		int main(){
			return;
		};
		'''.parse.assertNoErrors
	}
	
	@Test
	def void continueStatMainCTest() {
		'''
		int main(){
			continue;
		};
		'''.parse.assertNoErrors
	}
	
	/*************expression****************/
	
	//if statement start with IDENTIFIER cannot be an ExpressionStatement!!
	
	@Test
	def void add0StatMainCTest() {
		'''
		int main(){
			3+3;
		};
		'''.parse.assertNoErrors
	}
	
	@Test
	def void add1StatMainCTest() {
		'''
		int main(){
			3-val;
		};
		'''.parse.assertNoErrors
	}
	
	@Test
	def void mult0StatMainCTest() {
		'''
		int main(){
			3*val;
		};
		'''.parse.assertNoErrors
	}
	
	@Test
	def void mult1StatMainCTest() {
		'''
		int main(){
			val(3,i)%3;
		};
		'''.parse.assertNoErrors
	}
	
	@Test
	def void mult2StatMainCTest() {
		'''
		int main(){
			3/val;
		};
		'''.parse.assertNoErrors
	}
	
	@Test
	def void rel0StatMainCTest() {
		'''
		int main(){
			val<8;
		};
		'''.parse.assertNoErrors
	}
	
	@Test
	def void rel1StatMainCTest() {
		'''
		int main(){
			val<=8;
		};
		'''.parse.assertNoErrors
	}
	
	@Test
	def void rel2StatMainCTest() {
		'''
		int main(){
			val>=8;
		};
		'''.parse.assertNoErrors
	}
	
	@Test
	def void rel3StatMainCTest() {
		'''
		int main(){
			val>8;
		};
		'''.parse.assertNoErrors
	}
	
	@Test
	def void eq0StatMainCTest() {
		'''
		int main(){
			val==8;
		};
		'''.parse.assertNoErrors
	}
	
	@Test
	def void eq1StatMainCTest() {
		'''
		int main(){
			val==8;
		};
		'''.parse.assertNoErrors
	}
	
	@Test
	def void condStatMainCTest() {
		'''
		int swap(int i){
			return i? 1: 0;
		};
		'''.parse.assertNoErrors
	}
	
	@Test
	def void lOrStatMainCTest() {
		'''
		int main(){
			8||8;
		};
		'''.parse.assertNoErrors
	}
	
	@Test
	def void lAndStatMainCTest() {
		'''
		int main(){
			val&&8;
		};
		'''.parse.assertNoErrors
	}
	
	@Test
	def void orStatMainCTest() {
		'''
		int main(){
			val|8;
		};
		'''.parse.assertNoErrors
	}
	
	@Test
	def void andStatMainCTest() {
		'''
		int main(){
			val&8;
		};
		'''.parse.assertNoErrors
	}
	
	@Test
	def void xorStatMainCTest() {
		'''
		int main(){
			val^8;
		};
		'''.parse.assertNoErrors
	}
	
	@Test //seems not an expression
	def void parStatMainCTest() {
		'''
		int main(){
			(val+8);
		};
		'''.parse.assertNoErrors
	}
	
	@Test
	def void arrStatMainCTest() {
		'''
		int main(){
			v[5];
		};
		'''.parse.assertNoErrors
	}
	
	@Test
	def void incrStatMainCTest() {
		'''
		int main(){
			v++;
		};
		'''.parse.assertNoErrors
	}
	
	@Test
	def void structTypeCastMainCTest() {
		'''
		int main(){
			return (struct s{int z;})c;
		};
		'''.parse.assertNoErrors
	}
	
	//preprocessor
	
	@Test
	def void includeStdCTest() {
		'''
		#include <stdio.h>
		#include <mat.h>
		'''.parse.assertNoErrors
	}
	
	@Test
	def void includeHCTest() {
		'''
		#include "test.h"
		#include "prova.h"
		'''.parse.assertNoErrors
	}
}