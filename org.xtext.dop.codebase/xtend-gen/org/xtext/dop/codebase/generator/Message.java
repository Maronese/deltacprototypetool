package org.xtext.dop.codebase.generator;

import java.util.List;
import org.xtext.dop.rappresentation.Project;

@SuppressWarnings("all")
public class Message {
  private List<String> deltas;
  
  private Project project;
  
  public Message(final List<String> deltas, final Project project) {
    this.deltas = deltas;
    this.project = project;
  }
  
  public List<String> getDeltas() {
    return this.deltas;
  }
  
  public Project getProject() {
    return this.project;
  }
}
