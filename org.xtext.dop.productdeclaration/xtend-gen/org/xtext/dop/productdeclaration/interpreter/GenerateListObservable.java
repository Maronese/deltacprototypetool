package org.xtext.dop.productdeclaration.interpreter;

import java.util.List;
import java.util.Observable;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.xtext.dop.codebase.generator.Message;
import org.xtext.dop.productdeclaration.interpreter.Interpreter;
import org.xtext.dop.productdeclaration.productDeclaration.Product;
import org.xtext.dop.rappresentation.DopProject;

@SuppressWarnings("all")
public class GenerateListObservable extends Observable {
  private Interpreter interpreter;
  
  public GenerateListObservable(final Interpreter interpreter) {
    super();
    this.interpreter = interpreter;
  }
  
  public void sendSelection(final Object selection) {
    String _name = ((Product) selection).getName();
    final DopProject project = new DopProject(_name);
    final List<String> deltas = this.interpreter.interpret(((Product) selection));
    String _join = IterableExtensions.join(deltas, ", ");
    InputOutput.<String>println(_join);
    this.setChanged();
    Message _message = new Message(deltas, project);
    this.notifyObservers(_message);
  }
}
