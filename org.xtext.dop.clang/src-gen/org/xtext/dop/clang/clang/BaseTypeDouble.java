/**
 */
package org.xtext.dop.clang.clang;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Base Type Double</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getBaseTypeDouble()
 * @model
 * @generated
 */
public interface BaseTypeDouble extends BaseType
{
} // BaseTypeDouble
