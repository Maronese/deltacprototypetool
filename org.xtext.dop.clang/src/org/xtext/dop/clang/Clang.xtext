grammar org.xtext.dop.clang.Clang hidden(WS, ML_COMMENT, SL_COMMENT)

generate clang "http://www.xtext.org/dop/clang/Clang"

import "http://www.eclipse.org/emf/2002/Ecore" as ecore

Model: 
	includesStd+=StandardInclude*
	includesFile+=FileInclude*
	declarations+=Declaration*
;

StandardInclude:
	{StandardInclude} INCLUDE L_OP (std=Source | wild?=MULT_OP) G_OP
;

FileInclude:
	INCLUDE file=STRING_LITERAL
;

Source
	:	{CompleteName} name=IDENTIFIER DOT ext=IDENTIFIER
	|	{SimpleName} name=IDENTIFIER	
;

/**
 * In this implementation, I went with a very simple approach where I tried to generalize as much as possible what could be.
 * Indeed, if you notice in the companion .c file, C is a very permissive language,
 *  and ensuring that an identifier is not missing in a place where it is required is very complicated to do at the grammar level.
 * 
 * Hence I use the same grammar production for function definition/declaration, as well as for function variable declaration.
 * This is somehow a little bit counter intuitive (type declaration and variable definition being defined with the same grammar production),
 *  but this is the C grammar, it does allow such things!
 * More details in the companion .c file
 */


/*********************************************************/
/********************** declarations *********************/
/*********************************************************/


Declaration
  : GenericSpecifiedDeclaration        // for all declaration that are not typedef
  | TypeAliasDeclaration               // typedef
  | StaticAssertDeclaration   // assert statement present in the original grammar. One of the only thing not raising conflicts, so I keep it. 
  ;
  
TypeAliasDeclaration: TYPEDEF tdecl=SpecifiedType SEMICOLON; // looking closely, typedef is like parameters

GenericSpecifiedDeclaration: specs+=DeclarationSpecifier* type+=(SimpleType)+  decl=GenericDeclarationElements SEMICOLON //modified
; // all declaration can be annotated by keywords like "register", etc.

//NB void funct() {};!!

GenericDeclarationElements: els+=GenericDeclarationElement (COMMA next=GenericDeclarationElements/*els+=GenericDeclarationElement*/)?; //???
GenericDeclarationElement: id=GenericDeclarationId (init=GenericDeclarationInitializer)?;

GenericDeclarationId
  : {GenericDeclaration0} pointers+=MULT_OP+ (restrict=RESTRICT)? (inner=GenericDeclarationInnerId)? (posts+=GenericDeclarationIdPostfix)* 
  | {GenericDeclaration1} inner=GenericDeclarationInnerId (posts+=GenericDeclarationIdPostfix)*
  | {GenericDeclaration2} (posts+=GenericDeclarationIdPostfix)+ (inner=GenericDeclarationInnerId)? //add to name an array typedef
  ;

GenericDeclarationInnerId: {GlobalName} name=IDENTIFIER | LPAREN inner=GenericDeclarationId RPAREN ;
GenericDeclarationIdPostfix: array=ArrayPostfix | function=ParametersPostfix;
ArrayPostfix: {ArrayPostfix} LBRACKET (star=MULT_OP | exp=Expression)? RBRACKET;
ParametersPostfix: {ParametersPostfix} LPAREN (els+=Parameter next+=(NextParameter/*COMMA els+=Parameter*/)*)? (COMMA more=ELLIPSIS)? RPAREN;//???
NextParameter: COMMA els+=Parameter;//add
Parameter: specs+=DeclarationSpecifier* type+=(SimpleType)+ (id=GenericDeclarationId)? ; //modified


GenericDeclarationInitializer
  : {ExpressionInitializer} ASSIGN exp=Expression
  | {FunctionInitializer} block=CompoundStatement
  ;

SpecifiedType: par=Parameter; // naming everything "parameter" is not nice


StaticAssertDeclaration
  : STATIC_ASSERT LPAREN exp=BaseExpression COMMA string=STRING_LITERAL RPAREN SEMICOLON
  ;

/*********************************************************/
/********************* Simple Types **********************/
/*********************************************************/



SimpleType
  : BaseType
  | StructOrUnionType
  | EnumType
  | {Atomic} ATOMIC LPAREN type=(BaseType | StructOrUnionType | EnumType) RPAREN // well, should give a conflict, but it doesn't. OK then :)
  ;

// 1.1. base types: int, float, etc. Not complete for now, as we can have "long long unsigned int", thing like this. BUT adding this wont be difficult
BaseType
  : {BaseTypeVoid} VOID
  | {Custom} id=CUSTOM
  | {BaseTypeChar} CHAR
  | {BaseTypeShort} SHORT
  | {BaseTypeInt} INT
  | {BaseTypeLong} LONG
  | {BaseTypeFloat} FLOAT
  | {BaseTypeDouble} DOUBLE
  | {BaseTypeSigned} SIGNED
  | {BaseTypeUnsigned} UNSIGNED
  | {BaseTypeBool} BOOL
  | {BaseTypeComplex} COMPLEX
  | {BaseTypeImaginary} IMAGINARY     /* non-mandated extension */
  ;

// 1.2. Structured type declaration

StructOrUnionType
  : kind=StructOrUnion ( LCBRACKET content+=GenericSpecifiedDeclaration+ RCBRACKET
                       | name=IDENTIFIER (LCBRACKET content+=GenericSpecifiedDeclaration+ RCBRACKET)? )
  ;
StructOrUnion: STRUCT | UNION ;


EnumType
  : ENUM (LCBRACKET content=EnumeratorList COMMA? RCBRACKET
         | name=IDENTIFIER (LCBRACKET content=EnumeratorList COMMA? RCBRACKET)? )
  ;

EnumeratorList: elements+=Enumerator (COMMA elements+=Enumerator)*;//???
Enumerator: name=EnumerationConstant (ASSIGN exp=BaseExpression)? ;
EnumerationConstant: name=IDENTIFIER ;



/*********************************************************/
/************************ Specifiers *********************/
/*********************************************************/

DeclarationSpecifier
  : StorageClassSpecifier
  | TypeQualifier
  | FunctionSpecifier
  | AlignmentSpecifier
  ;


StorageClassSpecifier
  : {StorageClassSpecifierExtern} EXTERN
  | {StorageClassSpecifierStatic} STATIC
  | {StorageClassSpecifierThreadLocal} THREAD_LOCAL
  | {StorageClassSpecifierAuto} AUTO
  | {StorageClassSpecifierRegister} REGISTER
  ;

TypeQualifier
  : {TypeQualifierConst} CONST
  | {TypeQualifierVolatile} VOLATILE
  | {TypeQualifierAtomic} ATOMIC
  ;

FunctionSpecifier
  : {FunctionSpecifierInline} INLINE
  | {FunctionSpecifierNoReturn} NORETURN
  ;

AlignmentSpecifier
  : ALIGNAS LPAREN (type=SimpleType | ->exp=BaseExpression) RPAREN
  ;



/*********************************************************/
/********************** expressions **********************/
/*********************************************************/


Expression returns Expression: AssignmentExpression;

AssignmentExpression returns Expression: BaseExpression ({AssignmentExpression.exp=current} list+=(AssignmentExpressionElement)+)?;
AssignmentExpressionElement: op=AssignmentOperator exp=BaseExpression;

BaseExpression returns Expression: ConditionalExpression ; /* with constraints */

ConditionalExpression  returns Expression
	:	LogicalOrExpression ({ConditionalExpression.if=current} Q_OP yes=Expression COLON no=ConditionalExpression)?
;

LogicalOrExpression returns Expression
	:	LogicalAndExpression ({LogicalOrExpression.left=current} operand=LOR_OP right=LogicalOrExpression)?
;

LogicalAndExpression returns Expression
	:	InclusiveOrExpression ({LogicalAndExpression.left=current} operand=LAND_OP right=LogicalAndExpression)?
;

InclusiveOrExpression returns Expression
	:	ExclusiveOrExpression ({InclusiveOrExpression.left=current} operand=OR_OP right=InclusiveOrExpression)?
;

ExclusiveOrExpression returns Expression
	:	AndExpression ({ExclusiveOrExpression.left=current} operand=XOR_OP right=ExclusiveOrExpression)?
;

AndExpression returns Expression
	:	EqualityExpression ({AndExpression.left=current} operand=AND_OP right=AndExpression)?
;

EqualityExpression returns Expression
	:	RelationalExpression ({EqualityExpression.left=current} operand=EqualityOperator right=EqualityExpression)?
;

RelationalExpression returns Expression
	:	ShiftExpression ({RelationalExpression.left=current} operand=RelationalOperator right=RelationalExpression)?
;

ShiftExpression returns Expression
	:	AdditiveExpression ({ShiftExpression.left=current} operand=ShiftOperator right=ShiftExpression)?
;

AdditiveExpression returns Expression
	:	MultiplicativeExpression ({AdditiveExpression.left=current} operand=AdditiveOperator right=AdditiveExpression)?
;

MultiplicativeExpression returns Expression
	:	UnaryExpression ({MultiplicativeExpression.left=current} operand=MultiplicativeOperator right=MultiplicativeExpression)?
;

AssignmentOperator
	:	ASSIGN 
	|	MUL_ASSIGN 
	|	DIV_ASSIGN 
	|	MOD_ASSIGN 
	|	ADD_ASSIGN 
	|	SUB_ASSIGN 
	|	LEFT_ASSIGN
	|	RIGHT_ASSIGN
	|	AND_ASSIGN 
	|	XOR_ASSIGN 
	|	OR_ASSIGN
	;
  
EqualityOperator
	:	EQ_OP
	|	NE_OP
;//							'==' | '!='

RelationalOperator
	:	L_OP
	|	G_OP
	|	LE_OP
	|	GE_OP
;//							'<' | '>' | '<=' | '>='

AdditiveOperator
	:	ADD_OP
	|	MINUS_OP
;//							'+' | '-'

MultiplicativeOperator
	:	MULT_OP
	|	DIV_OP
	|	MOD_OP
;//							'*' | '/' | '%'

ShiftOperator
	:	LEFT_OP
	|	RIGHT_OP
;//							'<<' | '>>'



UnaryExpression returns Expression
  : PrimaryExpression ({ExpressionSimple.exp=current}                      next+=PostfixExpressionPostfix+)?
  | {ExpressionComplex}/*(LPAREN type=SpecifiedType RPAREN)?*/ LCBRACKET (exps+=Expression) (COMMA exps+=Expression)* COMMA? RCBRACKET next+=PostfixExpressionPostfix* //?
  | {ExpressionIncrement} INC_OP content=UnaryExpression
  | {ExpressionDecrement} DEC_OP content=UnaryExpression
  | {ExpressionSizeof} SIZEOF (-> content=UnaryExpression | LPAREN type=SpecifiedType RPAREN)//problem
  | {ExpressionUnaryOp} op=UNARY_OP content=UnaryExpression
  | {ExpressionAlign} ALIGNOF LPAREN type=SpecifiedType RPAREN
  | {ExpressionCast} (LPAREN type=SpecifiedType RPAREN) (-> exp=UnaryExpression)
  ;

PrimaryExpression  returns Expression
  : {ExpressionIdentifier} name=IDENTIFIER
  | {ExpressionString} value=StringExpression
  | {ExpressionInteger} value=I_CONSTANT            /* includes character_constant */
  | {ExpressionFloat} value=F_CONSTANT
  | {ExpressionSubExpression} LPAREN par=Expression RPAREN
  | {ExpressionGenericSelection} GENERIC LPAREN left=AssignmentExpression (COMMA right+=GenericAssociation)+ RPAREN //????
  ;

StringExpression
  : name=STRING_LITERAL
  | name=FUNC_NAME
  ;

GenericAssociation
  : {GenericAssociationCase} type=SpecifiedType COLON exp=AssignmentExpression
  | {GenericAssociationDefault} DEFAULT COLON exp=AssignmentExpression
  ;

PostfixExpressionPostfix
  : {PostfixExpressionPostfixExpression} LBRACKET value=Expression RBRACKET
  | {PostfixExpressionPostfixArgumentList} LPAREN (left+=AssignmentExpression next+=(NextAsignement/*COMMA right+=AssignmentExpression*/)* )? RPAREN //???
  | {PostfixExpressionPostfixAccess} DOT value=IDENTIFIER
  | {PostfixExpressionPostfixAccessPtr} PTR_OP value=IDENTIFIER
  | {PostfixExpressionPostfixIncrement} INC_OP
  | {PostfixExpressionPostfixDecrement} DEC_OP
  ;

NextAsignement://add
	COMMA right+=AssignmentExpression
;



/*********************************************************/
/********************** statements ***********************/
/*********************************************************/

CompoundStatement: {CompoundStatement} LCBRACKET  inner+=(BlockItem)* RCBRACKET ;

BlockItem
  : {StatementDeclaration} decl=Declaration 
  | {StatementInnerStatement} statement=Statement
  ;

Statements
  : ->CompoundStatement
  | LabeledStatement
  | ExpressionStatement
  | SelectionStatement
  | IterationStatement
  | JumpStatement
  ;

LabeledStatement
  : {LabeledStatementCase} CASE exp=BaseExpression COLON statement=Statement
  //| {LabeledStatementGoto} goto=IDENTIFIER COLON statement=Statement
  | {LabeledStatementDefault} DEFAULT COLON statement=Statement
  ;

Statement
	: {GoToPrefixStatement} goto=IDENTIFIER COLON statement=Statement
	| Statements
;

ExpressionStatement: {ExpressionStatement} (exp=Expression)? SEMICOLON;

SelectionStatement
  : {SelectionStatementIf} IF LPAREN exp=Expression RPAREN then_statement=Statement (-> ELSE else_statement=Statement)?
  | {SelectionStatementSwitch} SWITCH LPAREN exp=Expression RPAREN LCBRACKET content+=(LabeledStatement)* RCBRACKET 
  ;

IterationStatement
  : {IterationStatementWhile} WHILE LPAREN exp=Expression RPAREN statement=Statement
  | {IterationStatementDo} DO statement=Statement WHILE LPAREN exp=Expression RPAREN SEMICOLON
  | {IterationStatementFor} FOR LPAREN (field1=ExpressionStatement | decl=Declaration) field2=ExpressionStatement (field3=(Expression) next+=(NextField)*)? RPAREN statement=Statement //modified
  ;

NextField:
	COMMA next=AssignmentExpression
;

JumpStatement
  : {JumpStatementGoto} GOTO name=IDENTIFIER SEMICOLON
  | {JumpStatementContinue} CONTINUE SEMICOLON
  | {JumpStatementBreak} BREAK SEMICOLON
  | {JumpStatementReturn} RETURN (exp=Expression)? SEMICOLON
  ;




/*********************************************************/
/****************** syntactic categories *****************/
/*********************************************************/

terminal fragment O: '0' .. '7';
terminal fragment D: '0' .. '9';
terminal fragment NZ: '1' .. '9';
terminal fragment L:  'a' .. 'z' | 'A' .. 'Z' | '_' ;
terminal fragment A:  'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9';
terminal fragment H: 'a' .. 'f' | 'A' .. 'F' | '0' .. '9';
terminal fragment HP: '0x' | '0X' ;
terminal fragment E:   ('E' | 'e') ('+' | '-')? D+ ;
terminal fragment P:   ('P' | 'p') ('+' | '-')? D+ ;
terminal fragment FS:  'f' | 'F' | 'l' | 'L' ;
terminal fragment IS: ((('u'|'U')('l'|'L'|'ll'|'LL')?)|(('l'|'L'|'ll'|'LL')('u'|'U')?)) ;
terminal fragment CP:  'u' | 'U' | 'L' ;
terminal fragment SP:  'u8' | 'u' | 'U' | 'L' ;
terminal fragment ES:  ('\\' ("'" | '"' | '?' | '\\' | 'a' | 'b' | 'f' | 'n' | 'r' | 't' | 'v'| (O | O O | O O O) | 'x' H+)) ;



/*********************************************************/
/************************ comments ***********************/
/*********************************************************/

terminal ML_COMMENT : '/*' -> '*/';
terminal SL_COMMENT   : '//' !('\n'|'\r')* ('\r'? '\n')?;
terminal WS: ' ' | '\t' | '\b' | '\n' | '\f' | '\r';

/*********************************************************/
/************************ keywords ***********************/
/*********************************************************/

terminal AUTO: "auto";
terminal BREAK: "break";
terminal CASE: "case";
terminal CHAR: "char";
terminal CONST: "const";
terminal CONTINUE: "continue";
terminal DEFAULT: "default";
terminal DO: "do";
terminal DOUBLE: "double";
terminal ELSE: "else";
terminal ENUM: "enum";
terminal EXTERN: "extern";
terminal FLOAT: "float";
terminal FOR: "for";
terminal GOTO: "goto";
terminal IF: "if";
terminal INLINE: "inline";
terminal INT: "int";
terminal LONG: "long";
terminal REGISTER: "register";
terminal RESTRICT: "restrict";
terminal RETURN: "return";
terminal SHORT: "short";
terminal SIGNED: "signed";
terminal SIZEOF: "sizeof";
terminal STATIC: "static";
terminal STRUCT: "struct";
terminal SWITCH: "switch";
terminal TYPEDEF: "typedef";
terminal UNION: "union";
terminal UNSIGNED: "unsigned";
terminal VOID: "void";
terminal VOLATILE: "volatile";
terminal WHILE: "while";
terminal ALIGNAS: "_Alignas";
terminal ALIGNOF: "_Alignof";
terminal ATOMIC: "_Atomic";
terminal BOOL: "_Bool";
terminal COMPLEX: "_Complex";
terminal GENERIC: "_Generic";
terminal IMAGINARY: "_Imaginary";
terminal NORETURN: "_Noreturn";
terminal STATIC_ASSERT: "_Static_assert";
terminal THREAD_LOCAL: "_Thread_local";
terminal FUNC_NAME: "__func__";

/*********************************************************/
/********** identifiers, numbers and strings *************/
/*********************************************************/

terminal IDENTIFIER: L A*;
terminal CUSTOM: '$' L A*;
terminal I_CONSTANT
  : HP  H+ IS?
  | NZ D* IS?
  | '0' O* IS?
  | CP? "'" (!("'" | '\\' | '\n') | ES)+ "'"
  ;
terminal  F_CONSTANT
  : D+ E FS?
  | D* '.' D+ E? FS?
  | D+ '.' E? FS?
  | HP H+ P FS?
  | HP H* '.' H+ P FS?
  | HP H+ '.' P FS?
  ;

terminal STRING_LITERAL: (SP? '"' ( !('"' | '\\' | '\n') | ES)* '"')+; // a string can be written on different lines in C...

/*********************************************************/
/*********************** operators ***********************/
/*********************************************************/

terminal ELLIPSIS: "...";//         { return ELLIPSIS; }
terminal ASSIGN: "=";
terminal RIGHT_ASSIGN: ">>=";//         { return RIGHT_ASSIGN; }
terminal LEFT_ASSIGN: "<<=";//         { return LEFT_ASSIGN; }
terminal ADD_ASSIGN: "+=";//          { return ADD_ASSIGN; }
terminal SUB_ASSIGN: "-=";//          { return SUB_ASSIGN; }
terminal MUL_ASSIGN: "*=";//          { return MUL_ASSIGN; }
terminal DIV_ASSIGN: "/=";//          { return DIV_ASSIGN; }
terminal MOD_ASSIGN: "%=";//          { return MOD_ASSIGN; }
terminal AND_ASSIGN: "&=";//          { return AND_ASSIGN; }
terminal XOR_ASSIGN: "^=";//          { return XOR_ASSIGN; }
terminal OR_ASSIGN: "|=";//          { return OR_ASSIGN; }
terminal RIGHT_OP: ">>";//          { return RIGHT_OP; }
terminal LEFT_OP: "<<";//          { return LEFT_OP; }
terminal INC_OP: "++";//          { return INC_OP; }
terminal DEC_OP: "--";//          { return DEC_OP; }
terminal PTR_OP: "->";//          { return PTR_OP; }
terminal LAND_OP: "&&";//          { return AND_OP; }
terminal LOR_OP: "||";//          { return OR_OP; }
terminal LE_OP: "<=";//          { return LE_OP; }
terminal GE_OP: ">=";//          { return GE_OP; }
terminal EQ_OP: "==";//          { return EQ_OP; }
terminal NE_OP: "!=";//          { return NE_OP; }
terminal SEMICOLON: ";";//         { return ';'; }
terminal LCBRACKET: ("{"|"<%");//        { return '{'; }
terminal RCBRACKET: ("}"|"%>");//        { return '}'; }
terminal COMMA: ",";//         { return ','; }
terminal COLON: ":";//         { return ':'; }
terminal LPAREN: "(";//         { return '('; }
terminal RPAREN: ")";//         { return ')'; }
terminal LBRACKET: ("["|"<:");//        { return '['; }
terminal RBRACKET: ("]"|":>");//        { return ']'; }
terminal DOT:  ".";//         { return '.'; }
terminal AND_OP: "&";//         { return '&'; }
terminal NOT_OP: "!";//         { return '!'; }
terminal TILDE: "~";//         { return '~'; }
terminal MINUS_OP: "-";//         { return '-'; }
terminal ADD_OP: "+";//         { return '+'; }
terminal MULT_OP: "*";//         { return '*'; }
terminal DIV_OP: "/";//         { return '/'; }
terminal MOD_OP: "%";//         { return '%'; }
terminal L_OP: "<";//         { return '<'; }
terminal G_OP: ">";//         { return '>'; }
terminal XOR_OP: "^";//         { return '^'; }
terminal OR_OP: "|";//         { return '|'; }
terminal Q_OP: "?";//         { return '?'; }
UNARY_OP: AND_OP | MULT_OP | ADD_OP | MINUS_OP | TILDE | NOT_OP ;

//preprocessor

terminal INCLUDE: "#include";
terminal DQUOTE: '"';