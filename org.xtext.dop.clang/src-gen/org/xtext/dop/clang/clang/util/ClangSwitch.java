/**
 */
package org.xtext.dop.clang.clang.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.xtext.dop.clang.clang.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.xtext.dop.clang.clang.ClangPackage
 * @generated
 */
public class ClangSwitch<T> extends Switch<T>
{
  /**
   * The cached model package
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static ClangPackage modelPackage;

  /**
   * Creates an instance of the switch.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ClangSwitch()
  {
    if (modelPackage == null)
    {
      modelPackage = ClangPackage.eINSTANCE;
    }
  }

  /**
   * Checks whether this is a switch for the given package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param ePackage the package in question.
   * @return whether this is a switch for the given package.
   * @generated
   */
  @Override
  protected boolean isSwitchFor(EPackage ePackage)
  {
    return ePackage == modelPackage;
  }

  /**
   * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the first non-null result returned by a <code>caseXXX</code> call.
   * @generated
   */
  @Override
  protected T doSwitch(int classifierID, EObject theEObject)
  {
    switch (classifierID)
    {
      case ClangPackage.MODEL:
      {
        Model model = (Model)theEObject;
        T result = caseModel(model);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.STANDARD_INCLUDE:
      {
        StandardInclude standardInclude = (StandardInclude)theEObject;
        T result = caseStandardInclude(standardInclude);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.FILE_INCLUDE:
      {
        FileInclude fileInclude = (FileInclude)theEObject;
        T result = caseFileInclude(fileInclude);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.SOURCE:
      {
        Source source = (Source)theEObject;
        T result = caseSource(source);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.DECLARATION:
      {
        Declaration declaration = (Declaration)theEObject;
        T result = caseDeclaration(declaration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.TYPE_ALIAS_DECLARATION:
      {
        TypeAliasDeclaration typeAliasDeclaration = (TypeAliasDeclaration)theEObject;
        T result = caseTypeAliasDeclaration(typeAliasDeclaration);
        if (result == null) result = caseDeclaration(typeAliasDeclaration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.GENERIC_SPECIFIED_DECLARATION:
      {
        GenericSpecifiedDeclaration genericSpecifiedDeclaration = (GenericSpecifiedDeclaration)theEObject;
        T result = caseGenericSpecifiedDeclaration(genericSpecifiedDeclaration);
        if (result == null) result = caseDeclaration(genericSpecifiedDeclaration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.GENERIC_DECLARATION_ELEMENTS:
      {
        GenericDeclarationElements genericDeclarationElements = (GenericDeclarationElements)theEObject;
        T result = caseGenericDeclarationElements(genericDeclarationElements);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.GENERIC_DECLARATION_ELEMENT:
      {
        GenericDeclarationElement genericDeclarationElement = (GenericDeclarationElement)theEObject;
        T result = caseGenericDeclarationElement(genericDeclarationElement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.GENERIC_DECLARATION_ID:
      {
        GenericDeclarationId genericDeclarationId = (GenericDeclarationId)theEObject;
        T result = caseGenericDeclarationId(genericDeclarationId);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.GENERIC_DECLARATION_INNER_ID:
      {
        GenericDeclarationInnerId genericDeclarationInnerId = (GenericDeclarationInnerId)theEObject;
        T result = caseGenericDeclarationInnerId(genericDeclarationInnerId);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.GENERIC_DECLARATION_ID_POSTFIX:
      {
        GenericDeclarationIdPostfix genericDeclarationIdPostfix = (GenericDeclarationIdPostfix)theEObject;
        T result = caseGenericDeclarationIdPostfix(genericDeclarationIdPostfix);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.ARRAY_POSTFIX:
      {
        ArrayPostfix arrayPostfix = (ArrayPostfix)theEObject;
        T result = caseArrayPostfix(arrayPostfix);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.PARAMETERS_POSTFIX:
      {
        ParametersPostfix parametersPostfix = (ParametersPostfix)theEObject;
        T result = caseParametersPostfix(parametersPostfix);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.NEXT_PARAMETER:
      {
        NextParameter nextParameter = (NextParameter)theEObject;
        T result = caseNextParameter(nextParameter);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.PARAMETER:
      {
        Parameter parameter = (Parameter)theEObject;
        T result = caseParameter(parameter);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.GENERIC_DECLARATION_INITIALIZER:
      {
        GenericDeclarationInitializer genericDeclarationInitializer = (GenericDeclarationInitializer)theEObject;
        T result = caseGenericDeclarationInitializer(genericDeclarationInitializer);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.SPECIFIED_TYPE:
      {
        SpecifiedType specifiedType = (SpecifiedType)theEObject;
        T result = caseSpecifiedType(specifiedType);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.STATIC_ASSERT_DECLARATION:
      {
        StaticAssertDeclaration staticAssertDeclaration = (StaticAssertDeclaration)theEObject;
        T result = caseStaticAssertDeclaration(staticAssertDeclaration);
        if (result == null) result = caseDeclaration(staticAssertDeclaration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.SIMPLE_TYPE:
      {
        SimpleType simpleType = (SimpleType)theEObject;
        T result = caseSimpleType(simpleType);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.BASE_TYPE:
      {
        BaseType baseType = (BaseType)theEObject;
        T result = caseBaseType(baseType);
        if (result == null) result = caseSimpleType(baseType);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.STRUCT_OR_UNION_TYPE:
      {
        StructOrUnionType structOrUnionType = (StructOrUnionType)theEObject;
        T result = caseStructOrUnionType(structOrUnionType);
        if (result == null) result = caseSimpleType(structOrUnionType);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.ENUM_TYPE:
      {
        EnumType enumType = (EnumType)theEObject;
        T result = caseEnumType(enumType);
        if (result == null) result = caseSimpleType(enumType);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.ENUMERATOR_LIST:
      {
        EnumeratorList enumeratorList = (EnumeratorList)theEObject;
        T result = caseEnumeratorList(enumeratorList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.ENUMERATOR:
      {
        Enumerator enumerator = (Enumerator)theEObject;
        T result = caseEnumerator(enumerator);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.ENUMERATION_CONSTANT:
      {
        EnumerationConstant enumerationConstant = (EnumerationConstant)theEObject;
        T result = caseEnumerationConstant(enumerationConstant);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.DECLARATION_SPECIFIER:
      {
        DeclarationSpecifier declarationSpecifier = (DeclarationSpecifier)theEObject;
        T result = caseDeclarationSpecifier(declarationSpecifier);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.STORAGE_CLASS_SPECIFIER:
      {
        StorageClassSpecifier storageClassSpecifier = (StorageClassSpecifier)theEObject;
        T result = caseStorageClassSpecifier(storageClassSpecifier);
        if (result == null) result = caseDeclarationSpecifier(storageClassSpecifier);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.TYPE_QUALIFIER:
      {
        TypeQualifier typeQualifier = (TypeQualifier)theEObject;
        T result = caseTypeQualifier(typeQualifier);
        if (result == null) result = caseDeclarationSpecifier(typeQualifier);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.FUNCTION_SPECIFIER:
      {
        FunctionSpecifier functionSpecifier = (FunctionSpecifier)theEObject;
        T result = caseFunctionSpecifier(functionSpecifier);
        if (result == null) result = caseDeclarationSpecifier(functionSpecifier);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.ALIGNMENT_SPECIFIER:
      {
        AlignmentSpecifier alignmentSpecifier = (AlignmentSpecifier)theEObject;
        T result = caseAlignmentSpecifier(alignmentSpecifier);
        if (result == null) result = caseDeclarationSpecifier(alignmentSpecifier);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.EXPRESSION:
      {
        Expression expression = (Expression)theEObject;
        T result = caseExpression(expression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.ASSIGNMENT_EXPRESSION_ELEMENT:
      {
        AssignmentExpressionElement assignmentExpressionElement = (AssignmentExpressionElement)theEObject;
        T result = caseAssignmentExpressionElement(assignmentExpressionElement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.STRING_EXPRESSION:
      {
        StringExpression stringExpression = (StringExpression)theEObject;
        T result = caseStringExpression(stringExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.GENERIC_ASSOCIATION:
      {
        GenericAssociation genericAssociation = (GenericAssociation)theEObject;
        T result = caseGenericAssociation(genericAssociation);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.POSTFIX_EXPRESSION_POSTFIX:
      {
        PostfixExpressionPostfix postfixExpressionPostfix = (PostfixExpressionPostfix)theEObject;
        T result = casePostfixExpressionPostfix(postfixExpressionPostfix);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.NEXT_ASIGNEMENT:
      {
        NextAsignement nextAsignement = (NextAsignement)theEObject;
        T result = caseNextAsignement(nextAsignement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.COMPOUND_STATEMENT:
      {
        CompoundStatement compoundStatement = (CompoundStatement)theEObject;
        T result = caseCompoundStatement(compoundStatement);
        if (result == null) result = caseStatements(compoundStatement);
        if (result == null) result = caseStatement(compoundStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.BLOCK_ITEM:
      {
        BlockItem blockItem = (BlockItem)theEObject;
        T result = caseBlockItem(blockItem);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.STATEMENTS:
      {
        Statements statements = (Statements)theEObject;
        T result = caseStatements(statements);
        if (result == null) result = caseStatement(statements);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.LABELED_STATEMENT:
      {
        LabeledStatement labeledStatement = (LabeledStatement)theEObject;
        T result = caseLabeledStatement(labeledStatement);
        if (result == null) result = caseStatements(labeledStatement);
        if (result == null) result = caseStatement(labeledStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.STATEMENT:
      {
        Statement statement = (Statement)theEObject;
        T result = caseStatement(statement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.EXPRESSION_STATEMENT:
      {
        ExpressionStatement expressionStatement = (ExpressionStatement)theEObject;
        T result = caseExpressionStatement(expressionStatement);
        if (result == null) result = caseStatements(expressionStatement);
        if (result == null) result = caseStatement(expressionStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.SELECTION_STATEMENT:
      {
        SelectionStatement selectionStatement = (SelectionStatement)theEObject;
        T result = caseSelectionStatement(selectionStatement);
        if (result == null) result = caseStatements(selectionStatement);
        if (result == null) result = caseStatement(selectionStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.ITERATION_STATEMENT:
      {
        IterationStatement iterationStatement = (IterationStatement)theEObject;
        T result = caseIterationStatement(iterationStatement);
        if (result == null) result = caseStatements(iterationStatement);
        if (result == null) result = caseStatement(iterationStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.NEXT_FIELD:
      {
        NextField nextField = (NextField)theEObject;
        T result = caseNextField(nextField);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.JUMP_STATEMENT:
      {
        JumpStatement jumpStatement = (JumpStatement)theEObject;
        T result = caseJumpStatement(jumpStatement);
        if (result == null) result = caseStatements(jumpStatement);
        if (result == null) result = caseStatement(jumpStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.COMPLETE_NAME:
      {
        CompleteName completeName = (CompleteName)theEObject;
        T result = caseCompleteName(completeName);
        if (result == null) result = caseSource(completeName);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.SIMPLE_NAME:
      {
        SimpleName simpleName = (SimpleName)theEObject;
        T result = caseSimpleName(simpleName);
        if (result == null) result = caseSource(simpleName);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.GENERIC_DECLARATION0:
      {
        GenericDeclaration0 genericDeclaration0 = (GenericDeclaration0)theEObject;
        T result = caseGenericDeclaration0(genericDeclaration0);
        if (result == null) result = caseGenericDeclarationId(genericDeclaration0);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.GENERIC_DECLARATION1:
      {
        GenericDeclaration1 genericDeclaration1 = (GenericDeclaration1)theEObject;
        T result = caseGenericDeclaration1(genericDeclaration1);
        if (result == null) result = caseGenericDeclarationId(genericDeclaration1);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.GENERIC_DECLARATION2:
      {
        GenericDeclaration2 genericDeclaration2 = (GenericDeclaration2)theEObject;
        T result = caseGenericDeclaration2(genericDeclaration2);
        if (result == null) result = caseGenericDeclarationId(genericDeclaration2);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.GLOBAL_NAME:
      {
        GlobalName globalName = (GlobalName)theEObject;
        T result = caseGlobalName(globalName);
        if (result == null) result = caseGenericDeclarationInnerId(globalName);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.EXPRESSION_INITIALIZER:
      {
        ExpressionInitializer expressionInitializer = (ExpressionInitializer)theEObject;
        T result = caseExpressionInitializer(expressionInitializer);
        if (result == null) result = caseGenericDeclarationInitializer(expressionInitializer);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.FUNCTION_INITIALIZER:
      {
        FunctionInitializer functionInitializer = (FunctionInitializer)theEObject;
        T result = caseFunctionInitializer(functionInitializer);
        if (result == null) result = caseGenericDeclarationInitializer(functionInitializer);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.ATOMIC:
      {
        Atomic atomic = (Atomic)theEObject;
        T result = caseAtomic(atomic);
        if (result == null) result = caseSimpleType(atomic);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.BASE_TYPE_VOID:
      {
        BaseTypeVoid baseTypeVoid = (BaseTypeVoid)theEObject;
        T result = caseBaseTypeVoid(baseTypeVoid);
        if (result == null) result = caseBaseType(baseTypeVoid);
        if (result == null) result = caseSimpleType(baseTypeVoid);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.CUSTOM:
      {
        Custom custom = (Custom)theEObject;
        T result = caseCustom(custom);
        if (result == null) result = caseBaseType(custom);
        if (result == null) result = caseSimpleType(custom);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.BASE_TYPE_CHAR:
      {
        BaseTypeChar baseTypeChar = (BaseTypeChar)theEObject;
        T result = caseBaseTypeChar(baseTypeChar);
        if (result == null) result = caseBaseType(baseTypeChar);
        if (result == null) result = caseSimpleType(baseTypeChar);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.BASE_TYPE_SHORT:
      {
        BaseTypeShort baseTypeShort = (BaseTypeShort)theEObject;
        T result = caseBaseTypeShort(baseTypeShort);
        if (result == null) result = caseBaseType(baseTypeShort);
        if (result == null) result = caseSimpleType(baseTypeShort);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.BASE_TYPE_INT:
      {
        BaseTypeInt baseTypeInt = (BaseTypeInt)theEObject;
        T result = caseBaseTypeInt(baseTypeInt);
        if (result == null) result = caseBaseType(baseTypeInt);
        if (result == null) result = caseSimpleType(baseTypeInt);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.BASE_TYPE_LONG:
      {
        BaseTypeLong baseTypeLong = (BaseTypeLong)theEObject;
        T result = caseBaseTypeLong(baseTypeLong);
        if (result == null) result = caseBaseType(baseTypeLong);
        if (result == null) result = caseSimpleType(baseTypeLong);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.BASE_TYPE_FLOAT:
      {
        BaseTypeFloat baseTypeFloat = (BaseTypeFloat)theEObject;
        T result = caseBaseTypeFloat(baseTypeFloat);
        if (result == null) result = caseBaseType(baseTypeFloat);
        if (result == null) result = caseSimpleType(baseTypeFloat);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.BASE_TYPE_DOUBLE:
      {
        BaseTypeDouble baseTypeDouble = (BaseTypeDouble)theEObject;
        T result = caseBaseTypeDouble(baseTypeDouble);
        if (result == null) result = caseBaseType(baseTypeDouble);
        if (result == null) result = caseSimpleType(baseTypeDouble);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.BASE_TYPE_SIGNED:
      {
        BaseTypeSigned baseTypeSigned = (BaseTypeSigned)theEObject;
        T result = caseBaseTypeSigned(baseTypeSigned);
        if (result == null) result = caseBaseType(baseTypeSigned);
        if (result == null) result = caseSimpleType(baseTypeSigned);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.BASE_TYPE_UNSIGNED:
      {
        BaseTypeUnsigned baseTypeUnsigned = (BaseTypeUnsigned)theEObject;
        T result = caseBaseTypeUnsigned(baseTypeUnsigned);
        if (result == null) result = caseBaseType(baseTypeUnsigned);
        if (result == null) result = caseSimpleType(baseTypeUnsigned);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.BASE_TYPE_BOOL:
      {
        BaseTypeBool baseTypeBool = (BaseTypeBool)theEObject;
        T result = caseBaseTypeBool(baseTypeBool);
        if (result == null) result = caseBaseType(baseTypeBool);
        if (result == null) result = caseSimpleType(baseTypeBool);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.BASE_TYPE_COMPLEX:
      {
        BaseTypeComplex baseTypeComplex = (BaseTypeComplex)theEObject;
        T result = caseBaseTypeComplex(baseTypeComplex);
        if (result == null) result = caseBaseType(baseTypeComplex);
        if (result == null) result = caseSimpleType(baseTypeComplex);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.BASE_TYPE_IMAGINARY:
      {
        BaseTypeImaginary baseTypeImaginary = (BaseTypeImaginary)theEObject;
        T result = caseBaseTypeImaginary(baseTypeImaginary);
        if (result == null) result = caseBaseType(baseTypeImaginary);
        if (result == null) result = caseSimpleType(baseTypeImaginary);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.STORAGE_CLASS_SPECIFIER_EXTERN:
      {
        StorageClassSpecifierExtern storageClassSpecifierExtern = (StorageClassSpecifierExtern)theEObject;
        T result = caseStorageClassSpecifierExtern(storageClassSpecifierExtern);
        if (result == null) result = caseStorageClassSpecifier(storageClassSpecifierExtern);
        if (result == null) result = caseDeclarationSpecifier(storageClassSpecifierExtern);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.STORAGE_CLASS_SPECIFIER_STATIC:
      {
        StorageClassSpecifierStatic storageClassSpecifierStatic = (StorageClassSpecifierStatic)theEObject;
        T result = caseStorageClassSpecifierStatic(storageClassSpecifierStatic);
        if (result == null) result = caseStorageClassSpecifier(storageClassSpecifierStatic);
        if (result == null) result = caseDeclarationSpecifier(storageClassSpecifierStatic);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.STORAGE_CLASS_SPECIFIER_THREAD_LOCAL:
      {
        StorageClassSpecifierThreadLocal storageClassSpecifierThreadLocal = (StorageClassSpecifierThreadLocal)theEObject;
        T result = caseStorageClassSpecifierThreadLocal(storageClassSpecifierThreadLocal);
        if (result == null) result = caseStorageClassSpecifier(storageClassSpecifierThreadLocal);
        if (result == null) result = caseDeclarationSpecifier(storageClassSpecifierThreadLocal);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.STORAGE_CLASS_SPECIFIER_AUTO:
      {
        StorageClassSpecifierAuto storageClassSpecifierAuto = (StorageClassSpecifierAuto)theEObject;
        T result = caseStorageClassSpecifierAuto(storageClassSpecifierAuto);
        if (result == null) result = caseStorageClassSpecifier(storageClassSpecifierAuto);
        if (result == null) result = caseDeclarationSpecifier(storageClassSpecifierAuto);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.STORAGE_CLASS_SPECIFIER_REGISTER:
      {
        StorageClassSpecifierRegister storageClassSpecifierRegister = (StorageClassSpecifierRegister)theEObject;
        T result = caseStorageClassSpecifierRegister(storageClassSpecifierRegister);
        if (result == null) result = caseStorageClassSpecifier(storageClassSpecifierRegister);
        if (result == null) result = caseDeclarationSpecifier(storageClassSpecifierRegister);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.TYPE_QUALIFIER_CONST:
      {
        TypeQualifierConst typeQualifierConst = (TypeQualifierConst)theEObject;
        T result = caseTypeQualifierConst(typeQualifierConst);
        if (result == null) result = caseTypeQualifier(typeQualifierConst);
        if (result == null) result = caseDeclarationSpecifier(typeQualifierConst);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.TYPE_QUALIFIER_VOLATILE:
      {
        TypeQualifierVolatile typeQualifierVolatile = (TypeQualifierVolatile)theEObject;
        T result = caseTypeQualifierVolatile(typeQualifierVolatile);
        if (result == null) result = caseTypeQualifier(typeQualifierVolatile);
        if (result == null) result = caseDeclarationSpecifier(typeQualifierVolatile);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.TYPE_QUALIFIER_ATOMIC:
      {
        TypeQualifierAtomic typeQualifierAtomic = (TypeQualifierAtomic)theEObject;
        T result = caseTypeQualifierAtomic(typeQualifierAtomic);
        if (result == null) result = caseTypeQualifier(typeQualifierAtomic);
        if (result == null) result = caseDeclarationSpecifier(typeQualifierAtomic);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.FUNCTION_SPECIFIER_INLINE:
      {
        FunctionSpecifierInline functionSpecifierInline = (FunctionSpecifierInline)theEObject;
        T result = caseFunctionSpecifierInline(functionSpecifierInline);
        if (result == null) result = caseFunctionSpecifier(functionSpecifierInline);
        if (result == null) result = caseDeclarationSpecifier(functionSpecifierInline);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.FUNCTION_SPECIFIER_NO_RETURN:
      {
        FunctionSpecifierNoReturn functionSpecifierNoReturn = (FunctionSpecifierNoReturn)theEObject;
        T result = caseFunctionSpecifierNoReturn(functionSpecifierNoReturn);
        if (result == null) result = caseFunctionSpecifier(functionSpecifierNoReturn);
        if (result == null) result = caseDeclarationSpecifier(functionSpecifierNoReturn);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.ASSIGNMENT_EXPRESSION:
      {
        AssignmentExpression assignmentExpression = (AssignmentExpression)theEObject;
        T result = caseAssignmentExpression(assignmentExpression);
        if (result == null) result = caseExpression(assignmentExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.CONDITIONAL_EXPRESSION:
      {
        ConditionalExpression conditionalExpression = (ConditionalExpression)theEObject;
        T result = caseConditionalExpression(conditionalExpression);
        if (result == null) result = caseExpression(conditionalExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.LOGICAL_OR_EXPRESSION:
      {
        LogicalOrExpression logicalOrExpression = (LogicalOrExpression)theEObject;
        T result = caseLogicalOrExpression(logicalOrExpression);
        if (result == null) result = caseExpression(logicalOrExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.LOGICAL_AND_EXPRESSION:
      {
        LogicalAndExpression logicalAndExpression = (LogicalAndExpression)theEObject;
        T result = caseLogicalAndExpression(logicalAndExpression);
        if (result == null) result = caseExpression(logicalAndExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.INCLUSIVE_OR_EXPRESSION:
      {
        InclusiveOrExpression inclusiveOrExpression = (InclusiveOrExpression)theEObject;
        T result = caseInclusiveOrExpression(inclusiveOrExpression);
        if (result == null) result = caseExpression(inclusiveOrExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.EXCLUSIVE_OR_EXPRESSION:
      {
        ExclusiveOrExpression exclusiveOrExpression = (ExclusiveOrExpression)theEObject;
        T result = caseExclusiveOrExpression(exclusiveOrExpression);
        if (result == null) result = caseExpression(exclusiveOrExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.AND_EXPRESSION:
      {
        AndExpression andExpression = (AndExpression)theEObject;
        T result = caseAndExpression(andExpression);
        if (result == null) result = caseExpression(andExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.EQUALITY_EXPRESSION:
      {
        EqualityExpression equalityExpression = (EqualityExpression)theEObject;
        T result = caseEqualityExpression(equalityExpression);
        if (result == null) result = caseExpression(equalityExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.RELATIONAL_EXPRESSION:
      {
        RelationalExpression relationalExpression = (RelationalExpression)theEObject;
        T result = caseRelationalExpression(relationalExpression);
        if (result == null) result = caseExpression(relationalExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.SHIFT_EXPRESSION:
      {
        ShiftExpression shiftExpression = (ShiftExpression)theEObject;
        T result = caseShiftExpression(shiftExpression);
        if (result == null) result = caseExpression(shiftExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.ADDITIVE_EXPRESSION:
      {
        AdditiveExpression additiveExpression = (AdditiveExpression)theEObject;
        T result = caseAdditiveExpression(additiveExpression);
        if (result == null) result = caseExpression(additiveExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.MULTIPLICATIVE_EXPRESSION:
      {
        MultiplicativeExpression multiplicativeExpression = (MultiplicativeExpression)theEObject;
        T result = caseMultiplicativeExpression(multiplicativeExpression);
        if (result == null) result = caseExpression(multiplicativeExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.EXPRESSION_SIMPLE:
      {
        ExpressionSimple expressionSimple = (ExpressionSimple)theEObject;
        T result = caseExpressionSimple(expressionSimple);
        if (result == null) result = caseExpression(expressionSimple);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.EXPRESSION_COMPLEX:
      {
        ExpressionComplex expressionComplex = (ExpressionComplex)theEObject;
        T result = caseExpressionComplex(expressionComplex);
        if (result == null) result = caseExpression(expressionComplex);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.EXPRESSION_INCREMENT:
      {
        ExpressionIncrement expressionIncrement = (ExpressionIncrement)theEObject;
        T result = caseExpressionIncrement(expressionIncrement);
        if (result == null) result = caseExpression(expressionIncrement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.EXPRESSION_DECREMENT:
      {
        ExpressionDecrement expressionDecrement = (ExpressionDecrement)theEObject;
        T result = caseExpressionDecrement(expressionDecrement);
        if (result == null) result = caseExpression(expressionDecrement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.EXPRESSION_SIZEOF:
      {
        ExpressionSizeof expressionSizeof = (ExpressionSizeof)theEObject;
        T result = caseExpressionSizeof(expressionSizeof);
        if (result == null) result = caseExpression(expressionSizeof);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.EXPRESSION_UNARY_OP:
      {
        ExpressionUnaryOp expressionUnaryOp = (ExpressionUnaryOp)theEObject;
        T result = caseExpressionUnaryOp(expressionUnaryOp);
        if (result == null) result = caseExpression(expressionUnaryOp);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.EXPRESSION_ALIGN:
      {
        ExpressionAlign expressionAlign = (ExpressionAlign)theEObject;
        T result = caseExpressionAlign(expressionAlign);
        if (result == null) result = caseExpression(expressionAlign);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.EXPRESSION_CAST:
      {
        ExpressionCast expressionCast = (ExpressionCast)theEObject;
        T result = caseExpressionCast(expressionCast);
        if (result == null) result = caseExpression(expressionCast);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.EXPRESSION_IDENTIFIER:
      {
        ExpressionIdentifier expressionIdentifier = (ExpressionIdentifier)theEObject;
        T result = caseExpressionIdentifier(expressionIdentifier);
        if (result == null) result = caseExpression(expressionIdentifier);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.EXPRESSION_STRING:
      {
        ExpressionString expressionString = (ExpressionString)theEObject;
        T result = caseExpressionString(expressionString);
        if (result == null) result = caseExpression(expressionString);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.EXPRESSION_INTEGER:
      {
        ExpressionInteger expressionInteger = (ExpressionInteger)theEObject;
        T result = caseExpressionInteger(expressionInteger);
        if (result == null) result = caseExpression(expressionInteger);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.EXPRESSION_FLOAT:
      {
        ExpressionFloat expressionFloat = (ExpressionFloat)theEObject;
        T result = caseExpressionFloat(expressionFloat);
        if (result == null) result = caseExpression(expressionFloat);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.EXPRESSION_SUB_EXPRESSION:
      {
        ExpressionSubExpression expressionSubExpression = (ExpressionSubExpression)theEObject;
        T result = caseExpressionSubExpression(expressionSubExpression);
        if (result == null) result = caseExpression(expressionSubExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.EXPRESSION_GENERIC_SELECTION:
      {
        ExpressionGenericSelection expressionGenericSelection = (ExpressionGenericSelection)theEObject;
        T result = caseExpressionGenericSelection(expressionGenericSelection);
        if (result == null) result = caseExpression(expressionGenericSelection);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.GENERIC_ASSOCIATION_CASE:
      {
        GenericAssociationCase genericAssociationCase = (GenericAssociationCase)theEObject;
        T result = caseGenericAssociationCase(genericAssociationCase);
        if (result == null) result = caseGenericAssociation(genericAssociationCase);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.GENERIC_ASSOCIATION_DEFAULT:
      {
        GenericAssociationDefault genericAssociationDefault = (GenericAssociationDefault)theEObject;
        T result = caseGenericAssociationDefault(genericAssociationDefault);
        if (result == null) result = caseGenericAssociation(genericAssociationDefault);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.POSTFIX_EXPRESSION_POSTFIX_EXPRESSION:
      {
        PostfixExpressionPostfixExpression postfixExpressionPostfixExpression = (PostfixExpressionPostfixExpression)theEObject;
        T result = casePostfixExpressionPostfixExpression(postfixExpressionPostfixExpression);
        if (result == null) result = casePostfixExpressionPostfix(postfixExpressionPostfixExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.POSTFIX_EXPRESSION_POSTFIX_ARGUMENT_LIST:
      {
        PostfixExpressionPostfixArgumentList postfixExpressionPostfixArgumentList = (PostfixExpressionPostfixArgumentList)theEObject;
        T result = casePostfixExpressionPostfixArgumentList(postfixExpressionPostfixArgumentList);
        if (result == null) result = casePostfixExpressionPostfix(postfixExpressionPostfixArgumentList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.POSTFIX_EXPRESSION_POSTFIX_ACCESS:
      {
        PostfixExpressionPostfixAccess postfixExpressionPostfixAccess = (PostfixExpressionPostfixAccess)theEObject;
        T result = casePostfixExpressionPostfixAccess(postfixExpressionPostfixAccess);
        if (result == null) result = casePostfixExpressionPostfix(postfixExpressionPostfixAccess);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.POSTFIX_EXPRESSION_POSTFIX_ACCESS_PTR:
      {
        PostfixExpressionPostfixAccessPtr postfixExpressionPostfixAccessPtr = (PostfixExpressionPostfixAccessPtr)theEObject;
        T result = casePostfixExpressionPostfixAccessPtr(postfixExpressionPostfixAccessPtr);
        if (result == null) result = casePostfixExpressionPostfix(postfixExpressionPostfixAccessPtr);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.POSTFIX_EXPRESSION_POSTFIX_INCREMENT:
      {
        PostfixExpressionPostfixIncrement postfixExpressionPostfixIncrement = (PostfixExpressionPostfixIncrement)theEObject;
        T result = casePostfixExpressionPostfixIncrement(postfixExpressionPostfixIncrement);
        if (result == null) result = casePostfixExpressionPostfix(postfixExpressionPostfixIncrement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.POSTFIX_EXPRESSION_POSTFIX_DECREMENT:
      {
        PostfixExpressionPostfixDecrement postfixExpressionPostfixDecrement = (PostfixExpressionPostfixDecrement)theEObject;
        T result = casePostfixExpressionPostfixDecrement(postfixExpressionPostfixDecrement);
        if (result == null) result = casePostfixExpressionPostfix(postfixExpressionPostfixDecrement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.STATEMENT_DECLARATION:
      {
        StatementDeclaration statementDeclaration = (StatementDeclaration)theEObject;
        T result = caseStatementDeclaration(statementDeclaration);
        if (result == null) result = caseBlockItem(statementDeclaration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.STATEMENT_INNER_STATEMENT:
      {
        StatementInnerStatement statementInnerStatement = (StatementInnerStatement)theEObject;
        T result = caseStatementInnerStatement(statementInnerStatement);
        if (result == null) result = caseBlockItem(statementInnerStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.LABELED_STATEMENT_CASE:
      {
        LabeledStatementCase labeledStatementCase = (LabeledStatementCase)theEObject;
        T result = caseLabeledStatementCase(labeledStatementCase);
        if (result == null) result = caseLabeledStatement(labeledStatementCase);
        if (result == null) result = caseStatements(labeledStatementCase);
        if (result == null) result = caseStatement(labeledStatementCase);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.LABELED_STATEMENT_DEFAULT:
      {
        LabeledStatementDefault labeledStatementDefault = (LabeledStatementDefault)theEObject;
        T result = caseLabeledStatementDefault(labeledStatementDefault);
        if (result == null) result = caseLabeledStatement(labeledStatementDefault);
        if (result == null) result = caseStatements(labeledStatementDefault);
        if (result == null) result = caseStatement(labeledStatementDefault);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.GO_TO_PREFIX_STATEMENT:
      {
        GoToPrefixStatement goToPrefixStatement = (GoToPrefixStatement)theEObject;
        T result = caseGoToPrefixStatement(goToPrefixStatement);
        if (result == null) result = caseStatement(goToPrefixStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.SELECTION_STATEMENT_IF:
      {
        SelectionStatementIf selectionStatementIf = (SelectionStatementIf)theEObject;
        T result = caseSelectionStatementIf(selectionStatementIf);
        if (result == null) result = caseSelectionStatement(selectionStatementIf);
        if (result == null) result = caseStatements(selectionStatementIf);
        if (result == null) result = caseStatement(selectionStatementIf);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.SELECTION_STATEMENT_SWITCH:
      {
        SelectionStatementSwitch selectionStatementSwitch = (SelectionStatementSwitch)theEObject;
        T result = caseSelectionStatementSwitch(selectionStatementSwitch);
        if (result == null) result = caseSelectionStatement(selectionStatementSwitch);
        if (result == null) result = caseStatements(selectionStatementSwitch);
        if (result == null) result = caseStatement(selectionStatementSwitch);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.ITERATION_STATEMENT_WHILE:
      {
        IterationStatementWhile iterationStatementWhile = (IterationStatementWhile)theEObject;
        T result = caseIterationStatementWhile(iterationStatementWhile);
        if (result == null) result = caseIterationStatement(iterationStatementWhile);
        if (result == null) result = caseStatements(iterationStatementWhile);
        if (result == null) result = caseStatement(iterationStatementWhile);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.ITERATION_STATEMENT_DO:
      {
        IterationStatementDo iterationStatementDo = (IterationStatementDo)theEObject;
        T result = caseIterationStatementDo(iterationStatementDo);
        if (result == null) result = caseIterationStatement(iterationStatementDo);
        if (result == null) result = caseStatements(iterationStatementDo);
        if (result == null) result = caseStatement(iterationStatementDo);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.ITERATION_STATEMENT_FOR:
      {
        IterationStatementFor iterationStatementFor = (IterationStatementFor)theEObject;
        T result = caseIterationStatementFor(iterationStatementFor);
        if (result == null) result = caseIterationStatement(iterationStatementFor);
        if (result == null) result = caseStatements(iterationStatementFor);
        if (result == null) result = caseStatement(iterationStatementFor);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.JUMP_STATEMENT_GOTO:
      {
        JumpStatementGoto jumpStatementGoto = (JumpStatementGoto)theEObject;
        T result = caseJumpStatementGoto(jumpStatementGoto);
        if (result == null) result = caseJumpStatement(jumpStatementGoto);
        if (result == null) result = caseStatements(jumpStatementGoto);
        if (result == null) result = caseStatement(jumpStatementGoto);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.JUMP_STATEMENT_CONTINUE:
      {
        JumpStatementContinue jumpStatementContinue = (JumpStatementContinue)theEObject;
        T result = caseJumpStatementContinue(jumpStatementContinue);
        if (result == null) result = caseJumpStatement(jumpStatementContinue);
        if (result == null) result = caseStatements(jumpStatementContinue);
        if (result == null) result = caseStatement(jumpStatementContinue);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.JUMP_STATEMENT_BREAK:
      {
        JumpStatementBreak jumpStatementBreak = (JumpStatementBreak)theEObject;
        T result = caseJumpStatementBreak(jumpStatementBreak);
        if (result == null) result = caseJumpStatement(jumpStatementBreak);
        if (result == null) result = caseStatements(jumpStatementBreak);
        if (result == null) result = caseStatement(jumpStatementBreak);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ClangPackage.JUMP_STATEMENT_RETURN:
      {
        JumpStatementReturn jumpStatementReturn = (JumpStatementReturn)theEObject;
        T result = caseJumpStatementReturn(jumpStatementReturn);
        if (result == null) result = caseJumpStatement(jumpStatementReturn);
        if (result == null) result = caseStatements(jumpStatementReturn);
        if (result == null) result = caseStatement(jumpStatementReturn);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      default: return defaultCase(theEObject);
    }
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Model</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Model</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseModel(Model object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Standard Include</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Standard Include</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseStandardInclude(StandardInclude object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>File Include</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>File Include</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFileInclude(FileInclude object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Source</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Source</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSource(Source object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Declaration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Declaration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDeclaration(Declaration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Type Alias Declaration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Type Alias Declaration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTypeAliasDeclaration(TypeAliasDeclaration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Generic Specified Declaration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Generic Specified Declaration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseGenericSpecifiedDeclaration(GenericSpecifiedDeclaration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Generic Declaration Elements</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Generic Declaration Elements</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseGenericDeclarationElements(GenericDeclarationElements object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Generic Declaration Element</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Generic Declaration Element</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseGenericDeclarationElement(GenericDeclarationElement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Generic Declaration Id</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Generic Declaration Id</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseGenericDeclarationId(GenericDeclarationId object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Generic Declaration Inner Id</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Generic Declaration Inner Id</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseGenericDeclarationInnerId(GenericDeclarationInnerId object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Generic Declaration Id Postfix</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Generic Declaration Id Postfix</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseGenericDeclarationIdPostfix(GenericDeclarationIdPostfix object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Array Postfix</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Array Postfix</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseArrayPostfix(ArrayPostfix object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Parameters Postfix</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Parameters Postfix</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseParametersPostfix(ParametersPostfix object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Next Parameter</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Next Parameter</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseNextParameter(NextParameter object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Parameter</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Parameter</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseParameter(Parameter object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Generic Declaration Initializer</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Generic Declaration Initializer</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseGenericDeclarationInitializer(GenericDeclarationInitializer object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Specified Type</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Specified Type</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSpecifiedType(SpecifiedType object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Static Assert Declaration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Static Assert Declaration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseStaticAssertDeclaration(StaticAssertDeclaration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Simple Type</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Simple Type</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSimpleType(SimpleType object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Base Type</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Base Type</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseBaseType(BaseType object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Struct Or Union Type</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Struct Or Union Type</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseStructOrUnionType(StructOrUnionType object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Enum Type</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Enum Type</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseEnumType(EnumType object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Enumerator List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Enumerator List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseEnumeratorList(EnumeratorList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Enumerator</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Enumerator</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseEnumerator(Enumerator object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Enumeration Constant</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Enumeration Constant</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseEnumerationConstant(EnumerationConstant object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Declaration Specifier</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Declaration Specifier</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDeclarationSpecifier(DeclarationSpecifier object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Storage Class Specifier</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Storage Class Specifier</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseStorageClassSpecifier(StorageClassSpecifier object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Type Qualifier</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Type Qualifier</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTypeQualifier(TypeQualifier object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Function Specifier</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Function Specifier</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFunctionSpecifier(FunctionSpecifier object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Alignment Specifier</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Alignment Specifier</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAlignmentSpecifier(AlignmentSpecifier object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExpression(Expression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Assignment Expression Element</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Assignment Expression Element</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAssignmentExpressionElement(AssignmentExpressionElement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>String Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>String Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseStringExpression(StringExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Generic Association</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Generic Association</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseGenericAssociation(GenericAssociation object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Postfix Expression Postfix</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Postfix Expression Postfix</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePostfixExpressionPostfix(PostfixExpressionPostfix object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Next Asignement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Next Asignement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseNextAsignement(NextAsignement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Compound Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Compound Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCompoundStatement(CompoundStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Block Item</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Block Item</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseBlockItem(BlockItem object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Statements</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Statements</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseStatements(Statements object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Labeled Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Labeled Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLabeledStatement(LabeledStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseStatement(Statement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Expression Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Expression Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExpressionStatement(ExpressionStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Selection Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Selection Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSelectionStatement(SelectionStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Iteration Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Iteration Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseIterationStatement(IterationStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Next Field</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Next Field</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseNextField(NextField object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Jump Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Jump Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseJumpStatement(JumpStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Complete Name</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Complete Name</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCompleteName(CompleteName object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Simple Name</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Simple Name</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSimpleName(SimpleName object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Generic Declaration0</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Generic Declaration0</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseGenericDeclaration0(GenericDeclaration0 object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Generic Declaration1</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Generic Declaration1</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseGenericDeclaration1(GenericDeclaration1 object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Generic Declaration2</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Generic Declaration2</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseGenericDeclaration2(GenericDeclaration2 object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Global Name</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Global Name</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseGlobalName(GlobalName object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Expression Initializer</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Expression Initializer</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExpressionInitializer(ExpressionInitializer object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Function Initializer</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Function Initializer</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFunctionInitializer(FunctionInitializer object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Atomic</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Atomic</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAtomic(Atomic object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Base Type Void</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Base Type Void</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseBaseTypeVoid(BaseTypeVoid object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Custom</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Custom</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCustom(Custom object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Base Type Char</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Base Type Char</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseBaseTypeChar(BaseTypeChar object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Base Type Short</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Base Type Short</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseBaseTypeShort(BaseTypeShort object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Base Type Int</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Base Type Int</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseBaseTypeInt(BaseTypeInt object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Base Type Long</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Base Type Long</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseBaseTypeLong(BaseTypeLong object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Base Type Float</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Base Type Float</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseBaseTypeFloat(BaseTypeFloat object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Base Type Double</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Base Type Double</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseBaseTypeDouble(BaseTypeDouble object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Base Type Signed</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Base Type Signed</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseBaseTypeSigned(BaseTypeSigned object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Base Type Unsigned</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Base Type Unsigned</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseBaseTypeUnsigned(BaseTypeUnsigned object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Base Type Bool</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Base Type Bool</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseBaseTypeBool(BaseTypeBool object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Base Type Complex</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Base Type Complex</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseBaseTypeComplex(BaseTypeComplex object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Base Type Imaginary</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Base Type Imaginary</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseBaseTypeImaginary(BaseTypeImaginary object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Storage Class Specifier Extern</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Storage Class Specifier Extern</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseStorageClassSpecifierExtern(StorageClassSpecifierExtern object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Storage Class Specifier Static</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Storage Class Specifier Static</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseStorageClassSpecifierStatic(StorageClassSpecifierStatic object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Storage Class Specifier Thread Local</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Storage Class Specifier Thread Local</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseStorageClassSpecifierThreadLocal(StorageClassSpecifierThreadLocal object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Storage Class Specifier Auto</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Storage Class Specifier Auto</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseStorageClassSpecifierAuto(StorageClassSpecifierAuto object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Storage Class Specifier Register</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Storage Class Specifier Register</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseStorageClassSpecifierRegister(StorageClassSpecifierRegister object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Type Qualifier Const</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Type Qualifier Const</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTypeQualifierConst(TypeQualifierConst object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Type Qualifier Volatile</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Type Qualifier Volatile</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTypeQualifierVolatile(TypeQualifierVolatile object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Type Qualifier Atomic</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Type Qualifier Atomic</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTypeQualifierAtomic(TypeQualifierAtomic object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Function Specifier Inline</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Function Specifier Inline</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFunctionSpecifierInline(FunctionSpecifierInline object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Function Specifier No Return</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Function Specifier No Return</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFunctionSpecifierNoReturn(FunctionSpecifierNoReturn object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Assignment Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Assignment Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAssignmentExpression(AssignmentExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Conditional Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Conditional Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseConditionalExpression(ConditionalExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Logical Or Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Logical Or Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLogicalOrExpression(LogicalOrExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Logical And Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Logical And Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLogicalAndExpression(LogicalAndExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Inclusive Or Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Inclusive Or Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseInclusiveOrExpression(InclusiveOrExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Exclusive Or Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Exclusive Or Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExclusiveOrExpression(ExclusiveOrExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>And Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>And Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAndExpression(AndExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Equality Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Equality Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseEqualityExpression(EqualityExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Relational Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Relational Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRelationalExpression(RelationalExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Shift Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Shift Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseShiftExpression(ShiftExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Additive Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Additive Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAdditiveExpression(AdditiveExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Multiplicative Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Multiplicative Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseMultiplicativeExpression(MultiplicativeExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Expression Simple</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Expression Simple</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExpressionSimple(ExpressionSimple object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Expression Complex</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Expression Complex</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExpressionComplex(ExpressionComplex object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Expression Increment</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Expression Increment</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExpressionIncrement(ExpressionIncrement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Expression Decrement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Expression Decrement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExpressionDecrement(ExpressionDecrement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Expression Sizeof</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Expression Sizeof</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExpressionSizeof(ExpressionSizeof object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Expression Unary Op</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Expression Unary Op</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExpressionUnaryOp(ExpressionUnaryOp object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Expression Align</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Expression Align</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExpressionAlign(ExpressionAlign object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Expression Cast</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Expression Cast</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExpressionCast(ExpressionCast object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Expression Identifier</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Expression Identifier</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExpressionIdentifier(ExpressionIdentifier object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Expression String</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Expression String</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExpressionString(ExpressionString object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Expression Integer</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Expression Integer</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExpressionInteger(ExpressionInteger object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Expression Float</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Expression Float</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExpressionFloat(ExpressionFloat object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Expression Sub Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Expression Sub Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExpressionSubExpression(ExpressionSubExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Expression Generic Selection</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Expression Generic Selection</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExpressionGenericSelection(ExpressionGenericSelection object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Generic Association Case</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Generic Association Case</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseGenericAssociationCase(GenericAssociationCase object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Generic Association Default</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Generic Association Default</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseGenericAssociationDefault(GenericAssociationDefault object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Postfix Expression Postfix Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Postfix Expression Postfix Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePostfixExpressionPostfixExpression(PostfixExpressionPostfixExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Postfix Expression Postfix Argument List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Postfix Expression Postfix Argument List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePostfixExpressionPostfixArgumentList(PostfixExpressionPostfixArgumentList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Postfix Expression Postfix Access</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Postfix Expression Postfix Access</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePostfixExpressionPostfixAccess(PostfixExpressionPostfixAccess object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Postfix Expression Postfix Access Ptr</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Postfix Expression Postfix Access Ptr</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePostfixExpressionPostfixAccessPtr(PostfixExpressionPostfixAccessPtr object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Postfix Expression Postfix Increment</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Postfix Expression Postfix Increment</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePostfixExpressionPostfixIncrement(PostfixExpressionPostfixIncrement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Postfix Expression Postfix Decrement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Postfix Expression Postfix Decrement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePostfixExpressionPostfixDecrement(PostfixExpressionPostfixDecrement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Statement Declaration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Statement Declaration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseStatementDeclaration(StatementDeclaration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Statement Inner Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Statement Inner Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseStatementInnerStatement(StatementInnerStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Labeled Statement Case</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Labeled Statement Case</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLabeledStatementCase(LabeledStatementCase object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Labeled Statement Default</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Labeled Statement Default</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLabeledStatementDefault(LabeledStatementDefault object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Go To Prefix Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Go To Prefix Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseGoToPrefixStatement(GoToPrefixStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Selection Statement If</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Selection Statement If</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSelectionStatementIf(SelectionStatementIf object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Selection Statement Switch</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Selection Statement Switch</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSelectionStatementSwitch(SelectionStatementSwitch object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Iteration Statement While</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Iteration Statement While</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseIterationStatementWhile(IterationStatementWhile object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Iteration Statement Do</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Iteration Statement Do</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseIterationStatementDo(IterationStatementDo object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Iteration Statement For</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Iteration Statement For</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseIterationStatementFor(IterationStatementFor object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Jump Statement Goto</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Jump Statement Goto</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseJumpStatementGoto(JumpStatementGoto object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Jump Statement Continue</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Jump Statement Continue</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseJumpStatementContinue(JumpStatementContinue object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Jump Statement Break</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Jump Statement Break</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseJumpStatementBreak(JumpStatementBreak object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Jump Statement Return</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Jump Statement Return</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseJumpStatementReturn(JumpStatementReturn object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch, but this is the last case anyway.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject)
   * @generated
   */
  @Override
  public T defaultCase(EObject object)
  {
    return null;
  }

} //ClangSwitch
