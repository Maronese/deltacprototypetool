package org.xtext.dop.clang.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.xtext.dop.clang.services.ClangGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
@SuppressWarnings("all")
public class InternalClangParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_INCLUDE", "RULE_L_OP", "RULE_MULT_OP", "RULE_G_OP", "RULE_STRING_LITERAL", "RULE_IDENTIFIER", "RULE_DOT", "RULE_TYPEDEF", "RULE_SEMICOLON", "RULE_COMMA", "RULE_RESTRICT", "RULE_LPAREN", "RULE_RPAREN", "RULE_LBRACKET", "RULE_RBRACKET", "RULE_ELLIPSIS", "RULE_ASSIGN", "RULE_STATIC_ASSERT", "RULE_ATOMIC", "RULE_VOID", "RULE_CUSTOM", "RULE_CHAR", "RULE_SHORT", "RULE_INT", "RULE_LONG", "RULE_FLOAT", "RULE_DOUBLE", "RULE_SIGNED", "RULE_UNSIGNED", "RULE_BOOL", "RULE_COMPLEX", "RULE_IMAGINARY", "RULE_LCBRACKET", "RULE_RCBRACKET", "RULE_STRUCT", "RULE_UNION", "RULE_ENUM", "RULE_EXTERN", "RULE_STATIC", "RULE_THREAD_LOCAL", "RULE_AUTO", "RULE_REGISTER", "RULE_CONST", "RULE_VOLATILE", "RULE_INLINE", "RULE_NORETURN", "RULE_ALIGNAS", "RULE_Q_OP", "RULE_COLON", "RULE_LOR_OP", "RULE_LAND_OP", "RULE_OR_OP", "RULE_XOR_OP", "RULE_AND_OP", "RULE_MUL_ASSIGN", "RULE_DIV_ASSIGN", "RULE_MOD_ASSIGN", "RULE_ADD_ASSIGN", "RULE_SUB_ASSIGN", "RULE_LEFT_ASSIGN", "RULE_RIGHT_ASSIGN", "RULE_AND_ASSIGN", "RULE_XOR_ASSIGN", "RULE_OR_ASSIGN", "RULE_EQ_OP", "RULE_NE_OP", "RULE_LE_OP", "RULE_GE_OP", "RULE_ADD_OP", "RULE_MINUS_OP", "RULE_DIV_OP", "RULE_MOD_OP", "RULE_LEFT_OP", "RULE_RIGHT_OP", "RULE_INC_OP", "RULE_DEC_OP", "RULE_SIZEOF", "RULE_ALIGNOF", "RULE_I_CONSTANT", "RULE_F_CONSTANT", "RULE_GENERIC", "RULE_FUNC_NAME", "RULE_DEFAULT", "RULE_PTR_OP", "RULE_CASE", "RULE_IF", "RULE_ELSE", "RULE_SWITCH", "RULE_WHILE", "RULE_DO", "RULE_FOR", "RULE_GOTO", "RULE_CONTINUE", "RULE_BREAK", "RULE_RETURN", "RULE_TILDE", "RULE_NOT_OP", "RULE_O", "RULE_D", "RULE_NZ", "RULE_L", "RULE_A", "RULE_H", "RULE_HP", "RULE_E", "RULE_P", "RULE_FS", "RULE_IS", "RULE_CP", "RULE_SP", "RULE_ES", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_DQUOTE"
    };
    public static final int RULE_A=105;
    public static final int RULE_STRUCT=38;
    public static final int RULE_DEC_OP=79;
    public static final int RULE_ATOMIC=22;
    public static final int RULE_REGISTER=45;
    public static final int RULE_G_OP=7;
    public static final int RULE_WHILE=92;
    public static final int RULE_NOT_OP=100;
    public static final int RULE_FUNC_NAME=85;
    public static final int RULE_RETURN=98;
    public static final int RULE_LAND_OP=54;
    public static final int RULE_INLINE=48;
    public static final int RULE_SUB_ASSIGN=62;
    public static final int RULE_ASSIGN=20;
    public static final int RULE_CUSTOM=24;
    public static final int RULE_COLON=52;
    public static final int RULE_L_OP=5;
    public static final int RULE_TILDE=99;
    public static final int RULE_HP=107;
    public static final int RULE_AUTO=44;
    public static final int RULE_INT=27;
    public static final int RULE_ML_COMMENT=115;
    public static final int RULE_RESTRICT=14;
    public static final int RULE_LBRACKET=17;
    public static final int RULE_MUL_ASSIGN=58;
    public static final int RULE_IMAGINARY=35;
    public static final int RULE_ENUM=40;
    public static final int RULE_MINUS_OP=73;
    public static final int RULE_MULT_OP=6;
    public static final int RULE_I_CONSTANT=82;
    public static final int RULE_COMPLEX=34;
    public static final int RULE_IS=111;
    public static final int RULE_CONST=46;
    public static final int RULE_VOLATILE=47;
    public static final int RULE_DEFAULT=86;
    public static final int RULE_IF=89;
    public static final int RULE_ELLIPSIS=19;
    public static final int RULE_DOT=10;
    public static final int RULE_DIV_OP=74;
    public static final int RULE_AND_OP=57;
    public static final int RULE_SIGNED=31;
    public static final int RULE_LEFT_ASSIGN=63;
    public static final int RULE_STATIC_ASSERT=21;
    public static final int RULE_UNSIGNED=32;
    public static final int RULE_ALIGNAS=50;
    public static final int RULE_EQ_OP=68;
    public static final int RULE_STATIC=42;
    public static final int RULE_LEFT_OP=76;
    public static final int RULE_CONTINUE=96;
    public static final int RULE_BOOL=33;
    public static final int RULE_AND_ASSIGN=65;
    public static final int RULE_O=101;
    public static final int RULE_SWITCH=91;
    public static final int RULE_P=109;
    public static final int RULE_L=104;
    public static final int RULE_OR_OP=55;
    public static final int RULE_FLOAT=29;
    public static final int RULE_F_CONSTANT=83;
    public static final int RULE_H=106;
    public static final int RULE_E=108;
    public static final int RULE_INCLUDE=4;
    public static final int RULE_D=102;
    public static final int RULE_THREAD_LOCAL=43;
    public static final int RULE_NORETURN=49;
    public static final int RULE_DQUOTE=118;
    public static final int RULE_CP=112;
    public static final int RULE_LPAREN=15;
    public static final int RULE_EXTERN=41;
    public static final int RULE_GE_OP=71;
    public static final int RULE_CHAR=25;
    public static final int RULE_SP=113;
    public static final int RULE_COMMA=13;
    public static final int RULE_MOD_ASSIGN=60;
    public static final int RULE_SHORT=26;
    public static final int RULE_RCBRACKET=37;
    public static final int RULE_CASE=88;
    public static final int RULE_XOR_ASSIGN=66;
    public static final int RULE_DO=93;
    public static final int RULE_INC_OP=78;
    public static final int RULE_UNION=39;
    public static final int RULE_SEMICOLON=12;
    public static final int RULE_ELSE=90;
    public static final int RULE_TYPEDEF=11;
    public static final int RULE_LCBRACKET=36;
    public static final int RULE_IDENTIFIER=9;
    public static final int RULE_XOR_OP=56;
    public static final int RULE_OR_ASSIGN=67;
    public static final int RULE_NE_OP=69;
    public static final int RULE_ALIGNOF=81;
    public static final int RULE_PTR_OP=87;
    public static final int RULE_ES=114;
    public static final int RULE_STRING_LITERAL=8;
    public static final int RULE_SL_COMMENT=116;
    public static final int RULE_LOR_OP=53;
    public static final int RULE_DOUBLE=30;
    public static final int RULE_BREAK=97;
    public static final int RULE_ADD_ASSIGN=61;
    public static final int RULE_Q_OP=51;
    public static final int RULE_FOR=94;
    public static final int EOF=-1;
    public static final int RULE_VOID=23;
    public static final int RULE_ADD_OP=72;
    public static final int RULE_SIZEOF=80;
    public static final int RULE_GENERIC=84;
    public static final int RULE_GOTO=95;
    public static final int RULE_WS=117;
    public static final int RULE_RIGHT_ASSIGN=64;
    public static final int RULE_FS=110;
    public static final int RULE_DIV_ASSIGN=59;
    public static final int RULE_RIGHT_OP=77;
    public static final int RULE_RPAREN=16;
    public static final int RULE_MOD_OP=75;
    public static final int RULE_NZ=103;
    public static final int RULE_LE_OP=70;
    public static final int RULE_LONG=28;
    public static final int RULE_RBRACKET=18;

    // delegates
    // delegators


        public InternalClangParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalClangParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalClangParser.tokenNames; }
    public String getGrammarFileName() { return "../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g"; }



     	private ClangGrammarAccess grammarAccess;
     	
        public InternalClangParser(TokenStream input, ClangGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "Model";	
       	}
       	
       	@Override
       	protected ClangGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleModel"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:67:1: entryRuleModel returns [EObject current=null] : iv_ruleModel= ruleModel EOF ;
    public final EObject entryRuleModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleModel = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:68:2: (iv_ruleModel= ruleModel EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:69:2: iv_ruleModel= ruleModel EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getModelRule()); 
            }
            pushFollow(FOLLOW_ruleModel_in_entryRuleModel75);
            iv_ruleModel=ruleModel();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleModel; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleModel85); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleModel"


    // $ANTLR start "ruleModel"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:76:1: ruleModel returns [EObject current=null] : ( ( (lv_includesStd_0_0= ruleStandardInclude ) )* ( (lv_includesFile_1_0= ruleFileInclude ) )* ( (lv_declarations_2_0= ruleDeclaration ) )* ) ;
    public final EObject ruleModel() throws RecognitionException {
        EObject current = null;

        EObject lv_includesStd_0_0 = null;

        EObject lv_includesFile_1_0 = null;

        EObject lv_declarations_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:79:28: ( ( ( (lv_includesStd_0_0= ruleStandardInclude ) )* ( (lv_includesFile_1_0= ruleFileInclude ) )* ( (lv_declarations_2_0= ruleDeclaration ) )* ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:80:1: ( ( (lv_includesStd_0_0= ruleStandardInclude ) )* ( (lv_includesFile_1_0= ruleFileInclude ) )* ( (lv_declarations_2_0= ruleDeclaration ) )* )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:80:1: ( ( (lv_includesStd_0_0= ruleStandardInclude ) )* ( (lv_includesFile_1_0= ruleFileInclude ) )* ( (lv_declarations_2_0= ruleDeclaration ) )* )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:80:2: ( (lv_includesStd_0_0= ruleStandardInclude ) )* ( (lv_includesFile_1_0= ruleFileInclude ) )* ( (lv_declarations_2_0= ruleDeclaration ) )*
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:80:2: ( (lv_includesStd_0_0= ruleStandardInclude ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==RULE_INCLUDE) ) {
                    int LA1_1 = input.LA(2);

                    if ( (LA1_1==RULE_L_OP) ) {
                        alt1=1;
                    }


                }


                switch (alt1) {
            	case 1 :
            	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:81:1: (lv_includesStd_0_0= ruleStandardInclude )
            	    {
            	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:81:1: (lv_includesStd_0_0= ruleStandardInclude )
            	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:82:3: lv_includesStd_0_0= ruleStandardInclude
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getModelAccess().getIncludesStdStandardIncludeParserRuleCall_0_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleStandardInclude_in_ruleModel131);
            	    lv_includesStd_0_0=ruleStandardInclude();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getModelRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"includesStd",
            	              		lv_includesStd_0_0, 
            	              		"StandardInclude");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:98:3: ( (lv_includesFile_1_0= ruleFileInclude ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==RULE_INCLUDE) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:99:1: (lv_includesFile_1_0= ruleFileInclude )
            	    {
            	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:99:1: (lv_includesFile_1_0= ruleFileInclude )
            	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:100:3: lv_includesFile_1_0= ruleFileInclude
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getModelAccess().getIncludesFileFileIncludeParserRuleCall_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleFileInclude_in_ruleModel153);
            	    lv_includesFile_1_0=ruleFileInclude();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getModelRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"includesFile",
            	              		lv_includesFile_1_0, 
            	              		"FileInclude");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:116:3: ( (lv_declarations_2_0= ruleDeclaration ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==RULE_TYPEDEF||(LA3_0>=RULE_STATIC_ASSERT && LA3_0<=RULE_IMAGINARY)||(LA3_0>=RULE_STRUCT && LA3_0<=RULE_ALIGNAS)) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:117:1: (lv_declarations_2_0= ruleDeclaration )
            	    {
            	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:117:1: (lv_declarations_2_0= ruleDeclaration )
            	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:118:3: lv_declarations_2_0= ruleDeclaration
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getModelAccess().getDeclarationsDeclarationParserRuleCall_2_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleDeclaration_in_ruleModel175);
            	    lv_declarations_2_0=ruleDeclaration();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getModelRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"declarations",
            	              		lv_declarations_2_0, 
            	              		"Declaration");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleModel"


    // $ANTLR start "entryRuleStandardInclude"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:142:1: entryRuleStandardInclude returns [EObject current=null] : iv_ruleStandardInclude= ruleStandardInclude EOF ;
    public final EObject entryRuleStandardInclude() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStandardInclude = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:143:2: (iv_ruleStandardInclude= ruleStandardInclude EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:144:2: iv_ruleStandardInclude= ruleStandardInclude EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStandardIncludeRule()); 
            }
            pushFollow(FOLLOW_ruleStandardInclude_in_entryRuleStandardInclude212);
            iv_ruleStandardInclude=ruleStandardInclude();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStandardInclude; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleStandardInclude222); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStandardInclude"


    // $ANTLR start "ruleStandardInclude"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:151:1: ruleStandardInclude returns [EObject current=null] : ( () this_INCLUDE_1= RULE_INCLUDE this_L_OP_2= RULE_L_OP ( ( (lv_std_3_0= ruleSource ) ) | ( (lv_wild_4_0= RULE_MULT_OP ) ) ) this_G_OP_5= RULE_G_OP ) ;
    public final EObject ruleStandardInclude() throws RecognitionException {
        EObject current = null;

        Token this_INCLUDE_1=null;
        Token this_L_OP_2=null;
        Token lv_wild_4_0=null;
        Token this_G_OP_5=null;
        EObject lv_std_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:154:28: ( ( () this_INCLUDE_1= RULE_INCLUDE this_L_OP_2= RULE_L_OP ( ( (lv_std_3_0= ruleSource ) ) | ( (lv_wild_4_0= RULE_MULT_OP ) ) ) this_G_OP_5= RULE_G_OP ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:155:1: ( () this_INCLUDE_1= RULE_INCLUDE this_L_OP_2= RULE_L_OP ( ( (lv_std_3_0= ruleSource ) ) | ( (lv_wild_4_0= RULE_MULT_OP ) ) ) this_G_OP_5= RULE_G_OP )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:155:1: ( () this_INCLUDE_1= RULE_INCLUDE this_L_OP_2= RULE_L_OP ( ( (lv_std_3_0= ruleSource ) ) | ( (lv_wild_4_0= RULE_MULT_OP ) ) ) this_G_OP_5= RULE_G_OP )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:155:2: () this_INCLUDE_1= RULE_INCLUDE this_L_OP_2= RULE_L_OP ( ( (lv_std_3_0= ruleSource ) ) | ( (lv_wild_4_0= RULE_MULT_OP ) ) ) this_G_OP_5= RULE_G_OP
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:155:2: ()
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:156:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getStandardIncludeAccess().getStandardIncludeAction_0(),
                          current);
                  
            }

            }

            this_INCLUDE_1=(Token)match(input,RULE_INCLUDE,FOLLOW_RULE_INCLUDE_in_ruleStandardInclude267); if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                  newLeafNode(this_INCLUDE_1, grammarAccess.getStandardIncludeAccess().getINCLUDETerminalRuleCall_1()); 
                  
            }
            this_L_OP_2=(Token)match(input,RULE_L_OP,FOLLOW_RULE_L_OP_in_ruleStandardInclude277); if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                  newLeafNode(this_L_OP_2, grammarAccess.getStandardIncludeAccess().getL_OPTerminalRuleCall_2()); 
                  
            }
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:169:1: ( ( (lv_std_3_0= ruleSource ) ) | ( (lv_wild_4_0= RULE_MULT_OP ) ) )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==RULE_IDENTIFIER) ) {
                alt4=1;
            }
            else if ( (LA4_0==RULE_MULT_OP) ) {
                alt4=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:169:2: ( (lv_std_3_0= ruleSource ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:169:2: ( (lv_std_3_0= ruleSource ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:170:1: (lv_std_3_0= ruleSource )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:170:1: (lv_std_3_0= ruleSource )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:171:3: lv_std_3_0= ruleSource
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getStandardIncludeAccess().getStdSourceParserRuleCall_3_0_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleSource_in_ruleStandardInclude298);
                    lv_std_3_0=ruleSource();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getStandardIncludeRule());
                      	        }
                             		set(
                             			current, 
                             			"std",
                              		lv_std_3_0, 
                              		"Source");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:188:6: ( (lv_wild_4_0= RULE_MULT_OP ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:188:6: ( (lv_wild_4_0= RULE_MULT_OP ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:189:1: (lv_wild_4_0= RULE_MULT_OP )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:189:1: (lv_wild_4_0= RULE_MULT_OP )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:190:3: lv_wild_4_0= RULE_MULT_OP
                    {
                    lv_wild_4_0=(Token)match(input,RULE_MULT_OP,FOLLOW_RULE_MULT_OP_in_ruleStandardInclude321); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_wild_4_0, grammarAccess.getStandardIncludeAccess().getWildMULT_OPTerminalRuleCall_3_1_0()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getStandardIncludeRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"wild",
                              		true, 
                              		"MULT_OP");
                      	    
                    }

                    }


                    }


                    }
                    break;

            }

            this_G_OP_5=(Token)match(input,RULE_G_OP,FOLLOW_RULE_G_OP_in_ruleStandardInclude338); if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                  newLeafNode(this_G_OP_5, grammarAccess.getStandardIncludeAccess().getG_OPTerminalRuleCall_4()); 
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStandardInclude"


    // $ANTLR start "entryRuleFileInclude"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:218:1: entryRuleFileInclude returns [EObject current=null] : iv_ruleFileInclude= ruleFileInclude EOF ;
    public final EObject entryRuleFileInclude() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFileInclude = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:219:2: (iv_ruleFileInclude= ruleFileInclude EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:220:2: iv_ruleFileInclude= ruleFileInclude EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFileIncludeRule()); 
            }
            pushFollow(FOLLOW_ruleFileInclude_in_entryRuleFileInclude373);
            iv_ruleFileInclude=ruleFileInclude();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFileInclude; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleFileInclude383); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFileInclude"


    // $ANTLR start "ruleFileInclude"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:227:1: ruleFileInclude returns [EObject current=null] : (this_INCLUDE_0= RULE_INCLUDE ( (lv_file_1_0= RULE_STRING_LITERAL ) ) ) ;
    public final EObject ruleFileInclude() throws RecognitionException {
        EObject current = null;

        Token this_INCLUDE_0=null;
        Token lv_file_1_0=null;

         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:230:28: ( (this_INCLUDE_0= RULE_INCLUDE ( (lv_file_1_0= RULE_STRING_LITERAL ) ) ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:231:1: (this_INCLUDE_0= RULE_INCLUDE ( (lv_file_1_0= RULE_STRING_LITERAL ) ) )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:231:1: (this_INCLUDE_0= RULE_INCLUDE ( (lv_file_1_0= RULE_STRING_LITERAL ) ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:231:2: this_INCLUDE_0= RULE_INCLUDE ( (lv_file_1_0= RULE_STRING_LITERAL ) )
            {
            this_INCLUDE_0=(Token)match(input,RULE_INCLUDE,FOLLOW_RULE_INCLUDE_in_ruleFileInclude419); if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                  newLeafNode(this_INCLUDE_0, grammarAccess.getFileIncludeAccess().getINCLUDETerminalRuleCall_0()); 
                  
            }
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:235:1: ( (lv_file_1_0= RULE_STRING_LITERAL ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:236:1: (lv_file_1_0= RULE_STRING_LITERAL )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:236:1: (lv_file_1_0= RULE_STRING_LITERAL )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:237:3: lv_file_1_0= RULE_STRING_LITERAL
            {
            lv_file_1_0=(Token)match(input,RULE_STRING_LITERAL,FOLLOW_RULE_STRING_LITERAL_in_ruleFileInclude435); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_file_1_0, grammarAccess.getFileIncludeAccess().getFileSTRING_LITERALTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getFileIncludeRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"file",
                      		lv_file_1_0, 
                      		"STRING_LITERAL");
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFileInclude"


    // $ANTLR start "entryRuleSource"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:261:1: entryRuleSource returns [EObject current=null] : iv_ruleSource= ruleSource EOF ;
    public final EObject entryRuleSource() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSource = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:262:2: (iv_ruleSource= ruleSource EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:263:2: iv_ruleSource= ruleSource EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSourceRule()); 
            }
            pushFollow(FOLLOW_ruleSource_in_entryRuleSource476);
            iv_ruleSource=ruleSource();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSource; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleSource486); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSource"


    // $ANTLR start "ruleSource"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:270:1: ruleSource returns [EObject current=null] : ( ( () ( (lv_name_1_0= RULE_IDENTIFIER ) ) this_DOT_2= RULE_DOT ( (lv_ext_3_0= RULE_IDENTIFIER ) ) ) | ( () ( (lv_name_5_0= RULE_IDENTIFIER ) ) ) ) ;
    public final EObject ruleSource() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        Token this_DOT_2=null;
        Token lv_ext_3_0=null;
        Token lv_name_5_0=null;

         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:273:28: ( ( ( () ( (lv_name_1_0= RULE_IDENTIFIER ) ) this_DOT_2= RULE_DOT ( (lv_ext_3_0= RULE_IDENTIFIER ) ) ) | ( () ( (lv_name_5_0= RULE_IDENTIFIER ) ) ) ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:274:1: ( ( () ( (lv_name_1_0= RULE_IDENTIFIER ) ) this_DOT_2= RULE_DOT ( (lv_ext_3_0= RULE_IDENTIFIER ) ) ) | ( () ( (lv_name_5_0= RULE_IDENTIFIER ) ) ) )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:274:1: ( ( () ( (lv_name_1_0= RULE_IDENTIFIER ) ) this_DOT_2= RULE_DOT ( (lv_ext_3_0= RULE_IDENTIFIER ) ) ) | ( () ( (lv_name_5_0= RULE_IDENTIFIER ) ) ) )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==RULE_IDENTIFIER) ) {
                int LA5_1 = input.LA(2);

                if ( (LA5_1==RULE_DOT) ) {
                    alt5=1;
                }
                else if ( (LA5_1==EOF||LA5_1==RULE_G_OP) ) {
                    alt5=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 5, 1, input);

                    throw nvae;
                }
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:274:2: ( () ( (lv_name_1_0= RULE_IDENTIFIER ) ) this_DOT_2= RULE_DOT ( (lv_ext_3_0= RULE_IDENTIFIER ) ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:274:2: ( () ( (lv_name_1_0= RULE_IDENTIFIER ) ) this_DOT_2= RULE_DOT ( (lv_ext_3_0= RULE_IDENTIFIER ) ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:274:3: () ( (lv_name_1_0= RULE_IDENTIFIER ) ) this_DOT_2= RULE_DOT ( (lv_ext_3_0= RULE_IDENTIFIER ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:274:3: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:275:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getSourceAccess().getCompleteNameAction_0_0(),
                                  current);
                          
                    }

                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:280:2: ( (lv_name_1_0= RULE_IDENTIFIER ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:281:1: (lv_name_1_0= RULE_IDENTIFIER )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:281:1: (lv_name_1_0= RULE_IDENTIFIER )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:282:3: lv_name_1_0= RULE_IDENTIFIER
                    {
                    lv_name_1_0=(Token)match(input,RULE_IDENTIFIER,FOLLOW_RULE_IDENTIFIER_in_ruleSource538); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_name_1_0, grammarAccess.getSourceAccess().getNameIDENTIFIERTerminalRuleCall_0_1_0()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getSourceRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"name",
                              		lv_name_1_0, 
                              		"IDENTIFIER");
                      	    
                    }

                    }


                    }

                    this_DOT_2=(Token)match(input,RULE_DOT,FOLLOW_RULE_DOT_in_ruleSource554); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_DOT_2, grammarAccess.getSourceAccess().getDOTTerminalRuleCall_0_2()); 
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:302:1: ( (lv_ext_3_0= RULE_IDENTIFIER ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:303:1: (lv_ext_3_0= RULE_IDENTIFIER )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:303:1: (lv_ext_3_0= RULE_IDENTIFIER )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:304:3: lv_ext_3_0= RULE_IDENTIFIER
                    {
                    lv_ext_3_0=(Token)match(input,RULE_IDENTIFIER,FOLLOW_RULE_IDENTIFIER_in_ruleSource570); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_ext_3_0, grammarAccess.getSourceAccess().getExtIDENTIFIERTerminalRuleCall_0_3_0()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getSourceRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"ext",
                              		lv_ext_3_0, 
                              		"IDENTIFIER");
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:321:6: ( () ( (lv_name_5_0= RULE_IDENTIFIER ) ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:321:6: ( () ( (lv_name_5_0= RULE_IDENTIFIER ) ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:321:7: () ( (lv_name_5_0= RULE_IDENTIFIER ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:321:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:322:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getSourceAccess().getSimpleNameAction_1_0(),
                                  current);
                          
                    }

                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:327:2: ( (lv_name_5_0= RULE_IDENTIFIER ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:328:1: (lv_name_5_0= RULE_IDENTIFIER )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:328:1: (lv_name_5_0= RULE_IDENTIFIER )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:329:3: lv_name_5_0= RULE_IDENTIFIER
                    {
                    lv_name_5_0=(Token)match(input,RULE_IDENTIFIER,FOLLOW_RULE_IDENTIFIER_in_ruleSource609); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_name_5_0, grammarAccess.getSourceAccess().getNameIDENTIFIERTerminalRuleCall_1_1_0()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getSourceRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"name",
                              		lv_name_5_0, 
                              		"IDENTIFIER");
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSource"


    // $ANTLR start "entryRuleDeclaration"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:353:1: entryRuleDeclaration returns [EObject current=null] : iv_ruleDeclaration= ruleDeclaration EOF ;
    public final EObject entryRuleDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDeclaration = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:354:2: (iv_ruleDeclaration= ruleDeclaration EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:355:2: iv_ruleDeclaration= ruleDeclaration EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getDeclarationRule()); 
            }
            pushFollow(FOLLOW_ruleDeclaration_in_entryRuleDeclaration651);
            iv_ruleDeclaration=ruleDeclaration();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleDeclaration; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleDeclaration661); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDeclaration"


    // $ANTLR start "ruleDeclaration"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:362:1: ruleDeclaration returns [EObject current=null] : (this_GenericSpecifiedDeclaration_0= ruleGenericSpecifiedDeclaration | this_TypeAliasDeclaration_1= ruleTypeAliasDeclaration | this_StaticAssertDeclaration_2= ruleStaticAssertDeclaration ) ;
    public final EObject ruleDeclaration() throws RecognitionException {
        EObject current = null;

        EObject this_GenericSpecifiedDeclaration_0 = null;

        EObject this_TypeAliasDeclaration_1 = null;

        EObject this_StaticAssertDeclaration_2 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:365:28: ( (this_GenericSpecifiedDeclaration_0= ruleGenericSpecifiedDeclaration | this_TypeAliasDeclaration_1= ruleTypeAliasDeclaration | this_StaticAssertDeclaration_2= ruleStaticAssertDeclaration ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:366:1: (this_GenericSpecifiedDeclaration_0= ruleGenericSpecifiedDeclaration | this_TypeAliasDeclaration_1= ruleTypeAliasDeclaration | this_StaticAssertDeclaration_2= ruleStaticAssertDeclaration )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:366:1: (this_GenericSpecifiedDeclaration_0= ruleGenericSpecifiedDeclaration | this_TypeAliasDeclaration_1= ruleTypeAliasDeclaration | this_StaticAssertDeclaration_2= ruleStaticAssertDeclaration )
            int alt6=3;
            switch ( input.LA(1) ) {
            case RULE_ATOMIC:
            case RULE_VOID:
            case RULE_CUSTOM:
            case RULE_CHAR:
            case RULE_SHORT:
            case RULE_INT:
            case RULE_LONG:
            case RULE_FLOAT:
            case RULE_DOUBLE:
            case RULE_SIGNED:
            case RULE_UNSIGNED:
            case RULE_BOOL:
            case RULE_COMPLEX:
            case RULE_IMAGINARY:
            case RULE_STRUCT:
            case RULE_UNION:
            case RULE_ENUM:
            case RULE_EXTERN:
            case RULE_STATIC:
            case RULE_THREAD_LOCAL:
            case RULE_AUTO:
            case RULE_REGISTER:
            case RULE_CONST:
            case RULE_VOLATILE:
            case RULE_INLINE:
            case RULE_NORETURN:
            case RULE_ALIGNAS:
                {
                alt6=1;
                }
                break;
            case RULE_TYPEDEF:
                {
                alt6=2;
                }
                break;
            case RULE_STATIC_ASSERT:
                {
                alt6=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }

            switch (alt6) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:367:5: this_GenericSpecifiedDeclaration_0= ruleGenericSpecifiedDeclaration
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getDeclarationAccess().getGenericSpecifiedDeclarationParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleGenericSpecifiedDeclaration_in_ruleDeclaration708);
                    this_GenericSpecifiedDeclaration_0=ruleGenericSpecifiedDeclaration();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_GenericSpecifiedDeclaration_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:377:5: this_TypeAliasDeclaration_1= ruleTypeAliasDeclaration
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getDeclarationAccess().getTypeAliasDeclarationParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleTypeAliasDeclaration_in_ruleDeclaration735);
                    this_TypeAliasDeclaration_1=ruleTypeAliasDeclaration();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_TypeAliasDeclaration_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:387:5: this_StaticAssertDeclaration_2= ruleStaticAssertDeclaration
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getDeclarationAccess().getStaticAssertDeclarationParserRuleCall_2()); 
                          
                    }
                    pushFollow(FOLLOW_ruleStaticAssertDeclaration_in_ruleDeclaration762);
                    this_StaticAssertDeclaration_2=ruleStaticAssertDeclaration();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_StaticAssertDeclaration_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDeclaration"


    // $ANTLR start "entryRuleTypeAliasDeclaration"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:403:1: entryRuleTypeAliasDeclaration returns [EObject current=null] : iv_ruleTypeAliasDeclaration= ruleTypeAliasDeclaration EOF ;
    public final EObject entryRuleTypeAliasDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTypeAliasDeclaration = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:404:2: (iv_ruleTypeAliasDeclaration= ruleTypeAliasDeclaration EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:405:2: iv_ruleTypeAliasDeclaration= ruleTypeAliasDeclaration EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTypeAliasDeclarationRule()); 
            }
            pushFollow(FOLLOW_ruleTypeAliasDeclaration_in_entryRuleTypeAliasDeclaration797);
            iv_ruleTypeAliasDeclaration=ruleTypeAliasDeclaration();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTypeAliasDeclaration; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleTypeAliasDeclaration807); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTypeAliasDeclaration"


    // $ANTLR start "ruleTypeAliasDeclaration"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:412:1: ruleTypeAliasDeclaration returns [EObject current=null] : (this_TYPEDEF_0= RULE_TYPEDEF ( (lv_tdecl_1_0= ruleSpecifiedType ) ) this_SEMICOLON_2= RULE_SEMICOLON ) ;
    public final EObject ruleTypeAliasDeclaration() throws RecognitionException {
        EObject current = null;

        Token this_TYPEDEF_0=null;
        Token this_SEMICOLON_2=null;
        EObject lv_tdecl_1_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:415:28: ( (this_TYPEDEF_0= RULE_TYPEDEF ( (lv_tdecl_1_0= ruleSpecifiedType ) ) this_SEMICOLON_2= RULE_SEMICOLON ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:416:1: (this_TYPEDEF_0= RULE_TYPEDEF ( (lv_tdecl_1_0= ruleSpecifiedType ) ) this_SEMICOLON_2= RULE_SEMICOLON )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:416:1: (this_TYPEDEF_0= RULE_TYPEDEF ( (lv_tdecl_1_0= ruleSpecifiedType ) ) this_SEMICOLON_2= RULE_SEMICOLON )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:416:2: this_TYPEDEF_0= RULE_TYPEDEF ( (lv_tdecl_1_0= ruleSpecifiedType ) ) this_SEMICOLON_2= RULE_SEMICOLON
            {
            this_TYPEDEF_0=(Token)match(input,RULE_TYPEDEF,FOLLOW_RULE_TYPEDEF_in_ruleTypeAliasDeclaration843); if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                  newLeafNode(this_TYPEDEF_0, grammarAccess.getTypeAliasDeclarationAccess().getTYPEDEFTerminalRuleCall_0()); 
                  
            }
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:420:1: ( (lv_tdecl_1_0= ruleSpecifiedType ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:421:1: (lv_tdecl_1_0= ruleSpecifiedType )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:421:1: (lv_tdecl_1_0= ruleSpecifiedType )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:422:3: lv_tdecl_1_0= ruleSpecifiedType
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getTypeAliasDeclarationAccess().getTdeclSpecifiedTypeParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleSpecifiedType_in_ruleTypeAliasDeclaration863);
            lv_tdecl_1_0=ruleSpecifiedType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getTypeAliasDeclarationRule());
              	        }
                     		set(
                     			current, 
                     			"tdecl",
                      		lv_tdecl_1_0, 
                      		"SpecifiedType");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            this_SEMICOLON_2=(Token)match(input,RULE_SEMICOLON,FOLLOW_RULE_SEMICOLON_in_ruleTypeAliasDeclaration874); if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                  newLeafNode(this_SEMICOLON_2, grammarAccess.getTypeAliasDeclarationAccess().getSEMICOLONTerminalRuleCall_2()); 
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTypeAliasDeclaration"


    // $ANTLR start "entryRuleGenericSpecifiedDeclaration"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:450:1: entryRuleGenericSpecifiedDeclaration returns [EObject current=null] : iv_ruleGenericSpecifiedDeclaration= ruleGenericSpecifiedDeclaration EOF ;
    public final EObject entryRuleGenericSpecifiedDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGenericSpecifiedDeclaration = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:451:2: (iv_ruleGenericSpecifiedDeclaration= ruleGenericSpecifiedDeclaration EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:452:2: iv_ruleGenericSpecifiedDeclaration= ruleGenericSpecifiedDeclaration EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getGenericSpecifiedDeclarationRule()); 
            }
            pushFollow(FOLLOW_ruleGenericSpecifiedDeclaration_in_entryRuleGenericSpecifiedDeclaration909);
            iv_ruleGenericSpecifiedDeclaration=ruleGenericSpecifiedDeclaration();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleGenericSpecifiedDeclaration; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleGenericSpecifiedDeclaration919); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGenericSpecifiedDeclaration"


    // $ANTLR start "ruleGenericSpecifiedDeclaration"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:459:1: ruleGenericSpecifiedDeclaration returns [EObject current=null] : ( ( (lv_specs_0_0= ruleDeclarationSpecifier ) )* ( (lv_type_1_0= ruleSimpleType ) )+ ( (lv_decl_2_0= ruleGenericDeclarationElements ) ) this_SEMICOLON_3= RULE_SEMICOLON ) ;
    public final EObject ruleGenericSpecifiedDeclaration() throws RecognitionException {
        EObject current = null;

        Token this_SEMICOLON_3=null;
        EObject lv_specs_0_0 = null;

        EObject lv_type_1_0 = null;

        EObject lv_decl_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:462:28: ( ( ( (lv_specs_0_0= ruleDeclarationSpecifier ) )* ( (lv_type_1_0= ruleSimpleType ) )+ ( (lv_decl_2_0= ruleGenericDeclarationElements ) ) this_SEMICOLON_3= RULE_SEMICOLON ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:463:1: ( ( (lv_specs_0_0= ruleDeclarationSpecifier ) )* ( (lv_type_1_0= ruleSimpleType ) )+ ( (lv_decl_2_0= ruleGenericDeclarationElements ) ) this_SEMICOLON_3= RULE_SEMICOLON )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:463:1: ( ( (lv_specs_0_0= ruleDeclarationSpecifier ) )* ( (lv_type_1_0= ruleSimpleType ) )+ ( (lv_decl_2_0= ruleGenericDeclarationElements ) ) this_SEMICOLON_3= RULE_SEMICOLON )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:463:2: ( (lv_specs_0_0= ruleDeclarationSpecifier ) )* ( (lv_type_1_0= ruleSimpleType ) )+ ( (lv_decl_2_0= ruleGenericDeclarationElements ) ) this_SEMICOLON_3= RULE_SEMICOLON
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:463:2: ( (lv_specs_0_0= ruleDeclarationSpecifier ) )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==RULE_ATOMIC) ) {
                    int LA7_2 = input.LA(2);

                    if ( ((LA7_2>=RULE_ATOMIC && LA7_2<=RULE_IMAGINARY)||(LA7_2>=RULE_STRUCT && LA7_2<=RULE_ALIGNAS)) ) {
                        alt7=1;
                    }


                }
                else if ( ((LA7_0>=RULE_EXTERN && LA7_0<=RULE_ALIGNAS)) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:464:1: (lv_specs_0_0= ruleDeclarationSpecifier )
            	    {
            	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:464:1: (lv_specs_0_0= ruleDeclarationSpecifier )
            	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:465:3: lv_specs_0_0= ruleDeclarationSpecifier
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getGenericSpecifiedDeclarationAccess().getSpecsDeclarationSpecifierParserRuleCall_0_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleDeclarationSpecifier_in_ruleGenericSpecifiedDeclaration965);
            	    lv_specs_0_0=ruleDeclarationSpecifier();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getGenericSpecifiedDeclarationRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"specs",
            	              		lv_specs_0_0, 
            	              		"DeclarationSpecifier");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:481:3: ( (lv_type_1_0= ruleSimpleType ) )+
            int cnt8=0;
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( ((LA8_0>=RULE_ATOMIC && LA8_0<=RULE_IMAGINARY)||(LA8_0>=RULE_STRUCT && LA8_0<=RULE_ENUM)) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:482:1: (lv_type_1_0= ruleSimpleType )
            	    {
            	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:482:1: (lv_type_1_0= ruleSimpleType )
            	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:483:3: lv_type_1_0= ruleSimpleType
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getGenericSpecifiedDeclarationAccess().getTypeSimpleTypeParserRuleCall_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleSimpleType_in_ruleGenericSpecifiedDeclaration987);
            	    lv_type_1_0=ruleSimpleType();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getGenericSpecifiedDeclarationRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"type",
            	              		lv_type_1_0, 
            	              		"SimpleType");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt8 >= 1 ) break loop8;
            	    if (state.backtracking>0) {state.failed=true; return current;}
                        EarlyExitException eee =
                            new EarlyExitException(8, input);
                        throw eee;
                }
                cnt8++;
            } while (true);

            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:499:3: ( (lv_decl_2_0= ruleGenericDeclarationElements ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:500:1: (lv_decl_2_0= ruleGenericDeclarationElements )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:500:1: (lv_decl_2_0= ruleGenericDeclarationElements )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:501:3: lv_decl_2_0= ruleGenericDeclarationElements
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getGenericSpecifiedDeclarationAccess().getDeclGenericDeclarationElementsParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleGenericDeclarationElements_in_ruleGenericSpecifiedDeclaration1009);
            lv_decl_2_0=ruleGenericDeclarationElements();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getGenericSpecifiedDeclarationRule());
              	        }
                     		set(
                     			current, 
                     			"decl",
                      		lv_decl_2_0, 
                      		"GenericDeclarationElements");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            this_SEMICOLON_3=(Token)match(input,RULE_SEMICOLON,FOLLOW_RULE_SEMICOLON_in_ruleGenericSpecifiedDeclaration1020); if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                  newLeafNode(this_SEMICOLON_3, grammarAccess.getGenericSpecifiedDeclarationAccess().getSEMICOLONTerminalRuleCall_3()); 
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGenericSpecifiedDeclaration"


    // $ANTLR start "entryRuleGenericDeclarationElements"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:529:1: entryRuleGenericDeclarationElements returns [EObject current=null] : iv_ruleGenericDeclarationElements= ruleGenericDeclarationElements EOF ;
    public final EObject entryRuleGenericDeclarationElements() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGenericDeclarationElements = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:530:2: (iv_ruleGenericDeclarationElements= ruleGenericDeclarationElements EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:531:2: iv_ruleGenericDeclarationElements= ruleGenericDeclarationElements EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getGenericDeclarationElementsRule()); 
            }
            pushFollow(FOLLOW_ruleGenericDeclarationElements_in_entryRuleGenericDeclarationElements1055);
            iv_ruleGenericDeclarationElements=ruleGenericDeclarationElements();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleGenericDeclarationElements; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleGenericDeclarationElements1065); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGenericDeclarationElements"


    // $ANTLR start "ruleGenericDeclarationElements"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:538:1: ruleGenericDeclarationElements returns [EObject current=null] : ( ( (lv_els_0_0= ruleGenericDeclarationElement ) ) (this_COMMA_1= RULE_COMMA ( (lv_next_2_0= ruleGenericDeclarationElements ) ) )? ) ;
    public final EObject ruleGenericDeclarationElements() throws RecognitionException {
        EObject current = null;

        Token this_COMMA_1=null;
        EObject lv_els_0_0 = null;

        EObject lv_next_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:541:28: ( ( ( (lv_els_0_0= ruleGenericDeclarationElement ) ) (this_COMMA_1= RULE_COMMA ( (lv_next_2_0= ruleGenericDeclarationElements ) ) )? ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:542:1: ( ( (lv_els_0_0= ruleGenericDeclarationElement ) ) (this_COMMA_1= RULE_COMMA ( (lv_next_2_0= ruleGenericDeclarationElements ) ) )? )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:542:1: ( ( (lv_els_0_0= ruleGenericDeclarationElement ) ) (this_COMMA_1= RULE_COMMA ( (lv_next_2_0= ruleGenericDeclarationElements ) ) )? )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:542:2: ( (lv_els_0_0= ruleGenericDeclarationElement ) ) (this_COMMA_1= RULE_COMMA ( (lv_next_2_0= ruleGenericDeclarationElements ) ) )?
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:542:2: ( (lv_els_0_0= ruleGenericDeclarationElement ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:543:1: (lv_els_0_0= ruleGenericDeclarationElement )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:543:1: (lv_els_0_0= ruleGenericDeclarationElement )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:544:3: lv_els_0_0= ruleGenericDeclarationElement
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getGenericDeclarationElementsAccess().getElsGenericDeclarationElementParserRuleCall_0_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleGenericDeclarationElement_in_ruleGenericDeclarationElements1111);
            lv_els_0_0=ruleGenericDeclarationElement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getGenericDeclarationElementsRule());
              	        }
                     		add(
                     			current, 
                     			"els",
                      		lv_els_0_0, 
                      		"GenericDeclarationElement");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:560:2: (this_COMMA_1= RULE_COMMA ( (lv_next_2_0= ruleGenericDeclarationElements ) ) )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==RULE_COMMA) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:560:3: this_COMMA_1= RULE_COMMA ( (lv_next_2_0= ruleGenericDeclarationElements ) )
                    {
                    this_COMMA_1=(Token)match(input,RULE_COMMA,FOLLOW_RULE_COMMA_in_ruleGenericDeclarationElements1123); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_COMMA_1, grammarAccess.getGenericDeclarationElementsAccess().getCOMMATerminalRuleCall_1_0()); 
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:564:1: ( (lv_next_2_0= ruleGenericDeclarationElements ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:565:1: (lv_next_2_0= ruleGenericDeclarationElements )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:565:1: (lv_next_2_0= ruleGenericDeclarationElements )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:566:3: lv_next_2_0= ruleGenericDeclarationElements
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getGenericDeclarationElementsAccess().getNextGenericDeclarationElementsParserRuleCall_1_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleGenericDeclarationElements_in_ruleGenericDeclarationElements1143);
                    lv_next_2_0=ruleGenericDeclarationElements();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getGenericDeclarationElementsRule());
                      	        }
                             		set(
                             			current, 
                             			"next",
                              		lv_next_2_0, 
                              		"GenericDeclarationElements");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGenericDeclarationElements"


    // $ANTLR start "entryRuleGenericDeclarationElement"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:590:1: entryRuleGenericDeclarationElement returns [EObject current=null] : iv_ruleGenericDeclarationElement= ruleGenericDeclarationElement EOF ;
    public final EObject entryRuleGenericDeclarationElement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGenericDeclarationElement = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:591:2: (iv_ruleGenericDeclarationElement= ruleGenericDeclarationElement EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:592:2: iv_ruleGenericDeclarationElement= ruleGenericDeclarationElement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getGenericDeclarationElementRule()); 
            }
            pushFollow(FOLLOW_ruleGenericDeclarationElement_in_entryRuleGenericDeclarationElement1181);
            iv_ruleGenericDeclarationElement=ruleGenericDeclarationElement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleGenericDeclarationElement; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleGenericDeclarationElement1191); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGenericDeclarationElement"


    // $ANTLR start "ruleGenericDeclarationElement"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:599:1: ruleGenericDeclarationElement returns [EObject current=null] : ( ( (lv_id_0_0= ruleGenericDeclarationId ) ) ( (lv_init_1_0= ruleGenericDeclarationInitializer ) )? ) ;
    public final EObject ruleGenericDeclarationElement() throws RecognitionException {
        EObject current = null;

        EObject lv_id_0_0 = null;

        EObject lv_init_1_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:602:28: ( ( ( (lv_id_0_0= ruleGenericDeclarationId ) ) ( (lv_init_1_0= ruleGenericDeclarationInitializer ) )? ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:603:1: ( ( (lv_id_0_0= ruleGenericDeclarationId ) ) ( (lv_init_1_0= ruleGenericDeclarationInitializer ) )? )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:603:1: ( ( (lv_id_0_0= ruleGenericDeclarationId ) ) ( (lv_init_1_0= ruleGenericDeclarationInitializer ) )? )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:603:2: ( (lv_id_0_0= ruleGenericDeclarationId ) ) ( (lv_init_1_0= ruleGenericDeclarationInitializer ) )?
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:603:2: ( (lv_id_0_0= ruleGenericDeclarationId ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:604:1: (lv_id_0_0= ruleGenericDeclarationId )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:604:1: (lv_id_0_0= ruleGenericDeclarationId )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:605:3: lv_id_0_0= ruleGenericDeclarationId
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getGenericDeclarationElementAccess().getIdGenericDeclarationIdParserRuleCall_0_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleGenericDeclarationId_in_ruleGenericDeclarationElement1237);
            lv_id_0_0=ruleGenericDeclarationId();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getGenericDeclarationElementRule());
              	        }
                     		set(
                     			current, 
                     			"id",
                      		lv_id_0_0, 
                      		"GenericDeclarationId");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:621:2: ( (lv_init_1_0= ruleGenericDeclarationInitializer ) )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==RULE_ASSIGN||LA10_0==RULE_LCBRACKET) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:622:1: (lv_init_1_0= ruleGenericDeclarationInitializer )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:622:1: (lv_init_1_0= ruleGenericDeclarationInitializer )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:623:3: lv_init_1_0= ruleGenericDeclarationInitializer
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getGenericDeclarationElementAccess().getInitGenericDeclarationInitializerParserRuleCall_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleGenericDeclarationInitializer_in_ruleGenericDeclarationElement1258);
                    lv_init_1_0=ruleGenericDeclarationInitializer();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getGenericDeclarationElementRule());
                      	        }
                             		set(
                             			current, 
                             			"init",
                              		lv_init_1_0, 
                              		"GenericDeclarationInitializer");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGenericDeclarationElement"


    // $ANTLR start "entryRuleGenericDeclarationId"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:647:1: entryRuleGenericDeclarationId returns [EObject current=null] : iv_ruleGenericDeclarationId= ruleGenericDeclarationId EOF ;
    public final EObject entryRuleGenericDeclarationId() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGenericDeclarationId = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:648:2: (iv_ruleGenericDeclarationId= ruleGenericDeclarationId EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:649:2: iv_ruleGenericDeclarationId= ruleGenericDeclarationId EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getGenericDeclarationIdRule()); 
            }
            pushFollow(FOLLOW_ruleGenericDeclarationId_in_entryRuleGenericDeclarationId1295);
            iv_ruleGenericDeclarationId=ruleGenericDeclarationId();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleGenericDeclarationId; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleGenericDeclarationId1305); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGenericDeclarationId"


    // $ANTLR start "ruleGenericDeclarationId"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:656:1: ruleGenericDeclarationId returns [EObject current=null] : ( ( () ( (lv_pointers_1_0= RULE_MULT_OP ) )+ ( (lv_restrict_2_0= RULE_RESTRICT ) )? ( (lv_inner_3_0= ruleGenericDeclarationInnerId ) )? ( (lv_posts_4_0= ruleGenericDeclarationIdPostfix ) )* ) | ( () ( (lv_inner_6_0= ruleGenericDeclarationInnerId ) ) ( (lv_posts_7_0= ruleGenericDeclarationIdPostfix ) )* ) | ( () ( (lv_posts_9_0= ruleGenericDeclarationIdPostfix ) )+ ( (lv_inner_10_0= ruleGenericDeclarationInnerId ) )? ) ) ;
    public final EObject ruleGenericDeclarationId() throws RecognitionException {
        EObject current = null;

        Token lv_pointers_1_0=null;
        Token lv_restrict_2_0=null;
        EObject lv_inner_3_0 = null;

        EObject lv_posts_4_0 = null;

        EObject lv_inner_6_0 = null;

        EObject lv_posts_7_0 = null;

        EObject lv_posts_9_0 = null;

        EObject lv_inner_10_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:659:28: ( ( ( () ( (lv_pointers_1_0= RULE_MULT_OP ) )+ ( (lv_restrict_2_0= RULE_RESTRICT ) )? ( (lv_inner_3_0= ruleGenericDeclarationInnerId ) )? ( (lv_posts_4_0= ruleGenericDeclarationIdPostfix ) )* ) | ( () ( (lv_inner_6_0= ruleGenericDeclarationInnerId ) ) ( (lv_posts_7_0= ruleGenericDeclarationIdPostfix ) )* ) | ( () ( (lv_posts_9_0= ruleGenericDeclarationIdPostfix ) )+ ( (lv_inner_10_0= ruleGenericDeclarationInnerId ) )? ) ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:660:1: ( ( () ( (lv_pointers_1_0= RULE_MULT_OP ) )+ ( (lv_restrict_2_0= RULE_RESTRICT ) )? ( (lv_inner_3_0= ruleGenericDeclarationInnerId ) )? ( (lv_posts_4_0= ruleGenericDeclarationIdPostfix ) )* ) | ( () ( (lv_inner_6_0= ruleGenericDeclarationInnerId ) ) ( (lv_posts_7_0= ruleGenericDeclarationIdPostfix ) )* ) | ( () ( (lv_posts_9_0= ruleGenericDeclarationIdPostfix ) )+ ( (lv_inner_10_0= ruleGenericDeclarationInnerId ) )? ) )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:660:1: ( ( () ( (lv_pointers_1_0= RULE_MULT_OP ) )+ ( (lv_restrict_2_0= RULE_RESTRICT ) )? ( (lv_inner_3_0= ruleGenericDeclarationInnerId ) )? ( (lv_posts_4_0= ruleGenericDeclarationIdPostfix ) )* ) | ( () ( (lv_inner_6_0= ruleGenericDeclarationInnerId ) ) ( (lv_posts_7_0= ruleGenericDeclarationIdPostfix ) )* ) | ( () ( (lv_posts_9_0= ruleGenericDeclarationIdPostfix ) )+ ( (lv_inner_10_0= ruleGenericDeclarationInnerId ) )? ) )
            int alt18=3;
            switch ( input.LA(1) ) {
            case RULE_MULT_OP:
                {
                alt18=1;
                }
                break;
            case RULE_IDENTIFIER:
                {
                alt18=2;
                }
                break;
            case RULE_LPAREN:
                {
                int LA18_3 = input.LA(2);

                if ( (LA18_3==RULE_MULT_OP||LA18_3==RULE_IDENTIFIER||LA18_3==RULE_LPAREN||LA18_3==RULE_LBRACKET) ) {
                    alt18=2;
                }
                else if ( (LA18_3==RULE_COMMA||LA18_3==RULE_RPAREN||(LA18_3>=RULE_ATOMIC && LA18_3<=RULE_IMAGINARY)||(LA18_3>=RULE_STRUCT && LA18_3<=RULE_ALIGNAS)) ) {
                    alt18=3;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 18, 3, input);

                    throw nvae;
                }
                }
                break;
            case RULE_LBRACKET:
                {
                alt18=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 18, 0, input);

                throw nvae;
            }

            switch (alt18) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:660:2: ( () ( (lv_pointers_1_0= RULE_MULT_OP ) )+ ( (lv_restrict_2_0= RULE_RESTRICT ) )? ( (lv_inner_3_0= ruleGenericDeclarationInnerId ) )? ( (lv_posts_4_0= ruleGenericDeclarationIdPostfix ) )* )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:660:2: ( () ( (lv_pointers_1_0= RULE_MULT_OP ) )+ ( (lv_restrict_2_0= RULE_RESTRICT ) )? ( (lv_inner_3_0= ruleGenericDeclarationInnerId ) )? ( (lv_posts_4_0= ruleGenericDeclarationIdPostfix ) )* )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:660:3: () ( (lv_pointers_1_0= RULE_MULT_OP ) )+ ( (lv_restrict_2_0= RULE_RESTRICT ) )? ( (lv_inner_3_0= ruleGenericDeclarationInnerId ) )? ( (lv_posts_4_0= ruleGenericDeclarationIdPostfix ) )*
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:660:3: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:661:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getGenericDeclarationIdAccess().getGenericDeclaration0Action_0_0(),
                                  current);
                          
                    }

                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:666:2: ( (lv_pointers_1_0= RULE_MULT_OP ) )+
                    int cnt11=0;
                    loop11:
                    do {
                        int alt11=2;
                        int LA11_0 = input.LA(1);

                        if ( (LA11_0==RULE_MULT_OP) ) {
                            alt11=1;
                        }


                        switch (alt11) {
                    	case 1 :
                    	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:667:1: (lv_pointers_1_0= RULE_MULT_OP )
                    	    {
                    	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:667:1: (lv_pointers_1_0= RULE_MULT_OP )
                    	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:668:3: lv_pointers_1_0= RULE_MULT_OP
                    	    {
                    	    lv_pointers_1_0=(Token)match(input,RULE_MULT_OP,FOLLOW_RULE_MULT_OP_in_ruleGenericDeclarationId1357); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      			newLeafNode(lv_pointers_1_0, grammarAccess.getGenericDeclarationIdAccess().getPointersMULT_OPTerminalRuleCall_0_1_0()); 
                    	      		
                    	    }
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElement(grammarAccess.getGenericDeclarationIdRule());
                    	      	        }
                    	             		addWithLastConsumed(
                    	             			current, 
                    	             			"pointers",
                    	              		lv_pointers_1_0, 
                    	              		"MULT_OP");
                    	      	    
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt11 >= 1 ) break loop11;
                    	    if (state.backtracking>0) {state.failed=true; return current;}
                                EarlyExitException eee =
                                    new EarlyExitException(11, input);
                                throw eee;
                        }
                        cnt11++;
                    } while (true);

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:684:3: ( (lv_restrict_2_0= RULE_RESTRICT ) )?
                    int alt12=2;
                    int LA12_0 = input.LA(1);

                    if ( (LA12_0==RULE_RESTRICT) ) {
                        alt12=1;
                    }
                    switch (alt12) {
                        case 1 :
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:685:1: (lv_restrict_2_0= RULE_RESTRICT )
                            {
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:685:1: (lv_restrict_2_0= RULE_RESTRICT )
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:686:3: lv_restrict_2_0= RULE_RESTRICT
                            {
                            lv_restrict_2_0=(Token)match(input,RULE_RESTRICT,FOLLOW_RULE_RESTRICT_in_ruleGenericDeclarationId1380); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              			newLeafNode(lv_restrict_2_0, grammarAccess.getGenericDeclarationIdAccess().getRestrictRESTRICTTerminalRuleCall_0_2_0()); 
                              		
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getGenericDeclarationIdRule());
                              	        }
                                     		setWithLastConsumed(
                                     			current, 
                                     			"restrict",
                                      		lv_restrict_2_0, 
                                      		"RESTRICT");
                              	    
                            }

                            }


                            }
                            break;

                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:702:3: ( (lv_inner_3_0= ruleGenericDeclarationInnerId ) )?
                    int alt13=2;
                    int LA13_0 = input.LA(1);

                    if ( (LA13_0==RULE_IDENTIFIER) ) {
                        alt13=1;
                    }
                    else if ( (LA13_0==RULE_LPAREN) ) {
                        int LA13_2 = input.LA(2);

                        if ( (LA13_2==RULE_MULT_OP||LA13_2==RULE_IDENTIFIER||LA13_2==RULE_LPAREN||LA13_2==RULE_LBRACKET) ) {
                            alt13=1;
                        }
                    }
                    switch (alt13) {
                        case 1 :
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:703:1: (lv_inner_3_0= ruleGenericDeclarationInnerId )
                            {
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:703:1: (lv_inner_3_0= ruleGenericDeclarationInnerId )
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:704:3: lv_inner_3_0= ruleGenericDeclarationInnerId
                            {
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getGenericDeclarationIdAccess().getInnerGenericDeclarationInnerIdParserRuleCall_0_3_0()); 
                              	    
                            }
                            pushFollow(FOLLOW_ruleGenericDeclarationInnerId_in_ruleGenericDeclarationId1407);
                            lv_inner_3_0=ruleGenericDeclarationInnerId();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElementForParent(grammarAccess.getGenericDeclarationIdRule());
                              	        }
                                     		set(
                                     			current, 
                                     			"inner",
                                      		lv_inner_3_0, 
                                      		"GenericDeclarationInnerId");
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }


                            }
                            break;

                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:720:3: ( (lv_posts_4_0= ruleGenericDeclarationIdPostfix ) )*
                    loop14:
                    do {
                        int alt14=2;
                        int LA14_0 = input.LA(1);

                        if ( (LA14_0==RULE_LPAREN||LA14_0==RULE_LBRACKET) ) {
                            alt14=1;
                        }


                        switch (alt14) {
                    	case 1 :
                    	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:721:1: (lv_posts_4_0= ruleGenericDeclarationIdPostfix )
                    	    {
                    	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:721:1: (lv_posts_4_0= ruleGenericDeclarationIdPostfix )
                    	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:722:3: lv_posts_4_0= ruleGenericDeclarationIdPostfix
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getGenericDeclarationIdAccess().getPostsGenericDeclarationIdPostfixParserRuleCall_0_4_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FOLLOW_ruleGenericDeclarationIdPostfix_in_ruleGenericDeclarationId1429);
                    	    lv_posts_4_0=ruleGenericDeclarationIdPostfix();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getGenericDeclarationIdRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"posts",
                    	              		lv_posts_4_0, 
                    	              		"GenericDeclarationIdPostfix");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop14;
                        }
                    } while (true);


                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:739:6: ( () ( (lv_inner_6_0= ruleGenericDeclarationInnerId ) ) ( (lv_posts_7_0= ruleGenericDeclarationIdPostfix ) )* )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:739:6: ( () ( (lv_inner_6_0= ruleGenericDeclarationInnerId ) ) ( (lv_posts_7_0= ruleGenericDeclarationIdPostfix ) )* )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:739:7: () ( (lv_inner_6_0= ruleGenericDeclarationInnerId ) ) ( (lv_posts_7_0= ruleGenericDeclarationIdPostfix ) )*
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:739:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:740:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getGenericDeclarationIdAccess().getGenericDeclaration1Action_1_0(),
                                  current);
                          
                    }

                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:745:2: ( (lv_inner_6_0= ruleGenericDeclarationInnerId ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:746:1: (lv_inner_6_0= ruleGenericDeclarationInnerId )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:746:1: (lv_inner_6_0= ruleGenericDeclarationInnerId )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:747:3: lv_inner_6_0= ruleGenericDeclarationInnerId
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getGenericDeclarationIdAccess().getInnerGenericDeclarationInnerIdParserRuleCall_1_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleGenericDeclarationInnerId_in_ruleGenericDeclarationId1468);
                    lv_inner_6_0=ruleGenericDeclarationInnerId();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getGenericDeclarationIdRule());
                      	        }
                             		set(
                             			current, 
                             			"inner",
                              		lv_inner_6_0, 
                              		"GenericDeclarationInnerId");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:763:2: ( (lv_posts_7_0= ruleGenericDeclarationIdPostfix ) )*
                    loop15:
                    do {
                        int alt15=2;
                        int LA15_0 = input.LA(1);

                        if ( (LA15_0==RULE_LPAREN||LA15_0==RULE_LBRACKET) ) {
                            alt15=1;
                        }


                        switch (alt15) {
                    	case 1 :
                    	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:764:1: (lv_posts_7_0= ruleGenericDeclarationIdPostfix )
                    	    {
                    	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:764:1: (lv_posts_7_0= ruleGenericDeclarationIdPostfix )
                    	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:765:3: lv_posts_7_0= ruleGenericDeclarationIdPostfix
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getGenericDeclarationIdAccess().getPostsGenericDeclarationIdPostfixParserRuleCall_1_2_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FOLLOW_ruleGenericDeclarationIdPostfix_in_ruleGenericDeclarationId1489);
                    	    lv_posts_7_0=ruleGenericDeclarationIdPostfix();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getGenericDeclarationIdRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"posts",
                    	              		lv_posts_7_0, 
                    	              		"GenericDeclarationIdPostfix");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop15;
                        }
                    } while (true);


                    }


                    }
                    break;
                case 3 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:782:6: ( () ( (lv_posts_9_0= ruleGenericDeclarationIdPostfix ) )+ ( (lv_inner_10_0= ruleGenericDeclarationInnerId ) )? )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:782:6: ( () ( (lv_posts_9_0= ruleGenericDeclarationIdPostfix ) )+ ( (lv_inner_10_0= ruleGenericDeclarationInnerId ) )? )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:782:7: () ( (lv_posts_9_0= ruleGenericDeclarationIdPostfix ) )+ ( (lv_inner_10_0= ruleGenericDeclarationInnerId ) )?
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:782:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:783:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getGenericDeclarationIdAccess().getGenericDeclaration2Action_2_0(),
                                  current);
                          
                    }

                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:788:2: ( (lv_posts_9_0= ruleGenericDeclarationIdPostfix ) )+
                    int cnt16=0;
                    loop16:
                    do {
                        int alt16=2;
                        int LA16_0 = input.LA(1);

                        if ( (LA16_0==RULE_LPAREN) ) {
                            int LA16_2 = input.LA(2);

                            if ( (LA16_2==RULE_COMMA||LA16_2==RULE_RPAREN||(LA16_2>=RULE_ATOMIC && LA16_2<=RULE_IMAGINARY)||(LA16_2>=RULE_STRUCT && LA16_2<=RULE_ALIGNAS)) ) {
                                alt16=1;
                            }


                        }
                        else if ( (LA16_0==RULE_LBRACKET) ) {
                            alt16=1;
                        }


                        switch (alt16) {
                    	case 1 :
                    	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:789:1: (lv_posts_9_0= ruleGenericDeclarationIdPostfix )
                    	    {
                    	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:789:1: (lv_posts_9_0= ruleGenericDeclarationIdPostfix )
                    	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:790:3: lv_posts_9_0= ruleGenericDeclarationIdPostfix
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getGenericDeclarationIdAccess().getPostsGenericDeclarationIdPostfixParserRuleCall_2_1_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FOLLOW_ruleGenericDeclarationIdPostfix_in_ruleGenericDeclarationId1528);
                    	    lv_posts_9_0=ruleGenericDeclarationIdPostfix();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getGenericDeclarationIdRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"posts",
                    	              		lv_posts_9_0, 
                    	              		"GenericDeclarationIdPostfix");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt16 >= 1 ) break loop16;
                    	    if (state.backtracking>0) {state.failed=true; return current;}
                                EarlyExitException eee =
                                    new EarlyExitException(16, input);
                                throw eee;
                        }
                        cnt16++;
                    } while (true);

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:806:3: ( (lv_inner_10_0= ruleGenericDeclarationInnerId ) )?
                    int alt17=2;
                    int LA17_0 = input.LA(1);

                    if ( (LA17_0==RULE_IDENTIFIER||LA17_0==RULE_LPAREN) ) {
                        alt17=1;
                    }
                    switch (alt17) {
                        case 1 :
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:807:1: (lv_inner_10_0= ruleGenericDeclarationInnerId )
                            {
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:807:1: (lv_inner_10_0= ruleGenericDeclarationInnerId )
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:808:3: lv_inner_10_0= ruleGenericDeclarationInnerId
                            {
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getGenericDeclarationIdAccess().getInnerGenericDeclarationInnerIdParserRuleCall_2_2_0()); 
                              	    
                            }
                            pushFollow(FOLLOW_ruleGenericDeclarationInnerId_in_ruleGenericDeclarationId1550);
                            lv_inner_10_0=ruleGenericDeclarationInnerId();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElementForParent(grammarAccess.getGenericDeclarationIdRule());
                              	        }
                                     		set(
                                     			current, 
                                     			"inner",
                                      		lv_inner_10_0, 
                                      		"GenericDeclarationInnerId");
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }


                            }
                            break;

                    }


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGenericDeclarationId"


    // $ANTLR start "entryRuleGenericDeclarationInnerId"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:832:1: entryRuleGenericDeclarationInnerId returns [EObject current=null] : iv_ruleGenericDeclarationInnerId= ruleGenericDeclarationInnerId EOF ;
    public final EObject entryRuleGenericDeclarationInnerId() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGenericDeclarationInnerId = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:833:2: (iv_ruleGenericDeclarationInnerId= ruleGenericDeclarationInnerId EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:834:2: iv_ruleGenericDeclarationInnerId= ruleGenericDeclarationInnerId EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getGenericDeclarationInnerIdRule()); 
            }
            pushFollow(FOLLOW_ruleGenericDeclarationInnerId_in_entryRuleGenericDeclarationInnerId1588);
            iv_ruleGenericDeclarationInnerId=ruleGenericDeclarationInnerId();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleGenericDeclarationInnerId; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleGenericDeclarationInnerId1598); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGenericDeclarationInnerId"


    // $ANTLR start "ruleGenericDeclarationInnerId"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:841:1: ruleGenericDeclarationInnerId returns [EObject current=null] : ( ( () ( (lv_name_1_0= RULE_IDENTIFIER ) ) ) | (this_LPAREN_2= RULE_LPAREN ( (lv_inner_3_0= ruleGenericDeclarationId ) ) this_RPAREN_4= RULE_RPAREN ) ) ;
    public final EObject ruleGenericDeclarationInnerId() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        Token this_LPAREN_2=null;
        Token this_RPAREN_4=null;
        EObject lv_inner_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:844:28: ( ( ( () ( (lv_name_1_0= RULE_IDENTIFIER ) ) ) | (this_LPAREN_2= RULE_LPAREN ( (lv_inner_3_0= ruleGenericDeclarationId ) ) this_RPAREN_4= RULE_RPAREN ) ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:845:1: ( ( () ( (lv_name_1_0= RULE_IDENTIFIER ) ) ) | (this_LPAREN_2= RULE_LPAREN ( (lv_inner_3_0= ruleGenericDeclarationId ) ) this_RPAREN_4= RULE_RPAREN ) )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:845:1: ( ( () ( (lv_name_1_0= RULE_IDENTIFIER ) ) ) | (this_LPAREN_2= RULE_LPAREN ( (lv_inner_3_0= ruleGenericDeclarationId ) ) this_RPAREN_4= RULE_RPAREN ) )
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==RULE_IDENTIFIER) ) {
                alt19=1;
            }
            else if ( (LA19_0==RULE_LPAREN) ) {
                alt19=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 19, 0, input);

                throw nvae;
            }
            switch (alt19) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:845:2: ( () ( (lv_name_1_0= RULE_IDENTIFIER ) ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:845:2: ( () ( (lv_name_1_0= RULE_IDENTIFIER ) ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:845:3: () ( (lv_name_1_0= RULE_IDENTIFIER ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:845:3: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:846:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getGenericDeclarationInnerIdAccess().getGlobalNameAction_0_0(),
                                  current);
                          
                    }

                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:851:2: ( (lv_name_1_0= RULE_IDENTIFIER ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:852:1: (lv_name_1_0= RULE_IDENTIFIER )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:852:1: (lv_name_1_0= RULE_IDENTIFIER )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:853:3: lv_name_1_0= RULE_IDENTIFIER
                    {
                    lv_name_1_0=(Token)match(input,RULE_IDENTIFIER,FOLLOW_RULE_IDENTIFIER_in_ruleGenericDeclarationInnerId1650); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_name_1_0, grammarAccess.getGenericDeclarationInnerIdAccess().getNameIDENTIFIERTerminalRuleCall_0_1_0()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getGenericDeclarationInnerIdRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"name",
                              		lv_name_1_0, 
                              		"IDENTIFIER");
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:870:6: (this_LPAREN_2= RULE_LPAREN ( (lv_inner_3_0= ruleGenericDeclarationId ) ) this_RPAREN_4= RULE_RPAREN )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:870:6: (this_LPAREN_2= RULE_LPAREN ( (lv_inner_3_0= ruleGenericDeclarationId ) ) this_RPAREN_4= RULE_RPAREN )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:870:7: this_LPAREN_2= RULE_LPAREN ( (lv_inner_3_0= ruleGenericDeclarationId ) ) this_RPAREN_4= RULE_RPAREN
                    {
                    this_LPAREN_2=(Token)match(input,RULE_LPAREN,FOLLOW_RULE_LPAREN_in_ruleGenericDeclarationInnerId1674); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_LPAREN_2, grammarAccess.getGenericDeclarationInnerIdAccess().getLPARENTerminalRuleCall_1_0()); 
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:874:1: ( (lv_inner_3_0= ruleGenericDeclarationId ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:875:1: (lv_inner_3_0= ruleGenericDeclarationId )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:875:1: (lv_inner_3_0= ruleGenericDeclarationId )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:876:3: lv_inner_3_0= ruleGenericDeclarationId
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getGenericDeclarationInnerIdAccess().getInnerGenericDeclarationIdParserRuleCall_1_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleGenericDeclarationId_in_ruleGenericDeclarationInnerId1694);
                    lv_inner_3_0=ruleGenericDeclarationId();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getGenericDeclarationInnerIdRule());
                      	        }
                             		set(
                             			current, 
                             			"inner",
                              		lv_inner_3_0, 
                              		"GenericDeclarationId");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    this_RPAREN_4=(Token)match(input,RULE_RPAREN,FOLLOW_RULE_RPAREN_in_ruleGenericDeclarationInnerId1705); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_RPAREN_4, grammarAccess.getGenericDeclarationInnerIdAccess().getRPARENTerminalRuleCall_1_2()); 
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGenericDeclarationInnerId"


    // $ANTLR start "entryRuleGenericDeclarationIdPostfix"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:904:1: entryRuleGenericDeclarationIdPostfix returns [EObject current=null] : iv_ruleGenericDeclarationIdPostfix= ruleGenericDeclarationIdPostfix EOF ;
    public final EObject entryRuleGenericDeclarationIdPostfix() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGenericDeclarationIdPostfix = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:905:2: (iv_ruleGenericDeclarationIdPostfix= ruleGenericDeclarationIdPostfix EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:906:2: iv_ruleGenericDeclarationIdPostfix= ruleGenericDeclarationIdPostfix EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getGenericDeclarationIdPostfixRule()); 
            }
            pushFollow(FOLLOW_ruleGenericDeclarationIdPostfix_in_entryRuleGenericDeclarationIdPostfix1741);
            iv_ruleGenericDeclarationIdPostfix=ruleGenericDeclarationIdPostfix();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleGenericDeclarationIdPostfix; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleGenericDeclarationIdPostfix1751); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGenericDeclarationIdPostfix"


    // $ANTLR start "ruleGenericDeclarationIdPostfix"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:913:1: ruleGenericDeclarationIdPostfix returns [EObject current=null] : ( ( (lv_array_0_0= ruleArrayPostfix ) ) | ( (lv_function_1_0= ruleParametersPostfix ) ) ) ;
    public final EObject ruleGenericDeclarationIdPostfix() throws RecognitionException {
        EObject current = null;

        EObject lv_array_0_0 = null;

        EObject lv_function_1_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:916:28: ( ( ( (lv_array_0_0= ruleArrayPostfix ) ) | ( (lv_function_1_0= ruleParametersPostfix ) ) ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:917:1: ( ( (lv_array_0_0= ruleArrayPostfix ) ) | ( (lv_function_1_0= ruleParametersPostfix ) ) )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:917:1: ( ( (lv_array_0_0= ruleArrayPostfix ) ) | ( (lv_function_1_0= ruleParametersPostfix ) ) )
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==RULE_LBRACKET) ) {
                alt20=1;
            }
            else if ( (LA20_0==RULE_LPAREN) ) {
                alt20=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 20, 0, input);

                throw nvae;
            }
            switch (alt20) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:917:2: ( (lv_array_0_0= ruleArrayPostfix ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:917:2: ( (lv_array_0_0= ruleArrayPostfix ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:918:1: (lv_array_0_0= ruleArrayPostfix )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:918:1: (lv_array_0_0= ruleArrayPostfix )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:919:3: lv_array_0_0= ruleArrayPostfix
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getGenericDeclarationIdPostfixAccess().getArrayArrayPostfixParserRuleCall_0_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleArrayPostfix_in_ruleGenericDeclarationIdPostfix1797);
                    lv_array_0_0=ruleArrayPostfix();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getGenericDeclarationIdPostfixRule());
                      	        }
                             		set(
                             			current, 
                             			"array",
                              		lv_array_0_0, 
                              		"ArrayPostfix");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:936:6: ( (lv_function_1_0= ruleParametersPostfix ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:936:6: ( (lv_function_1_0= ruleParametersPostfix ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:937:1: (lv_function_1_0= ruleParametersPostfix )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:937:1: (lv_function_1_0= ruleParametersPostfix )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:938:3: lv_function_1_0= ruleParametersPostfix
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getGenericDeclarationIdPostfixAccess().getFunctionParametersPostfixParserRuleCall_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleParametersPostfix_in_ruleGenericDeclarationIdPostfix1824);
                    lv_function_1_0=ruleParametersPostfix();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getGenericDeclarationIdPostfixRule());
                      	        }
                             		set(
                             			current, 
                             			"function",
                              		lv_function_1_0, 
                              		"ParametersPostfix");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGenericDeclarationIdPostfix"


    // $ANTLR start "entryRuleArrayPostfix"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:962:1: entryRuleArrayPostfix returns [EObject current=null] : iv_ruleArrayPostfix= ruleArrayPostfix EOF ;
    public final EObject entryRuleArrayPostfix() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleArrayPostfix = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:963:2: (iv_ruleArrayPostfix= ruleArrayPostfix EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:964:2: iv_ruleArrayPostfix= ruleArrayPostfix EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getArrayPostfixRule()); 
            }
            pushFollow(FOLLOW_ruleArrayPostfix_in_entryRuleArrayPostfix1860);
            iv_ruleArrayPostfix=ruleArrayPostfix();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleArrayPostfix; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleArrayPostfix1870); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleArrayPostfix"


    // $ANTLR start "ruleArrayPostfix"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:971:1: ruleArrayPostfix returns [EObject current=null] : ( () this_LBRACKET_1= RULE_LBRACKET ( ( (lv_star_2_0= RULE_MULT_OP ) ) | ( (lv_exp_3_0= ruleExpression ) ) )? this_RBRACKET_4= RULE_RBRACKET ) ;
    public final EObject ruleArrayPostfix() throws RecognitionException {
        EObject current = null;

        Token this_LBRACKET_1=null;
        Token lv_star_2_0=null;
        Token this_RBRACKET_4=null;
        EObject lv_exp_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:974:28: ( ( () this_LBRACKET_1= RULE_LBRACKET ( ( (lv_star_2_0= RULE_MULT_OP ) ) | ( (lv_exp_3_0= ruleExpression ) ) )? this_RBRACKET_4= RULE_RBRACKET ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:975:1: ( () this_LBRACKET_1= RULE_LBRACKET ( ( (lv_star_2_0= RULE_MULT_OP ) ) | ( (lv_exp_3_0= ruleExpression ) ) )? this_RBRACKET_4= RULE_RBRACKET )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:975:1: ( () this_LBRACKET_1= RULE_LBRACKET ( ( (lv_star_2_0= RULE_MULT_OP ) ) | ( (lv_exp_3_0= ruleExpression ) ) )? this_RBRACKET_4= RULE_RBRACKET )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:975:2: () this_LBRACKET_1= RULE_LBRACKET ( ( (lv_star_2_0= RULE_MULT_OP ) ) | ( (lv_exp_3_0= ruleExpression ) ) )? this_RBRACKET_4= RULE_RBRACKET
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:975:2: ()
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:976:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getArrayPostfixAccess().getArrayPostfixAction_0(),
                          current);
                  
            }

            }

            this_LBRACKET_1=(Token)match(input,RULE_LBRACKET,FOLLOW_RULE_LBRACKET_in_ruleArrayPostfix1915); if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                  newLeafNode(this_LBRACKET_1, grammarAccess.getArrayPostfixAccess().getLBRACKETTerminalRuleCall_1()); 
                  
            }
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:985:1: ( ( (lv_star_2_0= RULE_MULT_OP ) ) | ( (lv_exp_3_0= ruleExpression ) ) )?
            int alt21=3;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==RULE_MULT_OP) ) {
                int LA21_1 = input.LA(2);

                if ( (LA21_1==RULE_MULT_OP||(LA21_1>=RULE_STRING_LITERAL && LA21_1<=RULE_IDENTIFIER)||LA21_1==RULE_LPAREN||LA21_1==RULE_LCBRACKET||LA21_1==RULE_AND_OP||(LA21_1>=RULE_ADD_OP && LA21_1<=RULE_MINUS_OP)||(LA21_1>=RULE_INC_OP && LA21_1<=RULE_FUNC_NAME)||(LA21_1>=RULE_TILDE && LA21_1<=RULE_NOT_OP)) ) {
                    alt21=2;
                }
                else if ( (LA21_1==RULE_RBRACKET) ) {
                    alt21=1;
                }
            }
            else if ( ((LA21_0>=RULE_STRING_LITERAL && LA21_0<=RULE_IDENTIFIER)||LA21_0==RULE_LPAREN||LA21_0==RULE_LCBRACKET||LA21_0==RULE_AND_OP||(LA21_0>=RULE_ADD_OP && LA21_0<=RULE_MINUS_OP)||(LA21_0>=RULE_INC_OP && LA21_0<=RULE_FUNC_NAME)||(LA21_0>=RULE_TILDE && LA21_0<=RULE_NOT_OP)) ) {
                alt21=2;
            }
            switch (alt21) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:985:2: ( (lv_star_2_0= RULE_MULT_OP ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:985:2: ( (lv_star_2_0= RULE_MULT_OP ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:986:1: (lv_star_2_0= RULE_MULT_OP )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:986:1: (lv_star_2_0= RULE_MULT_OP )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:987:3: lv_star_2_0= RULE_MULT_OP
                    {
                    lv_star_2_0=(Token)match(input,RULE_MULT_OP,FOLLOW_RULE_MULT_OP_in_ruleArrayPostfix1932); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_star_2_0, grammarAccess.getArrayPostfixAccess().getStarMULT_OPTerminalRuleCall_2_0_0()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getArrayPostfixRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"star",
                              		lv_star_2_0, 
                              		"MULT_OP");
                      	    
                    }

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1004:6: ( (lv_exp_3_0= ruleExpression ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1004:6: ( (lv_exp_3_0= ruleExpression ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1005:1: (lv_exp_3_0= ruleExpression )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1005:1: (lv_exp_3_0= ruleExpression )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1006:3: lv_exp_3_0= ruleExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getArrayPostfixAccess().getExpExpressionParserRuleCall_2_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleExpression_in_ruleArrayPostfix1964);
                    lv_exp_3_0=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getArrayPostfixRule());
                      	        }
                             		set(
                             			current, 
                             			"exp",
                              		lv_exp_3_0, 
                              		"Expression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }

            this_RBRACKET_4=(Token)match(input,RULE_RBRACKET,FOLLOW_RULE_RBRACKET_in_ruleArrayPostfix1977); if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                  newLeafNode(this_RBRACKET_4, grammarAccess.getArrayPostfixAccess().getRBRACKETTerminalRuleCall_3()); 
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleArrayPostfix"


    // $ANTLR start "entryRuleParametersPostfix"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1034:1: entryRuleParametersPostfix returns [EObject current=null] : iv_ruleParametersPostfix= ruleParametersPostfix EOF ;
    public final EObject entryRuleParametersPostfix() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParametersPostfix = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1035:2: (iv_ruleParametersPostfix= ruleParametersPostfix EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1036:2: iv_ruleParametersPostfix= ruleParametersPostfix EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getParametersPostfixRule()); 
            }
            pushFollow(FOLLOW_ruleParametersPostfix_in_entryRuleParametersPostfix2012);
            iv_ruleParametersPostfix=ruleParametersPostfix();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleParametersPostfix; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleParametersPostfix2022); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParametersPostfix"


    // $ANTLR start "ruleParametersPostfix"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1043:1: ruleParametersPostfix returns [EObject current=null] : ( () this_LPAREN_1= RULE_LPAREN ( ( (lv_els_2_0= ruleParameter ) ) ( (lv_next_3_0= ruleNextParameter ) )* )? (this_COMMA_4= RULE_COMMA ( (lv_more_5_0= RULE_ELLIPSIS ) ) )? this_RPAREN_6= RULE_RPAREN ) ;
    public final EObject ruleParametersPostfix() throws RecognitionException {
        EObject current = null;

        Token this_LPAREN_1=null;
        Token this_COMMA_4=null;
        Token lv_more_5_0=null;
        Token this_RPAREN_6=null;
        EObject lv_els_2_0 = null;

        EObject lv_next_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1046:28: ( ( () this_LPAREN_1= RULE_LPAREN ( ( (lv_els_2_0= ruleParameter ) ) ( (lv_next_3_0= ruleNextParameter ) )* )? (this_COMMA_4= RULE_COMMA ( (lv_more_5_0= RULE_ELLIPSIS ) ) )? this_RPAREN_6= RULE_RPAREN ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1047:1: ( () this_LPAREN_1= RULE_LPAREN ( ( (lv_els_2_0= ruleParameter ) ) ( (lv_next_3_0= ruleNextParameter ) )* )? (this_COMMA_4= RULE_COMMA ( (lv_more_5_0= RULE_ELLIPSIS ) ) )? this_RPAREN_6= RULE_RPAREN )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1047:1: ( () this_LPAREN_1= RULE_LPAREN ( ( (lv_els_2_0= ruleParameter ) ) ( (lv_next_3_0= ruleNextParameter ) )* )? (this_COMMA_4= RULE_COMMA ( (lv_more_5_0= RULE_ELLIPSIS ) ) )? this_RPAREN_6= RULE_RPAREN )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1047:2: () this_LPAREN_1= RULE_LPAREN ( ( (lv_els_2_0= ruleParameter ) ) ( (lv_next_3_0= ruleNextParameter ) )* )? (this_COMMA_4= RULE_COMMA ( (lv_more_5_0= RULE_ELLIPSIS ) ) )? this_RPAREN_6= RULE_RPAREN
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1047:2: ()
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1048:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getParametersPostfixAccess().getParametersPostfixAction_0(),
                          current);
                  
            }

            }

            this_LPAREN_1=(Token)match(input,RULE_LPAREN,FOLLOW_RULE_LPAREN_in_ruleParametersPostfix2067); if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                  newLeafNode(this_LPAREN_1, grammarAccess.getParametersPostfixAccess().getLPARENTerminalRuleCall_1()); 
                  
            }
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1057:1: ( ( (lv_els_2_0= ruleParameter ) ) ( (lv_next_3_0= ruleNextParameter ) )* )?
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( ((LA23_0>=RULE_ATOMIC && LA23_0<=RULE_IMAGINARY)||(LA23_0>=RULE_STRUCT && LA23_0<=RULE_ALIGNAS)) ) {
                alt23=1;
            }
            switch (alt23) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1057:2: ( (lv_els_2_0= ruleParameter ) ) ( (lv_next_3_0= ruleNextParameter ) )*
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1057:2: ( (lv_els_2_0= ruleParameter ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1058:1: (lv_els_2_0= ruleParameter )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1058:1: (lv_els_2_0= ruleParameter )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1059:3: lv_els_2_0= ruleParameter
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getParametersPostfixAccess().getElsParameterParserRuleCall_2_0_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleParameter_in_ruleParametersPostfix2088);
                    lv_els_2_0=ruleParameter();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getParametersPostfixRule());
                      	        }
                             		add(
                             			current, 
                             			"els",
                              		lv_els_2_0, 
                              		"Parameter");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1075:2: ( (lv_next_3_0= ruleNextParameter ) )*
                    loop22:
                    do {
                        int alt22=2;
                        int LA22_0 = input.LA(1);

                        if ( (LA22_0==RULE_COMMA) ) {
                            int LA22_1 = input.LA(2);

                            if ( ((LA22_1>=RULE_ATOMIC && LA22_1<=RULE_IMAGINARY)||(LA22_1>=RULE_STRUCT && LA22_1<=RULE_ALIGNAS)) ) {
                                alt22=1;
                            }


                        }


                        switch (alt22) {
                    	case 1 :
                    	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1076:1: (lv_next_3_0= ruleNextParameter )
                    	    {
                    	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1076:1: (lv_next_3_0= ruleNextParameter )
                    	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1077:3: lv_next_3_0= ruleNextParameter
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getParametersPostfixAccess().getNextNextParameterParserRuleCall_2_1_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FOLLOW_ruleNextParameter_in_ruleParametersPostfix2109);
                    	    lv_next_3_0=ruleNextParameter();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getParametersPostfixRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"next",
                    	              		lv_next_3_0, 
                    	              		"NextParameter");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop22;
                        }
                    } while (true);


                    }
                    break;

            }

            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1093:5: (this_COMMA_4= RULE_COMMA ( (lv_more_5_0= RULE_ELLIPSIS ) ) )?
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==RULE_COMMA) ) {
                alt24=1;
            }
            switch (alt24) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1093:6: this_COMMA_4= RULE_COMMA ( (lv_more_5_0= RULE_ELLIPSIS ) )
                    {
                    this_COMMA_4=(Token)match(input,RULE_COMMA,FOLLOW_RULE_COMMA_in_ruleParametersPostfix2124); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_COMMA_4, grammarAccess.getParametersPostfixAccess().getCOMMATerminalRuleCall_3_0()); 
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1097:1: ( (lv_more_5_0= RULE_ELLIPSIS ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1098:1: (lv_more_5_0= RULE_ELLIPSIS )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1098:1: (lv_more_5_0= RULE_ELLIPSIS )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1099:3: lv_more_5_0= RULE_ELLIPSIS
                    {
                    lv_more_5_0=(Token)match(input,RULE_ELLIPSIS,FOLLOW_RULE_ELLIPSIS_in_ruleParametersPostfix2140); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_more_5_0, grammarAccess.getParametersPostfixAccess().getMoreELLIPSISTerminalRuleCall_3_1_0()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getParametersPostfixRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"more",
                              		lv_more_5_0, 
                              		"ELLIPSIS");
                      	    
                    }

                    }


                    }


                    }
                    break;

            }

            this_RPAREN_6=(Token)match(input,RULE_RPAREN,FOLLOW_RULE_RPAREN_in_ruleParametersPostfix2158); if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                  newLeafNode(this_RPAREN_6, grammarAccess.getParametersPostfixAccess().getRPARENTerminalRuleCall_4()); 
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParametersPostfix"


    // $ANTLR start "entryRuleNextParameter"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1127:1: entryRuleNextParameter returns [EObject current=null] : iv_ruleNextParameter= ruleNextParameter EOF ;
    public final EObject entryRuleNextParameter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNextParameter = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1128:2: (iv_ruleNextParameter= ruleNextParameter EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1129:2: iv_ruleNextParameter= ruleNextParameter EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNextParameterRule()); 
            }
            pushFollow(FOLLOW_ruleNextParameter_in_entryRuleNextParameter2193);
            iv_ruleNextParameter=ruleNextParameter();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNextParameter; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleNextParameter2203); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNextParameter"


    // $ANTLR start "ruleNextParameter"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1136:1: ruleNextParameter returns [EObject current=null] : (this_COMMA_0= RULE_COMMA ( (lv_els_1_0= ruleParameter ) ) ) ;
    public final EObject ruleNextParameter() throws RecognitionException {
        EObject current = null;

        Token this_COMMA_0=null;
        EObject lv_els_1_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1139:28: ( (this_COMMA_0= RULE_COMMA ( (lv_els_1_0= ruleParameter ) ) ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1140:1: (this_COMMA_0= RULE_COMMA ( (lv_els_1_0= ruleParameter ) ) )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1140:1: (this_COMMA_0= RULE_COMMA ( (lv_els_1_0= ruleParameter ) ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1140:2: this_COMMA_0= RULE_COMMA ( (lv_els_1_0= ruleParameter ) )
            {
            this_COMMA_0=(Token)match(input,RULE_COMMA,FOLLOW_RULE_COMMA_in_ruleNextParameter2239); if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                  newLeafNode(this_COMMA_0, grammarAccess.getNextParameterAccess().getCOMMATerminalRuleCall_0()); 
                  
            }
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1144:1: ( (lv_els_1_0= ruleParameter ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1145:1: (lv_els_1_0= ruleParameter )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1145:1: (lv_els_1_0= ruleParameter )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1146:3: lv_els_1_0= ruleParameter
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getNextParameterAccess().getElsParameterParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleParameter_in_ruleNextParameter2259);
            lv_els_1_0=ruleParameter();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getNextParameterRule());
              	        }
                     		add(
                     			current, 
                     			"els",
                      		lv_els_1_0, 
                      		"Parameter");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNextParameter"


    // $ANTLR start "entryRuleParameter"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1170:1: entryRuleParameter returns [EObject current=null] : iv_ruleParameter= ruleParameter EOF ;
    public final EObject entryRuleParameter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParameter = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1171:2: (iv_ruleParameter= ruleParameter EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1172:2: iv_ruleParameter= ruleParameter EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getParameterRule()); 
            }
            pushFollow(FOLLOW_ruleParameter_in_entryRuleParameter2295);
            iv_ruleParameter=ruleParameter();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleParameter; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleParameter2305); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParameter"


    // $ANTLR start "ruleParameter"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1179:1: ruleParameter returns [EObject current=null] : ( ( (lv_specs_0_0= ruleDeclarationSpecifier ) )* ( (lv_type_1_0= ruleSimpleType ) )+ ( (lv_id_2_0= ruleGenericDeclarationId ) )? ) ;
    public final EObject ruleParameter() throws RecognitionException {
        EObject current = null;

        EObject lv_specs_0_0 = null;

        EObject lv_type_1_0 = null;

        EObject lv_id_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1182:28: ( ( ( (lv_specs_0_0= ruleDeclarationSpecifier ) )* ( (lv_type_1_0= ruleSimpleType ) )+ ( (lv_id_2_0= ruleGenericDeclarationId ) )? ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1183:1: ( ( (lv_specs_0_0= ruleDeclarationSpecifier ) )* ( (lv_type_1_0= ruleSimpleType ) )+ ( (lv_id_2_0= ruleGenericDeclarationId ) )? )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1183:1: ( ( (lv_specs_0_0= ruleDeclarationSpecifier ) )* ( (lv_type_1_0= ruleSimpleType ) )+ ( (lv_id_2_0= ruleGenericDeclarationId ) )? )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1183:2: ( (lv_specs_0_0= ruleDeclarationSpecifier ) )* ( (lv_type_1_0= ruleSimpleType ) )+ ( (lv_id_2_0= ruleGenericDeclarationId ) )?
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1183:2: ( (lv_specs_0_0= ruleDeclarationSpecifier ) )*
            loop25:
            do {
                int alt25=2;
                int LA25_0 = input.LA(1);

                if ( (LA25_0==RULE_ATOMIC) ) {
                    int LA25_2 = input.LA(2);

                    if ( ((LA25_2>=RULE_ATOMIC && LA25_2<=RULE_IMAGINARY)||(LA25_2>=RULE_STRUCT && LA25_2<=RULE_ALIGNAS)) ) {
                        alt25=1;
                    }


                }
                else if ( ((LA25_0>=RULE_EXTERN && LA25_0<=RULE_ALIGNAS)) ) {
                    alt25=1;
                }


                switch (alt25) {
            	case 1 :
            	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1184:1: (lv_specs_0_0= ruleDeclarationSpecifier )
            	    {
            	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1184:1: (lv_specs_0_0= ruleDeclarationSpecifier )
            	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1185:3: lv_specs_0_0= ruleDeclarationSpecifier
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getParameterAccess().getSpecsDeclarationSpecifierParserRuleCall_0_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleDeclarationSpecifier_in_ruleParameter2351);
            	    lv_specs_0_0=ruleDeclarationSpecifier();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getParameterRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"specs",
            	              		lv_specs_0_0, 
            	              		"DeclarationSpecifier");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop25;
                }
            } while (true);

            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1201:3: ( (lv_type_1_0= ruleSimpleType ) )+
            int cnt26=0;
            loop26:
            do {
                int alt26=2;
                int LA26_0 = input.LA(1);

                if ( ((LA26_0>=RULE_ATOMIC && LA26_0<=RULE_IMAGINARY)||(LA26_0>=RULE_STRUCT && LA26_0<=RULE_ENUM)) ) {
                    alt26=1;
                }


                switch (alt26) {
            	case 1 :
            	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1202:1: (lv_type_1_0= ruleSimpleType )
            	    {
            	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1202:1: (lv_type_1_0= ruleSimpleType )
            	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1203:3: lv_type_1_0= ruleSimpleType
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getParameterAccess().getTypeSimpleTypeParserRuleCall_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleSimpleType_in_ruleParameter2373);
            	    lv_type_1_0=ruleSimpleType();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getParameterRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"type",
            	              		lv_type_1_0, 
            	              		"SimpleType");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt26 >= 1 ) break loop26;
            	    if (state.backtracking>0) {state.failed=true; return current;}
                        EarlyExitException eee =
                            new EarlyExitException(26, input);
                        throw eee;
                }
                cnt26++;
            } while (true);

            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1219:3: ( (lv_id_2_0= ruleGenericDeclarationId ) )?
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==RULE_MULT_OP||LA27_0==RULE_IDENTIFIER||LA27_0==RULE_LPAREN||LA27_0==RULE_LBRACKET) ) {
                alt27=1;
            }
            switch (alt27) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1220:1: (lv_id_2_0= ruleGenericDeclarationId )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1220:1: (lv_id_2_0= ruleGenericDeclarationId )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1221:3: lv_id_2_0= ruleGenericDeclarationId
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getParameterAccess().getIdGenericDeclarationIdParserRuleCall_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleGenericDeclarationId_in_ruleParameter2395);
                    lv_id_2_0=ruleGenericDeclarationId();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getParameterRule());
                      	        }
                             		set(
                             			current, 
                             			"id",
                              		lv_id_2_0, 
                              		"GenericDeclarationId");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParameter"


    // $ANTLR start "entryRuleGenericDeclarationInitializer"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1245:1: entryRuleGenericDeclarationInitializer returns [EObject current=null] : iv_ruleGenericDeclarationInitializer= ruleGenericDeclarationInitializer EOF ;
    public final EObject entryRuleGenericDeclarationInitializer() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGenericDeclarationInitializer = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1246:2: (iv_ruleGenericDeclarationInitializer= ruleGenericDeclarationInitializer EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1247:2: iv_ruleGenericDeclarationInitializer= ruleGenericDeclarationInitializer EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getGenericDeclarationInitializerRule()); 
            }
            pushFollow(FOLLOW_ruleGenericDeclarationInitializer_in_entryRuleGenericDeclarationInitializer2432);
            iv_ruleGenericDeclarationInitializer=ruleGenericDeclarationInitializer();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleGenericDeclarationInitializer; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleGenericDeclarationInitializer2442); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGenericDeclarationInitializer"


    // $ANTLR start "ruleGenericDeclarationInitializer"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1254:1: ruleGenericDeclarationInitializer returns [EObject current=null] : ( ( () this_ASSIGN_1= RULE_ASSIGN ( (lv_exp_2_0= ruleExpression ) ) ) | ( () ( (lv_block_4_0= ruleCompoundStatement ) ) ) ) ;
    public final EObject ruleGenericDeclarationInitializer() throws RecognitionException {
        EObject current = null;

        Token this_ASSIGN_1=null;
        EObject lv_exp_2_0 = null;

        EObject lv_block_4_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1257:28: ( ( ( () this_ASSIGN_1= RULE_ASSIGN ( (lv_exp_2_0= ruleExpression ) ) ) | ( () ( (lv_block_4_0= ruleCompoundStatement ) ) ) ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1258:1: ( ( () this_ASSIGN_1= RULE_ASSIGN ( (lv_exp_2_0= ruleExpression ) ) ) | ( () ( (lv_block_4_0= ruleCompoundStatement ) ) ) )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1258:1: ( ( () this_ASSIGN_1= RULE_ASSIGN ( (lv_exp_2_0= ruleExpression ) ) ) | ( () ( (lv_block_4_0= ruleCompoundStatement ) ) ) )
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==RULE_ASSIGN) ) {
                alt28=1;
            }
            else if ( (LA28_0==RULE_LCBRACKET) ) {
                alt28=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 28, 0, input);

                throw nvae;
            }
            switch (alt28) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1258:2: ( () this_ASSIGN_1= RULE_ASSIGN ( (lv_exp_2_0= ruleExpression ) ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1258:2: ( () this_ASSIGN_1= RULE_ASSIGN ( (lv_exp_2_0= ruleExpression ) ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1258:3: () this_ASSIGN_1= RULE_ASSIGN ( (lv_exp_2_0= ruleExpression ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1258:3: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1259:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getGenericDeclarationInitializerAccess().getExpressionInitializerAction_0_0(),
                                  current);
                          
                    }

                    }

                    this_ASSIGN_1=(Token)match(input,RULE_ASSIGN,FOLLOW_RULE_ASSIGN_in_ruleGenericDeclarationInitializer2488); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_ASSIGN_1, grammarAccess.getGenericDeclarationInitializerAccess().getASSIGNTerminalRuleCall_0_1()); 
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1268:1: ( (lv_exp_2_0= ruleExpression ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1269:1: (lv_exp_2_0= ruleExpression )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1269:1: (lv_exp_2_0= ruleExpression )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1270:3: lv_exp_2_0= ruleExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getGenericDeclarationInitializerAccess().getExpExpressionParserRuleCall_0_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleExpression_in_ruleGenericDeclarationInitializer2508);
                    lv_exp_2_0=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getGenericDeclarationInitializerRule());
                      	        }
                             		set(
                             			current, 
                             			"exp",
                              		lv_exp_2_0, 
                              		"Expression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1287:6: ( () ( (lv_block_4_0= ruleCompoundStatement ) ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1287:6: ( () ( (lv_block_4_0= ruleCompoundStatement ) ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1287:7: () ( (lv_block_4_0= ruleCompoundStatement ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1287:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1288:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getGenericDeclarationInitializerAccess().getFunctionInitializerAction_1_0(),
                                  current);
                          
                    }

                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1293:2: ( (lv_block_4_0= ruleCompoundStatement ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1294:1: (lv_block_4_0= ruleCompoundStatement )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1294:1: (lv_block_4_0= ruleCompoundStatement )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1295:3: lv_block_4_0= ruleCompoundStatement
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getGenericDeclarationInitializerAccess().getBlockCompoundStatementParserRuleCall_1_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleCompoundStatement_in_ruleGenericDeclarationInitializer2546);
                    lv_block_4_0=ruleCompoundStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getGenericDeclarationInitializerRule());
                      	        }
                             		set(
                             			current, 
                             			"block",
                              		lv_block_4_0, 
                              		"CompoundStatement");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGenericDeclarationInitializer"


    // $ANTLR start "entryRuleSpecifiedType"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1319:1: entryRuleSpecifiedType returns [EObject current=null] : iv_ruleSpecifiedType= ruleSpecifiedType EOF ;
    public final EObject entryRuleSpecifiedType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSpecifiedType = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1320:2: (iv_ruleSpecifiedType= ruleSpecifiedType EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1321:2: iv_ruleSpecifiedType= ruleSpecifiedType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSpecifiedTypeRule()); 
            }
            pushFollow(FOLLOW_ruleSpecifiedType_in_entryRuleSpecifiedType2583);
            iv_ruleSpecifiedType=ruleSpecifiedType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSpecifiedType; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleSpecifiedType2593); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSpecifiedType"


    // $ANTLR start "ruleSpecifiedType"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1328:1: ruleSpecifiedType returns [EObject current=null] : ( (lv_par_0_0= ruleParameter ) ) ;
    public final EObject ruleSpecifiedType() throws RecognitionException {
        EObject current = null;

        EObject lv_par_0_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1331:28: ( ( (lv_par_0_0= ruleParameter ) ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1332:1: ( (lv_par_0_0= ruleParameter ) )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1332:1: ( (lv_par_0_0= ruleParameter ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1333:1: (lv_par_0_0= ruleParameter )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1333:1: (lv_par_0_0= ruleParameter )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1334:3: lv_par_0_0= ruleParameter
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getSpecifiedTypeAccess().getParParameterParserRuleCall_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleParameter_in_ruleSpecifiedType2638);
            lv_par_0_0=ruleParameter();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getSpecifiedTypeRule());
              	        }
                     		set(
                     			current, 
                     			"par",
                      		lv_par_0_0, 
                      		"Parameter");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSpecifiedType"


    // $ANTLR start "entryRuleStaticAssertDeclaration"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1358:1: entryRuleStaticAssertDeclaration returns [EObject current=null] : iv_ruleStaticAssertDeclaration= ruleStaticAssertDeclaration EOF ;
    public final EObject entryRuleStaticAssertDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStaticAssertDeclaration = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1359:2: (iv_ruleStaticAssertDeclaration= ruleStaticAssertDeclaration EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1360:2: iv_ruleStaticAssertDeclaration= ruleStaticAssertDeclaration EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStaticAssertDeclarationRule()); 
            }
            pushFollow(FOLLOW_ruleStaticAssertDeclaration_in_entryRuleStaticAssertDeclaration2673);
            iv_ruleStaticAssertDeclaration=ruleStaticAssertDeclaration();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStaticAssertDeclaration; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleStaticAssertDeclaration2683); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStaticAssertDeclaration"


    // $ANTLR start "ruleStaticAssertDeclaration"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1367:1: ruleStaticAssertDeclaration returns [EObject current=null] : (this_STATIC_ASSERT_0= RULE_STATIC_ASSERT this_LPAREN_1= RULE_LPAREN ( (lv_exp_2_0= ruleBaseExpression ) ) this_COMMA_3= RULE_COMMA ( (lv_string_4_0= RULE_STRING_LITERAL ) ) this_RPAREN_5= RULE_RPAREN this_SEMICOLON_6= RULE_SEMICOLON ) ;
    public final EObject ruleStaticAssertDeclaration() throws RecognitionException {
        EObject current = null;

        Token this_STATIC_ASSERT_0=null;
        Token this_LPAREN_1=null;
        Token this_COMMA_3=null;
        Token lv_string_4_0=null;
        Token this_RPAREN_5=null;
        Token this_SEMICOLON_6=null;
        EObject lv_exp_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1370:28: ( (this_STATIC_ASSERT_0= RULE_STATIC_ASSERT this_LPAREN_1= RULE_LPAREN ( (lv_exp_2_0= ruleBaseExpression ) ) this_COMMA_3= RULE_COMMA ( (lv_string_4_0= RULE_STRING_LITERAL ) ) this_RPAREN_5= RULE_RPAREN this_SEMICOLON_6= RULE_SEMICOLON ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1371:1: (this_STATIC_ASSERT_0= RULE_STATIC_ASSERT this_LPAREN_1= RULE_LPAREN ( (lv_exp_2_0= ruleBaseExpression ) ) this_COMMA_3= RULE_COMMA ( (lv_string_4_0= RULE_STRING_LITERAL ) ) this_RPAREN_5= RULE_RPAREN this_SEMICOLON_6= RULE_SEMICOLON )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1371:1: (this_STATIC_ASSERT_0= RULE_STATIC_ASSERT this_LPAREN_1= RULE_LPAREN ( (lv_exp_2_0= ruleBaseExpression ) ) this_COMMA_3= RULE_COMMA ( (lv_string_4_0= RULE_STRING_LITERAL ) ) this_RPAREN_5= RULE_RPAREN this_SEMICOLON_6= RULE_SEMICOLON )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1371:2: this_STATIC_ASSERT_0= RULE_STATIC_ASSERT this_LPAREN_1= RULE_LPAREN ( (lv_exp_2_0= ruleBaseExpression ) ) this_COMMA_3= RULE_COMMA ( (lv_string_4_0= RULE_STRING_LITERAL ) ) this_RPAREN_5= RULE_RPAREN this_SEMICOLON_6= RULE_SEMICOLON
            {
            this_STATIC_ASSERT_0=(Token)match(input,RULE_STATIC_ASSERT,FOLLOW_RULE_STATIC_ASSERT_in_ruleStaticAssertDeclaration2719); if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                  newLeafNode(this_STATIC_ASSERT_0, grammarAccess.getStaticAssertDeclarationAccess().getSTATIC_ASSERTTerminalRuleCall_0()); 
                  
            }
            this_LPAREN_1=(Token)match(input,RULE_LPAREN,FOLLOW_RULE_LPAREN_in_ruleStaticAssertDeclaration2729); if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                  newLeafNode(this_LPAREN_1, grammarAccess.getStaticAssertDeclarationAccess().getLPARENTerminalRuleCall_1()); 
                  
            }
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1379:1: ( (lv_exp_2_0= ruleBaseExpression ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1380:1: (lv_exp_2_0= ruleBaseExpression )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1380:1: (lv_exp_2_0= ruleBaseExpression )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1381:3: lv_exp_2_0= ruleBaseExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getStaticAssertDeclarationAccess().getExpBaseExpressionParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleBaseExpression_in_ruleStaticAssertDeclaration2749);
            lv_exp_2_0=ruleBaseExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getStaticAssertDeclarationRule());
              	        }
                     		set(
                     			current, 
                     			"exp",
                      		lv_exp_2_0, 
                      		"BaseExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            this_COMMA_3=(Token)match(input,RULE_COMMA,FOLLOW_RULE_COMMA_in_ruleStaticAssertDeclaration2760); if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                  newLeafNode(this_COMMA_3, grammarAccess.getStaticAssertDeclarationAccess().getCOMMATerminalRuleCall_3()); 
                  
            }
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1401:1: ( (lv_string_4_0= RULE_STRING_LITERAL ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1402:1: (lv_string_4_0= RULE_STRING_LITERAL )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1402:1: (lv_string_4_0= RULE_STRING_LITERAL )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1403:3: lv_string_4_0= RULE_STRING_LITERAL
            {
            lv_string_4_0=(Token)match(input,RULE_STRING_LITERAL,FOLLOW_RULE_STRING_LITERAL_in_ruleStaticAssertDeclaration2776); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_string_4_0, grammarAccess.getStaticAssertDeclarationAccess().getStringSTRING_LITERALTerminalRuleCall_4_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getStaticAssertDeclarationRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"string",
                      		lv_string_4_0, 
                      		"STRING_LITERAL");
              	    
            }

            }


            }

            this_RPAREN_5=(Token)match(input,RULE_RPAREN,FOLLOW_RULE_RPAREN_in_ruleStaticAssertDeclaration2792); if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                  newLeafNode(this_RPAREN_5, grammarAccess.getStaticAssertDeclarationAccess().getRPARENTerminalRuleCall_5()); 
                  
            }
            this_SEMICOLON_6=(Token)match(input,RULE_SEMICOLON,FOLLOW_RULE_SEMICOLON_in_ruleStaticAssertDeclaration2802); if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                  newLeafNode(this_SEMICOLON_6, grammarAccess.getStaticAssertDeclarationAccess().getSEMICOLONTerminalRuleCall_6()); 
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStaticAssertDeclaration"


    // $ANTLR start "entryRuleSimpleType"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1435:1: entryRuleSimpleType returns [EObject current=null] : iv_ruleSimpleType= ruleSimpleType EOF ;
    public final EObject entryRuleSimpleType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSimpleType = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1436:2: (iv_ruleSimpleType= ruleSimpleType EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1437:2: iv_ruleSimpleType= ruleSimpleType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSimpleTypeRule()); 
            }
            pushFollow(FOLLOW_ruleSimpleType_in_entryRuleSimpleType2837);
            iv_ruleSimpleType=ruleSimpleType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSimpleType; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleSimpleType2847); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSimpleType"


    // $ANTLR start "ruleSimpleType"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1444:1: ruleSimpleType returns [EObject current=null] : (this_BaseType_0= ruleBaseType | this_StructOrUnionType_1= ruleStructOrUnionType | this_EnumType_2= ruleEnumType | ( () this_ATOMIC_4= RULE_ATOMIC this_LPAREN_5= RULE_LPAREN ( ( (lv_type_6_1= ruleBaseType | lv_type_6_2= ruleStructOrUnionType | lv_type_6_3= ruleEnumType ) ) ) this_RPAREN_7= RULE_RPAREN ) ) ;
    public final EObject ruleSimpleType() throws RecognitionException {
        EObject current = null;

        Token this_ATOMIC_4=null;
        Token this_LPAREN_5=null;
        Token this_RPAREN_7=null;
        EObject this_BaseType_0 = null;

        EObject this_StructOrUnionType_1 = null;

        EObject this_EnumType_2 = null;

        EObject lv_type_6_1 = null;

        EObject lv_type_6_2 = null;

        EObject lv_type_6_3 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1447:28: ( (this_BaseType_0= ruleBaseType | this_StructOrUnionType_1= ruleStructOrUnionType | this_EnumType_2= ruleEnumType | ( () this_ATOMIC_4= RULE_ATOMIC this_LPAREN_5= RULE_LPAREN ( ( (lv_type_6_1= ruleBaseType | lv_type_6_2= ruleStructOrUnionType | lv_type_6_3= ruleEnumType ) ) ) this_RPAREN_7= RULE_RPAREN ) ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1448:1: (this_BaseType_0= ruleBaseType | this_StructOrUnionType_1= ruleStructOrUnionType | this_EnumType_2= ruleEnumType | ( () this_ATOMIC_4= RULE_ATOMIC this_LPAREN_5= RULE_LPAREN ( ( (lv_type_6_1= ruleBaseType | lv_type_6_2= ruleStructOrUnionType | lv_type_6_3= ruleEnumType ) ) ) this_RPAREN_7= RULE_RPAREN ) )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1448:1: (this_BaseType_0= ruleBaseType | this_StructOrUnionType_1= ruleStructOrUnionType | this_EnumType_2= ruleEnumType | ( () this_ATOMIC_4= RULE_ATOMIC this_LPAREN_5= RULE_LPAREN ( ( (lv_type_6_1= ruleBaseType | lv_type_6_2= ruleStructOrUnionType | lv_type_6_3= ruleEnumType ) ) ) this_RPAREN_7= RULE_RPAREN ) )
            int alt30=4;
            switch ( input.LA(1) ) {
            case RULE_VOID:
            case RULE_CUSTOM:
            case RULE_CHAR:
            case RULE_SHORT:
            case RULE_INT:
            case RULE_LONG:
            case RULE_FLOAT:
            case RULE_DOUBLE:
            case RULE_SIGNED:
            case RULE_UNSIGNED:
            case RULE_BOOL:
            case RULE_COMPLEX:
            case RULE_IMAGINARY:
                {
                alt30=1;
                }
                break;
            case RULE_STRUCT:
            case RULE_UNION:
                {
                alt30=2;
                }
                break;
            case RULE_ENUM:
                {
                alt30=3;
                }
                break;
            case RULE_ATOMIC:
                {
                alt30=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 30, 0, input);

                throw nvae;
            }

            switch (alt30) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1449:5: this_BaseType_0= ruleBaseType
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getSimpleTypeAccess().getBaseTypeParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleBaseType_in_ruleSimpleType2894);
                    this_BaseType_0=ruleBaseType();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_BaseType_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1459:5: this_StructOrUnionType_1= ruleStructOrUnionType
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getSimpleTypeAccess().getStructOrUnionTypeParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleStructOrUnionType_in_ruleSimpleType2921);
                    this_StructOrUnionType_1=ruleStructOrUnionType();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_StructOrUnionType_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1469:5: this_EnumType_2= ruleEnumType
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getSimpleTypeAccess().getEnumTypeParserRuleCall_2()); 
                          
                    }
                    pushFollow(FOLLOW_ruleEnumType_in_ruleSimpleType2948);
                    this_EnumType_2=ruleEnumType();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_EnumType_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 4 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1478:6: ( () this_ATOMIC_4= RULE_ATOMIC this_LPAREN_5= RULE_LPAREN ( ( (lv_type_6_1= ruleBaseType | lv_type_6_2= ruleStructOrUnionType | lv_type_6_3= ruleEnumType ) ) ) this_RPAREN_7= RULE_RPAREN )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1478:6: ( () this_ATOMIC_4= RULE_ATOMIC this_LPAREN_5= RULE_LPAREN ( ( (lv_type_6_1= ruleBaseType | lv_type_6_2= ruleStructOrUnionType | lv_type_6_3= ruleEnumType ) ) ) this_RPAREN_7= RULE_RPAREN )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1478:7: () this_ATOMIC_4= RULE_ATOMIC this_LPAREN_5= RULE_LPAREN ( ( (lv_type_6_1= ruleBaseType | lv_type_6_2= ruleStructOrUnionType | lv_type_6_3= ruleEnumType ) ) ) this_RPAREN_7= RULE_RPAREN
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1478:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1479:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getSimpleTypeAccess().getAtomicAction_3_0(),
                                  current);
                          
                    }

                    }

                    this_ATOMIC_4=(Token)match(input,RULE_ATOMIC,FOLLOW_RULE_ATOMIC_in_ruleSimpleType2974); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_ATOMIC_4, grammarAccess.getSimpleTypeAccess().getATOMICTerminalRuleCall_3_1()); 
                          
                    }
                    this_LPAREN_5=(Token)match(input,RULE_LPAREN,FOLLOW_RULE_LPAREN_in_ruleSimpleType2984); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_LPAREN_5, grammarAccess.getSimpleTypeAccess().getLPARENTerminalRuleCall_3_2()); 
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1492:1: ( ( (lv_type_6_1= ruleBaseType | lv_type_6_2= ruleStructOrUnionType | lv_type_6_3= ruleEnumType ) ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1493:1: ( (lv_type_6_1= ruleBaseType | lv_type_6_2= ruleStructOrUnionType | lv_type_6_3= ruleEnumType ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1493:1: ( (lv_type_6_1= ruleBaseType | lv_type_6_2= ruleStructOrUnionType | lv_type_6_3= ruleEnumType ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1494:1: (lv_type_6_1= ruleBaseType | lv_type_6_2= ruleStructOrUnionType | lv_type_6_3= ruleEnumType )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1494:1: (lv_type_6_1= ruleBaseType | lv_type_6_2= ruleStructOrUnionType | lv_type_6_3= ruleEnumType )
                    int alt29=3;
                    switch ( input.LA(1) ) {
                    case RULE_VOID:
                    case RULE_CUSTOM:
                    case RULE_CHAR:
                    case RULE_SHORT:
                    case RULE_INT:
                    case RULE_LONG:
                    case RULE_FLOAT:
                    case RULE_DOUBLE:
                    case RULE_SIGNED:
                    case RULE_UNSIGNED:
                    case RULE_BOOL:
                    case RULE_COMPLEX:
                    case RULE_IMAGINARY:
                        {
                        alt29=1;
                        }
                        break;
                    case RULE_STRUCT:
                    case RULE_UNION:
                        {
                        alt29=2;
                        }
                        break;
                    case RULE_ENUM:
                        {
                        alt29=3;
                        }
                        break;
                    default:
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 29, 0, input);

                        throw nvae;
                    }

                    switch (alt29) {
                        case 1 :
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1495:3: lv_type_6_1= ruleBaseType
                            {
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getSimpleTypeAccess().getTypeBaseTypeParserRuleCall_3_3_0_0()); 
                              	    
                            }
                            pushFollow(FOLLOW_ruleBaseType_in_ruleSimpleType3006);
                            lv_type_6_1=ruleBaseType();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElementForParent(grammarAccess.getSimpleTypeRule());
                              	        }
                                     		set(
                                     			current, 
                                     			"type",
                                      		lv_type_6_1, 
                                      		"BaseType");
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }
                            break;
                        case 2 :
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1510:8: lv_type_6_2= ruleStructOrUnionType
                            {
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getSimpleTypeAccess().getTypeStructOrUnionTypeParserRuleCall_3_3_0_1()); 
                              	    
                            }
                            pushFollow(FOLLOW_ruleStructOrUnionType_in_ruleSimpleType3025);
                            lv_type_6_2=ruleStructOrUnionType();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElementForParent(grammarAccess.getSimpleTypeRule());
                              	        }
                                     		set(
                                     			current, 
                                     			"type",
                                      		lv_type_6_2, 
                                      		"StructOrUnionType");
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }
                            break;
                        case 3 :
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1525:8: lv_type_6_3= ruleEnumType
                            {
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getSimpleTypeAccess().getTypeEnumTypeParserRuleCall_3_3_0_2()); 
                              	    
                            }
                            pushFollow(FOLLOW_ruleEnumType_in_ruleSimpleType3044);
                            lv_type_6_3=ruleEnumType();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElementForParent(grammarAccess.getSimpleTypeRule());
                              	        }
                                     		set(
                                     			current, 
                                     			"type",
                                      		lv_type_6_3, 
                                      		"EnumType");
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }
                            break;

                    }


                    }


                    }

                    this_RPAREN_7=(Token)match(input,RULE_RPAREN,FOLLOW_RULE_RPAREN_in_ruleSimpleType3058); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_RPAREN_7, grammarAccess.getSimpleTypeAccess().getRPARENTerminalRuleCall_3_4()); 
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSimpleType"


    // $ANTLR start "entryRuleBaseType"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1555:1: entryRuleBaseType returns [EObject current=null] : iv_ruleBaseType= ruleBaseType EOF ;
    public final EObject entryRuleBaseType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBaseType = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1556:2: (iv_ruleBaseType= ruleBaseType EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1557:2: iv_ruleBaseType= ruleBaseType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBaseTypeRule()); 
            }
            pushFollow(FOLLOW_ruleBaseType_in_entryRuleBaseType3094);
            iv_ruleBaseType=ruleBaseType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBaseType; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleBaseType3104); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBaseType"


    // $ANTLR start "ruleBaseType"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1564:1: ruleBaseType returns [EObject current=null] : ( ( () this_VOID_1= RULE_VOID ) | ( () ( (lv_id_3_0= RULE_CUSTOM ) ) ) | ( () this_CHAR_5= RULE_CHAR ) | ( () this_SHORT_7= RULE_SHORT ) | ( () this_INT_9= RULE_INT ) | ( () this_LONG_11= RULE_LONG ) | ( () this_FLOAT_13= RULE_FLOAT ) | ( () this_DOUBLE_15= RULE_DOUBLE ) | ( () this_SIGNED_17= RULE_SIGNED ) | ( () this_UNSIGNED_19= RULE_UNSIGNED ) | ( () this_BOOL_21= RULE_BOOL ) | ( () this_COMPLEX_23= RULE_COMPLEX ) | ( () this_IMAGINARY_25= RULE_IMAGINARY ) ) ;
    public final EObject ruleBaseType() throws RecognitionException {
        EObject current = null;

        Token this_VOID_1=null;
        Token lv_id_3_0=null;
        Token this_CHAR_5=null;
        Token this_SHORT_7=null;
        Token this_INT_9=null;
        Token this_LONG_11=null;
        Token this_FLOAT_13=null;
        Token this_DOUBLE_15=null;
        Token this_SIGNED_17=null;
        Token this_UNSIGNED_19=null;
        Token this_BOOL_21=null;
        Token this_COMPLEX_23=null;
        Token this_IMAGINARY_25=null;

         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1567:28: ( ( ( () this_VOID_1= RULE_VOID ) | ( () ( (lv_id_3_0= RULE_CUSTOM ) ) ) | ( () this_CHAR_5= RULE_CHAR ) | ( () this_SHORT_7= RULE_SHORT ) | ( () this_INT_9= RULE_INT ) | ( () this_LONG_11= RULE_LONG ) | ( () this_FLOAT_13= RULE_FLOAT ) | ( () this_DOUBLE_15= RULE_DOUBLE ) | ( () this_SIGNED_17= RULE_SIGNED ) | ( () this_UNSIGNED_19= RULE_UNSIGNED ) | ( () this_BOOL_21= RULE_BOOL ) | ( () this_COMPLEX_23= RULE_COMPLEX ) | ( () this_IMAGINARY_25= RULE_IMAGINARY ) ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1568:1: ( ( () this_VOID_1= RULE_VOID ) | ( () ( (lv_id_3_0= RULE_CUSTOM ) ) ) | ( () this_CHAR_5= RULE_CHAR ) | ( () this_SHORT_7= RULE_SHORT ) | ( () this_INT_9= RULE_INT ) | ( () this_LONG_11= RULE_LONG ) | ( () this_FLOAT_13= RULE_FLOAT ) | ( () this_DOUBLE_15= RULE_DOUBLE ) | ( () this_SIGNED_17= RULE_SIGNED ) | ( () this_UNSIGNED_19= RULE_UNSIGNED ) | ( () this_BOOL_21= RULE_BOOL ) | ( () this_COMPLEX_23= RULE_COMPLEX ) | ( () this_IMAGINARY_25= RULE_IMAGINARY ) )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1568:1: ( ( () this_VOID_1= RULE_VOID ) | ( () ( (lv_id_3_0= RULE_CUSTOM ) ) ) | ( () this_CHAR_5= RULE_CHAR ) | ( () this_SHORT_7= RULE_SHORT ) | ( () this_INT_9= RULE_INT ) | ( () this_LONG_11= RULE_LONG ) | ( () this_FLOAT_13= RULE_FLOAT ) | ( () this_DOUBLE_15= RULE_DOUBLE ) | ( () this_SIGNED_17= RULE_SIGNED ) | ( () this_UNSIGNED_19= RULE_UNSIGNED ) | ( () this_BOOL_21= RULE_BOOL ) | ( () this_COMPLEX_23= RULE_COMPLEX ) | ( () this_IMAGINARY_25= RULE_IMAGINARY ) )
            int alt31=13;
            switch ( input.LA(1) ) {
            case RULE_VOID:
                {
                alt31=1;
                }
                break;
            case RULE_CUSTOM:
                {
                alt31=2;
                }
                break;
            case RULE_CHAR:
                {
                alt31=3;
                }
                break;
            case RULE_SHORT:
                {
                alt31=4;
                }
                break;
            case RULE_INT:
                {
                alt31=5;
                }
                break;
            case RULE_LONG:
                {
                alt31=6;
                }
                break;
            case RULE_FLOAT:
                {
                alt31=7;
                }
                break;
            case RULE_DOUBLE:
                {
                alt31=8;
                }
                break;
            case RULE_SIGNED:
                {
                alt31=9;
                }
                break;
            case RULE_UNSIGNED:
                {
                alt31=10;
                }
                break;
            case RULE_BOOL:
                {
                alt31=11;
                }
                break;
            case RULE_COMPLEX:
                {
                alt31=12;
                }
                break;
            case RULE_IMAGINARY:
                {
                alt31=13;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 31, 0, input);

                throw nvae;
            }

            switch (alt31) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1568:2: ( () this_VOID_1= RULE_VOID )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1568:2: ( () this_VOID_1= RULE_VOID )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1568:3: () this_VOID_1= RULE_VOID
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1568:3: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1569:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getBaseTypeAccess().getBaseTypeVoidAction_0_0(),
                                  current);
                          
                    }

                    }

                    this_VOID_1=(Token)match(input,RULE_VOID,FOLLOW_RULE_VOID_in_ruleBaseType3150); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_VOID_1, grammarAccess.getBaseTypeAccess().getVOIDTerminalRuleCall_0_1()); 
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1579:6: ( () ( (lv_id_3_0= RULE_CUSTOM ) ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1579:6: ( () ( (lv_id_3_0= RULE_CUSTOM ) ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1579:7: () ( (lv_id_3_0= RULE_CUSTOM ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1579:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1580:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getBaseTypeAccess().getCustomAction_1_0(),
                                  current);
                          
                    }

                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1585:2: ( (lv_id_3_0= RULE_CUSTOM ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1586:1: (lv_id_3_0= RULE_CUSTOM )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1586:1: (lv_id_3_0= RULE_CUSTOM )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1587:3: lv_id_3_0= RULE_CUSTOM
                    {
                    lv_id_3_0=(Token)match(input,RULE_CUSTOM,FOLLOW_RULE_CUSTOM_in_ruleBaseType3183); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_id_3_0, grammarAccess.getBaseTypeAccess().getIdCUSTOMTerminalRuleCall_1_1_0()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getBaseTypeRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"id",
                              		lv_id_3_0, 
                              		"CUSTOM");
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1604:6: ( () this_CHAR_5= RULE_CHAR )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1604:6: ( () this_CHAR_5= RULE_CHAR )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1604:7: () this_CHAR_5= RULE_CHAR
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1604:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1605:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getBaseTypeAccess().getBaseTypeCharAction_2_0(),
                                  current);
                          
                    }

                    }

                    this_CHAR_5=(Token)match(input,RULE_CHAR,FOLLOW_RULE_CHAR_in_ruleBaseType3216); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_CHAR_5, grammarAccess.getBaseTypeAccess().getCHARTerminalRuleCall_2_1()); 
                          
                    }

                    }


                    }
                    break;
                case 4 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1615:6: ( () this_SHORT_7= RULE_SHORT )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1615:6: ( () this_SHORT_7= RULE_SHORT )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1615:7: () this_SHORT_7= RULE_SHORT
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1615:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1616:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getBaseTypeAccess().getBaseTypeShortAction_3_0(),
                                  current);
                          
                    }

                    }

                    this_SHORT_7=(Token)match(input,RULE_SHORT,FOLLOW_RULE_SHORT_in_ruleBaseType3243); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_SHORT_7, grammarAccess.getBaseTypeAccess().getSHORTTerminalRuleCall_3_1()); 
                          
                    }

                    }


                    }
                    break;
                case 5 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1626:6: ( () this_INT_9= RULE_INT )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1626:6: ( () this_INT_9= RULE_INT )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1626:7: () this_INT_9= RULE_INT
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1626:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1627:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getBaseTypeAccess().getBaseTypeIntAction_4_0(),
                                  current);
                          
                    }

                    }

                    this_INT_9=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleBaseType3270); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_INT_9, grammarAccess.getBaseTypeAccess().getINTTerminalRuleCall_4_1()); 
                          
                    }

                    }


                    }
                    break;
                case 6 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1637:6: ( () this_LONG_11= RULE_LONG )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1637:6: ( () this_LONG_11= RULE_LONG )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1637:7: () this_LONG_11= RULE_LONG
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1637:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1638:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getBaseTypeAccess().getBaseTypeLongAction_5_0(),
                                  current);
                          
                    }

                    }

                    this_LONG_11=(Token)match(input,RULE_LONG,FOLLOW_RULE_LONG_in_ruleBaseType3297); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_LONG_11, grammarAccess.getBaseTypeAccess().getLONGTerminalRuleCall_5_1()); 
                          
                    }

                    }


                    }
                    break;
                case 7 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1648:6: ( () this_FLOAT_13= RULE_FLOAT )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1648:6: ( () this_FLOAT_13= RULE_FLOAT )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1648:7: () this_FLOAT_13= RULE_FLOAT
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1648:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1649:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getBaseTypeAccess().getBaseTypeFloatAction_6_0(),
                                  current);
                          
                    }

                    }

                    this_FLOAT_13=(Token)match(input,RULE_FLOAT,FOLLOW_RULE_FLOAT_in_ruleBaseType3324); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_FLOAT_13, grammarAccess.getBaseTypeAccess().getFLOATTerminalRuleCall_6_1()); 
                          
                    }

                    }


                    }
                    break;
                case 8 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1659:6: ( () this_DOUBLE_15= RULE_DOUBLE )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1659:6: ( () this_DOUBLE_15= RULE_DOUBLE )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1659:7: () this_DOUBLE_15= RULE_DOUBLE
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1659:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1660:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getBaseTypeAccess().getBaseTypeDoubleAction_7_0(),
                                  current);
                          
                    }

                    }

                    this_DOUBLE_15=(Token)match(input,RULE_DOUBLE,FOLLOW_RULE_DOUBLE_in_ruleBaseType3351); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_DOUBLE_15, grammarAccess.getBaseTypeAccess().getDOUBLETerminalRuleCall_7_1()); 
                          
                    }

                    }


                    }
                    break;
                case 9 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1670:6: ( () this_SIGNED_17= RULE_SIGNED )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1670:6: ( () this_SIGNED_17= RULE_SIGNED )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1670:7: () this_SIGNED_17= RULE_SIGNED
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1670:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1671:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getBaseTypeAccess().getBaseTypeSignedAction_8_0(),
                                  current);
                          
                    }

                    }

                    this_SIGNED_17=(Token)match(input,RULE_SIGNED,FOLLOW_RULE_SIGNED_in_ruleBaseType3378); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_SIGNED_17, grammarAccess.getBaseTypeAccess().getSIGNEDTerminalRuleCall_8_1()); 
                          
                    }

                    }


                    }
                    break;
                case 10 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1681:6: ( () this_UNSIGNED_19= RULE_UNSIGNED )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1681:6: ( () this_UNSIGNED_19= RULE_UNSIGNED )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1681:7: () this_UNSIGNED_19= RULE_UNSIGNED
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1681:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1682:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getBaseTypeAccess().getBaseTypeUnsignedAction_9_0(),
                                  current);
                          
                    }

                    }

                    this_UNSIGNED_19=(Token)match(input,RULE_UNSIGNED,FOLLOW_RULE_UNSIGNED_in_ruleBaseType3405); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_UNSIGNED_19, grammarAccess.getBaseTypeAccess().getUNSIGNEDTerminalRuleCall_9_1()); 
                          
                    }

                    }


                    }
                    break;
                case 11 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1692:6: ( () this_BOOL_21= RULE_BOOL )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1692:6: ( () this_BOOL_21= RULE_BOOL )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1692:7: () this_BOOL_21= RULE_BOOL
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1692:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1693:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getBaseTypeAccess().getBaseTypeBoolAction_10_0(),
                                  current);
                          
                    }

                    }

                    this_BOOL_21=(Token)match(input,RULE_BOOL,FOLLOW_RULE_BOOL_in_ruleBaseType3432); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_BOOL_21, grammarAccess.getBaseTypeAccess().getBOOLTerminalRuleCall_10_1()); 
                          
                    }

                    }


                    }
                    break;
                case 12 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1703:6: ( () this_COMPLEX_23= RULE_COMPLEX )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1703:6: ( () this_COMPLEX_23= RULE_COMPLEX )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1703:7: () this_COMPLEX_23= RULE_COMPLEX
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1703:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1704:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getBaseTypeAccess().getBaseTypeComplexAction_11_0(),
                                  current);
                          
                    }

                    }

                    this_COMPLEX_23=(Token)match(input,RULE_COMPLEX,FOLLOW_RULE_COMPLEX_in_ruleBaseType3459); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_COMPLEX_23, grammarAccess.getBaseTypeAccess().getCOMPLEXTerminalRuleCall_11_1()); 
                          
                    }

                    }


                    }
                    break;
                case 13 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1714:6: ( () this_IMAGINARY_25= RULE_IMAGINARY )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1714:6: ( () this_IMAGINARY_25= RULE_IMAGINARY )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1714:7: () this_IMAGINARY_25= RULE_IMAGINARY
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1714:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1715:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getBaseTypeAccess().getBaseTypeImaginaryAction_12_0(),
                                  current);
                          
                    }

                    }

                    this_IMAGINARY_25=(Token)match(input,RULE_IMAGINARY,FOLLOW_RULE_IMAGINARY_in_ruleBaseType3486); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_IMAGINARY_25, grammarAccess.getBaseTypeAccess().getIMAGINARYTerminalRuleCall_12_1()); 
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBaseType"


    // $ANTLR start "entryRuleStructOrUnionType"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1732:1: entryRuleStructOrUnionType returns [EObject current=null] : iv_ruleStructOrUnionType= ruleStructOrUnionType EOF ;
    public final EObject entryRuleStructOrUnionType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStructOrUnionType = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1733:2: (iv_ruleStructOrUnionType= ruleStructOrUnionType EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1734:2: iv_ruleStructOrUnionType= ruleStructOrUnionType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStructOrUnionTypeRule()); 
            }
            pushFollow(FOLLOW_ruleStructOrUnionType_in_entryRuleStructOrUnionType3522);
            iv_ruleStructOrUnionType=ruleStructOrUnionType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStructOrUnionType; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleStructOrUnionType3532); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStructOrUnionType"


    // $ANTLR start "ruleStructOrUnionType"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1741:1: ruleStructOrUnionType returns [EObject current=null] : ( ( (lv_kind_0_0= ruleStructOrUnion ) ) ( (this_LCBRACKET_1= RULE_LCBRACKET ( (lv_content_2_0= ruleGenericSpecifiedDeclaration ) )+ this_RCBRACKET_3= RULE_RCBRACKET ) | ( ( (lv_name_4_0= RULE_IDENTIFIER ) ) (this_LCBRACKET_5= RULE_LCBRACKET ( (lv_content_6_0= ruleGenericSpecifiedDeclaration ) )+ this_RCBRACKET_7= RULE_RCBRACKET )? ) ) ) ;
    public final EObject ruleStructOrUnionType() throws RecognitionException {
        EObject current = null;

        Token this_LCBRACKET_1=null;
        Token this_RCBRACKET_3=null;
        Token lv_name_4_0=null;
        Token this_LCBRACKET_5=null;
        Token this_RCBRACKET_7=null;
        AntlrDatatypeRuleToken lv_kind_0_0 = null;

        EObject lv_content_2_0 = null;

        EObject lv_content_6_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1744:28: ( ( ( (lv_kind_0_0= ruleStructOrUnion ) ) ( (this_LCBRACKET_1= RULE_LCBRACKET ( (lv_content_2_0= ruleGenericSpecifiedDeclaration ) )+ this_RCBRACKET_3= RULE_RCBRACKET ) | ( ( (lv_name_4_0= RULE_IDENTIFIER ) ) (this_LCBRACKET_5= RULE_LCBRACKET ( (lv_content_6_0= ruleGenericSpecifiedDeclaration ) )+ this_RCBRACKET_7= RULE_RCBRACKET )? ) ) ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1745:1: ( ( (lv_kind_0_0= ruleStructOrUnion ) ) ( (this_LCBRACKET_1= RULE_LCBRACKET ( (lv_content_2_0= ruleGenericSpecifiedDeclaration ) )+ this_RCBRACKET_3= RULE_RCBRACKET ) | ( ( (lv_name_4_0= RULE_IDENTIFIER ) ) (this_LCBRACKET_5= RULE_LCBRACKET ( (lv_content_6_0= ruleGenericSpecifiedDeclaration ) )+ this_RCBRACKET_7= RULE_RCBRACKET )? ) ) )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1745:1: ( ( (lv_kind_0_0= ruleStructOrUnion ) ) ( (this_LCBRACKET_1= RULE_LCBRACKET ( (lv_content_2_0= ruleGenericSpecifiedDeclaration ) )+ this_RCBRACKET_3= RULE_RCBRACKET ) | ( ( (lv_name_4_0= RULE_IDENTIFIER ) ) (this_LCBRACKET_5= RULE_LCBRACKET ( (lv_content_6_0= ruleGenericSpecifiedDeclaration ) )+ this_RCBRACKET_7= RULE_RCBRACKET )? ) ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1745:2: ( (lv_kind_0_0= ruleStructOrUnion ) ) ( (this_LCBRACKET_1= RULE_LCBRACKET ( (lv_content_2_0= ruleGenericSpecifiedDeclaration ) )+ this_RCBRACKET_3= RULE_RCBRACKET ) | ( ( (lv_name_4_0= RULE_IDENTIFIER ) ) (this_LCBRACKET_5= RULE_LCBRACKET ( (lv_content_6_0= ruleGenericSpecifiedDeclaration ) )+ this_RCBRACKET_7= RULE_RCBRACKET )? ) )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1745:2: ( (lv_kind_0_0= ruleStructOrUnion ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1746:1: (lv_kind_0_0= ruleStructOrUnion )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1746:1: (lv_kind_0_0= ruleStructOrUnion )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1747:3: lv_kind_0_0= ruleStructOrUnion
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getStructOrUnionTypeAccess().getKindStructOrUnionParserRuleCall_0_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleStructOrUnion_in_ruleStructOrUnionType3578);
            lv_kind_0_0=ruleStructOrUnion();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getStructOrUnionTypeRule());
              	        }
                     		set(
                     			current, 
                     			"kind",
                      		lv_kind_0_0, 
                      		"StructOrUnion");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1763:2: ( (this_LCBRACKET_1= RULE_LCBRACKET ( (lv_content_2_0= ruleGenericSpecifiedDeclaration ) )+ this_RCBRACKET_3= RULE_RCBRACKET ) | ( ( (lv_name_4_0= RULE_IDENTIFIER ) ) (this_LCBRACKET_5= RULE_LCBRACKET ( (lv_content_6_0= ruleGenericSpecifiedDeclaration ) )+ this_RCBRACKET_7= RULE_RCBRACKET )? ) )
            int alt35=2;
            int LA35_0 = input.LA(1);

            if ( (LA35_0==RULE_LCBRACKET) ) {
                alt35=1;
            }
            else if ( (LA35_0==RULE_IDENTIFIER) ) {
                alt35=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 35, 0, input);

                throw nvae;
            }
            switch (alt35) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1763:3: (this_LCBRACKET_1= RULE_LCBRACKET ( (lv_content_2_0= ruleGenericSpecifiedDeclaration ) )+ this_RCBRACKET_3= RULE_RCBRACKET )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1763:3: (this_LCBRACKET_1= RULE_LCBRACKET ( (lv_content_2_0= ruleGenericSpecifiedDeclaration ) )+ this_RCBRACKET_3= RULE_RCBRACKET )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1763:4: this_LCBRACKET_1= RULE_LCBRACKET ( (lv_content_2_0= ruleGenericSpecifiedDeclaration ) )+ this_RCBRACKET_3= RULE_RCBRACKET
                    {
                    this_LCBRACKET_1=(Token)match(input,RULE_LCBRACKET,FOLLOW_RULE_LCBRACKET_in_ruleStructOrUnionType3591); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_LCBRACKET_1, grammarAccess.getStructOrUnionTypeAccess().getLCBRACKETTerminalRuleCall_1_0_0()); 
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1767:1: ( (lv_content_2_0= ruleGenericSpecifiedDeclaration ) )+
                    int cnt32=0;
                    loop32:
                    do {
                        int alt32=2;
                        int LA32_0 = input.LA(1);

                        if ( ((LA32_0>=RULE_ATOMIC && LA32_0<=RULE_IMAGINARY)||(LA32_0>=RULE_STRUCT && LA32_0<=RULE_ALIGNAS)) ) {
                            alt32=1;
                        }


                        switch (alt32) {
                    	case 1 :
                    	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1768:1: (lv_content_2_0= ruleGenericSpecifiedDeclaration )
                    	    {
                    	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1768:1: (lv_content_2_0= ruleGenericSpecifiedDeclaration )
                    	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1769:3: lv_content_2_0= ruleGenericSpecifiedDeclaration
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getStructOrUnionTypeAccess().getContentGenericSpecifiedDeclarationParserRuleCall_1_0_1_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FOLLOW_ruleGenericSpecifiedDeclaration_in_ruleStructOrUnionType3611);
                    	    lv_content_2_0=ruleGenericSpecifiedDeclaration();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getStructOrUnionTypeRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"content",
                    	              		lv_content_2_0, 
                    	              		"GenericSpecifiedDeclaration");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt32 >= 1 ) break loop32;
                    	    if (state.backtracking>0) {state.failed=true; return current;}
                                EarlyExitException eee =
                                    new EarlyExitException(32, input);
                                throw eee;
                        }
                        cnt32++;
                    } while (true);

                    this_RCBRACKET_3=(Token)match(input,RULE_RCBRACKET,FOLLOW_RULE_RCBRACKET_in_ruleStructOrUnionType3623); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_RCBRACKET_3, grammarAccess.getStructOrUnionTypeAccess().getRCBRACKETTerminalRuleCall_1_0_2()); 
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1790:6: ( ( (lv_name_4_0= RULE_IDENTIFIER ) ) (this_LCBRACKET_5= RULE_LCBRACKET ( (lv_content_6_0= ruleGenericSpecifiedDeclaration ) )+ this_RCBRACKET_7= RULE_RCBRACKET )? )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1790:6: ( ( (lv_name_4_0= RULE_IDENTIFIER ) ) (this_LCBRACKET_5= RULE_LCBRACKET ( (lv_content_6_0= ruleGenericSpecifiedDeclaration ) )+ this_RCBRACKET_7= RULE_RCBRACKET )? )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1790:7: ( (lv_name_4_0= RULE_IDENTIFIER ) ) (this_LCBRACKET_5= RULE_LCBRACKET ( (lv_content_6_0= ruleGenericSpecifiedDeclaration ) )+ this_RCBRACKET_7= RULE_RCBRACKET )?
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1790:7: ( (lv_name_4_0= RULE_IDENTIFIER ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1791:1: (lv_name_4_0= RULE_IDENTIFIER )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1791:1: (lv_name_4_0= RULE_IDENTIFIER )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1792:3: lv_name_4_0= RULE_IDENTIFIER
                    {
                    lv_name_4_0=(Token)match(input,RULE_IDENTIFIER,FOLLOW_RULE_IDENTIFIER_in_ruleStructOrUnionType3647); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_name_4_0, grammarAccess.getStructOrUnionTypeAccess().getNameIDENTIFIERTerminalRuleCall_1_1_0_0()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getStructOrUnionTypeRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"name",
                              		lv_name_4_0, 
                              		"IDENTIFIER");
                      	    
                    }

                    }


                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1808:2: (this_LCBRACKET_5= RULE_LCBRACKET ( (lv_content_6_0= ruleGenericSpecifiedDeclaration ) )+ this_RCBRACKET_7= RULE_RCBRACKET )?
                    int alt34=2;
                    int LA34_0 = input.LA(1);

                    if ( (LA34_0==RULE_LCBRACKET) ) {
                        alt34=1;
                    }
                    switch (alt34) {
                        case 1 :
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1808:3: this_LCBRACKET_5= RULE_LCBRACKET ( (lv_content_6_0= ruleGenericSpecifiedDeclaration ) )+ this_RCBRACKET_7= RULE_RCBRACKET
                            {
                            this_LCBRACKET_5=(Token)match(input,RULE_LCBRACKET,FOLLOW_RULE_LCBRACKET_in_ruleStructOrUnionType3664); if (state.failed) return current;
                            if ( state.backtracking==0 ) {
                               
                                  newLeafNode(this_LCBRACKET_5, grammarAccess.getStructOrUnionTypeAccess().getLCBRACKETTerminalRuleCall_1_1_1_0()); 
                                  
                            }
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1812:1: ( (lv_content_6_0= ruleGenericSpecifiedDeclaration ) )+
                            int cnt33=0;
                            loop33:
                            do {
                                int alt33=2;
                                int LA33_0 = input.LA(1);

                                if ( ((LA33_0>=RULE_ATOMIC && LA33_0<=RULE_IMAGINARY)||(LA33_0>=RULE_STRUCT && LA33_0<=RULE_ALIGNAS)) ) {
                                    alt33=1;
                                }


                                switch (alt33) {
                            	case 1 :
                            	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1813:1: (lv_content_6_0= ruleGenericSpecifiedDeclaration )
                            	    {
                            	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1813:1: (lv_content_6_0= ruleGenericSpecifiedDeclaration )
                            	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1814:3: lv_content_6_0= ruleGenericSpecifiedDeclaration
                            	    {
                            	    if ( state.backtracking==0 ) {
                            	       
                            	      	        newCompositeNode(grammarAccess.getStructOrUnionTypeAccess().getContentGenericSpecifiedDeclarationParserRuleCall_1_1_1_1_0()); 
                            	      	    
                            	    }
                            	    pushFollow(FOLLOW_ruleGenericSpecifiedDeclaration_in_ruleStructOrUnionType3684);
                            	    lv_content_6_0=ruleGenericSpecifiedDeclaration();

                            	    state._fsp--;
                            	    if (state.failed) return current;
                            	    if ( state.backtracking==0 ) {

                            	      	        if (current==null) {
                            	      	            current = createModelElementForParent(grammarAccess.getStructOrUnionTypeRule());
                            	      	        }
                            	             		add(
                            	             			current, 
                            	             			"content",
                            	              		lv_content_6_0, 
                            	              		"GenericSpecifiedDeclaration");
                            	      	        afterParserOrEnumRuleCall();
                            	      	    
                            	    }

                            	    }


                            	    }
                            	    break;

                            	default :
                            	    if ( cnt33 >= 1 ) break loop33;
                            	    if (state.backtracking>0) {state.failed=true; return current;}
                                        EarlyExitException eee =
                                            new EarlyExitException(33, input);
                                        throw eee;
                                }
                                cnt33++;
                            } while (true);

                            this_RCBRACKET_7=(Token)match(input,RULE_RCBRACKET,FOLLOW_RULE_RCBRACKET_in_ruleStructOrUnionType3696); if (state.failed) return current;
                            if ( state.backtracking==0 ) {
                               
                                  newLeafNode(this_RCBRACKET_7, grammarAccess.getStructOrUnionTypeAccess().getRCBRACKETTerminalRuleCall_1_1_1_2()); 
                                  
                            }

                            }
                            break;

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStructOrUnionType"


    // $ANTLR start "entryRuleStructOrUnion"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1842:1: entryRuleStructOrUnion returns [String current=null] : iv_ruleStructOrUnion= ruleStructOrUnion EOF ;
    public final String entryRuleStructOrUnion() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleStructOrUnion = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1843:2: (iv_ruleStructOrUnion= ruleStructOrUnion EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1844:2: iv_ruleStructOrUnion= ruleStructOrUnion EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStructOrUnionRule()); 
            }
            pushFollow(FOLLOW_ruleStructOrUnion_in_entryRuleStructOrUnion3736);
            iv_ruleStructOrUnion=ruleStructOrUnion();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStructOrUnion.getText(); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleStructOrUnion3747); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStructOrUnion"


    // $ANTLR start "ruleStructOrUnion"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1851:1: ruleStructOrUnion returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRUCT_0= RULE_STRUCT | this_UNION_1= RULE_UNION ) ;
    public final AntlrDatatypeRuleToken ruleStructOrUnion() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRUCT_0=null;
        Token this_UNION_1=null;

         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1854:28: ( (this_STRUCT_0= RULE_STRUCT | this_UNION_1= RULE_UNION ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1855:1: (this_STRUCT_0= RULE_STRUCT | this_UNION_1= RULE_UNION )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1855:1: (this_STRUCT_0= RULE_STRUCT | this_UNION_1= RULE_UNION )
            int alt36=2;
            int LA36_0 = input.LA(1);

            if ( (LA36_0==RULE_STRUCT) ) {
                alt36=1;
            }
            else if ( (LA36_0==RULE_UNION) ) {
                alt36=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 36, 0, input);

                throw nvae;
            }
            switch (alt36) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1855:6: this_STRUCT_0= RULE_STRUCT
                    {
                    this_STRUCT_0=(Token)match(input,RULE_STRUCT,FOLLOW_RULE_STRUCT_in_ruleStructOrUnion3787); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		current.merge(this_STRUCT_0);
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_STRUCT_0, grammarAccess.getStructOrUnionAccess().getSTRUCTTerminalRuleCall_0()); 
                          
                    }

                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1863:10: this_UNION_1= RULE_UNION
                    {
                    this_UNION_1=(Token)match(input,RULE_UNION,FOLLOW_RULE_UNION_in_ruleStructOrUnion3813); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		current.merge(this_UNION_1);
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_UNION_1, grammarAccess.getStructOrUnionAccess().getUNIONTerminalRuleCall_1()); 
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStructOrUnion"


    // $ANTLR start "entryRuleEnumType"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1878:1: entryRuleEnumType returns [EObject current=null] : iv_ruleEnumType= ruleEnumType EOF ;
    public final EObject entryRuleEnumType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEnumType = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1879:2: (iv_ruleEnumType= ruleEnumType EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1880:2: iv_ruleEnumType= ruleEnumType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEnumTypeRule()); 
            }
            pushFollow(FOLLOW_ruleEnumType_in_entryRuleEnumType3858);
            iv_ruleEnumType=ruleEnumType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEnumType; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleEnumType3868); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEnumType"


    // $ANTLR start "ruleEnumType"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1887:1: ruleEnumType returns [EObject current=null] : (this_ENUM_0= RULE_ENUM ( (this_LCBRACKET_1= RULE_LCBRACKET ( (lv_content_2_0= ruleEnumeratorList ) ) (this_COMMA_3= RULE_COMMA )? this_RCBRACKET_4= RULE_RCBRACKET ) | ( ( (lv_name_5_0= RULE_IDENTIFIER ) ) (this_LCBRACKET_6= RULE_LCBRACKET ( (lv_content_7_0= ruleEnumeratorList ) ) (this_COMMA_8= RULE_COMMA )? this_RCBRACKET_9= RULE_RCBRACKET )? ) ) ) ;
    public final EObject ruleEnumType() throws RecognitionException {
        EObject current = null;

        Token this_ENUM_0=null;
        Token this_LCBRACKET_1=null;
        Token this_COMMA_3=null;
        Token this_RCBRACKET_4=null;
        Token lv_name_5_0=null;
        Token this_LCBRACKET_6=null;
        Token this_COMMA_8=null;
        Token this_RCBRACKET_9=null;
        EObject lv_content_2_0 = null;

        EObject lv_content_7_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1890:28: ( (this_ENUM_0= RULE_ENUM ( (this_LCBRACKET_1= RULE_LCBRACKET ( (lv_content_2_0= ruleEnumeratorList ) ) (this_COMMA_3= RULE_COMMA )? this_RCBRACKET_4= RULE_RCBRACKET ) | ( ( (lv_name_5_0= RULE_IDENTIFIER ) ) (this_LCBRACKET_6= RULE_LCBRACKET ( (lv_content_7_0= ruleEnumeratorList ) ) (this_COMMA_8= RULE_COMMA )? this_RCBRACKET_9= RULE_RCBRACKET )? ) ) ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1891:1: (this_ENUM_0= RULE_ENUM ( (this_LCBRACKET_1= RULE_LCBRACKET ( (lv_content_2_0= ruleEnumeratorList ) ) (this_COMMA_3= RULE_COMMA )? this_RCBRACKET_4= RULE_RCBRACKET ) | ( ( (lv_name_5_0= RULE_IDENTIFIER ) ) (this_LCBRACKET_6= RULE_LCBRACKET ( (lv_content_7_0= ruleEnumeratorList ) ) (this_COMMA_8= RULE_COMMA )? this_RCBRACKET_9= RULE_RCBRACKET )? ) ) )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1891:1: (this_ENUM_0= RULE_ENUM ( (this_LCBRACKET_1= RULE_LCBRACKET ( (lv_content_2_0= ruleEnumeratorList ) ) (this_COMMA_3= RULE_COMMA )? this_RCBRACKET_4= RULE_RCBRACKET ) | ( ( (lv_name_5_0= RULE_IDENTIFIER ) ) (this_LCBRACKET_6= RULE_LCBRACKET ( (lv_content_7_0= ruleEnumeratorList ) ) (this_COMMA_8= RULE_COMMA )? this_RCBRACKET_9= RULE_RCBRACKET )? ) ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1891:2: this_ENUM_0= RULE_ENUM ( (this_LCBRACKET_1= RULE_LCBRACKET ( (lv_content_2_0= ruleEnumeratorList ) ) (this_COMMA_3= RULE_COMMA )? this_RCBRACKET_4= RULE_RCBRACKET ) | ( ( (lv_name_5_0= RULE_IDENTIFIER ) ) (this_LCBRACKET_6= RULE_LCBRACKET ( (lv_content_7_0= ruleEnumeratorList ) ) (this_COMMA_8= RULE_COMMA )? this_RCBRACKET_9= RULE_RCBRACKET )? ) )
            {
            this_ENUM_0=(Token)match(input,RULE_ENUM,FOLLOW_RULE_ENUM_in_ruleEnumType3904); if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                  newLeafNode(this_ENUM_0, grammarAccess.getEnumTypeAccess().getENUMTerminalRuleCall_0()); 
                  
            }
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1895:1: ( (this_LCBRACKET_1= RULE_LCBRACKET ( (lv_content_2_0= ruleEnumeratorList ) ) (this_COMMA_3= RULE_COMMA )? this_RCBRACKET_4= RULE_RCBRACKET ) | ( ( (lv_name_5_0= RULE_IDENTIFIER ) ) (this_LCBRACKET_6= RULE_LCBRACKET ( (lv_content_7_0= ruleEnumeratorList ) ) (this_COMMA_8= RULE_COMMA )? this_RCBRACKET_9= RULE_RCBRACKET )? ) )
            int alt40=2;
            int LA40_0 = input.LA(1);

            if ( (LA40_0==RULE_LCBRACKET) ) {
                alt40=1;
            }
            else if ( (LA40_0==RULE_IDENTIFIER) ) {
                alt40=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 40, 0, input);

                throw nvae;
            }
            switch (alt40) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1895:2: (this_LCBRACKET_1= RULE_LCBRACKET ( (lv_content_2_0= ruleEnumeratorList ) ) (this_COMMA_3= RULE_COMMA )? this_RCBRACKET_4= RULE_RCBRACKET )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1895:2: (this_LCBRACKET_1= RULE_LCBRACKET ( (lv_content_2_0= ruleEnumeratorList ) ) (this_COMMA_3= RULE_COMMA )? this_RCBRACKET_4= RULE_RCBRACKET )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1895:3: this_LCBRACKET_1= RULE_LCBRACKET ( (lv_content_2_0= ruleEnumeratorList ) ) (this_COMMA_3= RULE_COMMA )? this_RCBRACKET_4= RULE_RCBRACKET
                    {
                    this_LCBRACKET_1=(Token)match(input,RULE_LCBRACKET,FOLLOW_RULE_LCBRACKET_in_ruleEnumType3916); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_LCBRACKET_1, grammarAccess.getEnumTypeAccess().getLCBRACKETTerminalRuleCall_1_0_0()); 
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1899:1: ( (lv_content_2_0= ruleEnumeratorList ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1900:1: (lv_content_2_0= ruleEnumeratorList )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1900:1: (lv_content_2_0= ruleEnumeratorList )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1901:3: lv_content_2_0= ruleEnumeratorList
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getEnumTypeAccess().getContentEnumeratorListParserRuleCall_1_0_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleEnumeratorList_in_ruleEnumType3936);
                    lv_content_2_0=ruleEnumeratorList();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getEnumTypeRule());
                      	        }
                             		set(
                             			current, 
                             			"content",
                              		lv_content_2_0, 
                              		"EnumeratorList");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1917:2: (this_COMMA_3= RULE_COMMA )?
                    int alt37=2;
                    int LA37_0 = input.LA(1);

                    if ( (LA37_0==RULE_COMMA) ) {
                        alt37=1;
                    }
                    switch (alt37) {
                        case 1 :
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1917:3: this_COMMA_3= RULE_COMMA
                            {
                            this_COMMA_3=(Token)match(input,RULE_COMMA,FOLLOW_RULE_COMMA_in_ruleEnumType3948); if (state.failed) return current;
                            if ( state.backtracking==0 ) {
                               
                                  newLeafNode(this_COMMA_3, grammarAccess.getEnumTypeAccess().getCOMMATerminalRuleCall_1_0_2()); 
                                  
                            }

                            }
                            break;

                    }

                    this_RCBRACKET_4=(Token)match(input,RULE_RCBRACKET,FOLLOW_RULE_RCBRACKET_in_ruleEnumType3960); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_RCBRACKET_4, grammarAccess.getEnumTypeAccess().getRCBRACKETTerminalRuleCall_1_0_3()); 
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1926:6: ( ( (lv_name_5_0= RULE_IDENTIFIER ) ) (this_LCBRACKET_6= RULE_LCBRACKET ( (lv_content_7_0= ruleEnumeratorList ) ) (this_COMMA_8= RULE_COMMA )? this_RCBRACKET_9= RULE_RCBRACKET )? )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1926:6: ( ( (lv_name_5_0= RULE_IDENTIFIER ) ) (this_LCBRACKET_6= RULE_LCBRACKET ( (lv_content_7_0= ruleEnumeratorList ) ) (this_COMMA_8= RULE_COMMA )? this_RCBRACKET_9= RULE_RCBRACKET )? )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1926:7: ( (lv_name_5_0= RULE_IDENTIFIER ) ) (this_LCBRACKET_6= RULE_LCBRACKET ( (lv_content_7_0= ruleEnumeratorList ) ) (this_COMMA_8= RULE_COMMA )? this_RCBRACKET_9= RULE_RCBRACKET )?
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1926:7: ( (lv_name_5_0= RULE_IDENTIFIER ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1927:1: (lv_name_5_0= RULE_IDENTIFIER )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1927:1: (lv_name_5_0= RULE_IDENTIFIER )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1928:3: lv_name_5_0= RULE_IDENTIFIER
                    {
                    lv_name_5_0=(Token)match(input,RULE_IDENTIFIER,FOLLOW_RULE_IDENTIFIER_in_ruleEnumType3984); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_name_5_0, grammarAccess.getEnumTypeAccess().getNameIDENTIFIERTerminalRuleCall_1_1_0_0()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getEnumTypeRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"name",
                              		lv_name_5_0, 
                              		"IDENTIFIER");
                      	    
                    }

                    }


                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1944:2: (this_LCBRACKET_6= RULE_LCBRACKET ( (lv_content_7_0= ruleEnumeratorList ) ) (this_COMMA_8= RULE_COMMA )? this_RCBRACKET_9= RULE_RCBRACKET )?
                    int alt39=2;
                    int LA39_0 = input.LA(1);

                    if ( (LA39_0==RULE_LCBRACKET) ) {
                        alt39=1;
                    }
                    switch (alt39) {
                        case 1 :
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1944:3: this_LCBRACKET_6= RULE_LCBRACKET ( (lv_content_7_0= ruleEnumeratorList ) ) (this_COMMA_8= RULE_COMMA )? this_RCBRACKET_9= RULE_RCBRACKET
                            {
                            this_LCBRACKET_6=(Token)match(input,RULE_LCBRACKET,FOLLOW_RULE_LCBRACKET_in_ruleEnumType4001); if (state.failed) return current;
                            if ( state.backtracking==0 ) {
                               
                                  newLeafNode(this_LCBRACKET_6, grammarAccess.getEnumTypeAccess().getLCBRACKETTerminalRuleCall_1_1_1_0()); 
                                  
                            }
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1948:1: ( (lv_content_7_0= ruleEnumeratorList ) )
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1949:1: (lv_content_7_0= ruleEnumeratorList )
                            {
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1949:1: (lv_content_7_0= ruleEnumeratorList )
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1950:3: lv_content_7_0= ruleEnumeratorList
                            {
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getEnumTypeAccess().getContentEnumeratorListParserRuleCall_1_1_1_1_0()); 
                              	    
                            }
                            pushFollow(FOLLOW_ruleEnumeratorList_in_ruleEnumType4021);
                            lv_content_7_0=ruleEnumeratorList();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElementForParent(grammarAccess.getEnumTypeRule());
                              	        }
                                     		set(
                                     			current, 
                                     			"content",
                                      		lv_content_7_0, 
                                      		"EnumeratorList");
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }


                            }

                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1966:2: (this_COMMA_8= RULE_COMMA )?
                            int alt38=2;
                            int LA38_0 = input.LA(1);

                            if ( (LA38_0==RULE_COMMA) ) {
                                alt38=1;
                            }
                            switch (alt38) {
                                case 1 :
                                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1966:3: this_COMMA_8= RULE_COMMA
                                    {
                                    this_COMMA_8=(Token)match(input,RULE_COMMA,FOLLOW_RULE_COMMA_in_ruleEnumType4033); if (state.failed) return current;
                                    if ( state.backtracking==0 ) {
                                       
                                          newLeafNode(this_COMMA_8, grammarAccess.getEnumTypeAccess().getCOMMATerminalRuleCall_1_1_1_2()); 
                                          
                                    }

                                    }
                                    break;

                            }

                            this_RCBRACKET_9=(Token)match(input,RULE_RCBRACKET,FOLLOW_RULE_RCBRACKET_in_ruleEnumType4045); if (state.failed) return current;
                            if ( state.backtracking==0 ) {
                               
                                  newLeafNode(this_RCBRACKET_9, grammarAccess.getEnumTypeAccess().getRCBRACKETTerminalRuleCall_1_1_1_3()); 
                                  
                            }

                            }
                            break;

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEnumType"


    // $ANTLR start "entryRuleEnumeratorList"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1982:1: entryRuleEnumeratorList returns [EObject current=null] : iv_ruleEnumeratorList= ruleEnumeratorList EOF ;
    public final EObject entryRuleEnumeratorList() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEnumeratorList = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1983:2: (iv_ruleEnumeratorList= ruleEnumeratorList EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1984:2: iv_ruleEnumeratorList= ruleEnumeratorList EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEnumeratorListRule()); 
            }
            pushFollow(FOLLOW_ruleEnumeratorList_in_entryRuleEnumeratorList4084);
            iv_ruleEnumeratorList=ruleEnumeratorList();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEnumeratorList; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleEnumeratorList4094); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEnumeratorList"


    // $ANTLR start "ruleEnumeratorList"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1991:1: ruleEnumeratorList returns [EObject current=null] : ( ( (lv_elements_0_0= ruleEnumerator ) ) (this_COMMA_1= RULE_COMMA ( (lv_elements_2_0= ruleEnumerator ) ) )* ) ;
    public final EObject ruleEnumeratorList() throws RecognitionException {
        EObject current = null;

        Token this_COMMA_1=null;
        EObject lv_elements_0_0 = null;

        EObject lv_elements_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1994:28: ( ( ( (lv_elements_0_0= ruleEnumerator ) ) (this_COMMA_1= RULE_COMMA ( (lv_elements_2_0= ruleEnumerator ) ) )* ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1995:1: ( ( (lv_elements_0_0= ruleEnumerator ) ) (this_COMMA_1= RULE_COMMA ( (lv_elements_2_0= ruleEnumerator ) ) )* )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1995:1: ( ( (lv_elements_0_0= ruleEnumerator ) ) (this_COMMA_1= RULE_COMMA ( (lv_elements_2_0= ruleEnumerator ) ) )* )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1995:2: ( (lv_elements_0_0= ruleEnumerator ) ) (this_COMMA_1= RULE_COMMA ( (lv_elements_2_0= ruleEnumerator ) ) )*
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1995:2: ( (lv_elements_0_0= ruleEnumerator ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1996:1: (lv_elements_0_0= ruleEnumerator )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1996:1: (lv_elements_0_0= ruleEnumerator )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:1997:3: lv_elements_0_0= ruleEnumerator
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getEnumeratorListAccess().getElementsEnumeratorParserRuleCall_0_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleEnumerator_in_ruleEnumeratorList4140);
            lv_elements_0_0=ruleEnumerator();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getEnumeratorListRule());
              	        }
                     		add(
                     			current, 
                     			"elements",
                      		lv_elements_0_0, 
                      		"Enumerator");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2013:2: (this_COMMA_1= RULE_COMMA ( (lv_elements_2_0= ruleEnumerator ) ) )*
            loop41:
            do {
                int alt41=2;
                int LA41_0 = input.LA(1);

                if ( (LA41_0==RULE_COMMA) ) {
                    int LA41_1 = input.LA(2);

                    if ( (LA41_1==RULE_IDENTIFIER) ) {
                        alt41=1;
                    }


                }


                switch (alt41) {
            	case 1 :
            	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2013:3: this_COMMA_1= RULE_COMMA ( (lv_elements_2_0= ruleEnumerator ) )
            	    {
            	    this_COMMA_1=(Token)match(input,RULE_COMMA,FOLLOW_RULE_COMMA_in_ruleEnumeratorList4152); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {
            	       
            	          newLeafNode(this_COMMA_1, grammarAccess.getEnumeratorListAccess().getCOMMATerminalRuleCall_1_0()); 
            	          
            	    }
            	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2017:1: ( (lv_elements_2_0= ruleEnumerator ) )
            	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2018:1: (lv_elements_2_0= ruleEnumerator )
            	    {
            	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2018:1: (lv_elements_2_0= ruleEnumerator )
            	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2019:3: lv_elements_2_0= ruleEnumerator
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getEnumeratorListAccess().getElementsEnumeratorParserRuleCall_1_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleEnumerator_in_ruleEnumeratorList4172);
            	    lv_elements_2_0=ruleEnumerator();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getEnumeratorListRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"elements",
            	              		lv_elements_2_0, 
            	              		"Enumerator");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop41;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEnumeratorList"


    // $ANTLR start "entryRuleEnumerator"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2043:1: entryRuleEnumerator returns [EObject current=null] : iv_ruleEnumerator= ruleEnumerator EOF ;
    public final EObject entryRuleEnumerator() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEnumerator = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2044:2: (iv_ruleEnumerator= ruleEnumerator EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2045:2: iv_ruleEnumerator= ruleEnumerator EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEnumeratorRule()); 
            }
            pushFollow(FOLLOW_ruleEnumerator_in_entryRuleEnumerator4210);
            iv_ruleEnumerator=ruleEnumerator();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEnumerator; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleEnumerator4220); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEnumerator"


    // $ANTLR start "ruleEnumerator"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2052:1: ruleEnumerator returns [EObject current=null] : ( ( (lv_name_0_0= ruleEnumerationConstant ) ) (this_ASSIGN_1= RULE_ASSIGN ( (lv_exp_2_0= ruleBaseExpression ) ) )? ) ;
    public final EObject ruleEnumerator() throws RecognitionException {
        EObject current = null;

        Token this_ASSIGN_1=null;
        EObject lv_name_0_0 = null;

        EObject lv_exp_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2055:28: ( ( ( (lv_name_0_0= ruleEnumerationConstant ) ) (this_ASSIGN_1= RULE_ASSIGN ( (lv_exp_2_0= ruleBaseExpression ) ) )? ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2056:1: ( ( (lv_name_0_0= ruleEnumerationConstant ) ) (this_ASSIGN_1= RULE_ASSIGN ( (lv_exp_2_0= ruleBaseExpression ) ) )? )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2056:1: ( ( (lv_name_0_0= ruleEnumerationConstant ) ) (this_ASSIGN_1= RULE_ASSIGN ( (lv_exp_2_0= ruleBaseExpression ) ) )? )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2056:2: ( (lv_name_0_0= ruleEnumerationConstant ) ) (this_ASSIGN_1= RULE_ASSIGN ( (lv_exp_2_0= ruleBaseExpression ) ) )?
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2056:2: ( (lv_name_0_0= ruleEnumerationConstant ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2057:1: (lv_name_0_0= ruleEnumerationConstant )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2057:1: (lv_name_0_0= ruleEnumerationConstant )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2058:3: lv_name_0_0= ruleEnumerationConstant
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getEnumeratorAccess().getNameEnumerationConstantParserRuleCall_0_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleEnumerationConstant_in_ruleEnumerator4266);
            lv_name_0_0=ruleEnumerationConstant();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getEnumeratorRule());
              	        }
                     		set(
                     			current, 
                     			"name",
                      		lv_name_0_0, 
                      		"EnumerationConstant");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2074:2: (this_ASSIGN_1= RULE_ASSIGN ( (lv_exp_2_0= ruleBaseExpression ) ) )?
            int alt42=2;
            int LA42_0 = input.LA(1);

            if ( (LA42_0==RULE_ASSIGN) ) {
                alt42=1;
            }
            switch (alt42) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2074:3: this_ASSIGN_1= RULE_ASSIGN ( (lv_exp_2_0= ruleBaseExpression ) )
                    {
                    this_ASSIGN_1=(Token)match(input,RULE_ASSIGN,FOLLOW_RULE_ASSIGN_in_ruleEnumerator4278); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_ASSIGN_1, grammarAccess.getEnumeratorAccess().getASSIGNTerminalRuleCall_1_0()); 
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2078:1: ( (lv_exp_2_0= ruleBaseExpression ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2079:1: (lv_exp_2_0= ruleBaseExpression )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2079:1: (lv_exp_2_0= ruleBaseExpression )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2080:3: lv_exp_2_0= ruleBaseExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getEnumeratorAccess().getExpBaseExpressionParserRuleCall_1_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleBaseExpression_in_ruleEnumerator4298);
                    lv_exp_2_0=ruleBaseExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getEnumeratorRule());
                      	        }
                             		set(
                             			current, 
                             			"exp",
                              		lv_exp_2_0, 
                              		"BaseExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEnumerator"


    // $ANTLR start "entryRuleEnumerationConstant"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2104:1: entryRuleEnumerationConstant returns [EObject current=null] : iv_ruleEnumerationConstant= ruleEnumerationConstant EOF ;
    public final EObject entryRuleEnumerationConstant() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEnumerationConstant = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2105:2: (iv_ruleEnumerationConstant= ruleEnumerationConstant EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2106:2: iv_ruleEnumerationConstant= ruleEnumerationConstant EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEnumerationConstantRule()); 
            }
            pushFollow(FOLLOW_ruleEnumerationConstant_in_entryRuleEnumerationConstant4336);
            iv_ruleEnumerationConstant=ruleEnumerationConstant();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEnumerationConstant; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleEnumerationConstant4346); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEnumerationConstant"


    // $ANTLR start "ruleEnumerationConstant"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2113:1: ruleEnumerationConstant returns [EObject current=null] : ( (lv_name_0_0= RULE_IDENTIFIER ) ) ;
    public final EObject ruleEnumerationConstant() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;

         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2116:28: ( ( (lv_name_0_0= RULE_IDENTIFIER ) ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2117:1: ( (lv_name_0_0= RULE_IDENTIFIER ) )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2117:1: ( (lv_name_0_0= RULE_IDENTIFIER ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2118:1: (lv_name_0_0= RULE_IDENTIFIER )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2118:1: (lv_name_0_0= RULE_IDENTIFIER )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2119:3: lv_name_0_0= RULE_IDENTIFIER
            {
            lv_name_0_0=(Token)match(input,RULE_IDENTIFIER,FOLLOW_RULE_IDENTIFIER_in_ruleEnumerationConstant4387); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_0_0, grammarAccess.getEnumerationConstantAccess().getNameIDENTIFIERTerminalRuleCall_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getEnumerationConstantRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_0_0, 
                      		"IDENTIFIER");
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEnumerationConstant"


    // $ANTLR start "entryRuleDeclarationSpecifier"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2143:1: entryRuleDeclarationSpecifier returns [EObject current=null] : iv_ruleDeclarationSpecifier= ruleDeclarationSpecifier EOF ;
    public final EObject entryRuleDeclarationSpecifier() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDeclarationSpecifier = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2144:2: (iv_ruleDeclarationSpecifier= ruleDeclarationSpecifier EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2145:2: iv_ruleDeclarationSpecifier= ruleDeclarationSpecifier EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getDeclarationSpecifierRule()); 
            }
            pushFollow(FOLLOW_ruleDeclarationSpecifier_in_entryRuleDeclarationSpecifier4427);
            iv_ruleDeclarationSpecifier=ruleDeclarationSpecifier();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleDeclarationSpecifier; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleDeclarationSpecifier4437); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDeclarationSpecifier"


    // $ANTLR start "ruleDeclarationSpecifier"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2152:1: ruleDeclarationSpecifier returns [EObject current=null] : (this_StorageClassSpecifier_0= ruleStorageClassSpecifier | this_TypeQualifier_1= ruleTypeQualifier | this_FunctionSpecifier_2= ruleFunctionSpecifier | this_AlignmentSpecifier_3= ruleAlignmentSpecifier ) ;
    public final EObject ruleDeclarationSpecifier() throws RecognitionException {
        EObject current = null;

        EObject this_StorageClassSpecifier_0 = null;

        EObject this_TypeQualifier_1 = null;

        EObject this_FunctionSpecifier_2 = null;

        EObject this_AlignmentSpecifier_3 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2155:28: ( (this_StorageClassSpecifier_0= ruleStorageClassSpecifier | this_TypeQualifier_1= ruleTypeQualifier | this_FunctionSpecifier_2= ruleFunctionSpecifier | this_AlignmentSpecifier_3= ruleAlignmentSpecifier ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2156:1: (this_StorageClassSpecifier_0= ruleStorageClassSpecifier | this_TypeQualifier_1= ruleTypeQualifier | this_FunctionSpecifier_2= ruleFunctionSpecifier | this_AlignmentSpecifier_3= ruleAlignmentSpecifier )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2156:1: (this_StorageClassSpecifier_0= ruleStorageClassSpecifier | this_TypeQualifier_1= ruleTypeQualifier | this_FunctionSpecifier_2= ruleFunctionSpecifier | this_AlignmentSpecifier_3= ruleAlignmentSpecifier )
            int alt43=4;
            switch ( input.LA(1) ) {
            case RULE_EXTERN:
            case RULE_STATIC:
            case RULE_THREAD_LOCAL:
            case RULE_AUTO:
            case RULE_REGISTER:
                {
                alt43=1;
                }
                break;
            case RULE_ATOMIC:
            case RULE_CONST:
            case RULE_VOLATILE:
                {
                alt43=2;
                }
                break;
            case RULE_INLINE:
            case RULE_NORETURN:
                {
                alt43=3;
                }
                break;
            case RULE_ALIGNAS:
                {
                alt43=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 43, 0, input);

                throw nvae;
            }

            switch (alt43) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2157:5: this_StorageClassSpecifier_0= ruleStorageClassSpecifier
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getDeclarationSpecifierAccess().getStorageClassSpecifierParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleStorageClassSpecifier_in_ruleDeclarationSpecifier4484);
                    this_StorageClassSpecifier_0=ruleStorageClassSpecifier();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_StorageClassSpecifier_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2167:5: this_TypeQualifier_1= ruleTypeQualifier
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getDeclarationSpecifierAccess().getTypeQualifierParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleTypeQualifier_in_ruleDeclarationSpecifier4511);
                    this_TypeQualifier_1=ruleTypeQualifier();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_TypeQualifier_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2177:5: this_FunctionSpecifier_2= ruleFunctionSpecifier
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getDeclarationSpecifierAccess().getFunctionSpecifierParserRuleCall_2()); 
                          
                    }
                    pushFollow(FOLLOW_ruleFunctionSpecifier_in_ruleDeclarationSpecifier4538);
                    this_FunctionSpecifier_2=ruleFunctionSpecifier();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_FunctionSpecifier_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 4 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2187:5: this_AlignmentSpecifier_3= ruleAlignmentSpecifier
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getDeclarationSpecifierAccess().getAlignmentSpecifierParserRuleCall_3()); 
                          
                    }
                    pushFollow(FOLLOW_ruleAlignmentSpecifier_in_ruleDeclarationSpecifier4565);
                    this_AlignmentSpecifier_3=ruleAlignmentSpecifier();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_AlignmentSpecifier_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDeclarationSpecifier"


    // $ANTLR start "entryRuleStorageClassSpecifier"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2203:1: entryRuleStorageClassSpecifier returns [EObject current=null] : iv_ruleStorageClassSpecifier= ruleStorageClassSpecifier EOF ;
    public final EObject entryRuleStorageClassSpecifier() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStorageClassSpecifier = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2204:2: (iv_ruleStorageClassSpecifier= ruleStorageClassSpecifier EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2205:2: iv_ruleStorageClassSpecifier= ruleStorageClassSpecifier EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStorageClassSpecifierRule()); 
            }
            pushFollow(FOLLOW_ruleStorageClassSpecifier_in_entryRuleStorageClassSpecifier4600);
            iv_ruleStorageClassSpecifier=ruleStorageClassSpecifier();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStorageClassSpecifier; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleStorageClassSpecifier4610); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStorageClassSpecifier"


    // $ANTLR start "ruleStorageClassSpecifier"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2212:1: ruleStorageClassSpecifier returns [EObject current=null] : ( ( () this_EXTERN_1= RULE_EXTERN ) | ( () this_STATIC_3= RULE_STATIC ) | ( () this_THREAD_LOCAL_5= RULE_THREAD_LOCAL ) | ( () this_AUTO_7= RULE_AUTO ) | ( () this_REGISTER_9= RULE_REGISTER ) ) ;
    public final EObject ruleStorageClassSpecifier() throws RecognitionException {
        EObject current = null;

        Token this_EXTERN_1=null;
        Token this_STATIC_3=null;
        Token this_THREAD_LOCAL_5=null;
        Token this_AUTO_7=null;
        Token this_REGISTER_9=null;

         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2215:28: ( ( ( () this_EXTERN_1= RULE_EXTERN ) | ( () this_STATIC_3= RULE_STATIC ) | ( () this_THREAD_LOCAL_5= RULE_THREAD_LOCAL ) | ( () this_AUTO_7= RULE_AUTO ) | ( () this_REGISTER_9= RULE_REGISTER ) ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2216:1: ( ( () this_EXTERN_1= RULE_EXTERN ) | ( () this_STATIC_3= RULE_STATIC ) | ( () this_THREAD_LOCAL_5= RULE_THREAD_LOCAL ) | ( () this_AUTO_7= RULE_AUTO ) | ( () this_REGISTER_9= RULE_REGISTER ) )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2216:1: ( ( () this_EXTERN_1= RULE_EXTERN ) | ( () this_STATIC_3= RULE_STATIC ) | ( () this_THREAD_LOCAL_5= RULE_THREAD_LOCAL ) | ( () this_AUTO_7= RULE_AUTO ) | ( () this_REGISTER_9= RULE_REGISTER ) )
            int alt44=5;
            switch ( input.LA(1) ) {
            case RULE_EXTERN:
                {
                alt44=1;
                }
                break;
            case RULE_STATIC:
                {
                alt44=2;
                }
                break;
            case RULE_THREAD_LOCAL:
                {
                alt44=3;
                }
                break;
            case RULE_AUTO:
                {
                alt44=4;
                }
                break;
            case RULE_REGISTER:
                {
                alt44=5;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 44, 0, input);

                throw nvae;
            }

            switch (alt44) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2216:2: ( () this_EXTERN_1= RULE_EXTERN )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2216:2: ( () this_EXTERN_1= RULE_EXTERN )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2216:3: () this_EXTERN_1= RULE_EXTERN
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2216:3: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2217:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getStorageClassSpecifierAccess().getStorageClassSpecifierExternAction_0_0(),
                                  current);
                          
                    }

                    }

                    this_EXTERN_1=(Token)match(input,RULE_EXTERN,FOLLOW_RULE_EXTERN_in_ruleStorageClassSpecifier4656); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_EXTERN_1, grammarAccess.getStorageClassSpecifierAccess().getEXTERNTerminalRuleCall_0_1()); 
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2227:6: ( () this_STATIC_3= RULE_STATIC )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2227:6: ( () this_STATIC_3= RULE_STATIC )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2227:7: () this_STATIC_3= RULE_STATIC
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2227:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2228:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getStorageClassSpecifierAccess().getStorageClassSpecifierStaticAction_1_0(),
                                  current);
                          
                    }

                    }

                    this_STATIC_3=(Token)match(input,RULE_STATIC,FOLLOW_RULE_STATIC_in_ruleStorageClassSpecifier4683); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_STATIC_3, grammarAccess.getStorageClassSpecifierAccess().getSTATICTerminalRuleCall_1_1()); 
                          
                    }

                    }


                    }
                    break;
                case 3 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2238:6: ( () this_THREAD_LOCAL_5= RULE_THREAD_LOCAL )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2238:6: ( () this_THREAD_LOCAL_5= RULE_THREAD_LOCAL )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2238:7: () this_THREAD_LOCAL_5= RULE_THREAD_LOCAL
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2238:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2239:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getStorageClassSpecifierAccess().getStorageClassSpecifierThreadLocalAction_2_0(),
                                  current);
                          
                    }

                    }

                    this_THREAD_LOCAL_5=(Token)match(input,RULE_THREAD_LOCAL,FOLLOW_RULE_THREAD_LOCAL_in_ruleStorageClassSpecifier4710); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_THREAD_LOCAL_5, grammarAccess.getStorageClassSpecifierAccess().getTHREAD_LOCALTerminalRuleCall_2_1()); 
                          
                    }

                    }


                    }
                    break;
                case 4 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2249:6: ( () this_AUTO_7= RULE_AUTO )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2249:6: ( () this_AUTO_7= RULE_AUTO )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2249:7: () this_AUTO_7= RULE_AUTO
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2249:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2250:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getStorageClassSpecifierAccess().getStorageClassSpecifierAutoAction_3_0(),
                                  current);
                          
                    }

                    }

                    this_AUTO_7=(Token)match(input,RULE_AUTO,FOLLOW_RULE_AUTO_in_ruleStorageClassSpecifier4737); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_AUTO_7, grammarAccess.getStorageClassSpecifierAccess().getAUTOTerminalRuleCall_3_1()); 
                          
                    }

                    }


                    }
                    break;
                case 5 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2260:6: ( () this_REGISTER_9= RULE_REGISTER )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2260:6: ( () this_REGISTER_9= RULE_REGISTER )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2260:7: () this_REGISTER_9= RULE_REGISTER
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2260:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2261:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getStorageClassSpecifierAccess().getStorageClassSpecifierRegisterAction_4_0(),
                                  current);
                          
                    }

                    }

                    this_REGISTER_9=(Token)match(input,RULE_REGISTER,FOLLOW_RULE_REGISTER_in_ruleStorageClassSpecifier4764); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_REGISTER_9, grammarAccess.getStorageClassSpecifierAccess().getREGISTERTerminalRuleCall_4_1()); 
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStorageClassSpecifier"


    // $ANTLR start "entryRuleTypeQualifier"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2278:1: entryRuleTypeQualifier returns [EObject current=null] : iv_ruleTypeQualifier= ruleTypeQualifier EOF ;
    public final EObject entryRuleTypeQualifier() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTypeQualifier = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2279:2: (iv_ruleTypeQualifier= ruleTypeQualifier EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2280:2: iv_ruleTypeQualifier= ruleTypeQualifier EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTypeQualifierRule()); 
            }
            pushFollow(FOLLOW_ruleTypeQualifier_in_entryRuleTypeQualifier4800);
            iv_ruleTypeQualifier=ruleTypeQualifier();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTypeQualifier; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleTypeQualifier4810); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTypeQualifier"


    // $ANTLR start "ruleTypeQualifier"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2287:1: ruleTypeQualifier returns [EObject current=null] : ( ( () this_CONST_1= RULE_CONST ) | ( () this_VOLATILE_3= RULE_VOLATILE ) | ( () this_ATOMIC_5= RULE_ATOMIC ) ) ;
    public final EObject ruleTypeQualifier() throws RecognitionException {
        EObject current = null;

        Token this_CONST_1=null;
        Token this_VOLATILE_3=null;
        Token this_ATOMIC_5=null;

         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2290:28: ( ( ( () this_CONST_1= RULE_CONST ) | ( () this_VOLATILE_3= RULE_VOLATILE ) | ( () this_ATOMIC_5= RULE_ATOMIC ) ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2291:1: ( ( () this_CONST_1= RULE_CONST ) | ( () this_VOLATILE_3= RULE_VOLATILE ) | ( () this_ATOMIC_5= RULE_ATOMIC ) )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2291:1: ( ( () this_CONST_1= RULE_CONST ) | ( () this_VOLATILE_3= RULE_VOLATILE ) | ( () this_ATOMIC_5= RULE_ATOMIC ) )
            int alt45=3;
            switch ( input.LA(1) ) {
            case RULE_CONST:
                {
                alt45=1;
                }
                break;
            case RULE_VOLATILE:
                {
                alt45=2;
                }
                break;
            case RULE_ATOMIC:
                {
                alt45=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 45, 0, input);

                throw nvae;
            }

            switch (alt45) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2291:2: ( () this_CONST_1= RULE_CONST )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2291:2: ( () this_CONST_1= RULE_CONST )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2291:3: () this_CONST_1= RULE_CONST
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2291:3: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2292:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getTypeQualifierAccess().getTypeQualifierConstAction_0_0(),
                                  current);
                          
                    }

                    }

                    this_CONST_1=(Token)match(input,RULE_CONST,FOLLOW_RULE_CONST_in_ruleTypeQualifier4856); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_CONST_1, grammarAccess.getTypeQualifierAccess().getCONSTTerminalRuleCall_0_1()); 
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2302:6: ( () this_VOLATILE_3= RULE_VOLATILE )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2302:6: ( () this_VOLATILE_3= RULE_VOLATILE )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2302:7: () this_VOLATILE_3= RULE_VOLATILE
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2302:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2303:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getTypeQualifierAccess().getTypeQualifierVolatileAction_1_0(),
                                  current);
                          
                    }

                    }

                    this_VOLATILE_3=(Token)match(input,RULE_VOLATILE,FOLLOW_RULE_VOLATILE_in_ruleTypeQualifier4883); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_VOLATILE_3, grammarAccess.getTypeQualifierAccess().getVOLATILETerminalRuleCall_1_1()); 
                          
                    }

                    }


                    }
                    break;
                case 3 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2313:6: ( () this_ATOMIC_5= RULE_ATOMIC )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2313:6: ( () this_ATOMIC_5= RULE_ATOMIC )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2313:7: () this_ATOMIC_5= RULE_ATOMIC
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2313:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2314:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getTypeQualifierAccess().getTypeQualifierAtomicAction_2_0(),
                                  current);
                          
                    }

                    }

                    this_ATOMIC_5=(Token)match(input,RULE_ATOMIC,FOLLOW_RULE_ATOMIC_in_ruleTypeQualifier4910); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_ATOMIC_5, grammarAccess.getTypeQualifierAccess().getATOMICTerminalRuleCall_2_1()); 
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTypeQualifier"


    // $ANTLR start "entryRuleFunctionSpecifier"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2331:1: entryRuleFunctionSpecifier returns [EObject current=null] : iv_ruleFunctionSpecifier= ruleFunctionSpecifier EOF ;
    public final EObject entryRuleFunctionSpecifier() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFunctionSpecifier = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2332:2: (iv_ruleFunctionSpecifier= ruleFunctionSpecifier EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2333:2: iv_ruleFunctionSpecifier= ruleFunctionSpecifier EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFunctionSpecifierRule()); 
            }
            pushFollow(FOLLOW_ruleFunctionSpecifier_in_entryRuleFunctionSpecifier4946);
            iv_ruleFunctionSpecifier=ruleFunctionSpecifier();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFunctionSpecifier; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleFunctionSpecifier4956); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFunctionSpecifier"


    // $ANTLR start "ruleFunctionSpecifier"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2340:1: ruleFunctionSpecifier returns [EObject current=null] : ( ( () this_INLINE_1= RULE_INLINE ) | ( () this_NORETURN_3= RULE_NORETURN ) ) ;
    public final EObject ruleFunctionSpecifier() throws RecognitionException {
        EObject current = null;

        Token this_INLINE_1=null;
        Token this_NORETURN_3=null;

         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2343:28: ( ( ( () this_INLINE_1= RULE_INLINE ) | ( () this_NORETURN_3= RULE_NORETURN ) ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2344:1: ( ( () this_INLINE_1= RULE_INLINE ) | ( () this_NORETURN_3= RULE_NORETURN ) )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2344:1: ( ( () this_INLINE_1= RULE_INLINE ) | ( () this_NORETURN_3= RULE_NORETURN ) )
            int alt46=2;
            int LA46_0 = input.LA(1);

            if ( (LA46_0==RULE_INLINE) ) {
                alt46=1;
            }
            else if ( (LA46_0==RULE_NORETURN) ) {
                alt46=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 46, 0, input);

                throw nvae;
            }
            switch (alt46) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2344:2: ( () this_INLINE_1= RULE_INLINE )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2344:2: ( () this_INLINE_1= RULE_INLINE )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2344:3: () this_INLINE_1= RULE_INLINE
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2344:3: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2345:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getFunctionSpecifierAccess().getFunctionSpecifierInlineAction_0_0(),
                                  current);
                          
                    }

                    }

                    this_INLINE_1=(Token)match(input,RULE_INLINE,FOLLOW_RULE_INLINE_in_ruleFunctionSpecifier5002); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_INLINE_1, grammarAccess.getFunctionSpecifierAccess().getINLINETerminalRuleCall_0_1()); 
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2355:6: ( () this_NORETURN_3= RULE_NORETURN )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2355:6: ( () this_NORETURN_3= RULE_NORETURN )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2355:7: () this_NORETURN_3= RULE_NORETURN
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2355:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2356:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getFunctionSpecifierAccess().getFunctionSpecifierNoReturnAction_1_0(),
                                  current);
                          
                    }

                    }

                    this_NORETURN_3=(Token)match(input,RULE_NORETURN,FOLLOW_RULE_NORETURN_in_ruleFunctionSpecifier5029); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_NORETURN_3, grammarAccess.getFunctionSpecifierAccess().getNORETURNTerminalRuleCall_1_1()); 
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFunctionSpecifier"


    // $ANTLR start "entryRuleAlignmentSpecifier"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2373:1: entryRuleAlignmentSpecifier returns [EObject current=null] : iv_ruleAlignmentSpecifier= ruleAlignmentSpecifier EOF ;
    public final EObject entryRuleAlignmentSpecifier() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAlignmentSpecifier = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2374:2: (iv_ruleAlignmentSpecifier= ruleAlignmentSpecifier EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2375:2: iv_ruleAlignmentSpecifier= ruleAlignmentSpecifier EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAlignmentSpecifierRule()); 
            }
            pushFollow(FOLLOW_ruleAlignmentSpecifier_in_entryRuleAlignmentSpecifier5065);
            iv_ruleAlignmentSpecifier=ruleAlignmentSpecifier();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAlignmentSpecifier; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleAlignmentSpecifier5075); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAlignmentSpecifier"


    // $ANTLR start "ruleAlignmentSpecifier"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2382:1: ruleAlignmentSpecifier returns [EObject current=null] : (this_ALIGNAS_0= RULE_ALIGNAS this_LPAREN_1= RULE_LPAREN ( ( (lv_type_2_0= ruleSimpleType ) ) | ( ( RULE_IDENTIFIER | RULE_STRING_LITERAL | RULE_FUNC_NAME | RULE_I_CONSTANT | RULE_F_CONSTANT | RULE_LPAREN | RULE_GENERIC | RULE_LCBRACKET | RULE_INC_OP | RULE_DEC_OP | RULE_SIZEOF | RULE_AND_OP | RULE_MULT_OP | RULE_ADD_OP | RULE_MINUS_OP | RULE_TILDE | RULE_NOT_OP | RULE_ALIGNOF )=> (lv_exp_3_0= ruleBaseExpression ) ) ) this_RPAREN_4= RULE_RPAREN ) ;
    public final EObject ruleAlignmentSpecifier() throws RecognitionException {
        EObject current = null;

        Token this_ALIGNAS_0=null;
        Token this_LPAREN_1=null;
        Token this_RPAREN_4=null;
        EObject lv_type_2_0 = null;

        EObject lv_exp_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2385:28: ( (this_ALIGNAS_0= RULE_ALIGNAS this_LPAREN_1= RULE_LPAREN ( ( (lv_type_2_0= ruleSimpleType ) ) | ( ( RULE_IDENTIFIER | RULE_STRING_LITERAL | RULE_FUNC_NAME | RULE_I_CONSTANT | RULE_F_CONSTANT | RULE_LPAREN | RULE_GENERIC | RULE_LCBRACKET | RULE_INC_OP | RULE_DEC_OP | RULE_SIZEOF | RULE_AND_OP | RULE_MULT_OP | RULE_ADD_OP | RULE_MINUS_OP | RULE_TILDE | RULE_NOT_OP | RULE_ALIGNOF )=> (lv_exp_3_0= ruleBaseExpression ) ) ) this_RPAREN_4= RULE_RPAREN ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2386:1: (this_ALIGNAS_0= RULE_ALIGNAS this_LPAREN_1= RULE_LPAREN ( ( (lv_type_2_0= ruleSimpleType ) ) | ( ( RULE_IDENTIFIER | RULE_STRING_LITERAL | RULE_FUNC_NAME | RULE_I_CONSTANT | RULE_F_CONSTANT | RULE_LPAREN | RULE_GENERIC | RULE_LCBRACKET | RULE_INC_OP | RULE_DEC_OP | RULE_SIZEOF | RULE_AND_OP | RULE_MULT_OP | RULE_ADD_OP | RULE_MINUS_OP | RULE_TILDE | RULE_NOT_OP | RULE_ALIGNOF )=> (lv_exp_3_0= ruleBaseExpression ) ) ) this_RPAREN_4= RULE_RPAREN )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2386:1: (this_ALIGNAS_0= RULE_ALIGNAS this_LPAREN_1= RULE_LPAREN ( ( (lv_type_2_0= ruleSimpleType ) ) | ( ( RULE_IDENTIFIER | RULE_STRING_LITERAL | RULE_FUNC_NAME | RULE_I_CONSTANT | RULE_F_CONSTANT | RULE_LPAREN | RULE_GENERIC | RULE_LCBRACKET | RULE_INC_OP | RULE_DEC_OP | RULE_SIZEOF | RULE_AND_OP | RULE_MULT_OP | RULE_ADD_OP | RULE_MINUS_OP | RULE_TILDE | RULE_NOT_OP | RULE_ALIGNOF )=> (lv_exp_3_0= ruleBaseExpression ) ) ) this_RPAREN_4= RULE_RPAREN )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2386:2: this_ALIGNAS_0= RULE_ALIGNAS this_LPAREN_1= RULE_LPAREN ( ( (lv_type_2_0= ruleSimpleType ) ) | ( ( RULE_IDENTIFIER | RULE_STRING_LITERAL | RULE_FUNC_NAME | RULE_I_CONSTANT | RULE_F_CONSTANT | RULE_LPAREN | RULE_GENERIC | RULE_LCBRACKET | RULE_INC_OP | RULE_DEC_OP | RULE_SIZEOF | RULE_AND_OP | RULE_MULT_OP | RULE_ADD_OP | RULE_MINUS_OP | RULE_TILDE | RULE_NOT_OP | RULE_ALIGNOF )=> (lv_exp_3_0= ruleBaseExpression ) ) ) this_RPAREN_4= RULE_RPAREN
            {
            this_ALIGNAS_0=(Token)match(input,RULE_ALIGNAS,FOLLOW_RULE_ALIGNAS_in_ruleAlignmentSpecifier5111); if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                  newLeafNode(this_ALIGNAS_0, grammarAccess.getAlignmentSpecifierAccess().getALIGNASTerminalRuleCall_0()); 
                  
            }
            this_LPAREN_1=(Token)match(input,RULE_LPAREN,FOLLOW_RULE_LPAREN_in_ruleAlignmentSpecifier5121); if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                  newLeafNode(this_LPAREN_1, grammarAccess.getAlignmentSpecifierAccess().getLPARENTerminalRuleCall_1()); 
                  
            }
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2394:1: ( ( (lv_type_2_0= ruleSimpleType ) ) | ( ( RULE_IDENTIFIER | RULE_STRING_LITERAL | RULE_FUNC_NAME | RULE_I_CONSTANT | RULE_F_CONSTANT | RULE_LPAREN | RULE_GENERIC | RULE_LCBRACKET | RULE_INC_OP | RULE_DEC_OP | RULE_SIZEOF | RULE_AND_OP | RULE_MULT_OP | RULE_ADD_OP | RULE_MINUS_OP | RULE_TILDE | RULE_NOT_OP | RULE_ALIGNOF )=> (lv_exp_3_0= ruleBaseExpression ) ) )
            int alt47=2;
            alt47 = dfa47.predict(input);
            switch (alt47) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2394:2: ( (lv_type_2_0= ruleSimpleType ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2394:2: ( (lv_type_2_0= ruleSimpleType ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2395:1: (lv_type_2_0= ruleSimpleType )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2395:1: (lv_type_2_0= ruleSimpleType )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2396:3: lv_type_2_0= ruleSimpleType
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getAlignmentSpecifierAccess().getTypeSimpleTypeParserRuleCall_2_0_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleSimpleType_in_ruleAlignmentSpecifier5142);
                    lv_type_2_0=ruleSimpleType();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getAlignmentSpecifierRule());
                      	        }
                             		set(
                             			current, 
                             			"type",
                              		lv_type_2_0, 
                              		"SimpleType");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2413:6: ( ( RULE_IDENTIFIER | RULE_STRING_LITERAL | RULE_FUNC_NAME | RULE_I_CONSTANT | RULE_F_CONSTANT | RULE_LPAREN | RULE_GENERIC | RULE_LCBRACKET | RULE_INC_OP | RULE_DEC_OP | RULE_SIZEOF | RULE_AND_OP | RULE_MULT_OP | RULE_ADD_OP | RULE_MINUS_OP | RULE_TILDE | RULE_NOT_OP | RULE_ALIGNOF )=> (lv_exp_3_0= ruleBaseExpression ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2413:6: ( ( RULE_IDENTIFIER | RULE_STRING_LITERAL | RULE_FUNC_NAME | RULE_I_CONSTANT | RULE_F_CONSTANT | RULE_LPAREN | RULE_GENERIC | RULE_LCBRACKET | RULE_INC_OP | RULE_DEC_OP | RULE_SIZEOF | RULE_AND_OP | RULE_MULT_OP | RULE_ADD_OP | RULE_MINUS_OP | RULE_TILDE | RULE_NOT_OP | RULE_ALIGNOF )=> (lv_exp_3_0= ruleBaseExpression ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2413:7: ( RULE_IDENTIFIER | RULE_STRING_LITERAL | RULE_FUNC_NAME | RULE_I_CONSTANT | RULE_F_CONSTANT | RULE_LPAREN | RULE_GENERIC | RULE_LCBRACKET | RULE_INC_OP | RULE_DEC_OP | RULE_SIZEOF | RULE_AND_OP | RULE_MULT_OP | RULE_ADD_OP | RULE_MINUS_OP | RULE_TILDE | RULE_NOT_OP | RULE_ALIGNOF )=> (lv_exp_3_0= ruleBaseExpression )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2414:1: (lv_exp_3_0= ruleBaseExpression )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2415:3: lv_exp_3_0= ruleBaseExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getAlignmentSpecifierAccess().getExpBaseExpressionParserRuleCall_2_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleBaseExpression_in_ruleAlignmentSpecifier5259);
                    lv_exp_3_0=ruleBaseExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getAlignmentSpecifierRule());
                      	        }
                             		set(
                             			current, 
                             			"exp",
                              		lv_exp_3_0, 
                              		"BaseExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }

            this_RPAREN_4=(Token)match(input,RULE_RPAREN,FOLLOW_RULE_RPAREN_in_ruleAlignmentSpecifier5271); if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                  newLeafNode(this_RPAREN_4, grammarAccess.getAlignmentSpecifierAccess().getRPARENTerminalRuleCall_3()); 
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAlignmentSpecifier"


    // $ANTLR start "entryRuleExpression"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2443:1: entryRuleExpression returns [EObject current=null] : iv_ruleExpression= ruleExpression EOF ;
    public final EObject entryRuleExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpression = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2444:2: (iv_ruleExpression= ruleExpression EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2445:2: iv_ruleExpression= ruleExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleExpression_in_entryRuleExpression5306);
            iv_ruleExpression=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleExpression5316); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpression"


    // $ANTLR start "ruleExpression"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2452:1: ruleExpression returns [EObject current=null] : this_AssignmentExpression_0= ruleAssignmentExpression ;
    public final EObject ruleExpression() throws RecognitionException {
        EObject current = null;

        EObject this_AssignmentExpression_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2455:28: (this_AssignmentExpression_0= ruleAssignmentExpression )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2457:5: this_AssignmentExpression_0= ruleAssignmentExpression
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getExpressionAccess().getAssignmentExpressionParserRuleCall()); 
                  
            }
            pushFollow(FOLLOW_ruleAssignmentExpression_in_ruleExpression5362);
            this_AssignmentExpression_0=ruleAssignmentExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_AssignmentExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }

            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpression"


    // $ANTLR start "entryRuleAssignmentExpression"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2473:1: entryRuleAssignmentExpression returns [EObject current=null] : iv_ruleAssignmentExpression= ruleAssignmentExpression EOF ;
    public final EObject entryRuleAssignmentExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssignmentExpression = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2474:2: (iv_ruleAssignmentExpression= ruleAssignmentExpression EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2475:2: iv_ruleAssignmentExpression= ruleAssignmentExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAssignmentExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleAssignmentExpression_in_entryRuleAssignmentExpression5396);
            iv_ruleAssignmentExpression=ruleAssignmentExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAssignmentExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleAssignmentExpression5406); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssignmentExpression"


    // $ANTLR start "ruleAssignmentExpression"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2482:1: ruleAssignmentExpression returns [EObject current=null] : (this_BaseExpression_0= ruleBaseExpression ( () ( (lv_list_2_0= ruleAssignmentExpressionElement ) )+ )? ) ;
    public final EObject ruleAssignmentExpression() throws RecognitionException {
        EObject current = null;

        EObject this_BaseExpression_0 = null;

        EObject lv_list_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2485:28: ( (this_BaseExpression_0= ruleBaseExpression ( () ( (lv_list_2_0= ruleAssignmentExpressionElement ) )+ )? ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2486:1: (this_BaseExpression_0= ruleBaseExpression ( () ( (lv_list_2_0= ruleAssignmentExpressionElement ) )+ )? )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2486:1: (this_BaseExpression_0= ruleBaseExpression ( () ( (lv_list_2_0= ruleAssignmentExpressionElement ) )+ )? )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2487:5: this_BaseExpression_0= ruleBaseExpression ( () ( (lv_list_2_0= ruleAssignmentExpressionElement ) )+ )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getAssignmentExpressionAccess().getBaseExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FOLLOW_ruleBaseExpression_in_ruleAssignmentExpression5453);
            this_BaseExpression_0=ruleBaseExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_BaseExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2495:1: ( () ( (lv_list_2_0= ruleAssignmentExpressionElement ) )+ )?
            int alt49=2;
            int LA49_0 = input.LA(1);

            if ( (LA49_0==RULE_ASSIGN||(LA49_0>=RULE_MUL_ASSIGN && LA49_0<=RULE_OR_ASSIGN)) ) {
                alt49=1;
            }
            switch (alt49) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2495:2: () ( (lv_list_2_0= ruleAssignmentExpressionElement ) )+
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2495:2: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2496:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getAssignmentExpressionAccess().getAssignmentExpressionExpAction_1_0(),
                                  current);
                          
                    }

                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2501:2: ( (lv_list_2_0= ruleAssignmentExpressionElement ) )+
                    int cnt48=0;
                    loop48:
                    do {
                        int alt48=2;
                        int LA48_0 = input.LA(1);

                        if ( (LA48_0==RULE_ASSIGN||(LA48_0>=RULE_MUL_ASSIGN && LA48_0<=RULE_OR_ASSIGN)) ) {
                            alt48=1;
                        }


                        switch (alt48) {
                    	case 1 :
                    	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2502:1: (lv_list_2_0= ruleAssignmentExpressionElement )
                    	    {
                    	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2502:1: (lv_list_2_0= ruleAssignmentExpressionElement )
                    	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2503:3: lv_list_2_0= ruleAssignmentExpressionElement
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getAssignmentExpressionAccess().getListAssignmentExpressionElementParserRuleCall_1_1_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FOLLOW_ruleAssignmentExpressionElement_in_ruleAssignmentExpression5483);
                    	    lv_list_2_0=ruleAssignmentExpressionElement();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getAssignmentExpressionRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"list",
                    	              		lv_list_2_0, 
                    	              		"AssignmentExpressionElement");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt48 >= 1 ) break loop48;
                    	    if (state.backtracking>0) {state.failed=true; return current;}
                                EarlyExitException eee =
                                    new EarlyExitException(48, input);
                                throw eee;
                        }
                        cnt48++;
                    } while (true);


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssignmentExpression"


    // $ANTLR start "entryRuleAssignmentExpressionElement"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2527:1: entryRuleAssignmentExpressionElement returns [EObject current=null] : iv_ruleAssignmentExpressionElement= ruleAssignmentExpressionElement EOF ;
    public final EObject entryRuleAssignmentExpressionElement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssignmentExpressionElement = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2528:2: (iv_ruleAssignmentExpressionElement= ruleAssignmentExpressionElement EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2529:2: iv_ruleAssignmentExpressionElement= ruleAssignmentExpressionElement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAssignmentExpressionElementRule()); 
            }
            pushFollow(FOLLOW_ruleAssignmentExpressionElement_in_entryRuleAssignmentExpressionElement5522);
            iv_ruleAssignmentExpressionElement=ruleAssignmentExpressionElement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAssignmentExpressionElement; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleAssignmentExpressionElement5532); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssignmentExpressionElement"


    // $ANTLR start "ruleAssignmentExpressionElement"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2536:1: ruleAssignmentExpressionElement returns [EObject current=null] : ( ( (lv_op_0_0= ruleAssignmentOperator ) ) ( (lv_exp_1_0= ruleBaseExpression ) ) ) ;
    public final EObject ruleAssignmentExpressionElement() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_op_0_0 = null;

        EObject lv_exp_1_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2539:28: ( ( ( (lv_op_0_0= ruleAssignmentOperator ) ) ( (lv_exp_1_0= ruleBaseExpression ) ) ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2540:1: ( ( (lv_op_0_0= ruleAssignmentOperator ) ) ( (lv_exp_1_0= ruleBaseExpression ) ) )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2540:1: ( ( (lv_op_0_0= ruleAssignmentOperator ) ) ( (lv_exp_1_0= ruleBaseExpression ) ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2540:2: ( (lv_op_0_0= ruleAssignmentOperator ) ) ( (lv_exp_1_0= ruleBaseExpression ) )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2540:2: ( (lv_op_0_0= ruleAssignmentOperator ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2541:1: (lv_op_0_0= ruleAssignmentOperator )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2541:1: (lv_op_0_0= ruleAssignmentOperator )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2542:3: lv_op_0_0= ruleAssignmentOperator
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getAssignmentExpressionElementAccess().getOpAssignmentOperatorParserRuleCall_0_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleAssignmentOperator_in_ruleAssignmentExpressionElement5578);
            lv_op_0_0=ruleAssignmentOperator();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getAssignmentExpressionElementRule());
              	        }
                     		set(
                     			current, 
                     			"op",
                      		lv_op_0_0, 
                      		"AssignmentOperator");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2558:2: ( (lv_exp_1_0= ruleBaseExpression ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2559:1: (lv_exp_1_0= ruleBaseExpression )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2559:1: (lv_exp_1_0= ruleBaseExpression )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2560:3: lv_exp_1_0= ruleBaseExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getAssignmentExpressionElementAccess().getExpBaseExpressionParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleBaseExpression_in_ruleAssignmentExpressionElement5599);
            lv_exp_1_0=ruleBaseExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getAssignmentExpressionElementRule());
              	        }
                     		set(
                     			current, 
                     			"exp",
                      		lv_exp_1_0, 
                      		"BaseExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssignmentExpressionElement"


    // $ANTLR start "entryRuleBaseExpression"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2584:1: entryRuleBaseExpression returns [EObject current=null] : iv_ruleBaseExpression= ruleBaseExpression EOF ;
    public final EObject entryRuleBaseExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBaseExpression = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2585:2: (iv_ruleBaseExpression= ruleBaseExpression EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2586:2: iv_ruleBaseExpression= ruleBaseExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBaseExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleBaseExpression_in_entryRuleBaseExpression5635);
            iv_ruleBaseExpression=ruleBaseExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBaseExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleBaseExpression5645); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBaseExpression"


    // $ANTLR start "ruleBaseExpression"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2593:1: ruleBaseExpression returns [EObject current=null] : this_ConditionalExpression_0= ruleConditionalExpression ;
    public final EObject ruleBaseExpression() throws RecognitionException {
        EObject current = null;

        EObject this_ConditionalExpression_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2596:28: (this_ConditionalExpression_0= ruleConditionalExpression )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2598:5: this_ConditionalExpression_0= ruleConditionalExpression
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getBaseExpressionAccess().getConditionalExpressionParserRuleCall()); 
                  
            }
            pushFollow(FOLLOW_ruleConditionalExpression_in_ruleBaseExpression5691);
            this_ConditionalExpression_0=ruleConditionalExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_ConditionalExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }

            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBaseExpression"


    // $ANTLR start "entryRuleConditionalExpression"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2614:1: entryRuleConditionalExpression returns [EObject current=null] : iv_ruleConditionalExpression= ruleConditionalExpression EOF ;
    public final EObject entryRuleConditionalExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConditionalExpression = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2615:2: (iv_ruleConditionalExpression= ruleConditionalExpression EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2616:2: iv_ruleConditionalExpression= ruleConditionalExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getConditionalExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleConditionalExpression_in_entryRuleConditionalExpression5725);
            iv_ruleConditionalExpression=ruleConditionalExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleConditionalExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleConditionalExpression5735); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConditionalExpression"


    // $ANTLR start "ruleConditionalExpression"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2623:1: ruleConditionalExpression returns [EObject current=null] : (this_LogicalOrExpression_0= ruleLogicalOrExpression ( () this_Q_OP_2= RULE_Q_OP ( (lv_yes_3_0= ruleExpression ) ) this_COLON_4= RULE_COLON ( (lv_no_5_0= ruleConditionalExpression ) ) )? ) ;
    public final EObject ruleConditionalExpression() throws RecognitionException {
        EObject current = null;

        Token this_Q_OP_2=null;
        Token this_COLON_4=null;
        EObject this_LogicalOrExpression_0 = null;

        EObject lv_yes_3_0 = null;

        EObject lv_no_5_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2626:28: ( (this_LogicalOrExpression_0= ruleLogicalOrExpression ( () this_Q_OP_2= RULE_Q_OP ( (lv_yes_3_0= ruleExpression ) ) this_COLON_4= RULE_COLON ( (lv_no_5_0= ruleConditionalExpression ) ) )? ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2627:1: (this_LogicalOrExpression_0= ruleLogicalOrExpression ( () this_Q_OP_2= RULE_Q_OP ( (lv_yes_3_0= ruleExpression ) ) this_COLON_4= RULE_COLON ( (lv_no_5_0= ruleConditionalExpression ) ) )? )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2627:1: (this_LogicalOrExpression_0= ruleLogicalOrExpression ( () this_Q_OP_2= RULE_Q_OP ( (lv_yes_3_0= ruleExpression ) ) this_COLON_4= RULE_COLON ( (lv_no_5_0= ruleConditionalExpression ) ) )? )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2628:5: this_LogicalOrExpression_0= ruleLogicalOrExpression ( () this_Q_OP_2= RULE_Q_OP ( (lv_yes_3_0= ruleExpression ) ) this_COLON_4= RULE_COLON ( (lv_no_5_0= ruleConditionalExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getConditionalExpressionAccess().getLogicalOrExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FOLLOW_ruleLogicalOrExpression_in_ruleConditionalExpression5782);
            this_LogicalOrExpression_0=ruleLogicalOrExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_LogicalOrExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2636:1: ( () this_Q_OP_2= RULE_Q_OP ( (lv_yes_3_0= ruleExpression ) ) this_COLON_4= RULE_COLON ( (lv_no_5_0= ruleConditionalExpression ) ) )?
            int alt50=2;
            int LA50_0 = input.LA(1);

            if ( (LA50_0==RULE_Q_OP) ) {
                alt50=1;
            }
            switch (alt50) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2636:2: () this_Q_OP_2= RULE_Q_OP ( (lv_yes_3_0= ruleExpression ) ) this_COLON_4= RULE_COLON ( (lv_no_5_0= ruleConditionalExpression ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2636:2: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2637:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getConditionalExpressionAccess().getConditionalExpressionIfAction_1_0(),
                                  current);
                          
                    }

                    }

                    this_Q_OP_2=(Token)match(input,RULE_Q_OP,FOLLOW_RULE_Q_OP_in_ruleConditionalExpression5802); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_Q_OP_2, grammarAccess.getConditionalExpressionAccess().getQ_OPTerminalRuleCall_1_1()); 
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2646:1: ( (lv_yes_3_0= ruleExpression ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2647:1: (lv_yes_3_0= ruleExpression )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2647:1: (lv_yes_3_0= ruleExpression )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2648:3: lv_yes_3_0= ruleExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getConditionalExpressionAccess().getYesExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleExpression_in_ruleConditionalExpression5822);
                    lv_yes_3_0=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getConditionalExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"yes",
                              		lv_yes_3_0, 
                              		"Expression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    this_COLON_4=(Token)match(input,RULE_COLON,FOLLOW_RULE_COLON_in_ruleConditionalExpression5833); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_COLON_4, grammarAccess.getConditionalExpressionAccess().getCOLONTerminalRuleCall_1_3()); 
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2668:1: ( (lv_no_5_0= ruleConditionalExpression ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2669:1: (lv_no_5_0= ruleConditionalExpression )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2669:1: (lv_no_5_0= ruleConditionalExpression )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2670:3: lv_no_5_0= ruleConditionalExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getConditionalExpressionAccess().getNoConditionalExpressionParserRuleCall_1_4_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleConditionalExpression_in_ruleConditionalExpression5853);
                    lv_no_5_0=ruleConditionalExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getConditionalExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"no",
                              		lv_no_5_0, 
                              		"ConditionalExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConditionalExpression"


    // $ANTLR start "entryRuleLogicalOrExpression"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2694:1: entryRuleLogicalOrExpression returns [EObject current=null] : iv_ruleLogicalOrExpression= ruleLogicalOrExpression EOF ;
    public final EObject entryRuleLogicalOrExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLogicalOrExpression = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2695:2: (iv_ruleLogicalOrExpression= ruleLogicalOrExpression EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2696:2: iv_ruleLogicalOrExpression= ruleLogicalOrExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLogicalOrExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleLogicalOrExpression_in_entryRuleLogicalOrExpression5891);
            iv_ruleLogicalOrExpression=ruleLogicalOrExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLogicalOrExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleLogicalOrExpression5901); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLogicalOrExpression"


    // $ANTLR start "ruleLogicalOrExpression"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2703:1: ruleLogicalOrExpression returns [EObject current=null] : (this_LogicalAndExpression_0= ruleLogicalAndExpression ( () ( (lv_operand_2_0= RULE_LOR_OP ) ) ( (lv_right_3_0= ruleLogicalOrExpression ) ) )? ) ;
    public final EObject ruleLogicalOrExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operand_2_0=null;
        EObject this_LogicalAndExpression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2706:28: ( (this_LogicalAndExpression_0= ruleLogicalAndExpression ( () ( (lv_operand_2_0= RULE_LOR_OP ) ) ( (lv_right_3_0= ruleLogicalOrExpression ) ) )? ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2707:1: (this_LogicalAndExpression_0= ruleLogicalAndExpression ( () ( (lv_operand_2_0= RULE_LOR_OP ) ) ( (lv_right_3_0= ruleLogicalOrExpression ) ) )? )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2707:1: (this_LogicalAndExpression_0= ruleLogicalAndExpression ( () ( (lv_operand_2_0= RULE_LOR_OP ) ) ( (lv_right_3_0= ruleLogicalOrExpression ) ) )? )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2708:5: this_LogicalAndExpression_0= ruleLogicalAndExpression ( () ( (lv_operand_2_0= RULE_LOR_OP ) ) ( (lv_right_3_0= ruleLogicalOrExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getLogicalOrExpressionAccess().getLogicalAndExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FOLLOW_ruleLogicalAndExpression_in_ruleLogicalOrExpression5948);
            this_LogicalAndExpression_0=ruleLogicalAndExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_LogicalAndExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2716:1: ( () ( (lv_operand_2_0= RULE_LOR_OP ) ) ( (lv_right_3_0= ruleLogicalOrExpression ) ) )?
            int alt51=2;
            int LA51_0 = input.LA(1);

            if ( (LA51_0==RULE_LOR_OP) ) {
                alt51=1;
            }
            switch (alt51) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2716:2: () ( (lv_operand_2_0= RULE_LOR_OP ) ) ( (lv_right_3_0= ruleLogicalOrExpression ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2716:2: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2717:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getLogicalOrExpressionAccess().getLogicalOrExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2722:2: ( (lv_operand_2_0= RULE_LOR_OP ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2723:1: (lv_operand_2_0= RULE_LOR_OP )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2723:1: (lv_operand_2_0= RULE_LOR_OP )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2724:3: lv_operand_2_0= RULE_LOR_OP
                    {
                    lv_operand_2_0=(Token)match(input,RULE_LOR_OP,FOLLOW_RULE_LOR_OP_in_ruleLogicalOrExpression5974); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_operand_2_0, grammarAccess.getLogicalOrExpressionAccess().getOperandLOR_OPTerminalRuleCall_1_1_0()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getLogicalOrExpressionRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"operand",
                              		lv_operand_2_0, 
                              		"LOR_OP");
                      	    
                    }

                    }


                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2740:2: ( (lv_right_3_0= ruleLogicalOrExpression ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2741:1: (lv_right_3_0= ruleLogicalOrExpression )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2741:1: (lv_right_3_0= ruleLogicalOrExpression )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2742:3: lv_right_3_0= ruleLogicalOrExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getLogicalOrExpressionAccess().getRightLogicalOrExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleLogicalOrExpression_in_ruleLogicalOrExpression6000);
                    lv_right_3_0=ruleLogicalOrExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getLogicalOrExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"LogicalOrExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLogicalOrExpression"


    // $ANTLR start "entryRuleLogicalAndExpression"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2766:1: entryRuleLogicalAndExpression returns [EObject current=null] : iv_ruleLogicalAndExpression= ruleLogicalAndExpression EOF ;
    public final EObject entryRuleLogicalAndExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLogicalAndExpression = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2767:2: (iv_ruleLogicalAndExpression= ruleLogicalAndExpression EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2768:2: iv_ruleLogicalAndExpression= ruleLogicalAndExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLogicalAndExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleLogicalAndExpression_in_entryRuleLogicalAndExpression6038);
            iv_ruleLogicalAndExpression=ruleLogicalAndExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLogicalAndExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleLogicalAndExpression6048); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLogicalAndExpression"


    // $ANTLR start "ruleLogicalAndExpression"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2775:1: ruleLogicalAndExpression returns [EObject current=null] : (this_InclusiveOrExpression_0= ruleInclusiveOrExpression ( () ( (lv_operand_2_0= RULE_LAND_OP ) ) ( (lv_right_3_0= ruleLogicalAndExpression ) ) )? ) ;
    public final EObject ruleLogicalAndExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operand_2_0=null;
        EObject this_InclusiveOrExpression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2778:28: ( (this_InclusiveOrExpression_0= ruleInclusiveOrExpression ( () ( (lv_operand_2_0= RULE_LAND_OP ) ) ( (lv_right_3_0= ruleLogicalAndExpression ) ) )? ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2779:1: (this_InclusiveOrExpression_0= ruleInclusiveOrExpression ( () ( (lv_operand_2_0= RULE_LAND_OP ) ) ( (lv_right_3_0= ruleLogicalAndExpression ) ) )? )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2779:1: (this_InclusiveOrExpression_0= ruleInclusiveOrExpression ( () ( (lv_operand_2_0= RULE_LAND_OP ) ) ( (lv_right_3_0= ruleLogicalAndExpression ) ) )? )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2780:5: this_InclusiveOrExpression_0= ruleInclusiveOrExpression ( () ( (lv_operand_2_0= RULE_LAND_OP ) ) ( (lv_right_3_0= ruleLogicalAndExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getLogicalAndExpressionAccess().getInclusiveOrExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FOLLOW_ruleInclusiveOrExpression_in_ruleLogicalAndExpression6095);
            this_InclusiveOrExpression_0=ruleInclusiveOrExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_InclusiveOrExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2788:1: ( () ( (lv_operand_2_0= RULE_LAND_OP ) ) ( (lv_right_3_0= ruleLogicalAndExpression ) ) )?
            int alt52=2;
            int LA52_0 = input.LA(1);

            if ( (LA52_0==RULE_LAND_OP) ) {
                alt52=1;
            }
            switch (alt52) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2788:2: () ( (lv_operand_2_0= RULE_LAND_OP ) ) ( (lv_right_3_0= ruleLogicalAndExpression ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2788:2: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2789:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getLogicalAndExpressionAccess().getLogicalAndExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2794:2: ( (lv_operand_2_0= RULE_LAND_OP ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2795:1: (lv_operand_2_0= RULE_LAND_OP )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2795:1: (lv_operand_2_0= RULE_LAND_OP )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2796:3: lv_operand_2_0= RULE_LAND_OP
                    {
                    lv_operand_2_0=(Token)match(input,RULE_LAND_OP,FOLLOW_RULE_LAND_OP_in_ruleLogicalAndExpression6121); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_operand_2_0, grammarAccess.getLogicalAndExpressionAccess().getOperandLAND_OPTerminalRuleCall_1_1_0()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getLogicalAndExpressionRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"operand",
                              		lv_operand_2_0, 
                              		"LAND_OP");
                      	    
                    }

                    }


                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2812:2: ( (lv_right_3_0= ruleLogicalAndExpression ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2813:1: (lv_right_3_0= ruleLogicalAndExpression )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2813:1: (lv_right_3_0= ruleLogicalAndExpression )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2814:3: lv_right_3_0= ruleLogicalAndExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getLogicalAndExpressionAccess().getRightLogicalAndExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleLogicalAndExpression_in_ruleLogicalAndExpression6147);
                    lv_right_3_0=ruleLogicalAndExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getLogicalAndExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"LogicalAndExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLogicalAndExpression"


    // $ANTLR start "entryRuleInclusiveOrExpression"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2838:1: entryRuleInclusiveOrExpression returns [EObject current=null] : iv_ruleInclusiveOrExpression= ruleInclusiveOrExpression EOF ;
    public final EObject entryRuleInclusiveOrExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInclusiveOrExpression = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2839:2: (iv_ruleInclusiveOrExpression= ruleInclusiveOrExpression EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2840:2: iv_ruleInclusiveOrExpression= ruleInclusiveOrExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getInclusiveOrExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleInclusiveOrExpression_in_entryRuleInclusiveOrExpression6185);
            iv_ruleInclusiveOrExpression=ruleInclusiveOrExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleInclusiveOrExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleInclusiveOrExpression6195); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInclusiveOrExpression"


    // $ANTLR start "ruleInclusiveOrExpression"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2847:1: ruleInclusiveOrExpression returns [EObject current=null] : (this_ExclusiveOrExpression_0= ruleExclusiveOrExpression ( () ( (lv_operand_2_0= RULE_OR_OP ) ) ( (lv_right_3_0= ruleInclusiveOrExpression ) ) )? ) ;
    public final EObject ruleInclusiveOrExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operand_2_0=null;
        EObject this_ExclusiveOrExpression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2850:28: ( (this_ExclusiveOrExpression_0= ruleExclusiveOrExpression ( () ( (lv_operand_2_0= RULE_OR_OP ) ) ( (lv_right_3_0= ruleInclusiveOrExpression ) ) )? ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2851:1: (this_ExclusiveOrExpression_0= ruleExclusiveOrExpression ( () ( (lv_operand_2_0= RULE_OR_OP ) ) ( (lv_right_3_0= ruleInclusiveOrExpression ) ) )? )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2851:1: (this_ExclusiveOrExpression_0= ruleExclusiveOrExpression ( () ( (lv_operand_2_0= RULE_OR_OP ) ) ( (lv_right_3_0= ruleInclusiveOrExpression ) ) )? )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2852:5: this_ExclusiveOrExpression_0= ruleExclusiveOrExpression ( () ( (lv_operand_2_0= RULE_OR_OP ) ) ( (lv_right_3_0= ruleInclusiveOrExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getInclusiveOrExpressionAccess().getExclusiveOrExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FOLLOW_ruleExclusiveOrExpression_in_ruleInclusiveOrExpression6242);
            this_ExclusiveOrExpression_0=ruleExclusiveOrExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_ExclusiveOrExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2860:1: ( () ( (lv_operand_2_0= RULE_OR_OP ) ) ( (lv_right_3_0= ruleInclusiveOrExpression ) ) )?
            int alt53=2;
            int LA53_0 = input.LA(1);

            if ( (LA53_0==RULE_OR_OP) ) {
                alt53=1;
            }
            switch (alt53) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2860:2: () ( (lv_operand_2_0= RULE_OR_OP ) ) ( (lv_right_3_0= ruleInclusiveOrExpression ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2860:2: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2861:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getInclusiveOrExpressionAccess().getInclusiveOrExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2866:2: ( (lv_operand_2_0= RULE_OR_OP ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2867:1: (lv_operand_2_0= RULE_OR_OP )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2867:1: (lv_operand_2_0= RULE_OR_OP )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2868:3: lv_operand_2_0= RULE_OR_OP
                    {
                    lv_operand_2_0=(Token)match(input,RULE_OR_OP,FOLLOW_RULE_OR_OP_in_ruleInclusiveOrExpression6268); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_operand_2_0, grammarAccess.getInclusiveOrExpressionAccess().getOperandOR_OPTerminalRuleCall_1_1_0()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getInclusiveOrExpressionRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"operand",
                              		lv_operand_2_0, 
                              		"OR_OP");
                      	    
                    }

                    }


                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2884:2: ( (lv_right_3_0= ruleInclusiveOrExpression ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2885:1: (lv_right_3_0= ruleInclusiveOrExpression )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2885:1: (lv_right_3_0= ruleInclusiveOrExpression )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2886:3: lv_right_3_0= ruleInclusiveOrExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getInclusiveOrExpressionAccess().getRightInclusiveOrExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleInclusiveOrExpression_in_ruleInclusiveOrExpression6294);
                    lv_right_3_0=ruleInclusiveOrExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getInclusiveOrExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"InclusiveOrExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInclusiveOrExpression"


    // $ANTLR start "entryRuleExclusiveOrExpression"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2910:1: entryRuleExclusiveOrExpression returns [EObject current=null] : iv_ruleExclusiveOrExpression= ruleExclusiveOrExpression EOF ;
    public final EObject entryRuleExclusiveOrExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExclusiveOrExpression = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2911:2: (iv_ruleExclusiveOrExpression= ruleExclusiveOrExpression EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2912:2: iv_ruleExclusiveOrExpression= ruleExclusiveOrExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExclusiveOrExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleExclusiveOrExpression_in_entryRuleExclusiveOrExpression6332);
            iv_ruleExclusiveOrExpression=ruleExclusiveOrExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExclusiveOrExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleExclusiveOrExpression6342); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExclusiveOrExpression"


    // $ANTLR start "ruleExclusiveOrExpression"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2919:1: ruleExclusiveOrExpression returns [EObject current=null] : (this_AndExpression_0= ruleAndExpression ( () ( (lv_operand_2_0= RULE_XOR_OP ) ) ( (lv_right_3_0= ruleExclusiveOrExpression ) ) )? ) ;
    public final EObject ruleExclusiveOrExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operand_2_0=null;
        EObject this_AndExpression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2922:28: ( (this_AndExpression_0= ruleAndExpression ( () ( (lv_operand_2_0= RULE_XOR_OP ) ) ( (lv_right_3_0= ruleExclusiveOrExpression ) ) )? ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2923:1: (this_AndExpression_0= ruleAndExpression ( () ( (lv_operand_2_0= RULE_XOR_OP ) ) ( (lv_right_3_0= ruleExclusiveOrExpression ) ) )? )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2923:1: (this_AndExpression_0= ruleAndExpression ( () ( (lv_operand_2_0= RULE_XOR_OP ) ) ( (lv_right_3_0= ruleExclusiveOrExpression ) ) )? )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2924:5: this_AndExpression_0= ruleAndExpression ( () ( (lv_operand_2_0= RULE_XOR_OP ) ) ( (lv_right_3_0= ruleExclusiveOrExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getExclusiveOrExpressionAccess().getAndExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FOLLOW_ruleAndExpression_in_ruleExclusiveOrExpression6389);
            this_AndExpression_0=ruleAndExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_AndExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2932:1: ( () ( (lv_operand_2_0= RULE_XOR_OP ) ) ( (lv_right_3_0= ruleExclusiveOrExpression ) ) )?
            int alt54=2;
            int LA54_0 = input.LA(1);

            if ( (LA54_0==RULE_XOR_OP) ) {
                alt54=1;
            }
            switch (alt54) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2932:2: () ( (lv_operand_2_0= RULE_XOR_OP ) ) ( (lv_right_3_0= ruleExclusiveOrExpression ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2932:2: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2933:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getExclusiveOrExpressionAccess().getExclusiveOrExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2938:2: ( (lv_operand_2_0= RULE_XOR_OP ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2939:1: (lv_operand_2_0= RULE_XOR_OP )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2939:1: (lv_operand_2_0= RULE_XOR_OP )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2940:3: lv_operand_2_0= RULE_XOR_OP
                    {
                    lv_operand_2_0=(Token)match(input,RULE_XOR_OP,FOLLOW_RULE_XOR_OP_in_ruleExclusiveOrExpression6415); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_operand_2_0, grammarAccess.getExclusiveOrExpressionAccess().getOperandXOR_OPTerminalRuleCall_1_1_0()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getExclusiveOrExpressionRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"operand",
                              		lv_operand_2_0, 
                              		"XOR_OP");
                      	    
                    }

                    }


                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2956:2: ( (lv_right_3_0= ruleExclusiveOrExpression ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2957:1: (lv_right_3_0= ruleExclusiveOrExpression )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2957:1: (lv_right_3_0= ruleExclusiveOrExpression )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2958:3: lv_right_3_0= ruleExclusiveOrExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getExclusiveOrExpressionAccess().getRightExclusiveOrExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleExclusiveOrExpression_in_ruleExclusiveOrExpression6441);
                    lv_right_3_0=ruleExclusiveOrExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getExclusiveOrExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"ExclusiveOrExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExclusiveOrExpression"


    // $ANTLR start "entryRuleAndExpression"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2982:1: entryRuleAndExpression returns [EObject current=null] : iv_ruleAndExpression= ruleAndExpression EOF ;
    public final EObject entryRuleAndExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAndExpression = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2983:2: (iv_ruleAndExpression= ruleAndExpression EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2984:2: iv_ruleAndExpression= ruleAndExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAndExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleAndExpression_in_entryRuleAndExpression6479);
            iv_ruleAndExpression=ruleAndExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAndExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleAndExpression6489); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAndExpression"


    // $ANTLR start "ruleAndExpression"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2991:1: ruleAndExpression returns [EObject current=null] : (this_EqualityExpression_0= ruleEqualityExpression ( () ( (lv_operand_2_0= RULE_AND_OP ) ) ( (lv_right_3_0= ruleAndExpression ) ) )? ) ;
    public final EObject ruleAndExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operand_2_0=null;
        EObject this_EqualityExpression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2994:28: ( (this_EqualityExpression_0= ruleEqualityExpression ( () ( (lv_operand_2_0= RULE_AND_OP ) ) ( (lv_right_3_0= ruleAndExpression ) ) )? ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2995:1: (this_EqualityExpression_0= ruleEqualityExpression ( () ( (lv_operand_2_0= RULE_AND_OP ) ) ( (lv_right_3_0= ruleAndExpression ) ) )? )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2995:1: (this_EqualityExpression_0= ruleEqualityExpression ( () ( (lv_operand_2_0= RULE_AND_OP ) ) ( (lv_right_3_0= ruleAndExpression ) ) )? )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2996:5: this_EqualityExpression_0= ruleEqualityExpression ( () ( (lv_operand_2_0= RULE_AND_OP ) ) ( (lv_right_3_0= ruleAndExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getAndExpressionAccess().getEqualityExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FOLLOW_ruleEqualityExpression_in_ruleAndExpression6536);
            this_EqualityExpression_0=ruleEqualityExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_EqualityExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3004:1: ( () ( (lv_operand_2_0= RULE_AND_OP ) ) ( (lv_right_3_0= ruleAndExpression ) ) )?
            int alt55=2;
            int LA55_0 = input.LA(1);

            if ( (LA55_0==RULE_AND_OP) ) {
                alt55=1;
            }
            switch (alt55) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3004:2: () ( (lv_operand_2_0= RULE_AND_OP ) ) ( (lv_right_3_0= ruleAndExpression ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3004:2: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3005:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getAndExpressionAccess().getAndExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3010:2: ( (lv_operand_2_0= RULE_AND_OP ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3011:1: (lv_operand_2_0= RULE_AND_OP )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3011:1: (lv_operand_2_0= RULE_AND_OP )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3012:3: lv_operand_2_0= RULE_AND_OP
                    {
                    lv_operand_2_0=(Token)match(input,RULE_AND_OP,FOLLOW_RULE_AND_OP_in_ruleAndExpression6562); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_operand_2_0, grammarAccess.getAndExpressionAccess().getOperandAND_OPTerminalRuleCall_1_1_0()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getAndExpressionRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"operand",
                              		lv_operand_2_0, 
                              		"AND_OP");
                      	    
                    }

                    }


                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3028:2: ( (lv_right_3_0= ruleAndExpression ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3029:1: (lv_right_3_0= ruleAndExpression )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3029:1: (lv_right_3_0= ruleAndExpression )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3030:3: lv_right_3_0= ruleAndExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getAndExpressionAccess().getRightAndExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleAndExpression_in_ruleAndExpression6588);
                    lv_right_3_0=ruleAndExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getAndExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"AndExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAndExpression"


    // $ANTLR start "entryRuleEqualityExpression"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3054:1: entryRuleEqualityExpression returns [EObject current=null] : iv_ruleEqualityExpression= ruleEqualityExpression EOF ;
    public final EObject entryRuleEqualityExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEqualityExpression = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3055:2: (iv_ruleEqualityExpression= ruleEqualityExpression EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3056:2: iv_ruleEqualityExpression= ruleEqualityExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEqualityExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleEqualityExpression_in_entryRuleEqualityExpression6626);
            iv_ruleEqualityExpression=ruleEqualityExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEqualityExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleEqualityExpression6636); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEqualityExpression"


    // $ANTLR start "ruleEqualityExpression"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3063:1: ruleEqualityExpression returns [EObject current=null] : (this_RelationalExpression_0= ruleRelationalExpression ( () ( (lv_operand_2_0= ruleEqualityOperator ) ) ( (lv_right_3_0= ruleEqualityExpression ) ) )? ) ;
    public final EObject ruleEqualityExpression() throws RecognitionException {
        EObject current = null;

        EObject this_RelationalExpression_0 = null;

        AntlrDatatypeRuleToken lv_operand_2_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3066:28: ( (this_RelationalExpression_0= ruleRelationalExpression ( () ( (lv_operand_2_0= ruleEqualityOperator ) ) ( (lv_right_3_0= ruleEqualityExpression ) ) )? ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3067:1: (this_RelationalExpression_0= ruleRelationalExpression ( () ( (lv_operand_2_0= ruleEqualityOperator ) ) ( (lv_right_3_0= ruleEqualityExpression ) ) )? )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3067:1: (this_RelationalExpression_0= ruleRelationalExpression ( () ( (lv_operand_2_0= ruleEqualityOperator ) ) ( (lv_right_3_0= ruleEqualityExpression ) ) )? )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3068:5: this_RelationalExpression_0= ruleRelationalExpression ( () ( (lv_operand_2_0= ruleEqualityOperator ) ) ( (lv_right_3_0= ruleEqualityExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getEqualityExpressionAccess().getRelationalExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FOLLOW_ruleRelationalExpression_in_ruleEqualityExpression6683);
            this_RelationalExpression_0=ruleRelationalExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_RelationalExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3076:1: ( () ( (lv_operand_2_0= ruleEqualityOperator ) ) ( (lv_right_3_0= ruleEqualityExpression ) ) )?
            int alt56=2;
            int LA56_0 = input.LA(1);

            if ( ((LA56_0>=RULE_EQ_OP && LA56_0<=RULE_NE_OP)) ) {
                alt56=1;
            }
            switch (alt56) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3076:2: () ( (lv_operand_2_0= ruleEqualityOperator ) ) ( (lv_right_3_0= ruleEqualityExpression ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3076:2: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3077:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getEqualityExpressionAccess().getEqualityExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3082:2: ( (lv_operand_2_0= ruleEqualityOperator ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3083:1: (lv_operand_2_0= ruleEqualityOperator )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3083:1: (lv_operand_2_0= ruleEqualityOperator )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3084:3: lv_operand_2_0= ruleEqualityOperator
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getEqualityExpressionAccess().getOperandEqualityOperatorParserRuleCall_1_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleEqualityOperator_in_ruleEqualityExpression6713);
                    lv_operand_2_0=ruleEqualityOperator();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getEqualityExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"operand",
                              		lv_operand_2_0, 
                              		"EqualityOperator");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3100:2: ( (lv_right_3_0= ruleEqualityExpression ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3101:1: (lv_right_3_0= ruleEqualityExpression )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3101:1: (lv_right_3_0= ruleEqualityExpression )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3102:3: lv_right_3_0= ruleEqualityExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getEqualityExpressionAccess().getRightEqualityExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleEqualityExpression_in_ruleEqualityExpression6734);
                    lv_right_3_0=ruleEqualityExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getEqualityExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"EqualityExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEqualityExpression"


    // $ANTLR start "entryRuleRelationalExpression"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3126:1: entryRuleRelationalExpression returns [EObject current=null] : iv_ruleRelationalExpression= ruleRelationalExpression EOF ;
    public final EObject entryRuleRelationalExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRelationalExpression = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3127:2: (iv_ruleRelationalExpression= ruleRelationalExpression EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3128:2: iv_ruleRelationalExpression= ruleRelationalExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRelationalExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleRelationalExpression_in_entryRuleRelationalExpression6772);
            iv_ruleRelationalExpression=ruleRelationalExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRelationalExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleRelationalExpression6782); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRelationalExpression"


    // $ANTLR start "ruleRelationalExpression"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3135:1: ruleRelationalExpression returns [EObject current=null] : (this_ShiftExpression_0= ruleShiftExpression ( () ( (lv_operand_2_0= ruleRelationalOperator ) ) ( (lv_right_3_0= ruleRelationalExpression ) ) )? ) ;
    public final EObject ruleRelationalExpression() throws RecognitionException {
        EObject current = null;

        EObject this_ShiftExpression_0 = null;

        AntlrDatatypeRuleToken lv_operand_2_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3138:28: ( (this_ShiftExpression_0= ruleShiftExpression ( () ( (lv_operand_2_0= ruleRelationalOperator ) ) ( (lv_right_3_0= ruleRelationalExpression ) ) )? ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3139:1: (this_ShiftExpression_0= ruleShiftExpression ( () ( (lv_operand_2_0= ruleRelationalOperator ) ) ( (lv_right_3_0= ruleRelationalExpression ) ) )? )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3139:1: (this_ShiftExpression_0= ruleShiftExpression ( () ( (lv_operand_2_0= ruleRelationalOperator ) ) ( (lv_right_3_0= ruleRelationalExpression ) ) )? )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3140:5: this_ShiftExpression_0= ruleShiftExpression ( () ( (lv_operand_2_0= ruleRelationalOperator ) ) ( (lv_right_3_0= ruleRelationalExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getRelationalExpressionAccess().getShiftExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FOLLOW_ruleShiftExpression_in_ruleRelationalExpression6829);
            this_ShiftExpression_0=ruleShiftExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_ShiftExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3148:1: ( () ( (lv_operand_2_0= ruleRelationalOperator ) ) ( (lv_right_3_0= ruleRelationalExpression ) ) )?
            int alt57=2;
            int LA57_0 = input.LA(1);

            if ( (LA57_0==RULE_L_OP||LA57_0==RULE_G_OP||(LA57_0>=RULE_LE_OP && LA57_0<=RULE_GE_OP)) ) {
                alt57=1;
            }
            switch (alt57) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3148:2: () ( (lv_operand_2_0= ruleRelationalOperator ) ) ( (lv_right_3_0= ruleRelationalExpression ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3148:2: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3149:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getRelationalExpressionAccess().getRelationalExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3154:2: ( (lv_operand_2_0= ruleRelationalOperator ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3155:1: (lv_operand_2_0= ruleRelationalOperator )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3155:1: (lv_operand_2_0= ruleRelationalOperator )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3156:3: lv_operand_2_0= ruleRelationalOperator
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getRelationalExpressionAccess().getOperandRelationalOperatorParserRuleCall_1_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleRelationalOperator_in_ruleRelationalExpression6859);
                    lv_operand_2_0=ruleRelationalOperator();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getRelationalExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"operand",
                              		lv_operand_2_0, 
                              		"RelationalOperator");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3172:2: ( (lv_right_3_0= ruleRelationalExpression ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3173:1: (lv_right_3_0= ruleRelationalExpression )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3173:1: (lv_right_3_0= ruleRelationalExpression )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3174:3: lv_right_3_0= ruleRelationalExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getRelationalExpressionAccess().getRightRelationalExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleRelationalExpression_in_ruleRelationalExpression6880);
                    lv_right_3_0=ruleRelationalExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getRelationalExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"RelationalExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRelationalExpression"


    // $ANTLR start "entryRuleShiftExpression"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3198:1: entryRuleShiftExpression returns [EObject current=null] : iv_ruleShiftExpression= ruleShiftExpression EOF ;
    public final EObject entryRuleShiftExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleShiftExpression = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3199:2: (iv_ruleShiftExpression= ruleShiftExpression EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3200:2: iv_ruleShiftExpression= ruleShiftExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getShiftExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleShiftExpression_in_entryRuleShiftExpression6918);
            iv_ruleShiftExpression=ruleShiftExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleShiftExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleShiftExpression6928); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleShiftExpression"


    // $ANTLR start "ruleShiftExpression"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3207:1: ruleShiftExpression returns [EObject current=null] : (this_AdditiveExpression_0= ruleAdditiveExpression ( () ( (lv_operand_2_0= ruleShiftOperator ) ) ( (lv_right_3_0= ruleShiftExpression ) ) )? ) ;
    public final EObject ruleShiftExpression() throws RecognitionException {
        EObject current = null;

        EObject this_AdditiveExpression_0 = null;

        AntlrDatatypeRuleToken lv_operand_2_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3210:28: ( (this_AdditiveExpression_0= ruleAdditiveExpression ( () ( (lv_operand_2_0= ruleShiftOperator ) ) ( (lv_right_3_0= ruleShiftExpression ) ) )? ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3211:1: (this_AdditiveExpression_0= ruleAdditiveExpression ( () ( (lv_operand_2_0= ruleShiftOperator ) ) ( (lv_right_3_0= ruleShiftExpression ) ) )? )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3211:1: (this_AdditiveExpression_0= ruleAdditiveExpression ( () ( (lv_operand_2_0= ruleShiftOperator ) ) ( (lv_right_3_0= ruleShiftExpression ) ) )? )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3212:5: this_AdditiveExpression_0= ruleAdditiveExpression ( () ( (lv_operand_2_0= ruleShiftOperator ) ) ( (lv_right_3_0= ruleShiftExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getShiftExpressionAccess().getAdditiveExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FOLLOW_ruleAdditiveExpression_in_ruleShiftExpression6975);
            this_AdditiveExpression_0=ruleAdditiveExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_AdditiveExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3220:1: ( () ( (lv_operand_2_0= ruleShiftOperator ) ) ( (lv_right_3_0= ruleShiftExpression ) ) )?
            int alt58=2;
            int LA58_0 = input.LA(1);

            if ( ((LA58_0>=RULE_LEFT_OP && LA58_0<=RULE_RIGHT_OP)) ) {
                alt58=1;
            }
            switch (alt58) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3220:2: () ( (lv_operand_2_0= ruleShiftOperator ) ) ( (lv_right_3_0= ruleShiftExpression ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3220:2: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3221:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getShiftExpressionAccess().getShiftExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3226:2: ( (lv_operand_2_0= ruleShiftOperator ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3227:1: (lv_operand_2_0= ruleShiftOperator )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3227:1: (lv_operand_2_0= ruleShiftOperator )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3228:3: lv_operand_2_0= ruleShiftOperator
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getShiftExpressionAccess().getOperandShiftOperatorParserRuleCall_1_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleShiftOperator_in_ruleShiftExpression7005);
                    lv_operand_2_0=ruleShiftOperator();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getShiftExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"operand",
                              		lv_operand_2_0, 
                              		"ShiftOperator");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3244:2: ( (lv_right_3_0= ruleShiftExpression ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3245:1: (lv_right_3_0= ruleShiftExpression )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3245:1: (lv_right_3_0= ruleShiftExpression )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3246:3: lv_right_3_0= ruleShiftExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getShiftExpressionAccess().getRightShiftExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleShiftExpression_in_ruleShiftExpression7026);
                    lv_right_3_0=ruleShiftExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getShiftExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"ShiftExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleShiftExpression"


    // $ANTLR start "entryRuleAdditiveExpression"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3270:1: entryRuleAdditiveExpression returns [EObject current=null] : iv_ruleAdditiveExpression= ruleAdditiveExpression EOF ;
    public final EObject entryRuleAdditiveExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAdditiveExpression = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3271:2: (iv_ruleAdditiveExpression= ruleAdditiveExpression EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3272:2: iv_ruleAdditiveExpression= ruleAdditiveExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAdditiveExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleAdditiveExpression_in_entryRuleAdditiveExpression7064);
            iv_ruleAdditiveExpression=ruleAdditiveExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAdditiveExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleAdditiveExpression7074); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAdditiveExpression"


    // $ANTLR start "ruleAdditiveExpression"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3279:1: ruleAdditiveExpression returns [EObject current=null] : (this_MultiplicativeExpression_0= ruleMultiplicativeExpression ( () ( (lv_operand_2_0= ruleAdditiveOperator ) ) ( (lv_right_3_0= ruleAdditiveExpression ) ) )? ) ;
    public final EObject ruleAdditiveExpression() throws RecognitionException {
        EObject current = null;

        EObject this_MultiplicativeExpression_0 = null;

        AntlrDatatypeRuleToken lv_operand_2_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3282:28: ( (this_MultiplicativeExpression_0= ruleMultiplicativeExpression ( () ( (lv_operand_2_0= ruleAdditiveOperator ) ) ( (lv_right_3_0= ruleAdditiveExpression ) ) )? ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3283:1: (this_MultiplicativeExpression_0= ruleMultiplicativeExpression ( () ( (lv_operand_2_0= ruleAdditiveOperator ) ) ( (lv_right_3_0= ruleAdditiveExpression ) ) )? )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3283:1: (this_MultiplicativeExpression_0= ruleMultiplicativeExpression ( () ( (lv_operand_2_0= ruleAdditiveOperator ) ) ( (lv_right_3_0= ruleAdditiveExpression ) ) )? )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3284:5: this_MultiplicativeExpression_0= ruleMultiplicativeExpression ( () ( (lv_operand_2_0= ruleAdditiveOperator ) ) ( (lv_right_3_0= ruleAdditiveExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getAdditiveExpressionAccess().getMultiplicativeExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FOLLOW_ruleMultiplicativeExpression_in_ruleAdditiveExpression7121);
            this_MultiplicativeExpression_0=ruleMultiplicativeExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_MultiplicativeExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3292:1: ( () ( (lv_operand_2_0= ruleAdditiveOperator ) ) ( (lv_right_3_0= ruleAdditiveExpression ) ) )?
            int alt59=2;
            int LA59_0 = input.LA(1);

            if ( ((LA59_0>=RULE_ADD_OP && LA59_0<=RULE_MINUS_OP)) ) {
                alt59=1;
            }
            switch (alt59) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3292:2: () ( (lv_operand_2_0= ruleAdditiveOperator ) ) ( (lv_right_3_0= ruleAdditiveExpression ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3292:2: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3293:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getAdditiveExpressionAccess().getAdditiveExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3298:2: ( (lv_operand_2_0= ruleAdditiveOperator ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3299:1: (lv_operand_2_0= ruleAdditiveOperator )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3299:1: (lv_operand_2_0= ruleAdditiveOperator )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3300:3: lv_operand_2_0= ruleAdditiveOperator
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getAdditiveExpressionAccess().getOperandAdditiveOperatorParserRuleCall_1_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleAdditiveOperator_in_ruleAdditiveExpression7151);
                    lv_operand_2_0=ruleAdditiveOperator();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getAdditiveExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"operand",
                              		lv_operand_2_0, 
                              		"AdditiveOperator");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3316:2: ( (lv_right_3_0= ruleAdditiveExpression ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3317:1: (lv_right_3_0= ruleAdditiveExpression )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3317:1: (lv_right_3_0= ruleAdditiveExpression )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3318:3: lv_right_3_0= ruleAdditiveExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getAdditiveExpressionAccess().getRightAdditiveExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleAdditiveExpression_in_ruleAdditiveExpression7172);
                    lv_right_3_0=ruleAdditiveExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getAdditiveExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"AdditiveExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAdditiveExpression"


    // $ANTLR start "entryRuleMultiplicativeExpression"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3342:1: entryRuleMultiplicativeExpression returns [EObject current=null] : iv_ruleMultiplicativeExpression= ruleMultiplicativeExpression EOF ;
    public final EObject entryRuleMultiplicativeExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMultiplicativeExpression = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3343:2: (iv_ruleMultiplicativeExpression= ruleMultiplicativeExpression EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3344:2: iv_ruleMultiplicativeExpression= ruleMultiplicativeExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getMultiplicativeExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleMultiplicativeExpression_in_entryRuleMultiplicativeExpression7210);
            iv_ruleMultiplicativeExpression=ruleMultiplicativeExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMultiplicativeExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleMultiplicativeExpression7220); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMultiplicativeExpression"


    // $ANTLR start "ruleMultiplicativeExpression"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3351:1: ruleMultiplicativeExpression returns [EObject current=null] : (this_UnaryExpression_0= ruleUnaryExpression ( () ( (lv_operand_2_0= ruleMultiplicativeOperator ) ) ( (lv_right_3_0= ruleMultiplicativeExpression ) ) )? ) ;
    public final EObject ruleMultiplicativeExpression() throws RecognitionException {
        EObject current = null;

        EObject this_UnaryExpression_0 = null;

        AntlrDatatypeRuleToken lv_operand_2_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3354:28: ( (this_UnaryExpression_0= ruleUnaryExpression ( () ( (lv_operand_2_0= ruleMultiplicativeOperator ) ) ( (lv_right_3_0= ruleMultiplicativeExpression ) ) )? ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3355:1: (this_UnaryExpression_0= ruleUnaryExpression ( () ( (lv_operand_2_0= ruleMultiplicativeOperator ) ) ( (lv_right_3_0= ruleMultiplicativeExpression ) ) )? )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3355:1: (this_UnaryExpression_0= ruleUnaryExpression ( () ( (lv_operand_2_0= ruleMultiplicativeOperator ) ) ( (lv_right_3_0= ruleMultiplicativeExpression ) ) )? )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3356:5: this_UnaryExpression_0= ruleUnaryExpression ( () ( (lv_operand_2_0= ruleMultiplicativeOperator ) ) ( (lv_right_3_0= ruleMultiplicativeExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getMultiplicativeExpressionAccess().getUnaryExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FOLLOW_ruleUnaryExpression_in_ruleMultiplicativeExpression7267);
            this_UnaryExpression_0=ruleUnaryExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_UnaryExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3364:1: ( () ( (lv_operand_2_0= ruleMultiplicativeOperator ) ) ( (lv_right_3_0= ruleMultiplicativeExpression ) ) )?
            int alt60=2;
            int LA60_0 = input.LA(1);

            if ( (LA60_0==RULE_MULT_OP||(LA60_0>=RULE_DIV_OP && LA60_0<=RULE_MOD_OP)) ) {
                alt60=1;
            }
            switch (alt60) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3364:2: () ( (lv_operand_2_0= ruleMultiplicativeOperator ) ) ( (lv_right_3_0= ruleMultiplicativeExpression ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3364:2: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3365:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getMultiplicativeExpressionAccess().getMultiplicativeExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3370:2: ( (lv_operand_2_0= ruleMultiplicativeOperator ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3371:1: (lv_operand_2_0= ruleMultiplicativeOperator )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3371:1: (lv_operand_2_0= ruleMultiplicativeOperator )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3372:3: lv_operand_2_0= ruleMultiplicativeOperator
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getMultiplicativeExpressionAccess().getOperandMultiplicativeOperatorParserRuleCall_1_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleMultiplicativeOperator_in_ruleMultiplicativeExpression7297);
                    lv_operand_2_0=ruleMultiplicativeOperator();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getMultiplicativeExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"operand",
                              		lv_operand_2_0, 
                              		"MultiplicativeOperator");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3388:2: ( (lv_right_3_0= ruleMultiplicativeExpression ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3389:1: (lv_right_3_0= ruleMultiplicativeExpression )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3389:1: (lv_right_3_0= ruleMultiplicativeExpression )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3390:3: lv_right_3_0= ruleMultiplicativeExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getMultiplicativeExpressionAccess().getRightMultiplicativeExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleMultiplicativeExpression_in_ruleMultiplicativeExpression7318);
                    lv_right_3_0=ruleMultiplicativeExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getMultiplicativeExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"MultiplicativeExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMultiplicativeExpression"


    // $ANTLR start "entryRuleAssignmentOperator"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3414:1: entryRuleAssignmentOperator returns [String current=null] : iv_ruleAssignmentOperator= ruleAssignmentOperator EOF ;
    public final String entryRuleAssignmentOperator() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleAssignmentOperator = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3415:2: (iv_ruleAssignmentOperator= ruleAssignmentOperator EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3416:2: iv_ruleAssignmentOperator= ruleAssignmentOperator EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAssignmentOperatorRule()); 
            }
            pushFollow(FOLLOW_ruleAssignmentOperator_in_entryRuleAssignmentOperator7357);
            iv_ruleAssignmentOperator=ruleAssignmentOperator();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAssignmentOperator.getText(); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleAssignmentOperator7368); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssignmentOperator"


    // $ANTLR start "ruleAssignmentOperator"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3423:1: ruleAssignmentOperator returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ASSIGN_0= RULE_ASSIGN | this_MUL_ASSIGN_1= RULE_MUL_ASSIGN | this_DIV_ASSIGN_2= RULE_DIV_ASSIGN | this_MOD_ASSIGN_3= RULE_MOD_ASSIGN | this_ADD_ASSIGN_4= RULE_ADD_ASSIGN | this_SUB_ASSIGN_5= RULE_SUB_ASSIGN | this_LEFT_ASSIGN_6= RULE_LEFT_ASSIGN | this_RIGHT_ASSIGN_7= RULE_RIGHT_ASSIGN | this_AND_ASSIGN_8= RULE_AND_ASSIGN | this_XOR_ASSIGN_9= RULE_XOR_ASSIGN | this_OR_ASSIGN_10= RULE_OR_ASSIGN ) ;
    public final AntlrDatatypeRuleToken ruleAssignmentOperator() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ASSIGN_0=null;
        Token this_MUL_ASSIGN_1=null;
        Token this_DIV_ASSIGN_2=null;
        Token this_MOD_ASSIGN_3=null;
        Token this_ADD_ASSIGN_4=null;
        Token this_SUB_ASSIGN_5=null;
        Token this_LEFT_ASSIGN_6=null;
        Token this_RIGHT_ASSIGN_7=null;
        Token this_AND_ASSIGN_8=null;
        Token this_XOR_ASSIGN_9=null;
        Token this_OR_ASSIGN_10=null;

         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3426:28: ( (this_ASSIGN_0= RULE_ASSIGN | this_MUL_ASSIGN_1= RULE_MUL_ASSIGN | this_DIV_ASSIGN_2= RULE_DIV_ASSIGN | this_MOD_ASSIGN_3= RULE_MOD_ASSIGN | this_ADD_ASSIGN_4= RULE_ADD_ASSIGN | this_SUB_ASSIGN_5= RULE_SUB_ASSIGN | this_LEFT_ASSIGN_6= RULE_LEFT_ASSIGN | this_RIGHT_ASSIGN_7= RULE_RIGHT_ASSIGN | this_AND_ASSIGN_8= RULE_AND_ASSIGN | this_XOR_ASSIGN_9= RULE_XOR_ASSIGN | this_OR_ASSIGN_10= RULE_OR_ASSIGN ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3427:1: (this_ASSIGN_0= RULE_ASSIGN | this_MUL_ASSIGN_1= RULE_MUL_ASSIGN | this_DIV_ASSIGN_2= RULE_DIV_ASSIGN | this_MOD_ASSIGN_3= RULE_MOD_ASSIGN | this_ADD_ASSIGN_4= RULE_ADD_ASSIGN | this_SUB_ASSIGN_5= RULE_SUB_ASSIGN | this_LEFT_ASSIGN_6= RULE_LEFT_ASSIGN | this_RIGHT_ASSIGN_7= RULE_RIGHT_ASSIGN | this_AND_ASSIGN_8= RULE_AND_ASSIGN | this_XOR_ASSIGN_9= RULE_XOR_ASSIGN | this_OR_ASSIGN_10= RULE_OR_ASSIGN )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3427:1: (this_ASSIGN_0= RULE_ASSIGN | this_MUL_ASSIGN_1= RULE_MUL_ASSIGN | this_DIV_ASSIGN_2= RULE_DIV_ASSIGN | this_MOD_ASSIGN_3= RULE_MOD_ASSIGN | this_ADD_ASSIGN_4= RULE_ADD_ASSIGN | this_SUB_ASSIGN_5= RULE_SUB_ASSIGN | this_LEFT_ASSIGN_6= RULE_LEFT_ASSIGN | this_RIGHT_ASSIGN_7= RULE_RIGHT_ASSIGN | this_AND_ASSIGN_8= RULE_AND_ASSIGN | this_XOR_ASSIGN_9= RULE_XOR_ASSIGN | this_OR_ASSIGN_10= RULE_OR_ASSIGN )
            int alt61=11;
            switch ( input.LA(1) ) {
            case RULE_ASSIGN:
                {
                alt61=1;
                }
                break;
            case RULE_MUL_ASSIGN:
                {
                alt61=2;
                }
                break;
            case RULE_DIV_ASSIGN:
                {
                alt61=3;
                }
                break;
            case RULE_MOD_ASSIGN:
                {
                alt61=4;
                }
                break;
            case RULE_ADD_ASSIGN:
                {
                alt61=5;
                }
                break;
            case RULE_SUB_ASSIGN:
                {
                alt61=6;
                }
                break;
            case RULE_LEFT_ASSIGN:
                {
                alt61=7;
                }
                break;
            case RULE_RIGHT_ASSIGN:
                {
                alt61=8;
                }
                break;
            case RULE_AND_ASSIGN:
                {
                alt61=9;
                }
                break;
            case RULE_XOR_ASSIGN:
                {
                alt61=10;
                }
                break;
            case RULE_OR_ASSIGN:
                {
                alt61=11;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 61, 0, input);

                throw nvae;
            }

            switch (alt61) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3427:6: this_ASSIGN_0= RULE_ASSIGN
                    {
                    this_ASSIGN_0=(Token)match(input,RULE_ASSIGN,FOLLOW_RULE_ASSIGN_in_ruleAssignmentOperator7408); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		current.merge(this_ASSIGN_0);
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_ASSIGN_0, grammarAccess.getAssignmentOperatorAccess().getASSIGNTerminalRuleCall_0()); 
                          
                    }

                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3435:10: this_MUL_ASSIGN_1= RULE_MUL_ASSIGN
                    {
                    this_MUL_ASSIGN_1=(Token)match(input,RULE_MUL_ASSIGN,FOLLOW_RULE_MUL_ASSIGN_in_ruleAssignmentOperator7434); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		current.merge(this_MUL_ASSIGN_1);
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_MUL_ASSIGN_1, grammarAccess.getAssignmentOperatorAccess().getMUL_ASSIGNTerminalRuleCall_1()); 
                          
                    }

                    }
                    break;
                case 3 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3443:10: this_DIV_ASSIGN_2= RULE_DIV_ASSIGN
                    {
                    this_DIV_ASSIGN_2=(Token)match(input,RULE_DIV_ASSIGN,FOLLOW_RULE_DIV_ASSIGN_in_ruleAssignmentOperator7460); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		current.merge(this_DIV_ASSIGN_2);
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_DIV_ASSIGN_2, grammarAccess.getAssignmentOperatorAccess().getDIV_ASSIGNTerminalRuleCall_2()); 
                          
                    }

                    }
                    break;
                case 4 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3451:10: this_MOD_ASSIGN_3= RULE_MOD_ASSIGN
                    {
                    this_MOD_ASSIGN_3=(Token)match(input,RULE_MOD_ASSIGN,FOLLOW_RULE_MOD_ASSIGN_in_ruleAssignmentOperator7486); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		current.merge(this_MOD_ASSIGN_3);
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_MOD_ASSIGN_3, grammarAccess.getAssignmentOperatorAccess().getMOD_ASSIGNTerminalRuleCall_3()); 
                          
                    }

                    }
                    break;
                case 5 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3459:10: this_ADD_ASSIGN_4= RULE_ADD_ASSIGN
                    {
                    this_ADD_ASSIGN_4=(Token)match(input,RULE_ADD_ASSIGN,FOLLOW_RULE_ADD_ASSIGN_in_ruleAssignmentOperator7512); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		current.merge(this_ADD_ASSIGN_4);
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_ADD_ASSIGN_4, grammarAccess.getAssignmentOperatorAccess().getADD_ASSIGNTerminalRuleCall_4()); 
                          
                    }

                    }
                    break;
                case 6 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3467:10: this_SUB_ASSIGN_5= RULE_SUB_ASSIGN
                    {
                    this_SUB_ASSIGN_5=(Token)match(input,RULE_SUB_ASSIGN,FOLLOW_RULE_SUB_ASSIGN_in_ruleAssignmentOperator7538); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		current.merge(this_SUB_ASSIGN_5);
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_SUB_ASSIGN_5, grammarAccess.getAssignmentOperatorAccess().getSUB_ASSIGNTerminalRuleCall_5()); 
                          
                    }

                    }
                    break;
                case 7 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3475:10: this_LEFT_ASSIGN_6= RULE_LEFT_ASSIGN
                    {
                    this_LEFT_ASSIGN_6=(Token)match(input,RULE_LEFT_ASSIGN,FOLLOW_RULE_LEFT_ASSIGN_in_ruleAssignmentOperator7564); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		current.merge(this_LEFT_ASSIGN_6);
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_LEFT_ASSIGN_6, grammarAccess.getAssignmentOperatorAccess().getLEFT_ASSIGNTerminalRuleCall_6()); 
                          
                    }

                    }
                    break;
                case 8 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3483:10: this_RIGHT_ASSIGN_7= RULE_RIGHT_ASSIGN
                    {
                    this_RIGHT_ASSIGN_7=(Token)match(input,RULE_RIGHT_ASSIGN,FOLLOW_RULE_RIGHT_ASSIGN_in_ruleAssignmentOperator7590); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		current.merge(this_RIGHT_ASSIGN_7);
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_RIGHT_ASSIGN_7, grammarAccess.getAssignmentOperatorAccess().getRIGHT_ASSIGNTerminalRuleCall_7()); 
                          
                    }

                    }
                    break;
                case 9 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3491:10: this_AND_ASSIGN_8= RULE_AND_ASSIGN
                    {
                    this_AND_ASSIGN_8=(Token)match(input,RULE_AND_ASSIGN,FOLLOW_RULE_AND_ASSIGN_in_ruleAssignmentOperator7616); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		current.merge(this_AND_ASSIGN_8);
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_AND_ASSIGN_8, grammarAccess.getAssignmentOperatorAccess().getAND_ASSIGNTerminalRuleCall_8()); 
                          
                    }

                    }
                    break;
                case 10 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3499:10: this_XOR_ASSIGN_9= RULE_XOR_ASSIGN
                    {
                    this_XOR_ASSIGN_9=(Token)match(input,RULE_XOR_ASSIGN,FOLLOW_RULE_XOR_ASSIGN_in_ruleAssignmentOperator7642); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		current.merge(this_XOR_ASSIGN_9);
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_XOR_ASSIGN_9, grammarAccess.getAssignmentOperatorAccess().getXOR_ASSIGNTerminalRuleCall_9()); 
                          
                    }

                    }
                    break;
                case 11 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3507:10: this_OR_ASSIGN_10= RULE_OR_ASSIGN
                    {
                    this_OR_ASSIGN_10=(Token)match(input,RULE_OR_ASSIGN,FOLLOW_RULE_OR_ASSIGN_in_ruleAssignmentOperator7668); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		current.merge(this_OR_ASSIGN_10);
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_OR_ASSIGN_10, grammarAccess.getAssignmentOperatorAccess().getOR_ASSIGNTerminalRuleCall_10()); 
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssignmentOperator"


    // $ANTLR start "entryRuleEqualityOperator"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3522:1: entryRuleEqualityOperator returns [String current=null] : iv_ruleEqualityOperator= ruleEqualityOperator EOF ;
    public final String entryRuleEqualityOperator() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEqualityOperator = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3523:2: (iv_ruleEqualityOperator= ruleEqualityOperator EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3524:2: iv_ruleEqualityOperator= ruleEqualityOperator EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEqualityOperatorRule()); 
            }
            pushFollow(FOLLOW_ruleEqualityOperator_in_entryRuleEqualityOperator7714);
            iv_ruleEqualityOperator=ruleEqualityOperator();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEqualityOperator.getText(); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleEqualityOperator7725); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEqualityOperator"


    // $ANTLR start "ruleEqualityOperator"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3531:1: ruleEqualityOperator returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_EQ_OP_0= RULE_EQ_OP | this_NE_OP_1= RULE_NE_OP ) ;
    public final AntlrDatatypeRuleToken ruleEqualityOperator() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_EQ_OP_0=null;
        Token this_NE_OP_1=null;

         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3534:28: ( (this_EQ_OP_0= RULE_EQ_OP | this_NE_OP_1= RULE_NE_OP ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3535:1: (this_EQ_OP_0= RULE_EQ_OP | this_NE_OP_1= RULE_NE_OP )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3535:1: (this_EQ_OP_0= RULE_EQ_OP | this_NE_OP_1= RULE_NE_OP )
            int alt62=2;
            int LA62_0 = input.LA(1);

            if ( (LA62_0==RULE_EQ_OP) ) {
                alt62=1;
            }
            else if ( (LA62_0==RULE_NE_OP) ) {
                alt62=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 62, 0, input);

                throw nvae;
            }
            switch (alt62) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3535:6: this_EQ_OP_0= RULE_EQ_OP
                    {
                    this_EQ_OP_0=(Token)match(input,RULE_EQ_OP,FOLLOW_RULE_EQ_OP_in_ruleEqualityOperator7765); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		current.merge(this_EQ_OP_0);
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_EQ_OP_0, grammarAccess.getEqualityOperatorAccess().getEQ_OPTerminalRuleCall_0()); 
                          
                    }

                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3543:10: this_NE_OP_1= RULE_NE_OP
                    {
                    this_NE_OP_1=(Token)match(input,RULE_NE_OP,FOLLOW_RULE_NE_OP_in_ruleEqualityOperator7791); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		current.merge(this_NE_OP_1);
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_NE_OP_1, grammarAccess.getEqualityOperatorAccess().getNE_OPTerminalRuleCall_1()); 
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEqualityOperator"


    // $ANTLR start "entryRuleRelationalOperator"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3558:1: entryRuleRelationalOperator returns [String current=null] : iv_ruleRelationalOperator= ruleRelationalOperator EOF ;
    public final String entryRuleRelationalOperator() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleRelationalOperator = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3559:2: (iv_ruleRelationalOperator= ruleRelationalOperator EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3560:2: iv_ruleRelationalOperator= ruleRelationalOperator EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRelationalOperatorRule()); 
            }
            pushFollow(FOLLOW_ruleRelationalOperator_in_entryRuleRelationalOperator7837);
            iv_ruleRelationalOperator=ruleRelationalOperator();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRelationalOperator.getText(); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleRelationalOperator7848); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRelationalOperator"


    // $ANTLR start "ruleRelationalOperator"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3567:1: ruleRelationalOperator returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_L_OP_0= RULE_L_OP | this_G_OP_1= RULE_G_OP | this_LE_OP_2= RULE_LE_OP | this_GE_OP_3= RULE_GE_OP ) ;
    public final AntlrDatatypeRuleToken ruleRelationalOperator() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_L_OP_0=null;
        Token this_G_OP_1=null;
        Token this_LE_OP_2=null;
        Token this_GE_OP_3=null;

         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3570:28: ( (this_L_OP_0= RULE_L_OP | this_G_OP_1= RULE_G_OP | this_LE_OP_2= RULE_LE_OP | this_GE_OP_3= RULE_GE_OP ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3571:1: (this_L_OP_0= RULE_L_OP | this_G_OP_1= RULE_G_OP | this_LE_OP_2= RULE_LE_OP | this_GE_OP_3= RULE_GE_OP )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3571:1: (this_L_OP_0= RULE_L_OP | this_G_OP_1= RULE_G_OP | this_LE_OP_2= RULE_LE_OP | this_GE_OP_3= RULE_GE_OP )
            int alt63=4;
            switch ( input.LA(1) ) {
            case RULE_L_OP:
                {
                alt63=1;
                }
                break;
            case RULE_G_OP:
                {
                alt63=2;
                }
                break;
            case RULE_LE_OP:
                {
                alt63=3;
                }
                break;
            case RULE_GE_OP:
                {
                alt63=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 63, 0, input);

                throw nvae;
            }

            switch (alt63) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3571:6: this_L_OP_0= RULE_L_OP
                    {
                    this_L_OP_0=(Token)match(input,RULE_L_OP,FOLLOW_RULE_L_OP_in_ruleRelationalOperator7888); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		current.merge(this_L_OP_0);
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_L_OP_0, grammarAccess.getRelationalOperatorAccess().getL_OPTerminalRuleCall_0()); 
                          
                    }

                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3579:10: this_G_OP_1= RULE_G_OP
                    {
                    this_G_OP_1=(Token)match(input,RULE_G_OP,FOLLOW_RULE_G_OP_in_ruleRelationalOperator7914); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		current.merge(this_G_OP_1);
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_G_OP_1, grammarAccess.getRelationalOperatorAccess().getG_OPTerminalRuleCall_1()); 
                          
                    }

                    }
                    break;
                case 3 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3587:10: this_LE_OP_2= RULE_LE_OP
                    {
                    this_LE_OP_2=(Token)match(input,RULE_LE_OP,FOLLOW_RULE_LE_OP_in_ruleRelationalOperator7940); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		current.merge(this_LE_OP_2);
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_LE_OP_2, grammarAccess.getRelationalOperatorAccess().getLE_OPTerminalRuleCall_2()); 
                          
                    }

                    }
                    break;
                case 4 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3595:10: this_GE_OP_3= RULE_GE_OP
                    {
                    this_GE_OP_3=(Token)match(input,RULE_GE_OP,FOLLOW_RULE_GE_OP_in_ruleRelationalOperator7966); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		current.merge(this_GE_OP_3);
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_GE_OP_3, grammarAccess.getRelationalOperatorAccess().getGE_OPTerminalRuleCall_3()); 
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRelationalOperator"


    // $ANTLR start "entryRuleAdditiveOperator"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3610:1: entryRuleAdditiveOperator returns [String current=null] : iv_ruleAdditiveOperator= ruleAdditiveOperator EOF ;
    public final String entryRuleAdditiveOperator() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleAdditiveOperator = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3611:2: (iv_ruleAdditiveOperator= ruleAdditiveOperator EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3612:2: iv_ruleAdditiveOperator= ruleAdditiveOperator EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAdditiveOperatorRule()); 
            }
            pushFollow(FOLLOW_ruleAdditiveOperator_in_entryRuleAdditiveOperator8012);
            iv_ruleAdditiveOperator=ruleAdditiveOperator();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAdditiveOperator.getText(); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleAdditiveOperator8023); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAdditiveOperator"


    // $ANTLR start "ruleAdditiveOperator"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3619:1: ruleAdditiveOperator returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ADD_OP_0= RULE_ADD_OP | this_MINUS_OP_1= RULE_MINUS_OP ) ;
    public final AntlrDatatypeRuleToken ruleAdditiveOperator() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ADD_OP_0=null;
        Token this_MINUS_OP_1=null;

         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3622:28: ( (this_ADD_OP_0= RULE_ADD_OP | this_MINUS_OP_1= RULE_MINUS_OP ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3623:1: (this_ADD_OP_0= RULE_ADD_OP | this_MINUS_OP_1= RULE_MINUS_OP )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3623:1: (this_ADD_OP_0= RULE_ADD_OP | this_MINUS_OP_1= RULE_MINUS_OP )
            int alt64=2;
            int LA64_0 = input.LA(1);

            if ( (LA64_0==RULE_ADD_OP) ) {
                alt64=1;
            }
            else if ( (LA64_0==RULE_MINUS_OP) ) {
                alt64=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 64, 0, input);

                throw nvae;
            }
            switch (alt64) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3623:6: this_ADD_OP_0= RULE_ADD_OP
                    {
                    this_ADD_OP_0=(Token)match(input,RULE_ADD_OP,FOLLOW_RULE_ADD_OP_in_ruleAdditiveOperator8063); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		current.merge(this_ADD_OP_0);
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_ADD_OP_0, grammarAccess.getAdditiveOperatorAccess().getADD_OPTerminalRuleCall_0()); 
                          
                    }

                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3631:10: this_MINUS_OP_1= RULE_MINUS_OP
                    {
                    this_MINUS_OP_1=(Token)match(input,RULE_MINUS_OP,FOLLOW_RULE_MINUS_OP_in_ruleAdditiveOperator8089); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		current.merge(this_MINUS_OP_1);
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_MINUS_OP_1, grammarAccess.getAdditiveOperatorAccess().getMINUS_OPTerminalRuleCall_1()); 
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAdditiveOperator"


    // $ANTLR start "entryRuleMultiplicativeOperator"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3646:1: entryRuleMultiplicativeOperator returns [String current=null] : iv_ruleMultiplicativeOperator= ruleMultiplicativeOperator EOF ;
    public final String entryRuleMultiplicativeOperator() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleMultiplicativeOperator = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3647:2: (iv_ruleMultiplicativeOperator= ruleMultiplicativeOperator EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3648:2: iv_ruleMultiplicativeOperator= ruleMultiplicativeOperator EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getMultiplicativeOperatorRule()); 
            }
            pushFollow(FOLLOW_ruleMultiplicativeOperator_in_entryRuleMultiplicativeOperator8135);
            iv_ruleMultiplicativeOperator=ruleMultiplicativeOperator();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMultiplicativeOperator.getText(); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleMultiplicativeOperator8146); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMultiplicativeOperator"


    // $ANTLR start "ruleMultiplicativeOperator"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3655:1: ruleMultiplicativeOperator returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_MULT_OP_0= RULE_MULT_OP | this_DIV_OP_1= RULE_DIV_OP | this_MOD_OP_2= RULE_MOD_OP ) ;
    public final AntlrDatatypeRuleToken ruleMultiplicativeOperator() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_MULT_OP_0=null;
        Token this_DIV_OP_1=null;
        Token this_MOD_OP_2=null;

         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3658:28: ( (this_MULT_OP_0= RULE_MULT_OP | this_DIV_OP_1= RULE_DIV_OP | this_MOD_OP_2= RULE_MOD_OP ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3659:1: (this_MULT_OP_0= RULE_MULT_OP | this_DIV_OP_1= RULE_DIV_OP | this_MOD_OP_2= RULE_MOD_OP )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3659:1: (this_MULT_OP_0= RULE_MULT_OP | this_DIV_OP_1= RULE_DIV_OP | this_MOD_OP_2= RULE_MOD_OP )
            int alt65=3;
            switch ( input.LA(1) ) {
            case RULE_MULT_OP:
                {
                alt65=1;
                }
                break;
            case RULE_DIV_OP:
                {
                alt65=2;
                }
                break;
            case RULE_MOD_OP:
                {
                alt65=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 65, 0, input);

                throw nvae;
            }

            switch (alt65) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3659:6: this_MULT_OP_0= RULE_MULT_OP
                    {
                    this_MULT_OP_0=(Token)match(input,RULE_MULT_OP,FOLLOW_RULE_MULT_OP_in_ruleMultiplicativeOperator8186); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		current.merge(this_MULT_OP_0);
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_MULT_OP_0, grammarAccess.getMultiplicativeOperatorAccess().getMULT_OPTerminalRuleCall_0()); 
                          
                    }

                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3667:10: this_DIV_OP_1= RULE_DIV_OP
                    {
                    this_DIV_OP_1=(Token)match(input,RULE_DIV_OP,FOLLOW_RULE_DIV_OP_in_ruleMultiplicativeOperator8212); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		current.merge(this_DIV_OP_1);
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_DIV_OP_1, grammarAccess.getMultiplicativeOperatorAccess().getDIV_OPTerminalRuleCall_1()); 
                          
                    }

                    }
                    break;
                case 3 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3675:10: this_MOD_OP_2= RULE_MOD_OP
                    {
                    this_MOD_OP_2=(Token)match(input,RULE_MOD_OP,FOLLOW_RULE_MOD_OP_in_ruleMultiplicativeOperator8238); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		current.merge(this_MOD_OP_2);
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_MOD_OP_2, grammarAccess.getMultiplicativeOperatorAccess().getMOD_OPTerminalRuleCall_2()); 
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMultiplicativeOperator"


    // $ANTLR start "entryRuleShiftOperator"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3690:1: entryRuleShiftOperator returns [String current=null] : iv_ruleShiftOperator= ruleShiftOperator EOF ;
    public final String entryRuleShiftOperator() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleShiftOperator = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3691:2: (iv_ruleShiftOperator= ruleShiftOperator EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3692:2: iv_ruleShiftOperator= ruleShiftOperator EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getShiftOperatorRule()); 
            }
            pushFollow(FOLLOW_ruleShiftOperator_in_entryRuleShiftOperator8284);
            iv_ruleShiftOperator=ruleShiftOperator();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleShiftOperator.getText(); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleShiftOperator8295); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleShiftOperator"


    // $ANTLR start "ruleShiftOperator"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3699:1: ruleShiftOperator returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_LEFT_OP_0= RULE_LEFT_OP | this_RIGHT_OP_1= RULE_RIGHT_OP ) ;
    public final AntlrDatatypeRuleToken ruleShiftOperator() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_LEFT_OP_0=null;
        Token this_RIGHT_OP_1=null;

         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3702:28: ( (this_LEFT_OP_0= RULE_LEFT_OP | this_RIGHT_OP_1= RULE_RIGHT_OP ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3703:1: (this_LEFT_OP_0= RULE_LEFT_OP | this_RIGHT_OP_1= RULE_RIGHT_OP )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3703:1: (this_LEFT_OP_0= RULE_LEFT_OP | this_RIGHT_OP_1= RULE_RIGHT_OP )
            int alt66=2;
            int LA66_0 = input.LA(1);

            if ( (LA66_0==RULE_LEFT_OP) ) {
                alt66=1;
            }
            else if ( (LA66_0==RULE_RIGHT_OP) ) {
                alt66=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 66, 0, input);

                throw nvae;
            }
            switch (alt66) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3703:6: this_LEFT_OP_0= RULE_LEFT_OP
                    {
                    this_LEFT_OP_0=(Token)match(input,RULE_LEFT_OP,FOLLOW_RULE_LEFT_OP_in_ruleShiftOperator8335); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		current.merge(this_LEFT_OP_0);
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_LEFT_OP_0, grammarAccess.getShiftOperatorAccess().getLEFT_OPTerminalRuleCall_0()); 
                          
                    }

                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3711:10: this_RIGHT_OP_1= RULE_RIGHT_OP
                    {
                    this_RIGHT_OP_1=(Token)match(input,RULE_RIGHT_OP,FOLLOW_RULE_RIGHT_OP_in_ruleShiftOperator8361); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		current.merge(this_RIGHT_OP_1);
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_RIGHT_OP_1, grammarAccess.getShiftOperatorAccess().getRIGHT_OPTerminalRuleCall_1()); 
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleShiftOperator"


    // $ANTLR start "entryRuleUnaryExpression"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3726:1: entryRuleUnaryExpression returns [EObject current=null] : iv_ruleUnaryExpression= ruleUnaryExpression EOF ;
    public final EObject entryRuleUnaryExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUnaryExpression = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3727:2: (iv_ruleUnaryExpression= ruleUnaryExpression EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3728:2: iv_ruleUnaryExpression= ruleUnaryExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getUnaryExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleUnaryExpression_in_entryRuleUnaryExpression8406);
            iv_ruleUnaryExpression=ruleUnaryExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleUnaryExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleUnaryExpression8416); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUnaryExpression"


    // $ANTLR start "ruleUnaryExpression"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3735:1: ruleUnaryExpression returns [EObject current=null] : ( (this_PrimaryExpression_0= rulePrimaryExpression ( () ( (lv_next_2_0= rulePostfixExpressionPostfix ) )+ )? ) | ( () this_LCBRACKET_4= RULE_LCBRACKET ( (lv_exps_5_0= ruleExpression ) ) (this_COMMA_6= RULE_COMMA ( (lv_exps_7_0= ruleExpression ) ) )* (this_COMMA_8= RULE_COMMA )? this_RCBRACKET_9= RULE_RCBRACKET ( (lv_next_10_0= rulePostfixExpressionPostfix ) )* ) | ( () this_INC_OP_12= RULE_INC_OP ( (lv_content_13_0= ruleUnaryExpression ) ) ) | ( () this_DEC_OP_15= RULE_DEC_OP ( (lv_content_16_0= ruleUnaryExpression ) ) ) | ( () this_SIZEOF_18= RULE_SIZEOF ( ( ( RULE_IDENTIFIER | RULE_STRING_LITERAL | RULE_FUNC_NAME | RULE_I_CONSTANT | RULE_F_CONSTANT | RULE_LPAREN | RULE_GENERIC | RULE_LCBRACKET | RULE_INC_OP | RULE_DEC_OP | RULE_SIZEOF | RULE_AND_OP | RULE_MULT_OP | RULE_ADD_OP | RULE_MINUS_OP | RULE_TILDE | RULE_NOT_OP | RULE_ALIGNOF )=> (lv_content_19_0= ruleUnaryExpression ) ) | (this_LPAREN_20= RULE_LPAREN ( (lv_type_21_0= ruleSpecifiedType ) ) this_RPAREN_22= RULE_RPAREN ) ) ) | ( () ( (lv_op_24_0= ruleUNARY_OP ) ) ( (lv_content_25_0= ruleUnaryExpression ) ) ) | ( () this_ALIGNOF_27= RULE_ALIGNOF this_LPAREN_28= RULE_LPAREN ( (lv_type_29_0= ruleSpecifiedType ) ) this_RPAREN_30= RULE_RPAREN ) | ( () (this_LPAREN_32= RULE_LPAREN ( (lv_type_33_0= ruleSpecifiedType ) ) this_RPAREN_34= RULE_RPAREN ) ( ( RULE_IDENTIFIER | RULE_STRING_LITERAL | RULE_FUNC_NAME | RULE_I_CONSTANT | RULE_F_CONSTANT | RULE_LPAREN | RULE_GENERIC | RULE_LCBRACKET | RULE_INC_OP | RULE_DEC_OP | RULE_SIZEOF | RULE_AND_OP | RULE_MULT_OP | RULE_ADD_OP | RULE_MINUS_OP | RULE_TILDE | RULE_NOT_OP | RULE_ALIGNOF )=> (lv_exp_35_0= ruleUnaryExpression ) ) ) ) ;
    public final EObject ruleUnaryExpression() throws RecognitionException {
        EObject current = null;

        Token this_LCBRACKET_4=null;
        Token this_COMMA_6=null;
        Token this_COMMA_8=null;
        Token this_RCBRACKET_9=null;
        Token this_INC_OP_12=null;
        Token this_DEC_OP_15=null;
        Token this_SIZEOF_18=null;
        Token this_LPAREN_20=null;
        Token this_RPAREN_22=null;
        Token this_ALIGNOF_27=null;
        Token this_LPAREN_28=null;
        Token this_RPAREN_30=null;
        Token this_LPAREN_32=null;
        Token this_RPAREN_34=null;
        EObject this_PrimaryExpression_0 = null;

        EObject lv_next_2_0 = null;

        EObject lv_exps_5_0 = null;

        EObject lv_exps_7_0 = null;

        EObject lv_next_10_0 = null;

        EObject lv_content_13_0 = null;

        EObject lv_content_16_0 = null;

        EObject lv_content_19_0 = null;

        EObject lv_type_21_0 = null;

        AntlrDatatypeRuleToken lv_op_24_0 = null;

        EObject lv_content_25_0 = null;

        EObject lv_type_29_0 = null;

        EObject lv_type_33_0 = null;

        EObject lv_exp_35_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3738:28: ( ( (this_PrimaryExpression_0= rulePrimaryExpression ( () ( (lv_next_2_0= rulePostfixExpressionPostfix ) )+ )? ) | ( () this_LCBRACKET_4= RULE_LCBRACKET ( (lv_exps_5_0= ruleExpression ) ) (this_COMMA_6= RULE_COMMA ( (lv_exps_7_0= ruleExpression ) ) )* (this_COMMA_8= RULE_COMMA )? this_RCBRACKET_9= RULE_RCBRACKET ( (lv_next_10_0= rulePostfixExpressionPostfix ) )* ) | ( () this_INC_OP_12= RULE_INC_OP ( (lv_content_13_0= ruleUnaryExpression ) ) ) | ( () this_DEC_OP_15= RULE_DEC_OP ( (lv_content_16_0= ruleUnaryExpression ) ) ) | ( () this_SIZEOF_18= RULE_SIZEOF ( ( ( RULE_IDENTIFIER | RULE_STRING_LITERAL | RULE_FUNC_NAME | RULE_I_CONSTANT | RULE_F_CONSTANT | RULE_LPAREN | RULE_GENERIC | RULE_LCBRACKET | RULE_INC_OP | RULE_DEC_OP | RULE_SIZEOF | RULE_AND_OP | RULE_MULT_OP | RULE_ADD_OP | RULE_MINUS_OP | RULE_TILDE | RULE_NOT_OP | RULE_ALIGNOF )=> (lv_content_19_0= ruleUnaryExpression ) ) | (this_LPAREN_20= RULE_LPAREN ( (lv_type_21_0= ruleSpecifiedType ) ) this_RPAREN_22= RULE_RPAREN ) ) ) | ( () ( (lv_op_24_0= ruleUNARY_OP ) ) ( (lv_content_25_0= ruleUnaryExpression ) ) ) | ( () this_ALIGNOF_27= RULE_ALIGNOF this_LPAREN_28= RULE_LPAREN ( (lv_type_29_0= ruleSpecifiedType ) ) this_RPAREN_30= RULE_RPAREN ) | ( () (this_LPAREN_32= RULE_LPAREN ( (lv_type_33_0= ruleSpecifiedType ) ) this_RPAREN_34= RULE_RPAREN ) ( ( RULE_IDENTIFIER | RULE_STRING_LITERAL | RULE_FUNC_NAME | RULE_I_CONSTANT | RULE_F_CONSTANT | RULE_LPAREN | RULE_GENERIC | RULE_LCBRACKET | RULE_INC_OP | RULE_DEC_OP | RULE_SIZEOF | RULE_AND_OP | RULE_MULT_OP | RULE_ADD_OP | RULE_MINUS_OP | RULE_TILDE | RULE_NOT_OP | RULE_ALIGNOF )=> (lv_exp_35_0= ruleUnaryExpression ) ) ) ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3739:1: ( (this_PrimaryExpression_0= rulePrimaryExpression ( () ( (lv_next_2_0= rulePostfixExpressionPostfix ) )+ )? ) | ( () this_LCBRACKET_4= RULE_LCBRACKET ( (lv_exps_5_0= ruleExpression ) ) (this_COMMA_6= RULE_COMMA ( (lv_exps_7_0= ruleExpression ) ) )* (this_COMMA_8= RULE_COMMA )? this_RCBRACKET_9= RULE_RCBRACKET ( (lv_next_10_0= rulePostfixExpressionPostfix ) )* ) | ( () this_INC_OP_12= RULE_INC_OP ( (lv_content_13_0= ruleUnaryExpression ) ) ) | ( () this_DEC_OP_15= RULE_DEC_OP ( (lv_content_16_0= ruleUnaryExpression ) ) ) | ( () this_SIZEOF_18= RULE_SIZEOF ( ( ( RULE_IDENTIFIER | RULE_STRING_LITERAL | RULE_FUNC_NAME | RULE_I_CONSTANT | RULE_F_CONSTANT | RULE_LPAREN | RULE_GENERIC | RULE_LCBRACKET | RULE_INC_OP | RULE_DEC_OP | RULE_SIZEOF | RULE_AND_OP | RULE_MULT_OP | RULE_ADD_OP | RULE_MINUS_OP | RULE_TILDE | RULE_NOT_OP | RULE_ALIGNOF )=> (lv_content_19_0= ruleUnaryExpression ) ) | (this_LPAREN_20= RULE_LPAREN ( (lv_type_21_0= ruleSpecifiedType ) ) this_RPAREN_22= RULE_RPAREN ) ) ) | ( () ( (lv_op_24_0= ruleUNARY_OP ) ) ( (lv_content_25_0= ruleUnaryExpression ) ) ) | ( () this_ALIGNOF_27= RULE_ALIGNOF this_LPAREN_28= RULE_LPAREN ( (lv_type_29_0= ruleSpecifiedType ) ) this_RPAREN_30= RULE_RPAREN ) | ( () (this_LPAREN_32= RULE_LPAREN ( (lv_type_33_0= ruleSpecifiedType ) ) this_RPAREN_34= RULE_RPAREN ) ( ( RULE_IDENTIFIER | RULE_STRING_LITERAL | RULE_FUNC_NAME | RULE_I_CONSTANT | RULE_F_CONSTANT | RULE_LPAREN | RULE_GENERIC | RULE_LCBRACKET | RULE_INC_OP | RULE_DEC_OP | RULE_SIZEOF | RULE_AND_OP | RULE_MULT_OP | RULE_ADD_OP | RULE_MINUS_OP | RULE_TILDE | RULE_NOT_OP | RULE_ALIGNOF )=> (lv_exp_35_0= ruleUnaryExpression ) ) ) )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3739:1: ( (this_PrimaryExpression_0= rulePrimaryExpression ( () ( (lv_next_2_0= rulePostfixExpressionPostfix ) )+ )? ) | ( () this_LCBRACKET_4= RULE_LCBRACKET ( (lv_exps_5_0= ruleExpression ) ) (this_COMMA_6= RULE_COMMA ( (lv_exps_7_0= ruleExpression ) ) )* (this_COMMA_8= RULE_COMMA )? this_RCBRACKET_9= RULE_RCBRACKET ( (lv_next_10_0= rulePostfixExpressionPostfix ) )* ) | ( () this_INC_OP_12= RULE_INC_OP ( (lv_content_13_0= ruleUnaryExpression ) ) ) | ( () this_DEC_OP_15= RULE_DEC_OP ( (lv_content_16_0= ruleUnaryExpression ) ) ) | ( () this_SIZEOF_18= RULE_SIZEOF ( ( ( RULE_IDENTIFIER | RULE_STRING_LITERAL | RULE_FUNC_NAME | RULE_I_CONSTANT | RULE_F_CONSTANT | RULE_LPAREN | RULE_GENERIC | RULE_LCBRACKET | RULE_INC_OP | RULE_DEC_OP | RULE_SIZEOF | RULE_AND_OP | RULE_MULT_OP | RULE_ADD_OP | RULE_MINUS_OP | RULE_TILDE | RULE_NOT_OP | RULE_ALIGNOF )=> (lv_content_19_0= ruleUnaryExpression ) ) | (this_LPAREN_20= RULE_LPAREN ( (lv_type_21_0= ruleSpecifiedType ) ) this_RPAREN_22= RULE_RPAREN ) ) ) | ( () ( (lv_op_24_0= ruleUNARY_OP ) ) ( (lv_content_25_0= ruleUnaryExpression ) ) ) | ( () this_ALIGNOF_27= RULE_ALIGNOF this_LPAREN_28= RULE_LPAREN ( (lv_type_29_0= ruleSpecifiedType ) ) this_RPAREN_30= RULE_RPAREN ) | ( () (this_LPAREN_32= RULE_LPAREN ( (lv_type_33_0= ruleSpecifiedType ) ) this_RPAREN_34= RULE_RPAREN ) ( ( RULE_IDENTIFIER | RULE_STRING_LITERAL | RULE_FUNC_NAME | RULE_I_CONSTANT | RULE_F_CONSTANT | RULE_LPAREN | RULE_GENERIC | RULE_LCBRACKET | RULE_INC_OP | RULE_DEC_OP | RULE_SIZEOF | RULE_AND_OP | RULE_MULT_OP | RULE_ADD_OP | RULE_MINUS_OP | RULE_TILDE | RULE_NOT_OP | RULE_ALIGNOF )=> (lv_exp_35_0= ruleUnaryExpression ) ) ) )
            int alt73=8;
            alt73 = dfa73.predict(input);
            switch (alt73) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3739:2: (this_PrimaryExpression_0= rulePrimaryExpression ( () ( (lv_next_2_0= rulePostfixExpressionPostfix ) )+ )? )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3739:2: (this_PrimaryExpression_0= rulePrimaryExpression ( () ( (lv_next_2_0= rulePostfixExpressionPostfix ) )+ )? )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3740:5: this_PrimaryExpression_0= rulePrimaryExpression ( () ( (lv_next_2_0= rulePostfixExpressionPostfix ) )+ )?
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getUnaryExpressionAccess().getPrimaryExpressionParserRuleCall_0_0()); 
                          
                    }
                    pushFollow(FOLLOW_rulePrimaryExpression_in_ruleUnaryExpression8464);
                    this_PrimaryExpression_0=rulePrimaryExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_PrimaryExpression_0; 
                              afterParserOrEnumRuleCall();
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3748:1: ( () ( (lv_next_2_0= rulePostfixExpressionPostfix ) )+ )?
                    int alt68=2;
                    int LA68_0 = input.LA(1);

                    if ( (LA68_0==RULE_DOT||LA68_0==RULE_LPAREN||LA68_0==RULE_LBRACKET||(LA68_0>=RULE_INC_OP && LA68_0<=RULE_DEC_OP)||LA68_0==RULE_PTR_OP) ) {
                        alt68=1;
                    }
                    switch (alt68) {
                        case 1 :
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3748:2: () ( (lv_next_2_0= rulePostfixExpressionPostfix ) )+
                            {
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3748:2: ()
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3749:5: 
                            {
                            if ( state.backtracking==0 ) {

                                      current = forceCreateModelElementAndSet(
                                          grammarAccess.getUnaryExpressionAccess().getExpressionSimpleExpAction_0_1_0(),
                                          current);
                                  
                            }

                            }

                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3754:2: ( (lv_next_2_0= rulePostfixExpressionPostfix ) )+
                            int cnt67=0;
                            loop67:
                            do {
                                int alt67=2;
                                int LA67_0 = input.LA(1);

                                if ( (LA67_0==RULE_DOT||LA67_0==RULE_LPAREN||LA67_0==RULE_LBRACKET||(LA67_0>=RULE_INC_OP && LA67_0<=RULE_DEC_OP)||LA67_0==RULE_PTR_OP) ) {
                                    alt67=1;
                                }


                                switch (alt67) {
                            	case 1 :
                            	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3755:1: (lv_next_2_0= rulePostfixExpressionPostfix )
                            	    {
                            	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3755:1: (lv_next_2_0= rulePostfixExpressionPostfix )
                            	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3756:3: lv_next_2_0= rulePostfixExpressionPostfix
                            	    {
                            	    if ( state.backtracking==0 ) {
                            	       
                            	      	        newCompositeNode(grammarAccess.getUnaryExpressionAccess().getNextPostfixExpressionPostfixParserRuleCall_0_1_1_0()); 
                            	      	    
                            	    }
                            	    pushFollow(FOLLOW_rulePostfixExpressionPostfix_in_ruleUnaryExpression8494);
                            	    lv_next_2_0=rulePostfixExpressionPostfix();

                            	    state._fsp--;
                            	    if (state.failed) return current;
                            	    if ( state.backtracking==0 ) {

                            	      	        if (current==null) {
                            	      	            current = createModelElementForParent(grammarAccess.getUnaryExpressionRule());
                            	      	        }
                            	             		add(
                            	             			current, 
                            	             			"next",
                            	              		lv_next_2_0, 
                            	              		"PostfixExpressionPostfix");
                            	      	        afterParserOrEnumRuleCall();
                            	      	    
                            	    }

                            	    }


                            	    }
                            	    break;

                            	default :
                            	    if ( cnt67 >= 1 ) break loop67;
                            	    if (state.backtracking>0) {state.failed=true; return current;}
                                        EarlyExitException eee =
                                            new EarlyExitException(67, input);
                                        throw eee;
                                }
                                cnt67++;
                            } while (true);


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3773:6: ( () this_LCBRACKET_4= RULE_LCBRACKET ( (lv_exps_5_0= ruleExpression ) ) (this_COMMA_6= RULE_COMMA ( (lv_exps_7_0= ruleExpression ) ) )* (this_COMMA_8= RULE_COMMA )? this_RCBRACKET_9= RULE_RCBRACKET ( (lv_next_10_0= rulePostfixExpressionPostfix ) )* )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3773:6: ( () this_LCBRACKET_4= RULE_LCBRACKET ( (lv_exps_5_0= ruleExpression ) ) (this_COMMA_6= RULE_COMMA ( (lv_exps_7_0= ruleExpression ) ) )* (this_COMMA_8= RULE_COMMA )? this_RCBRACKET_9= RULE_RCBRACKET ( (lv_next_10_0= rulePostfixExpressionPostfix ) )* )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3773:7: () this_LCBRACKET_4= RULE_LCBRACKET ( (lv_exps_5_0= ruleExpression ) ) (this_COMMA_6= RULE_COMMA ( (lv_exps_7_0= ruleExpression ) ) )* (this_COMMA_8= RULE_COMMA )? this_RCBRACKET_9= RULE_RCBRACKET ( (lv_next_10_0= rulePostfixExpressionPostfix ) )*
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3773:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3774:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getUnaryExpressionAccess().getExpressionComplexAction_1_0(),
                                  current);
                          
                    }

                    }

                    this_LCBRACKET_4=(Token)match(input,RULE_LCBRACKET,FOLLOW_RULE_LCBRACKET_in_ruleUnaryExpression8525); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_LCBRACKET_4, grammarAccess.getUnaryExpressionAccess().getLCBRACKETTerminalRuleCall_1_1()); 
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3783:1: ( (lv_exps_5_0= ruleExpression ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3784:1: (lv_exps_5_0= ruleExpression )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3784:1: (lv_exps_5_0= ruleExpression )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3785:3: lv_exps_5_0= ruleExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getUnaryExpressionAccess().getExpsExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleExpression_in_ruleUnaryExpression8545);
                    lv_exps_5_0=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getUnaryExpressionRule());
                      	        }
                             		add(
                             			current, 
                             			"exps",
                              		lv_exps_5_0, 
                              		"Expression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3801:2: (this_COMMA_6= RULE_COMMA ( (lv_exps_7_0= ruleExpression ) ) )*
                    loop69:
                    do {
                        int alt69=2;
                        int LA69_0 = input.LA(1);

                        if ( (LA69_0==RULE_COMMA) ) {
                            int LA69_1 = input.LA(2);

                            if ( (LA69_1==RULE_MULT_OP||(LA69_1>=RULE_STRING_LITERAL && LA69_1<=RULE_IDENTIFIER)||LA69_1==RULE_LPAREN||LA69_1==RULE_LCBRACKET||LA69_1==RULE_AND_OP||(LA69_1>=RULE_ADD_OP && LA69_1<=RULE_MINUS_OP)||(LA69_1>=RULE_INC_OP && LA69_1<=RULE_FUNC_NAME)||(LA69_1>=RULE_TILDE && LA69_1<=RULE_NOT_OP)) ) {
                                alt69=1;
                            }


                        }


                        switch (alt69) {
                    	case 1 :
                    	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3801:3: this_COMMA_6= RULE_COMMA ( (lv_exps_7_0= ruleExpression ) )
                    	    {
                    	    this_COMMA_6=(Token)match(input,RULE_COMMA,FOLLOW_RULE_COMMA_in_ruleUnaryExpression8557); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {
                    	       
                    	          newLeafNode(this_COMMA_6, grammarAccess.getUnaryExpressionAccess().getCOMMATerminalRuleCall_1_3_0()); 
                    	          
                    	    }
                    	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3805:1: ( (lv_exps_7_0= ruleExpression ) )
                    	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3806:1: (lv_exps_7_0= ruleExpression )
                    	    {
                    	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3806:1: (lv_exps_7_0= ruleExpression )
                    	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3807:3: lv_exps_7_0= ruleExpression
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getUnaryExpressionAccess().getExpsExpressionParserRuleCall_1_3_1_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FOLLOW_ruleExpression_in_ruleUnaryExpression8577);
                    	    lv_exps_7_0=ruleExpression();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getUnaryExpressionRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"exps",
                    	              		lv_exps_7_0, 
                    	              		"Expression");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop69;
                        }
                    } while (true);

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3823:4: (this_COMMA_8= RULE_COMMA )?
                    int alt70=2;
                    int LA70_0 = input.LA(1);

                    if ( (LA70_0==RULE_COMMA) ) {
                        alt70=1;
                    }
                    switch (alt70) {
                        case 1 :
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3823:5: this_COMMA_8= RULE_COMMA
                            {
                            this_COMMA_8=(Token)match(input,RULE_COMMA,FOLLOW_RULE_COMMA_in_ruleUnaryExpression8591); if (state.failed) return current;
                            if ( state.backtracking==0 ) {
                               
                                  newLeafNode(this_COMMA_8, grammarAccess.getUnaryExpressionAccess().getCOMMATerminalRuleCall_1_4()); 
                                  
                            }

                            }
                            break;

                    }

                    this_RCBRACKET_9=(Token)match(input,RULE_RCBRACKET,FOLLOW_RULE_RCBRACKET_in_ruleUnaryExpression8603); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_RCBRACKET_9, grammarAccess.getUnaryExpressionAccess().getRCBRACKETTerminalRuleCall_1_5()); 
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3831:1: ( (lv_next_10_0= rulePostfixExpressionPostfix ) )*
                    loop71:
                    do {
                        int alt71=2;
                        int LA71_0 = input.LA(1);

                        if ( (LA71_0==RULE_DOT||LA71_0==RULE_LPAREN||LA71_0==RULE_LBRACKET||(LA71_0>=RULE_INC_OP && LA71_0<=RULE_DEC_OP)||LA71_0==RULE_PTR_OP) ) {
                            alt71=1;
                        }


                        switch (alt71) {
                    	case 1 :
                    	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3832:1: (lv_next_10_0= rulePostfixExpressionPostfix )
                    	    {
                    	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3832:1: (lv_next_10_0= rulePostfixExpressionPostfix )
                    	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3833:3: lv_next_10_0= rulePostfixExpressionPostfix
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getUnaryExpressionAccess().getNextPostfixExpressionPostfixParserRuleCall_1_6_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FOLLOW_rulePostfixExpressionPostfix_in_ruleUnaryExpression8623);
                    	    lv_next_10_0=rulePostfixExpressionPostfix();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getUnaryExpressionRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"next",
                    	              		lv_next_10_0, 
                    	              		"PostfixExpressionPostfix");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop71;
                        }
                    } while (true);


                    }


                    }
                    break;
                case 3 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3850:6: ( () this_INC_OP_12= RULE_INC_OP ( (lv_content_13_0= ruleUnaryExpression ) ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3850:6: ( () this_INC_OP_12= RULE_INC_OP ( (lv_content_13_0= ruleUnaryExpression ) ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3850:7: () this_INC_OP_12= RULE_INC_OP ( (lv_content_13_0= ruleUnaryExpression ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3850:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3851:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getUnaryExpressionAccess().getExpressionIncrementAction_2_0(),
                                  current);
                          
                    }

                    }

                    this_INC_OP_12=(Token)match(input,RULE_INC_OP,FOLLOW_RULE_INC_OP_in_ruleUnaryExpression8652); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_INC_OP_12, grammarAccess.getUnaryExpressionAccess().getINC_OPTerminalRuleCall_2_1()); 
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3860:1: ( (lv_content_13_0= ruleUnaryExpression ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3861:1: (lv_content_13_0= ruleUnaryExpression )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3861:1: (lv_content_13_0= ruleUnaryExpression )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3862:3: lv_content_13_0= ruleUnaryExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getUnaryExpressionAccess().getContentUnaryExpressionParserRuleCall_2_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleUnaryExpression_in_ruleUnaryExpression8672);
                    lv_content_13_0=ruleUnaryExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getUnaryExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"content",
                              		lv_content_13_0, 
                              		"UnaryExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;
                case 4 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3879:6: ( () this_DEC_OP_15= RULE_DEC_OP ( (lv_content_16_0= ruleUnaryExpression ) ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3879:6: ( () this_DEC_OP_15= RULE_DEC_OP ( (lv_content_16_0= ruleUnaryExpression ) ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3879:7: () this_DEC_OP_15= RULE_DEC_OP ( (lv_content_16_0= ruleUnaryExpression ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3879:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3880:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getUnaryExpressionAccess().getExpressionDecrementAction_3_0(),
                                  current);
                          
                    }

                    }

                    this_DEC_OP_15=(Token)match(input,RULE_DEC_OP,FOLLOW_RULE_DEC_OP_in_ruleUnaryExpression8700); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_DEC_OP_15, grammarAccess.getUnaryExpressionAccess().getDEC_OPTerminalRuleCall_3_1()); 
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3889:1: ( (lv_content_16_0= ruleUnaryExpression ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3890:1: (lv_content_16_0= ruleUnaryExpression )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3890:1: (lv_content_16_0= ruleUnaryExpression )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3891:3: lv_content_16_0= ruleUnaryExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getUnaryExpressionAccess().getContentUnaryExpressionParserRuleCall_3_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleUnaryExpression_in_ruleUnaryExpression8720);
                    lv_content_16_0=ruleUnaryExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getUnaryExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"content",
                              		lv_content_16_0, 
                              		"UnaryExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;
                case 5 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3908:6: ( () this_SIZEOF_18= RULE_SIZEOF ( ( ( RULE_IDENTIFIER | RULE_STRING_LITERAL | RULE_FUNC_NAME | RULE_I_CONSTANT | RULE_F_CONSTANT | RULE_LPAREN | RULE_GENERIC | RULE_LCBRACKET | RULE_INC_OP | RULE_DEC_OP | RULE_SIZEOF | RULE_AND_OP | RULE_MULT_OP | RULE_ADD_OP | RULE_MINUS_OP | RULE_TILDE | RULE_NOT_OP | RULE_ALIGNOF )=> (lv_content_19_0= ruleUnaryExpression ) ) | (this_LPAREN_20= RULE_LPAREN ( (lv_type_21_0= ruleSpecifiedType ) ) this_RPAREN_22= RULE_RPAREN ) ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3908:6: ( () this_SIZEOF_18= RULE_SIZEOF ( ( ( RULE_IDENTIFIER | RULE_STRING_LITERAL | RULE_FUNC_NAME | RULE_I_CONSTANT | RULE_F_CONSTANT | RULE_LPAREN | RULE_GENERIC | RULE_LCBRACKET | RULE_INC_OP | RULE_DEC_OP | RULE_SIZEOF | RULE_AND_OP | RULE_MULT_OP | RULE_ADD_OP | RULE_MINUS_OP | RULE_TILDE | RULE_NOT_OP | RULE_ALIGNOF )=> (lv_content_19_0= ruleUnaryExpression ) ) | (this_LPAREN_20= RULE_LPAREN ( (lv_type_21_0= ruleSpecifiedType ) ) this_RPAREN_22= RULE_RPAREN ) ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3908:7: () this_SIZEOF_18= RULE_SIZEOF ( ( ( RULE_IDENTIFIER | RULE_STRING_LITERAL | RULE_FUNC_NAME | RULE_I_CONSTANT | RULE_F_CONSTANT | RULE_LPAREN | RULE_GENERIC | RULE_LCBRACKET | RULE_INC_OP | RULE_DEC_OP | RULE_SIZEOF | RULE_AND_OP | RULE_MULT_OP | RULE_ADD_OP | RULE_MINUS_OP | RULE_TILDE | RULE_NOT_OP | RULE_ALIGNOF )=> (lv_content_19_0= ruleUnaryExpression ) ) | (this_LPAREN_20= RULE_LPAREN ( (lv_type_21_0= ruleSpecifiedType ) ) this_RPAREN_22= RULE_RPAREN ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3908:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3909:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getUnaryExpressionAccess().getExpressionSizeofAction_4_0(),
                                  current);
                          
                    }

                    }

                    this_SIZEOF_18=(Token)match(input,RULE_SIZEOF,FOLLOW_RULE_SIZEOF_in_ruleUnaryExpression8748); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_SIZEOF_18, grammarAccess.getUnaryExpressionAccess().getSIZEOFTerminalRuleCall_4_1()); 
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3918:1: ( ( ( RULE_IDENTIFIER | RULE_STRING_LITERAL | RULE_FUNC_NAME | RULE_I_CONSTANT | RULE_F_CONSTANT | RULE_LPAREN | RULE_GENERIC | RULE_LCBRACKET | RULE_INC_OP | RULE_DEC_OP | RULE_SIZEOF | RULE_AND_OP | RULE_MULT_OP | RULE_ADD_OP | RULE_MINUS_OP | RULE_TILDE | RULE_NOT_OP | RULE_ALIGNOF )=> (lv_content_19_0= ruleUnaryExpression ) ) | (this_LPAREN_20= RULE_LPAREN ( (lv_type_21_0= ruleSpecifiedType ) ) this_RPAREN_22= RULE_RPAREN ) )
                    int alt72=2;
                    alt72 = dfa72.predict(input);
                    switch (alt72) {
                        case 1 :
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3918:2: ( ( RULE_IDENTIFIER | RULE_STRING_LITERAL | RULE_FUNC_NAME | RULE_I_CONSTANT | RULE_F_CONSTANT | RULE_LPAREN | RULE_GENERIC | RULE_LCBRACKET | RULE_INC_OP | RULE_DEC_OP | RULE_SIZEOF | RULE_AND_OP | RULE_MULT_OP | RULE_ADD_OP | RULE_MINUS_OP | RULE_TILDE | RULE_NOT_OP | RULE_ALIGNOF )=> (lv_content_19_0= ruleUnaryExpression ) )
                            {
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3918:2: ( ( RULE_IDENTIFIER | RULE_STRING_LITERAL | RULE_FUNC_NAME | RULE_I_CONSTANT | RULE_F_CONSTANT | RULE_LPAREN | RULE_GENERIC | RULE_LCBRACKET | RULE_INC_OP | RULE_DEC_OP | RULE_SIZEOF | RULE_AND_OP | RULE_MULT_OP | RULE_ADD_OP | RULE_MINUS_OP | RULE_TILDE | RULE_NOT_OP | RULE_ALIGNOF )=> (lv_content_19_0= ruleUnaryExpression ) )
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3918:3: ( RULE_IDENTIFIER | RULE_STRING_LITERAL | RULE_FUNC_NAME | RULE_I_CONSTANT | RULE_F_CONSTANT | RULE_LPAREN | RULE_GENERIC | RULE_LCBRACKET | RULE_INC_OP | RULE_DEC_OP | RULE_SIZEOF | RULE_AND_OP | RULE_MULT_OP | RULE_ADD_OP | RULE_MINUS_OP | RULE_TILDE | RULE_NOT_OP | RULE_ALIGNOF )=> (lv_content_19_0= ruleUnaryExpression )
                            {
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3919:1: (lv_content_19_0= ruleUnaryExpression )
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3920:3: lv_content_19_0= ruleUnaryExpression
                            {
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getUnaryExpressionAccess().getContentUnaryExpressionParserRuleCall_4_2_0_0()); 
                              	    
                            }
                            pushFollow(FOLLOW_ruleUnaryExpression_in_ruleUnaryExpression8859);
                            lv_content_19_0=ruleUnaryExpression();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElementForParent(grammarAccess.getUnaryExpressionRule());
                              	        }
                                     		set(
                                     			current, 
                                     			"content",
                                      		lv_content_19_0, 
                                      		"UnaryExpression");
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }


                            }


                            }
                            break;
                        case 2 :
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3937:6: (this_LPAREN_20= RULE_LPAREN ( (lv_type_21_0= ruleSpecifiedType ) ) this_RPAREN_22= RULE_RPAREN )
                            {
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3937:6: (this_LPAREN_20= RULE_LPAREN ( (lv_type_21_0= ruleSpecifiedType ) ) this_RPAREN_22= RULE_RPAREN )
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3937:7: this_LPAREN_20= RULE_LPAREN ( (lv_type_21_0= ruleSpecifiedType ) ) this_RPAREN_22= RULE_RPAREN
                            {
                            this_LPAREN_20=(Token)match(input,RULE_LPAREN,FOLLOW_RULE_LPAREN_in_ruleUnaryExpression8877); if (state.failed) return current;
                            if ( state.backtracking==0 ) {
                               
                                  newLeafNode(this_LPAREN_20, grammarAccess.getUnaryExpressionAccess().getLPARENTerminalRuleCall_4_2_1_0()); 
                                  
                            }
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3941:1: ( (lv_type_21_0= ruleSpecifiedType ) )
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3942:1: (lv_type_21_0= ruleSpecifiedType )
                            {
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3942:1: (lv_type_21_0= ruleSpecifiedType )
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3943:3: lv_type_21_0= ruleSpecifiedType
                            {
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getUnaryExpressionAccess().getTypeSpecifiedTypeParserRuleCall_4_2_1_1_0()); 
                              	    
                            }
                            pushFollow(FOLLOW_ruleSpecifiedType_in_ruleUnaryExpression8897);
                            lv_type_21_0=ruleSpecifiedType();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElementForParent(grammarAccess.getUnaryExpressionRule());
                              	        }
                                     		set(
                                     			current, 
                                     			"type",
                                      		lv_type_21_0, 
                                      		"SpecifiedType");
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }


                            }

                            this_RPAREN_22=(Token)match(input,RULE_RPAREN,FOLLOW_RULE_RPAREN_in_ruleUnaryExpression8908); if (state.failed) return current;
                            if ( state.backtracking==0 ) {
                               
                                  newLeafNode(this_RPAREN_22, grammarAccess.getUnaryExpressionAccess().getRPARENTerminalRuleCall_4_2_1_2()); 
                                  
                            }

                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 6 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3964:6: ( () ( (lv_op_24_0= ruleUNARY_OP ) ) ( (lv_content_25_0= ruleUnaryExpression ) ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3964:6: ( () ( (lv_op_24_0= ruleUNARY_OP ) ) ( (lv_content_25_0= ruleUnaryExpression ) ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3964:7: () ( (lv_op_24_0= ruleUNARY_OP ) ) ( (lv_content_25_0= ruleUnaryExpression ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3964:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3965:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getUnaryExpressionAccess().getExpressionUnaryOpAction_5_0(),
                                  current);
                          
                    }

                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3970:2: ( (lv_op_24_0= ruleUNARY_OP ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3971:1: (lv_op_24_0= ruleUNARY_OP )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3971:1: (lv_op_24_0= ruleUNARY_OP )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3972:3: lv_op_24_0= ruleUNARY_OP
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getUnaryExpressionAccess().getOpUNARY_OPParserRuleCall_5_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleUNARY_OP_in_ruleUnaryExpression8947);
                    lv_op_24_0=ruleUNARY_OP();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getUnaryExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"op",
                              		lv_op_24_0, 
                              		"UNARY_OP");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3988:2: ( (lv_content_25_0= ruleUnaryExpression ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3989:1: (lv_content_25_0= ruleUnaryExpression )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3989:1: (lv_content_25_0= ruleUnaryExpression )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3990:3: lv_content_25_0= ruleUnaryExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getUnaryExpressionAccess().getContentUnaryExpressionParserRuleCall_5_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleUnaryExpression_in_ruleUnaryExpression8968);
                    lv_content_25_0=ruleUnaryExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getUnaryExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"content",
                              		lv_content_25_0, 
                              		"UnaryExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;
                case 7 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4007:6: ( () this_ALIGNOF_27= RULE_ALIGNOF this_LPAREN_28= RULE_LPAREN ( (lv_type_29_0= ruleSpecifiedType ) ) this_RPAREN_30= RULE_RPAREN )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4007:6: ( () this_ALIGNOF_27= RULE_ALIGNOF this_LPAREN_28= RULE_LPAREN ( (lv_type_29_0= ruleSpecifiedType ) ) this_RPAREN_30= RULE_RPAREN )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4007:7: () this_ALIGNOF_27= RULE_ALIGNOF this_LPAREN_28= RULE_LPAREN ( (lv_type_29_0= ruleSpecifiedType ) ) this_RPAREN_30= RULE_RPAREN
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4007:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4008:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getUnaryExpressionAccess().getExpressionAlignAction_6_0(),
                                  current);
                          
                    }

                    }

                    this_ALIGNOF_27=(Token)match(input,RULE_ALIGNOF,FOLLOW_RULE_ALIGNOF_in_ruleUnaryExpression8996); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_ALIGNOF_27, grammarAccess.getUnaryExpressionAccess().getALIGNOFTerminalRuleCall_6_1()); 
                          
                    }
                    this_LPAREN_28=(Token)match(input,RULE_LPAREN,FOLLOW_RULE_LPAREN_in_ruleUnaryExpression9006); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_LPAREN_28, grammarAccess.getUnaryExpressionAccess().getLPARENTerminalRuleCall_6_2()); 
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4021:1: ( (lv_type_29_0= ruleSpecifiedType ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4022:1: (lv_type_29_0= ruleSpecifiedType )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4022:1: (lv_type_29_0= ruleSpecifiedType )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4023:3: lv_type_29_0= ruleSpecifiedType
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getUnaryExpressionAccess().getTypeSpecifiedTypeParserRuleCall_6_3_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleSpecifiedType_in_ruleUnaryExpression9026);
                    lv_type_29_0=ruleSpecifiedType();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getUnaryExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"type",
                              		lv_type_29_0, 
                              		"SpecifiedType");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    this_RPAREN_30=(Token)match(input,RULE_RPAREN,FOLLOW_RULE_RPAREN_in_ruleUnaryExpression9037); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_RPAREN_30, grammarAccess.getUnaryExpressionAccess().getRPARENTerminalRuleCall_6_4()); 
                          
                    }

                    }


                    }
                    break;
                case 8 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4044:6: ( () (this_LPAREN_32= RULE_LPAREN ( (lv_type_33_0= ruleSpecifiedType ) ) this_RPAREN_34= RULE_RPAREN ) ( ( RULE_IDENTIFIER | RULE_STRING_LITERAL | RULE_FUNC_NAME | RULE_I_CONSTANT | RULE_F_CONSTANT | RULE_LPAREN | RULE_GENERIC | RULE_LCBRACKET | RULE_INC_OP | RULE_DEC_OP | RULE_SIZEOF | RULE_AND_OP | RULE_MULT_OP | RULE_ADD_OP | RULE_MINUS_OP | RULE_TILDE | RULE_NOT_OP | RULE_ALIGNOF )=> (lv_exp_35_0= ruleUnaryExpression ) ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4044:6: ( () (this_LPAREN_32= RULE_LPAREN ( (lv_type_33_0= ruleSpecifiedType ) ) this_RPAREN_34= RULE_RPAREN ) ( ( RULE_IDENTIFIER | RULE_STRING_LITERAL | RULE_FUNC_NAME | RULE_I_CONSTANT | RULE_F_CONSTANT | RULE_LPAREN | RULE_GENERIC | RULE_LCBRACKET | RULE_INC_OP | RULE_DEC_OP | RULE_SIZEOF | RULE_AND_OP | RULE_MULT_OP | RULE_ADD_OP | RULE_MINUS_OP | RULE_TILDE | RULE_NOT_OP | RULE_ALIGNOF )=> (lv_exp_35_0= ruleUnaryExpression ) ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4044:7: () (this_LPAREN_32= RULE_LPAREN ( (lv_type_33_0= ruleSpecifiedType ) ) this_RPAREN_34= RULE_RPAREN ) ( ( RULE_IDENTIFIER | RULE_STRING_LITERAL | RULE_FUNC_NAME | RULE_I_CONSTANT | RULE_F_CONSTANT | RULE_LPAREN | RULE_GENERIC | RULE_LCBRACKET | RULE_INC_OP | RULE_DEC_OP | RULE_SIZEOF | RULE_AND_OP | RULE_MULT_OP | RULE_ADD_OP | RULE_MINUS_OP | RULE_TILDE | RULE_NOT_OP | RULE_ALIGNOF )=> (lv_exp_35_0= ruleUnaryExpression ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4044:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4045:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getUnaryExpressionAccess().getExpressionCastAction_7_0(),
                                  current);
                          
                    }

                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4050:2: (this_LPAREN_32= RULE_LPAREN ( (lv_type_33_0= ruleSpecifiedType ) ) this_RPAREN_34= RULE_RPAREN )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4050:3: this_LPAREN_32= RULE_LPAREN ( (lv_type_33_0= ruleSpecifiedType ) ) this_RPAREN_34= RULE_RPAREN
                    {
                    this_LPAREN_32=(Token)match(input,RULE_LPAREN,FOLLOW_RULE_LPAREN_in_ruleUnaryExpression9065); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_LPAREN_32, grammarAccess.getUnaryExpressionAccess().getLPARENTerminalRuleCall_7_1_0()); 
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4054:1: ( (lv_type_33_0= ruleSpecifiedType ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4055:1: (lv_type_33_0= ruleSpecifiedType )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4055:1: (lv_type_33_0= ruleSpecifiedType )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4056:3: lv_type_33_0= ruleSpecifiedType
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getUnaryExpressionAccess().getTypeSpecifiedTypeParserRuleCall_7_1_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleSpecifiedType_in_ruleUnaryExpression9085);
                    lv_type_33_0=ruleSpecifiedType();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getUnaryExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"type",
                              		lv_type_33_0, 
                              		"SpecifiedType");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    this_RPAREN_34=(Token)match(input,RULE_RPAREN,FOLLOW_RULE_RPAREN_in_ruleUnaryExpression9096); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_RPAREN_34, grammarAccess.getUnaryExpressionAccess().getRPARENTerminalRuleCall_7_1_2()); 
                          
                    }

                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4076:2: ( ( RULE_IDENTIFIER | RULE_STRING_LITERAL | RULE_FUNC_NAME | RULE_I_CONSTANT | RULE_F_CONSTANT | RULE_LPAREN | RULE_GENERIC | RULE_LCBRACKET | RULE_INC_OP | RULE_DEC_OP | RULE_SIZEOF | RULE_AND_OP | RULE_MULT_OP | RULE_ADD_OP | RULE_MINUS_OP | RULE_TILDE | RULE_NOT_OP | RULE_ALIGNOF )=> (lv_exp_35_0= ruleUnaryExpression ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4076:3: ( RULE_IDENTIFIER | RULE_STRING_LITERAL | RULE_FUNC_NAME | RULE_I_CONSTANT | RULE_F_CONSTANT | RULE_LPAREN | RULE_GENERIC | RULE_LCBRACKET | RULE_INC_OP | RULE_DEC_OP | RULE_SIZEOF | RULE_AND_OP | RULE_MULT_OP | RULE_ADD_OP | RULE_MINUS_OP | RULE_TILDE | RULE_NOT_OP | RULE_ALIGNOF )=> (lv_exp_35_0= ruleUnaryExpression )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4077:1: (lv_exp_35_0= ruleUnaryExpression )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4078:3: lv_exp_35_0= ruleUnaryExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getUnaryExpressionAccess().getExpUnaryExpressionParserRuleCall_7_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleUnaryExpression_in_ruleUnaryExpression9207);
                    lv_exp_35_0=ruleUnaryExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getUnaryExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"exp",
                              		lv_exp_35_0, 
                              		"UnaryExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUnaryExpression"


    // $ANTLR start "entryRulePrimaryExpression"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4102:1: entryRulePrimaryExpression returns [EObject current=null] : iv_rulePrimaryExpression= rulePrimaryExpression EOF ;
    public final EObject entryRulePrimaryExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePrimaryExpression = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4103:2: (iv_rulePrimaryExpression= rulePrimaryExpression EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4104:2: iv_rulePrimaryExpression= rulePrimaryExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getPrimaryExpressionRule()); 
            }
            pushFollow(FOLLOW_rulePrimaryExpression_in_entryRulePrimaryExpression9244);
            iv_rulePrimaryExpression=rulePrimaryExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_rulePrimaryExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRulePrimaryExpression9254); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePrimaryExpression"


    // $ANTLR start "rulePrimaryExpression"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4111:1: rulePrimaryExpression returns [EObject current=null] : ( ( () ( (lv_name_1_0= RULE_IDENTIFIER ) ) ) | ( () ( (lv_value_3_0= ruleStringExpression ) ) ) | ( () ( (lv_value_5_0= RULE_I_CONSTANT ) ) ) | ( () ( (lv_value_7_0= RULE_F_CONSTANT ) ) ) | ( () this_LPAREN_9= RULE_LPAREN ( (lv_par_10_0= ruleExpression ) ) this_RPAREN_11= RULE_RPAREN ) | ( () this_GENERIC_13= RULE_GENERIC this_LPAREN_14= RULE_LPAREN ( (lv_left_15_0= ruleAssignmentExpression ) ) (this_COMMA_16= RULE_COMMA ( (lv_right_17_0= ruleGenericAssociation ) ) )+ this_RPAREN_18= RULE_RPAREN ) ) ;
    public final EObject rulePrimaryExpression() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        Token lv_value_5_0=null;
        Token lv_value_7_0=null;
        Token this_LPAREN_9=null;
        Token this_RPAREN_11=null;
        Token this_GENERIC_13=null;
        Token this_LPAREN_14=null;
        Token this_COMMA_16=null;
        Token this_RPAREN_18=null;
        EObject lv_value_3_0 = null;

        EObject lv_par_10_0 = null;

        EObject lv_left_15_0 = null;

        EObject lv_right_17_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4114:28: ( ( ( () ( (lv_name_1_0= RULE_IDENTIFIER ) ) ) | ( () ( (lv_value_3_0= ruleStringExpression ) ) ) | ( () ( (lv_value_5_0= RULE_I_CONSTANT ) ) ) | ( () ( (lv_value_7_0= RULE_F_CONSTANT ) ) ) | ( () this_LPAREN_9= RULE_LPAREN ( (lv_par_10_0= ruleExpression ) ) this_RPAREN_11= RULE_RPAREN ) | ( () this_GENERIC_13= RULE_GENERIC this_LPAREN_14= RULE_LPAREN ( (lv_left_15_0= ruleAssignmentExpression ) ) (this_COMMA_16= RULE_COMMA ( (lv_right_17_0= ruleGenericAssociation ) ) )+ this_RPAREN_18= RULE_RPAREN ) ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4115:1: ( ( () ( (lv_name_1_0= RULE_IDENTIFIER ) ) ) | ( () ( (lv_value_3_0= ruleStringExpression ) ) ) | ( () ( (lv_value_5_0= RULE_I_CONSTANT ) ) ) | ( () ( (lv_value_7_0= RULE_F_CONSTANT ) ) ) | ( () this_LPAREN_9= RULE_LPAREN ( (lv_par_10_0= ruleExpression ) ) this_RPAREN_11= RULE_RPAREN ) | ( () this_GENERIC_13= RULE_GENERIC this_LPAREN_14= RULE_LPAREN ( (lv_left_15_0= ruleAssignmentExpression ) ) (this_COMMA_16= RULE_COMMA ( (lv_right_17_0= ruleGenericAssociation ) ) )+ this_RPAREN_18= RULE_RPAREN ) )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4115:1: ( ( () ( (lv_name_1_0= RULE_IDENTIFIER ) ) ) | ( () ( (lv_value_3_0= ruleStringExpression ) ) ) | ( () ( (lv_value_5_0= RULE_I_CONSTANT ) ) ) | ( () ( (lv_value_7_0= RULE_F_CONSTANT ) ) ) | ( () this_LPAREN_9= RULE_LPAREN ( (lv_par_10_0= ruleExpression ) ) this_RPAREN_11= RULE_RPAREN ) | ( () this_GENERIC_13= RULE_GENERIC this_LPAREN_14= RULE_LPAREN ( (lv_left_15_0= ruleAssignmentExpression ) ) (this_COMMA_16= RULE_COMMA ( (lv_right_17_0= ruleGenericAssociation ) ) )+ this_RPAREN_18= RULE_RPAREN ) )
            int alt75=6;
            switch ( input.LA(1) ) {
            case RULE_IDENTIFIER:
                {
                alt75=1;
                }
                break;
            case RULE_STRING_LITERAL:
            case RULE_FUNC_NAME:
                {
                alt75=2;
                }
                break;
            case RULE_I_CONSTANT:
                {
                alt75=3;
                }
                break;
            case RULE_F_CONSTANT:
                {
                alt75=4;
                }
                break;
            case RULE_LPAREN:
                {
                alt75=5;
                }
                break;
            case RULE_GENERIC:
                {
                alt75=6;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 75, 0, input);

                throw nvae;
            }

            switch (alt75) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4115:2: ( () ( (lv_name_1_0= RULE_IDENTIFIER ) ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4115:2: ( () ( (lv_name_1_0= RULE_IDENTIFIER ) ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4115:3: () ( (lv_name_1_0= RULE_IDENTIFIER ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4115:3: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4116:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getPrimaryExpressionAccess().getExpressionIdentifierAction_0_0(),
                                  current);
                          
                    }

                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4121:2: ( (lv_name_1_0= RULE_IDENTIFIER ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4122:1: (lv_name_1_0= RULE_IDENTIFIER )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4122:1: (lv_name_1_0= RULE_IDENTIFIER )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4123:3: lv_name_1_0= RULE_IDENTIFIER
                    {
                    lv_name_1_0=(Token)match(input,RULE_IDENTIFIER,FOLLOW_RULE_IDENTIFIER_in_rulePrimaryExpression9306); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_name_1_0, grammarAccess.getPrimaryExpressionAccess().getNameIDENTIFIERTerminalRuleCall_0_1_0()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getPrimaryExpressionRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"name",
                              		lv_name_1_0, 
                              		"IDENTIFIER");
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4140:6: ( () ( (lv_value_3_0= ruleStringExpression ) ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4140:6: ( () ( (lv_value_3_0= ruleStringExpression ) ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4140:7: () ( (lv_value_3_0= ruleStringExpression ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4140:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4141:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getPrimaryExpressionAccess().getExpressionStringAction_1_0(),
                                  current);
                          
                    }

                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4146:2: ( (lv_value_3_0= ruleStringExpression ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4147:1: (lv_value_3_0= ruleStringExpression )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4147:1: (lv_value_3_0= ruleStringExpression )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4148:3: lv_value_3_0= ruleStringExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getPrimaryExpressionAccess().getValueStringExpressionParserRuleCall_1_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleStringExpression_in_rulePrimaryExpression9349);
                    lv_value_3_0=ruleStringExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getPrimaryExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"value",
                              		lv_value_3_0, 
                              		"StringExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4165:6: ( () ( (lv_value_5_0= RULE_I_CONSTANT ) ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4165:6: ( () ( (lv_value_5_0= RULE_I_CONSTANT ) ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4165:7: () ( (lv_value_5_0= RULE_I_CONSTANT ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4165:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4166:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getPrimaryExpressionAccess().getExpressionIntegerAction_2_0(),
                                  current);
                          
                    }

                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4171:2: ( (lv_value_5_0= RULE_I_CONSTANT ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4172:1: (lv_value_5_0= RULE_I_CONSTANT )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4172:1: (lv_value_5_0= RULE_I_CONSTANT )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4173:3: lv_value_5_0= RULE_I_CONSTANT
                    {
                    lv_value_5_0=(Token)match(input,RULE_I_CONSTANT,FOLLOW_RULE_I_CONSTANT_in_rulePrimaryExpression9383); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_value_5_0, grammarAccess.getPrimaryExpressionAccess().getValueI_CONSTANTTerminalRuleCall_2_1_0()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getPrimaryExpressionRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"value",
                              		lv_value_5_0, 
                              		"I_CONSTANT");
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;
                case 4 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4190:6: ( () ( (lv_value_7_0= RULE_F_CONSTANT ) ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4190:6: ( () ( (lv_value_7_0= RULE_F_CONSTANT ) ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4190:7: () ( (lv_value_7_0= RULE_F_CONSTANT ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4190:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4191:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getPrimaryExpressionAccess().getExpressionFloatAction_3_0(),
                                  current);
                          
                    }

                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4196:2: ( (lv_value_7_0= RULE_F_CONSTANT ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4197:1: (lv_value_7_0= RULE_F_CONSTANT )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4197:1: (lv_value_7_0= RULE_F_CONSTANT )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4198:3: lv_value_7_0= RULE_F_CONSTANT
                    {
                    lv_value_7_0=(Token)match(input,RULE_F_CONSTANT,FOLLOW_RULE_F_CONSTANT_in_rulePrimaryExpression9422); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_value_7_0, grammarAccess.getPrimaryExpressionAccess().getValueF_CONSTANTTerminalRuleCall_3_1_0()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getPrimaryExpressionRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"value",
                              		lv_value_7_0, 
                              		"F_CONSTANT");
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;
                case 5 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4215:6: ( () this_LPAREN_9= RULE_LPAREN ( (lv_par_10_0= ruleExpression ) ) this_RPAREN_11= RULE_RPAREN )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4215:6: ( () this_LPAREN_9= RULE_LPAREN ( (lv_par_10_0= ruleExpression ) ) this_RPAREN_11= RULE_RPAREN )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4215:7: () this_LPAREN_9= RULE_LPAREN ( (lv_par_10_0= ruleExpression ) ) this_RPAREN_11= RULE_RPAREN
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4215:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4216:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getPrimaryExpressionAccess().getExpressionSubExpressionAction_4_0(),
                                  current);
                          
                    }

                    }

                    this_LPAREN_9=(Token)match(input,RULE_LPAREN,FOLLOW_RULE_LPAREN_in_rulePrimaryExpression9455); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_LPAREN_9, grammarAccess.getPrimaryExpressionAccess().getLPARENTerminalRuleCall_4_1()); 
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4225:1: ( (lv_par_10_0= ruleExpression ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4226:1: (lv_par_10_0= ruleExpression )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4226:1: (lv_par_10_0= ruleExpression )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4227:3: lv_par_10_0= ruleExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getPrimaryExpressionAccess().getParExpressionParserRuleCall_4_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleExpression_in_rulePrimaryExpression9475);
                    lv_par_10_0=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getPrimaryExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"par",
                              		lv_par_10_0, 
                              		"Expression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    this_RPAREN_11=(Token)match(input,RULE_RPAREN,FOLLOW_RULE_RPAREN_in_rulePrimaryExpression9486); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_RPAREN_11, grammarAccess.getPrimaryExpressionAccess().getRPARENTerminalRuleCall_4_3()); 
                          
                    }

                    }


                    }
                    break;
                case 6 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4248:6: ( () this_GENERIC_13= RULE_GENERIC this_LPAREN_14= RULE_LPAREN ( (lv_left_15_0= ruleAssignmentExpression ) ) (this_COMMA_16= RULE_COMMA ( (lv_right_17_0= ruleGenericAssociation ) ) )+ this_RPAREN_18= RULE_RPAREN )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4248:6: ( () this_GENERIC_13= RULE_GENERIC this_LPAREN_14= RULE_LPAREN ( (lv_left_15_0= ruleAssignmentExpression ) ) (this_COMMA_16= RULE_COMMA ( (lv_right_17_0= ruleGenericAssociation ) ) )+ this_RPAREN_18= RULE_RPAREN )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4248:7: () this_GENERIC_13= RULE_GENERIC this_LPAREN_14= RULE_LPAREN ( (lv_left_15_0= ruleAssignmentExpression ) ) (this_COMMA_16= RULE_COMMA ( (lv_right_17_0= ruleGenericAssociation ) ) )+ this_RPAREN_18= RULE_RPAREN
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4248:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4249:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getPrimaryExpressionAccess().getExpressionGenericSelectionAction_5_0(),
                                  current);
                          
                    }

                    }

                    this_GENERIC_13=(Token)match(input,RULE_GENERIC,FOLLOW_RULE_GENERIC_in_rulePrimaryExpression9513); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_GENERIC_13, grammarAccess.getPrimaryExpressionAccess().getGENERICTerminalRuleCall_5_1()); 
                          
                    }
                    this_LPAREN_14=(Token)match(input,RULE_LPAREN,FOLLOW_RULE_LPAREN_in_rulePrimaryExpression9523); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_LPAREN_14, grammarAccess.getPrimaryExpressionAccess().getLPARENTerminalRuleCall_5_2()); 
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4262:1: ( (lv_left_15_0= ruleAssignmentExpression ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4263:1: (lv_left_15_0= ruleAssignmentExpression )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4263:1: (lv_left_15_0= ruleAssignmentExpression )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4264:3: lv_left_15_0= ruleAssignmentExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getPrimaryExpressionAccess().getLeftAssignmentExpressionParserRuleCall_5_3_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleAssignmentExpression_in_rulePrimaryExpression9543);
                    lv_left_15_0=ruleAssignmentExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getPrimaryExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"left",
                              		lv_left_15_0, 
                              		"AssignmentExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4280:2: (this_COMMA_16= RULE_COMMA ( (lv_right_17_0= ruleGenericAssociation ) ) )+
                    int cnt74=0;
                    loop74:
                    do {
                        int alt74=2;
                        int LA74_0 = input.LA(1);

                        if ( (LA74_0==RULE_COMMA) ) {
                            alt74=1;
                        }


                        switch (alt74) {
                    	case 1 :
                    	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4280:3: this_COMMA_16= RULE_COMMA ( (lv_right_17_0= ruleGenericAssociation ) )
                    	    {
                    	    this_COMMA_16=(Token)match(input,RULE_COMMA,FOLLOW_RULE_COMMA_in_rulePrimaryExpression9555); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {
                    	       
                    	          newLeafNode(this_COMMA_16, grammarAccess.getPrimaryExpressionAccess().getCOMMATerminalRuleCall_5_4_0()); 
                    	          
                    	    }
                    	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4284:1: ( (lv_right_17_0= ruleGenericAssociation ) )
                    	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4285:1: (lv_right_17_0= ruleGenericAssociation )
                    	    {
                    	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4285:1: (lv_right_17_0= ruleGenericAssociation )
                    	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4286:3: lv_right_17_0= ruleGenericAssociation
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getPrimaryExpressionAccess().getRightGenericAssociationParserRuleCall_5_4_1_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FOLLOW_ruleGenericAssociation_in_rulePrimaryExpression9575);
                    	    lv_right_17_0=ruleGenericAssociation();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getPrimaryExpressionRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"right",
                    	              		lv_right_17_0, 
                    	              		"GenericAssociation");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt74 >= 1 ) break loop74;
                    	    if (state.backtracking>0) {state.failed=true; return current;}
                                EarlyExitException eee =
                                    new EarlyExitException(74, input);
                                throw eee;
                        }
                        cnt74++;
                    } while (true);

                    this_RPAREN_18=(Token)match(input,RULE_RPAREN,FOLLOW_RULE_RPAREN_in_rulePrimaryExpression9588); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_RPAREN_18, grammarAccess.getPrimaryExpressionAccess().getRPARENTerminalRuleCall_5_5()); 
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePrimaryExpression"


    // $ANTLR start "entryRuleStringExpression"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4314:1: entryRuleStringExpression returns [EObject current=null] : iv_ruleStringExpression= ruleStringExpression EOF ;
    public final EObject entryRuleStringExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStringExpression = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4315:2: (iv_ruleStringExpression= ruleStringExpression EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4316:2: iv_ruleStringExpression= ruleStringExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStringExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleStringExpression_in_entryRuleStringExpression9624);
            iv_ruleStringExpression=ruleStringExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStringExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleStringExpression9634); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStringExpression"


    // $ANTLR start "ruleStringExpression"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4323:1: ruleStringExpression returns [EObject current=null] : ( ( (lv_name_0_0= RULE_STRING_LITERAL ) ) | ( (lv_name_1_0= RULE_FUNC_NAME ) ) ) ;
    public final EObject ruleStringExpression() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token lv_name_1_0=null;

         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4326:28: ( ( ( (lv_name_0_0= RULE_STRING_LITERAL ) ) | ( (lv_name_1_0= RULE_FUNC_NAME ) ) ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4327:1: ( ( (lv_name_0_0= RULE_STRING_LITERAL ) ) | ( (lv_name_1_0= RULE_FUNC_NAME ) ) )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4327:1: ( ( (lv_name_0_0= RULE_STRING_LITERAL ) ) | ( (lv_name_1_0= RULE_FUNC_NAME ) ) )
            int alt76=2;
            int LA76_0 = input.LA(1);

            if ( (LA76_0==RULE_STRING_LITERAL) ) {
                alt76=1;
            }
            else if ( (LA76_0==RULE_FUNC_NAME) ) {
                alt76=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 76, 0, input);

                throw nvae;
            }
            switch (alt76) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4327:2: ( (lv_name_0_0= RULE_STRING_LITERAL ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4327:2: ( (lv_name_0_0= RULE_STRING_LITERAL ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4328:1: (lv_name_0_0= RULE_STRING_LITERAL )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4328:1: (lv_name_0_0= RULE_STRING_LITERAL )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4329:3: lv_name_0_0= RULE_STRING_LITERAL
                    {
                    lv_name_0_0=(Token)match(input,RULE_STRING_LITERAL,FOLLOW_RULE_STRING_LITERAL_in_ruleStringExpression9676); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_name_0_0, grammarAccess.getStringExpressionAccess().getNameSTRING_LITERALTerminalRuleCall_0_0()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getStringExpressionRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"name",
                              		lv_name_0_0, 
                              		"STRING_LITERAL");
                      	    
                    }

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4346:6: ( (lv_name_1_0= RULE_FUNC_NAME ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4346:6: ( (lv_name_1_0= RULE_FUNC_NAME ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4347:1: (lv_name_1_0= RULE_FUNC_NAME )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4347:1: (lv_name_1_0= RULE_FUNC_NAME )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4348:3: lv_name_1_0= RULE_FUNC_NAME
                    {
                    lv_name_1_0=(Token)match(input,RULE_FUNC_NAME,FOLLOW_RULE_FUNC_NAME_in_ruleStringExpression9704); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_name_1_0, grammarAccess.getStringExpressionAccess().getNameFUNC_NAMETerminalRuleCall_1_0()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getStringExpressionRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"name",
                              		lv_name_1_0, 
                              		"FUNC_NAME");
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStringExpression"


    // $ANTLR start "entryRuleGenericAssociation"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4372:1: entryRuleGenericAssociation returns [EObject current=null] : iv_ruleGenericAssociation= ruleGenericAssociation EOF ;
    public final EObject entryRuleGenericAssociation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGenericAssociation = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4373:2: (iv_ruleGenericAssociation= ruleGenericAssociation EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4374:2: iv_ruleGenericAssociation= ruleGenericAssociation EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getGenericAssociationRule()); 
            }
            pushFollow(FOLLOW_ruleGenericAssociation_in_entryRuleGenericAssociation9745);
            iv_ruleGenericAssociation=ruleGenericAssociation();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleGenericAssociation; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleGenericAssociation9755); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGenericAssociation"


    // $ANTLR start "ruleGenericAssociation"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4381:1: ruleGenericAssociation returns [EObject current=null] : ( ( () ( (lv_type_1_0= ruleSpecifiedType ) ) this_COLON_2= RULE_COLON ( (lv_exp_3_0= ruleAssignmentExpression ) ) ) | ( () this_DEFAULT_5= RULE_DEFAULT this_COLON_6= RULE_COLON ( (lv_exp_7_0= ruleAssignmentExpression ) ) ) ) ;
    public final EObject ruleGenericAssociation() throws RecognitionException {
        EObject current = null;

        Token this_COLON_2=null;
        Token this_DEFAULT_5=null;
        Token this_COLON_6=null;
        EObject lv_type_1_0 = null;

        EObject lv_exp_3_0 = null;

        EObject lv_exp_7_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4384:28: ( ( ( () ( (lv_type_1_0= ruleSpecifiedType ) ) this_COLON_2= RULE_COLON ( (lv_exp_3_0= ruleAssignmentExpression ) ) ) | ( () this_DEFAULT_5= RULE_DEFAULT this_COLON_6= RULE_COLON ( (lv_exp_7_0= ruleAssignmentExpression ) ) ) ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4385:1: ( ( () ( (lv_type_1_0= ruleSpecifiedType ) ) this_COLON_2= RULE_COLON ( (lv_exp_3_0= ruleAssignmentExpression ) ) ) | ( () this_DEFAULT_5= RULE_DEFAULT this_COLON_6= RULE_COLON ( (lv_exp_7_0= ruleAssignmentExpression ) ) ) )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4385:1: ( ( () ( (lv_type_1_0= ruleSpecifiedType ) ) this_COLON_2= RULE_COLON ( (lv_exp_3_0= ruleAssignmentExpression ) ) ) | ( () this_DEFAULT_5= RULE_DEFAULT this_COLON_6= RULE_COLON ( (lv_exp_7_0= ruleAssignmentExpression ) ) ) )
            int alt77=2;
            int LA77_0 = input.LA(1);

            if ( ((LA77_0>=RULE_ATOMIC && LA77_0<=RULE_IMAGINARY)||(LA77_0>=RULE_STRUCT && LA77_0<=RULE_ALIGNAS)) ) {
                alt77=1;
            }
            else if ( (LA77_0==RULE_DEFAULT) ) {
                alt77=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 77, 0, input);

                throw nvae;
            }
            switch (alt77) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4385:2: ( () ( (lv_type_1_0= ruleSpecifiedType ) ) this_COLON_2= RULE_COLON ( (lv_exp_3_0= ruleAssignmentExpression ) ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4385:2: ( () ( (lv_type_1_0= ruleSpecifiedType ) ) this_COLON_2= RULE_COLON ( (lv_exp_3_0= ruleAssignmentExpression ) ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4385:3: () ( (lv_type_1_0= ruleSpecifiedType ) ) this_COLON_2= RULE_COLON ( (lv_exp_3_0= ruleAssignmentExpression ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4385:3: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4386:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getGenericAssociationAccess().getGenericAssociationCaseAction_0_0(),
                                  current);
                          
                    }

                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4391:2: ( (lv_type_1_0= ruleSpecifiedType ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4392:1: (lv_type_1_0= ruleSpecifiedType )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4392:1: (lv_type_1_0= ruleSpecifiedType )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4393:3: lv_type_1_0= ruleSpecifiedType
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getGenericAssociationAccess().getTypeSpecifiedTypeParserRuleCall_0_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleSpecifiedType_in_ruleGenericAssociation9811);
                    lv_type_1_0=ruleSpecifiedType();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getGenericAssociationRule());
                      	        }
                             		set(
                             			current, 
                             			"type",
                              		lv_type_1_0, 
                              		"SpecifiedType");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    this_COLON_2=(Token)match(input,RULE_COLON,FOLLOW_RULE_COLON_in_ruleGenericAssociation9822); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_COLON_2, grammarAccess.getGenericAssociationAccess().getCOLONTerminalRuleCall_0_2()); 
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4413:1: ( (lv_exp_3_0= ruleAssignmentExpression ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4414:1: (lv_exp_3_0= ruleAssignmentExpression )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4414:1: (lv_exp_3_0= ruleAssignmentExpression )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4415:3: lv_exp_3_0= ruleAssignmentExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getGenericAssociationAccess().getExpAssignmentExpressionParserRuleCall_0_3_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleAssignmentExpression_in_ruleGenericAssociation9842);
                    lv_exp_3_0=ruleAssignmentExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getGenericAssociationRule());
                      	        }
                             		set(
                             			current, 
                             			"exp",
                              		lv_exp_3_0, 
                              		"AssignmentExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4432:6: ( () this_DEFAULT_5= RULE_DEFAULT this_COLON_6= RULE_COLON ( (lv_exp_7_0= ruleAssignmentExpression ) ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4432:6: ( () this_DEFAULT_5= RULE_DEFAULT this_COLON_6= RULE_COLON ( (lv_exp_7_0= ruleAssignmentExpression ) ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4432:7: () this_DEFAULT_5= RULE_DEFAULT this_COLON_6= RULE_COLON ( (lv_exp_7_0= ruleAssignmentExpression ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4432:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4433:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getGenericAssociationAccess().getGenericAssociationDefaultAction_1_0(),
                                  current);
                          
                    }

                    }

                    this_DEFAULT_5=(Token)match(input,RULE_DEFAULT,FOLLOW_RULE_DEFAULT_in_ruleGenericAssociation9870); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_DEFAULT_5, grammarAccess.getGenericAssociationAccess().getDEFAULTTerminalRuleCall_1_1()); 
                          
                    }
                    this_COLON_6=(Token)match(input,RULE_COLON,FOLLOW_RULE_COLON_in_ruleGenericAssociation9880); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_COLON_6, grammarAccess.getGenericAssociationAccess().getCOLONTerminalRuleCall_1_2()); 
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4446:1: ( (lv_exp_7_0= ruleAssignmentExpression ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4447:1: (lv_exp_7_0= ruleAssignmentExpression )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4447:1: (lv_exp_7_0= ruleAssignmentExpression )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4448:3: lv_exp_7_0= ruleAssignmentExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getGenericAssociationAccess().getExpAssignmentExpressionParserRuleCall_1_3_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleAssignmentExpression_in_ruleGenericAssociation9900);
                    lv_exp_7_0=ruleAssignmentExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getGenericAssociationRule());
                      	        }
                             		set(
                             			current, 
                             			"exp",
                              		lv_exp_7_0, 
                              		"AssignmentExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGenericAssociation"


    // $ANTLR start "entryRulePostfixExpressionPostfix"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4472:1: entryRulePostfixExpressionPostfix returns [EObject current=null] : iv_rulePostfixExpressionPostfix= rulePostfixExpressionPostfix EOF ;
    public final EObject entryRulePostfixExpressionPostfix() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePostfixExpressionPostfix = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4473:2: (iv_rulePostfixExpressionPostfix= rulePostfixExpressionPostfix EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4474:2: iv_rulePostfixExpressionPostfix= rulePostfixExpressionPostfix EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getPostfixExpressionPostfixRule()); 
            }
            pushFollow(FOLLOW_rulePostfixExpressionPostfix_in_entryRulePostfixExpressionPostfix9937);
            iv_rulePostfixExpressionPostfix=rulePostfixExpressionPostfix();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_rulePostfixExpressionPostfix; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRulePostfixExpressionPostfix9947); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePostfixExpressionPostfix"


    // $ANTLR start "rulePostfixExpressionPostfix"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4481:1: rulePostfixExpressionPostfix returns [EObject current=null] : ( ( () this_LBRACKET_1= RULE_LBRACKET ( (lv_value_2_0= ruleExpression ) ) this_RBRACKET_3= RULE_RBRACKET ) | ( () this_LPAREN_5= RULE_LPAREN ( ( (lv_left_6_0= ruleAssignmentExpression ) ) ( (lv_next_7_0= ruleNextAsignement ) )* )? this_RPAREN_8= RULE_RPAREN ) | ( () this_DOT_10= RULE_DOT ( (lv_value_11_0= RULE_IDENTIFIER ) ) ) | ( () this_PTR_OP_13= RULE_PTR_OP ( (lv_value_14_0= RULE_IDENTIFIER ) ) ) | ( () this_INC_OP_16= RULE_INC_OP ) | ( () this_DEC_OP_18= RULE_DEC_OP ) ) ;
    public final EObject rulePostfixExpressionPostfix() throws RecognitionException {
        EObject current = null;

        Token this_LBRACKET_1=null;
        Token this_RBRACKET_3=null;
        Token this_LPAREN_5=null;
        Token this_RPAREN_8=null;
        Token this_DOT_10=null;
        Token lv_value_11_0=null;
        Token this_PTR_OP_13=null;
        Token lv_value_14_0=null;
        Token this_INC_OP_16=null;
        Token this_DEC_OP_18=null;
        EObject lv_value_2_0 = null;

        EObject lv_left_6_0 = null;

        EObject lv_next_7_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4484:28: ( ( ( () this_LBRACKET_1= RULE_LBRACKET ( (lv_value_2_0= ruleExpression ) ) this_RBRACKET_3= RULE_RBRACKET ) | ( () this_LPAREN_5= RULE_LPAREN ( ( (lv_left_6_0= ruleAssignmentExpression ) ) ( (lv_next_7_0= ruleNextAsignement ) )* )? this_RPAREN_8= RULE_RPAREN ) | ( () this_DOT_10= RULE_DOT ( (lv_value_11_0= RULE_IDENTIFIER ) ) ) | ( () this_PTR_OP_13= RULE_PTR_OP ( (lv_value_14_0= RULE_IDENTIFIER ) ) ) | ( () this_INC_OP_16= RULE_INC_OP ) | ( () this_DEC_OP_18= RULE_DEC_OP ) ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4485:1: ( ( () this_LBRACKET_1= RULE_LBRACKET ( (lv_value_2_0= ruleExpression ) ) this_RBRACKET_3= RULE_RBRACKET ) | ( () this_LPAREN_5= RULE_LPAREN ( ( (lv_left_6_0= ruleAssignmentExpression ) ) ( (lv_next_7_0= ruleNextAsignement ) )* )? this_RPAREN_8= RULE_RPAREN ) | ( () this_DOT_10= RULE_DOT ( (lv_value_11_0= RULE_IDENTIFIER ) ) ) | ( () this_PTR_OP_13= RULE_PTR_OP ( (lv_value_14_0= RULE_IDENTIFIER ) ) ) | ( () this_INC_OP_16= RULE_INC_OP ) | ( () this_DEC_OP_18= RULE_DEC_OP ) )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4485:1: ( ( () this_LBRACKET_1= RULE_LBRACKET ( (lv_value_2_0= ruleExpression ) ) this_RBRACKET_3= RULE_RBRACKET ) | ( () this_LPAREN_5= RULE_LPAREN ( ( (lv_left_6_0= ruleAssignmentExpression ) ) ( (lv_next_7_0= ruleNextAsignement ) )* )? this_RPAREN_8= RULE_RPAREN ) | ( () this_DOT_10= RULE_DOT ( (lv_value_11_0= RULE_IDENTIFIER ) ) ) | ( () this_PTR_OP_13= RULE_PTR_OP ( (lv_value_14_0= RULE_IDENTIFIER ) ) ) | ( () this_INC_OP_16= RULE_INC_OP ) | ( () this_DEC_OP_18= RULE_DEC_OP ) )
            int alt80=6;
            switch ( input.LA(1) ) {
            case RULE_LBRACKET:
                {
                alt80=1;
                }
                break;
            case RULE_LPAREN:
                {
                alt80=2;
                }
                break;
            case RULE_DOT:
                {
                alt80=3;
                }
                break;
            case RULE_PTR_OP:
                {
                alt80=4;
                }
                break;
            case RULE_INC_OP:
                {
                alt80=5;
                }
                break;
            case RULE_DEC_OP:
                {
                alt80=6;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 80, 0, input);

                throw nvae;
            }

            switch (alt80) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4485:2: ( () this_LBRACKET_1= RULE_LBRACKET ( (lv_value_2_0= ruleExpression ) ) this_RBRACKET_3= RULE_RBRACKET )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4485:2: ( () this_LBRACKET_1= RULE_LBRACKET ( (lv_value_2_0= ruleExpression ) ) this_RBRACKET_3= RULE_RBRACKET )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4485:3: () this_LBRACKET_1= RULE_LBRACKET ( (lv_value_2_0= ruleExpression ) ) this_RBRACKET_3= RULE_RBRACKET
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4485:3: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4486:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getPostfixExpressionPostfixAccess().getPostfixExpressionPostfixExpressionAction_0_0(),
                                  current);
                          
                    }

                    }

                    this_LBRACKET_1=(Token)match(input,RULE_LBRACKET,FOLLOW_RULE_LBRACKET_in_rulePostfixExpressionPostfix9993); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_LBRACKET_1, grammarAccess.getPostfixExpressionPostfixAccess().getLBRACKETTerminalRuleCall_0_1()); 
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4495:1: ( (lv_value_2_0= ruleExpression ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4496:1: (lv_value_2_0= ruleExpression )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4496:1: (lv_value_2_0= ruleExpression )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4497:3: lv_value_2_0= ruleExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getPostfixExpressionPostfixAccess().getValueExpressionParserRuleCall_0_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleExpression_in_rulePostfixExpressionPostfix10013);
                    lv_value_2_0=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getPostfixExpressionPostfixRule());
                      	        }
                             		set(
                             			current, 
                             			"value",
                              		lv_value_2_0, 
                              		"Expression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    this_RBRACKET_3=(Token)match(input,RULE_RBRACKET,FOLLOW_RULE_RBRACKET_in_rulePostfixExpressionPostfix10024); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_RBRACKET_3, grammarAccess.getPostfixExpressionPostfixAccess().getRBRACKETTerminalRuleCall_0_3()); 
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4518:6: ( () this_LPAREN_5= RULE_LPAREN ( ( (lv_left_6_0= ruleAssignmentExpression ) ) ( (lv_next_7_0= ruleNextAsignement ) )* )? this_RPAREN_8= RULE_RPAREN )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4518:6: ( () this_LPAREN_5= RULE_LPAREN ( ( (lv_left_6_0= ruleAssignmentExpression ) ) ( (lv_next_7_0= ruleNextAsignement ) )* )? this_RPAREN_8= RULE_RPAREN )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4518:7: () this_LPAREN_5= RULE_LPAREN ( ( (lv_left_6_0= ruleAssignmentExpression ) ) ( (lv_next_7_0= ruleNextAsignement ) )* )? this_RPAREN_8= RULE_RPAREN
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4518:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4519:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getPostfixExpressionPostfixAccess().getPostfixExpressionPostfixArgumentListAction_1_0(),
                                  current);
                          
                    }

                    }

                    this_LPAREN_5=(Token)match(input,RULE_LPAREN,FOLLOW_RULE_LPAREN_in_rulePostfixExpressionPostfix10051); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_LPAREN_5, grammarAccess.getPostfixExpressionPostfixAccess().getLPARENTerminalRuleCall_1_1()); 
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4528:1: ( ( (lv_left_6_0= ruleAssignmentExpression ) ) ( (lv_next_7_0= ruleNextAsignement ) )* )?
                    int alt79=2;
                    int LA79_0 = input.LA(1);

                    if ( (LA79_0==RULE_MULT_OP||(LA79_0>=RULE_STRING_LITERAL && LA79_0<=RULE_IDENTIFIER)||LA79_0==RULE_LPAREN||LA79_0==RULE_LCBRACKET||LA79_0==RULE_AND_OP||(LA79_0>=RULE_ADD_OP && LA79_0<=RULE_MINUS_OP)||(LA79_0>=RULE_INC_OP && LA79_0<=RULE_FUNC_NAME)||(LA79_0>=RULE_TILDE && LA79_0<=RULE_NOT_OP)) ) {
                        alt79=1;
                    }
                    switch (alt79) {
                        case 1 :
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4528:2: ( (lv_left_6_0= ruleAssignmentExpression ) ) ( (lv_next_7_0= ruleNextAsignement ) )*
                            {
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4528:2: ( (lv_left_6_0= ruleAssignmentExpression ) )
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4529:1: (lv_left_6_0= ruleAssignmentExpression )
                            {
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4529:1: (lv_left_6_0= ruleAssignmentExpression )
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4530:3: lv_left_6_0= ruleAssignmentExpression
                            {
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getPostfixExpressionPostfixAccess().getLeftAssignmentExpressionParserRuleCall_1_2_0_0()); 
                              	    
                            }
                            pushFollow(FOLLOW_ruleAssignmentExpression_in_rulePostfixExpressionPostfix10072);
                            lv_left_6_0=ruleAssignmentExpression();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElementForParent(grammarAccess.getPostfixExpressionPostfixRule());
                              	        }
                                     		add(
                                     			current, 
                                     			"left",
                                      		lv_left_6_0, 
                                      		"AssignmentExpression");
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }


                            }

                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4546:2: ( (lv_next_7_0= ruleNextAsignement ) )*
                            loop78:
                            do {
                                int alt78=2;
                                int LA78_0 = input.LA(1);

                                if ( (LA78_0==RULE_COMMA) ) {
                                    alt78=1;
                                }


                                switch (alt78) {
                            	case 1 :
                            	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4547:1: (lv_next_7_0= ruleNextAsignement )
                            	    {
                            	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4547:1: (lv_next_7_0= ruleNextAsignement )
                            	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4548:3: lv_next_7_0= ruleNextAsignement
                            	    {
                            	    if ( state.backtracking==0 ) {
                            	       
                            	      	        newCompositeNode(grammarAccess.getPostfixExpressionPostfixAccess().getNextNextAsignementParserRuleCall_1_2_1_0()); 
                            	      	    
                            	    }
                            	    pushFollow(FOLLOW_ruleNextAsignement_in_rulePostfixExpressionPostfix10093);
                            	    lv_next_7_0=ruleNextAsignement();

                            	    state._fsp--;
                            	    if (state.failed) return current;
                            	    if ( state.backtracking==0 ) {

                            	      	        if (current==null) {
                            	      	            current = createModelElementForParent(grammarAccess.getPostfixExpressionPostfixRule());
                            	      	        }
                            	             		add(
                            	             			current, 
                            	             			"next",
                            	              		lv_next_7_0, 
                            	              		"NextAsignement");
                            	      	        afterParserOrEnumRuleCall();
                            	      	    
                            	    }

                            	    }


                            	    }
                            	    break;

                            	default :
                            	    break loop78;
                                }
                            } while (true);


                            }
                            break;

                    }

                    this_RPAREN_8=(Token)match(input,RULE_RPAREN,FOLLOW_RULE_RPAREN_in_rulePostfixExpressionPostfix10107); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_RPAREN_8, grammarAccess.getPostfixExpressionPostfixAccess().getRPARENTerminalRuleCall_1_3()); 
                          
                    }

                    }


                    }
                    break;
                case 3 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4569:6: ( () this_DOT_10= RULE_DOT ( (lv_value_11_0= RULE_IDENTIFIER ) ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4569:6: ( () this_DOT_10= RULE_DOT ( (lv_value_11_0= RULE_IDENTIFIER ) ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4569:7: () this_DOT_10= RULE_DOT ( (lv_value_11_0= RULE_IDENTIFIER ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4569:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4570:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getPostfixExpressionPostfixAccess().getPostfixExpressionPostfixAccessAction_2_0(),
                                  current);
                          
                    }

                    }

                    this_DOT_10=(Token)match(input,RULE_DOT,FOLLOW_RULE_DOT_in_rulePostfixExpressionPostfix10134); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_DOT_10, grammarAccess.getPostfixExpressionPostfixAccess().getDOTTerminalRuleCall_2_1()); 
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4579:1: ( (lv_value_11_0= RULE_IDENTIFIER ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4580:1: (lv_value_11_0= RULE_IDENTIFIER )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4580:1: (lv_value_11_0= RULE_IDENTIFIER )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4581:3: lv_value_11_0= RULE_IDENTIFIER
                    {
                    lv_value_11_0=(Token)match(input,RULE_IDENTIFIER,FOLLOW_RULE_IDENTIFIER_in_rulePostfixExpressionPostfix10150); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_value_11_0, grammarAccess.getPostfixExpressionPostfixAccess().getValueIDENTIFIERTerminalRuleCall_2_2_0()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getPostfixExpressionPostfixRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"value",
                              		lv_value_11_0, 
                              		"IDENTIFIER");
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;
                case 4 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4598:6: ( () this_PTR_OP_13= RULE_PTR_OP ( (lv_value_14_0= RULE_IDENTIFIER ) ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4598:6: ( () this_PTR_OP_13= RULE_PTR_OP ( (lv_value_14_0= RULE_IDENTIFIER ) ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4598:7: () this_PTR_OP_13= RULE_PTR_OP ( (lv_value_14_0= RULE_IDENTIFIER ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4598:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4599:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getPostfixExpressionPostfixAccess().getPostfixExpressionPostfixAccessPtrAction_3_0(),
                                  current);
                          
                    }

                    }

                    this_PTR_OP_13=(Token)match(input,RULE_PTR_OP,FOLLOW_RULE_PTR_OP_in_rulePostfixExpressionPostfix10183); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_PTR_OP_13, grammarAccess.getPostfixExpressionPostfixAccess().getPTR_OPTerminalRuleCall_3_1()); 
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4608:1: ( (lv_value_14_0= RULE_IDENTIFIER ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4609:1: (lv_value_14_0= RULE_IDENTIFIER )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4609:1: (lv_value_14_0= RULE_IDENTIFIER )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4610:3: lv_value_14_0= RULE_IDENTIFIER
                    {
                    lv_value_14_0=(Token)match(input,RULE_IDENTIFIER,FOLLOW_RULE_IDENTIFIER_in_rulePostfixExpressionPostfix10199); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_value_14_0, grammarAccess.getPostfixExpressionPostfixAccess().getValueIDENTIFIERTerminalRuleCall_3_2_0()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getPostfixExpressionPostfixRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"value",
                              		lv_value_14_0, 
                              		"IDENTIFIER");
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;
                case 5 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4627:6: ( () this_INC_OP_16= RULE_INC_OP )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4627:6: ( () this_INC_OP_16= RULE_INC_OP )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4627:7: () this_INC_OP_16= RULE_INC_OP
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4627:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4628:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getPostfixExpressionPostfixAccess().getPostfixExpressionPostfixIncrementAction_4_0(),
                                  current);
                          
                    }

                    }

                    this_INC_OP_16=(Token)match(input,RULE_INC_OP,FOLLOW_RULE_INC_OP_in_rulePostfixExpressionPostfix10232); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_INC_OP_16, grammarAccess.getPostfixExpressionPostfixAccess().getINC_OPTerminalRuleCall_4_1()); 
                          
                    }

                    }


                    }
                    break;
                case 6 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4638:6: ( () this_DEC_OP_18= RULE_DEC_OP )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4638:6: ( () this_DEC_OP_18= RULE_DEC_OP )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4638:7: () this_DEC_OP_18= RULE_DEC_OP
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4638:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4639:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getPostfixExpressionPostfixAccess().getPostfixExpressionPostfixDecrementAction_5_0(),
                                  current);
                          
                    }

                    }

                    this_DEC_OP_18=(Token)match(input,RULE_DEC_OP,FOLLOW_RULE_DEC_OP_in_rulePostfixExpressionPostfix10259); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_DEC_OP_18, grammarAccess.getPostfixExpressionPostfixAccess().getDEC_OPTerminalRuleCall_5_1()); 
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePostfixExpressionPostfix"


    // $ANTLR start "entryRuleNextAsignement"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4656:1: entryRuleNextAsignement returns [EObject current=null] : iv_ruleNextAsignement= ruleNextAsignement EOF ;
    public final EObject entryRuleNextAsignement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNextAsignement = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4657:2: (iv_ruleNextAsignement= ruleNextAsignement EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4658:2: iv_ruleNextAsignement= ruleNextAsignement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNextAsignementRule()); 
            }
            pushFollow(FOLLOW_ruleNextAsignement_in_entryRuleNextAsignement10295);
            iv_ruleNextAsignement=ruleNextAsignement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNextAsignement; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleNextAsignement10305); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNextAsignement"


    // $ANTLR start "ruleNextAsignement"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4665:1: ruleNextAsignement returns [EObject current=null] : (this_COMMA_0= RULE_COMMA ( (lv_right_1_0= ruleAssignmentExpression ) ) ) ;
    public final EObject ruleNextAsignement() throws RecognitionException {
        EObject current = null;

        Token this_COMMA_0=null;
        EObject lv_right_1_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4668:28: ( (this_COMMA_0= RULE_COMMA ( (lv_right_1_0= ruleAssignmentExpression ) ) ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4669:1: (this_COMMA_0= RULE_COMMA ( (lv_right_1_0= ruleAssignmentExpression ) ) )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4669:1: (this_COMMA_0= RULE_COMMA ( (lv_right_1_0= ruleAssignmentExpression ) ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4669:2: this_COMMA_0= RULE_COMMA ( (lv_right_1_0= ruleAssignmentExpression ) )
            {
            this_COMMA_0=(Token)match(input,RULE_COMMA,FOLLOW_RULE_COMMA_in_ruleNextAsignement10341); if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                  newLeafNode(this_COMMA_0, grammarAccess.getNextAsignementAccess().getCOMMATerminalRuleCall_0()); 
                  
            }
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4673:1: ( (lv_right_1_0= ruleAssignmentExpression ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4674:1: (lv_right_1_0= ruleAssignmentExpression )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4674:1: (lv_right_1_0= ruleAssignmentExpression )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4675:3: lv_right_1_0= ruleAssignmentExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getNextAsignementAccess().getRightAssignmentExpressionParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleAssignmentExpression_in_ruleNextAsignement10361);
            lv_right_1_0=ruleAssignmentExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getNextAsignementRule());
              	        }
                     		add(
                     			current, 
                     			"right",
                      		lv_right_1_0, 
                      		"AssignmentExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNextAsignement"


    // $ANTLR start "entryRuleCompoundStatement"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4699:1: entryRuleCompoundStatement returns [EObject current=null] : iv_ruleCompoundStatement= ruleCompoundStatement EOF ;
    public final EObject entryRuleCompoundStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCompoundStatement = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4700:2: (iv_ruleCompoundStatement= ruleCompoundStatement EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4701:2: iv_ruleCompoundStatement= ruleCompoundStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getCompoundStatementRule()); 
            }
            pushFollow(FOLLOW_ruleCompoundStatement_in_entryRuleCompoundStatement10397);
            iv_ruleCompoundStatement=ruleCompoundStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCompoundStatement; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleCompoundStatement10407); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCompoundStatement"


    // $ANTLR start "ruleCompoundStatement"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4708:1: ruleCompoundStatement returns [EObject current=null] : ( () this_LCBRACKET_1= RULE_LCBRACKET ( (lv_inner_2_0= ruleBlockItem ) )* this_RCBRACKET_3= RULE_RCBRACKET ) ;
    public final EObject ruleCompoundStatement() throws RecognitionException {
        EObject current = null;

        Token this_LCBRACKET_1=null;
        Token this_RCBRACKET_3=null;
        EObject lv_inner_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4711:28: ( ( () this_LCBRACKET_1= RULE_LCBRACKET ( (lv_inner_2_0= ruleBlockItem ) )* this_RCBRACKET_3= RULE_RCBRACKET ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4712:1: ( () this_LCBRACKET_1= RULE_LCBRACKET ( (lv_inner_2_0= ruleBlockItem ) )* this_RCBRACKET_3= RULE_RCBRACKET )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4712:1: ( () this_LCBRACKET_1= RULE_LCBRACKET ( (lv_inner_2_0= ruleBlockItem ) )* this_RCBRACKET_3= RULE_RCBRACKET )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4712:2: () this_LCBRACKET_1= RULE_LCBRACKET ( (lv_inner_2_0= ruleBlockItem ) )* this_RCBRACKET_3= RULE_RCBRACKET
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4712:2: ()
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4713:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getCompoundStatementAccess().getCompoundStatementAction_0(),
                          current);
                  
            }

            }

            this_LCBRACKET_1=(Token)match(input,RULE_LCBRACKET,FOLLOW_RULE_LCBRACKET_in_ruleCompoundStatement10452); if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                  newLeafNode(this_LCBRACKET_1, grammarAccess.getCompoundStatementAccess().getLCBRACKETTerminalRuleCall_1()); 
                  
            }
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4722:1: ( (lv_inner_2_0= ruleBlockItem ) )*
            loop81:
            do {
                int alt81=2;
                int LA81_0 = input.LA(1);

                if ( (LA81_0==RULE_MULT_OP||(LA81_0>=RULE_STRING_LITERAL && LA81_0<=RULE_IDENTIFIER)||(LA81_0>=RULE_TYPEDEF && LA81_0<=RULE_SEMICOLON)||LA81_0==RULE_LPAREN||(LA81_0>=RULE_STATIC_ASSERT && LA81_0<=RULE_LCBRACKET)||(LA81_0>=RULE_STRUCT && LA81_0<=RULE_ALIGNAS)||LA81_0==RULE_AND_OP||(LA81_0>=RULE_ADD_OP && LA81_0<=RULE_MINUS_OP)||(LA81_0>=RULE_INC_OP && LA81_0<=RULE_DEFAULT)||(LA81_0>=RULE_CASE && LA81_0<=RULE_IF)||(LA81_0>=RULE_SWITCH && LA81_0<=RULE_NOT_OP)) ) {
                    alt81=1;
                }


                switch (alt81) {
            	case 1 :
            	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4723:1: (lv_inner_2_0= ruleBlockItem )
            	    {
            	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4723:1: (lv_inner_2_0= ruleBlockItem )
            	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4724:3: lv_inner_2_0= ruleBlockItem
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getCompoundStatementAccess().getInnerBlockItemParserRuleCall_2_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleBlockItem_in_ruleCompoundStatement10472);
            	    lv_inner_2_0=ruleBlockItem();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getCompoundStatementRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"inner",
            	              		lv_inner_2_0, 
            	              		"BlockItem");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop81;
                }
            } while (true);

            this_RCBRACKET_3=(Token)match(input,RULE_RCBRACKET,FOLLOW_RULE_RCBRACKET_in_ruleCompoundStatement10484); if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                  newLeafNode(this_RCBRACKET_3, grammarAccess.getCompoundStatementAccess().getRCBRACKETTerminalRuleCall_3()); 
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCompoundStatement"


    // $ANTLR start "entryRuleBlockItem"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4752:1: entryRuleBlockItem returns [EObject current=null] : iv_ruleBlockItem= ruleBlockItem EOF ;
    public final EObject entryRuleBlockItem() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBlockItem = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4753:2: (iv_ruleBlockItem= ruleBlockItem EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4754:2: iv_ruleBlockItem= ruleBlockItem EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBlockItemRule()); 
            }
            pushFollow(FOLLOW_ruleBlockItem_in_entryRuleBlockItem10519);
            iv_ruleBlockItem=ruleBlockItem();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBlockItem; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleBlockItem10529); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBlockItem"


    // $ANTLR start "ruleBlockItem"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4761:1: ruleBlockItem returns [EObject current=null] : ( ( () ( (lv_decl_1_0= ruleDeclaration ) ) ) | ( () ( (lv_statement_3_0= ruleStatement ) ) ) ) ;
    public final EObject ruleBlockItem() throws RecognitionException {
        EObject current = null;

        EObject lv_decl_1_0 = null;

        EObject lv_statement_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4764:28: ( ( ( () ( (lv_decl_1_0= ruleDeclaration ) ) ) | ( () ( (lv_statement_3_0= ruleStatement ) ) ) ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4765:1: ( ( () ( (lv_decl_1_0= ruleDeclaration ) ) ) | ( () ( (lv_statement_3_0= ruleStatement ) ) ) )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4765:1: ( ( () ( (lv_decl_1_0= ruleDeclaration ) ) ) | ( () ( (lv_statement_3_0= ruleStatement ) ) ) )
            int alt82=2;
            int LA82_0 = input.LA(1);

            if ( (LA82_0==RULE_TYPEDEF||(LA82_0>=RULE_STATIC_ASSERT && LA82_0<=RULE_IMAGINARY)||(LA82_0>=RULE_STRUCT && LA82_0<=RULE_ALIGNAS)) ) {
                alt82=1;
            }
            else if ( (LA82_0==RULE_MULT_OP||(LA82_0>=RULE_STRING_LITERAL && LA82_0<=RULE_IDENTIFIER)||LA82_0==RULE_SEMICOLON||LA82_0==RULE_LPAREN||LA82_0==RULE_LCBRACKET||LA82_0==RULE_AND_OP||(LA82_0>=RULE_ADD_OP && LA82_0<=RULE_MINUS_OP)||(LA82_0>=RULE_INC_OP && LA82_0<=RULE_DEFAULT)||(LA82_0>=RULE_CASE && LA82_0<=RULE_IF)||(LA82_0>=RULE_SWITCH && LA82_0<=RULE_NOT_OP)) ) {
                alt82=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 82, 0, input);

                throw nvae;
            }
            switch (alt82) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4765:2: ( () ( (lv_decl_1_0= ruleDeclaration ) ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4765:2: ( () ( (lv_decl_1_0= ruleDeclaration ) ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4765:3: () ( (lv_decl_1_0= ruleDeclaration ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4765:3: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4766:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getBlockItemAccess().getStatementDeclarationAction_0_0(),
                                  current);
                          
                    }

                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4771:2: ( (lv_decl_1_0= ruleDeclaration ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4772:1: (lv_decl_1_0= ruleDeclaration )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4772:1: (lv_decl_1_0= ruleDeclaration )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4773:3: lv_decl_1_0= ruleDeclaration
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getBlockItemAccess().getDeclDeclarationParserRuleCall_0_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleDeclaration_in_ruleBlockItem10585);
                    lv_decl_1_0=ruleDeclaration();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getBlockItemRule());
                      	        }
                             		set(
                             			current, 
                             			"decl",
                              		lv_decl_1_0, 
                              		"Declaration");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4790:6: ( () ( (lv_statement_3_0= ruleStatement ) ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4790:6: ( () ( (lv_statement_3_0= ruleStatement ) ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4790:7: () ( (lv_statement_3_0= ruleStatement ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4790:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4791:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getBlockItemAccess().getStatementInnerStatementAction_1_0(),
                                  current);
                          
                    }

                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4796:2: ( (lv_statement_3_0= ruleStatement ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4797:1: (lv_statement_3_0= ruleStatement )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4797:1: (lv_statement_3_0= ruleStatement )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4798:3: lv_statement_3_0= ruleStatement
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getBlockItemAccess().getStatementStatementParserRuleCall_1_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleStatement_in_ruleBlockItem10623);
                    lv_statement_3_0=ruleStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getBlockItemRule());
                      	        }
                             		set(
                             			current, 
                             			"statement",
                              		lv_statement_3_0, 
                              		"Statement");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBlockItem"


    // $ANTLR start "entryRuleStatements"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4822:1: entryRuleStatements returns [EObject current=null] : iv_ruleStatements= ruleStatements EOF ;
    public final EObject entryRuleStatements() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStatements = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4823:2: (iv_ruleStatements= ruleStatements EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4824:2: iv_ruleStatements= ruleStatements EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStatementsRule()); 
            }
            pushFollow(FOLLOW_ruleStatements_in_entryRuleStatements10660);
            iv_ruleStatements=ruleStatements();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStatements; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleStatements10670); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStatements"


    // $ANTLR start "ruleStatements"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4831:1: ruleStatements returns [EObject current=null] : ( ( ( RULE_LCBRACKET )=>this_CompoundStatement_0= ruleCompoundStatement ) | this_LabeledStatement_1= ruleLabeledStatement | this_ExpressionStatement_2= ruleExpressionStatement | this_SelectionStatement_3= ruleSelectionStatement | this_IterationStatement_4= ruleIterationStatement | this_JumpStatement_5= ruleJumpStatement ) ;
    public final EObject ruleStatements() throws RecognitionException {
        EObject current = null;

        EObject this_CompoundStatement_0 = null;

        EObject this_LabeledStatement_1 = null;

        EObject this_ExpressionStatement_2 = null;

        EObject this_SelectionStatement_3 = null;

        EObject this_IterationStatement_4 = null;

        EObject this_JumpStatement_5 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4834:28: ( ( ( ( RULE_LCBRACKET )=>this_CompoundStatement_0= ruleCompoundStatement ) | this_LabeledStatement_1= ruleLabeledStatement | this_ExpressionStatement_2= ruleExpressionStatement | this_SelectionStatement_3= ruleSelectionStatement | this_IterationStatement_4= ruleIterationStatement | this_JumpStatement_5= ruleJumpStatement ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4835:1: ( ( ( RULE_LCBRACKET )=>this_CompoundStatement_0= ruleCompoundStatement ) | this_LabeledStatement_1= ruleLabeledStatement | this_ExpressionStatement_2= ruleExpressionStatement | this_SelectionStatement_3= ruleSelectionStatement | this_IterationStatement_4= ruleIterationStatement | this_JumpStatement_5= ruleJumpStatement )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4835:1: ( ( ( RULE_LCBRACKET )=>this_CompoundStatement_0= ruleCompoundStatement ) | this_LabeledStatement_1= ruleLabeledStatement | this_ExpressionStatement_2= ruleExpressionStatement | this_SelectionStatement_3= ruleSelectionStatement | this_IterationStatement_4= ruleIterationStatement | this_JumpStatement_5= ruleJumpStatement )
            int alt83=6;
            alt83 = dfa83.predict(input);
            switch (alt83) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4835:2: ( ( RULE_LCBRACKET )=>this_CompoundStatement_0= ruleCompoundStatement )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4835:2: ( ( RULE_LCBRACKET )=>this_CompoundStatement_0= ruleCompoundStatement )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4835:3: ( RULE_LCBRACKET )=>this_CompoundStatement_0= ruleCompoundStatement
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getStatementsAccess().getCompoundStatementParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleCompoundStatement_in_ruleStatements10723);
                    this_CompoundStatement_0=ruleCompoundStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_CompoundStatement_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4846:5: this_LabeledStatement_1= ruleLabeledStatement
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getStatementsAccess().getLabeledStatementParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleLabeledStatement_in_ruleStatements10751);
                    this_LabeledStatement_1=ruleLabeledStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_LabeledStatement_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4856:5: this_ExpressionStatement_2= ruleExpressionStatement
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getStatementsAccess().getExpressionStatementParserRuleCall_2()); 
                          
                    }
                    pushFollow(FOLLOW_ruleExpressionStatement_in_ruleStatements10778);
                    this_ExpressionStatement_2=ruleExpressionStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ExpressionStatement_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 4 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4866:5: this_SelectionStatement_3= ruleSelectionStatement
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getStatementsAccess().getSelectionStatementParserRuleCall_3()); 
                          
                    }
                    pushFollow(FOLLOW_ruleSelectionStatement_in_ruleStatements10805);
                    this_SelectionStatement_3=ruleSelectionStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_SelectionStatement_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 5 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4876:5: this_IterationStatement_4= ruleIterationStatement
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getStatementsAccess().getIterationStatementParserRuleCall_4()); 
                          
                    }
                    pushFollow(FOLLOW_ruleIterationStatement_in_ruleStatements10832);
                    this_IterationStatement_4=ruleIterationStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_IterationStatement_4; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 6 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4886:5: this_JumpStatement_5= ruleJumpStatement
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getStatementsAccess().getJumpStatementParserRuleCall_5()); 
                          
                    }
                    pushFollow(FOLLOW_ruleJumpStatement_in_ruleStatements10859);
                    this_JumpStatement_5=ruleJumpStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_JumpStatement_5; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStatements"


    // $ANTLR start "entryRuleLabeledStatement"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4902:1: entryRuleLabeledStatement returns [EObject current=null] : iv_ruleLabeledStatement= ruleLabeledStatement EOF ;
    public final EObject entryRuleLabeledStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLabeledStatement = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4903:2: (iv_ruleLabeledStatement= ruleLabeledStatement EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4904:2: iv_ruleLabeledStatement= ruleLabeledStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLabeledStatementRule()); 
            }
            pushFollow(FOLLOW_ruleLabeledStatement_in_entryRuleLabeledStatement10894);
            iv_ruleLabeledStatement=ruleLabeledStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLabeledStatement; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleLabeledStatement10904); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLabeledStatement"


    // $ANTLR start "ruleLabeledStatement"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4911:1: ruleLabeledStatement returns [EObject current=null] : ( ( () this_CASE_1= RULE_CASE ( (lv_exp_2_0= ruleBaseExpression ) ) this_COLON_3= RULE_COLON ( (lv_statement_4_0= ruleStatement ) ) ) | ( () this_DEFAULT_6= RULE_DEFAULT this_COLON_7= RULE_COLON ( (lv_statement_8_0= ruleStatement ) ) ) ) ;
    public final EObject ruleLabeledStatement() throws RecognitionException {
        EObject current = null;

        Token this_CASE_1=null;
        Token this_COLON_3=null;
        Token this_DEFAULT_6=null;
        Token this_COLON_7=null;
        EObject lv_exp_2_0 = null;

        EObject lv_statement_4_0 = null;

        EObject lv_statement_8_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4914:28: ( ( ( () this_CASE_1= RULE_CASE ( (lv_exp_2_0= ruleBaseExpression ) ) this_COLON_3= RULE_COLON ( (lv_statement_4_0= ruleStatement ) ) ) | ( () this_DEFAULT_6= RULE_DEFAULT this_COLON_7= RULE_COLON ( (lv_statement_8_0= ruleStatement ) ) ) ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4915:1: ( ( () this_CASE_1= RULE_CASE ( (lv_exp_2_0= ruleBaseExpression ) ) this_COLON_3= RULE_COLON ( (lv_statement_4_0= ruleStatement ) ) ) | ( () this_DEFAULT_6= RULE_DEFAULT this_COLON_7= RULE_COLON ( (lv_statement_8_0= ruleStatement ) ) ) )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4915:1: ( ( () this_CASE_1= RULE_CASE ( (lv_exp_2_0= ruleBaseExpression ) ) this_COLON_3= RULE_COLON ( (lv_statement_4_0= ruleStatement ) ) ) | ( () this_DEFAULT_6= RULE_DEFAULT this_COLON_7= RULE_COLON ( (lv_statement_8_0= ruleStatement ) ) ) )
            int alt84=2;
            int LA84_0 = input.LA(1);

            if ( (LA84_0==RULE_CASE) ) {
                alt84=1;
            }
            else if ( (LA84_0==RULE_DEFAULT) ) {
                alt84=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 84, 0, input);

                throw nvae;
            }
            switch (alt84) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4915:2: ( () this_CASE_1= RULE_CASE ( (lv_exp_2_0= ruleBaseExpression ) ) this_COLON_3= RULE_COLON ( (lv_statement_4_0= ruleStatement ) ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4915:2: ( () this_CASE_1= RULE_CASE ( (lv_exp_2_0= ruleBaseExpression ) ) this_COLON_3= RULE_COLON ( (lv_statement_4_0= ruleStatement ) ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4915:3: () this_CASE_1= RULE_CASE ( (lv_exp_2_0= ruleBaseExpression ) ) this_COLON_3= RULE_COLON ( (lv_statement_4_0= ruleStatement ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4915:3: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4916:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getLabeledStatementAccess().getLabeledStatementCaseAction_0_0(),
                                  current);
                          
                    }

                    }

                    this_CASE_1=(Token)match(input,RULE_CASE,FOLLOW_RULE_CASE_in_ruleLabeledStatement10950); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_CASE_1, grammarAccess.getLabeledStatementAccess().getCASETerminalRuleCall_0_1()); 
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4925:1: ( (lv_exp_2_0= ruleBaseExpression ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4926:1: (lv_exp_2_0= ruleBaseExpression )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4926:1: (lv_exp_2_0= ruleBaseExpression )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4927:3: lv_exp_2_0= ruleBaseExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getLabeledStatementAccess().getExpBaseExpressionParserRuleCall_0_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleBaseExpression_in_ruleLabeledStatement10970);
                    lv_exp_2_0=ruleBaseExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getLabeledStatementRule());
                      	        }
                             		set(
                             			current, 
                             			"exp",
                              		lv_exp_2_0, 
                              		"BaseExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    this_COLON_3=(Token)match(input,RULE_COLON,FOLLOW_RULE_COLON_in_ruleLabeledStatement10981); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_COLON_3, grammarAccess.getLabeledStatementAccess().getCOLONTerminalRuleCall_0_3()); 
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4947:1: ( (lv_statement_4_0= ruleStatement ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4948:1: (lv_statement_4_0= ruleStatement )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4948:1: (lv_statement_4_0= ruleStatement )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4949:3: lv_statement_4_0= ruleStatement
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getLabeledStatementAccess().getStatementStatementParserRuleCall_0_4_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleStatement_in_ruleLabeledStatement11001);
                    lv_statement_4_0=ruleStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getLabeledStatementRule());
                      	        }
                             		set(
                             			current, 
                             			"statement",
                              		lv_statement_4_0, 
                              		"Statement");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4966:6: ( () this_DEFAULT_6= RULE_DEFAULT this_COLON_7= RULE_COLON ( (lv_statement_8_0= ruleStatement ) ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4966:6: ( () this_DEFAULT_6= RULE_DEFAULT this_COLON_7= RULE_COLON ( (lv_statement_8_0= ruleStatement ) ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4966:7: () this_DEFAULT_6= RULE_DEFAULT this_COLON_7= RULE_COLON ( (lv_statement_8_0= ruleStatement ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4966:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4967:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getLabeledStatementAccess().getLabeledStatementDefaultAction_1_0(),
                                  current);
                          
                    }

                    }

                    this_DEFAULT_6=(Token)match(input,RULE_DEFAULT,FOLLOW_RULE_DEFAULT_in_ruleLabeledStatement11029); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_DEFAULT_6, grammarAccess.getLabeledStatementAccess().getDEFAULTTerminalRuleCall_1_1()); 
                          
                    }
                    this_COLON_7=(Token)match(input,RULE_COLON,FOLLOW_RULE_COLON_in_ruleLabeledStatement11039); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_COLON_7, grammarAccess.getLabeledStatementAccess().getCOLONTerminalRuleCall_1_2()); 
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4980:1: ( (lv_statement_8_0= ruleStatement ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4981:1: (lv_statement_8_0= ruleStatement )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4981:1: (lv_statement_8_0= ruleStatement )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4982:3: lv_statement_8_0= ruleStatement
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getLabeledStatementAccess().getStatementStatementParserRuleCall_1_3_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleStatement_in_ruleLabeledStatement11059);
                    lv_statement_8_0=ruleStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getLabeledStatementRule());
                      	        }
                             		set(
                             			current, 
                             			"statement",
                              		lv_statement_8_0, 
                              		"Statement");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLabeledStatement"


    // $ANTLR start "entryRuleStatement"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5006:1: entryRuleStatement returns [EObject current=null] : iv_ruleStatement= ruleStatement EOF ;
    public final EObject entryRuleStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStatement = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5007:2: (iv_ruleStatement= ruleStatement EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5008:2: iv_ruleStatement= ruleStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStatementRule()); 
            }
            pushFollow(FOLLOW_ruleStatement_in_entryRuleStatement11096);
            iv_ruleStatement=ruleStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStatement; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleStatement11106); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStatement"


    // $ANTLR start "ruleStatement"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5015:1: ruleStatement returns [EObject current=null] : ( ( () ( (lv_goto_1_0= RULE_IDENTIFIER ) ) this_COLON_2= RULE_COLON ( (lv_statement_3_0= ruleStatement ) ) ) | this_Statements_4= ruleStatements ) ;
    public final EObject ruleStatement() throws RecognitionException {
        EObject current = null;

        Token lv_goto_1_0=null;
        Token this_COLON_2=null;
        EObject lv_statement_3_0 = null;

        EObject this_Statements_4 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5018:28: ( ( ( () ( (lv_goto_1_0= RULE_IDENTIFIER ) ) this_COLON_2= RULE_COLON ( (lv_statement_3_0= ruleStatement ) ) ) | this_Statements_4= ruleStatements ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5019:1: ( ( () ( (lv_goto_1_0= RULE_IDENTIFIER ) ) this_COLON_2= RULE_COLON ( (lv_statement_3_0= ruleStatement ) ) ) | this_Statements_4= ruleStatements )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5019:1: ( ( () ( (lv_goto_1_0= RULE_IDENTIFIER ) ) this_COLON_2= RULE_COLON ( (lv_statement_3_0= ruleStatement ) ) ) | this_Statements_4= ruleStatements )
            int alt85=2;
            int LA85_0 = input.LA(1);

            if ( (LA85_0==RULE_IDENTIFIER) ) {
                int LA85_1 = input.LA(2);

                if ( (LA85_1==RULE_COLON) ) {
                    alt85=1;
                }
                else if ( ((LA85_1>=RULE_L_OP && LA85_1<=RULE_G_OP)||LA85_1==RULE_DOT||LA85_1==RULE_SEMICOLON||LA85_1==RULE_LPAREN||LA85_1==RULE_LBRACKET||LA85_1==RULE_ASSIGN||LA85_1==RULE_Q_OP||(LA85_1>=RULE_LOR_OP && LA85_1<=RULE_DEC_OP)||LA85_1==RULE_PTR_OP) ) {
                    alt85=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 85, 1, input);

                    throw nvae;
                }
            }
            else if ( (LA85_0==RULE_MULT_OP||LA85_0==RULE_STRING_LITERAL||LA85_0==RULE_SEMICOLON||LA85_0==RULE_LPAREN||LA85_0==RULE_LCBRACKET||LA85_0==RULE_AND_OP||(LA85_0>=RULE_ADD_OP && LA85_0<=RULE_MINUS_OP)||(LA85_0>=RULE_INC_OP && LA85_0<=RULE_DEFAULT)||(LA85_0>=RULE_CASE && LA85_0<=RULE_IF)||(LA85_0>=RULE_SWITCH && LA85_0<=RULE_NOT_OP)) ) {
                alt85=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 85, 0, input);

                throw nvae;
            }
            switch (alt85) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5019:2: ( () ( (lv_goto_1_0= RULE_IDENTIFIER ) ) this_COLON_2= RULE_COLON ( (lv_statement_3_0= ruleStatement ) ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5019:2: ( () ( (lv_goto_1_0= RULE_IDENTIFIER ) ) this_COLON_2= RULE_COLON ( (lv_statement_3_0= ruleStatement ) ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5019:3: () ( (lv_goto_1_0= RULE_IDENTIFIER ) ) this_COLON_2= RULE_COLON ( (lv_statement_3_0= ruleStatement ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5019:3: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5020:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getStatementAccess().getGoToPrefixStatementAction_0_0(),
                                  current);
                          
                    }

                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5025:2: ( (lv_goto_1_0= RULE_IDENTIFIER ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5026:1: (lv_goto_1_0= RULE_IDENTIFIER )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5026:1: (lv_goto_1_0= RULE_IDENTIFIER )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5027:3: lv_goto_1_0= RULE_IDENTIFIER
                    {
                    lv_goto_1_0=(Token)match(input,RULE_IDENTIFIER,FOLLOW_RULE_IDENTIFIER_in_ruleStatement11158); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_goto_1_0, grammarAccess.getStatementAccess().getGotoIDENTIFIERTerminalRuleCall_0_1_0()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getStatementRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"goto",
                              		lv_goto_1_0, 
                              		"IDENTIFIER");
                      	    
                    }

                    }


                    }

                    this_COLON_2=(Token)match(input,RULE_COLON,FOLLOW_RULE_COLON_in_ruleStatement11174); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_COLON_2, grammarAccess.getStatementAccess().getCOLONTerminalRuleCall_0_2()); 
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5047:1: ( (lv_statement_3_0= ruleStatement ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5048:1: (lv_statement_3_0= ruleStatement )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5048:1: (lv_statement_3_0= ruleStatement )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5049:3: lv_statement_3_0= ruleStatement
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getStatementAccess().getStatementStatementParserRuleCall_0_3_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleStatement_in_ruleStatement11194);
                    lv_statement_3_0=ruleStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getStatementRule());
                      	        }
                             		set(
                             			current, 
                             			"statement",
                              		lv_statement_3_0, 
                              		"Statement");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5067:5: this_Statements_4= ruleStatements
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getStatementAccess().getStatementsParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleStatements_in_ruleStatement11223);
                    this_Statements_4=ruleStatements();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Statements_4; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStatement"


    // $ANTLR start "entryRuleExpressionStatement"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5083:1: entryRuleExpressionStatement returns [EObject current=null] : iv_ruleExpressionStatement= ruleExpressionStatement EOF ;
    public final EObject entryRuleExpressionStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpressionStatement = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5084:2: (iv_ruleExpressionStatement= ruleExpressionStatement EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5085:2: iv_ruleExpressionStatement= ruleExpressionStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExpressionStatementRule()); 
            }
            pushFollow(FOLLOW_ruleExpressionStatement_in_entryRuleExpressionStatement11258);
            iv_ruleExpressionStatement=ruleExpressionStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExpressionStatement; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleExpressionStatement11268); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpressionStatement"


    // $ANTLR start "ruleExpressionStatement"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5092:1: ruleExpressionStatement returns [EObject current=null] : ( () ( (lv_exp_1_0= ruleExpression ) )? this_SEMICOLON_2= RULE_SEMICOLON ) ;
    public final EObject ruleExpressionStatement() throws RecognitionException {
        EObject current = null;

        Token this_SEMICOLON_2=null;
        EObject lv_exp_1_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5095:28: ( ( () ( (lv_exp_1_0= ruleExpression ) )? this_SEMICOLON_2= RULE_SEMICOLON ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5096:1: ( () ( (lv_exp_1_0= ruleExpression ) )? this_SEMICOLON_2= RULE_SEMICOLON )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5096:1: ( () ( (lv_exp_1_0= ruleExpression ) )? this_SEMICOLON_2= RULE_SEMICOLON )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5096:2: () ( (lv_exp_1_0= ruleExpression ) )? this_SEMICOLON_2= RULE_SEMICOLON
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5096:2: ()
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5097:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getExpressionStatementAccess().getExpressionStatementAction_0(),
                          current);
                  
            }

            }

            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5102:2: ( (lv_exp_1_0= ruleExpression ) )?
            int alt86=2;
            int LA86_0 = input.LA(1);

            if ( (LA86_0==RULE_MULT_OP||(LA86_0>=RULE_STRING_LITERAL && LA86_0<=RULE_IDENTIFIER)||LA86_0==RULE_LPAREN||LA86_0==RULE_LCBRACKET||LA86_0==RULE_AND_OP||(LA86_0>=RULE_ADD_OP && LA86_0<=RULE_MINUS_OP)||(LA86_0>=RULE_INC_OP && LA86_0<=RULE_FUNC_NAME)||(LA86_0>=RULE_TILDE && LA86_0<=RULE_NOT_OP)) ) {
                alt86=1;
            }
            switch (alt86) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5103:1: (lv_exp_1_0= ruleExpression )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5103:1: (lv_exp_1_0= ruleExpression )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5104:3: lv_exp_1_0= ruleExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getExpressionStatementAccess().getExpExpressionParserRuleCall_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleExpression_in_ruleExpressionStatement11323);
                    lv_exp_1_0=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getExpressionStatementRule());
                      	        }
                             		set(
                             			current, 
                             			"exp",
                              		lv_exp_1_0, 
                              		"Expression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }

            this_SEMICOLON_2=(Token)match(input,RULE_SEMICOLON,FOLLOW_RULE_SEMICOLON_in_ruleExpressionStatement11335); if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                  newLeafNode(this_SEMICOLON_2, grammarAccess.getExpressionStatementAccess().getSEMICOLONTerminalRuleCall_2()); 
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpressionStatement"


    // $ANTLR start "entryRuleSelectionStatement"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5132:1: entryRuleSelectionStatement returns [EObject current=null] : iv_ruleSelectionStatement= ruleSelectionStatement EOF ;
    public final EObject entryRuleSelectionStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSelectionStatement = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5133:2: (iv_ruleSelectionStatement= ruleSelectionStatement EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5134:2: iv_ruleSelectionStatement= ruleSelectionStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSelectionStatementRule()); 
            }
            pushFollow(FOLLOW_ruleSelectionStatement_in_entryRuleSelectionStatement11370);
            iv_ruleSelectionStatement=ruleSelectionStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSelectionStatement; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleSelectionStatement11380); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSelectionStatement"


    // $ANTLR start "ruleSelectionStatement"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5141:1: ruleSelectionStatement returns [EObject current=null] : ( ( () this_IF_1= RULE_IF this_LPAREN_2= RULE_LPAREN ( (lv_exp_3_0= ruleExpression ) ) this_RPAREN_4= RULE_RPAREN ( (lv_then_statement_5_0= ruleStatement ) ) ( ( ( RULE_ELSE )=>this_ELSE_6= RULE_ELSE ) ( (lv_else_statement_7_0= ruleStatement ) ) )? ) | ( () this_SWITCH_9= RULE_SWITCH this_LPAREN_10= RULE_LPAREN ( (lv_exp_11_0= ruleExpression ) ) this_RPAREN_12= RULE_RPAREN this_LCBRACKET_13= RULE_LCBRACKET ( (lv_content_14_0= ruleLabeledStatement ) )* this_RCBRACKET_15= RULE_RCBRACKET ) ) ;
    public final EObject ruleSelectionStatement() throws RecognitionException {
        EObject current = null;

        Token this_IF_1=null;
        Token this_LPAREN_2=null;
        Token this_RPAREN_4=null;
        Token this_ELSE_6=null;
        Token this_SWITCH_9=null;
        Token this_LPAREN_10=null;
        Token this_RPAREN_12=null;
        Token this_LCBRACKET_13=null;
        Token this_RCBRACKET_15=null;
        EObject lv_exp_3_0 = null;

        EObject lv_then_statement_5_0 = null;

        EObject lv_else_statement_7_0 = null;

        EObject lv_exp_11_0 = null;

        EObject lv_content_14_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5144:28: ( ( ( () this_IF_1= RULE_IF this_LPAREN_2= RULE_LPAREN ( (lv_exp_3_0= ruleExpression ) ) this_RPAREN_4= RULE_RPAREN ( (lv_then_statement_5_0= ruleStatement ) ) ( ( ( RULE_ELSE )=>this_ELSE_6= RULE_ELSE ) ( (lv_else_statement_7_0= ruleStatement ) ) )? ) | ( () this_SWITCH_9= RULE_SWITCH this_LPAREN_10= RULE_LPAREN ( (lv_exp_11_0= ruleExpression ) ) this_RPAREN_12= RULE_RPAREN this_LCBRACKET_13= RULE_LCBRACKET ( (lv_content_14_0= ruleLabeledStatement ) )* this_RCBRACKET_15= RULE_RCBRACKET ) ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5145:1: ( ( () this_IF_1= RULE_IF this_LPAREN_2= RULE_LPAREN ( (lv_exp_3_0= ruleExpression ) ) this_RPAREN_4= RULE_RPAREN ( (lv_then_statement_5_0= ruleStatement ) ) ( ( ( RULE_ELSE )=>this_ELSE_6= RULE_ELSE ) ( (lv_else_statement_7_0= ruleStatement ) ) )? ) | ( () this_SWITCH_9= RULE_SWITCH this_LPAREN_10= RULE_LPAREN ( (lv_exp_11_0= ruleExpression ) ) this_RPAREN_12= RULE_RPAREN this_LCBRACKET_13= RULE_LCBRACKET ( (lv_content_14_0= ruleLabeledStatement ) )* this_RCBRACKET_15= RULE_RCBRACKET ) )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5145:1: ( ( () this_IF_1= RULE_IF this_LPAREN_2= RULE_LPAREN ( (lv_exp_3_0= ruleExpression ) ) this_RPAREN_4= RULE_RPAREN ( (lv_then_statement_5_0= ruleStatement ) ) ( ( ( RULE_ELSE )=>this_ELSE_6= RULE_ELSE ) ( (lv_else_statement_7_0= ruleStatement ) ) )? ) | ( () this_SWITCH_9= RULE_SWITCH this_LPAREN_10= RULE_LPAREN ( (lv_exp_11_0= ruleExpression ) ) this_RPAREN_12= RULE_RPAREN this_LCBRACKET_13= RULE_LCBRACKET ( (lv_content_14_0= ruleLabeledStatement ) )* this_RCBRACKET_15= RULE_RCBRACKET ) )
            int alt89=2;
            int LA89_0 = input.LA(1);

            if ( (LA89_0==RULE_IF) ) {
                alt89=1;
            }
            else if ( (LA89_0==RULE_SWITCH) ) {
                alt89=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 89, 0, input);

                throw nvae;
            }
            switch (alt89) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5145:2: ( () this_IF_1= RULE_IF this_LPAREN_2= RULE_LPAREN ( (lv_exp_3_0= ruleExpression ) ) this_RPAREN_4= RULE_RPAREN ( (lv_then_statement_5_0= ruleStatement ) ) ( ( ( RULE_ELSE )=>this_ELSE_6= RULE_ELSE ) ( (lv_else_statement_7_0= ruleStatement ) ) )? )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5145:2: ( () this_IF_1= RULE_IF this_LPAREN_2= RULE_LPAREN ( (lv_exp_3_0= ruleExpression ) ) this_RPAREN_4= RULE_RPAREN ( (lv_then_statement_5_0= ruleStatement ) ) ( ( ( RULE_ELSE )=>this_ELSE_6= RULE_ELSE ) ( (lv_else_statement_7_0= ruleStatement ) ) )? )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5145:3: () this_IF_1= RULE_IF this_LPAREN_2= RULE_LPAREN ( (lv_exp_3_0= ruleExpression ) ) this_RPAREN_4= RULE_RPAREN ( (lv_then_statement_5_0= ruleStatement ) ) ( ( ( RULE_ELSE )=>this_ELSE_6= RULE_ELSE ) ( (lv_else_statement_7_0= ruleStatement ) ) )?
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5145:3: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5146:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getSelectionStatementAccess().getSelectionStatementIfAction_0_0(),
                                  current);
                          
                    }

                    }

                    this_IF_1=(Token)match(input,RULE_IF,FOLLOW_RULE_IF_in_ruleSelectionStatement11426); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_IF_1, grammarAccess.getSelectionStatementAccess().getIFTerminalRuleCall_0_1()); 
                          
                    }
                    this_LPAREN_2=(Token)match(input,RULE_LPAREN,FOLLOW_RULE_LPAREN_in_ruleSelectionStatement11436); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_LPAREN_2, grammarAccess.getSelectionStatementAccess().getLPARENTerminalRuleCall_0_2()); 
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5159:1: ( (lv_exp_3_0= ruleExpression ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5160:1: (lv_exp_3_0= ruleExpression )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5160:1: (lv_exp_3_0= ruleExpression )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5161:3: lv_exp_3_0= ruleExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getSelectionStatementAccess().getExpExpressionParserRuleCall_0_3_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleExpression_in_ruleSelectionStatement11456);
                    lv_exp_3_0=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getSelectionStatementRule());
                      	        }
                             		set(
                             			current, 
                             			"exp",
                              		lv_exp_3_0, 
                              		"Expression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    this_RPAREN_4=(Token)match(input,RULE_RPAREN,FOLLOW_RULE_RPAREN_in_ruleSelectionStatement11467); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_RPAREN_4, grammarAccess.getSelectionStatementAccess().getRPARENTerminalRuleCall_0_4()); 
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5181:1: ( (lv_then_statement_5_0= ruleStatement ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5182:1: (lv_then_statement_5_0= ruleStatement )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5182:1: (lv_then_statement_5_0= ruleStatement )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5183:3: lv_then_statement_5_0= ruleStatement
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getSelectionStatementAccess().getThen_statementStatementParserRuleCall_0_5_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleStatement_in_ruleSelectionStatement11487);
                    lv_then_statement_5_0=ruleStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getSelectionStatementRule());
                      	        }
                             		set(
                             			current, 
                             			"then_statement",
                              		lv_then_statement_5_0, 
                              		"Statement");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5199:2: ( ( ( RULE_ELSE )=>this_ELSE_6= RULE_ELSE ) ( (lv_else_statement_7_0= ruleStatement ) ) )?
                    int alt87=2;
                    int LA87_0 = input.LA(1);

                    if ( (LA87_0==RULE_ELSE) ) {
                        int LA87_1 = input.LA(2);

                        if ( (synpred5_InternalClang()) ) {
                            alt87=1;
                        }
                    }
                    switch (alt87) {
                        case 1 :
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5199:3: ( ( RULE_ELSE )=>this_ELSE_6= RULE_ELSE ) ( (lv_else_statement_7_0= ruleStatement ) )
                            {
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5199:3: ( ( RULE_ELSE )=>this_ELSE_6= RULE_ELSE )
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5199:4: ( RULE_ELSE )=>this_ELSE_6= RULE_ELSE
                            {
                            this_ELSE_6=(Token)match(input,RULE_ELSE,FOLLOW_RULE_ELSE_in_ruleSelectionStatement11505); if (state.failed) return current;
                            if ( state.backtracking==0 ) {
                               
                                  newLeafNode(this_ELSE_6, grammarAccess.getSelectionStatementAccess().getELSETerminalRuleCall_0_6_0()); 
                                  
                            }

                            }

                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5203:2: ( (lv_else_statement_7_0= ruleStatement ) )
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5204:1: (lv_else_statement_7_0= ruleStatement )
                            {
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5204:1: (lv_else_statement_7_0= ruleStatement )
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5205:3: lv_else_statement_7_0= ruleStatement
                            {
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getSelectionStatementAccess().getElse_statementStatementParserRuleCall_0_6_1_0()); 
                              	    
                            }
                            pushFollow(FOLLOW_ruleStatement_in_ruleSelectionStatement11526);
                            lv_else_statement_7_0=ruleStatement();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElementForParent(grammarAccess.getSelectionStatementRule());
                              	        }
                                     		set(
                                     			current, 
                                     			"else_statement",
                                      		lv_else_statement_7_0, 
                                      		"Statement");
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }


                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5222:6: ( () this_SWITCH_9= RULE_SWITCH this_LPAREN_10= RULE_LPAREN ( (lv_exp_11_0= ruleExpression ) ) this_RPAREN_12= RULE_RPAREN this_LCBRACKET_13= RULE_LCBRACKET ( (lv_content_14_0= ruleLabeledStatement ) )* this_RCBRACKET_15= RULE_RCBRACKET )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5222:6: ( () this_SWITCH_9= RULE_SWITCH this_LPAREN_10= RULE_LPAREN ( (lv_exp_11_0= ruleExpression ) ) this_RPAREN_12= RULE_RPAREN this_LCBRACKET_13= RULE_LCBRACKET ( (lv_content_14_0= ruleLabeledStatement ) )* this_RCBRACKET_15= RULE_RCBRACKET )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5222:7: () this_SWITCH_9= RULE_SWITCH this_LPAREN_10= RULE_LPAREN ( (lv_exp_11_0= ruleExpression ) ) this_RPAREN_12= RULE_RPAREN this_LCBRACKET_13= RULE_LCBRACKET ( (lv_content_14_0= ruleLabeledStatement ) )* this_RCBRACKET_15= RULE_RCBRACKET
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5222:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5223:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getSelectionStatementAccess().getSelectionStatementSwitchAction_1_0(),
                                  current);
                          
                    }

                    }

                    this_SWITCH_9=(Token)match(input,RULE_SWITCH,FOLLOW_RULE_SWITCH_in_ruleSelectionStatement11556); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_SWITCH_9, grammarAccess.getSelectionStatementAccess().getSWITCHTerminalRuleCall_1_1()); 
                          
                    }
                    this_LPAREN_10=(Token)match(input,RULE_LPAREN,FOLLOW_RULE_LPAREN_in_ruleSelectionStatement11566); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_LPAREN_10, grammarAccess.getSelectionStatementAccess().getLPARENTerminalRuleCall_1_2()); 
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5236:1: ( (lv_exp_11_0= ruleExpression ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5237:1: (lv_exp_11_0= ruleExpression )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5237:1: (lv_exp_11_0= ruleExpression )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5238:3: lv_exp_11_0= ruleExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getSelectionStatementAccess().getExpExpressionParserRuleCall_1_3_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleExpression_in_ruleSelectionStatement11586);
                    lv_exp_11_0=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getSelectionStatementRule());
                      	        }
                             		set(
                             			current, 
                             			"exp",
                              		lv_exp_11_0, 
                              		"Expression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    this_RPAREN_12=(Token)match(input,RULE_RPAREN,FOLLOW_RULE_RPAREN_in_ruleSelectionStatement11597); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_RPAREN_12, grammarAccess.getSelectionStatementAccess().getRPARENTerminalRuleCall_1_4()); 
                          
                    }
                    this_LCBRACKET_13=(Token)match(input,RULE_LCBRACKET,FOLLOW_RULE_LCBRACKET_in_ruleSelectionStatement11607); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_LCBRACKET_13, grammarAccess.getSelectionStatementAccess().getLCBRACKETTerminalRuleCall_1_5()); 
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5262:1: ( (lv_content_14_0= ruleLabeledStatement ) )*
                    loop88:
                    do {
                        int alt88=2;
                        int LA88_0 = input.LA(1);

                        if ( (LA88_0==RULE_DEFAULT||LA88_0==RULE_CASE) ) {
                            alt88=1;
                        }


                        switch (alt88) {
                    	case 1 :
                    	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5263:1: (lv_content_14_0= ruleLabeledStatement )
                    	    {
                    	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5263:1: (lv_content_14_0= ruleLabeledStatement )
                    	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5264:3: lv_content_14_0= ruleLabeledStatement
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getSelectionStatementAccess().getContentLabeledStatementParserRuleCall_1_6_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FOLLOW_ruleLabeledStatement_in_ruleSelectionStatement11627);
                    	    lv_content_14_0=ruleLabeledStatement();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getSelectionStatementRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"content",
                    	              		lv_content_14_0, 
                    	              		"LabeledStatement");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop88;
                        }
                    } while (true);

                    this_RCBRACKET_15=(Token)match(input,RULE_RCBRACKET,FOLLOW_RULE_RCBRACKET_in_ruleSelectionStatement11639); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_RCBRACKET_15, grammarAccess.getSelectionStatementAccess().getRCBRACKETTerminalRuleCall_1_7()); 
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSelectionStatement"


    // $ANTLR start "entryRuleIterationStatement"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5292:1: entryRuleIterationStatement returns [EObject current=null] : iv_ruleIterationStatement= ruleIterationStatement EOF ;
    public final EObject entryRuleIterationStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIterationStatement = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5293:2: (iv_ruleIterationStatement= ruleIterationStatement EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5294:2: iv_ruleIterationStatement= ruleIterationStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIterationStatementRule()); 
            }
            pushFollow(FOLLOW_ruleIterationStatement_in_entryRuleIterationStatement11675);
            iv_ruleIterationStatement=ruleIterationStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIterationStatement; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleIterationStatement11685); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIterationStatement"


    // $ANTLR start "ruleIterationStatement"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5301:1: ruleIterationStatement returns [EObject current=null] : ( ( () this_WHILE_1= RULE_WHILE this_LPAREN_2= RULE_LPAREN ( (lv_exp_3_0= ruleExpression ) ) this_RPAREN_4= RULE_RPAREN ( (lv_statement_5_0= ruleStatement ) ) ) | ( () this_DO_7= RULE_DO ( (lv_statement_8_0= ruleStatement ) ) this_WHILE_9= RULE_WHILE this_LPAREN_10= RULE_LPAREN ( (lv_exp_11_0= ruleExpression ) ) this_RPAREN_12= RULE_RPAREN this_SEMICOLON_13= RULE_SEMICOLON ) | ( () this_FOR_15= RULE_FOR this_LPAREN_16= RULE_LPAREN ( ( (lv_field1_17_0= ruleExpressionStatement ) ) | ( (lv_decl_18_0= ruleDeclaration ) ) ) ( (lv_field2_19_0= ruleExpressionStatement ) ) ( ( (lv_field3_20_0= ruleExpression ) ) ( (lv_next_21_0= ruleNextField ) )* )? this_RPAREN_22= RULE_RPAREN ( (lv_statement_23_0= ruleStatement ) ) ) ) ;
    public final EObject ruleIterationStatement() throws RecognitionException {
        EObject current = null;

        Token this_WHILE_1=null;
        Token this_LPAREN_2=null;
        Token this_RPAREN_4=null;
        Token this_DO_7=null;
        Token this_WHILE_9=null;
        Token this_LPAREN_10=null;
        Token this_RPAREN_12=null;
        Token this_SEMICOLON_13=null;
        Token this_FOR_15=null;
        Token this_LPAREN_16=null;
        Token this_RPAREN_22=null;
        EObject lv_exp_3_0 = null;

        EObject lv_statement_5_0 = null;

        EObject lv_statement_8_0 = null;

        EObject lv_exp_11_0 = null;

        EObject lv_field1_17_0 = null;

        EObject lv_decl_18_0 = null;

        EObject lv_field2_19_0 = null;

        EObject lv_field3_20_0 = null;

        EObject lv_next_21_0 = null;

        EObject lv_statement_23_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5304:28: ( ( ( () this_WHILE_1= RULE_WHILE this_LPAREN_2= RULE_LPAREN ( (lv_exp_3_0= ruleExpression ) ) this_RPAREN_4= RULE_RPAREN ( (lv_statement_5_0= ruleStatement ) ) ) | ( () this_DO_7= RULE_DO ( (lv_statement_8_0= ruleStatement ) ) this_WHILE_9= RULE_WHILE this_LPAREN_10= RULE_LPAREN ( (lv_exp_11_0= ruleExpression ) ) this_RPAREN_12= RULE_RPAREN this_SEMICOLON_13= RULE_SEMICOLON ) | ( () this_FOR_15= RULE_FOR this_LPAREN_16= RULE_LPAREN ( ( (lv_field1_17_0= ruleExpressionStatement ) ) | ( (lv_decl_18_0= ruleDeclaration ) ) ) ( (lv_field2_19_0= ruleExpressionStatement ) ) ( ( (lv_field3_20_0= ruleExpression ) ) ( (lv_next_21_0= ruleNextField ) )* )? this_RPAREN_22= RULE_RPAREN ( (lv_statement_23_0= ruleStatement ) ) ) ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5305:1: ( ( () this_WHILE_1= RULE_WHILE this_LPAREN_2= RULE_LPAREN ( (lv_exp_3_0= ruleExpression ) ) this_RPAREN_4= RULE_RPAREN ( (lv_statement_5_0= ruleStatement ) ) ) | ( () this_DO_7= RULE_DO ( (lv_statement_8_0= ruleStatement ) ) this_WHILE_9= RULE_WHILE this_LPAREN_10= RULE_LPAREN ( (lv_exp_11_0= ruleExpression ) ) this_RPAREN_12= RULE_RPAREN this_SEMICOLON_13= RULE_SEMICOLON ) | ( () this_FOR_15= RULE_FOR this_LPAREN_16= RULE_LPAREN ( ( (lv_field1_17_0= ruleExpressionStatement ) ) | ( (lv_decl_18_0= ruleDeclaration ) ) ) ( (lv_field2_19_0= ruleExpressionStatement ) ) ( ( (lv_field3_20_0= ruleExpression ) ) ( (lv_next_21_0= ruleNextField ) )* )? this_RPAREN_22= RULE_RPAREN ( (lv_statement_23_0= ruleStatement ) ) ) )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5305:1: ( ( () this_WHILE_1= RULE_WHILE this_LPAREN_2= RULE_LPAREN ( (lv_exp_3_0= ruleExpression ) ) this_RPAREN_4= RULE_RPAREN ( (lv_statement_5_0= ruleStatement ) ) ) | ( () this_DO_7= RULE_DO ( (lv_statement_8_0= ruleStatement ) ) this_WHILE_9= RULE_WHILE this_LPAREN_10= RULE_LPAREN ( (lv_exp_11_0= ruleExpression ) ) this_RPAREN_12= RULE_RPAREN this_SEMICOLON_13= RULE_SEMICOLON ) | ( () this_FOR_15= RULE_FOR this_LPAREN_16= RULE_LPAREN ( ( (lv_field1_17_0= ruleExpressionStatement ) ) | ( (lv_decl_18_0= ruleDeclaration ) ) ) ( (lv_field2_19_0= ruleExpressionStatement ) ) ( ( (lv_field3_20_0= ruleExpression ) ) ( (lv_next_21_0= ruleNextField ) )* )? this_RPAREN_22= RULE_RPAREN ( (lv_statement_23_0= ruleStatement ) ) ) )
            int alt93=3;
            switch ( input.LA(1) ) {
            case RULE_WHILE:
                {
                alt93=1;
                }
                break;
            case RULE_DO:
                {
                alt93=2;
                }
                break;
            case RULE_FOR:
                {
                alt93=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 93, 0, input);

                throw nvae;
            }

            switch (alt93) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5305:2: ( () this_WHILE_1= RULE_WHILE this_LPAREN_2= RULE_LPAREN ( (lv_exp_3_0= ruleExpression ) ) this_RPAREN_4= RULE_RPAREN ( (lv_statement_5_0= ruleStatement ) ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5305:2: ( () this_WHILE_1= RULE_WHILE this_LPAREN_2= RULE_LPAREN ( (lv_exp_3_0= ruleExpression ) ) this_RPAREN_4= RULE_RPAREN ( (lv_statement_5_0= ruleStatement ) ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5305:3: () this_WHILE_1= RULE_WHILE this_LPAREN_2= RULE_LPAREN ( (lv_exp_3_0= ruleExpression ) ) this_RPAREN_4= RULE_RPAREN ( (lv_statement_5_0= ruleStatement ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5305:3: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5306:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getIterationStatementAccess().getIterationStatementWhileAction_0_0(),
                                  current);
                          
                    }

                    }

                    this_WHILE_1=(Token)match(input,RULE_WHILE,FOLLOW_RULE_WHILE_in_ruleIterationStatement11731); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_WHILE_1, grammarAccess.getIterationStatementAccess().getWHILETerminalRuleCall_0_1()); 
                          
                    }
                    this_LPAREN_2=(Token)match(input,RULE_LPAREN,FOLLOW_RULE_LPAREN_in_ruleIterationStatement11741); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_LPAREN_2, grammarAccess.getIterationStatementAccess().getLPARENTerminalRuleCall_0_2()); 
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5319:1: ( (lv_exp_3_0= ruleExpression ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5320:1: (lv_exp_3_0= ruleExpression )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5320:1: (lv_exp_3_0= ruleExpression )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5321:3: lv_exp_3_0= ruleExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getIterationStatementAccess().getExpExpressionParserRuleCall_0_3_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleExpression_in_ruleIterationStatement11761);
                    lv_exp_3_0=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getIterationStatementRule());
                      	        }
                             		set(
                             			current, 
                             			"exp",
                              		lv_exp_3_0, 
                              		"Expression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    this_RPAREN_4=(Token)match(input,RULE_RPAREN,FOLLOW_RULE_RPAREN_in_ruleIterationStatement11772); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_RPAREN_4, grammarAccess.getIterationStatementAccess().getRPARENTerminalRuleCall_0_4()); 
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5341:1: ( (lv_statement_5_0= ruleStatement ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5342:1: (lv_statement_5_0= ruleStatement )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5342:1: (lv_statement_5_0= ruleStatement )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5343:3: lv_statement_5_0= ruleStatement
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getIterationStatementAccess().getStatementStatementParserRuleCall_0_5_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleStatement_in_ruleIterationStatement11792);
                    lv_statement_5_0=ruleStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getIterationStatementRule());
                      	        }
                             		set(
                             			current, 
                             			"statement",
                              		lv_statement_5_0, 
                              		"Statement");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5360:6: ( () this_DO_7= RULE_DO ( (lv_statement_8_0= ruleStatement ) ) this_WHILE_9= RULE_WHILE this_LPAREN_10= RULE_LPAREN ( (lv_exp_11_0= ruleExpression ) ) this_RPAREN_12= RULE_RPAREN this_SEMICOLON_13= RULE_SEMICOLON )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5360:6: ( () this_DO_7= RULE_DO ( (lv_statement_8_0= ruleStatement ) ) this_WHILE_9= RULE_WHILE this_LPAREN_10= RULE_LPAREN ( (lv_exp_11_0= ruleExpression ) ) this_RPAREN_12= RULE_RPAREN this_SEMICOLON_13= RULE_SEMICOLON )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5360:7: () this_DO_7= RULE_DO ( (lv_statement_8_0= ruleStatement ) ) this_WHILE_9= RULE_WHILE this_LPAREN_10= RULE_LPAREN ( (lv_exp_11_0= ruleExpression ) ) this_RPAREN_12= RULE_RPAREN this_SEMICOLON_13= RULE_SEMICOLON
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5360:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5361:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getIterationStatementAccess().getIterationStatementDoAction_1_0(),
                                  current);
                          
                    }

                    }

                    this_DO_7=(Token)match(input,RULE_DO,FOLLOW_RULE_DO_in_ruleIterationStatement11820); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_DO_7, grammarAccess.getIterationStatementAccess().getDOTerminalRuleCall_1_1()); 
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5370:1: ( (lv_statement_8_0= ruleStatement ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5371:1: (lv_statement_8_0= ruleStatement )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5371:1: (lv_statement_8_0= ruleStatement )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5372:3: lv_statement_8_0= ruleStatement
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getIterationStatementAccess().getStatementStatementParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleStatement_in_ruleIterationStatement11840);
                    lv_statement_8_0=ruleStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getIterationStatementRule());
                      	        }
                             		set(
                             			current, 
                             			"statement",
                              		lv_statement_8_0, 
                              		"Statement");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    this_WHILE_9=(Token)match(input,RULE_WHILE,FOLLOW_RULE_WHILE_in_ruleIterationStatement11851); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_WHILE_9, grammarAccess.getIterationStatementAccess().getWHILETerminalRuleCall_1_3()); 
                          
                    }
                    this_LPAREN_10=(Token)match(input,RULE_LPAREN,FOLLOW_RULE_LPAREN_in_ruleIterationStatement11861); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_LPAREN_10, grammarAccess.getIterationStatementAccess().getLPARENTerminalRuleCall_1_4()); 
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5396:1: ( (lv_exp_11_0= ruleExpression ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5397:1: (lv_exp_11_0= ruleExpression )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5397:1: (lv_exp_11_0= ruleExpression )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5398:3: lv_exp_11_0= ruleExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getIterationStatementAccess().getExpExpressionParserRuleCall_1_5_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleExpression_in_ruleIterationStatement11881);
                    lv_exp_11_0=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getIterationStatementRule());
                      	        }
                             		set(
                             			current, 
                             			"exp",
                              		lv_exp_11_0, 
                              		"Expression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    this_RPAREN_12=(Token)match(input,RULE_RPAREN,FOLLOW_RULE_RPAREN_in_ruleIterationStatement11892); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_RPAREN_12, grammarAccess.getIterationStatementAccess().getRPARENTerminalRuleCall_1_6()); 
                          
                    }
                    this_SEMICOLON_13=(Token)match(input,RULE_SEMICOLON,FOLLOW_RULE_SEMICOLON_in_ruleIterationStatement11902); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_SEMICOLON_13, grammarAccess.getIterationStatementAccess().getSEMICOLONTerminalRuleCall_1_7()); 
                          
                    }

                    }


                    }
                    break;
                case 3 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5423:6: ( () this_FOR_15= RULE_FOR this_LPAREN_16= RULE_LPAREN ( ( (lv_field1_17_0= ruleExpressionStatement ) ) | ( (lv_decl_18_0= ruleDeclaration ) ) ) ( (lv_field2_19_0= ruleExpressionStatement ) ) ( ( (lv_field3_20_0= ruleExpression ) ) ( (lv_next_21_0= ruleNextField ) )* )? this_RPAREN_22= RULE_RPAREN ( (lv_statement_23_0= ruleStatement ) ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5423:6: ( () this_FOR_15= RULE_FOR this_LPAREN_16= RULE_LPAREN ( ( (lv_field1_17_0= ruleExpressionStatement ) ) | ( (lv_decl_18_0= ruleDeclaration ) ) ) ( (lv_field2_19_0= ruleExpressionStatement ) ) ( ( (lv_field3_20_0= ruleExpression ) ) ( (lv_next_21_0= ruleNextField ) )* )? this_RPAREN_22= RULE_RPAREN ( (lv_statement_23_0= ruleStatement ) ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5423:7: () this_FOR_15= RULE_FOR this_LPAREN_16= RULE_LPAREN ( ( (lv_field1_17_0= ruleExpressionStatement ) ) | ( (lv_decl_18_0= ruleDeclaration ) ) ) ( (lv_field2_19_0= ruleExpressionStatement ) ) ( ( (lv_field3_20_0= ruleExpression ) ) ( (lv_next_21_0= ruleNextField ) )* )? this_RPAREN_22= RULE_RPAREN ( (lv_statement_23_0= ruleStatement ) )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5423:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5424:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getIterationStatementAccess().getIterationStatementForAction_2_0(),
                                  current);
                          
                    }

                    }

                    this_FOR_15=(Token)match(input,RULE_FOR,FOLLOW_RULE_FOR_in_ruleIterationStatement11929); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_FOR_15, grammarAccess.getIterationStatementAccess().getFORTerminalRuleCall_2_1()); 
                          
                    }
                    this_LPAREN_16=(Token)match(input,RULE_LPAREN,FOLLOW_RULE_LPAREN_in_ruleIterationStatement11939); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_LPAREN_16, grammarAccess.getIterationStatementAccess().getLPARENTerminalRuleCall_2_2()); 
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5437:1: ( ( (lv_field1_17_0= ruleExpressionStatement ) ) | ( (lv_decl_18_0= ruleDeclaration ) ) )
                    int alt90=2;
                    int LA90_0 = input.LA(1);

                    if ( (LA90_0==RULE_MULT_OP||(LA90_0>=RULE_STRING_LITERAL && LA90_0<=RULE_IDENTIFIER)||LA90_0==RULE_SEMICOLON||LA90_0==RULE_LPAREN||LA90_0==RULE_LCBRACKET||LA90_0==RULE_AND_OP||(LA90_0>=RULE_ADD_OP && LA90_0<=RULE_MINUS_OP)||(LA90_0>=RULE_INC_OP && LA90_0<=RULE_FUNC_NAME)||(LA90_0>=RULE_TILDE && LA90_0<=RULE_NOT_OP)) ) {
                        alt90=1;
                    }
                    else if ( (LA90_0==RULE_TYPEDEF||(LA90_0>=RULE_STATIC_ASSERT && LA90_0<=RULE_IMAGINARY)||(LA90_0>=RULE_STRUCT && LA90_0<=RULE_ALIGNAS)) ) {
                        alt90=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 90, 0, input);

                        throw nvae;
                    }
                    switch (alt90) {
                        case 1 :
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5437:2: ( (lv_field1_17_0= ruleExpressionStatement ) )
                            {
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5437:2: ( (lv_field1_17_0= ruleExpressionStatement ) )
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5438:1: (lv_field1_17_0= ruleExpressionStatement )
                            {
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5438:1: (lv_field1_17_0= ruleExpressionStatement )
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5439:3: lv_field1_17_0= ruleExpressionStatement
                            {
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getIterationStatementAccess().getField1ExpressionStatementParserRuleCall_2_3_0_0()); 
                              	    
                            }
                            pushFollow(FOLLOW_ruleExpressionStatement_in_ruleIterationStatement11960);
                            lv_field1_17_0=ruleExpressionStatement();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElementForParent(grammarAccess.getIterationStatementRule());
                              	        }
                                     		set(
                                     			current, 
                                     			"field1",
                                      		lv_field1_17_0, 
                                      		"ExpressionStatement");
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }


                            }


                            }
                            break;
                        case 2 :
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5456:6: ( (lv_decl_18_0= ruleDeclaration ) )
                            {
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5456:6: ( (lv_decl_18_0= ruleDeclaration ) )
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5457:1: (lv_decl_18_0= ruleDeclaration )
                            {
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5457:1: (lv_decl_18_0= ruleDeclaration )
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5458:3: lv_decl_18_0= ruleDeclaration
                            {
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getIterationStatementAccess().getDeclDeclarationParserRuleCall_2_3_1_0()); 
                              	    
                            }
                            pushFollow(FOLLOW_ruleDeclaration_in_ruleIterationStatement11987);
                            lv_decl_18_0=ruleDeclaration();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElementForParent(grammarAccess.getIterationStatementRule());
                              	        }
                                     		set(
                                     			current, 
                                     			"decl",
                                      		lv_decl_18_0, 
                                      		"Declaration");
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }


                            }


                            }
                            break;

                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5474:3: ( (lv_field2_19_0= ruleExpressionStatement ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5475:1: (lv_field2_19_0= ruleExpressionStatement )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5475:1: (lv_field2_19_0= ruleExpressionStatement )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5476:3: lv_field2_19_0= ruleExpressionStatement
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getIterationStatementAccess().getField2ExpressionStatementParserRuleCall_2_4_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleExpressionStatement_in_ruleIterationStatement12009);
                    lv_field2_19_0=ruleExpressionStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getIterationStatementRule());
                      	        }
                             		set(
                             			current, 
                             			"field2",
                              		lv_field2_19_0, 
                              		"ExpressionStatement");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5492:2: ( ( (lv_field3_20_0= ruleExpression ) ) ( (lv_next_21_0= ruleNextField ) )* )?
                    int alt92=2;
                    int LA92_0 = input.LA(1);

                    if ( (LA92_0==RULE_MULT_OP||(LA92_0>=RULE_STRING_LITERAL && LA92_0<=RULE_IDENTIFIER)||LA92_0==RULE_LPAREN||LA92_0==RULE_LCBRACKET||LA92_0==RULE_AND_OP||(LA92_0>=RULE_ADD_OP && LA92_0<=RULE_MINUS_OP)||(LA92_0>=RULE_INC_OP && LA92_0<=RULE_FUNC_NAME)||(LA92_0>=RULE_TILDE && LA92_0<=RULE_NOT_OP)) ) {
                        alt92=1;
                    }
                    switch (alt92) {
                        case 1 :
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5492:3: ( (lv_field3_20_0= ruleExpression ) ) ( (lv_next_21_0= ruleNextField ) )*
                            {
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5492:3: ( (lv_field3_20_0= ruleExpression ) )
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5493:1: (lv_field3_20_0= ruleExpression )
                            {
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5493:1: (lv_field3_20_0= ruleExpression )
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5494:3: lv_field3_20_0= ruleExpression
                            {
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getIterationStatementAccess().getField3ExpressionParserRuleCall_2_5_0_0()); 
                              	    
                            }
                            pushFollow(FOLLOW_ruleExpression_in_ruleIterationStatement12031);
                            lv_field3_20_0=ruleExpression();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElementForParent(grammarAccess.getIterationStatementRule());
                              	        }
                                     		set(
                                     			current, 
                                     			"field3",
                                      		lv_field3_20_0, 
                                      		"Expression");
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }


                            }

                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5510:2: ( (lv_next_21_0= ruleNextField ) )*
                            loop91:
                            do {
                                int alt91=2;
                                int LA91_0 = input.LA(1);

                                if ( (LA91_0==RULE_COMMA) ) {
                                    alt91=1;
                                }


                                switch (alt91) {
                            	case 1 :
                            	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5511:1: (lv_next_21_0= ruleNextField )
                            	    {
                            	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5511:1: (lv_next_21_0= ruleNextField )
                            	    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5512:3: lv_next_21_0= ruleNextField
                            	    {
                            	    if ( state.backtracking==0 ) {
                            	       
                            	      	        newCompositeNode(grammarAccess.getIterationStatementAccess().getNextNextFieldParserRuleCall_2_5_1_0()); 
                            	      	    
                            	    }
                            	    pushFollow(FOLLOW_ruleNextField_in_ruleIterationStatement12052);
                            	    lv_next_21_0=ruleNextField();

                            	    state._fsp--;
                            	    if (state.failed) return current;
                            	    if ( state.backtracking==0 ) {

                            	      	        if (current==null) {
                            	      	            current = createModelElementForParent(grammarAccess.getIterationStatementRule());
                            	      	        }
                            	             		add(
                            	             			current, 
                            	             			"next",
                            	              		lv_next_21_0, 
                            	              		"NextField");
                            	      	        afterParserOrEnumRuleCall();
                            	      	    
                            	    }

                            	    }


                            	    }
                            	    break;

                            	default :
                            	    break loop91;
                                }
                            } while (true);


                            }
                            break;

                    }

                    this_RPAREN_22=(Token)match(input,RULE_RPAREN,FOLLOW_RULE_RPAREN_in_ruleIterationStatement12066); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_RPAREN_22, grammarAccess.getIterationStatementAccess().getRPARENTerminalRuleCall_2_6()); 
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5532:1: ( (lv_statement_23_0= ruleStatement ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5533:1: (lv_statement_23_0= ruleStatement )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5533:1: (lv_statement_23_0= ruleStatement )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5534:3: lv_statement_23_0= ruleStatement
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getIterationStatementAccess().getStatementStatementParserRuleCall_2_7_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleStatement_in_ruleIterationStatement12086);
                    lv_statement_23_0=ruleStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getIterationStatementRule());
                      	        }
                             		set(
                             			current, 
                             			"statement",
                              		lv_statement_23_0, 
                              		"Statement");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIterationStatement"


    // $ANTLR start "entryRuleNextField"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5558:1: entryRuleNextField returns [EObject current=null] : iv_ruleNextField= ruleNextField EOF ;
    public final EObject entryRuleNextField() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNextField = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5559:2: (iv_ruleNextField= ruleNextField EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5560:2: iv_ruleNextField= ruleNextField EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNextFieldRule()); 
            }
            pushFollow(FOLLOW_ruleNextField_in_entryRuleNextField12123);
            iv_ruleNextField=ruleNextField();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNextField; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleNextField12133); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNextField"


    // $ANTLR start "ruleNextField"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5567:1: ruleNextField returns [EObject current=null] : (this_COMMA_0= RULE_COMMA ( (lv_next_1_0= ruleAssignmentExpression ) ) ) ;
    public final EObject ruleNextField() throws RecognitionException {
        EObject current = null;

        Token this_COMMA_0=null;
        EObject lv_next_1_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5570:28: ( (this_COMMA_0= RULE_COMMA ( (lv_next_1_0= ruleAssignmentExpression ) ) ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5571:1: (this_COMMA_0= RULE_COMMA ( (lv_next_1_0= ruleAssignmentExpression ) ) )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5571:1: (this_COMMA_0= RULE_COMMA ( (lv_next_1_0= ruleAssignmentExpression ) ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5571:2: this_COMMA_0= RULE_COMMA ( (lv_next_1_0= ruleAssignmentExpression ) )
            {
            this_COMMA_0=(Token)match(input,RULE_COMMA,FOLLOW_RULE_COMMA_in_ruleNextField12169); if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                  newLeafNode(this_COMMA_0, grammarAccess.getNextFieldAccess().getCOMMATerminalRuleCall_0()); 
                  
            }
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5575:1: ( (lv_next_1_0= ruleAssignmentExpression ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5576:1: (lv_next_1_0= ruleAssignmentExpression )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5576:1: (lv_next_1_0= ruleAssignmentExpression )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5577:3: lv_next_1_0= ruleAssignmentExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getNextFieldAccess().getNextAssignmentExpressionParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleAssignmentExpression_in_ruleNextField12189);
            lv_next_1_0=ruleAssignmentExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getNextFieldRule());
              	        }
                     		set(
                     			current, 
                     			"next",
                      		lv_next_1_0, 
                      		"AssignmentExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNextField"


    // $ANTLR start "entryRuleJumpStatement"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5601:1: entryRuleJumpStatement returns [EObject current=null] : iv_ruleJumpStatement= ruleJumpStatement EOF ;
    public final EObject entryRuleJumpStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleJumpStatement = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5602:2: (iv_ruleJumpStatement= ruleJumpStatement EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5603:2: iv_ruleJumpStatement= ruleJumpStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getJumpStatementRule()); 
            }
            pushFollow(FOLLOW_ruleJumpStatement_in_entryRuleJumpStatement12225);
            iv_ruleJumpStatement=ruleJumpStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleJumpStatement; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleJumpStatement12235); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleJumpStatement"


    // $ANTLR start "ruleJumpStatement"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5610:1: ruleJumpStatement returns [EObject current=null] : ( ( () this_GOTO_1= RULE_GOTO ( (lv_name_2_0= RULE_IDENTIFIER ) ) this_SEMICOLON_3= RULE_SEMICOLON ) | ( () this_CONTINUE_5= RULE_CONTINUE this_SEMICOLON_6= RULE_SEMICOLON ) | ( () this_BREAK_8= RULE_BREAK this_SEMICOLON_9= RULE_SEMICOLON ) | ( () this_RETURN_11= RULE_RETURN ( (lv_exp_12_0= ruleExpression ) )? this_SEMICOLON_13= RULE_SEMICOLON ) ) ;
    public final EObject ruleJumpStatement() throws RecognitionException {
        EObject current = null;

        Token this_GOTO_1=null;
        Token lv_name_2_0=null;
        Token this_SEMICOLON_3=null;
        Token this_CONTINUE_5=null;
        Token this_SEMICOLON_6=null;
        Token this_BREAK_8=null;
        Token this_SEMICOLON_9=null;
        Token this_RETURN_11=null;
        Token this_SEMICOLON_13=null;
        EObject lv_exp_12_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5613:28: ( ( ( () this_GOTO_1= RULE_GOTO ( (lv_name_2_0= RULE_IDENTIFIER ) ) this_SEMICOLON_3= RULE_SEMICOLON ) | ( () this_CONTINUE_5= RULE_CONTINUE this_SEMICOLON_6= RULE_SEMICOLON ) | ( () this_BREAK_8= RULE_BREAK this_SEMICOLON_9= RULE_SEMICOLON ) | ( () this_RETURN_11= RULE_RETURN ( (lv_exp_12_0= ruleExpression ) )? this_SEMICOLON_13= RULE_SEMICOLON ) ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5614:1: ( ( () this_GOTO_1= RULE_GOTO ( (lv_name_2_0= RULE_IDENTIFIER ) ) this_SEMICOLON_3= RULE_SEMICOLON ) | ( () this_CONTINUE_5= RULE_CONTINUE this_SEMICOLON_6= RULE_SEMICOLON ) | ( () this_BREAK_8= RULE_BREAK this_SEMICOLON_9= RULE_SEMICOLON ) | ( () this_RETURN_11= RULE_RETURN ( (lv_exp_12_0= ruleExpression ) )? this_SEMICOLON_13= RULE_SEMICOLON ) )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5614:1: ( ( () this_GOTO_1= RULE_GOTO ( (lv_name_2_0= RULE_IDENTIFIER ) ) this_SEMICOLON_3= RULE_SEMICOLON ) | ( () this_CONTINUE_5= RULE_CONTINUE this_SEMICOLON_6= RULE_SEMICOLON ) | ( () this_BREAK_8= RULE_BREAK this_SEMICOLON_9= RULE_SEMICOLON ) | ( () this_RETURN_11= RULE_RETURN ( (lv_exp_12_0= ruleExpression ) )? this_SEMICOLON_13= RULE_SEMICOLON ) )
            int alt95=4;
            switch ( input.LA(1) ) {
            case RULE_GOTO:
                {
                alt95=1;
                }
                break;
            case RULE_CONTINUE:
                {
                alt95=2;
                }
                break;
            case RULE_BREAK:
                {
                alt95=3;
                }
                break;
            case RULE_RETURN:
                {
                alt95=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 95, 0, input);

                throw nvae;
            }

            switch (alt95) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5614:2: ( () this_GOTO_1= RULE_GOTO ( (lv_name_2_0= RULE_IDENTIFIER ) ) this_SEMICOLON_3= RULE_SEMICOLON )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5614:2: ( () this_GOTO_1= RULE_GOTO ( (lv_name_2_0= RULE_IDENTIFIER ) ) this_SEMICOLON_3= RULE_SEMICOLON )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5614:3: () this_GOTO_1= RULE_GOTO ( (lv_name_2_0= RULE_IDENTIFIER ) ) this_SEMICOLON_3= RULE_SEMICOLON
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5614:3: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5615:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getJumpStatementAccess().getJumpStatementGotoAction_0_0(),
                                  current);
                          
                    }

                    }

                    this_GOTO_1=(Token)match(input,RULE_GOTO,FOLLOW_RULE_GOTO_in_ruleJumpStatement12281); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_GOTO_1, grammarAccess.getJumpStatementAccess().getGOTOTerminalRuleCall_0_1()); 
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5624:1: ( (lv_name_2_0= RULE_IDENTIFIER ) )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5625:1: (lv_name_2_0= RULE_IDENTIFIER )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5625:1: (lv_name_2_0= RULE_IDENTIFIER )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5626:3: lv_name_2_0= RULE_IDENTIFIER
                    {
                    lv_name_2_0=(Token)match(input,RULE_IDENTIFIER,FOLLOW_RULE_IDENTIFIER_in_ruleJumpStatement12297); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_name_2_0, grammarAccess.getJumpStatementAccess().getNameIDENTIFIERTerminalRuleCall_0_2_0()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getJumpStatementRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"name",
                              		lv_name_2_0, 
                              		"IDENTIFIER");
                      	    
                    }

                    }


                    }

                    this_SEMICOLON_3=(Token)match(input,RULE_SEMICOLON,FOLLOW_RULE_SEMICOLON_in_ruleJumpStatement12313); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_SEMICOLON_3, grammarAccess.getJumpStatementAccess().getSEMICOLONTerminalRuleCall_0_3()); 
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5647:6: ( () this_CONTINUE_5= RULE_CONTINUE this_SEMICOLON_6= RULE_SEMICOLON )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5647:6: ( () this_CONTINUE_5= RULE_CONTINUE this_SEMICOLON_6= RULE_SEMICOLON )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5647:7: () this_CONTINUE_5= RULE_CONTINUE this_SEMICOLON_6= RULE_SEMICOLON
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5647:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5648:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getJumpStatementAccess().getJumpStatementContinueAction_1_0(),
                                  current);
                          
                    }

                    }

                    this_CONTINUE_5=(Token)match(input,RULE_CONTINUE,FOLLOW_RULE_CONTINUE_in_ruleJumpStatement12340); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_CONTINUE_5, grammarAccess.getJumpStatementAccess().getCONTINUETerminalRuleCall_1_1()); 
                          
                    }
                    this_SEMICOLON_6=(Token)match(input,RULE_SEMICOLON,FOLLOW_RULE_SEMICOLON_in_ruleJumpStatement12350); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_SEMICOLON_6, grammarAccess.getJumpStatementAccess().getSEMICOLONTerminalRuleCall_1_2()); 
                          
                    }

                    }


                    }
                    break;
                case 3 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5662:6: ( () this_BREAK_8= RULE_BREAK this_SEMICOLON_9= RULE_SEMICOLON )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5662:6: ( () this_BREAK_8= RULE_BREAK this_SEMICOLON_9= RULE_SEMICOLON )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5662:7: () this_BREAK_8= RULE_BREAK this_SEMICOLON_9= RULE_SEMICOLON
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5662:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5663:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getJumpStatementAccess().getJumpStatementBreakAction_2_0(),
                                  current);
                          
                    }

                    }

                    this_BREAK_8=(Token)match(input,RULE_BREAK,FOLLOW_RULE_BREAK_in_ruleJumpStatement12377); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_BREAK_8, grammarAccess.getJumpStatementAccess().getBREAKTerminalRuleCall_2_1()); 
                          
                    }
                    this_SEMICOLON_9=(Token)match(input,RULE_SEMICOLON,FOLLOW_RULE_SEMICOLON_in_ruleJumpStatement12387); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_SEMICOLON_9, grammarAccess.getJumpStatementAccess().getSEMICOLONTerminalRuleCall_2_2()); 
                          
                    }

                    }


                    }
                    break;
                case 4 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5677:6: ( () this_RETURN_11= RULE_RETURN ( (lv_exp_12_0= ruleExpression ) )? this_SEMICOLON_13= RULE_SEMICOLON )
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5677:6: ( () this_RETURN_11= RULE_RETURN ( (lv_exp_12_0= ruleExpression ) )? this_SEMICOLON_13= RULE_SEMICOLON )
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5677:7: () this_RETURN_11= RULE_RETURN ( (lv_exp_12_0= ruleExpression ) )? this_SEMICOLON_13= RULE_SEMICOLON
                    {
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5677:7: ()
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5678:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getJumpStatementAccess().getJumpStatementReturnAction_3_0(),
                                  current);
                          
                    }

                    }

                    this_RETURN_11=(Token)match(input,RULE_RETURN,FOLLOW_RULE_RETURN_in_ruleJumpStatement12414); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_RETURN_11, grammarAccess.getJumpStatementAccess().getRETURNTerminalRuleCall_3_1()); 
                          
                    }
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5687:1: ( (lv_exp_12_0= ruleExpression ) )?
                    int alt94=2;
                    int LA94_0 = input.LA(1);

                    if ( (LA94_0==RULE_MULT_OP||(LA94_0>=RULE_STRING_LITERAL && LA94_0<=RULE_IDENTIFIER)||LA94_0==RULE_LPAREN||LA94_0==RULE_LCBRACKET||LA94_0==RULE_AND_OP||(LA94_0>=RULE_ADD_OP && LA94_0<=RULE_MINUS_OP)||(LA94_0>=RULE_INC_OP && LA94_0<=RULE_FUNC_NAME)||(LA94_0>=RULE_TILDE && LA94_0<=RULE_NOT_OP)) ) {
                        alt94=1;
                    }
                    switch (alt94) {
                        case 1 :
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5688:1: (lv_exp_12_0= ruleExpression )
                            {
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5688:1: (lv_exp_12_0= ruleExpression )
                            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5689:3: lv_exp_12_0= ruleExpression
                            {
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getJumpStatementAccess().getExpExpressionParserRuleCall_3_2_0()); 
                              	    
                            }
                            pushFollow(FOLLOW_ruleExpression_in_ruleJumpStatement12434);
                            lv_exp_12_0=ruleExpression();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElementForParent(grammarAccess.getJumpStatementRule());
                              	        }
                                     		set(
                                     			current, 
                                     			"exp",
                                      		lv_exp_12_0, 
                                      		"Expression");
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }


                            }
                            break;

                    }

                    this_SEMICOLON_13=(Token)match(input,RULE_SEMICOLON,FOLLOW_RULE_SEMICOLON_in_ruleJumpStatement12446); if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_SEMICOLON_13, grammarAccess.getJumpStatementAccess().getSEMICOLONTerminalRuleCall_3_3()); 
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleJumpStatement"


    // $ANTLR start "entryRuleUNARY_OP"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5717:1: entryRuleUNARY_OP returns [String current=null] : iv_ruleUNARY_OP= ruleUNARY_OP EOF ;
    public final String entryRuleUNARY_OP() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleUNARY_OP = null;


        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5718:2: (iv_ruleUNARY_OP= ruleUNARY_OP EOF )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5719:2: iv_ruleUNARY_OP= ruleUNARY_OP EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getUNARY_OPRule()); 
            }
            pushFollow(FOLLOW_ruleUNARY_OP_in_entryRuleUNARY_OP12483);
            iv_ruleUNARY_OP=ruleUNARY_OP();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleUNARY_OP.getText(); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleUNARY_OP12494); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUNARY_OP"


    // $ANTLR start "ruleUNARY_OP"
    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5726:1: ruleUNARY_OP returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_AND_OP_0= RULE_AND_OP | this_MULT_OP_1= RULE_MULT_OP | this_ADD_OP_2= RULE_ADD_OP | this_MINUS_OP_3= RULE_MINUS_OP | this_TILDE_4= RULE_TILDE | this_NOT_OP_5= RULE_NOT_OP ) ;
    public final AntlrDatatypeRuleToken ruleUNARY_OP() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_AND_OP_0=null;
        Token this_MULT_OP_1=null;
        Token this_ADD_OP_2=null;
        Token this_MINUS_OP_3=null;
        Token this_TILDE_4=null;
        Token this_NOT_OP_5=null;

         enterRule(); 
            
        try {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5729:28: ( (this_AND_OP_0= RULE_AND_OP | this_MULT_OP_1= RULE_MULT_OP | this_ADD_OP_2= RULE_ADD_OP | this_MINUS_OP_3= RULE_MINUS_OP | this_TILDE_4= RULE_TILDE | this_NOT_OP_5= RULE_NOT_OP ) )
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5730:1: (this_AND_OP_0= RULE_AND_OP | this_MULT_OP_1= RULE_MULT_OP | this_ADD_OP_2= RULE_ADD_OP | this_MINUS_OP_3= RULE_MINUS_OP | this_TILDE_4= RULE_TILDE | this_NOT_OP_5= RULE_NOT_OP )
            {
            // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5730:1: (this_AND_OP_0= RULE_AND_OP | this_MULT_OP_1= RULE_MULT_OP | this_ADD_OP_2= RULE_ADD_OP | this_MINUS_OP_3= RULE_MINUS_OP | this_TILDE_4= RULE_TILDE | this_NOT_OP_5= RULE_NOT_OP )
            int alt96=6;
            switch ( input.LA(1) ) {
            case RULE_AND_OP:
                {
                alt96=1;
                }
                break;
            case RULE_MULT_OP:
                {
                alt96=2;
                }
                break;
            case RULE_ADD_OP:
                {
                alt96=3;
                }
                break;
            case RULE_MINUS_OP:
                {
                alt96=4;
                }
                break;
            case RULE_TILDE:
                {
                alt96=5;
                }
                break;
            case RULE_NOT_OP:
                {
                alt96=6;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 96, 0, input);

                throw nvae;
            }

            switch (alt96) {
                case 1 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5730:6: this_AND_OP_0= RULE_AND_OP
                    {
                    this_AND_OP_0=(Token)match(input,RULE_AND_OP,FOLLOW_RULE_AND_OP_in_ruleUNARY_OP12534); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		current.merge(this_AND_OP_0);
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_AND_OP_0, grammarAccess.getUNARY_OPAccess().getAND_OPTerminalRuleCall_0()); 
                          
                    }

                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5738:10: this_MULT_OP_1= RULE_MULT_OP
                    {
                    this_MULT_OP_1=(Token)match(input,RULE_MULT_OP,FOLLOW_RULE_MULT_OP_in_ruleUNARY_OP12560); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		current.merge(this_MULT_OP_1);
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_MULT_OP_1, grammarAccess.getUNARY_OPAccess().getMULT_OPTerminalRuleCall_1()); 
                          
                    }

                    }
                    break;
                case 3 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5746:10: this_ADD_OP_2= RULE_ADD_OP
                    {
                    this_ADD_OP_2=(Token)match(input,RULE_ADD_OP,FOLLOW_RULE_ADD_OP_in_ruleUNARY_OP12586); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		current.merge(this_ADD_OP_2);
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_ADD_OP_2, grammarAccess.getUNARY_OPAccess().getADD_OPTerminalRuleCall_2()); 
                          
                    }

                    }
                    break;
                case 4 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5754:10: this_MINUS_OP_3= RULE_MINUS_OP
                    {
                    this_MINUS_OP_3=(Token)match(input,RULE_MINUS_OP,FOLLOW_RULE_MINUS_OP_in_ruleUNARY_OP12612); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		current.merge(this_MINUS_OP_3);
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_MINUS_OP_3, grammarAccess.getUNARY_OPAccess().getMINUS_OPTerminalRuleCall_3()); 
                          
                    }

                    }
                    break;
                case 5 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5762:10: this_TILDE_4= RULE_TILDE
                    {
                    this_TILDE_4=(Token)match(input,RULE_TILDE,FOLLOW_RULE_TILDE_in_ruleUNARY_OP12638); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		current.merge(this_TILDE_4);
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_TILDE_4, grammarAccess.getUNARY_OPAccess().getTILDETerminalRuleCall_4()); 
                          
                    }

                    }
                    break;
                case 6 :
                    // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5770:10: this_NOT_OP_5= RULE_NOT_OP
                    {
                    this_NOT_OP_5=(Token)match(input,RULE_NOT_OP,FOLLOW_RULE_NOT_OP_in_ruleUNARY_OP12664); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		current.merge(this_NOT_OP_5);
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_NOT_OP_5, grammarAccess.getUNARY_OPAccess().getNOT_OPTerminalRuleCall_5()); 
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUNARY_OP"

    // $ANTLR start synpred1_InternalClang
    public final void synpred1_InternalClang_fragment() throws RecognitionException {   
        // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:2413:7: ( RULE_IDENTIFIER | RULE_STRING_LITERAL | RULE_FUNC_NAME | RULE_I_CONSTANT | RULE_F_CONSTANT | RULE_LPAREN | RULE_GENERIC | RULE_LCBRACKET | RULE_INC_OP | RULE_DEC_OP | RULE_SIZEOF | RULE_AND_OP | RULE_MULT_OP | RULE_ADD_OP | RULE_MINUS_OP | RULE_TILDE | RULE_NOT_OP | RULE_ALIGNOF )
        // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:
        {
        if ( input.LA(1)==RULE_MULT_OP||(input.LA(1)>=RULE_STRING_LITERAL && input.LA(1)<=RULE_IDENTIFIER)||input.LA(1)==RULE_LPAREN||input.LA(1)==RULE_LCBRACKET||input.LA(1)==RULE_AND_OP||(input.LA(1)>=RULE_ADD_OP && input.LA(1)<=RULE_MINUS_OP)||(input.LA(1)>=RULE_INC_OP && input.LA(1)<=RULE_FUNC_NAME)||(input.LA(1)>=RULE_TILDE && input.LA(1)<=RULE_NOT_OP) ) {
            input.consume();
            state.errorRecovery=false;state.failed=false;
        }
        else {
            if (state.backtracking>0) {state.failed=true; return ;}
            MismatchedSetException mse = new MismatchedSetException(null,input);
            throw mse;
        }


        }
    }
    // $ANTLR end synpred1_InternalClang

    // $ANTLR start synpred2_InternalClang
    public final void synpred2_InternalClang_fragment() throws RecognitionException {   
        // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:3918:3: ( RULE_IDENTIFIER | RULE_STRING_LITERAL | RULE_FUNC_NAME | RULE_I_CONSTANT | RULE_F_CONSTANT | RULE_LPAREN | RULE_GENERIC | RULE_LCBRACKET | RULE_INC_OP | RULE_DEC_OP | RULE_SIZEOF | RULE_AND_OP | RULE_MULT_OP | RULE_ADD_OP | RULE_MINUS_OP | RULE_TILDE | RULE_NOT_OP | RULE_ALIGNOF )
        // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:
        {
        if ( input.LA(1)==RULE_MULT_OP||(input.LA(1)>=RULE_STRING_LITERAL && input.LA(1)<=RULE_IDENTIFIER)||input.LA(1)==RULE_LPAREN||input.LA(1)==RULE_LCBRACKET||input.LA(1)==RULE_AND_OP||(input.LA(1)>=RULE_ADD_OP && input.LA(1)<=RULE_MINUS_OP)||(input.LA(1)>=RULE_INC_OP && input.LA(1)<=RULE_FUNC_NAME)||(input.LA(1)>=RULE_TILDE && input.LA(1)<=RULE_NOT_OP) ) {
            input.consume();
            state.errorRecovery=false;state.failed=false;
        }
        else {
            if (state.backtracking>0) {state.failed=true; return ;}
            MismatchedSetException mse = new MismatchedSetException(null,input);
            throw mse;
        }


        }
    }
    // $ANTLR end synpred2_InternalClang

    // $ANTLR start synpred4_InternalClang
    public final void synpred4_InternalClang_fragment() throws RecognitionException {   
        // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4835:3: ( RULE_LCBRACKET )
        // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:4835:5: RULE_LCBRACKET
        {
        match(input,RULE_LCBRACKET,FOLLOW_RULE_LCBRACKET_in_synpred4_InternalClang10707); if (state.failed) return ;

        }
    }
    // $ANTLR end synpred4_InternalClang

    // $ANTLR start synpred5_InternalClang
    public final void synpred5_InternalClang_fragment() throws RecognitionException {   
        // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5199:4: ( RULE_ELSE )
        // ../org.xtext.dop.clang/src-gen/org/xtext/dop/clang/parser/antlr/internal/InternalClang.g:5199:6: RULE_ELSE
        {
        match(input,RULE_ELSE,FOLLOW_RULE_ELSE_in_synpred5_InternalClang11500); if (state.failed) return ;

        }
    }
    // $ANTLR end synpred5_InternalClang

    // Delegated rules

    public final boolean synpred1_InternalClang() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred1_InternalClang_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred5_InternalClang() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred5_InternalClang_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred2_InternalClang() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred2_InternalClang_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred4_InternalClang() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred4_InternalClang_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


    protected DFA47 dfa47 = new DFA47(this);
    protected DFA73 dfa73 = new DFA73(this);
    protected DFA72 dfa72 = new DFA72(this);
    protected DFA83 dfa83 = new DFA83(this);
    static final String DFA47_eotS =
        "\24\uffff";
    static final String DFA47_eofS =
        "\24\uffff";
    static final String DFA47_minS =
        "\1\6\23\uffff";
    static final String DFA47_maxS =
        "\1\144\23\uffff";
    static final String DFA47_acceptS =
        "\1\uffff\1\1\22\2";
    static final String DFA47_specialS =
        "\1\0\23\uffff}>";
    static final String[] DFA47_transitionS = {
            "\1\16\1\uffff\1\3\1\2\5\uffff\1\7\6\uffff\16\1\1\11\1\uffff"+
            "\3\1\20\uffff\1\15\16\uffff\1\17\1\20\4\uffff\1\12\1\13\1\14"+
            "\1\23\1\5\1\6\1\10\1\4\15\uffff\1\21\1\22",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA47_eot = DFA.unpackEncodedString(DFA47_eotS);
    static final short[] DFA47_eof = DFA.unpackEncodedString(DFA47_eofS);
    static final char[] DFA47_min = DFA.unpackEncodedStringToUnsignedChars(DFA47_minS);
    static final char[] DFA47_max = DFA.unpackEncodedStringToUnsignedChars(DFA47_maxS);
    static final short[] DFA47_accept = DFA.unpackEncodedString(DFA47_acceptS);
    static final short[] DFA47_special = DFA.unpackEncodedString(DFA47_specialS);
    static final short[][] DFA47_transition;

    static {
        int numStates = DFA47_transitionS.length;
        DFA47_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA47_transition[i] = DFA.unpackEncodedString(DFA47_transitionS[i]);
        }
    }

    class DFA47 extends DFA {

        public DFA47(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 47;
            this.eot = DFA47_eot;
            this.eof = DFA47_eof;
            this.min = DFA47_min;
            this.max = DFA47_max;
            this.accept = DFA47_accept;
            this.special = DFA47_special;
            this.transition = DFA47_transition;
        }
        public String getDescription() {
            return "2394:1: ( ( (lv_type_2_0= ruleSimpleType ) ) | ( ( RULE_IDENTIFIER | RULE_STRING_LITERAL | RULE_FUNC_NAME | RULE_I_CONSTANT | RULE_F_CONSTANT | RULE_LPAREN | RULE_GENERIC | RULE_LCBRACKET | RULE_INC_OP | RULE_DEC_OP | RULE_SIZEOF | RULE_AND_OP | RULE_MULT_OP | RULE_ADD_OP | RULE_MINUS_OP | RULE_TILDE | RULE_NOT_OP | RULE_ALIGNOF )=> (lv_exp_3_0= ruleBaseExpression ) ) )";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA47_0 = input.LA(1);

                         
                        int index47_0 = input.index();
                        input.rewind();
                        s = -1;
                        if ( ((LA47_0>=RULE_ATOMIC && LA47_0<=RULE_IMAGINARY)||(LA47_0>=RULE_STRUCT && LA47_0<=RULE_ENUM)) ) {s = 1;}

                        else if ( (LA47_0==RULE_IDENTIFIER) && (synpred1_InternalClang())) {s = 2;}

                        else if ( (LA47_0==RULE_STRING_LITERAL) && (synpred1_InternalClang())) {s = 3;}

                        else if ( (LA47_0==RULE_FUNC_NAME) && (synpred1_InternalClang())) {s = 4;}

                        else if ( (LA47_0==RULE_I_CONSTANT) && (synpred1_InternalClang())) {s = 5;}

                        else if ( (LA47_0==RULE_F_CONSTANT) && (synpred1_InternalClang())) {s = 6;}

                        else if ( (LA47_0==RULE_LPAREN) && (synpred1_InternalClang())) {s = 7;}

                        else if ( (LA47_0==RULE_GENERIC) && (synpred1_InternalClang())) {s = 8;}

                        else if ( (LA47_0==RULE_LCBRACKET) && (synpred1_InternalClang())) {s = 9;}

                        else if ( (LA47_0==RULE_INC_OP) && (synpred1_InternalClang())) {s = 10;}

                        else if ( (LA47_0==RULE_DEC_OP) && (synpred1_InternalClang())) {s = 11;}

                        else if ( (LA47_0==RULE_SIZEOF) && (synpred1_InternalClang())) {s = 12;}

                        else if ( (LA47_0==RULE_AND_OP) && (synpred1_InternalClang())) {s = 13;}

                        else if ( (LA47_0==RULE_MULT_OP) && (synpred1_InternalClang())) {s = 14;}

                        else if ( (LA47_0==RULE_ADD_OP) && (synpred1_InternalClang())) {s = 15;}

                        else if ( (LA47_0==RULE_MINUS_OP) && (synpred1_InternalClang())) {s = 16;}

                        else if ( (LA47_0==RULE_TILDE) && (synpred1_InternalClang())) {s = 17;}

                        else if ( (LA47_0==RULE_NOT_OP) && (synpred1_InternalClang())) {s = 18;}

                        else if ( (LA47_0==RULE_ALIGNOF) && (synpred1_InternalClang())) {s = 19;}

                         
                        input.seek(index47_0);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 47, _s, input);
            error(nvae);
            throw nvae;
        }
    }
    static final String DFA73_eotS =
        "\12\uffff";
    static final String DFA73_eofS =
        "\12\uffff";
    static final String DFA73_minS =
        "\1\6\1\uffff\1\6\7\uffff";
    static final String DFA73_maxS =
        "\1\144\1\uffff\1\144\7\uffff";
    static final String DFA73_acceptS =
        "\1\uffff\1\1\1\uffff\1\2\1\3\1\4\1\5\1\6\1\7\1\10";
    static final String DFA73_specialS =
        "\12\uffff}>";
    static final String[] DFA73_transitionS = {
            "\1\7\1\uffff\2\1\5\uffff\1\2\24\uffff\1\3\24\uffff\1\7\16\uffff"+
            "\2\7\4\uffff\1\4\1\5\1\6\1\10\4\1\15\uffff\2\7",
            "",
            "\1\1\1\uffff\2\1\5\uffff\1\1\6\uffff\16\11\1\1\1\uffff\15"+
            "\11\6\uffff\1\1\16\uffff\2\1\4\uffff\10\1\15\uffff\2\1",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA73_eot = DFA.unpackEncodedString(DFA73_eotS);
    static final short[] DFA73_eof = DFA.unpackEncodedString(DFA73_eofS);
    static final char[] DFA73_min = DFA.unpackEncodedStringToUnsignedChars(DFA73_minS);
    static final char[] DFA73_max = DFA.unpackEncodedStringToUnsignedChars(DFA73_maxS);
    static final short[] DFA73_accept = DFA.unpackEncodedString(DFA73_acceptS);
    static final short[] DFA73_special = DFA.unpackEncodedString(DFA73_specialS);
    static final short[][] DFA73_transition;

    static {
        int numStates = DFA73_transitionS.length;
        DFA73_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA73_transition[i] = DFA.unpackEncodedString(DFA73_transitionS[i]);
        }
    }

    class DFA73 extends DFA {

        public DFA73(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 73;
            this.eot = DFA73_eot;
            this.eof = DFA73_eof;
            this.min = DFA73_min;
            this.max = DFA73_max;
            this.accept = DFA73_accept;
            this.special = DFA73_special;
            this.transition = DFA73_transition;
        }
        public String getDescription() {
            return "3739:1: ( (this_PrimaryExpression_0= rulePrimaryExpression ( () ( (lv_next_2_0= rulePostfixExpressionPostfix ) )+ )? ) | ( () this_LCBRACKET_4= RULE_LCBRACKET ( (lv_exps_5_0= ruleExpression ) ) (this_COMMA_6= RULE_COMMA ( (lv_exps_7_0= ruleExpression ) ) )* (this_COMMA_8= RULE_COMMA )? this_RCBRACKET_9= RULE_RCBRACKET ( (lv_next_10_0= rulePostfixExpressionPostfix ) )* ) | ( () this_INC_OP_12= RULE_INC_OP ( (lv_content_13_0= ruleUnaryExpression ) ) ) | ( () this_DEC_OP_15= RULE_DEC_OP ( (lv_content_16_0= ruleUnaryExpression ) ) ) | ( () this_SIZEOF_18= RULE_SIZEOF ( ( ( RULE_IDENTIFIER | RULE_STRING_LITERAL | RULE_FUNC_NAME | RULE_I_CONSTANT | RULE_F_CONSTANT | RULE_LPAREN | RULE_GENERIC | RULE_LCBRACKET | RULE_INC_OP | RULE_DEC_OP | RULE_SIZEOF | RULE_AND_OP | RULE_MULT_OP | RULE_ADD_OP | RULE_MINUS_OP | RULE_TILDE | RULE_NOT_OP | RULE_ALIGNOF )=> (lv_content_19_0= ruleUnaryExpression ) ) | (this_LPAREN_20= RULE_LPAREN ( (lv_type_21_0= ruleSpecifiedType ) ) this_RPAREN_22= RULE_RPAREN ) ) ) | ( () ( (lv_op_24_0= ruleUNARY_OP ) ) ( (lv_content_25_0= ruleUnaryExpression ) ) ) | ( () this_ALIGNOF_27= RULE_ALIGNOF this_LPAREN_28= RULE_LPAREN ( (lv_type_29_0= ruleSpecifiedType ) ) this_RPAREN_30= RULE_RPAREN ) | ( () (this_LPAREN_32= RULE_LPAREN ( (lv_type_33_0= ruleSpecifiedType ) ) this_RPAREN_34= RULE_RPAREN ) ( ( RULE_IDENTIFIER | RULE_STRING_LITERAL | RULE_FUNC_NAME | RULE_I_CONSTANT | RULE_F_CONSTANT | RULE_LPAREN | RULE_GENERIC | RULE_LCBRACKET | RULE_INC_OP | RULE_DEC_OP | RULE_SIZEOF | RULE_AND_OP | RULE_MULT_OP | RULE_ADD_OP | RULE_MINUS_OP | RULE_TILDE | RULE_NOT_OP | RULE_ALIGNOF )=> (lv_exp_35_0= ruleUnaryExpression ) ) ) )";
        }
    }
    static final String DFA72_eotS =
        "\24\uffff";
    static final String DFA72_eofS =
        "\24\uffff";
    static final String DFA72_minS =
        "\1\6\5\uffff\1\0\15\uffff";
    static final String DFA72_maxS =
        "\1\144\5\uffff\1\0\15\uffff";
    static final String DFA72_acceptS =
        "\1\uffff\5\1\1\uffff\14\1\1\2";
    static final String DFA72_specialS =
        "\1\0\5\uffff\1\1\15\uffff}>";
    static final String[] DFA72_transitionS = {
            "\1\15\1\uffff\1\2\1\1\5\uffff\1\6\24\uffff\1\10\24\uffff\1"+
            "\14\16\uffff\1\16\1\17\4\uffff\1\11\1\12\1\13\1\22\1\4\1\5\1"+
            "\7\1\3\15\uffff\1\20\1\21",
            "",
            "",
            "",
            "",
            "",
            "\1\uffff",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA72_eot = DFA.unpackEncodedString(DFA72_eotS);
    static final short[] DFA72_eof = DFA.unpackEncodedString(DFA72_eofS);
    static final char[] DFA72_min = DFA.unpackEncodedStringToUnsignedChars(DFA72_minS);
    static final char[] DFA72_max = DFA.unpackEncodedStringToUnsignedChars(DFA72_maxS);
    static final short[] DFA72_accept = DFA.unpackEncodedString(DFA72_acceptS);
    static final short[] DFA72_special = DFA.unpackEncodedString(DFA72_specialS);
    static final short[][] DFA72_transition;

    static {
        int numStates = DFA72_transitionS.length;
        DFA72_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA72_transition[i] = DFA.unpackEncodedString(DFA72_transitionS[i]);
        }
    }

    class DFA72 extends DFA {

        public DFA72(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 72;
            this.eot = DFA72_eot;
            this.eof = DFA72_eof;
            this.min = DFA72_min;
            this.max = DFA72_max;
            this.accept = DFA72_accept;
            this.special = DFA72_special;
            this.transition = DFA72_transition;
        }
        public String getDescription() {
            return "3918:1: ( ( ( RULE_IDENTIFIER | RULE_STRING_LITERAL | RULE_FUNC_NAME | RULE_I_CONSTANT | RULE_F_CONSTANT | RULE_LPAREN | RULE_GENERIC | RULE_LCBRACKET | RULE_INC_OP | RULE_DEC_OP | RULE_SIZEOF | RULE_AND_OP | RULE_MULT_OP | RULE_ADD_OP | RULE_MINUS_OP | RULE_TILDE | RULE_NOT_OP | RULE_ALIGNOF )=> (lv_content_19_0= ruleUnaryExpression ) ) | (this_LPAREN_20= RULE_LPAREN ( (lv_type_21_0= ruleSpecifiedType ) ) this_RPAREN_22= RULE_RPAREN ) )";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA72_0 = input.LA(1);

                         
                        int index72_0 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (LA72_0==RULE_IDENTIFIER) && (synpred2_InternalClang())) {s = 1;}

                        else if ( (LA72_0==RULE_STRING_LITERAL) && (synpred2_InternalClang())) {s = 2;}

                        else if ( (LA72_0==RULE_FUNC_NAME) && (synpred2_InternalClang())) {s = 3;}

                        else if ( (LA72_0==RULE_I_CONSTANT) && (synpred2_InternalClang())) {s = 4;}

                        else if ( (LA72_0==RULE_F_CONSTANT) && (synpred2_InternalClang())) {s = 5;}

                        else if ( (LA72_0==RULE_LPAREN) ) {s = 6;}

                        else if ( (LA72_0==RULE_GENERIC) && (synpred2_InternalClang())) {s = 7;}

                        else if ( (LA72_0==RULE_LCBRACKET) && (synpred2_InternalClang())) {s = 8;}

                        else if ( (LA72_0==RULE_INC_OP) && (synpred2_InternalClang())) {s = 9;}

                        else if ( (LA72_0==RULE_DEC_OP) && (synpred2_InternalClang())) {s = 10;}

                        else if ( (LA72_0==RULE_SIZEOF) && (synpred2_InternalClang())) {s = 11;}

                        else if ( (LA72_0==RULE_AND_OP) && (synpred2_InternalClang())) {s = 12;}

                        else if ( (LA72_0==RULE_MULT_OP) && (synpred2_InternalClang())) {s = 13;}

                        else if ( (LA72_0==RULE_ADD_OP) && (synpred2_InternalClang())) {s = 14;}

                        else if ( (LA72_0==RULE_MINUS_OP) && (synpred2_InternalClang())) {s = 15;}

                        else if ( (LA72_0==RULE_TILDE) && (synpred2_InternalClang())) {s = 16;}

                        else if ( (LA72_0==RULE_NOT_OP) && (synpred2_InternalClang())) {s = 17;}

                        else if ( (LA72_0==RULE_ALIGNOF) && (synpred2_InternalClang())) {s = 18;}

                         
                        input.seek(index72_0);
                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA72_6 = input.LA(1);

                         
                        int index72_6 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred2_InternalClang()) ) {s = 18;}

                        else if ( (true) ) {s = 19;}

                         
                        input.seek(index72_6);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 72, _s, input);
            error(nvae);
            throw nvae;
        }
    }
    static final String DFA83_eotS =
        "\40\uffff";
    static final String DFA83_eofS =
        "\40\uffff";
    static final String DFA83_minS =
        "\1\6\1\0\36\uffff";
    static final String DFA83_maxS =
        "\1\144\1\0\36\uffff";
    static final String DFA83_acceptS =
        "\2\uffff\1\2\1\uffff\1\3\21\uffff\1\4\1\uffff\1\5\2\uffff\1\6\3"+
        "\uffff\1\1";
    static final String DFA83_specialS =
        "\1\uffff\1\0\36\uffff}>";
    static final String[] DFA83_transitionS = {
            "\1\4\1\uffff\2\4\2\uffff\1\4\2\uffff\1\4\24\uffff\1\1\24\uffff"+
            "\1\4\16\uffff\2\4\4\uffff\10\4\1\2\1\uffff\1\2\1\26\1\uffff"+
            "\1\26\3\30\4\33\2\4",
            "\1\uffff",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA83_eot = DFA.unpackEncodedString(DFA83_eotS);
    static final short[] DFA83_eof = DFA.unpackEncodedString(DFA83_eofS);
    static final char[] DFA83_min = DFA.unpackEncodedStringToUnsignedChars(DFA83_minS);
    static final char[] DFA83_max = DFA.unpackEncodedStringToUnsignedChars(DFA83_maxS);
    static final short[] DFA83_accept = DFA.unpackEncodedString(DFA83_acceptS);
    static final short[] DFA83_special = DFA.unpackEncodedString(DFA83_specialS);
    static final short[][] DFA83_transition;

    static {
        int numStates = DFA83_transitionS.length;
        DFA83_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA83_transition[i] = DFA.unpackEncodedString(DFA83_transitionS[i]);
        }
    }

    class DFA83 extends DFA {

        public DFA83(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 83;
            this.eot = DFA83_eot;
            this.eof = DFA83_eof;
            this.min = DFA83_min;
            this.max = DFA83_max;
            this.accept = DFA83_accept;
            this.special = DFA83_special;
            this.transition = DFA83_transition;
        }
        public String getDescription() {
            return "4835:1: ( ( ( RULE_LCBRACKET )=>this_CompoundStatement_0= ruleCompoundStatement ) | this_LabeledStatement_1= ruleLabeledStatement | this_ExpressionStatement_2= ruleExpressionStatement | this_SelectionStatement_3= ruleSelectionStatement | this_IterationStatement_4= ruleIterationStatement | this_JumpStatement_5= ruleJumpStatement )";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA83_1 = input.LA(1);

                         
                        int index83_1 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred4_InternalClang()) ) {s = 31;}

                        else if ( (true) ) {s = 4;}

                         
                        input.seek(index83_1);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 83, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

    public static final BitSet FOLLOW_ruleModel_in_entryRuleModel75 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleModel85 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStandardInclude_in_ruleModel131 = new BitSet(new long[]{0x0007FFCFFFE00812L});
    public static final BitSet FOLLOW_ruleFileInclude_in_ruleModel153 = new BitSet(new long[]{0x0007FFCFFFE00812L});
    public static final BitSet FOLLOW_ruleDeclaration_in_ruleModel175 = new BitSet(new long[]{0x0007FFCFFFE00802L});
    public static final BitSet FOLLOW_ruleStandardInclude_in_entryRuleStandardInclude212 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleStandardInclude222 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INCLUDE_in_ruleStandardInclude267 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_L_OP_in_ruleStandardInclude277 = new BitSet(new long[]{0x0000000000000240L});
    public static final BitSet FOLLOW_ruleSource_in_ruleStandardInclude298 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_RULE_MULT_OP_in_ruleStandardInclude321 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_RULE_G_OP_in_ruleStandardInclude338 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFileInclude_in_entryRuleFileInclude373 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFileInclude383 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INCLUDE_in_ruleFileInclude419 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_RULE_STRING_LITERAL_in_ruleFileInclude435 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSource_in_entryRuleSource476 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSource486 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_IDENTIFIER_in_ruleSource538 = new BitSet(new long[]{0x0000000000000400L});
    public static final BitSet FOLLOW_RULE_DOT_in_ruleSource554 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_RULE_IDENTIFIER_in_ruleSource570 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_IDENTIFIER_in_ruleSource609 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDeclaration_in_entryRuleDeclaration651 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDeclaration661 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGenericSpecifiedDeclaration_in_ruleDeclaration708 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTypeAliasDeclaration_in_ruleDeclaration735 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStaticAssertDeclaration_in_ruleDeclaration762 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTypeAliasDeclaration_in_entryRuleTypeAliasDeclaration797 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTypeAliasDeclaration807 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_TYPEDEF_in_ruleTypeAliasDeclaration843 = new BitSet(new long[]{0x0007FFCFFFC00000L});
    public static final BitSet FOLLOW_ruleSpecifiedType_in_ruleTypeAliasDeclaration863 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_RULE_SEMICOLON_in_ruleTypeAliasDeclaration874 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGenericSpecifiedDeclaration_in_entryRuleGenericSpecifiedDeclaration909 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleGenericSpecifiedDeclaration919 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDeclarationSpecifier_in_ruleGenericSpecifiedDeclaration965 = new BitSet(new long[]{0x0007FFCFFFC00000L});
    public static final BitSet FOLLOW_ruleSimpleType_in_ruleGenericSpecifiedDeclaration987 = new BitSet(new long[]{0x0007FFCFFFC28240L});
    public static final BitSet FOLLOW_ruleGenericDeclarationElements_in_ruleGenericSpecifiedDeclaration1009 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_RULE_SEMICOLON_in_ruleGenericSpecifiedDeclaration1020 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGenericDeclarationElements_in_entryRuleGenericDeclarationElements1055 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleGenericDeclarationElements1065 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGenericDeclarationElement_in_ruleGenericDeclarationElements1111 = new BitSet(new long[]{0x0000000000002002L});
    public static final BitSet FOLLOW_RULE_COMMA_in_ruleGenericDeclarationElements1123 = new BitSet(new long[]{0x0000000000028240L});
    public static final BitSet FOLLOW_ruleGenericDeclarationElements_in_ruleGenericDeclarationElements1143 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGenericDeclarationElement_in_entryRuleGenericDeclarationElement1181 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleGenericDeclarationElement1191 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGenericDeclarationId_in_ruleGenericDeclarationElement1237 = new BitSet(new long[]{0x0000001000100002L});
    public static final BitSet FOLLOW_ruleGenericDeclarationInitializer_in_ruleGenericDeclarationElement1258 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGenericDeclarationId_in_entryRuleGenericDeclarationId1295 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleGenericDeclarationId1305 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_MULT_OP_in_ruleGenericDeclarationId1357 = new BitSet(new long[]{0x000000000002C242L});
    public static final BitSet FOLLOW_RULE_RESTRICT_in_ruleGenericDeclarationId1380 = new BitSet(new long[]{0x0000000000028242L});
    public static final BitSet FOLLOW_ruleGenericDeclarationInnerId_in_ruleGenericDeclarationId1407 = new BitSet(new long[]{0x0000000000028242L});
    public static final BitSet FOLLOW_ruleGenericDeclarationIdPostfix_in_ruleGenericDeclarationId1429 = new BitSet(new long[]{0x0000000000028242L});
    public static final BitSet FOLLOW_ruleGenericDeclarationInnerId_in_ruleGenericDeclarationId1468 = new BitSet(new long[]{0x0000000000028242L});
    public static final BitSet FOLLOW_ruleGenericDeclarationIdPostfix_in_ruleGenericDeclarationId1489 = new BitSet(new long[]{0x0000000000028242L});
    public static final BitSet FOLLOW_ruleGenericDeclarationIdPostfix_in_ruleGenericDeclarationId1528 = new BitSet(new long[]{0x0000000000028242L});
    public static final BitSet FOLLOW_ruleGenericDeclarationInnerId_in_ruleGenericDeclarationId1550 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGenericDeclarationInnerId_in_entryRuleGenericDeclarationInnerId1588 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleGenericDeclarationInnerId1598 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_IDENTIFIER_in_ruleGenericDeclarationInnerId1650 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_LPAREN_in_ruleGenericDeclarationInnerId1674 = new BitSet(new long[]{0x0000000000028240L});
    public static final BitSet FOLLOW_ruleGenericDeclarationId_in_ruleGenericDeclarationInnerId1694 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_RULE_RPAREN_in_ruleGenericDeclarationInnerId1705 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGenericDeclarationIdPostfix_in_entryRuleGenericDeclarationIdPostfix1741 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleGenericDeclarationIdPostfix1751 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleArrayPostfix_in_ruleGenericDeclarationIdPostfix1797 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleParametersPostfix_in_ruleGenericDeclarationIdPostfix1824 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleArrayPostfix_in_entryRuleArrayPostfix1860 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleArrayPostfix1870 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_LBRACKET_in_ruleArrayPostfix1915 = new BitSet(new long[]{0x0200001000048340L,0x00000018003FC300L});
    public static final BitSet FOLLOW_RULE_MULT_OP_in_ruleArrayPostfix1932 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleArrayPostfix1964 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_RULE_RBRACKET_in_ruleArrayPostfix1977 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleParametersPostfix_in_entryRuleParametersPostfix2012 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleParametersPostfix2022 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_LPAREN_in_ruleParametersPostfix2067 = new BitSet(new long[]{0x0007FFCFFFC12000L});
    public static final BitSet FOLLOW_ruleParameter_in_ruleParametersPostfix2088 = new BitSet(new long[]{0x0000000000012000L});
    public static final BitSet FOLLOW_ruleNextParameter_in_ruleParametersPostfix2109 = new BitSet(new long[]{0x0000000000012000L});
    public static final BitSet FOLLOW_RULE_COMMA_in_ruleParametersPostfix2124 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_RULE_ELLIPSIS_in_ruleParametersPostfix2140 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_RULE_RPAREN_in_ruleParametersPostfix2158 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNextParameter_in_entryRuleNextParameter2193 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleNextParameter2203 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_COMMA_in_ruleNextParameter2239 = new BitSet(new long[]{0x0007FFCFFFC00000L});
    public static final BitSet FOLLOW_ruleParameter_in_ruleNextParameter2259 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleParameter_in_entryRuleParameter2295 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleParameter2305 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDeclarationSpecifier_in_ruleParameter2351 = new BitSet(new long[]{0x0007FFCFFFC00000L});
    public static final BitSet FOLLOW_ruleSimpleType_in_ruleParameter2373 = new BitSet(new long[]{0x0007FFCFFFC28242L});
    public static final BitSet FOLLOW_ruleGenericDeclarationId_in_ruleParameter2395 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGenericDeclarationInitializer_in_entryRuleGenericDeclarationInitializer2432 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleGenericDeclarationInitializer2442 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ASSIGN_in_ruleGenericDeclarationInitializer2488 = new BitSet(new long[]{0x0200001000008340L,0x00000018003FC300L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleGenericDeclarationInitializer2508 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCompoundStatement_in_ruleGenericDeclarationInitializer2546 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSpecifiedType_in_entryRuleSpecifiedType2583 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSpecifiedType2593 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleParameter_in_ruleSpecifiedType2638 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStaticAssertDeclaration_in_entryRuleStaticAssertDeclaration2673 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleStaticAssertDeclaration2683 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STATIC_ASSERT_in_ruleStaticAssertDeclaration2719 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_RULE_LPAREN_in_ruleStaticAssertDeclaration2729 = new BitSet(new long[]{0x0200001000008340L,0x00000018003FC300L});
    public static final BitSet FOLLOW_ruleBaseExpression_in_ruleStaticAssertDeclaration2749 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_RULE_COMMA_in_ruleStaticAssertDeclaration2760 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_RULE_STRING_LITERAL_in_ruleStaticAssertDeclaration2776 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_RULE_RPAREN_in_ruleStaticAssertDeclaration2792 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_RULE_SEMICOLON_in_ruleStaticAssertDeclaration2802 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSimpleType_in_entryRuleSimpleType2837 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSimpleType2847 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBaseType_in_ruleSimpleType2894 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStructOrUnionType_in_ruleSimpleType2921 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEnumType_in_ruleSimpleType2948 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ATOMIC_in_ruleSimpleType2974 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_RULE_LPAREN_in_ruleSimpleType2984 = new BitSet(new long[]{0x000001CFFF800000L});
    public static final BitSet FOLLOW_ruleBaseType_in_ruleSimpleType3006 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_ruleStructOrUnionType_in_ruleSimpleType3025 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_ruleEnumType_in_ruleSimpleType3044 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_RULE_RPAREN_in_ruleSimpleType3058 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBaseType_in_entryRuleBaseType3094 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBaseType3104 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_VOID_in_ruleBaseType3150 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_CUSTOM_in_ruleBaseType3183 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_CHAR_in_ruleBaseType3216 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_SHORT_in_ruleBaseType3243 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleBaseType3270 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_LONG_in_ruleBaseType3297 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_FLOAT_in_ruleBaseType3324 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_DOUBLE_in_ruleBaseType3351 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_SIGNED_in_ruleBaseType3378 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_UNSIGNED_in_ruleBaseType3405 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_BOOL_in_ruleBaseType3432 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_COMPLEX_in_ruleBaseType3459 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_IMAGINARY_in_ruleBaseType3486 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStructOrUnionType_in_entryRuleStructOrUnionType3522 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleStructOrUnionType3532 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStructOrUnion_in_ruleStructOrUnionType3578 = new BitSet(new long[]{0x0000001000000200L});
    public static final BitSet FOLLOW_RULE_LCBRACKET_in_ruleStructOrUnionType3591 = new BitSet(new long[]{0x0007FFCFFFC00000L});
    public static final BitSet FOLLOW_ruleGenericSpecifiedDeclaration_in_ruleStructOrUnionType3611 = new BitSet(new long[]{0x0007FFEFFFC00000L});
    public static final BitSet FOLLOW_RULE_RCBRACKET_in_ruleStructOrUnionType3623 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_IDENTIFIER_in_ruleStructOrUnionType3647 = new BitSet(new long[]{0x0000001000000002L});
    public static final BitSet FOLLOW_RULE_LCBRACKET_in_ruleStructOrUnionType3664 = new BitSet(new long[]{0x0007FFCFFFC00000L});
    public static final BitSet FOLLOW_ruleGenericSpecifiedDeclaration_in_ruleStructOrUnionType3684 = new BitSet(new long[]{0x0007FFEFFFC00000L});
    public static final BitSet FOLLOW_RULE_RCBRACKET_in_ruleStructOrUnionType3696 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStructOrUnion_in_entryRuleStructOrUnion3736 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleStructOrUnion3747 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRUCT_in_ruleStructOrUnion3787 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_UNION_in_ruleStructOrUnion3813 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEnumType_in_entryRuleEnumType3858 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleEnumType3868 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ENUM_in_ruleEnumType3904 = new BitSet(new long[]{0x0000001000000200L});
    public static final BitSet FOLLOW_RULE_LCBRACKET_in_ruleEnumType3916 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_ruleEnumeratorList_in_ruleEnumType3936 = new BitSet(new long[]{0x0000002000002000L});
    public static final BitSet FOLLOW_RULE_COMMA_in_ruleEnumType3948 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_RULE_RCBRACKET_in_ruleEnumType3960 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_IDENTIFIER_in_ruleEnumType3984 = new BitSet(new long[]{0x0000001000000002L});
    public static final BitSet FOLLOW_RULE_LCBRACKET_in_ruleEnumType4001 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_ruleEnumeratorList_in_ruleEnumType4021 = new BitSet(new long[]{0x0000002000002000L});
    public static final BitSet FOLLOW_RULE_COMMA_in_ruleEnumType4033 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_RULE_RCBRACKET_in_ruleEnumType4045 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEnumeratorList_in_entryRuleEnumeratorList4084 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleEnumeratorList4094 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEnumerator_in_ruleEnumeratorList4140 = new BitSet(new long[]{0x0000000000002002L});
    public static final BitSet FOLLOW_RULE_COMMA_in_ruleEnumeratorList4152 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_ruleEnumerator_in_ruleEnumeratorList4172 = new BitSet(new long[]{0x0000000000002002L});
    public static final BitSet FOLLOW_ruleEnumerator_in_entryRuleEnumerator4210 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleEnumerator4220 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEnumerationConstant_in_ruleEnumerator4266 = new BitSet(new long[]{0x0000000000100002L});
    public static final BitSet FOLLOW_RULE_ASSIGN_in_ruleEnumerator4278 = new BitSet(new long[]{0x0200001000008340L,0x00000018003FC300L});
    public static final BitSet FOLLOW_ruleBaseExpression_in_ruleEnumerator4298 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEnumerationConstant_in_entryRuleEnumerationConstant4336 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleEnumerationConstant4346 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_IDENTIFIER_in_ruleEnumerationConstant4387 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDeclarationSpecifier_in_entryRuleDeclarationSpecifier4427 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDeclarationSpecifier4437 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStorageClassSpecifier_in_ruleDeclarationSpecifier4484 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTypeQualifier_in_ruleDeclarationSpecifier4511 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFunctionSpecifier_in_ruleDeclarationSpecifier4538 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAlignmentSpecifier_in_ruleDeclarationSpecifier4565 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStorageClassSpecifier_in_entryRuleStorageClassSpecifier4600 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleStorageClassSpecifier4610 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_EXTERN_in_ruleStorageClassSpecifier4656 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STATIC_in_ruleStorageClassSpecifier4683 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_THREAD_LOCAL_in_ruleStorageClassSpecifier4710 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_AUTO_in_ruleStorageClassSpecifier4737 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_REGISTER_in_ruleStorageClassSpecifier4764 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTypeQualifier_in_entryRuleTypeQualifier4800 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTypeQualifier4810 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_CONST_in_ruleTypeQualifier4856 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_VOLATILE_in_ruleTypeQualifier4883 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ATOMIC_in_ruleTypeQualifier4910 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFunctionSpecifier_in_entryRuleFunctionSpecifier4946 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFunctionSpecifier4956 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INLINE_in_ruleFunctionSpecifier5002 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_NORETURN_in_ruleFunctionSpecifier5029 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAlignmentSpecifier_in_entryRuleAlignmentSpecifier5065 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAlignmentSpecifier5075 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ALIGNAS_in_ruleAlignmentSpecifier5111 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_RULE_LPAREN_in_ruleAlignmentSpecifier5121 = new BitSet(new long[]{0x0207FFDFFFC08340L,0x00000018003FC300L});
    public static final BitSet FOLLOW_ruleSimpleType_in_ruleAlignmentSpecifier5142 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_ruleBaseExpression_in_ruleAlignmentSpecifier5259 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_RULE_RPAREN_in_ruleAlignmentSpecifier5271 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExpression_in_entryRuleExpression5306 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleExpression5316 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAssignmentExpression_in_ruleExpression5362 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAssignmentExpression_in_entryRuleAssignmentExpression5396 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAssignmentExpression5406 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBaseExpression_in_ruleAssignmentExpression5453 = new BitSet(new long[]{0xFC00000000100002L,0x000000000000000FL});
    public static final BitSet FOLLOW_ruleAssignmentExpressionElement_in_ruleAssignmentExpression5483 = new BitSet(new long[]{0xFC00000000100002L,0x000000000000000FL});
    public static final BitSet FOLLOW_ruleAssignmentExpressionElement_in_entryRuleAssignmentExpressionElement5522 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAssignmentExpressionElement5532 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAssignmentOperator_in_ruleAssignmentExpressionElement5578 = new BitSet(new long[]{0x0200001000008340L,0x00000018003FC300L});
    public static final BitSet FOLLOW_ruleBaseExpression_in_ruleAssignmentExpressionElement5599 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBaseExpression_in_entryRuleBaseExpression5635 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBaseExpression5645 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleConditionalExpression_in_ruleBaseExpression5691 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleConditionalExpression_in_entryRuleConditionalExpression5725 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleConditionalExpression5735 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLogicalOrExpression_in_ruleConditionalExpression5782 = new BitSet(new long[]{0x0008000000000002L});
    public static final BitSet FOLLOW_RULE_Q_OP_in_ruleConditionalExpression5802 = new BitSet(new long[]{0x0200001000008340L,0x00000018003FC300L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleConditionalExpression5822 = new BitSet(new long[]{0x0010000000000000L});
    public static final BitSet FOLLOW_RULE_COLON_in_ruleConditionalExpression5833 = new BitSet(new long[]{0x0200001000008340L,0x00000018003FC300L});
    public static final BitSet FOLLOW_ruleConditionalExpression_in_ruleConditionalExpression5853 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLogicalOrExpression_in_entryRuleLogicalOrExpression5891 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLogicalOrExpression5901 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLogicalAndExpression_in_ruleLogicalOrExpression5948 = new BitSet(new long[]{0x0020000000000002L});
    public static final BitSet FOLLOW_RULE_LOR_OP_in_ruleLogicalOrExpression5974 = new BitSet(new long[]{0x0200001000008340L,0x00000018003FC300L});
    public static final BitSet FOLLOW_ruleLogicalOrExpression_in_ruleLogicalOrExpression6000 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLogicalAndExpression_in_entryRuleLogicalAndExpression6038 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLogicalAndExpression6048 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInclusiveOrExpression_in_ruleLogicalAndExpression6095 = new BitSet(new long[]{0x0040000000000002L});
    public static final BitSet FOLLOW_RULE_LAND_OP_in_ruleLogicalAndExpression6121 = new BitSet(new long[]{0x0200001000008340L,0x00000018003FC300L});
    public static final BitSet FOLLOW_ruleLogicalAndExpression_in_ruleLogicalAndExpression6147 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInclusiveOrExpression_in_entryRuleInclusiveOrExpression6185 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleInclusiveOrExpression6195 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExclusiveOrExpression_in_ruleInclusiveOrExpression6242 = new BitSet(new long[]{0x0080000000000002L});
    public static final BitSet FOLLOW_RULE_OR_OP_in_ruleInclusiveOrExpression6268 = new BitSet(new long[]{0x0200001000008340L,0x00000018003FC300L});
    public static final BitSet FOLLOW_ruleInclusiveOrExpression_in_ruleInclusiveOrExpression6294 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExclusiveOrExpression_in_entryRuleExclusiveOrExpression6332 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleExclusiveOrExpression6342 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAndExpression_in_ruleExclusiveOrExpression6389 = new BitSet(new long[]{0x0100000000000002L});
    public static final BitSet FOLLOW_RULE_XOR_OP_in_ruleExclusiveOrExpression6415 = new BitSet(new long[]{0x0200001000008340L,0x00000018003FC300L});
    public static final BitSet FOLLOW_ruleExclusiveOrExpression_in_ruleExclusiveOrExpression6441 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAndExpression_in_entryRuleAndExpression6479 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAndExpression6489 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEqualityExpression_in_ruleAndExpression6536 = new BitSet(new long[]{0x0200000000000002L});
    public static final BitSet FOLLOW_RULE_AND_OP_in_ruleAndExpression6562 = new BitSet(new long[]{0x0200001000008340L,0x00000018003FC300L});
    public static final BitSet FOLLOW_ruleAndExpression_in_ruleAndExpression6588 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEqualityExpression_in_entryRuleEqualityExpression6626 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleEqualityExpression6636 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRelationalExpression_in_ruleEqualityExpression6683 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000030L});
    public static final BitSet FOLLOW_ruleEqualityOperator_in_ruleEqualityExpression6713 = new BitSet(new long[]{0x0200001000008340L,0x00000018003FC300L});
    public static final BitSet FOLLOW_ruleEqualityExpression_in_ruleEqualityExpression6734 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRelationalExpression_in_entryRuleRelationalExpression6772 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRelationalExpression6782 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleShiftExpression_in_ruleRelationalExpression6829 = new BitSet(new long[]{0x00000000000000A2L,0x00000000000000C0L});
    public static final BitSet FOLLOW_ruleRelationalOperator_in_ruleRelationalExpression6859 = new BitSet(new long[]{0x0200001000008340L,0x00000018003FC300L});
    public static final BitSet FOLLOW_ruleRelationalExpression_in_ruleRelationalExpression6880 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleShiftExpression_in_entryRuleShiftExpression6918 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleShiftExpression6928 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAdditiveExpression_in_ruleShiftExpression6975 = new BitSet(new long[]{0x0000000000000002L,0x0000000000003000L});
    public static final BitSet FOLLOW_ruleShiftOperator_in_ruleShiftExpression7005 = new BitSet(new long[]{0x0200001000008340L,0x00000018003FC300L});
    public static final BitSet FOLLOW_ruleShiftExpression_in_ruleShiftExpression7026 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAdditiveExpression_in_entryRuleAdditiveExpression7064 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAdditiveExpression7074 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMultiplicativeExpression_in_ruleAdditiveExpression7121 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000300L});
    public static final BitSet FOLLOW_ruleAdditiveOperator_in_ruleAdditiveExpression7151 = new BitSet(new long[]{0x0200001000008340L,0x00000018003FC300L});
    public static final BitSet FOLLOW_ruleAdditiveExpression_in_ruleAdditiveExpression7172 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMultiplicativeExpression_in_entryRuleMultiplicativeExpression7210 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMultiplicativeExpression7220 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleUnaryExpression_in_ruleMultiplicativeExpression7267 = new BitSet(new long[]{0x0000000000000042L,0x0000000000000C00L});
    public static final BitSet FOLLOW_ruleMultiplicativeOperator_in_ruleMultiplicativeExpression7297 = new BitSet(new long[]{0x0200001000008340L,0x00000018003FC300L});
    public static final BitSet FOLLOW_ruleMultiplicativeExpression_in_ruleMultiplicativeExpression7318 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAssignmentOperator_in_entryRuleAssignmentOperator7357 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAssignmentOperator7368 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ASSIGN_in_ruleAssignmentOperator7408 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_MUL_ASSIGN_in_ruleAssignmentOperator7434 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_DIV_ASSIGN_in_ruleAssignmentOperator7460 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_MOD_ASSIGN_in_ruleAssignmentOperator7486 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ADD_ASSIGN_in_ruleAssignmentOperator7512 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_SUB_ASSIGN_in_ruleAssignmentOperator7538 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_LEFT_ASSIGN_in_ruleAssignmentOperator7564 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_RIGHT_ASSIGN_in_ruleAssignmentOperator7590 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_AND_ASSIGN_in_ruleAssignmentOperator7616 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_XOR_ASSIGN_in_ruleAssignmentOperator7642 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_OR_ASSIGN_in_ruleAssignmentOperator7668 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEqualityOperator_in_entryRuleEqualityOperator7714 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleEqualityOperator7725 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_EQ_OP_in_ruleEqualityOperator7765 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_NE_OP_in_ruleEqualityOperator7791 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRelationalOperator_in_entryRuleRelationalOperator7837 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRelationalOperator7848 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_L_OP_in_ruleRelationalOperator7888 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_G_OP_in_ruleRelationalOperator7914 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_LE_OP_in_ruleRelationalOperator7940 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_GE_OP_in_ruleRelationalOperator7966 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAdditiveOperator_in_entryRuleAdditiveOperator8012 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAdditiveOperator8023 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ADD_OP_in_ruleAdditiveOperator8063 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_MINUS_OP_in_ruleAdditiveOperator8089 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMultiplicativeOperator_in_entryRuleMultiplicativeOperator8135 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMultiplicativeOperator8146 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_MULT_OP_in_ruleMultiplicativeOperator8186 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_DIV_OP_in_ruleMultiplicativeOperator8212 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_MOD_OP_in_ruleMultiplicativeOperator8238 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleShiftOperator_in_entryRuleShiftOperator8284 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleShiftOperator8295 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_LEFT_OP_in_ruleShiftOperator8335 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_RIGHT_OP_in_ruleShiftOperator8361 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleUnaryExpression_in_entryRuleUnaryExpression8406 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleUnaryExpression8416 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePrimaryExpression_in_ruleUnaryExpression8464 = new BitSet(new long[]{0x0000000000028402L,0x000000000080C000L});
    public static final BitSet FOLLOW_rulePostfixExpressionPostfix_in_ruleUnaryExpression8494 = new BitSet(new long[]{0x0000000000028402L,0x000000000080C000L});
    public static final BitSet FOLLOW_RULE_LCBRACKET_in_ruleUnaryExpression8525 = new BitSet(new long[]{0x0200001000008340L,0x00000018003FC300L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleUnaryExpression8545 = new BitSet(new long[]{0x0000002000002000L});
    public static final BitSet FOLLOW_RULE_COMMA_in_ruleUnaryExpression8557 = new BitSet(new long[]{0x0200001000008340L,0x00000018003FC300L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleUnaryExpression8577 = new BitSet(new long[]{0x0000002000002000L});
    public static final BitSet FOLLOW_RULE_COMMA_in_ruleUnaryExpression8591 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_RULE_RCBRACKET_in_ruleUnaryExpression8603 = new BitSet(new long[]{0x0000000000028402L,0x000000000080C000L});
    public static final BitSet FOLLOW_rulePostfixExpressionPostfix_in_ruleUnaryExpression8623 = new BitSet(new long[]{0x0000000000028402L,0x000000000080C000L});
    public static final BitSet FOLLOW_RULE_INC_OP_in_ruleUnaryExpression8652 = new BitSet(new long[]{0x0200001000008340L,0x00000018003FC300L});
    public static final BitSet FOLLOW_ruleUnaryExpression_in_ruleUnaryExpression8672 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_DEC_OP_in_ruleUnaryExpression8700 = new BitSet(new long[]{0x0200001000008340L,0x00000018003FC300L});
    public static final BitSet FOLLOW_ruleUnaryExpression_in_ruleUnaryExpression8720 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_SIZEOF_in_ruleUnaryExpression8748 = new BitSet(new long[]{0x0200001000008340L,0x00000018003FC300L});
    public static final BitSet FOLLOW_ruleUnaryExpression_in_ruleUnaryExpression8859 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_LPAREN_in_ruleUnaryExpression8877 = new BitSet(new long[]{0x0007FFCFFFC00000L});
    public static final BitSet FOLLOW_ruleSpecifiedType_in_ruleUnaryExpression8897 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_RULE_RPAREN_in_ruleUnaryExpression8908 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleUNARY_OP_in_ruleUnaryExpression8947 = new BitSet(new long[]{0x0200001000008340L,0x00000018003FC300L});
    public static final BitSet FOLLOW_ruleUnaryExpression_in_ruleUnaryExpression8968 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ALIGNOF_in_ruleUnaryExpression8996 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_RULE_LPAREN_in_ruleUnaryExpression9006 = new BitSet(new long[]{0x0007FFCFFFC00000L});
    public static final BitSet FOLLOW_ruleSpecifiedType_in_ruleUnaryExpression9026 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_RULE_RPAREN_in_ruleUnaryExpression9037 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_LPAREN_in_ruleUnaryExpression9065 = new BitSet(new long[]{0x0007FFCFFFC00000L});
    public static final BitSet FOLLOW_ruleSpecifiedType_in_ruleUnaryExpression9085 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_RULE_RPAREN_in_ruleUnaryExpression9096 = new BitSet(new long[]{0x0200001000008340L,0x00000018003FC300L});
    public static final BitSet FOLLOW_ruleUnaryExpression_in_ruleUnaryExpression9207 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePrimaryExpression_in_entryRulePrimaryExpression9244 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePrimaryExpression9254 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_IDENTIFIER_in_rulePrimaryExpression9306 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStringExpression_in_rulePrimaryExpression9349 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_I_CONSTANT_in_rulePrimaryExpression9383 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_F_CONSTANT_in_rulePrimaryExpression9422 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_LPAREN_in_rulePrimaryExpression9455 = new BitSet(new long[]{0x0200001000008340L,0x00000018003FC300L});
    public static final BitSet FOLLOW_ruleExpression_in_rulePrimaryExpression9475 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_RULE_RPAREN_in_rulePrimaryExpression9486 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_GENERIC_in_rulePrimaryExpression9513 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_RULE_LPAREN_in_rulePrimaryExpression9523 = new BitSet(new long[]{0x0200001000008340L,0x00000018003FC300L});
    public static final BitSet FOLLOW_ruleAssignmentExpression_in_rulePrimaryExpression9543 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_RULE_COMMA_in_rulePrimaryExpression9555 = new BitSet(new long[]{0x0007FFCFFFC00000L,0x0000000000400000L});
    public static final BitSet FOLLOW_ruleGenericAssociation_in_rulePrimaryExpression9575 = new BitSet(new long[]{0x0000000000012000L});
    public static final BitSet FOLLOW_RULE_RPAREN_in_rulePrimaryExpression9588 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStringExpression_in_entryRuleStringExpression9624 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleStringExpression9634 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_LITERAL_in_ruleStringExpression9676 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_FUNC_NAME_in_ruleStringExpression9704 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGenericAssociation_in_entryRuleGenericAssociation9745 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleGenericAssociation9755 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSpecifiedType_in_ruleGenericAssociation9811 = new BitSet(new long[]{0x0010000000000000L});
    public static final BitSet FOLLOW_RULE_COLON_in_ruleGenericAssociation9822 = new BitSet(new long[]{0x0200001000008340L,0x00000018003FC300L});
    public static final BitSet FOLLOW_ruleAssignmentExpression_in_ruleGenericAssociation9842 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_DEFAULT_in_ruleGenericAssociation9870 = new BitSet(new long[]{0x0010000000000000L});
    public static final BitSet FOLLOW_RULE_COLON_in_ruleGenericAssociation9880 = new BitSet(new long[]{0x0200001000008340L,0x00000018003FC300L});
    public static final BitSet FOLLOW_ruleAssignmentExpression_in_ruleGenericAssociation9900 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePostfixExpressionPostfix_in_entryRulePostfixExpressionPostfix9937 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePostfixExpressionPostfix9947 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_LBRACKET_in_rulePostfixExpressionPostfix9993 = new BitSet(new long[]{0x0200001000008340L,0x00000018003FC300L});
    public static final BitSet FOLLOW_ruleExpression_in_rulePostfixExpressionPostfix10013 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_RULE_RBRACKET_in_rulePostfixExpressionPostfix10024 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_LPAREN_in_rulePostfixExpressionPostfix10051 = new BitSet(new long[]{0x0200001000018340L,0x00000018003FC300L});
    public static final BitSet FOLLOW_ruleAssignmentExpression_in_rulePostfixExpressionPostfix10072 = new BitSet(new long[]{0x0000000000012000L});
    public static final BitSet FOLLOW_ruleNextAsignement_in_rulePostfixExpressionPostfix10093 = new BitSet(new long[]{0x0000000000012000L});
    public static final BitSet FOLLOW_RULE_RPAREN_in_rulePostfixExpressionPostfix10107 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_DOT_in_rulePostfixExpressionPostfix10134 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_RULE_IDENTIFIER_in_rulePostfixExpressionPostfix10150 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_PTR_OP_in_rulePostfixExpressionPostfix10183 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_RULE_IDENTIFIER_in_rulePostfixExpressionPostfix10199 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INC_OP_in_rulePostfixExpressionPostfix10232 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_DEC_OP_in_rulePostfixExpressionPostfix10259 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNextAsignement_in_entryRuleNextAsignement10295 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleNextAsignement10305 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_COMMA_in_ruleNextAsignement10341 = new BitSet(new long[]{0x0200001000008340L,0x00000018003FC300L});
    public static final BitSet FOLLOW_ruleAssignmentExpression_in_ruleNextAsignement10361 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCompoundStatement_in_entryRuleCompoundStatement10397 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCompoundStatement10407 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_LCBRACKET_in_ruleCompoundStatement10452 = new BitSet(new long[]{0x0207FFFFFFF09B40L,0x0000001FFB7FC300L});
    public static final BitSet FOLLOW_ruleBlockItem_in_ruleCompoundStatement10472 = new BitSet(new long[]{0x0207FFFFFFF09B40L,0x0000001FFB7FC300L});
    public static final BitSet FOLLOW_RULE_RCBRACKET_in_ruleCompoundStatement10484 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBlockItem_in_entryRuleBlockItem10519 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBlockItem10529 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDeclaration_in_ruleBlockItem10585 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStatement_in_ruleBlockItem10623 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStatements_in_entryRuleStatements10660 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleStatements10670 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCompoundStatement_in_ruleStatements10723 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLabeledStatement_in_ruleStatements10751 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExpressionStatement_in_ruleStatements10778 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSelectionStatement_in_ruleStatements10805 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIterationStatement_in_ruleStatements10832 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleJumpStatement_in_ruleStatements10859 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLabeledStatement_in_entryRuleLabeledStatement10894 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLabeledStatement10904 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_CASE_in_ruleLabeledStatement10950 = new BitSet(new long[]{0x0200001000008340L,0x00000018003FC300L});
    public static final BitSet FOLLOW_ruleBaseExpression_in_ruleLabeledStatement10970 = new BitSet(new long[]{0x0010000000000000L});
    public static final BitSet FOLLOW_RULE_COLON_in_ruleLabeledStatement10981 = new BitSet(new long[]{0x0207FFDFFFF09B40L,0x0000001FFB7FC300L});
    public static final BitSet FOLLOW_ruleStatement_in_ruleLabeledStatement11001 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_DEFAULT_in_ruleLabeledStatement11029 = new BitSet(new long[]{0x0010000000000000L});
    public static final BitSet FOLLOW_RULE_COLON_in_ruleLabeledStatement11039 = new BitSet(new long[]{0x0207FFDFFFF09B40L,0x0000001FFB7FC300L});
    public static final BitSet FOLLOW_ruleStatement_in_ruleLabeledStatement11059 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStatement_in_entryRuleStatement11096 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleStatement11106 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_IDENTIFIER_in_ruleStatement11158 = new BitSet(new long[]{0x0010000000000000L});
    public static final BitSet FOLLOW_RULE_COLON_in_ruleStatement11174 = new BitSet(new long[]{0x0207FFDFFFF09B40L,0x0000001FFB7FC300L});
    public static final BitSet FOLLOW_ruleStatement_in_ruleStatement11194 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStatements_in_ruleStatement11223 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExpressionStatement_in_entryRuleExpressionStatement11258 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleExpressionStatement11268 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleExpressionStatement11323 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_RULE_SEMICOLON_in_ruleExpressionStatement11335 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSelectionStatement_in_entryRuleSelectionStatement11370 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSelectionStatement11380 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_IF_in_ruleSelectionStatement11426 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_RULE_LPAREN_in_ruleSelectionStatement11436 = new BitSet(new long[]{0x0200001000008340L,0x00000018003FC300L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleSelectionStatement11456 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_RULE_RPAREN_in_ruleSelectionStatement11467 = new BitSet(new long[]{0x0207FFDFFFF09B40L,0x0000001FFB7FC300L});
    public static final BitSet FOLLOW_ruleStatement_in_ruleSelectionStatement11487 = new BitSet(new long[]{0x0000000000000002L,0x0000000004000000L});
    public static final BitSet FOLLOW_RULE_ELSE_in_ruleSelectionStatement11505 = new BitSet(new long[]{0x0207FFDFFFF09B40L,0x0000001FFB7FC300L});
    public static final BitSet FOLLOW_ruleStatement_in_ruleSelectionStatement11526 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_SWITCH_in_ruleSelectionStatement11556 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_RULE_LPAREN_in_ruleSelectionStatement11566 = new BitSet(new long[]{0x0200001000008340L,0x00000018003FC300L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleSelectionStatement11586 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_RULE_RPAREN_in_ruleSelectionStatement11597 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_RULE_LCBRACKET_in_ruleSelectionStatement11607 = new BitSet(new long[]{0x0000002000000000L,0x0000000001400000L});
    public static final BitSet FOLLOW_ruleLabeledStatement_in_ruleSelectionStatement11627 = new BitSet(new long[]{0x0000002000000000L,0x0000000001400000L});
    public static final BitSet FOLLOW_RULE_RCBRACKET_in_ruleSelectionStatement11639 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIterationStatement_in_entryRuleIterationStatement11675 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIterationStatement11685 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_WHILE_in_ruleIterationStatement11731 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_RULE_LPAREN_in_ruleIterationStatement11741 = new BitSet(new long[]{0x0200001000008340L,0x00000018003FC300L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleIterationStatement11761 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_RULE_RPAREN_in_ruleIterationStatement11772 = new BitSet(new long[]{0x0207FFDFFFF09B40L,0x0000001FFB7FC300L});
    public static final BitSet FOLLOW_ruleStatement_in_ruleIterationStatement11792 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_DO_in_ruleIterationStatement11820 = new BitSet(new long[]{0x0207FFDFFFF09B40L,0x0000001FFB7FC300L});
    public static final BitSet FOLLOW_ruleStatement_in_ruleIterationStatement11840 = new BitSet(new long[]{0x0000000000000000L,0x0000000010000000L});
    public static final BitSet FOLLOW_RULE_WHILE_in_ruleIterationStatement11851 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_RULE_LPAREN_in_ruleIterationStatement11861 = new BitSet(new long[]{0x0200001000008340L,0x00000018003FC300L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleIterationStatement11881 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_RULE_RPAREN_in_ruleIterationStatement11892 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_RULE_SEMICOLON_in_ruleIterationStatement11902 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_FOR_in_ruleIterationStatement11929 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_RULE_LPAREN_in_ruleIterationStatement11939 = new BitSet(new long[]{0x0207FFDFFFE09B40L,0x00000018003FC300L});
    public static final BitSet FOLLOW_ruleExpressionStatement_in_ruleIterationStatement11960 = new BitSet(new long[]{0x0200001000009340L,0x00000018003FC300L});
    public static final BitSet FOLLOW_ruleDeclaration_in_ruleIterationStatement11987 = new BitSet(new long[]{0x0200001000009340L,0x00000018003FC300L});
    public static final BitSet FOLLOW_ruleExpressionStatement_in_ruleIterationStatement12009 = new BitSet(new long[]{0x0200001000018340L,0x00000018003FC300L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleIterationStatement12031 = new BitSet(new long[]{0x0000000000012000L});
    public static final BitSet FOLLOW_ruleNextField_in_ruleIterationStatement12052 = new BitSet(new long[]{0x0000000000012000L});
    public static final BitSet FOLLOW_RULE_RPAREN_in_ruleIterationStatement12066 = new BitSet(new long[]{0x0207FFDFFFF09B40L,0x0000001FFB7FC300L});
    public static final BitSet FOLLOW_ruleStatement_in_ruleIterationStatement12086 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNextField_in_entryRuleNextField12123 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleNextField12133 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_COMMA_in_ruleNextField12169 = new BitSet(new long[]{0x0200001000008340L,0x00000018003FC300L});
    public static final BitSet FOLLOW_ruleAssignmentExpression_in_ruleNextField12189 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleJumpStatement_in_entryRuleJumpStatement12225 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleJumpStatement12235 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_GOTO_in_ruleJumpStatement12281 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_RULE_IDENTIFIER_in_ruleJumpStatement12297 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_RULE_SEMICOLON_in_ruleJumpStatement12313 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_CONTINUE_in_ruleJumpStatement12340 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_RULE_SEMICOLON_in_ruleJumpStatement12350 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_BREAK_in_ruleJumpStatement12377 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_RULE_SEMICOLON_in_ruleJumpStatement12387 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_RETURN_in_ruleJumpStatement12414 = new BitSet(new long[]{0x0200001000009340L,0x00000018003FC300L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleJumpStatement12434 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_RULE_SEMICOLON_in_ruleJumpStatement12446 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleUNARY_OP_in_entryRuleUNARY_OP12483 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleUNARY_OP12494 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_AND_OP_in_ruleUNARY_OP12534 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_MULT_OP_in_ruleUNARY_OP12560 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ADD_OP_in_ruleUNARY_OP12586 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_MINUS_OP_in_ruleUNARY_OP12612 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_TILDE_in_ruleUNARY_OP12638 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_NOT_OP_in_ruleUNARY_OP12664 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_synpred1_InternalClang5158 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_synpred2_InternalClang8758 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_LCBRACKET_in_synpred4_InternalClang10707 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ELSE_in_synpred5_InternalClang11500 = new BitSet(new long[]{0x0000000000000002L});

}