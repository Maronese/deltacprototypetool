/**
 */
package org.xtext.dop.clang.clang;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Selection Statement If</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.clang.clang.SelectionStatementIf#getThen_statement <em>Then statement</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.SelectionStatementIf#getElse_statement <em>Else statement</em>}</li>
 * </ul>
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getSelectionStatementIf()
 * @model
 * @generated
 */
public interface SelectionStatementIf extends SelectionStatement
{
  /**
   * Returns the value of the '<em><b>Then statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Then statement</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Then statement</em>' containment reference.
   * @see #setThen_statement(Statement)
   * @see org.xtext.dop.clang.clang.ClangPackage#getSelectionStatementIf_Then_statement()
   * @model containment="true"
   * @generated
   */
  Statement getThen_statement();

  /**
   * Sets the value of the '{@link org.xtext.dop.clang.clang.SelectionStatementIf#getThen_statement <em>Then statement</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Then statement</em>' containment reference.
   * @see #getThen_statement()
   * @generated
   */
  void setThen_statement(Statement value);

  /**
   * Returns the value of the '<em><b>Else statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Else statement</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Else statement</em>' containment reference.
   * @see #setElse_statement(Statement)
   * @see org.xtext.dop.clang.clang.ClangPackage#getSelectionStatementIf_Else_statement()
   * @model containment="true"
   * @generated
   */
  Statement getElse_statement();

  /**
   * Sets the value of the '{@link org.xtext.dop.clang.clang.SelectionStatementIf#getElse_statement <em>Else statement</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Else statement</em>' containment reference.
   * @see #getElse_statement()
   * @generated
   */
  void setElse_statement(Statement value);

} // SelectionStatementIf
