/**
 */
package org.xtext.dop.clang.clang;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Jump Statement Break</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getJumpStatementBreak()
 * @model
 * @generated
 */
public interface JumpStatementBreak extends JumpStatement
{
} // JumpStatementBreak
