/**
 */
package org.xtext.dop.clang.clang;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Generic Declaration Inner Id</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.clang.clang.GenericDeclarationInnerId#getInner <em>Inner</em>}</li>
 * </ul>
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getGenericDeclarationInnerId()
 * @model
 * @generated
 */
public interface GenericDeclarationInnerId extends EObject
{
  /**
   * Returns the value of the '<em><b>Inner</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Inner</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Inner</em>' containment reference.
   * @see #setInner(GenericDeclarationId)
   * @see org.xtext.dop.clang.clang.ClangPackage#getGenericDeclarationInnerId_Inner()
   * @model containment="true"
   * @generated
   */
  GenericDeclarationId getInner();

  /**
   * Sets the value of the '{@link org.xtext.dop.clang.clang.GenericDeclarationInnerId#getInner <em>Inner</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Inner</em>' containment reference.
   * @see #getInner()
   * @generated
   */
  void setInner(GenericDeclarationId value);

} // GenericDeclarationInnerId
