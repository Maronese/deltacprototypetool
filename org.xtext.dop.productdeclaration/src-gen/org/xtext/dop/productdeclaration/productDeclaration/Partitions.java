/**
 */
package org.xtext.dop.productdeclaration.productDeclaration;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Partitions</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.productdeclaration.productDeclaration.Partitions#getList <em>List</em>}</li>
 * </ul>
 *
 * @see org.xtext.dop.productdeclaration.productDeclaration.ProductDeclarationPackage#getPartitions()
 * @model
 * @generated
 */
public interface Partitions extends EObject
{
  /**
   * Returns the value of the '<em><b>List</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.dop.productdeclaration.productDeclaration.Partition}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>List</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>List</em>' containment reference list.
   * @see org.xtext.dop.productdeclaration.productDeclaration.ProductDeclarationPackage#getPartitions_List()
   * @model containment="true"
   * @generated
   */
  EList<Partition> getList();

} // Partitions
