/**
 */
package org.xtext.dop.productdeclaration.productDeclaration;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Espression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.dop.productdeclaration.productDeclaration.ProductDeclarationPackage#getEspression()
 * @model
 * @generated
 */
public interface Espression extends Expression
{
} // Espression
