/**
 */
package org.xtext.dop.clang.clang;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Postfix Expression Postfix</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getPostfixExpressionPostfix()
 * @model
 * @generated
 */
public interface PostfixExpressionPostfix extends EObject
{
} // PostfixExpressionPostfix
