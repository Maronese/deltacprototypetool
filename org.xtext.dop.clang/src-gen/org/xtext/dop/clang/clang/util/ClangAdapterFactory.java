/**
 */
package org.xtext.dop.clang.clang.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import org.xtext.dop.clang.clang.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.xtext.dop.clang.clang.ClangPackage
 * @generated
 */
public class ClangAdapterFactory extends AdapterFactoryImpl
{
  /**
   * The cached model package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static ClangPackage modelPackage;

  /**
   * Creates an instance of the adapter factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ClangAdapterFactory()
  {
    if (modelPackage == null)
    {
      modelPackage = ClangPackage.eINSTANCE;
    }
  }

  /**
   * Returns whether this factory is applicable for the type of the object.
   * <!-- begin-user-doc -->
   * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
   * <!-- end-user-doc -->
   * @return whether this factory is applicable for the type of the object.
   * @generated
   */
  @Override
  public boolean isFactoryForType(Object object)
  {
    if (object == modelPackage)
    {
      return true;
    }
    if (object instanceof EObject)
    {
      return ((EObject)object).eClass().getEPackage() == modelPackage;
    }
    return false;
  }

  /**
   * The switch that delegates to the <code>createXXX</code> methods.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ClangSwitch<Adapter> modelSwitch =
    new ClangSwitch<Adapter>()
    {
      @Override
      public Adapter caseModel(Model object)
      {
        return createModelAdapter();
      }
      @Override
      public Adapter caseStandardInclude(StandardInclude object)
      {
        return createStandardIncludeAdapter();
      }
      @Override
      public Adapter caseFileInclude(FileInclude object)
      {
        return createFileIncludeAdapter();
      }
      @Override
      public Adapter caseSource(Source object)
      {
        return createSourceAdapter();
      }
      @Override
      public Adapter caseDeclaration(Declaration object)
      {
        return createDeclarationAdapter();
      }
      @Override
      public Adapter caseTypeAliasDeclaration(TypeAliasDeclaration object)
      {
        return createTypeAliasDeclarationAdapter();
      }
      @Override
      public Adapter caseGenericSpecifiedDeclaration(GenericSpecifiedDeclaration object)
      {
        return createGenericSpecifiedDeclarationAdapter();
      }
      @Override
      public Adapter caseGenericDeclarationElements(GenericDeclarationElements object)
      {
        return createGenericDeclarationElementsAdapter();
      }
      @Override
      public Adapter caseGenericDeclarationElement(GenericDeclarationElement object)
      {
        return createGenericDeclarationElementAdapter();
      }
      @Override
      public Adapter caseGenericDeclarationId(GenericDeclarationId object)
      {
        return createGenericDeclarationIdAdapter();
      }
      @Override
      public Adapter caseGenericDeclarationInnerId(GenericDeclarationInnerId object)
      {
        return createGenericDeclarationInnerIdAdapter();
      }
      @Override
      public Adapter caseGenericDeclarationIdPostfix(GenericDeclarationIdPostfix object)
      {
        return createGenericDeclarationIdPostfixAdapter();
      }
      @Override
      public Adapter caseArrayPostfix(ArrayPostfix object)
      {
        return createArrayPostfixAdapter();
      }
      @Override
      public Adapter caseParametersPostfix(ParametersPostfix object)
      {
        return createParametersPostfixAdapter();
      }
      @Override
      public Adapter caseNextParameter(NextParameter object)
      {
        return createNextParameterAdapter();
      }
      @Override
      public Adapter caseParameter(Parameter object)
      {
        return createParameterAdapter();
      }
      @Override
      public Adapter caseGenericDeclarationInitializer(GenericDeclarationInitializer object)
      {
        return createGenericDeclarationInitializerAdapter();
      }
      @Override
      public Adapter caseSpecifiedType(SpecifiedType object)
      {
        return createSpecifiedTypeAdapter();
      }
      @Override
      public Adapter caseStaticAssertDeclaration(StaticAssertDeclaration object)
      {
        return createStaticAssertDeclarationAdapter();
      }
      @Override
      public Adapter caseSimpleType(SimpleType object)
      {
        return createSimpleTypeAdapter();
      }
      @Override
      public Adapter caseBaseType(BaseType object)
      {
        return createBaseTypeAdapter();
      }
      @Override
      public Adapter caseStructOrUnionType(StructOrUnionType object)
      {
        return createStructOrUnionTypeAdapter();
      }
      @Override
      public Adapter caseEnumType(EnumType object)
      {
        return createEnumTypeAdapter();
      }
      @Override
      public Adapter caseEnumeratorList(EnumeratorList object)
      {
        return createEnumeratorListAdapter();
      }
      @Override
      public Adapter caseEnumerator(Enumerator object)
      {
        return createEnumeratorAdapter();
      }
      @Override
      public Adapter caseEnumerationConstant(EnumerationConstant object)
      {
        return createEnumerationConstantAdapter();
      }
      @Override
      public Adapter caseDeclarationSpecifier(DeclarationSpecifier object)
      {
        return createDeclarationSpecifierAdapter();
      }
      @Override
      public Adapter caseStorageClassSpecifier(StorageClassSpecifier object)
      {
        return createStorageClassSpecifierAdapter();
      }
      @Override
      public Adapter caseTypeQualifier(TypeQualifier object)
      {
        return createTypeQualifierAdapter();
      }
      @Override
      public Adapter caseFunctionSpecifier(FunctionSpecifier object)
      {
        return createFunctionSpecifierAdapter();
      }
      @Override
      public Adapter caseAlignmentSpecifier(AlignmentSpecifier object)
      {
        return createAlignmentSpecifierAdapter();
      }
      @Override
      public Adapter caseExpression(Expression object)
      {
        return createExpressionAdapter();
      }
      @Override
      public Adapter caseAssignmentExpressionElement(AssignmentExpressionElement object)
      {
        return createAssignmentExpressionElementAdapter();
      }
      @Override
      public Adapter caseStringExpression(StringExpression object)
      {
        return createStringExpressionAdapter();
      }
      @Override
      public Adapter caseGenericAssociation(GenericAssociation object)
      {
        return createGenericAssociationAdapter();
      }
      @Override
      public Adapter casePostfixExpressionPostfix(PostfixExpressionPostfix object)
      {
        return createPostfixExpressionPostfixAdapter();
      }
      @Override
      public Adapter caseNextAsignement(NextAsignement object)
      {
        return createNextAsignementAdapter();
      }
      @Override
      public Adapter caseCompoundStatement(CompoundStatement object)
      {
        return createCompoundStatementAdapter();
      }
      @Override
      public Adapter caseBlockItem(BlockItem object)
      {
        return createBlockItemAdapter();
      }
      @Override
      public Adapter caseStatements(Statements object)
      {
        return createStatementsAdapter();
      }
      @Override
      public Adapter caseLabeledStatement(LabeledStatement object)
      {
        return createLabeledStatementAdapter();
      }
      @Override
      public Adapter caseStatement(Statement object)
      {
        return createStatementAdapter();
      }
      @Override
      public Adapter caseExpressionStatement(ExpressionStatement object)
      {
        return createExpressionStatementAdapter();
      }
      @Override
      public Adapter caseSelectionStatement(SelectionStatement object)
      {
        return createSelectionStatementAdapter();
      }
      @Override
      public Adapter caseIterationStatement(IterationStatement object)
      {
        return createIterationStatementAdapter();
      }
      @Override
      public Adapter caseNextField(NextField object)
      {
        return createNextFieldAdapter();
      }
      @Override
      public Adapter caseJumpStatement(JumpStatement object)
      {
        return createJumpStatementAdapter();
      }
      @Override
      public Adapter caseCompleteName(CompleteName object)
      {
        return createCompleteNameAdapter();
      }
      @Override
      public Adapter caseSimpleName(SimpleName object)
      {
        return createSimpleNameAdapter();
      }
      @Override
      public Adapter caseGenericDeclaration0(GenericDeclaration0 object)
      {
        return createGenericDeclaration0Adapter();
      }
      @Override
      public Adapter caseGenericDeclaration1(GenericDeclaration1 object)
      {
        return createGenericDeclaration1Adapter();
      }
      @Override
      public Adapter caseGenericDeclaration2(GenericDeclaration2 object)
      {
        return createGenericDeclaration2Adapter();
      }
      @Override
      public Adapter caseGlobalName(GlobalName object)
      {
        return createGlobalNameAdapter();
      }
      @Override
      public Adapter caseExpressionInitializer(ExpressionInitializer object)
      {
        return createExpressionInitializerAdapter();
      }
      @Override
      public Adapter caseFunctionInitializer(FunctionInitializer object)
      {
        return createFunctionInitializerAdapter();
      }
      @Override
      public Adapter caseAtomic(Atomic object)
      {
        return createAtomicAdapter();
      }
      @Override
      public Adapter caseBaseTypeVoid(BaseTypeVoid object)
      {
        return createBaseTypeVoidAdapter();
      }
      @Override
      public Adapter caseCustom(Custom object)
      {
        return createCustomAdapter();
      }
      @Override
      public Adapter caseBaseTypeChar(BaseTypeChar object)
      {
        return createBaseTypeCharAdapter();
      }
      @Override
      public Adapter caseBaseTypeShort(BaseTypeShort object)
      {
        return createBaseTypeShortAdapter();
      }
      @Override
      public Adapter caseBaseTypeInt(BaseTypeInt object)
      {
        return createBaseTypeIntAdapter();
      }
      @Override
      public Adapter caseBaseTypeLong(BaseTypeLong object)
      {
        return createBaseTypeLongAdapter();
      }
      @Override
      public Adapter caseBaseTypeFloat(BaseTypeFloat object)
      {
        return createBaseTypeFloatAdapter();
      }
      @Override
      public Adapter caseBaseTypeDouble(BaseTypeDouble object)
      {
        return createBaseTypeDoubleAdapter();
      }
      @Override
      public Adapter caseBaseTypeSigned(BaseTypeSigned object)
      {
        return createBaseTypeSignedAdapter();
      }
      @Override
      public Adapter caseBaseTypeUnsigned(BaseTypeUnsigned object)
      {
        return createBaseTypeUnsignedAdapter();
      }
      @Override
      public Adapter caseBaseTypeBool(BaseTypeBool object)
      {
        return createBaseTypeBoolAdapter();
      }
      @Override
      public Adapter caseBaseTypeComplex(BaseTypeComplex object)
      {
        return createBaseTypeComplexAdapter();
      }
      @Override
      public Adapter caseBaseTypeImaginary(BaseTypeImaginary object)
      {
        return createBaseTypeImaginaryAdapter();
      }
      @Override
      public Adapter caseStorageClassSpecifierExtern(StorageClassSpecifierExtern object)
      {
        return createStorageClassSpecifierExternAdapter();
      }
      @Override
      public Adapter caseStorageClassSpecifierStatic(StorageClassSpecifierStatic object)
      {
        return createStorageClassSpecifierStaticAdapter();
      }
      @Override
      public Adapter caseStorageClassSpecifierThreadLocal(StorageClassSpecifierThreadLocal object)
      {
        return createStorageClassSpecifierThreadLocalAdapter();
      }
      @Override
      public Adapter caseStorageClassSpecifierAuto(StorageClassSpecifierAuto object)
      {
        return createStorageClassSpecifierAutoAdapter();
      }
      @Override
      public Adapter caseStorageClassSpecifierRegister(StorageClassSpecifierRegister object)
      {
        return createStorageClassSpecifierRegisterAdapter();
      }
      @Override
      public Adapter caseTypeQualifierConst(TypeQualifierConst object)
      {
        return createTypeQualifierConstAdapter();
      }
      @Override
      public Adapter caseTypeQualifierVolatile(TypeQualifierVolatile object)
      {
        return createTypeQualifierVolatileAdapter();
      }
      @Override
      public Adapter caseTypeQualifierAtomic(TypeQualifierAtomic object)
      {
        return createTypeQualifierAtomicAdapter();
      }
      @Override
      public Adapter caseFunctionSpecifierInline(FunctionSpecifierInline object)
      {
        return createFunctionSpecifierInlineAdapter();
      }
      @Override
      public Adapter caseFunctionSpecifierNoReturn(FunctionSpecifierNoReturn object)
      {
        return createFunctionSpecifierNoReturnAdapter();
      }
      @Override
      public Adapter caseAssignmentExpression(AssignmentExpression object)
      {
        return createAssignmentExpressionAdapter();
      }
      @Override
      public Adapter caseConditionalExpression(ConditionalExpression object)
      {
        return createConditionalExpressionAdapter();
      }
      @Override
      public Adapter caseLogicalOrExpression(LogicalOrExpression object)
      {
        return createLogicalOrExpressionAdapter();
      }
      @Override
      public Adapter caseLogicalAndExpression(LogicalAndExpression object)
      {
        return createLogicalAndExpressionAdapter();
      }
      @Override
      public Adapter caseInclusiveOrExpression(InclusiveOrExpression object)
      {
        return createInclusiveOrExpressionAdapter();
      }
      @Override
      public Adapter caseExclusiveOrExpression(ExclusiveOrExpression object)
      {
        return createExclusiveOrExpressionAdapter();
      }
      @Override
      public Adapter caseAndExpression(AndExpression object)
      {
        return createAndExpressionAdapter();
      }
      @Override
      public Adapter caseEqualityExpression(EqualityExpression object)
      {
        return createEqualityExpressionAdapter();
      }
      @Override
      public Adapter caseRelationalExpression(RelationalExpression object)
      {
        return createRelationalExpressionAdapter();
      }
      @Override
      public Adapter caseShiftExpression(ShiftExpression object)
      {
        return createShiftExpressionAdapter();
      }
      @Override
      public Adapter caseAdditiveExpression(AdditiveExpression object)
      {
        return createAdditiveExpressionAdapter();
      }
      @Override
      public Adapter caseMultiplicativeExpression(MultiplicativeExpression object)
      {
        return createMultiplicativeExpressionAdapter();
      }
      @Override
      public Adapter caseExpressionSimple(ExpressionSimple object)
      {
        return createExpressionSimpleAdapter();
      }
      @Override
      public Adapter caseExpressionComplex(ExpressionComplex object)
      {
        return createExpressionComplexAdapter();
      }
      @Override
      public Adapter caseExpressionIncrement(ExpressionIncrement object)
      {
        return createExpressionIncrementAdapter();
      }
      @Override
      public Adapter caseExpressionDecrement(ExpressionDecrement object)
      {
        return createExpressionDecrementAdapter();
      }
      @Override
      public Adapter caseExpressionSizeof(ExpressionSizeof object)
      {
        return createExpressionSizeofAdapter();
      }
      @Override
      public Adapter caseExpressionUnaryOp(ExpressionUnaryOp object)
      {
        return createExpressionUnaryOpAdapter();
      }
      @Override
      public Adapter caseExpressionAlign(ExpressionAlign object)
      {
        return createExpressionAlignAdapter();
      }
      @Override
      public Adapter caseExpressionCast(ExpressionCast object)
      {
        return createExpressionCastAdapter();
      }
      @Override
      public Adapter caseExpressionIdentifier(ExpressionIdentifier object)
      {
        return createExpressionIdentifierAdapter();
      }
      @Override
      public Adapter caseExpressionString(ExpressionString object)
      {
        return createExpressionStringAdapter();
      }
      @Override
      public Adapter caseExpressionInteger(ExpressionInteger object)
      {
        return createExpressionIntegerAdapter();
      }
      @Override
      public Adapter caseExpressionFloat(ExpressionFloat object)
      {
        return createExpressionFloatAdapter();
      }
      @Override
      public Adapter caseExpressionSubExpression(ExpressionSubExpression object)
      {
        return createExpressionSubExpressionAdapter();
      }
      @Override
      public Adapter caseExpressionGenericSelection(ExpressionGenericSelection object)
      {
        return createExpressionGenericSelectionAdapter();
      }
      @Override
      public Adapter caseGenericAssociationCase(GenericAssociationCase object)
      {
        return createGenericAssociationCaseAdapter();
      }
      @Override
      public Adapter caseGenericAssociationDefault(GenericAssociationDefault object)
      {
        return createGenericAssociationDefaultAdapter();
      }
      @Override
      public Adapter casePostfixExpressionPostfixExpression(PostfixExpressionPostfixExpression object)
      {
        return createPostfixExpressionPostfixExpressionAdapter();
      }
      @Override
      public Adapter casePostfixExpressionPostfixArgumentList(PostfixExpressionPostfixArgumentList object)
      {
        return createPostfixExpressionPostfixArgumentListAdapter();
      }
      @Override
      public Adapter casePostfixExpressionPostfixAccess(PostfixExpressionPostfixAccess object)
      {
        return createPostfixExpressionPostfixAccessAdapter();
      }
      @Override
      public Adapter casePostfixExpressionPostfixAccessPtr(PostfixExpressionPostfixAccessPtr object)
      {
        return createPostfixExpressionPostfixAccessPtrAdapter();
      }
      @Override
      public Adapter casePostfixExpressionPostfixIncrement(PostfixExpressionPostfixIncrement object)
      {
        return createPostfixExpressionPostfixIncrementAdapter();
      }
      @Override
      public Adapter casePostfixExpressionPostfixDecrement(PostfixExpressionPostfixDecrement object)
      {
        return createPostfixExpressionPostfixDecrementAdapter();
      }
      @Override
      public Adapter caseStatementDeclaration(StatementDeclaration object)
      {
        return createStatementDeclarationAdapter();
      }
      @Override
      public Adapter caseStatementInnerStatement(StatementInnerStatement object)
      {
        return createStatementInnerStatementAdapter();
      }
      @Override
      public Adapter caseLabeledStatementCase(LabeledStatementCase object)
      {
        return createLabeledStatementCaseAdapter();
      }
      @Override
      public Adapter caseLabeledStatementDefault(LabeledStatementDefault object)
      {
        return createLabeledStatementDefaultAdapter();
      }
      @Override
      public Adapter caseGoToPrefixStatement(GoToPrefixStatement object)
      {
        return createGoToPrefixStatementAdapter();
      }
      @Override
      public Adapter caseSelectionStatementIf(SelectionStatementIf object)
      {
        return createSelectionStatementIfAdapter();
      }
      @Override
      public Adapter caseSelectionStatementSwitch(SelectionStatementSwitch object)
      {
        return createSelectionStatementSwitchAdapter();
      }
      @Override
      public Adapter caseIterationStatementWhile(IterationStatementWhile object)
      {
        return createIterationStatementWhileAdapter();
      }
      @Override
      public Adapter caseIterationStatementDo(IterationStatementDo object)
      {
        return createIterationStatementDoAdapter();
      }
      @Override
      public Adapter caseIterationStatementFor(IterationStatementFor object)
      {
        return createIterationStatementForAdapter();
      }
      @Override
      public Adapter caseJumpStatementGoto(JumpStatementGoto object)
      {
        return createJumpStatementGotoAdapter();
      }
      @Override
      public Adapter caseJumpStatementContinue(JumpStatementContinue object)
      {
        return createJumpStatementContinueAdapter();
      }
      @Override
      public Adapter caseJumpStatementBreak(JumpStatementBreak object)
      {
        return createJumpStatementBreakAdapter();
      }
      @Override
      public Adapter caseJumpStatementReturn(JumpStatementReturn object)
      {
        return createJumpStatementReturnAdapter();
      }
      @Override
      public Adapter defaultCase(EObject object)
      {
        return createEObjectAdapter();
      }
    };

  /**
   * Creates an adapter for the <code>target</code>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param target the object to adapt.
   * @return the adapter for the <code>target</code>.
   * @generated
   */
  @Override
  public Adapter createAdapter(Notifier target)
  {
    return modelSwitch.doSwitch((EObject)target);
  }


  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.Model <em>Model</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.Model
   * @generated
   */
  public Adapter createModelAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.StandardInclude <em>Standard Include</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.StandardInclude
   * @generated
   */
  public Adapter createStandardIncludeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.FileInclude <em>File Include</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.FileInclude
   * @generated
   */
  public Adapter createFileIncludeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.Source <em>Source</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.Source
   * @generated
   */
  public Adapter createSourceAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.Declaration <em>Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.Declaration
   * @generated
   */
  public Adapter createDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.TypeAliasDeclaration <em>Type Alias Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.TypeAliasDeclaration
   * @generated
   */
  public Adapter createTypeAliasDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.GenericSpecifiedDeclaration <em>Generic Specified Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.GenericSpecifiedDeclaration
   * @generated
   */
  public Adapter createGenericSpecifiedDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.GenericDeclarationElements <em>Generic Declaration Elements</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.GenericDeclarationElements
   * @generated
   */
  public Adapter createGenericDeclarationElementsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.GenericDeclarationElement <em>Generic Declaration Element</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.GenericDeclarationElement
   * @generated
   */
  public Adapter createGenericDeclarationElementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.GenericDeclarationId <em>Generic Declaration Id</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.GenericDeclarationId
   * @generated
   */
  public Adapter createGenericDeclarationIdAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.GenericDeclarationInnerId <em>Generic Declaration Inner Id</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.GenericDeclarationInnerId
   * @generated
   */
  public Adapter createGenericDeclarationInnerIdAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.GenericDeclarationIdPostfix <em>Generic Declaration Id Postfix</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.GenericDeclarationIdPostfix
   * @generated
   */
  public Adapter createGenericDeclarationIdPostfixAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.ArrayPostfix <em>Array Postfix</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.ArrayPostfix
   * @generated
   */
  public Adapter createArrayPostfixAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.ParametersPostfix <em>Parameters Postfix</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.ParametersPostfix
   * @generated
   */
  public Adapter createParametersPostfixAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.NextParameter <em>Next Parameter</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.NextParameter
   * @generated
   */
  public Adapter createNextParameterAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.Parameter <em>Parameter</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.Parameter
   * @generated
   */
  public Adapter createParameterAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.GenericDeclarationInitializer <em>Generic Declaration Initializer</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.GenericDeclarationInitializer
   * @generated
   */
  public Adapter createGenericDeclarationInitializerAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.SpecifiedType <em>Specified Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.SpecifiedType
   * @generated
   */
  public Adapter createSpecifiedTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.StaticAssertDeclaration <em>Static Assert Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.StaticAssertDeclaration
   * @generated
   */
  public Adapter createStaticAssertDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.SimpleType <em>Simple Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.SimpleType
   * @generated
   */
  public Adapter createSimpleTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.BaseType <em>Base Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.BaseType
   * @generated
   */
  public Adapter createBaseTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.StructOrUnionType <em>Struct Or Union Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.StructOrUnionType
   * @generated
   */
  public Adapter createStructOrUnionTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.EnumType <em>Enum Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.EnumType
   * @generated
   */
  public Adapter createEnumTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.EnumeratorList <em>Enumerator List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.EnumeratorList
   * @generated
   */
  public Adapter createEnumeratorListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.Enumerator <em>Enumerator</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.Enumerator
   * @generated
   */
  public Adapter createEnumeratorAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.EnumerationConstant <em>Enumeration Constant</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.EnumerationConstant
   * @generated
   */
  public Adapter createEnumerationConstantAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.DeclarationSpecifier <em>Declaration Specifier</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.DeclarationSpecifier
   * @generated
   */
  public Adapter createDeclarationSpecifierAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.StorageClassSpecifier <em>Storage Class Specifier</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.StorageClassSpecifier
   * @generated
   */
  public Adapter createStorageClassSpecifierAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.TypeQualifier <em>Type Qualifier</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.TypeQualifier
   * @generated
   */
  public Adapter createTypeQualifierAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.FunctionSpecifier <em>Function Specifier</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.FunctionSpecifier
   * @generated
   */
  public Adapter createFunctionSpecifierAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.AlignmentSpecifier <em>Alignment Specifier</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.AlignmentSpecifier
   * @generated
   */
  public Adapter createAlignmentSpecifierAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.Expression <em>Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.Expression
   * @generated
   */
  public Adapter createExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.AssignmentExpressionElement <em>Assignment Expression Element</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.AssignmentExpressionElement
   * @generated
   */
  public Adapter createAssignmentExpressionElementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.StringExpression <em>String Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.StringExpression
   * @generated
   */
  public Adapter createStringExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.GenericAssociation <em>Generic Association</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.GenericAssociation
   * @generated
   */
  public Adapter createGenericAssociationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.PostfixExpressionPostfix <em>Postfix Expression Postfix</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.PostfixExpressionPostfix
   * @generated
   */
  public Adapter createPostfixExpressionPostfixAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.NextAsignement <em>Next Asignement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.NextAsignement
   * @generated
   */
  public Adapter createNextAsignementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.CompoundStatement <em>Compound Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.CompoundStatement
   * @generated
   */
  public Adapter createCompoundStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.BlockItem <em>Block Item</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.BlockItem
   * @generated
   */
  public Adapter createBlockItemAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.Statements <em>Statements</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.Statements
   * @generated
   */
  public Adapter createStatementsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.LabeledStatement <em>Labeled Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.LabeledStatement
   * @generated
   */
  public Adapter createLabeledStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.Statement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.Statement
   * @generated
   */
  public Adapter createStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.ExpressionStatement <em>Expression Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.ExpressionStatement
   * @generated
   */
  public Adapter createExpressionStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.SelectionStatement <em>Selection Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.SelectionStatement
   * @generated
   */
  public Adapter createSelectionStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.IterationStatement <em>Iteration Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.IterationStatement
   * @generated
   */
  public Adapter createIterationStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.NextField <em>Next Field</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.NextField
   * @generated
   */
  public Adapter createNextFieldAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.JumpStatement <em>Jump Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.JumpStatement
   * @generated
   */
  public Adapter createJumpStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.CompleteName <em>Complete Name</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.CompleteName
   * @generated
   */
  public Adapter createCompleteNameAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.SimpleName <em>Simple Name</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.SimpleName
   * @generated
   */
  public Adapter createSimpleNameAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.GenericDeclaration0 <em>Generic Declaration0</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.GenericDeclaration0
   * @generated
   */
  public Adapter createGenericDeclaration0Adapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.GenericDeclaration1 <em>Generic Declaration1</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.GenericDeclaration1
   * @generated
   */
  public Adapter createGenericDeclaration1Adapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.GenericDeclaration2 <em>Generic Declaration2</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.GenericDeclaration2
   * @generated
   */
  public Adapter createGenericDeclaration2Adapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.GlobalName <em>Global Name</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.GlobalName
   * @generated
   */
  public Adapter createGlobalNameAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.ExpressionInitializer <em>Expression Initializer</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.ExpressionInitializer
   * @generated
   */
  public Adapter createExpressionInitializerAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.FunctionInitializer <em>Function Initializer</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.FunctionInitializer
   * @generated
   */
  public Adapter createFunctionInitializerAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.Atomic <em>Atomic</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.Atomic
   * @generated
   */
  public Adapter createAtomicAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.BaseTypeVoid <em>Base Type Void</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.BaseTypeVoid
   * @generated
   */
  public Adapter createBaseTypeVoidAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.Custom <em>Custom</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.Custom
   * @generated
   */
  public Adapter createCustomAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.BaseTypeChar <em>Base Type Char</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.BaseTypeChar
   * @generated
   */
  public Adapter createBaseTypeCharAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.BaseTypeShort <em>Base Type Short</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.BaseTypeShort
   * @generated
   */
  public Adapter createBaseTypeShortAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.BaseTypeInt <em>Base Type Int</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.BaseTypeInt
   * @generated
   */
  public Adapter createBaseTypeIntAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.BaseTypeLong <em>Base Type Long</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.BaseTypeLong
   * @generated
   */
  public Adapter createBaseTypeLongAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.BaseTypeFloat <em>Base Type Float</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.BaseTypeFloat
   * @generated
   */
  public Adapter createBaseTypeFloatAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.BaseTypeDouble <em>Base Type Double</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.BaseTypeDouble
   * @generated
   */
  public Adapter createBaseTypeDoubleAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.BaseTypeSigned <em>Base Type Signed</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.BaseTypeSigned
   * @generated
   */
  public Adapter createBaseTypeSignedAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.BaseTypeUnsigned <em>Base Type Unsigned</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.BaseTypeUnsigned
   * @generated
   */
  public Adapter createBaseTypeUnsignedAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.BaseTypeBool <em>Base Type Bool</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.BaseTypeBool
   * @generated
   */
  public Adapter createBaseTypeBoolAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.BaseTypeComplex <em>Base Type Complex</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.BaseTypeComplex
   * @generated
   */
  public Adapter createBaseTypeComplexAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.BaseTypeImaginary <em>Base Type Imaginary</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.BaseTypeImaginary
   * @generated
   */
  public Adapter createBaseTypeImaginaryAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.StorageClassSpecifierExtern <em>Storage Class Specifier Extern</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.StorageClassSpecifierExtern
   * @generated
   */
  public Adapter createStorageClassSpecifierExternAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.StorageClassSpecifierStatic <em>Storage Class Specifier Static</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.StorageClassSpecifierStatic
   * @generated
   */
  public Adapter createStorageClassSpecifierStaticAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.StorageClassSpecifierThreadLocal <em>Storage Class Specifier Thread Local</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.StorageClassSpecifierThreadLocal
   * @generated
   */
  public Adapter createStorageClassSpecifierThreadLocalAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.StorageClassSpecifierAuto <em>Storage Class Specifier Auto</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.StorageClassSpecifierAuto
   * @generated
   */
  public Adapter createStorageClassSpecifierAutoAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.StorageClassSpecifierRegister <em>Storage Class Specifier Register</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.StorageClassSpecifierRegister
   * @generated
   */
  public Adapter createStorageClassSpecifierRegisterAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.TypeQualifierConst <em>Type Qualifier Const</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.TypeQualifierConst
   * @generated
   */
  public Adapter createTypeQualifierConstAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.TypeQualifierVolatile <em>Type Qualifier Volatile</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.TypeQualifierVolatile
   * @generated
   */
  public Adapter createTypeQualifierVolatileAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.TypeQualifierAtomic <em>Type Qualifier Atomic</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.TypeQualifierAtomic
   * @generated
   */
  public Adapter createTypeQualifierAtomicAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.FunctionSpecifierInline <em>Function Specifier Inline</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.FunctionSpecifierInline
   * @generated
   */
  public Adapter createFunctionSpecifierInlineAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.FunctionSpecifierNoReturn <em>Function Specifier No Return</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.FunctionSpecifierNoReturn
   * @generated
   */
  public Adapter createFunctionSpecifierNoReturnAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.AssignmentExpression <em>Assignment Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.AssignmentExpression
   * @generated
   */
  public Adapter createAssignmentExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.ConditionalExpression <em>Conditional Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.ConditionalExpression
   * @generated
   */
  public Adapter createConditionalExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.LogicalOrExpression <em>Logical Or Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.LogicalOrExpression
   * @generated
   */
  public Adapter createLogicalOrExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.LogicalAndExpression <em>Logical And Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.LogicalAndExpression
   * @generated
   */
  public Adapter createLogicalAndExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.InclusiveOrExpression <em>Inclusive Or Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.InclusiveOrExpression
   * @generated
   */
  public Adapter createInclusiveOrExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.ExclusiveOrExpression <em>Exclusive Or Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.ExclusiveOrExpression
   * @generated
   */
  public Adapter createExclusiveOrExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.AndExpression <em>And Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.AndExpression
   * @generated
   */
  public Adapter createAndExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.EqualityExpression <em>Equality Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.EqualityExpression
   * @generated
   */
  public Adapter createEqualityExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.RelationalExpression <em>Relational Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.RelationalExpression
   * @generated
   */
  public Adapter createRelationalExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.ShiftExpression <em>Shift Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.ShiftExpression
   * @generated
   */
  public Adapter createShiftExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.AdditiveExpression <em>Additive Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.AdditiveExpression
   * @generated
   */
  public Adapter createAdditiveExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.MultiplicativeExpression <em>Multiplicative Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.MultiplicativeExpression
   * @generated
   */
  public Adapter createMultiplicativeExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.ExpressionSimple <em>Expression Simple</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.ExpressionSimple
   * @generated
   */
  public Adapter createExpressionSimpleAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.ExpressionComplex <em>Expression Complex</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.ExpressionComplex
   * @generated
   */
  public Adapter createExpressionComplexAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.ExpressionIncrement <em>Expression Increment</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.ExpressionIncrement
   * @generated
   */
  public Adapter createExpressionIncrementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.ExpressionDecrement <em>Expression Decrement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.ExpressionDecrement
   * @generated
   */
  public Adapter createExpressionDecrementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.ExpressionSizeof <em>Expression Sizeof</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.ExpressionSizeof
   * @generated
   */
  public Adapter createExpressionSizeofAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.ExpressionUnaryOp <em>Expression Unary Op</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.ExpressionUnaryOp
   * @generated
   */
  public Adapter createExpressionUnaryOpAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.ExpressionAlign <em>Expression Align</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.ExpressionAlign
   * @generated
   */
  public Adapter createExpressionAlignAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.ExpressionCast <em>Expression Cast</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.ExpressionCast
   * @generated
   */
  public Adapter createExpressionCastAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.ExpressionIdentifier <em>Expression Identifier</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.ExpressionIdentifier
   * @generated
   */
  public Adapter createExpressionIdentifierAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.ExpressionString <em>Expression String</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.ExpressionString
   * @generated
   */
  public Adapter createExpressionStringAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.ExpressionInteger <em>Expression Integer</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.ExpressionInteger
   * @generated
   */
  public Adapter createExpressionIntegerAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.ExpressionFloat <em>Expression Float</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.ExpressionFloat
   * @generated
   */
  public Adapter createExpressionFloatAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.ExpressionSubExpression <em>Expression Sub Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.ExpressionSubExpression
   * @generated
   */
  public Adapter createExpressionSubExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.ExpressionGenericSelection <em>Expression Generic Selection</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.ExpressionGenericSelection
   * @generated
   */
  public Adapter createExpressionGenericSelectionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.GenericAssociationCase <em>Generic Association Case</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.GenericAssociationCase
   * @generated
   */
  public Adapter createGenericAssociationCaseAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.GenericAssociationDefault <em>Generic Association Default</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.GenericAssociationDefault
   * @generated
   */
  public Adapter createGenericAssociationDefaultAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.PostfixExpressionPostfixExpression <em>Postfix Expression Postfix Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.PostfixExpressionPostfixExpression
   * @generated
   */
  public Adapter createPostfixExpressionPostfixExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.PostfixExpressionPostfixArgumentList <em>Postfix Expression Postfix Argument List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.PostfixExpressionPostfixArgumentList
   * @generated
   */
  public Adapter createPostfixExpressionPostfixArgumentListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.PostfixExpressionPostfixAccess <em>Postfix Expression Postfix Access</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.PostfixExpressionPostfixAccess
   * @generated
   */
  public Adapter createPostfixExpressionPostfixAccessAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.PostfixExpressionPostfixAccessPtr <em>Postfix Expression Postfix Access Ptr</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.PostfixExpressionPostfixAccessPtr
   * @generated
   */
  public Adapter createPostfixExpressionPostfixAccessPtrAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.PostfixExpressionPostfixIncrement <em>Postfix Expression Postfix Increment</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.PostfixExpressionPostfixIncrement
   * @generated
   */
  public Adapter createPostfixExpressionPostfixIncrementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.PostfixExpressionPostfixDecrement <em>Postfix Expression Postfix Decrement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.PostfixExpressionPostfixDecrement
   * @generated
   */
  public Adapter createPostfixExpressionPostfixDecrementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.StatementDeclaration <em>Statement Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.StatementDeclaration
   * @generated
   */
  public Adapter createStatementDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.StatementInnerStatement <em>Statement Inner Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.StatementInnerStatement
   * @generated
   */
  public Adapter createStatementInnerStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.LabeledStatementCase <em>Labeled Statement Case</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.LabeledStatementCase
   * @generated
   */
  public Adapter createLabeledStatementCaseAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.LabeledStatementDefault <em>Labeled Statement Default</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.LabeledStatementDefault
   * @generated
   */
  public Adapter createLabeledStatementDefaultAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.GoToPrefixStatement <em>Go To Prefix Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.GoToPrefixStatement
   * @generated
   */
  public Adapter createGoToPrefixStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.SelectionStatementIf <em>Selection Statement If</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.SelectionStatementIf
   * @generated
   */
  public Adapter createSelectionStatementIfAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.SelectionStatementSwitch <em>Selection Statement Switch</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.SelectionStatementSwitch
   * @generated
   */
  public Adapter createSelectionStatementSwitchAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.IterationStatementWhile <em>Iteration Statement While</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.IterationStatementWhile
   * @generated
   */
  public Adapter createIterationStatementWhileAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.IterationStatementDo <em>Iteration Statement Do</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.IterationStatementDo
   * @generated
   */
  public Adapter createIterationStatementDoAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.IterationStatementFor <em>Iteration Statement For</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.IterationStatementFor
   * @generated
   */
  public Adapter createIterationStatementForAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.JumpStatementGoto <em>Jump Statement Goto</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.JumpStatementGoto
   * @generated
   */
  public Adapter createJumpStatementGotoAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.JumpStatementContinue <em>Jump Statement Continue</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.JumpStatementContinue
   * @generated
   */
  public Adapter createJumpStatementContinueAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.JumpStatementBreak <em>Jump Statement Break</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.JumpStatementBreak
   * @generated
   */
  public Adapter createJumpStatementBreakAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.dop.clang.clang.JumpStatementReturn <em>Jump Statement Return</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.dop.clang.clang.JumpStatementReturn
   * @generated
   */
  public Adapter createJumpStatementReturnAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for the default case.
   * <!-- begin-user-doc -->
   * This default implementation returns null.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @generated
   */
  public Adapter createEObjectAdapter()
  {
    return null;
  }

} //ClangAdapterFactory
