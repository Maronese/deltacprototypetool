package org.xtext.dop.codebase.interpreter

import org.xtext.dop.clang.clang.Declaration
import org.xtext.dop.clang.clang.GlobalName
import org.xtext.dop.codebase.codeBase.AddDeclaration
import org.xtext.dop.codebase.codeBase.AddFileInclude
import org.xtext.dop.codebase.codeBase.AddSource
import org.xtext.dop.codebase.codeBase.AddStandardInclude
import org.xtext.dop.codebase.codeBase.Adds
import org.xtext.dop.codebase.codeBase.DeltaModule
import org.xtext.dop.codebase.codeBase.FileOperation
import org.xtext.dop.codebase.codeBase.Modifies
import org.xtext.dop.codebase.codeBase.ModifyFunction
import org.xtext.dop.codebase.codeBase.RemoveFileInclude
import org.xtext.dop.codebase.codeBase.RemoveGlobal
import org.xtext.dop.codebase.codeBase.RemoveSource
import org.xtext.dop.codebase.codeBase.RemoveStandardInclude
import org.xtext.dop.codebase.codeBase.Removes
import org.xtext.dop.codebase.codeBase.SourceOperation
import org.xtext.dop.rappresentation.DopResource
import org.xtext.dop.rappresentation.Project
import org.xtext.dop.rappresentation.Resource
import org.xtext.dop.clang.inspector.Inspector
import org.xtext.dop.clang.clang.GenericSpecifiedDeclaration
import org.xtext.dop.clang.clang.FunctionInitializer
import org.xtext.dop.clang.clang.FileInclude

class Interpreter {
	
	Project project
	extension Inspector inspector
	
	new(Project project) {
		this.project = project
		this.inspector = new Inspector
	}
	
	def interpret(DeltaModule delta) {
		for(FileOperation op : delta.body) {
			switch op {
				Adds:
					for(AddSource source : (op as Adds).files) {
						val fileName = source.fileName.name
						val resource = new DopResource(fileName, source.code)
						resource.creator = delta.name
						project.addResource(fileName , resource) //no ext
					}
				Removes:
					for(RemoveSource source : (op as Removes).files) {
						println("remove ->"+source.fileName.name)
						println(project.removeResource(source.fileName.name))
					}
				Modifies:
					{
						val source = project.getResource((op as Modifies).fileName.name)
						for(SourceOperation act : (op as Modifies).action) {
							source.interpret(act)
						}
					}
			}
		}
	}
	
	def interpret(Resource resource, SourceOperation op) {
		val model = resource.getSource
		switch op {
			AddDeclaration:
				{
					var i = model.indexFirstFunction
					//println("index: "+i)
					if(/*c =*/!(op as AddDeclaration).decl.eAllContents.filter(typeof(FunctionInitializer)).empty||i==-1) {
						//println("is funct:"+c)
						model.declarations.add((op as AddDeclaration).decl)
					}
					//model.declarations.add((op as AddDeclaration).decl)
					//println("is funct:"+c)
					//
					//println("index: "+i)
					else {
						//println("is funct:"+c)
						model.declarations.add(i, (op as AddDeclaration).decl)
					}
						
				}
			AddStandardInclude:
				{
					model.includesStd.add((op as AddStandardInclude).incl)
					//da segnalare error se *.h
				}
			AddFileInclude:
				{
					model.includesFile.add((op as AddFileInclude).incl)
					//da segnalare error se *.h
				}
			RemoveGlobal:
				{
					var Declaration target
					for(Declaration decl : model.declarations) {
						val global = decl.eAllContents.filter(typeof(GlobalName)).findFirst[it.name==(op as RemoveGlobal).global]
						if(global!=null) {
							target = decl
							return model.declarations.remove(target)
						}
					}
				}
			RemoveStandardInclude:
				{
					val include = (op as RemoveStandardInclude).incl
					if(include.wild) {
						model.includesStd.clear
					}
					else {
						val target = model.includesStd.findFirst[it.std.name==include.std.name] //no controllo ext
						println(include.std)
						println(model.includesStd.length)
						println(target)
						val index = model.includesStd.indexOf(target)
						println(index)
						model.includesStd.remove(index)
						println(model.includesStd.length)
					}
				}
			RemoveFileInclude:
				{
					val include = (op as RemoveFileInclude).incl
					//println("im here"+include.file)
					//println("is * "+include.file=='"*"')
					if(include.file=='"*"') {
						model.includesFile.clear
					}
					else {
						val target = model.includesFile.findFirst[println(include.file+"\n"+it.file);println(it.file==include.file)]
						val index = model.includesFile.indexOf(target)
						model.includesFile.remove(index)
					}
				}
			ModifyFunction://da rivedere (mantiene signature cambia solo il body)
				{
					//var Declaration target
					//var index = 0
					val name = (op as ModifyFunction).decl.eAllContents.filter(typeof(GlobalName)).next.name
					println("name = "+name)
					val block = (op as ModifyFunction).decl.eAllContents.filter(typeof(FunctionInitializer)).next.block
					println("block = "+block)
					/*for(Declaration decl : model.declarations) {
						//index++
						val global = decl.eAllContents.filter(typeof(GlobalName)).findFirst[it.name==name]
						if(global!=null) {
							target = decl
							model.declarations.remove(index)
							//da gestire il caso original
							model.declarations.add(index, (op as ModifyFunction).decl)
							return model.declarations.remove(target)
						}
					}*/
					val global = model.declarations.findFirst[
							it instanceof GenericSpecifiedDeclaration &&					//è una declaration
							!it.eAllContents.filter(typeof(FunctionInitializer)).empty && 	//è un metodo
							it.eAllContents.filter(typeof(GlobalName)).next.name==name		//il suo nome è <name>
						]
					global.eAllContents.filter(typeof(FunctionInitializer)).next.block = block
				}
		}
	}
	
	/*def interpret(Declaration elem) {
		elem.eAllContents.filter(typeof(GenericDeclarationInnerId))
		switch elem {
			GenericSpecifiedDeclaration:
				(elem as GenericSpecifiedDeclaration).interpret
			default:
				false
		}
	}
	
	def interpret(GenericSpecifiedDeclaration elem) {
		elem.decl.interpret
	}
	
	def interpret(GenericDeclarationElements elem) {
		for(GenericDeclarationElement gen : elem.els) {
			if(gen.interpret) {
				return true
			}
		}
		false
	}
	
	
	def interpret(GenericDeclarationElement elem) {
		elem.id.interpret
	}
	
	def interpret(GenericDeclarationId elem) {
		switch elem {
			
		}
	}
	
	def interpret(GenericDeclarationInnerId elem) {
		switch elem {
			
		}
	}
	
	def interpret(GenericDeclarationIdPostfix elem) {
		switch elem {
			
		}
	}
	
	def interpret(ArrayPostfix elem) {
		switch elem {
			
		}
	}
	
	def interpret(ParametersPostfix elem) {
		switch elem {
			
		}
	}
	
	def interpret(Parameter elem) {
		switch elem {
			
		}
	}*/
}