/*
 * generated by Xtext
 */
package org.xtext.dop.clang.ui.contentassist.antlr;

import java.util.Collection;
import java.util.Map;
import java.util.HashMap;

import org.antlr.runtime.RecognitionException;
import org.eclipse.xtext.AbstractElement;
import org.eclipse.xtext.ui.editor.contentassist.antlr.AbstractContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.FollowElement;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;

import com.google.inject.Inject;

import org.xtext.dop.clang.services.ClangGrammarAccess;

public class ClangParser extends AbstractContentAssistParser {
	
	@Inject
	private ClangGrammarAccess grammarAccess;
	
	private Map<AbstractElement, String> nameMappings;
	
	@Override
	protected org.xtext.dop.clang.ui.contentassist.antlr.internal.InternalClangParser createParser() {
		org.xtext.dop.clang.ui.contentassist.antlr.internal.InternalClangParser result = new org.xtext.dop.clang.ui.contentassist.antlr.internal.InternalClangParser(null);
		result.setGrammarAccess(grammarAccess);
		return result;
	}
	
	@Override
	protected String getRuleName(AbstractElement element) {
		if (nameMappings == null) {
			nameMappings = new HashMap<AbstractElement, String>() {
				private static final long serialVersionUID = 1L;
				{
					put(grammarAccess.getStandardIncludeAccess().getAlternatives_3(), "rule__StandardInclude__Alternatives_3");
					put(grammarAccess.getSourceAccess().getAlternatives(), "rule__Source__Alternatives");
					put(grammarAccess.getDeclarationAccess().getAlternatives(), "rule__Declaration__Alternatives");
					put(grammarAccess.getGenericDeclarationIdAccess().getAlternatives(), "rule__GenericDeclarationId__Alternatives");
					put(grammarAccess.getGenericDeclarationInnerIdAccess().getAlternatives(), "rule__GenericDeclarationInnerId__Alternatives");
					put(grammarAccess.getGenericDeclarationIdPostfixAccess().getAlternatives(), "rule__GenericDeclarationIdPostfix__Alternatives");
					put(grammarAccess.getArrayPostfixAccess().getAlternatives_2(), "rule__ArrayPostfix__Alternatives_2");
					put(grammarAccess.getGenericDeclarationInitializerAccess().getAlternatives(), "rule__GenericDeclarationInitializer__Alternatives");
					put(grammarAccess.getSimpleTypeAccess().getAlternatives(), "rule__SimpleType__Alternatives");
					put(grammarAccess.getSimpleTypeAccess().getTypeAlternatives_3_3_0(), "rule__SimpleType__TypeAlternatives_3_3_0");
					put(grammarAccess.getBaseTypeAccess().getAlternatives(), "rule__BaseType__Alternatives");
					put(grammarAccess.getStructOrUnionTypeAccess().getAlternatives_1(), "rule__StructOrUnionType__Alternatives_1");
					put(grammarAccess.getStructOrUnionAccess().getAlternatives(), "rule__StructOrUnion__Alternatives");
					put(grammarAccess.getEnumTypeAccess().getAlternatives_1(), "rule__EnumType__Alternatives_1");
					put(grammarAccess.getDeclarationSpecifierAccess().getAlternatives(), "rule__DeclarationSpecifier__Alternatives");
					put(grammarAccess.getStorageClassSpecifierAccess().getAlternatives(), "rule__StorageClassSpecifier__Alternatives");
					put(grammarAccess.getTypeQualifierAccess().getAlternatives(), "rule__TypeQualifier__Alternatives");
					put(grammarAccess.getFunctionSpecifierAccess().getAlternatives(), "rule__FunctionSpecifier__Alternatives");
					put(grammarAccess.getAlignmentSpecifierAccess().getAlternatives_2(), "rule__AlignmentSpecifier__Alternatives_2");
					put(grammarAccess.getAssignmentOperatorAccess().getAlternatives(), "rule__AssignmentOperator__Alternatives");
					put(grammarAccess.getEqualityOperatorAccess().getAlternatives(), "rule__EqualityOperator__Alternatives");
					put(grammarAccess.getRelationalOperatorAccess().getAlternatives(), "rule__RelationalOperator__Alternatives");
					put(grammarAccess.getAdditiveOperatorAccess().getAlternatives(), "rule__AdditiveOperator__Alternatives");
					put(grammarAccess.getMultiplicativeOperatorAccess().getAlternatives(), "rule__MultiplicativeOperator__Alternatives");
					put(grammarAccess.getShiftOperatorAccess().getAlternatives(), "rule__ShiftOperator__Alternatives");
					put(grammarAccess.getUnaryExpressionAccess().getAlternatives(), "rule__UnaryExpression__Alternatives");
					put(grammarAccess.getUnaryExpressionAccess().getAlternatives_4_2(), "rule__UnaryExpression__Alternatives_4_2");
					put(grammarAccess.getPrimaryExpressionAccess().getAlternatives(), "rule__PrimaryExpression__Alternatives");
					put(grammarAccess.getStringExpressionAccess().getAlternatives(), "rule__StringExpression__Alternatives");
					put(grammarAccess.getGenericAssociationAccess().getAlternatives(), "rule__GenericAssociation__Alternatives");
					put(grammarAccess.getPostfixExpressionPostfixAccess().getAlternatives(), "rule__PostfixExpressionPostfix__Alternatives");
					put(grammarAccess.getBlockItemAccess().getAlternatives(), "rule__BlockItem__Alternatives");
					put(grammarAccess.getStatementsAccess().getAlternatives(), "rule__Statements__Alternatives");
					put(grammarAccess.getLabeledStatementAccess().getAlternatives(), "rule__LabeledStatement__Alternatives");
					put(grammarAccess.getStatementAccess().getAlternatives(), "rule__Statement__Alternatives");
					put(grammarAccess.getSelectionStatementAccess().getAlternatives(), "rule__SelectionStatement__Alternatives");
					put(grammarAccess.getIterationStatementAccess().getAlternatives(), "rule__IterationStatement__Alternatives");
					put(grammarAccess.getIterationStatementAccess().getAlternatives_2_3(), "rule__IterationStatement__Alternatives_2_3");
					put(grammarAccess.getJumpStatementAccess().getAlternatives(), "rule__JumpStatement__Alternatives");
					put(grammarAccess.getUNARY_OPAccess().getAlternatives(), "rule__UNARY_OP__Alternatives");
					put(grammarAccess.getModelAccess().getGroup(), "rule__Model__Group__0");
					put(grammarAccess.getStandardIncludeAccess().getGroup(), "rule__StandardInclude__Group__0");
					put(grammarAccess.getFileIncludeAccess().getGroup(), "rule__FileInclude__Group__0");
					put(grammarAccess.getSourceAccess().getGroup_0(), "rule__Source__Group_0__0");
					put(grammarAccess.getSourceAccess().getGroup_1(), "rule__Source__Group_1__0");
					put(grammarAccess.getTypeAliasDeclarationAccess().getGroup(), "rule__TypeAliasDeclaration__Group__0");
					put(grammarAccess.getGenericSpecifiedDeclarationAccess().getGroup(), "rule__GenericSpecifiedDeclaration__Group__0");
					put(grammarAccess.getGenericDeclarationElementsAccess().getGroup(), "rule__GenericDeclarationElements__Group__0");
					put(grammarAccess.getGenericDeclarationElementsAccess().getGroup_1(), "rule__GenericDeclarationElements__Group_1__0");
					put(grammarAccess.getGenericDeclarationElementAccess().getGroup(), "rule__GenericDeclarationElement__Group__0");
					put(grammarAccess.getGenericDeclarationIdAccess().getGroup_0(), "rule__GenericDeclarationId__Group_0__0");
					put(grammarAccess.getGenericDeclarationIdAccess().getGroup_1(), "rule__GenericDeclarationId__Group_1__0");
					put(grammarAccess.getGenericDeclarationIdAccess().getGroup_2(), "rule__GenericDeclarationId__Group_2__0");
					put(grammarAccess.getGenericDeclarationInnerIdAccess().getGroup_0(), "rule__GenericDeclarationInnerId__Group_0__0");
					put(grammarAccess.getGenericDeclarationInnerIdAccess().getGroup_1(), "rule__GenericDeclarationInnerId__Group_1__0");
					put(grammarAccess.getArrayPostfixAccess().getGroup(), "rule__ArrayPostfix__Group__0");
					put(grammarAccess.getParametersPostfixAccess().getGroup(), "rule__ParametersPostfix__Group__0");
					put(grammarAccess.getParametersPostfixAccess().getGroup_2(), "rule__ParametersPostfix__Group_2__0");
					put(grammarAccess.getParametersPostfixAccess().getGroup_3(), "rule__ParametersPostfix__Group_3__0");
					put(grammarAccess.getNextParameterAccess().getGroup(), "rule__NextParameter__Group__0");
					put(grammarAccess.getParameterAccess().getGroup(), "rule__Parameter__Group__0");
					put(grammarAccess.getGenericDeclarationInitializerAccess().getGroup_0(), "rule__GenericDeclarationInitializer__Group_0__0");
					put(grammarAccess.getGenericDeclarationInitializerAccess().getGroup_1(), "rule__GenericDeclarationInitializer__Group_1__0");
					put(grammarAccess.getStaticAssertDeclarationAccess().getGroup(), "rule__StaticAssertDeclaration__Group__0");
					put(grammarAccess.getSimpleTypeAccess().getGroup_3(), "rule__SimpleType__Group_3__0");
					put(grammarAccess.getBaseTypeAccess().getGroup_0(), "rule__BaseType__Group_0__0");
					put(grammarAccess.getBaseTypeAccess().getGroup_1(), "rule__BaseType__Group_1__0");
					put(grammarAccess.getBaseTypeAccess().getGroup_2(), "rule__BaseType__Group_2__0");
					put(grammarAccess.getBaseTypeAccess().getGroup_3(), "rule__BaseType__Group_3__0");
					put(grammarAccess.getBaseTypeAccess().getGroup_4(), "rule__BaseType__Group_4__0");
					put(grammarAccess.getBaseTypeAccess().getGroup_5(), "rule__BaseType__Group_5__0");
					put(grammarAccess.getBaseTypeAccess().getGroup_6(), "rule__BaseType__Group_6__0");
					put(grammarAccess.getBaseTypeAccess().getGroup_7(), "rule__BaseType__Group_7__0");
					put(grammarAccess.getBaseTypeAccess().getGroup_8(), "rule__BaseType__Group_8__0");
					put(grammarAccess.getBaseTypeAccess().getGroup_9(), "rule__BaseType__Group_9__0");
					put(grammarAccess.getBaseTypeAccess().getGroup_10(), "rule__BaseType__Group_10__0");
					put(grammarAccess.getBaseTypeAccess().getGroup_11(), "rule__BaseType__Group_11__0");
					put(grammarAccess.getBaseTypeAccess().getGroup_12(), "rule__BaseType__Group_12__0");
					put(grammarAccess.getStructOrUnionTypeAccess().getGroup(), "rule__StructOrUnionType__Group__0");
					put(grammarAccess.getStructOrUnionTypeAccess().getGroup_1_0(), "rule__StructOrUnionType__Group_1_0__0");
					put(grammarAccess.getStructOrUnionTypeAccess().getGroup_1_1(), "rule__StructOrUnionType__Group_1_1__0");
					put(grammarAccess.getStructOrUnionTypeAccess().getGroup_1_1_1(), "rule__StructOrUnionType__Group_1_1_1__0");
					put(grammarAccess.getEnumTypeAccess().getGroup(), "rule__EnumType__Group__0");
					put(grammarAccess.getEnumTypeAccess().getGroup_1_0(), "rule__EnumType__Group_1_0__0");
					put(grammarAccess.getEnumTypeAccess().getGroup_1_1(), "rule__EnumType__Group_1_1__0");
					put(grammarAccess.getEnumTypeAccess().getGroup_1_1_1(), "rule__EnumType__Group_1_1_1__0");
					put(grammarAccess.getEnumeratorListAccess().getGroup(), "rule__EnumeratorList__Group__0");
					put(grammarAccess.getEnumeratorListAccess().getGroup_1(), "rule__EnumeratorList__Group_1__0");
					put(grammarAccess.getEnumeratorAccess().getGroup(), "rule__Enumerator__Group__0");
					put(grammarAccess.getEnumeratorAccess().getGroup_1(), "rule__Enumerator__Group_1__0");
					put(grammarAccess.getStorageClassSpecifierAccess().getGroup_0(), "rule__StorageClassSpecifier__Group_0__0");
					put(grammarAccess.getStorageClassSpecifierAccess().getGroup_1(), "rule__StorageClassSpecifier__Group_1__0");
					put(grammarAccess.getStorageClassSpecifierAccess().getGroup_2(), "rule__StorageClassSpecifier__Group_2__0");
					put(grammarAccess.getStorageClassSpecifierAccess().getGroup_3(), "rule__StorageClassSpecifier__Group_3__0");
					put(grammarAccess.getStorageClassSpecifierAccess().getGroup_4(), "rule__StorageClassSpecifier__Group_4__0");
					put(grammarAccess.getTypeQualifierAccess().getGroup_0(), "rule__TypeQualifier__Group_0__0");
					put(grammarAccess.getTypeQualifierAccess().getGroup_1(), "rule__TypeQualifier__Group_1__0");
					put(grammarAccess.getTypeQualifierAccess().getGroup_2(), "rule__TypeQualifier__Group_2__0");
					put(grammarAccess.getFunctionSpecifierAccess().getGroup_0(), "rule__FunctionSpecifier__Group_0__0");
					put(grammarAccess.getFunctionSpecifierAccess().getGroup_1(), "rule__FunctionSpecifier__Group_1__0");
					put(grammarAccess.getAlignmentSpecifierAccess().getGroup(), "rule__AlignmentSpecifier__Group__0");
					put(grammarAccess.getAssignmentExpressionAccess().getGroup(), "rule__AssignmentExpression__Group__0");
					put(grammarAccess.getAssignmentExpressionAccess().getGroup_1(), "rule__AssignmentExpression__Group_1__0");
					put(grammarAccess.getAssignmentExpressionElementAccess().getGroup(), "rule__AssignmentExpressionElement__Group__0");
					put(grammarAccess.getConditionalExpressionAccess().getGroup(), "rule__ConditionalExpression__Group__0");
					put(grammarAccess.getConditionalExpressionAccess().getGroup_1(), "rule__ConditionalExpression__Group_1__0");
					put(grammarAccess.getLogicalOrExpressionAccess().getGroup(), "rule__LogicalOrExpression__Group__0");
					put(grammarAccess.getLogicalOrExpressionAccess().getGroup_1(), "rule__LogicalOrExpression__Group_1__0");
					put(grammarAccess.getLogicalAndExpressionAccess().getGroup(), "rule__LogicalAndExpression__Group__0");
					put(grammarAccess.getLogicalAndExpressionAccess().getGroup_1(), "rule__LogicalAndExpression__Group_1__0");
					put(grammarAccess.getInclusiveOrExpressionAccess().getGroup(), "rule__InclusiveOrExpression__Group__0");
					put(grammarAccess.getInclusiveOrExpressionAccess().getGroup_1(), "rule__InclusiveOrExpression__Group_1__0");
					put(grammarAccess.getExclusiveOrExpressionAccess().getGroup(), "rule__ExclusiveOrExpression__Group__0");
					put(grammarAccess.getExclusiveOrExpressionAccess().getGroup_1(), "rule__ExclusiveOrExpression__Group_1__0");
					put(grammarAccess.getAndExpressionAccess().getGroup(), "rule__AndExpression__Group__0");
					put(grammarAccess.getAndExpressionAccess().getGroup_1(), "rule__AndExpression__Group_1__0");
					put(grammarAccess.getEqualityExpressionAccess().getGroup(), "rule__EqualityExpression__Group__0");
					put(grammarAccess.getEqualityExpressionAccess().getGroup_1(), "rule__EqualityExpression__Group_1__0");
					put(grammarAccess.getRelationalExpressionAccess().getGroup(), "rule__RelationalExpression__Group__0");
					put(grammarAccess.getRelationalExpressionAccess().getGroup_1(), "rule__RelationalExpression__Group_1__0");
					put(grammarAccess.getShiftExpressionAccess().getGroup(), "rule__ShiftExpression__Group__0");
					put(grammarAccess.getShiftExpressionAccess().getGroup_1(), "rule__ShiftExpression__Group_1__0");
					put(grammarAccess.getAdditiveExpressionAccess().getGroup(), "rule__AdditiveExpression__Group__0");
					put(grammarAccess.getAdditiveExpressionAccess().getGroup_1(), "rule__AdditiveExpression__Group_1__0");
					put(grammarAccess.getMultiplicativeExpressionAccess().getGroup(), "rule__MultiplicativeExpression__Group__0");
					put(grammarAccess.getMultiplicativeExpressionAccess().getGroup_1(), "rule__MultiplicativeExpression__Group_1__0");
					put(grammarAccess.getUnaryExpressionAccess().getGroup_0(), "rule__UnaryExpression__Group_0__0");
					put(grammarAccess.getUnaryExpressionAccess().getGroup_0_1(), "rule__UnaryExpression__Group_0_1__0");
					put(grammarAccess.getUnaryExpressionAccess().getGroup_1(), "rule__UnaryExpression__Group_1__0");
					put(grammarAccess.getUnaryExpressionAccess().getGroup_1_3(), "rule__UnaryExpression__Group_1_3__0");
					put(grammarAccess.getUnaryExpressionAccess().getGroup_2(), "rule__UnaryExpression__Group_2__0");
					put(grammarAccess.getUnaryExpressionAccess().getGroup_3(), "rule__UnaryExpression__Group_3__0");
					put(grammarAccess.getUnaryExpressionAccess().getGroup_4(), "rule__UnaryExpression__Group_4__0");
					put(grammarAccess.getUnaryExpressionAccess().getGroup_4_2_1(), "rule__UnaryExpression__Group_4_2_1__0");
					put(grammarAccess.getUnaryExpressionAccess().getGroup_5(), "rule__UnaryExpression__Group_5__0");
					put(grammarAccess.getUnaryExpressionAccess().getGroup_6(), "rule__UnaryExpression__Group_6__0");
					put(grammarAccess.getUnaryExpressionAccess().getGroup_7(), "rule__UnaryExpression__Group_7__0");
					put(grammarAccess.getUnaryExpressionAccess().getGroup_7_1(), "rule__UnaryExpression__Group_7_1__0");
					put(grammarAccess.getPrimaryExpressionAccess().getGroup_0(), "rule__PrimaryExpression__Group_0__0");
					put(grammarAccess.getPrimaryExpressionAccess().getGroup_1(), "rule__PrimaryExpression__Group_1__0");
					put(grammarAccess.getPrimaryExpressionAccess().getGroup_2(), "rule__PrimaryExpression__Group_2__0");
					put(grammarAccess.getPrimaryExpressionAccess().getGroup_3(), "rule__PrimaryExpression__Group_3__0");
					put(grammarAccess.getPrimaryExpressionAccess().getGroup_4(), "rule__PrimaryExpression__Group_4__0");
					put(grammarAccess.getPrimaryExpressionAccess().getGroup_5(), "rule__PrimaryExpression__Group_5__0");
					put(grammarAccess.getPrimaryExpressionAccess().getGroup_5_4(), "rule__PrimaryExpression__Group_5_4__0");
					put(grammarAccess.getGenericAssociationAccess().getGroup_0(), "rule__GenericAssociation__Group_0__0");
					put(grammarAccess.getGenericAssociationAccess().getGroup_1(), "rule__GenericAssociation__Group_1__0");
					put(grammarAccess.getPostfixExpressionPostfixAccess().getGroup_0(), "rule__PostfixExpressionPostfix__Group_0__0");
					put(grammarAccess.getPostfixExpressionPostfixAccess().getGroup_1(), "rule__PostfixExpressionPostfix__Group_1__0");
					put(grammarAccess.getPostfixExpressionPostfixAccess().getGroup_1_2(), "rule__PostfixExpressionPostfix__Group_1_2__0");
					put(grammarAccess.getPostfixExpressionPostfixAccess().getGroup_2(), "rule__PostfixExpressionPostfix__Group_2__0");
					put(grammarAccess.getPostfixExpressionPostfixAccess().getGroup_3(), "rule__PostfixExpressionPostfix__Group_3__0");
					put(grammarAccess.getPostfixExpressionPostfixAccess().getGroup_4(), "rule__PostfixExpressionPostfix__Group_4__0");
					put(grammarAccess.getPostfixExpressionPostfixAccess().getGroup_5(), "rule__PostfixExpressionPostfix__Group_5__0");
					put(grammarAccess.getNextAsignementAccess().getGroup(), "rule__NextAsignement__Group__0");
					put(grammarAccess.getCompoundStatementAccess().getGroup(), "rule__CompoundStatement__Group__0");
					put(grammarAccess.getBlockItemAccess().getGroup_0(), "rule__BlockItem__Group_0__0");
					put(grammarAccess.getBlockItemAccess().getGroup_1(), "rule__BlockItem__Group_1__0");
					put(grammarAccess.getLabeledStatementAccess().getGroup_0(), "rule__LabeledStatement__Group_0__0");
					put(grammarAccess.getLabeledStatementAccess().getGroup_1(), "rule__LabeledStatement__Group_1__0");
					put(grammarAccess.getStatementAccess().getGroup_0(), "rule__Statement__Group_0__0");
					put(grammarAccess.getExpressionStatementAccess().getGroup(), "rule__ExpressionStatement__Group__0");
					put(grammarAccess.getSelectionStatementAccess().getGroup_0(), "rule__SelectionStatement__Group_0__0");
					put(grammarAccess.getSelectionStatementAccess().getGroup_0_6(), "rule__SelectionStatement__Group_0_6__0");
					put(grammarAccess.getSelectionStatementAccess().getGroup_1(), "rule__SelectionStatement__Group_1__0");
					put(grammarAccess.getIterationStatementAccess().getGroup_0(), "rule__IterationStatement__Group_0__0");
					put(grammarAccess.getIterationStatementAccess().getGroup_1(), "rule__IterationStatement__Group_1__0");
					put(grammarAccess.getIterationStatementAccess().getGroup_2(), "rule__IterationStatement__Group_2__0");
					put(grammarAccess.getIterationStatementAccess().getGroup_2_5(), "rule__IterationStatement__Group_2_5__0");
					put(grammarAccess.getNextFieldAccess().getGroup(), "rule__NextField__Group__0");
					put(grammarAccess.getJumpStatementAccess().getGroup_0(), "rule__JumpStatement__Group_0__0");
					put(grammarAccess.getJumpStatementAccess().getGroup_1(), "rule__JumpStatement__Group_1__0");
					put(grammarAccess.getJumpStatementAccess().getGroup_2(), "rule__JumpStatement__Group_2__0");
					put(grammarAccess.getJumpStatementAccess().getGroup_3(), "rule__JumpStatement__Group_3__0");
					put(grammarAccess.getModelAccess().getIncludesStdAssignment_0(), "rule__Model__IncludesStdAssignment_0");
					put(grammarAccess.getModelAccess().getIncludesFileAssignment_1(), "rule__Model__IncludesFileAssignment_1");
					put(grammarAccess.getModelAccess().getDeclarationsAssignment_2(), "rule__Model__DeclarationsAssignment_2");
					put(grammarAccess.getStandardIncludeAccess().getStdAssignment_3_0(), "rule__StandardInclude__StdAssignment_3_0");
					put(grammarAccess.getStandardIncludeAccess().getWildAssignment_3_1(), "rule__StandardInclude__WildAssignment_3_1");
					put(grammarAccess.getFileIncludeAccess().getFileAssignment_1(), "rule__FileInclude__FileAssignment_1");
					put(grammarAccess.getSourceAccess().getNameAssignment_0_1(), "rule__Source__NameAssignment_0_1");
					put(grammarAccess.getSourceAccess().getExtAssignment_0_3(), "rule__Source__ExtAssignment_0_3");
					put(grammarAccess.getSourceAccess().getNameAssignment_1_1(), "rule__Source__NameAssignment_1_1");
					put(grammarAccess.getTypeAliasDeclarationAccess().getTdeclAssignment_1(), "rule__TypeAliasDeclaration__TdeclAssignment_1");
					put(grammarAccess.getGenericSpecifiedDeclarationAccess().getSpecsAssignment_0(), "rule__GenericSpecifiedDeclaration__SpecsAssignment_0");
					put(grammarAccess.getGenericSpecifiedDeclarationAccess().getTypeAssignment_1(), "rule__GenericSpecifiedDeclaration__TypeAssignment_1");
					put(grammarAccess.getGenericSpecifiedDeclarationAccess().getDeclAssignment_2(), "rule__GenericSpecifiedDeclaration__DeclAssignment_2");
					put(grammarAccess.getGenericDeclarationElementsAccess().getElsAssignment_0(), "rule__GenericDeclarationElements__ElsAssignment_0");
					put(grammarAccess.getGenericDeclarationElementsAccess().getNextAssignment_1_1(), "rule__GenericDeclarationElements__NextAssignment_1_1");
					put(grammarAccess.getGenericDeclarationElementAccess().getIdAssignment_0(), "rule__GenericDeclarationElement__IdAssignment_0");
					put(grammarAccess.getGenericDeclarationElementAccess().getInitAssignment_1(), "rule__GenericDeclarationElement__InitAssignment_1");
					put(grammarAccess.getGenericDeclarationIdAccess().getPointersAssignment_0_1(), "rule__GenericDeclarationId__PointersAssignment_0_1");
					put(grammarAccess.getGenericDeclarationIdAccess().getRestrictAssignment_0_2(), "rule__GenericDeclarationId__RestrictAssignment_0_2");
					put(grammarAccess.getGenericDeclarationIdAccess().getInnerAssignment_0_3(), "rule__GenericDeclarationId__InnerAssignment_0_3");
					put(grammarAccess.getGenericDeclarationIdAccess().getPostsAssignment_0_4(), "rule__GenericDeclarationId__PostsAssignment_0_4");
					put(grammarAccess.getGenericDeclarationIdAccess().getInnerAssignment_1_1(), "rule__GenericDeclarationId__InnerAssignment_1_1");
					put(grammarAccess.getGenericDeclarationIdAccess().getPostsAssignment_1_2(), "rule__GenericDeclarationId__PostsAssignment_1_2");
					put(grammarAccess.getGenericDeclarationIdAccess().getPostsAssignment_2_1(), "rule__GenericDeclarationId__PostsAssignment_2_1");
					put(grammarAccess.getGenericDeclarationIdAccess().getInnerAssignment_2_2(), "rule__GenericDeclarationId__InnerAssignment_2_2");
					put(grammarAccess.getGenericDeclarationInnerIdAccess().getNameAssignment_0_1(), "rule__GenericDeclarationInnerId__NameAssignment_0_1");
					put(grammarAccess.getGenericDeclarationInnerIdAccess().getInnerAssignment_1_1(), "rule__GenericDeclarationInnerId__InnerAssignment_1_1");
					put(grammarAccess.getGenericDeclarationIdPostfixAccess().getArrayAssignment_0(), "rule__GenericDeclarationIdPostfix__ArrayAssignment_0");
					put(grammarAccess.getGenericDeclarationIdPostfixAccess().getFunctionAssignment_1(), "rule__GenericDeclarationIdPostfix__FunctionAssignment_1");
					put(grammarAccess.getArrayPostfixAccess().getStarAssignment_2_0(), "rule__ArrayPostfix__StarAssignment_2_0");
					put(grammarAccess.getArrayPostfixAccess().getExpAssignment_2_1(), "rule__ArrayPostfix__ExpAssignment_2_1");
					put(grammarAccess.getParametersPostfixAccess().getElsAssignment_2_0(), "rule__ParametersPostfix__ElsAssignment_2_0");
					put(grammarAccess.getParametersPostfixAccess().getNextAssignment_2_1(), "rule__ParametersPostfix__NextAssignment_2_1");
					put(grammarAccess.getParametersPostfixAccess().getMoreAssignment_3_1(), "rule__ParametersPostfix__MoreAssignment_3_1");
					put(grammarAccess.getNextParameterAccess().getElsAssignment_1(), "rule__NextParameter__ElsAssignment_1");
					put(grammarAccess.getParameterAccess().getSpecsAssignment_0(), "rule__Parameter__SpecsAssignment_0");
					put(grammarAccess.getParameterAccess().getTypeAssignment_1(), "rule__Parameter__TypeAssignment_1");
					put(grammarAccess.getParameterAccess().getIdAssignment_2(), "rule__Parameter__IdAssignment_2");
					put(grammarAccess.getGenericDeclarationInitializerAccess().getExpAssignment_0_2(), "rule__GenericDeclarationInitializer__ExpAssignment_0_2");
					put(grammarAccess.getGenericDeclarationInitializerAccess().getBlockAssignment_1_1(), "rule__GenericDeclarationInitializer__BlockAssignment_1_1");
					put(grammarAccess.getSpecifiedTypeAccess().getParAssignment(), "rule__SpecifiedType__ParAssignment");
					put(grammarAccess.getStaticAssertDeclarationAccess().getExpAssignment_2(), "rule__StaticAssertDeclaration__ExpAssignment_2");
					put(grammarAccess.getStaticAssertDeclarationAccess().getStringAssignment_4(), "rule__StaticAssertDeclaration__StringAssignment_4");
					put(grammarAccess.getSimpleTypeAccess().getTypeAssignment_3_3(), "rule__SimpleType__TypeAssignment_3_3");
					put(grammarAccess.getBaseTypeAccess().getIdAssignment_1_1(), "rule__BaseType__IdAssignment_1_1");
					put(grammarAccess.getStructOrUnionTypeAccess().getKindAssignment_0(), "rule__StructOrUnionType__KindAssignment_0");
					put(grammarAccess.getStructOrUnionTypeAccess().getContentAssignment_1_0_1(), "rule__StructOrUnionType__ContentAssignment_1_0_1");
					put(grammarAccess.getStructOrUnionTypeAccess().getNameAssignment_1_1_0(), "rule__StructOrUnionType__NameAssignment_1_1_0");
					put(grammarAccess.getStructOrUnionTypeAccess().getContentAssignment_1_1_1_1(), "rule__StructOrUnionType__ContentAssignment_1_1_1_1");
					put(grammarAccess.getEnumTypeAccess().getContentAssignment_1_0_1(), "rule__EnumType__ContentAssignment_1_0_1");
					put(grammarAccess.getEnumTypeAccess().getNameAssignment_1_1_0(), "rule__EnumType__NameAssignment_1_1_0");
					put(grammarAccess.getEnumTypeAccess().getContentAssignment_1_1_1_1(), "rule__EnumType__ContentAssignment_1_1_1_1");
					put(grammarAccess.getEnumeratorListAccess().getElementsAssignment_0(), "rule__EnumeratorList__ElementsAssignment_0");
					put(grammarAccess.getEnumeratorListAccess().getElementsAssignment_1_1(), "rule__EnumeratorList__ElementsAssignment_1_1");
					put(grammarAccess.getEnumeratorAccess().getNameAssignment_0(), "rule__Enumerator__NameAssignment_0");
					put(grammarAccess.getEnumeratorAccess().getExpAssignment_1_1(), "rule__Enumerator__ExpAssignment_1_1");
					put(grammarAccess.getEnumerationConstantAccess().getNameAssignment(), "rule__EnumerationConstant__NameAssignment");
					put(grammarAccess.getAlignmentSpecifierAccess().getTypeAssignment_2_0(), "rule__AlignmentSpecifier__TypeAssignment_2_0");
					put(grammarAccess.getAlignmentSpecifierAccess().getExpAssignment_2_1(), "rule__AlignmentSpecifier__ExpAssignment_2_1");
					put(grammarAccess.getAssignmentExpressionAccess().getListAssignment_1_1(), "rule__AssignmentExpression__ListAssignment_1_1");
					put(grammarAccess.getAssignmentExpressionElementAccess().getOpAssignment_0(), "rule__AssignmentExpressionElement__OpAssignment_0");
					put(grammarAccess.getAssignmentExpressionElementAccess().getExpAssignment_1(), "rule__AssignmentExpressionElement__ExpAssignment_1");
					put(grammarAccess.getConditionalExpressionAccess().getYesAssignment_1_2(), "rule__ConditionalExpression__YesAssignment_1_2");
					put(grammarAccess.getConditionalExpressionAccess().getNoAssignment_1_4(), "rule__ConditionalExpression__NoAssignment_1_4");
					put(grammarAccess.getLogicalOrExpressionAccess().getOperandAssignment_1_1(), "rule__LogicalOrExpression__OperandAssignment_1_1");
					put(grammarAccess.getLogicalOrExpressionAccess().getRightAssignment_1_2(), "rule__LogicalOrExpression__RightAssignment_1_2");
					put(grammarAccess.getLogicalAndExpressionAccess().getOperandAssignment_1_1(), "rule__LogicalAndExpression__OperandAssignment_1_1");
					put(grammarAccess.getLogicalAndExpressionAccess().getRightAssignment_1_2(), "rule__LogicalAndExpression__RightAssignment_1_2");
					put(grammarAccess.getInclusiveOrExpressionAccess().getOperandAssignment_1_1(), "rule__InclusiveOrExpression__OperandAssignment_1_1");
					put(grammarAccess.getInclusiveOrExpressionAccess().getRightAssignment_1_2(), "rule__InclusiveOrExpression__RightAssignment_1_2");
					put(grammarAccess.getExclusiveOrExpressionAccess().getOperandAssignment_1_1(), "rule__ExclusiveOrExpression__OperandAssignment_1_1");
					put(grammarAccess.getExclusiveOrExpressionAccess().getRightAssignment_1_2(), "rule__ExclusiveOrExpression__RightAssignment_1_2");
					put(grammarAccess.getAndExpressionAccess().getOperandAssignment_1_1(), "rule__AndExpression__OperandAssignment_1_1");
					put(grammarAccess.getAndExpressionAccess().getRightAssignment_1_2(), "rule__AndExpression__RightAssignment_1_2");
					put(grammarAccess.getEqualityExpressionAccess().getOperandAssignment_1_1(), "rule__EqualityExpression__OperandAssignment_1_1");
					put(grammarAccess.getEqualityExpressionAccess().getRightAssignment_1_2(), "rule__EqualityExpression__RightAssignment_1_2");
					put(grammarAccess.getRelationalExpressionAccess().getOperandAssignment_1_1(), "rule__RelationalExpression__OperandAssignment_1_1");
					put(grammarAccess.getRelationalExpressionAccess().getRightAssignment_1_2(), "rule__RelationalExpression__RightAssignment_1_2");
					put(grammarAccess.getShiftExpressionAccess().getOperandAssignment_1_1(), "rule__ShiftExpression__OperandAssignment_1_1");
					put(grammarAccess.getShiftExpressionAccess().getRightAssignment_1_2(), "rule__ShiftExpression__RightAssignment_1_2");
					put(grammarAccess.getAdditiveExpressionAccess().getOperandAssignment_1_1(), "rule__AdditiveExpression__OperandAssignment_1_1");
					put(grammarAccess.getAdditiveExpressionAccess().getRightAssignment_1_2(), "rule__AdditiveExpression__RightAssignment_1_2");
					put(grammarAccess.getMultiplicativeExpressionAccess().getOperandAssignment_1_1(), "rule__MultiplicativeExpression__OperandAssignment_1_1");
					put(grammarAccess.getMultiplicativeExpressionAccess().getRightAssignment_1_2(), "rule__MultiplicativeExpression__RightAssignment_1_2");
					put(grammarAccess.getUnaryExpressionAccess().getNextAssignment_0_1_1(), "rule__UnaryExpression__NextAssignment_0_1_1");
					put(grammarAccess.getUnaryExpressionAccess().getExpsAssignment_1_2(), "rule__UnaryExpression__ExpsAssignment_1_2");
					put(grammarAccess.getUnaryExpressionAccess().getExpsAssignment_1_3_1(), "rule__UnaryExpression__ExpsAssignment_1_3_1");
					put(grammarAccess.getUnaryExpressionAccess().getNextAssignment_1_6(), "rule__UnaryExpression__NextAssignment_1_6");
					put(grammarAccess.getUnaryExpressionAccess().getContentAssignment_2_2(), "rule__UnaryExpression__ContentAssignment_2_2");
					put(grammarAccess.getUnaryExpressionAccess().getContentAssignment_3_2(), "rule__UnaryExpression__ContentAssignment_3_2");
					put(grammarAccess.getUnaryExpressionAccess().getContentAssignment_4_2_0(), "rule__UnaryExpression__ContentAssignment_4_2_0");
					put(grammarAccess.getUnaryExpressionAccess().getTypeAssignment_4_2_1_1(), "rule__UnaryExpression__TypeAssignment_4_2_1_1");
					put(grammarAccess.getUnaryExpressionAccess().getOpAssignment_5_1(), "rule__UnaryExpression__OpAssignment_5_1");
					put(grammarAccess.getUnaryExpressionAccess().getContentAssignment_5_2(), "rule__UnaryExpression__ContentAssignment_5_2");
					put(grammarAccess.getUnaryExpressionAccess().getTypeAssignment_6_3(), "rule__UnaryExpression__TypeAssignment_6_3");
					put(grammarAccess.getUnaryExpressionAccess().getTypeAssignment_7_1_1(), "rule__UnaryExpression__TypeAssignment_7_1_1");
					put(grammarAccess.getUnaryExpressionAccess().getExpAssignment_7_2(), "rule__UnaryExpression__ExpAssignment_7_2");
					put(grammarAccess.getPrimaryExpressionAccess().getNameAssignment_0_1(), "rule__PrimaryExpression__NameAssignment_0_1");
					put(grammarAccess.getPrimaryExpressionAccess().getValueAssignment_1_1(), "rule__PrimaryExpression__ValueAssignment_1_1");
					put(grammarAccess.getPrimaryExpressionAccess().getValueAssignment_2_1(), "rule__PrimaryExpression__ValueAssignment_2_1");
					put(grammarAccess.getPrimaryExpressionAccess().getValueAssignment_3_1(), "rule__PrimaryExpression__ValueAssignment_3_1");
					put(grammarAccess.getPrimaryExpressionAccess().getParAssignment_4_2(), "rule__PrimaryExpression__ParAssignment_4_2");
					put(grammarAccess.getPrimaryExpressionAccess().getLeftAssignment_5_3(), "rule__PrimaryExpression__LeftAssignment_5_3");
					put(grammarAccess.getPrimaryExpressionAccess().getRightAssignment_5_4_1(), "rule__PrimaryExpression__RightAssignment_5_4_1");
					put(grammarAccess.getStringExpressionAccess().getNameAssignment_0(), "rule__StringExpression__NameAssignment_0");
					put(grammarAccess.getStringExpressionAccess().getNameAssignment_1(), "rule__StringExpression__NameAssignment_1");
					put(grammarAccess.getGenericAssociationAccess().getTypeAssignment_0_1(), "rule__GenericAssociation__TypeAssignment_0_1");
					put(grammarAccess.getGenericAssociationAccess().getExpAssignment_0_3(), "rule__GenericAssociation__ExpAssignment_0_3");
					put(grammarAccess.getGenericAssociationAccess().getExpAssignment_1_3(), "rule__GenericAssociation__ExpAssignment_1_3");
					put(grammarAccess.getPostfixExpressionPostfixAccess().getValueAssignment_0_2(), "rule__PostfixExpressionPostfix__ValueAssignment_0_2");
					put(grammarAccess.getPostfixExpressionPostfixAccess().getLeftAssignment_1_2_0(), "rule__PostfixExpressionPostfix__LeftAssignment_1_2_0");
					put(grammarAccess.getPostfixExpressionPostfixAccess().getNextAssignment_1_2_1(), "rule__PostfixExpressionPostfix__NextAssignment_1_2_1");
					put(grammarAccess.getPostfixExpressionPostfixAccess().getValueAssignment_2_2(), "rule__PostfixExpressionPostfix__ValueAssignment_2_2");
					put(grammarAccess.getPostfixExpressionPostfixAccess().getValueAssignment_3_2(), "rule__PostfixExpressionPostfix__ValueAssignment_3_2");
					put(grammarAccess.getNextAsignementAccess().getRightAssignment_1(), "rule__NextAsignement__RightAssignment_1");
					put(grammarAccess.getCompoundStatementAccess().getInnerAssignment_2(), "rule__CompoundStatement__InnerAssignment_2");
					put(grammarAccess.getBlockItemAccess().getDeclAssignment_0_1(), "rule__BlockItem__DeclAssignment_0_1");
					put(grammarAccess.getBlockItemAccess().getStatementAssignment_1_1(), "rule__BlockItem__StatementAssignment_1_1");
					put(grammarAccess.getLabeledStatementAccess().getExpAssignment_0_2(), "rule__LabeledStatement__ExpAssignment_0_2");
					put(grammarAccess.getLabeledStatementAccess().getStatementAssignment_0_4(), "rule__LabeledStatement__StatementAssignment_0_4");
					put(grammarAccess.getLabeledStatementAccess().getStatementAssignment_1_3(), "rule__LabeledStatement__StatementAssignment_1_3");
					put(grammarAccess.getStatementAccess().getGotoAssignment_0_1(), "rule__Statement__GotoAssignment_0_1");
					put(grammarAccess.getStatementAccess().getStatementAssignment_0_3(), "rule__Statement__StatementAssignment_0_3");
					put(grammarAccess.getExpressionStatementAccess().getExpAssignment_1(), "rule__ExpressionStatement__ExpAssignment_1");
					put(grammarAccess.getSelectionStatementAccess().getExpAssignment_0_3(), "rule__SelectionStatement__ExpAssignment_0_3");
					put(grammarAccess.getSelectionStatementAccess().getThen_statementAssignment_0_5(), "rule__SelectionStatement__Then_statementAssignment_0_5");
					put(grammarAccess.getSelectionStatementAccess().getElse_statementAssignment_0_6_1(), "rule__SelectionStatement__Else_statementAssignment_0_6_1");
					put(grammarAccess.getSelectionStatementAccess().getExpAssignment_1_3(), "rule__SelectionStatement__ExpAssignment_1_3");
					put(grammarAccess.getSelectionStatementAccess().getContentAssignment_1_6(), "rule__SelectionStatement__ContentAssignment_1_6");
					put(grammarAccess.getIterationStatementAccess().getExpAssignment_0_3(), "rule__IterationStatement__ExpAssignment_0_3");
					put(grammarAccess.getIterationStatementAccess().getStatementAssignment_0_5(), "rule__IterationStatement__StatementAssignment_0_5");
					put(grammarAccess.getIterationStatementAccess().getStatementAssignment_1_2(), "rule__IterationStatement__StatementAssignment_1_2");
					put(grammarAccess.getIterationStatementAccess().getExpAssignment_1_5(), "rule__IterationStatement__ExpAssignment_1_5");
					put(grammarAccess.getIterationStatementAccess().getField1Assignment_2_3_0(), "rule__IterationStatement__Field1Assignment_2_3_0");
					put(grammarAccess.getIterationStatementAccess().getDeclAssignment_2_3_1(), "rule__IterationStatement__DeclAssignment_2_3_1");
					put(grammarAccess.getIterationStatementAccess().getField2Assignment_2_4(), "rule__IterationStatement__Field2Assignment_2_4");
					put(grammarAccess.getIterationStatementAccess().getField3Assignment_2_5_0(), "rule__IterationStatement__Field3Assignment_2_5_0");
					put(grammarAccess.getIterationStatementAccess().getNextAssignment_2_5_1(), "rule__IterationStatement__NextAssignment_2_5_1");
					put(grammarAccess.getIterationStatementAccess().getStatementAssignment_2_7(), "rule__IterationStatement__StatementAssignment_2_7");
					put(grammarAccess.getNextFieldAccess().getNextAssignment_1(), "rule__NextField__NextAssignment_1");
					put(grammarAccess.getJumpStatementAccess().getNameAssignment_0_2(), "rule__JumpStatement__NameAssignment_0_2");
					put(grammarAccess.getJumpStatementAccess().getExpAssignment_3_2(), "rule__JumpStatement__ExpAssignment_3_2");
				}
			};
		}
		return nameMappings.get(element);
	}
	
	@Override
	protected Collection<FollowElement> getFollowElements(AbstractInternalContentAssistParser parser) {
		try {
			org.xtext.dop.clang.ui.contentassist.antlr.internal.InternalClangParser typedParser = (org.xtext.dop.clang.ui.contentassist.antlr.internal.InternalClangParser) parser;
			typedParser.entryRuleModel();
			return typedParser.getFollowElements();
		} catch(RecognitionException ex) {
			throw new RuntimeException(ex);
		}		
	}
	
	@Override
	protected String[] getInitialHiddenTokens() {
		return new String[] { "RULE_WS", "RULE_ML_COMMENT", "RULE_SL_COMMENT" };
	}
	
	public ClangGrammarAccess getGrammarAccess() {
		return this.grammarAccess;
	}
	
	public void setGrammarAccess(ClangGrammarAccess grammarAccess) {
		this.grammarAccess = grammarAccess;
	}
}
