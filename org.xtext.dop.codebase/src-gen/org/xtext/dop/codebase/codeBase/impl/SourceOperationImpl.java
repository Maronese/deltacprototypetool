/**
 */
package org.xtext.dop.codebase.codeBase.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.xtext.dop.codebase.codeBase.CodeBasePackage;
import org.xtext.dop.codebase.codeBase.SourceOperation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Source Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SourceOperationImpl extends MinimalEObjectImpl.Container implements SourceOperation
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SourceOperationImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return CodeBasePackage.Literals.SOURCE_OPERATION;
  }

} //SourceOperationImpl
