/**
 */
package org.xtext.dop.clang.clang.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.xtext.dop.clang.clang.ClangPackage;
import org.xtext.dop.clang.clang.DeclarationSpecifier;
import org.xtext.dop.clang.clang.GenericDeclarationId;
import org.xtext.dop.clang.clang.Parameter;
import org.xtext.dop.clang.clang.SimpleType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Parameter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.clang.clang.impl.ParameterImpl#getSpecs <em>Specs</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.impl.ParameterImpl#getType <em>Type</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.impl.ParameterImpl#getId <em>Id</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ParameterImpl extends MinimalEObjectImpl.Container implements Parameter
{
  /**
   * The cached value of the '{@link #getSpecs() <em>Specs</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSpecs()
   * @generated
   * @ordered
   */
  protected EList<DeclarationSpecifier> specs;

  /**
   * The cached value of the '{@link #getType() <em>Type</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getType()
   * @generated
   * @ordered
   */
  protected EList<SimpleType> type;

  /**
   * The cached value of the '{@link #getId() <em>Id</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getId()
   * @generated
   * @ordered
   */
  protected GenericDeclarationId id;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ParameterImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ClangPackage.Literals.PARAMETER;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<DeclarationSpecifier> getSpecs()
  {
    if (specs == null)
    {
      specs = new EObjectContainmentEList<DeclarationSpecifier>(DeclarationSpecifier.class, this, ClangPackage.PARAMETER__SPECS);
    }
    return specs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<SimpleType> getType()
  {
    if (type == null)
    {
      type = new EObjectContainmentEList<SimpleType>(SimpleType.class, this, ClangPackage.PARAMETER__TYPE);
    }
    return type;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GenericDeclarationId getId()
  {
    return id;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetId(GenericDeclarationId newId, NotificationChain msgs)
  {
    GenericDeclarationId oldId = id;
    id = newId;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ClangPackage.PARAMETER__ID, oldId, newId);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setId(GenericDeclarationId newId)
  {
    if (newId != id)
    {
      NotificationChain msgs = null;
      if (id != null)
        msgs = ((InternalEObject)id).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ClangPackage.PARAMETER__ID, null, msgs);
      if (newId != null)
        msgs = ((InternalEObject)newId).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ClangPackage.PARAMETER__ID, null, msgs);
      msgs = basicSetId(newId, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ClangPackage.PARAMETER__ID, newId, newId));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case ClangPackage.PARAMETER__SPECS:
        return ((InternalEList<?>)getSpecs()).basicRemove(otherEnd, msgs);
      case ClangPackage.PARAMETER__TYPE:
        return ((InternalEList<?>)getType()).basicRemove(otherEnd, msgs);
      case ClangPackage.PARAMETER__ID:
        return basicSetId(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case ClangPackage.PARAMETER__SPECS:
        return getSpecs();
      case ClangPackage.PARAMETER__TYPE:
        return getType();
      case ClangPackage.PARAMETER__ID:
        return getId();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case ClangPackage.PARAMETER__SPECS:
        getSpecs().clear();
        getSpecs().addAll((Collection<? extends DeclarationSpecifier>)newValue);
        return;
      case ClangPackage.PARAMETER__TYPE:
        getType().clear();
        getType().addAll((Collection<? extends SimpleType>)newValue);
        return;
      case ClangPackage.PARAMETER__ID:
        setId((GenericDeclarationId)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case ClangPackage.PARAMETER__SPECS:
        getSpecs().clear();
        return;
      case ClangPackage.PARAMETER__TYPE:
        getType().clear();
        return;
      case ClangPackage.PARAMETER__ID:
        setId((GenericDeclarationId)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case ClangPackage.PARAMETER__SPECS:
        return specs != null && !specs.isEmpty();
      case ClangPackage.PARAMETER__TYPE:
        return type != null && !type.isEmpty();
      case ClangPackage.PARAMETER__ID:
        return id != null;
    }
    return super.eIsSet(featureID);
  }

} //ParameterImpl
