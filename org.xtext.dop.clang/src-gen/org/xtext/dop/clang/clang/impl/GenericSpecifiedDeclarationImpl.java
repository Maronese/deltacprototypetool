/**
 */
package org.xtext.dop.clang.clang.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.xtext.dop.clang.clang.ClangPackage;
import org.xtext.dop.clang.clang.DeclarationSpecifier;
import org.xtext.dop.clang.clang.GenericDeclarationElements;
import org.xtext.dop.clang.clang.GenericSpecifiedDeclaration;
import org.xtext.dop.clang.clang.SimpleType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Generic Specified Declaration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.clang.clang.impl.GenericSpecifiedDeclarationImpl#getSpecs <em>Specs</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.impl.GenericSpecifiedDeclarationImpl#getType <em>Type</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.impl.GenericSpecifiedDeclarationImpl#getDecl <em>Decl</em>}</li>
 * </ul>
 *
 * @generated
 */
public class GenericSpecifiedDeclarationImpl extends DeclarationImpl implements GenericSpecifiedDeclaration
{
  /**
   * The cached value of the '{@link #getSpecs() <em>Specs</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSpecs()
   * @generated
   * @ordered
   */
  protected EList<DeclarationSpecifier> specs;

  /**
   * The cached value of the '{@link #getType() <em>Type</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getType()
   * @generated
   * @ordered
   */
  protected EList<SimpleType> type;

  /**
   * The cached value of the '{@link #getDecl() <em>Decl</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDecl()
   * @generated
   * @ordered
   */
  protected GenericDeclarationElements decl;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected GenericSpecifiedDeclarationImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ClangPackage.Literals.GENERIC_SPECIFIED_DECLARATION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<DeclarationSpecifier> getSpecs()
  {
    if (specs == null)
    {
      specs = new EObjectContainmentEList<DeclarationSpecifier>(DeclarationSpecifier.class, this, ClangPackage.GENERIC_SPECIFIED_DECLARATION__SPECS);
    }
    return specs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<SimpleType> getType()
  {
    if (type == null)
    {
      type = new EObjectContainmentEList<SimpleType>(SimpleType.class, this, ClangPackage.GENERIC_SPECIFIED_DECLARATION__TYPE);
    }
    return type;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GenericDeclarationElements getDecl()
  {
    return decl;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDecl(GenericDeclarationElements newDecl, NotificationChain msgs)
  {
    GenericDeclarationElements oldDecl = decl;
    decl = newDecl;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ClangPackage.GENERIC_SPECIFIED_DECLARATION__DECL, oldDecl, newDecl);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDecl(GenericDeclarationElements newDecl)
  {
    if (newDecl != decl)
    {
      NotificationChain msgs = null;
      if (decl != null)
        msgs = ((InternalEObject)decl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ClangPackage.GENERIC_SPECIFIED_DECLARATION__DECL, null, msgs);
      if (newDecl != null)
        msgs = ((InternalEObject)newDecl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ClangPackage.GENERIC_SPECIFIED_DECLARATION__DECL, null, msgs);
      msgs = basicSetDecl(newDecl, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ClangPackage.GENERIC_SPECIFIED_DECLARATION__DECL, newDecl, newDecl));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case ClangPackage.GENERIC_SPECIFIED_DECLARATION__SPECS:
        return ((InternalEList<?>)getSpecs()).basicRemove(otherEnd, msgs);
      case ClangPackage.GENERIC_SPECIFIED_DECLARATION__TYPE:
        return ((InternalEList<?>)getType()).basicRemove(otherEnd, msgs);
      case ClangPackage.GENERIC_SPECIFIED_DECLARATION__DECL:
        return basicSetDecl(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case ClangPackage.GENERIC_SPECIFIED_DECLARATION__SPECS:
        return getSpecs();
      case ClangPackage.GENERIC_SPECIFIED_DECLARATION__TYPE:
        return getType();
      case ClangPackage.GENERIC_SPECIFIED_DECLARATION__DECL:
        return getDecl();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case ClangPackage.GENERIC_SPECIFIED_DECLARATION__SPECS:
        getSpecs().clear();
        getSpecs().addAll((Collection<? extends DeclarationSpecifier>)newValue);
        return;
      case ClangPackage.GENERIC_SPECIFIED_DECLARATION__TYPE:
        getType().clear();
        getType().addAll((Collection<? extends SimpleType>)newValue);
        return;
      case ClangPackage.GENERIC_SPECIFIED_DECLARATION__DECL:
        setDecl((GenericDeclarationElements)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case ClangPackage.GENERIC_SPECIFIED_DECLARATION__SPECS:
        getSpecs().clear();
        return;
      case ClangPackage.GENERIC_SPECIFIED_DECLARATION__TYPE:
        getType().clear();
        return;
      case ClangPackage.GENERIC_SPECIFIED_DECLARATION__DECL:
        setDecl((GenericDeclarationElements)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case ClangPackage.GENERIC_SPECIFIED_DECLARATION__SPECS:
        return specs != null && !specs.isEmpty();
      case ClangPackage.GENERIC_SPECIFIED_DECLARATION__TYPE:
        return type != null && !type.isEmpty();
      case ClangPackage.GENERIC_SPECIFIED_DECLARATION__DECL:
        return decl != null;
    }
    return super.eIsSet(featureID);
  }

} //GenericSpecifiedDeclarationImpl
