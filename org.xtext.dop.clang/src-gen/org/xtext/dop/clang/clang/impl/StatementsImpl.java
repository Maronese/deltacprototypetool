/**
 */
package org.xtext.dop.clang.clang.impl;

import org.eclipse.emf.ecore.EClass;

import org.xtext.dop.clang.clang.ClangPackage;
import org.xtext.dop.clang.clang.Statements;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Statements</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class StatementsImpl extends StatementImpl implements Statements
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected StatementsImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ClangPackage.Literals.STATEMENTS;
  }

} //StatementsImpl
