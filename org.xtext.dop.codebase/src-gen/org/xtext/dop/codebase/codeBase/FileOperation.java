/**
 */
package org.xtext.dop.codebase.codeBase;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>File Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.dop.codebase.codeBase.CodeBasePackage#getFileOperation()
 * @model
 * @generated
 */
public interface FileOperation extends EObject
{
} // FileOperation
