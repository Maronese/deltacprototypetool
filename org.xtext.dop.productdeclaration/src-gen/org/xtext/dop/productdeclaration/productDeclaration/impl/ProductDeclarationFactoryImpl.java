/**
 */
package org.xtext.dop.productdeclaration.productDeclaration.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.xtext.dop.productdeclaration.productDeclaration.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ProductDeclarationFactoryImpl extends EFactoryImpl implements ProductDeclarationFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static ProductDeclarationFactory init()
  {
    try
    {
      ProductDeclarationFactory theProductDeclarationFactory = (ProductDeclarationFactory)EPackage.Registry.INSTANCE.getEFactory(ProductDeclarationPackage.eNS_URI);
      if (theProductDeclarationFactory != null)
      {
        return theProductDeclarationFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new ProductDeclarationFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ProductDeclarationFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case ProductDeclarationPackage.PRODUCT_DECLARATION: return createProductDeclaration();
      case ProductDeclarationPackage.FEATURES: return createFeatures();
      case ProductDeclarationPackage.DELTAS: return createDeltas();
      case ProductDeclarationPackage.PRODUCTS: return createProducts();
      case ProductDeclarationPackage.PRODUCT: return createProduct();
      case ProductDeclarationPackage.PARTITIONS: return createPartitions();
      case ProductDeclarationPackage.PARTITION: return createPartition();
      case ProductDeclarationPackage.CONSTRAINT: return createConstraint();
      case ProductDeclarationPackage.LIST_ELEM: return createListElem();
      case ProductDeclarationPackage.ELEMENT: return createElement();
      case ProductDeclarationPackage.CORE: return createCore();
      case ProductDeclarationPackage.EXPRESSION: return createExpression();
      case ProductDeclarationPackage.ESPRESSION: return createEspression();
      case ProductDeclarationPackage.OR: return createOr();
      case ProductDeclarationPackage.AND: return createAnd();
      case ProductDeclarationPackage.SUB: return createSub();
      case ProductDeclarationPackage.NOT: return createNot();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ProductDeclaration createProductDeclaration()
  {
    ProductDeclarationImpl productDeclaration = new ProductDeclarationImpl();
    return productDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Features createFeatures()
  {
    FeaturesImpl features = new FeaturesImpl();
    return features;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Deltas createDeltas()
  {
    DeltasImpl deltas = new DeltasImpl();
    return deltas;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Products createProducts()
  {
    ProductsImpl products = new ProductsImpl();
    return products;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Product createProduct()
  {
    ProductImpl product = new ProductImpl();
    return product;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Partitions createPartitions()
  {
    PartitionsImpl partitions = new PartitionsImpl();
    return partitions;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Partition createPartition()
  {
    PartitionImpl partition = new PartitionImpl();
    return partition;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Constraint createConstraint()
  {
    ConstraintImpl constraint = new ConstraintImpl();
    return constraint;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ListElem createListElem()
  {
    ListElemImpl listElem = new ListElemImpl();
    return listElem;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Element createElement()
  {
    ElementImpl element = new ElementImpl();
    return element;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Core createCore()
  {
    CoreImpl core = new CoreImpl();
    return core;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Expression createExpression()
  {
    ExpressionImpl expression = new ExpressionImpl();
    return expression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Espression createEspression()
  {
    EspressionImpl espression = new EspressionImpl();
    return espression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Or createOr()
  {
    OrImpl or = new OrImpl();
    return or;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public And createAnd()
  {
    AndImpl and = new AndImpl();
    return and;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Sub createSub()
  {
    SubImpl sub = new SubImpl();
    return sub;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Not createNot()
  {
    NotImpl not = new NotImpl();
    return not;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ProductDeclarationPackage getProductDeclarationPackage()
  {
    return (ProductDeclarationPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static ProductDeclarationPackage getPackage()
  {
    return ProductDeclarationPackage.eINSTANCE;
  }

} //ProductDeclarationFactoryImpl
