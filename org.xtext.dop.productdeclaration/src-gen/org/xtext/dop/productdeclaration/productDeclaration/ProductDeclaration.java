/**
 */
package org.xtext.dop.productdeclaration.productDeclaration;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Product Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.productdeclaration.productDeclaration.ProductDeclaration#getName <em>Name</em>}</li>
 *   <li>{@link org.xtext.dop.productdeclaration.productDeclaration.ProductDeclaration#getFeatures <em>Features</em>}</li>
 *   <li>{@link org.xtext.dop.productdeclaration.productDeclaration.ProductDeclaration#getDeltas <em>Deltas</em>}</li>
 *   <li>{@link org.xtext.dop.productdeclaration.productDeclaration.ProductDeclaration#getConstraints <em>Constraints</em>}</li>
 *   <li>{@link org.xtext.dop.productdeclaration.productDeclaration.ProductDeclaration#getPartitions <em>Partitions</em>}</li>
 *   <li>{@link org.xtext.dop.productdeclaration.productDeclaration.ProductDeclaration#getProducts <em>Products</em>}</li>
 * </ul>
 *
 * @see org.xtext.dop.productdeclaration.productDeclaration.ProductDeclarationPackage#getProductDeclaration()
 * @model
 * @generated
 */
public interface ProductDeclaration extends EObject
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see org.xtext.dop.productdeclaration.productDeclaration.ProductDeclarationPackage#getProductDeclaration_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link org.xtext.dop.productdeclaration.productDeclaration.ProductDeclaration#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Features</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Features</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Features</em>' containment reference.
   * @see #setFeatures(Features)
   * @see org.xtext.dop.productdeclaration.productDeclaration.ProductDeclarationPackage#getProductDeclaration_Features()
   * @model containment="true"
   * @generated
   */
  Features getFeatures();

  /**
   * Sets the value of the '{@link org.xtext.dop.productdeclaration.productDeclaration.ProductDeclaration#getFeatures <em>Features</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Features</em>' containment reference.
   * @see #getFeatures()
   * @generated
   */
  void setFeatures(Features value);

  /**
   * Returns the value of the '<em><b>Deltas</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Deltas</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Deltas</em>' containment reference.
   * @see #setDeltas(Deltas)
   * @see org.xtext.dop.productdeclaration.productDeclaration.ProductDeclarationPackage#getProductDeclaration_Deltas()
   * @model containment="true"
   * @generated
   */
  Deltas getDeltas();

  /**
   * Sets the value of the '{@link org.xtext.dop.productdeclaration.productDeclaration.ProductDeclaration#getDeltas <em>Deltas</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Deltas</em>' containment reference.
   * @see #getDeltas()
   * @generated
   */
  void setDeltas(Deltas value);

  /**
   * Returns the value of the '<em><b>Constraints</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Constraints</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Constraints</em>' containment reference.
   * @see #setConstraints(Core)
   * @see org.xtext.dop.productdeclaration.productDeclaration.ProductDeclarationPackage#getProductDeclaration_Constraints()
   * @model containment="true"
   * @generated
   */
  Core getConstraints();

  /**
   * Sets the value of the '{@link org.xtext.dop.productdeclaration.productDeclaration.ProductDeclaration#getConstraints <em>Constraints</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Constraints</em>' containment reference.
   * @see #getConstraints()
   * @generated
   */
  void setConstraints(Core value);

  /**
   * Returns the value of the '<em><b>Partitions</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Partitions</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Partitions</em>' containment reference.
   * @see #setPartitions(Partitions)
   * @see org.xtext.dop.productdeclaration.productDeclaration.ProductDeclarationPackage#getProductDeclaration_Partitions()
   * @model containment="true"
   * @generated
   */
  Partitions getPartitions();

  /**
   * Sets the value of the '{@link org.xtext.dop.productdeclaration.productDeclaration.ProductDeclaration#getPartitions <em>Partitions</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Partitions</em>' containment reference.
   * @see #getPartitions()
   * @generated
   */
  void setPartitions(Partitions value);

  /**
   * Returns the value of the '<em><b>Products</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Products</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Products</em>' containment reference.
   * @see #setProducts(Products)
   * @see org.xtext.dop.productdeclaration.productDeclaration.ProductDeclarationPackage#getProductDeclaration_Products()
   * @model containment="true"
   * @generated
   */
  Products getProducts();

  /**
   * Sets the value of the '{@link org.xtext.dop.productdeclaration.productDeclaration.ProductDeclaration#getProducts <em>Products</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Products</em>' containment reference.
   * @see #getProducts()
   * @generated
   */
  void setProducts(Products value);

} // ProductDeclaration
