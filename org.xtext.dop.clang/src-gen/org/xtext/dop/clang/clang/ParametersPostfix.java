/**
 */
package org.xtext.dop.clang.clang;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameters Postfix</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.clang.clang.ParametersPostfix#getEls <em>Els</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.ParametersPostfix#getNext <em>Next</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.ParametersPostfix#getMore <em>More</em>}</li>
 * </ul>
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getParametersPostfix()
 * @model
 * @generated
 */
public interface ParametersPostfix extends EObject
{
  /**
   * Returns the value of the '<em><b>Els</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.dop.clang.clang.Parameter}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Els</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Els</em>' containment reference list.
   * @see org.xtext.dop.clang.clang.ClangPackage#getParametersPostfix_Els()
   * @model containment="true"
   * @generated
   */
  EList<Parameter> getEls();

  /**
   * Returns the value of the '<em><b>Next</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.dop.clang.clang.NextParameter}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Next</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Next</em>' containment reference list.
   * @see org.xtext.dop.clang.clang.ClangPackage#getParametersPostfix_Next()
   * @model containment="true"
   * @generated
   */
  EList<NextParameter> getNext();

  /**
   * Returns the value of the '<em><b>More</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>More</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>More</em>' attribute.
   * @see #setMore(String)
   * @see org.xtext.dop.clang.clang.ClangPackage#getParametersPostfix_More()
   * @model
   * @generated
   */
  String getMore();

  /**
   * Sets the value of the '{@link org.xtext.dop.clang.clang.ParametersPostfix#getMore <em>More</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>More</em>' attribute.
   * @see #getMore()
   * @generated
   */
  void setMore(String value);

} // ParametersPostfix
