/**
 */
package org.xtext.dop.clang.clang.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.xtext.dop.clang.clang.ClangPackage;
import org.xtext.dop.clang.clang.Expression;
import org.xtext.dop.clang.clang.NextAsignement;
import org.xtext.dop.clang.clang.PostfixExpressionPostfixArgumentList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Postfix Expression Postfix Argument List</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.clang.clang.impl.PostfixExpressionPostfixArgumentListImpl#getLeft <em>Left</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.impl.PostfixExpressionPostfixArgumentListImpl#getNext <em>Next</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PostfixExpressionPostfixArgumentListImpl extends PostfixExpressionPostfixImpl implements PostfixExpressionPostfixArgumentList
{
  /**
   * The cached value of the '{@link #getLeft() <em>Left</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLeft()
   * @generated
   * @ordered
   */
  protected EList<Expression> left;

  /**
   * The cached value of the '{@link #getNext() <em>Next</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNext()
   * @generated
   * @ordered
   */
  protected EList<NextAsignement> next;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected PostfixExpressionPostfixArgumentListImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ClangPackage.Literals.POSTFIX_EXPRESSION_POSTFIX_ARGUMENT_LIST;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Expression> getLeft()
  {
    if (left == null)
    {
      left = new EObjectContainmentEList<Expression>(Expression.class, this, ClangPackage.POSTFIX_EXPRESSION_POSTFIX_ARGUMENT_LIST__LEFT);
    }
    return left;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<NextAsignement> getNext()
  {
    if (next == null)
    {
      next = new EObjectContainmentEList<NextAsignement>(NextAsignement.class, this, ClangPackage.POSTFIX_EXPRESSION_POSTFIX_ARGUMENT_LIST__NEXT);
    }
    return next;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case ClangPackage.POSTFIX_EXPRESSION_POSTFIX_ARGUMENT_LIST__LEFT:
        return ((InternalEList<?>)getLeft()).basicRemove(otherEnd, msgs);
      case ClangPackage.POSTFIX_EXPRESSION_POSTFIX_ARGUMENT_LIST__NEXT:
        return ((InternalEList<?>)getNext()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case ClangPackage.POSTFIX_EXPRESSION_POSTFIX_ARGUMENT_LIST__LEFT:
        return getLeft();
      case ClangPackage.POSTFIX_EXPRESSION_POSTFIX_ARGUMENT_LIST__NEXT:
        return getNext();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case ClangPackage.POSTFIX_EXPRESSION_POSTFIX_ARGUMENT_LIST__LEFT:
        getLeft().clear();
        getLeft().addAll((Collection<? extends Expression>)newValue);
        return;
      case ClangPackage.POSTFIX_EXPRESSION_POSTFIX_ARGUMENT_LIST__NEXT:
        getNext().clear();
        getNext().addAll((Collection<? extends NextAsignement>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case ClangPackage.POSTFIX_EXPRESSION_POSTFIX_ARGUMENT_LIST__LEFT:
        getLeft().clear();
        return;
      case ClangPackage.POSTFIX_EXPRESSION_POSTFIX_ARGUMENT_LIST__NEXT:
        getNext().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case ClangPackage.POSTFIX_EXPRESSION_POSTFIX_ARGUMENT_LIST__LEFT:
        return left != null && !left.isEmpty();
      case ClangPackage.POSTFIX_EXPRESSION_POSTFIX_ARGUMENT_LIST__NEXT:
        return next != null && !next.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //PostfixExpressionPostfixArgumentListImpl
