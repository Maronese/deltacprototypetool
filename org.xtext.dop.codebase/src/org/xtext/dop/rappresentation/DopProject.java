package org.xtext.dop.rappresentation;

import java.util.Collection;
import java.util.HashMap;

import com.google.inject.Inject;
import com.google.inject.Singleton;

public class DopProject implements Project {
	
	private String name;
	
	private HashMap<String, Resource>  map;
	
	public DopProject(String name) {
		this.name= name;
		this.map = new HashMap<>();
	}
	
	@Override
	public boolean removeResource(String path) {
		return map.containsKey(path)? map.remove(path)!=null : false;
	}

	@Override
	public Resource getResource(String path) {
		return map.get(path);
	}

	@Override
	public boolean addResource(String path, Resource fileContent) {
		return !map.containsKey(path)? map.put(path, fileContent)==null : false;
	}

	@Override
	public void setProjectName(String name) {
		this.name = name;
	}

	@Override
	public String getProjectName() {
		return name;
	}

	@Override
	public Collection<Resource> values() {
		return map.values();
	}

}
