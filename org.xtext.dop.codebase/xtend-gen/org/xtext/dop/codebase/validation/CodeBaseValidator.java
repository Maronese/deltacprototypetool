/**
 * generated by Xtext
 */
package org.xtext.dop.codebase.validation;

import org.xtext.dop.codebase.validation.AbstractCodeBaseValidator;

/**
 * This class contains custom validation rules.
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#validation
 */
@SuppressWarnings("all")
public class CodeBaseValidator extends AbstractCodeBaseValidator {
}
