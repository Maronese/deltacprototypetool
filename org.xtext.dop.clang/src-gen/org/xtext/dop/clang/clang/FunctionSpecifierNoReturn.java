/**
 */
package org.xtext.dop.clang.clang;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Function Specifier No Return</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getFunctionSpecifierNoReturn()
 * @model
 * @generated
 */
public interface FunctionSpecifierNoReturn extends FunctionSpecifier
{
} // FunctionSpecifierNoReturn
