/**
 */
package org.xtext.dop.codebase.codeBase.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.xtext.dop.clang.clang.ClangPackage;

import org.xtext.dop.codebase.codeBase.AddDeclaration;
import org.xtext.dop.codebase.codeBase.AddFileInclude;
import org.xtext.dop.codebase.codeBase.AddSource;
import org.xtext.dop.codebase.codeBase.AddStandardInclude;
import org.xtext.dop.codebase.codeBase.Adds;
import org.xtext.dop.codebase.codeBase.CodeBaseFactory;
import org.xtext.dop.codebase.codeBase.CodeBasePackage;
import org.xtext.dop.codebase.codeBase.DeltaModule;
import org.xtext.dop.codebase.codeBase.FileOperation;
import org.xtext.dop.codebase.codeBase.Modifies;
import org.xtext.dop.codebase.codeBase.ModifyFunction;
import org.xtext.dop.codebase.codeBase.RemoveFileInclude;
import org.xtext.dop.codebase.codeBase.RemoveGlobal;
import org.xtext.dop.codebase.codeBase.RemoveSource;
import org.xtext.dop.codebase.codeBase.RemoveStandardInclude;
import org.xtext.dop.codebase.codeBase.Removes;
import org.xtext.dop.codebase.codeBase.SourceOperation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CodeBasePackageImpl extends EPackageImpl implements CodeBasePackage
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass deltaModuleEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fileOperationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass addSourceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass removeSourceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass sourceOperationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass addsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass removesEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass modifiesEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass addDeclarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass addStandardIncludeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass addFileIncludeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass removeGlobalEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass removeStandardIncludeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass removeFileIncludeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass modifyFunctionEClass = null;

  /**
   * Creates an instance of the model <b>Package</b>, registered with
   * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
   * package URI value.
   * <p>Note: the correct way to create the package is via the static
   * factory method {@link #init init()}, which also performs
   * initialization of the package, or returns the registered package,
   * if one already exists.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.emf.ecore.EPackage.Registry
   * @see org.xtext.dop.codebase.codeBase.CodeBasePackage#eNS_URI
   * @see #init()
   * @generated
   */
  private CodeBasePackageImpl()
  {
    super(eNS_URI, CodeBaseFactory.eINSTANCE);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static boolean isInited = false;

  /**
   * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
   * 
   * <p>This method is used to initialize {@link CodeBasePackage#eINSTANCE} when that field is accessed.
   * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #eNS_URI
   * @see #createPackageContents()
   * @see #initializePackageContents()
   * @generated
   */
  public static CodeBasePackage init()
  {
    if (isInited) return (CodeBasePackage)EPackage.Registry.INSTANCE.getEPackage(CodeBasePackage.eNS_URI);

    // Obtain or create and register package
    CodeBasePackageImpl theCodeBasePackage = (CodeBasePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof CodeBasePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new CodeBasePackageImpl());

    isInited = true;

    // Initialize simple dependencies
    ClangPackage.eINSTANCE.eClass();

    // Create package meta-data objects
    theCodeBasePackage.createPackageContents();

    // Initialize created meta-data
    theCodeBasePackage.initializePackageContents();

    // Mark meta-data to indicate it can't be changed
    theCodeBasePackage.freeze();

  
    // Update the registry and return the package
    EPackage.Registry.INSTANCE.put(CodeBasePackage.eNS_URI, theCodeBasePackage);
    return theCodeBasePackage;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDeltaModule()
  {
    return deltaModuleEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getDeltaModule_Name()
  {
    return (EAttribute)deltaModuleEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDeltaModule_Body()
  {
    return (EReference)deltaModuleEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFileOperation()
  {
    return fileOperationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAddSource()
  {
    return addSourceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAddSource_FileName()
  {
    return (EReference)addSourceEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAddSource_Code()
  {
    return (EReference)addSourceEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRemoveSource()
  {
    return removeSourceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRemoveSource_FileName()
  {
    return (EReference)removeSourceEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSourceOperation()
  {
    return sourceOperationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAdds()
  {
    return addsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAdds_Files()
  {
    return (EReference)addsEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRemoves()
  {
    return removesEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRemoves_Files()
  {
    return (EReference)removesEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getModifies()
  {
    return modifiesEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getModifies_FileName()
  {
    return (EReference)modifiesEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getModifies_Action()
  {
    return (EReference)modifiesEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAddDeclaration()
  {
    return addDeclarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAddDeclaration_Decl()
  {
    return (EReference)addDeclarationEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAddStandardInclude()
  {
    return addStandardIncludeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAddStandardInclude_Incl()
  {
    return (EReference)addStandardIncludeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAddFileInclude()
  {
    return addFileIncludeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAddFileInclude_Incl()
  {
    return (EReference)addFileIncludeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRemoveGlobal()
  {
    return removeGlobalEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getRemoveGlobal_Global()
  {
    return (EAttribute)removeGlobalEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRemoveStandardInclude()
  {
    return removeStandardIncludeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRemoveStandardInclude_Incl()
  {
    return (EReference)removeStandardIncludeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRemoveFileInclude()
  {
    return removeFileIncludeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRemoveFileInclude_Incl()
  {
    return (EReference)removeFileIncludeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getModifyFunction()
  {
    return modifyFunctionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getModifyFunction_Decl()
  {
    return (EReference)modifyFunctionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CodeBaseFactory getCodeBaseFactory()
  {
    return (CodeBaseFactory)getEFactoryInstance();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isCreated = false;

  /**
   * Creates the meta-model objects for the package.  This method is
   * guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void createPackageContents()
  {
    if (isCreated) return;
    isCreated = true;

    // Create classes and their features
    deltaModuleEClass = createEClass(DELTA_MODULE);
    createEAttribute(deltaModuleEClass, DELTA_MODULE__NAME);
    createEReference(deltaModuleEClass, DELTA_MODULE__BODY);

    fileOperationEClass = createEClass(FILE_OPERATION);

    addSourceEClass = createEClass(ADD_SOURCE);
    createEReference(addSourceEClass, ADD_SOURCE__FILE_NAME);
    createEReference(addSourceEClass, ADD_SOURCE__CODE);

    removeSourceEClass = createEClass(REMOVE_SOURCE);
    createEReference(removeSourceEClass, REMOVE_SOURCE__FILE_NAME);

    sourceOperationEClass = createEClass(SOURCE_OPERATION);

    addsEClass = createEClass(ADDS);
    createEReference(addsEClass, ADDS__FILES);

    removesEClass = createEClass(REMOVES);
    createEReference(removesEClass, REMOVES__FILES);

    modifiesEClass = createEClass(MODIFIES);
    createEReference(modifiesEClass, MODIFIES__FILE_NAME);
    createEReference(modifiesEClass, MODIFIES__ACTION);

    addDeclarationEClass = createEClass(ADD_DECLARATION);
    createEReference(addDeclarationEClass, ADD_DECLARATION__DECL);

    addStandardIncludeEClass = createEClass(ADD_STANDARD_INCLUDE);
    createEReference(addStandardIncludeEClass, ADD_STANDARD_INCLUDE__INCL);

    addFileIncludeEClass = createEClass(ADD_FILE_INCLUDE);
    createEReference(addFileIncludeEClass, ADD_FILE_INCLUDE__INCL);

    removeGlobalEClass = createEClass(REMOVE_GLOBAL);
    createEAttribute(removeGlobalEClass, REMOVE_GLOBAL__GLOBAL);

    removeStandardIncludeEClass = createEClass(REMOVE_STANDARD_INCLUDE);
    createEReference(removeStandardIncludeEClass, REMOVE_STANDARD_INCLUDE__INCL);

    removeFileIncludeEClass = createEClass(REMOVE_FILE_INCLUDE);
    createEReference(removeFileIncludeEClass, REMOVE_FILE_INCLUDE__INCL);

    modifyFunctionEClass = createEClass(MODIFY_FUNCTION);
    createEReference(modifyFunctionEClass, MODIFY_FUNCTION__DECL);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isInitialized = false;

  /**
   * Complete the initialization of the package and its meta-model.  This
   * method is guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void initializePackageContents()
  {
    if (isInitialized) return;
    isInitialized = true;

    // Initialize package
    setName(eNAME);
    setNsPrefix(eNS_PREFIX);
    setNsURI(eNS_URI);

    // Obtain other dependent packages
    ClangPackage theClangPackage = (ClangPackage)EPackage.Registry.INSTANCE.getEPackage(ClangPackage.eNS_URI);

    // Create type parameters

    // Set bounds for type parameters

    // Add supertypes to classes
    addsEClass.getESuperTypes().add(this.getFileOperation());
    removesEClass.getESuperTypes().add(this.getFileOperation());
    modifiesEClass.getESuperTypes().add(this.getFileOperation());
    addDeclarationEClass.getESuperTypes().add(this.getSourceOperation());
    addStandardIncludeEClass.getESuperTypes().add(this.getSourceOperation());
    addFileIncludeEClass.getESuperTypes().add(this.getSourceOperation());
    removeGlobalEClass.getESuperTypes().add(this.getSourceOperation());
    removeStandardIncludeEClass.getESuperTypes().add(this.getSourceOperation());
    removeFileIncludeEClass.getESuperTypes().add(this.getSourceOperation());
    modifyFunctionEClass.getESuperTypes().add(this.getSourceOperation());

    // Initialize classes and features; add operations and parameters
    initEClass(deltaModuleEClass, DeltaModule.class, "DeltaModule", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getDeltaModule_Name(), ecorePackage.getEString(), "name", null, 0, 1, DeltaModule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getDeltaModule_Body(), this.getFileOperation(), null, "body", null, 0, -1, DeltaModule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(fileOperationEClass, FileOperation.class, "FileOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(addSourceEClass, AddSource.class, "AddSource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getAddSource_FileName(), theClangPackage.getSource(), null, "fileName", null, 0, 1, AddSource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getAddSource_Code(), theClangPackage.getModel(), null, "code", null, 0, 1, AddSource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(removeSourceEClass, RemoveSource.class, "RemoveSource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getRemoveSource_FileName(), theClangPackage.getSource(), null, "fileName", null, 0, 1, RemoveSource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(sourceOperationEClass, SourceOperation.class, "SourceOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(addsEClass, Adds.class, "Adds", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getAdds_Files(), this.getAddSource(), null, "files", null, 0, -1, Adds.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(removesEClass, Removes.class, "Removes", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getRemoves_Files(), this.getRemoveSource(), null, "files", null, 0, -1, Removes.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(modifiesEClass, Modifies.class, "Modifies", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getModifies_FileName(), theClangPackage.getSource(), null, "fileName", null, 0, 1, Modifies.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getModifies_Action(), this.getSourceOperation(), null, "action", null, 0, -1, Modifies.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(addDeclarationEClass, AddDeclaration.class, "AddDeclaration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getAddDeclaration_Decl(), theClangPackage.getDeclaration(), null, "decl", null, 0, 1, AddDeclaration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(addStandardIncludeEClass, AddStandardInclude.class, "AddStandardInclude", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getAddStandardInclude_Incl(), theClangPackage.getStandardInclude(), null, "incl", null, 0, 1, AddStandardInclude.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(addFileIncludeEClass, AddFileInclude.class, "AddFileInclude", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getAddFileInclude_Incl(), theClangPackage.getFileInclude(), null, "incl", null, 0, 1, AddFileInclude.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(removeGlobalEClass, RemoveGlobal.class, "RemoveGlobal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getRemoveGlobal_Global(), ecorePackage.getEString(), "global", null, 0, 1, RemoveGlobal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(removeStandardIncludeEClass, RemoveStandardInclude.class, "RemoveStandardInclude", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getRemoveStandardInclude_Incl(), theClangPackage.getStandardInclude(), null, "incl", null, 0, 1, RemoveStandardInclude.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(removeFileIncludeEClass, RemoveFileInclude.class, "RemoveFileInclude", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getRemoveFileInclude_Incl(), theClangPackage.getFileInclude(), null, "incl", null, 0, 1, RemoveFileInclude.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(modifyFunctionEClass, ModifyFunction.class, "ModifyFunction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getModifyFunction_Decl(), theClangPackage.getDeclaration(), null, "decl", null, 0, 1, ModifyFunction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    // Create resource
    createResource(eNS_URI);
  }

} //CodeBasePackageImpl
