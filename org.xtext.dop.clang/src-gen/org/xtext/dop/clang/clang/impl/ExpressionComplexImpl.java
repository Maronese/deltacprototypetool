/**
 */
package org.xtext.dop.clang.clang.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.xtext.dop.clang.clang.ClangPackage;
import org.xtext.dop.clang.clang.Expression;
import org.xtext.dop.clang.clang.ExpressionComplex;
import org.xtext.dop.clang.clang.PostfixExpressionPostfix;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Expression Complex</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.clang.clang.impl.ExpressionComplexImpl#getExps <em>Exps</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.impl.ExpressionComplexImpl#getNext <em>Next</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ExpressionComplexImpl extends ExpressionImpl implements ExpressionComplex
{
  /**
   * The cached value of the '{@link #getExps() <em>Exps</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExps()
   * @generated
   * @ordered
   */
  protected EList<Expression> exps;

  /**
   * The cached value of the '{@link #getNext() <em>Next</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNext()
   * @generated
   * @ordered
   */
  protected EList<PostfixExpressionPostfix> next;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ExpressionComplexImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ClangPackage.Literals.EXPRESSION_COMPLEX;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Expression> getExps()
  {
    if (exps == null)
    {
      exps = new EObjectContainmentEList<Expression>(Expression.class, this, ClangPackage.EXPRESSION_COMPLEX__EXPS);
    }
    return exps;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<PostfixExpressionPostfix> getNext()
  {
    if (next == null)
    {
      next = new EObjectContainmentEList<PostfixExpressionPostfix>(PostfixExpressionPostfix.class, this, ClangPackage.EXPRESSION_COMPLEX__NEXT);
    }
    return next;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case ClangPackage.EXPRESSION_COMPLEX__EXPS:
        return ((InternalEList<?>)getExps()).basicRemove(otherEnd, msgs);
      case ClangPackage.EXPRESSION_COMPLEX__NEXT:
        return ((InternalEList<?>)getNext()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case ClangPackage.EXPRESSION_COMPLEX__EXPS:
        return getExps();
      case ClangPackage.EXPRESSION_COMPLEX__NEXT:
        return getNext();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case ClangPackage.EXPRESSION_COMPLEX__EXPS:
        getExps().clear();
        getExps().addAll((Collection<? extends Expression>)newValue);
        return;
      case ClangPackage.EXPRESSION_COMPLEX__NEXT:
        getNext().clear();
        getNext().addAll((Collection<? extends PostfixExpressionPostfix>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case ClangPackage.EXPRESSION_COMPLEX__EXPS:
        getExps().clear();
        return;
      case ClangPackage.EXPRESSION_COMPLEX__NEXT:
        getNext().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case ClangPackage.EXPRESSION_COMPLEX__EXPS:
        return exps != null && !exps.isEmpty();
      case ClangPackage.EXPRESSION_COMPLEX__NEXT:
        return next != null && !next.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //ExpressionComplexImpl
