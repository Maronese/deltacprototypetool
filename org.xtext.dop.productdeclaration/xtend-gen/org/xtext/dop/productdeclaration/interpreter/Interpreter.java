package org.xtext.dop.productdeclaration.interpreter;

import java.util.LinkedList;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.xtext.dop.codebase.generator.Main;
import org.xtext.dop.productdeclaration.interpreter.Panel;
import org.xtext.dop.productdeclaration.productDeclaration.And;
import org.xtext.dop.productdeclaration.productDeclaration.Constraint;
import org.xtext.dop.productdeclaration.productDeclaration.Element;
import org.xtext.dop.productdeclaration.productDeclaration.Expression;
import org.xtext.dop.productdeclaration.productDeclaration.ListElem;
import org.xtext.dop.productdeclaration.productDeclaration.Not;
import org.xtext.dop.productdeclaration.productDeclaration.Or;
import org.xtext.dop.productdeclaration.productDeclaration.Partition;
import org.xtext.dop.productdeclaration.productDeclaration.Partitions;
import org.xtext.dop.productdeclaration.productDeclaration.Product;
import org.xtext.dop.productdeclaration.productDeclaration.ProductDeclaration;
import org.xtext.dop.productdeclaration.productDeclaration.Products;
import org.xtext.dop.productdeclaration.productDeclaration.Sub;

@SuppressWarnings("all")
public class Interpreter {
  private Product target;
  
  private ProductDeclaration decl;
  
  private List<String> deltas;
  
  public Interpreter(final ProductDeclaration decl) {
    this.decl = decl;
    LinkedList<String> _linkedList = new LinkedList<String>();
    this.deltas = _linkedList;
  }
  
  public List<String> interpret(final Product target) {
    List<String> _xblockexpression = null;
    {
      this.setTarget(target);
      Partitions _partitions = this.decl.getPartitions();
      EList<Partition> _list = _partitions.getList();
      for (final Partition part : _list) {
        EList<Constraint> _constr = part.getConstr();
        for (final Constraint constr : _constr) {
          Expression _expr = constr.getExpr();
          boolean _value = this.value(_expr);
          if (_value) {
            Element _delta = constr.getDelta();
            String _name = _delta.getName();
            this.deltas.add(_name);
          }
        }
      }
      _xblockexpression = this.deltas;
    }
    return _xblockexpression;
  }
  
  private boolean value(final Expression expr) {
    try {
      boolean _switchResult = false;
      boolean _matched = false;
      if (!_matched) {
        if (expr instanceof Or) {
          _matched=true;
          boolean _or = false;
          Expression _left = ((Or) expr).getLeft();
          boolean _value = this.value(_left);
          if (_value) {
            _or = true;
          } else {
            Expression _right = ((Or) expr).getRight();
            boolean _value_1 = this.value(_right);
            _or = _value_1;
          }
          _switchResult = _or;
        }
      }
      if (!_matched) {
        if (expr instanceof And) {
          _matched=true;
          boolean _and = false;
          Expression _left = ((And) expr).getLeft();
          boolean _value = this.value(_left);
          if (!_value) {
            _and = false;
          } else {
            Expression _right = ((And) expr).getRight();
            boolean _value_1 = this.value(_right);
            _and = _value_1;
          }
          _switchResult = _and;
        }
      }
      if (!_matched) {
        if (expr instanceof Not) {
          _matched=true;
          Expression _value = ((Not) expr).getValue();
          boolean _value_1 = this.value(_value);
          _switchResult = (!_value_1);
        }
      }
      if (!_matched) {
        if (expr instanceof Element) {
          _matched=true;
          EList<ListElem> _list = this.target.getList();
          final Function1<ListElem, String> _function = (ListElem it) -> {
            Element _elem = it.getElem();
            return _elem.getName();
          };
          List<String> _map = ListExtensions.<ListElem, String>map(_list, _function);
          String _name = ((Element) expr).getName();
          _switchResult = _map.contains(_name);
        }
      }
      if (!_matched) {
        if (expr instanceof Sub) {
          _matched=true;
          Expression _expr = ((Sub) expr).getExpr();
          _switchResult = this.value(_expr);
        }
      }
      if (!_matched) {
        throw new Error();
      }
      return _switchResult;
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  public Panel guiSelection(final String name, final Main codebase) {
    Products _products = this.decl.getProducts();
    EList<Product> _list = _products.getList();
    return new Panel(name, _list, this, codebase);
  }
  
  public Product setTarget(final Product target) {
    return this.target = target;
  }
}
