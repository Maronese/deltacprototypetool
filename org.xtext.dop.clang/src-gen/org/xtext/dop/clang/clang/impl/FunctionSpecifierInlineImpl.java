/**
 */
package org.xtext.dop.clang.clang.impl;

import org.eclipse.emf.ecore.EClass;

import org.xtext.dop.clang.clang.ClangPackage;
import org.xtext.dop.clang.clang.FunctionSpecifierInline;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Function Specifier Inline</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class FunctionSpecifierInlineImpl extends FunctionSpecifierImpl implements FunctionSpecifierInline
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected FunctionSpecifierInlineImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ClangPackage.Literals.FUNCTION_SPECIFIER_INLINE;
  }

} //FunctionSpecifierInlineImpl
