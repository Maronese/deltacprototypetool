/**
 */
package org.xtext.dop.clang.clang.impl;

import org.eclipse.emf.ecore.EClass;

import org.xtext.dop.clang.clang.ClangPackage;
import org.xtext.dop.clang.clang.StorageClassSpecifierRegister;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Storage Class Specifier Register</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class StorageClassSpecifierRegisterImpl extends StorageClassSpecifierImpl implements StorageClassSpecifierRegister
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected StorageClassSpecifierRegisterImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ClangPackage.Literals.STORAGE_CLASS_SPECIFIER_REGISTER;
  }

} //StorageClassSpecifierRegisterImpl
