/**
 */
package org.xtext.dop.clang.clang.impl;

import org.eclipse.emf.ecore.EClass;

import org.xtext.dop.clang.clang.BaseTypeShort;
import org.xtext.dop.clang.clang.ClangPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Base Type Short</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BaseTypeShortImpl extends BaseTypeImpl implements BaseTypeShort
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected BaseTypeShortImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ClangPackage.Literals.BASE_TYPE_SHORT;
  }

} //BaseTypeShortImpl
