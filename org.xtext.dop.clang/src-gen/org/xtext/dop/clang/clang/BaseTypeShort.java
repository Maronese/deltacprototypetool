/**
 */
package org.xtext.dop.clang.clang;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Base Type Short</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getBaseTypeShort()
 * @model
 * @generated
 */
public interface BaseTypeShort extends BaseType
{
} // BaseTypeShort
