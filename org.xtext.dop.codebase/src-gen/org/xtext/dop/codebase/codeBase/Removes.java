/**
 */
package org.xtext.dop.codebase.codeBase;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Removes</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.codebase.codeBase.Removes#getFiles <em>Files</em>}</li>
 * </ul>
 *
 * @see org.xtext.dop.codebase.codeBase.CodeBasePackage#getRemoves()
 * @model
 * @generated
 */
public interface Removes extends FileOperation
{
  /**
   * Returns the value of the '<em><b>Files</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.dop.codebase.codeBase.RemoveSource}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Files</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Files</em>' containment reference list.
   * @see org.xtext.dop.codebase.codeBase.CodeBasePackage#getRemoves_Files()
   * @model containment="true"
   * @generated
   */
  EList<RemoveSource> getFiles();

} // Removes
