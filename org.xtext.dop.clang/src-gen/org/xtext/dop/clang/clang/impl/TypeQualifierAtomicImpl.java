/**
 */
package org.xtext.dop.clang.clang.impl;

import org.eclipse.emf.ecore.EClass;

import org.xtext.dop.clang.clang.ClangPackage;
import org.xtext.dop.clang.clang.TypeQualifierAtomic;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Type Qualifier Atomic</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TypeQualifierAtomicImpl extends TypeQualifierImpl implements TypeQualifierAtomic
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TypeQualifierAtomicImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ClangPackage.Literals.TYPE_QUALIFIER_ATOMIC;
  }

} //TypeQualifierAtomicImpl
