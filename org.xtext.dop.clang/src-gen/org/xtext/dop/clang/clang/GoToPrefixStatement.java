/**
 */
package org.xtext.dop.clang.clang;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Go To Prefix Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.clang.clang.GoToPrefixStatement#getGoto <em>Goto</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.GoToPrefixStatement#getStatement <em>Statement</em>}</li>
 * </ul>
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getGoToPrefixStatement()
 * @model
 * @generated
 */
public interface GoToPrefixStatement extends Statement
{
  /**
   * Returns the value of the '<em><b>Goto</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Goto</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Goto</em>' attribute.
   * @see #setGoto(String)
   * @see org.xtext.dop.clang.clang.ClangPackage#getGoToPrefixStatement_Goto()
   * @model
   * @generated
   */
  String getGoto();

  /**
   * Sets the value of the '{@link org.xtext.dop.clang.clang.GoToPrefixStatement#getGoto <em>Goto</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Goto</em>' attribute.
   * @see #getGoto()
   * @generated
   */
  void setGoto(String value);

  /**
   * Returns the value of the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Statement</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Statement</em>' containment reference.
   * @see #setStatement(Statement)
   * @see org.xtext.dop.clang.clang.ClangPackage#getGoToPrefixStatement_Statement()
   * @model containment="true"
   * @generated
   */
  Statement getStatement();

  /**
   * Sets the value of the '{@link org.xtext.dop.clang.clang.GoToPrefixStatement#getStatement <em>Statement</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Statement</em>' containment reference.
   * @see #getStatement()
   * @generated
   */
  void setStatement(Statement value);

} // GoToPrefixStatement
