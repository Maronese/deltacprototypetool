/**
 */
package org.xtext.dop.clang.clang;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Generic Declaration2</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getGenericDeclaration2()
 * @model
 * @generated
 */
public interface GenericDeclaration2 extends GenericDeclarationId
{
} // GenericDeclaration2
