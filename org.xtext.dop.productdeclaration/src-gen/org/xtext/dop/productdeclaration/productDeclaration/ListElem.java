/**
 */
package org.xtext.dop.productdeclaration.productDeclaration;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>List Elem</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.productdeclaration.productDeclaration.ListElem#getElem <em>Elem</em>}</li>
 *   <li>{@link org.xtext.dop.productdeclaration.productDeclaration.ListElem#isLast <em>Last</em>}</li>
 * </ul>
 *
 * @see org.xtext.dop.productdeclaration.productDeclaration.ProductDeclarationPackage#getListElem()
 * @model
 * @generated
 */
public interface ListElem extends EObject
{
  /**
   * Returns the value of the '<em><b>Elem</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Elem</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Elem</em>' containment reference.
   * @see #setElem(Element)
   * @see org.xtext.dop.productdeclaration.productDeclaration.ProductDeclarationPackage#getListElem_Elem()
   * @model containment="true"
   * @generated
   */
  Element getElem();

  /**
   * Sets the value of the '{@link org.xtext.dop.productdeclaration.productDeclaration.ListElem#getElem <em>Elem</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Elem</em>' containment reference.
   * @see #getElem()
   * @generated
   */
  void setElem(Element value);

  /**
   * Returns the value of the '<em><b>Last</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Last</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Last</em>' attribute.
   * @see #setLast(boolean)
   * @see org.xtext.dop.productdeclaration.productDeclaration.ProductDeclarationPackage#getListElem_Last()
   * @model
   * @generated
   */
  boolean isLast();

  /**
   * Sets the value of the '{@link org.xtext.dop.productdeclaration.productDeclaration.ListElem#isLast <em>Last</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Last</em>' attribute.
   * @see #isLast()
   * @generated
   */
  void setLast(boolean value);

} // ListElem
