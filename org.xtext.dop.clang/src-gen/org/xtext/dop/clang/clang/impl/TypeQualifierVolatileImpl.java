/**
 */
package org.xtext.dop.clang.clang.impl;

import org.eclipse.emf.ecore.EClass;

import org.xtext.dop.clang.clang.ClangPackage;
import org.xtext.dop.clang.clang.TypeQualifierVolatile;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Type Qualifier Volatile</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TypeQualifierVolatileImpl extends TypeQualifierImpl implements TypeQualifierVolatile
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TypeQualifierVolatileImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ClangPackage.Literals.TYPE_QUALIFIER_VOLATILE;
  }

} //TypeQualifierVolatileImpl
