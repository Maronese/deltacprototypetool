/**
 */
package org.xtext.dop.clang.clang;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Generic Declaration Id Postfix</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.clang.clang.GenericDeclarationIdPostfix#getArray <em>Array</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.GenericDeclarationIdPostfix#getFunction <em>Function</em>}</li>
 * </ul>
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getGenericDeclarationIdPostfix()
 * @model
 * @generated
 */
public interface GenericDeclarationIdPostfix extends EObject
{
  /**
   * Returns the value of the '<em><b>Array</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Array</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Array</em>' containment reference.
   * @see #setArray(ArrayPostfix)
   * @see org.xtext.dop.clang.clang.ClangPackage#getGenericDeclarationIdPostfix_Array()
   * @model containment="true"
   * @generated
   */
  ArrayPostfix getArray();

  /**
   * Sets the value of the '{@link org.xtext.dop.clang.clang.GenericDeclarationIdPostfix#getArray <em>Array</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Array</em>' containment reference.
   * @see #getArray()
   * @generated
   */
  void setArray(ArrayPostfix value);

  /**
   * Returns the value of the '<em><b>Function</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Function</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Function</em>' containment reference.
   * @see #setFunction(ParametersPostfix)
   * @see org.xtext.dop.clang.clang.ClangPackage#getGenericDeclarationIdPostfix_Function()
   * @model containment="true"
   * @generated
   */
  ParametersPostfix getFunction();

  /**
   * Sets the value of the '{@link org.xtext.dop.clang.clang.GenericDeclarationIdPostfix#getFunction <em>Function</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Function</em>' containment reference.
   * @see #getFunction()
   * @generated
   */
  void setFunction(ParametersPostfix value);

} // GenericDeclarationIdPostfix
