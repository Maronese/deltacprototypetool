/**
 */
package org.xtext.dop.clang.clang;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.xtext.dop.clang.clang.ClangFactory
 * @model kind="package"
 * @generated
 */
public interface ClangPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "clang";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.xtext.org/dop/clang/Clang";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "clang";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  ClangPackage eINSTANCE = org.xtext.dop.clang.clang.impl.ClangPackageImpl.init();

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.ModelImpl <em>Model</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.ModelImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getModel()
   * @generated
   */
  int MODEL = 0;

  /**
   * The feature id for the '<em><b>Includes Std</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODEL__INCLUDES_STD = 0;

  /**
   * The feature id for the '<em><b>Includes File</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODEL__INCLUDES_FILE = 1;

  /**
   * The feature id for the '<em><b>Declarations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODEL__DECLARATIONS = 2;

  /**
   * The number of structural features of the '<em>Model</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODEL_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.StandardIncludeImpl <em>Standard Include</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.StandardIncludeImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getStandardInclude()
   * @generated
   */
  int STANDARD_INCLUDE = 1;

  /**
   * The feature id for the '<em><b>Std</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STANDARD_INCLUDE__STD = 0;

  /**
   * The feature id for the '<em><b>Wild</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STANDARD_INCLUDE__WILD = 1;

  /**
   * The number of structural features of the '<em>Standard Include</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STANDARD_INCLUDE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.FileIncludeImpl <em>File Include</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.FileIncludeImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getFileInclude()
   * @generated
   */
  int FILE_INCLUDE = 2;

  /**
   * The feature id for the '<em><b>File</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FILE_INCLUDE__FILE = 0;

  /**
   * The number of structural features of the '<em>File Include</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FILE_INCLUDE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.SourceImpl <em>Source</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.SourceImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getSource()
   * @generated
   */
  int SOURCE = 3;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOURCE__NAME = 0;

  /**
   * The number of structural features of the '<em>Source</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOURCE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.DeclarationImpl <em>Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.DeclarationImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getDeclaration()
   * @generated
   */
  int DECLARATION = 4;

  /**
   * The number of structural features of the '<em>Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DECLARATION_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.TypeAliasDeclarationImpl <em>Type Alias Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.TypeAliasDeclarationImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getTypeAliasDeclaration()
   * @generated
   */
  int TYPE_ALIAS_DECLARATION = 5;

  /**
   * The feature id for the '<em><b>Tdecl</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_ALIAS_DECLARATION__TDECL = DECLARATION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Type Alias Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_ALIAS_DECLARATION_FEATURE_COUNT = DECLARATION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.GenericSpecifiedDeclarationImpl <em>Generic Specified Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.GenericSpecifiedDeclarationImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getGenericSpecifiedDeclaration()
   * @generated
   */
  int GENERIC_SPECIFIED_DECLARATION = 6;

  /**
   * The feature id for the '<em><b>Specs</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_SPECIFIED_DECLARATION__SPECS = DECLARATION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_SPECIFIED_DECLARATION__TYPE = DECLARATION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Decl</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_SPECIFIED_DECLARATION__DECL = DECLARATION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Generic Specified Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_SPECIFIED_DECLARATION_FEATURE_COUNT = DECLARATION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.GenericDeclarationElementsImpl <em>Generic Declaration Elements</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.GenericDeclarationElementsImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getGenericDeclarationElements()
   * @generated
   */
  int GENERIC_DECLARATION_ELEMENTS = 7;

  /**
   * The feature id for the '<em><b>Els</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_DECLARATION_ELEMENTS__ELS = 0;

  /**
   * The feature id for the '<em><b>Next</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_DECLARATION_ELEMENTS__NEXT = 1;

  /**
   * The number of structural features of the '<em>Generic Declaration Elements</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_DECLARATION_ELEMENTS_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.GenericDeclarationElementImpl <em>Generic Declaration Element</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.GenericDeclarationElementImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getGenericDeclarationElement()
   * @generated
   */
  int GENERIC_DECLARATION_ELEMENT = 8;

  /**
   * The feature id for the '<em><b>Id</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_DECLARATION_ELEMENT__ID = 0;

  /**
   * The feature id for the '<em><b>Init</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_DECLARATION_ELEMENT__INIT = 1;

  /**
   * The number of structural features of the '<em>Generic Declaration Element</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_DECLARATION_ELEMENT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.GenericDeclarationIdImpl <em>Generic Declaration Id</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.GenericDeclarationIdImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getGenericDeclarationId()
   * @generated
   */
  int GENERIC_DECLARATION_ID = 9;

  /**
   * The feature id for the '<em><b>Inner</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_DECLARATION_ID__INNER = 0;

  /**
   * The feature id for the '<em><b>Posts</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_DECLARATION_ID__POSTS = 1;

  /**
   * The number of structural features of the '<em>Generic Declaration Id</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_DECLARATION_ID_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.GenericDeclarationInnerIdImpl <em>Generic Declaration Inner Id</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.GenericDeclarationInnerIdImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getGenericDeclarationInnerId()
   * @generated
   */
  int GENERIC_DECLARATION_INNER_ID = 10;

  /**
   * The feature id for the '<em><b>Inner</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_DECLARATION_INNER_ID__INNER = 0;

  /**
   * The number of structural features of the '<em>Generic Declaration Inner Id</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_DECLARATION_INNER_ID_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.GenericDeclarationIdPostfixImpl <em>Generic Declaration Id Postfix</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.GenericDeclarationIdPostfixImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getGenericDeclarationIdPostfix()
   * @generated
   */
  int GENERIC_DECLARATION_ID_POSTFIX = 11;

  /**
   * The feature id for the '<em><b>Array</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_DECLARATION_ID_POSTFIX__ARRAY = 0;

  /**
   * The feature id for the '<em><b>Function</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_DECLARATION_ID_POSTFIX__FUNCTION = 1;

  /**
   * The number of structural features of the '<em>Generic Declaration Id Postfix</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_DECLARATION_ID_POSTFIX_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.ArrayPostfixImpl <em>Array Postfix</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.ArrayPostfixImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getArrayPostfix()
   * @generated
   */
  int ARRAY_POSTFIX = 12;

  /**
   * The feature id for the '<em><b>Star</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_POSTFIX__STAR = 0;

  /**
   * The feature id for the '<em><b>Exp</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_POSTFIX__EXP = 1;

  /**
   * The number of structural features of the '<em>Array Postfix</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_POSTFIX_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.ParametersPostfixImpl <em>Parameters Postfix</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.ParametersPostfixImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getParametersPostfix()
   * @generated
   */
  int PARAMETERS_POSTFIX = 13;

  /**
   * The feature id for the '<em><b>Els</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETERS_POSTFIX__ELS = 0;

  /**
   * The feature id for the '<em><b>Next</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETERS_POSTFIX__NEXT = 1;

  /**
   * The feature id for the '<em><b>More</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETERS_POSTFIX__MORE = 2;

  /**
   * The number of structural features of the '<em>Parameters Postfix</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETERS_POSTFIX_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.NextParameterImpl <em>Next Parameter</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.NextParameterImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getNextParameter()
   * @generated
   */
  int NEXT_PARAMETER = 14;

  /**
   * The feature id for the '<em><b>Els</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NEXT_PARAMETER__ELS = 0;

  /**
   * The number of structural features of the '<em>Next Parameter</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NEXT_PARAMETER_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.ParameterImpl <em>Parameter</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.ParameterImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getParameter()
   * @generated
   */
  int PARAMETER = 15;

  /**
   * The feature id for the '<em><b>Specs</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETER__SPECS = 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETER__TYPE = 1;

  /**
   * The feature id for the '<em><b>Id</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETER__ID = 2;

  /**
   * The number of structural features of the '<em>Parameter</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETER_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.GenericDeclarationInitializerImpl <em>Generic Declaration Initializer</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.GenericDeclarationInitializerImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getGenericDeclarationInitializer()
   * @generated
   */
  int GENERIC_DECLARATION_INITIALIZER = 16;

  /**
   * The number of structural features of the '<em>Generic Declaration Initializer</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_DECLARATION_INITIALIZER_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.SpecifiedTypeImpl <em>Specified Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.SpecifiedTypeImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getSpecifiedType()
   * @generated
   */
  int SPECIFIED_TYPE = 17;

  /**
   * The feature id for the '<em><b>Par</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SPECIFIED_TYPE__PAR = 0;

  /**
   * The number of structural features of the '<em>Specified Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SPECIFIED_TYPE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.StaticAssertDeclarationImpl <em>Static Assert Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.StaticAssertDeclarationImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getStaticAssertDeclaration()
   * @generated
   */
  int STATIC_ASSERT_DECLARATION = 18;

  /**
   * The feature id for the '<em><b>Exp</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATIC_ASSERT_DECLARATION__EXP = DECLARATION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>String</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATIC_ASSERT_DECLARATION__STRING = DECLARATION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Static Assert Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATIC_ASSERT_DECLARATION_FEATURE_COUNT = DECLARATION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.SimpleTypeImpl <em>Simple Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.SimpleTypeImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getSimpleType()
   * @generated
   */
  int SIMPLE_TYPE = 19;

  /**
   * The number of structural features of the '<em>Simple Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMPLE_TYPE_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.BaseTypeImpl <em>Base Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.BaseTypeImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getBaseType()
   * @generated
   */
  int BASE_TYPE = 20;

  /**
   * The number of structural features of the '<em>Base Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASE_TYPE_FEATURE_COUNT = SIMPLE_TYPE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.StructOrUnionTypeImpl <em>Struct Or Union Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.StructOrUnionTypeImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getStructOrUnionType()
   * @generated
   */
  int STRUCT_OR_UNION_TYPE = 21;

  /**
   * The feature id for the '<em><b>Kind</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRUCT_OR_UNION_TYPE__KIND = SIMPLE_TYPE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Content</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRUCT_OR_UNION_TYPE__CONTENT = SIMPLE_TYPE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRUCT_OR_UNION_TYPE__NAME = SIMPLE_TYPE_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Struct Or Union Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRUCT_OR_UNION_TYPE_FEATURE_COUNT = SIMPLE_TYPE_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.EnumTypeImpl <em>Enum Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.EnumTypeImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getEnumType()
   * @generated
   */
  int ENUM_TYPE = 22;

  /**
   * The feature id for the '<em><b>Content</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENUM_TYPE__CONTENT = SIMPLE_TYPE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENUM_TYPE__NAME = SIMPLE_TYPE_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Enum Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENUM_TYPE_FEATURE_COUNT = SIMPLE_TYPE_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.EnumeratorListImpl <em>Enumerator List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.EnumeratorListImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getEnumeratorList()
   * @generated
   */
  int ENUMERATOR_LIST = 23;

  /**
   * The feature id for the '<em><b>Elements</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENUMERATOR_LIST__ELEMENTS = 0;

  /**
   * The number of structural features of the '<em>Enumerator List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENUMERATOR_LIST_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.EnumeratorImpl <em>Enumerator</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.EnumeratorImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getEnumerator()
   * @generated
   */
  int ENUMERATOR = 24;

  /**
   * The feature id for the '<em><b>Name</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENUMERATOR__NAME = 0;

  /**
   * The feature id for the '<em><b>Exp</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENUMERATOR__EXP = 1;

  /**
   * The number of structural features of the '<em>Enumerator</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENUMERATOR_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.EnumerationConstantImpl <em>Enumeration Constant</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.EnumerationConstantImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getEnumerationConstant()
   * @generated
   */
  int ENUMERATION_CONSTANT = 25;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENUMERATION_CONSTANT__NAME = 0;

  /**
   * The number of structural features of the '<em>Enumeration Constant</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENUMERATION_CONSTANT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.DeclarationSpecifierImpl <em>Declaration Specifier</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.DeclarationSpecifierImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getDeclarationSpecifier()
   * @generated
   */
  int DECLARATION_SPECIFIER = 26;

  /**
   * The number of structural features of the '<em>Declaration Specifier</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DECLARATION_SPECIFIER_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.StorageClassSpecifierImpl <em>Storage Class Specifier</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.StorageClassSpecifierImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getStorageClassSpecifier()
   * @generated
   */
  int STORAGE_CLASS_SPECIFIER = 27;

  /**
   * The number of structural features of the '<em>Storage Class Specifier</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STORAGE_CLASS_SPECIFIER_FEATURE_COUNT = DECLARATION_SPECIFIER_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.TypeQualifierImpl <em>Type Qualifier</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.TypeQualifierImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getTypeQualifier()
   * @generated
   */
  int TYPE_QUALIFIER = 28;

  /**
   * The number of structural features of the '<em>Type Qualifier</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_QUALIFIER_FEATURE_COUNT = DECLARATION_SPECIFIER_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.FunctionSpecifierImpl <em>Function Specifier</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.FunctionSpecifierImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getFunctionSpecifier()
   * @generated
   */
  int FUNCTION_SPECIFIER = 29;

  /**
   * The number of structural features of the '<em>Function Specifier</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_SPECIFIER_FEATURE_COUNT = DECLARATION_SPECIFIER_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.AlignmentSpecifierImpl <em>Alignment Specifier</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.AlignmentSpecifierImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getAlignmentSpecifier()
   * @generated
   */
  int ALIGNMENT_SPECIFIER = 30;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALIGNMENT_SPECIFIER__TYPE = DECLARATION_SPECIFIER_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Exp</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALIGNMENT_SPECIFIER__EXP = DECLARATION_SPECIFIER_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Alignment Specifier</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALIGNMENT_SPECIFIER_FEATURE_COUNT = DECLARATION_SPECIFIER_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.ExpressionImpl <em>Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.ExpressionImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getExpression()
   * @generated
   */
  int EXPRESSION = 31;

  /**
   * The number of structural features of the '<em>Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.AssignmentExpressionElementImpl <em>Assignment Expression Element</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.AssignmentExpressionElementImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getAssignmentExpressionElement()
   * @generated
   */
  int ASSIGNMENT_EXPRESSION_ELEMENT = 32;

  /**
   * The feature id for the '<em><b>Op</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASSIGNMENT_EXPRESSION_ELEMENT__OP = 0;

  /**
   * The feature id for the '<em><b>Exp</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASSIGNMENT_EXPRESSION_ELEMENT__EXP = 1;

  /**
   * The number of structural features of the '<em>Assignment Expression Element</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASSIGNMENT_EXPRESSION_ELEMENT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.StringExpressionImpl <em>String Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.StringExpressionImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getStringExpression()
   * @generated
   */
  int STRING_EXPRESSION = 33;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRING_EXPRESSION__NAME = 0;

  /**
   * The number of structural features of the '<em>String Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRING_EXPRESSION_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.GenericAssociationImpl <em>Generic Association</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.GenericAssociationImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getGenericAssociation()
   * @generated
   */
  int GENERIC_ASSOCIATION = 34;

  /**
   * The feature id for the '<em><b>Exp</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_ASSOCIATION__EXP = 0;

  /**
   * The number of structural features of the '<em>Generic Association</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_ASSOCIATION_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.PostfixExpressionPostfixImpl <em>Postfix Expression Postfix</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.PostfixExpressionPostfixImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getPostfixExpressionPostfix()
   * @generated
   */
  int POSTFIX_EXPRESSION_POSTFIX = 35;

  /**
   * The number of structural features of the '<em>Postfix Expression Postfix</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int POSTFIX_EXPRESSION_POSTFIX_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.NextAsignementImpl <em>Next Asignement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.NextAsignementImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getNextAsignement()
   * @generated
   */
  int NEXT_ASIGNEMENT = 36;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NEXT_ASIGNEMENT__RIGHT = 0;

  /**
   * The number of structural features of the '<em>Next Asignement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NEXT_ASIGNEMENT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.StatementImpl <em>Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.StatementImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getStatement()
   * @generated
   */
  int STATEMENT = 41;

  /**
   * The number of structural features of the '<em>Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATEMENT_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.StatementsImpl <em>Statements</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.StatementsImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getStatements()
   * @generated
   */
  int STATEMENTS = 39;

  /**
   * The number of structural features of the '<em>Statements</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATEMENTS_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.CompoundStatementImpl <em>Compound Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.CompoundStatementImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getCompoundStatement()
   * @generated
   */
  int COMPOUND_STATEMENT = 37;

  /**
   * The feature id for the '<em><b>Inner</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPOUND_STATEMENT__INNER = STATEMENTS_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Compound Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPOUND_STATEMENT_FEATURE_COUNT = STATEMENTS_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.BlockItemImpl <em>Block Item</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.BlockItemImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getBlockItem()
   * @generated
   */
  int BLOCK_ITEM = 38;

  /**
   * The number of structural features of the '<em>Block Item</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BLOCK_ITEM_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.LabeledStatementImpl <em>Labeled Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.LabeledStatementImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getLabeledStatement()
   * @generated
   */
  int LABELED_STATEMENT = 40;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LABELED_STATEMENT__STATEMENT = STATEMENTS_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Labeled Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LABELED_STATEMENT_FEATURE_COUNT = STATEMENTS_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.ExpressionStatementImpl <em>Expression Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.ExpressionStatementImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getExpressionStatement()
   * @generated
   */
  int EXPRESSION_STATEMENT = 42;

  /**
   * The feature id for the '<em><b>Exp</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_STATEMENT__EXP = STATEMENTS_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Expression Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_STATEMENT_FEATURE_COUNT = STATEMENTS_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.SelectionStatementImpl <em>Selection Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.SelectionStatementImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getSelectionStatement()
   * @generated
   */
  int SELECTION_STATEMENT = 43;

  /**
   * The feature id for the '<em><b>Exp</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECTION_STATEMENT__EXP = STATEMENTS_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Selection Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECTION_STATEMENT_FEATURE_COUNT = STATEMENTS_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.IterationStatementImpl <em>Iteration Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.IterationStatementImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getIterationStatement()
   * @generated
   */
  int ITERATION_STATEMENT = 44;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ITERATION_STATEMENT__STATEMENT = STATEMENTS_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Iteration Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ITERATION_STATEMENT_FEATURE_COUNT = STATEMENTS_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.NextFieldImpl <em>Next Field</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.NextFieldImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getNextField()
   * @generated
   */
  int NEXT_FIELD = 45;

  /**
   * The feature id for the '<em><b>Next</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NEXT_FIELD__NEXT = 0;

  /**
   * The number of structural features of the '<em>Next Field</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NEXT_FIELD_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.JumpStatementImpl <em>Jump Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.JumpStatementImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getJumpStatement()
   * @generated
   */
  int JUMP_STATEMENT = 46;

  /**
   * The number of structural features of the '<em>Jump Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JUMP_STATEMENT_FEATURE_COUNT = STATEMENTS_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.CompleteNameImpl <em>Complete Name</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.CompleteNameImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getCompleteName()
   * @generated
   */
  int COMPLETE_NAME = 47;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPLETE_NAME__NAME = SOURCE__NAME;

  /**
   * The feature id for the '<em><b>Ext</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPLETE_NAME__EXT = SOURCE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Complete Name</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPLETE_NAME_FEATURE_COUNT = SOURCE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.SimpleNameImpl <em>Simple Name</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.SimpleNameImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getSimpleName()
   * @generated
   */
  int SIMPLE_NAME = 48;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMPLE_NAME__NAME = SOURCE__NAME;

  /**
   * The number of structural features of the '<em>Simple Name</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMPLE_NAME_FEATURE_COUNT = SOURCE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.GenericDeclaration0Impl <em>Generic Declaration0</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.GenericDeclaration0Impl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getGenericDeclaration0()
   * @generated
   */
  int GENERIC_DECLARATION0 = 49;

  /**
   * The feature id for the '<em><b>Inner</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_DECLARATION0__INNER = GENERIC_DECLARATION_ID__INNER;

  /**
   * The feature id for the '<em><b>Posts</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_DECLARATION0__POSTS = GENERIC_DECLARATION_ID__POSTS;

  /**
   * The feature id for the '<em><b>Pointers</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_DECLARATION0__POINTERS = GENERIC_DECLARATION_ID_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Restrict</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_DECLARATION0__RESTRICT = GENERIC_DECLARATION_ID_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Generic Declaration0</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_DECLARATION0_FEATURE_COUNT = GENERIC_DECLARATION_ID_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.GenericDeclaration1Impl <em>Generic Declaration1</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.GenericDeclaration1Impl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getGenericDeclaration1()
   * @generated
   */
  int GENERIC_DECLARATION1 = 50;

  /**
   * The feature id for the '<em><b>Inner</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_DECLARATION1__INNER = GENERIC_DECLARATION_ID__INNER;

  /**
   * The feature id for the '<em><b>Posts</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_DECLARATION1__POSTS = GENERIC_DECLARATION_ID__POSTS;

  /**
   * The number of structural features of the '<em>Generic Declaration1</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_DECLARATION1_FEATURE_COUNT = GENERIC_DECLARATION_ID_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.GenericDeclaration2Impl <em>Generic Declaration2</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.GenericDeclaration2Impl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getGenericDeclaration2()
   * @generated
   */
  int GENERIC_DECLARATION2 = 51;

  /**
   * The feature id for the '<em><b>Inner</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_DECLARATION2__INNER = GENERIC_DECLARATION_ID__INNER;

  /**
   * The feature id for the '<em><b>Posts</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_DECLARATION2__POSTS = GENERIC_DECLARATION_ID__POSTS;

  /**
   * The number of structural features of the '<em>Generic Declaration2</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_DECLARATION2_FEATURE_COUNT = GENERIC_DECLARATION_ID_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.GlobalNameImpl <em>Global Name</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.GlobalNameImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getGlobalName()
   * @generated
   */
  int GLOBAL_NAME = 52;

  /**
   * The feature id for the '<em><b>Inner</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GLOBAL_NAME__INNER = GENERIC_DECLARATION_INNER_ID__INNER;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GLOBAL_NAME__NAME = GENERIC_DECLARATION_INNER_ID_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Global Name</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GLOBAL_NAME_FEATURE_COUNT = GENERIC_DECLARATION_INNER_ID_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.ExpressionInitializerImpl <em>Expression Initializer</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.ExpressionInitializerImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getExpressionInitializer()
   * @generated
   */
  int EXPRESSION_INITIALIZER = 53;

  /**
   * The feature id for the '<em><b>Exp</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_INITIALIZER__EXP = GENERIC_DECLARATION_INITIALIZER_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Expression Initializer</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_INITIALIZER_FEATURE_COUNT = GENERIC_DECLARATION_INITIALIZER_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.FunctionInitializerImpl <em>Function Initializer</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.FunctionInitializerImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getFunctionInitializer()
   * @generated
   */
  int FUNCTION_INITIALIZER = 54;

  /**
   * The feature id for the '<em><b>Block</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_INITIALIZER__BLOCK = GENERIC_DECLARATION_INITIALIZER_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Function Initializer</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_INITIALIZER_FEATURE_COUNT = GENERIC_DECLARATION_INITIALIZER_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.AtomicImpl <em>Atomic</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.AtomicImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getAtomic()
   * @generated
   */
  int ATOMIC = 55;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ATOMIC__TYPE = SIMPLE_TYPE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Atomic</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ATOMIC_FEATURE_COUNT = SIMPLE_TYPE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.BaseTypeVoidImpl <em>Base Type Void</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.BaseTypeVoidImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getBaseTypeVoid()
   * @generated
   */
  int BASE_TYPE_VOID = 56;

  /**
   * The number of structural features of the '<em>Base Type Void</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASE_TYPE_VOID_FEATURE_COUNT = BASE_TYPE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.CustomImpl <em>Custom</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.CustomImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getCustom()
   * @generated
   */
  int CUSTOM = 57;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CUSTOM__ID = BASE_TYPE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Custom</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CUSTOM_FEATURE_COUNT = BASE_TYPE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.BaseTypeCharImpl <em>Base Type Char</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.BaseTypeCharImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getBaseTypeChar()
   * @generated
   */
  int BASE_TYPE_CHAR = 58;

  /**
   * The number of structural features of the '<em>Base Type Char</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASE_TYPE_CHAR_FEATURE_COUNT = BASE_TYPE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.BaseTypeShortImpl <em>Base Type Short</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.BaseTypeShortImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getBaseTypeShort()
   * @generated
   */
  int BASE_TYPE_SHORT = 59;

  /**
   * The number of structural features of the '<em>Base Type Short</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASE_TYPE_SHORT_FEATURE_COUNT = BASE_TYPE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.BaseTypeIntImpl <em>Base Type Int</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.BaseTypeIntImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getBaseTypeInt()
   * @generated
   */
  int BASE_TYPE_INT = 60;

  /**
   * The number of structural features of the '<em>Base Type Int</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASE_TYPE_INT_FEATURE_COUNT = BASE_TYPE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.BaseTypeLongImpl <em>Base Type Long</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.BaseTypeLongImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getBaseTypeLong()
   * @generated
   */
  int BASE_TYPE_LONG = 61;

  /**
   * The number of structural features of the '<em>Base Type Long</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASE_TYPE_LONG_FEATURE_COUNT = BASE_TYPE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.BaseTypeFloatImpl <em>Base Type Float</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.BaseTypeFloatImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getBaseTypeFloat()
   * @generated
   */
  int BASE_TYPE_FLOAT = 62;

  /**
   * The number of structural features of the '<em>Base Type Float</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASE_TYPE_FLOAT_FEATURE_COUNT = BASE_TYPE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.BaseTypeDoubleImpl <em>Base Type Double</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.BaseTypeDoubleImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getBaseTypeDouble()
   * @generated
   */
  int BASE_TYPE_DOUBLE = 63;

  /**
   * The number of structural features of the '<em>Base Type Double</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASE_TYPE_DOUBLE_FEATURE_COUNT = BASE_TYPE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.BaseTypeSignedImpl <em>Base Type Signed</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.BaseTypeSignedImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getBaseTypeSigned()
   * @generated
   */
  int BASE_TYPE_SIGNED = 64;

  /**
   * The number of structural features of the '<em>Base Type Signed</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASE_TYPE_SIGNED_FEATURE_COUNT = BASE_TYPE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.BaseTypeUnsignedImpl <em>Base Type Unsigned</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.BaseTypeUnsignedImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getBaseTypeUnsigned()
   * @generated
   */
  int BASE_TYPE_UNSIGNED = 65;

  /**
   * The number of structural features of the '<em>Base Type Unsigned</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASE_TYPE_UNSIGNED_FEATURE_COUNT = BASE_TYPE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.BaseTypeBoolImpl <em>Base Type Bool</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.BaseTypeBoolImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getBaseTypeBool()
   * @generated
   */
  int BASE_TYPE_BOOL = 66;

  /**
   * The number of structural features of the '<em>Base Type Bool</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASE_TYPE_BOOL_FEATURE_COUNT = BASE_TYPE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.BaseTypeComplexImpl <em>Base Type Complex</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.BaseTypeComplexImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getBaseTypeComplex()
   * @generated
   */
  int BASE_TYPE_COMPLEX = 67;

  /**
   * The number of structural features of the '<em>Base Type Complex</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASE_TYPE_COMPLEX_FEATURE_COUNT = BASE_TYPE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.BaseTypeImaginaryImpl <em>Base Type Imaginary</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.BaseTypeImaginaryImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getBaseTypeImaginary()
   * @generated
   */
  int BASE_TYPE_IMAGINARY = 68;

  /**
   * The number of structural features of the '<em>Base Type Imaginary</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASE_TYPE_IMAGINARY_FEATURE_COUNT = BASE_TYPE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.StorageClassSpecifierExternImpl <em>Storage Class Specifier Extern</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.StorageClassSpecifierExternImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getStorageClassSpecifierExtern()
   * @generated
   */
  int STORAGE_CLASS_SPECIFIER_EXTERN = 69;

  /**
   * The number of structural features of the '<em>Storage Class Specifier Extern</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STORAGE_CLASS_SPECIFIER_EXTERN_FEATURE_COUNT = STORAGE_CLASS_SPECIFIER_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.StorageClassSpecifierStaticImpl <em>Storage Class Specifier Static</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.StorageClassSpecifierStaticImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getStorageClassSpecifierStatic()
   * @generated
   */
  int STORAGE_CLASS_SPECIFIER_STATIC = 70;

  /**
   * The number of structural features of the '<em>Storage Class Specifier Static</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STORAGE_CLASS_SPECIFIER_STATIC_FEATURE_COUNT = STORAGE_CLASS_SPECIFIER_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.StorageClassSpecifierThreadLocalImpl <em>Storage Class Specifier Thread Local</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.StorageClassSpecifierThreadLocalImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getStorageClassSpecifierThreadLocal()
   * @generated
   */
  int STORAGE_CLASS_SPECIFIER_THREAD_LOCAL = 71;

  /**
   * The number of structural features of the '<em>Storage Class Specifier Thread Local</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STORAGE_CLASS_SPECIFIER_THREAD_LOCAL_FEATURE_COUNT = STORAGE_CLASS_SPECIFIER_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.StorageClassSpecifierAutoImpl <em>Storage Class Specifier Auto</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.StorageClassSpecifierAutoImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getStorageClassSpecifierAuto()
   * @generated
   */
  int STORAGE_CLASS_SPECIFIER_AUTO = 72;

  /**
   * The number of structural features of the '<em>Storage Class Specifier Auto</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STORAGE_CLASS_SPECIFIER_AUTO_FEATURE_COUNT = STORAGE_CLASS_SPECIFIER_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.StorageClassSpecifierRegisterImpl <em>Storage Class Specifier Register</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.StorageClassSpecifierRegisterImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getStorageClassSpecifierRegister()
   * @generated
   */
  int STORAGE_CLASS_SPECIFIER_REGISTER = 73;

  /**
   * The number of structural features of the '<em>Storage Class Specifier Register</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STORAGE_CLASS_SPECIFIER_REGISTER_FEATURE_COUNT = STORAGE_CLASS_SPECIFIER_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.TypeQualifierConstImpl <em>Type Qualifier Const</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.TypeQualifierConstImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getTypeQualifierConst()
   * @generated
   */
  int TYPE_QUALIFIER_CONST = 74;

  /**
   * The number of structural features of the '<em>Type Qualifier Const</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_QUALIFIER_CONST_FEATURE_COUNT = TYPE_QUALIFIER_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.TypeQualifierVolatileImpl <em>Type Qualifier Volatile</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.TypeQualifierVolatileImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getTypeQualifierVolatile()
   * @generated
   */
  int TYPE_QUALIFIER_VOLATILE = 75;

  /**
   * The number of structural features of the '<em>Type Qualifier Volatile</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_QUALIFIER_VOLATILE_FEATURE_COUNT = TYPE_QUALIFIER_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.TypeQualifierAtomicImpl <em>Type Qualifier Atomic</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.TypeQualifierAtomicImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getTypeQualifierAtomic()
   * @generated
   */
  int TYPE_QUALIFIER_ATOMIC = 76;

  /**
   * The number of structural features of the '<em>Type Qualifier Atomic</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_QUALIFIER_ATOMIC_FEATURE_COUNT = TYPE_QUALIFIER_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.FunctionSpecifierInlineImpl <em>Function Specifier Inline</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.FunctionSpecifierInlineImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getFunctionSpecifierInline()
   * @generated
   */
  int FUNCTION_SPECIFIER_INLINE = 77;

  /**
   * The number of structural features of the '<em>Function Specifier Inline</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_SPECIFIER_INLINE_FEATURE_COUNT = FUNCTION_SPECIFIER_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.FunctionSpecifierNoReturnImpl <em>Function Specifier No Return</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.FunctionSpecifierNoReturnImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getFunctionSpecifierNoReturn()
   * @generated
   */
  int FUNCTION_SPECIFIER_NO_RETURN = 78;

  /**
   * The number of structural features of the '<em>Function Specifier No Return</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_SPECIFIER_NO_RETURN_FEATURE_COUNT = FUNCTION_SPECIFIER_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.AssignmentExpressionImpl <em>Assignment Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.AssignmentExpressionImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getAssignmentExpression()
   * @generated
   */
  int ASSIGNMENT_EXPRESSION = 79;

  /**
   * The feature id for the '<em><b>Exp</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASSIGNMENT_EXPRESSION__EXP = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>List</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASSIGNMENT_EXPRESSION__LIST = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Assignment Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASSIGNMENT_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.ConditionalExpressionImpl <em>Conditional Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.ConditionalExpressionImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getConditionalExpression()
   * @generated
   */
  int CONDITIONAL_EXPRESSION = 80;

  /**
   * The feature id for the '<em><b>If</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONDITIONAL_EXPRESSION__IF = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Yes</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONDITIONAL_EXPRESSION__YES = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>No</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONDITIONAL_EXPRESSION__NO = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Conditional Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONDITIONAL_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.LogicalOrExpressionImpl <em>Logical Or Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.LogicalOrExpressionImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getLogicalOrExpression()
   * @generated
   */
  int LOGICAL_OR_EXPRESSION = 81;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LOGICAL_OR_EXPRESSION__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Operand</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LOGICAL_OR_EXPRESSION__OPERAND = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LOGICAL_OR_EXPRESSION__RIGHT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Logical Or Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LOGICAL_OR_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.LogicalAndExpressionImpl <em>Logical And Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.LogicalAndExpressionImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getLogicalAndExpression()
   * @generated
   */
  int LOGICAL_AND_EXPRESSION = 82;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LOGICAL_AND_EXPRESSION__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Operand</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LOGICAL_AND_EXPRESSION__OPERAND = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LOGICAL_AND_EXPRESSION__RIGHT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Logical And Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LOGICAL_AND_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.InclusiveOrExpressionImpl <em>Inclusive Or Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.InclusiveOrExpressionImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getInclusiveOrExpression()
   * @generated
   */
  int INCLUSIVE_OR_EXPRESSION = 83;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INCLUSIVE_OR_EXPRESSION__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Operand</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INCLUSIVE_OR_EXPRESSION__OPERAND = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INCLUSIVE_OR_EXPRESSION__RIGHT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Inclusive Or Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INCLUSIVE_OR_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.ExclusiveOrExpressionImpl <em>Exclusive Or Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.ExclusiveOrExpressionImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getExclusiveOrExpression()
   * @generated
   */
  int EXCLUSIVE_OR_EXPRESSION = 84;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCLUSIVE_OR_EXPRESSION__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Operand</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCLUSIVE_OR_EXPRESSION__OPERAND = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCLUSIVE_OR_EXPRESSION__RIGHT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Exclusive Or Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCLUSIVE_OR_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.AndExpressionImpl <em>And Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.AndExpressionImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getAndExpression()
   * @generated
   */
  int AND_EXPRESSION = 85;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AND_EXPRESSION__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Operand</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AND_EXPRESSION__OPERAND = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AND_EXPRESSION__RIGHT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>And Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AND_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.EqualityExpressionImpl <em>Equality Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.EqualityExpressionImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getEqualityExpression()
   * @generated
   */
  int EQUALITY_EXPRESSION = 86;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EQUALITY_EXPRESSION__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Operand</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EQUALITY_EXPRESSION__OPERAND = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EQUALITY_EXPRESSION__RIGHT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Equality Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EQUALITY_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.RelationalExpressionImpl <em>Relational Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.RelationalExpressionImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getRelationalExpression()
   * @generated
   */
  int RELATIONAL_EXPRESSION = 87;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RELATIONAL_EXPRESSION__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Operand</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RELATIONAL_EXPRESSION__OPERAND = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RELATIONAL_EXPRESSION__RIGHT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Relational Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RELATIONAL_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.ShiftExpressionImpl <em>Shift Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.ShiftExpressionImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getShiftExpression()
   * @generated
   */
  int SHIFT_EXPRESSION = 88;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SHIFT_EXPRESSION__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Operand</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SHIFT_EXPRESSION__OPERAND = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SHIFT_EXPRESSION__RIGHT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Shift Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SHIFT_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.AdditiveExpressionImpl <em>Additive Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.AdditiveExpressionImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getAdditiveExpression()
   * @generated
   */
  int ADDITIVE_EXPRESSION = 89;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADDITIVE_EXPRESSION__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Operand</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADDITIVE_EXPRESSION__OPERAND = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADDITIVE_EXPRESSION__RIGHT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Additive Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADDITIVE_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.MultiplicativeExpressionImpl <em>Multiplicative Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.MultiplicativeExpressionImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getMultiplicativeExpression()
   * @generated
   */
  int MULTIPLICATIVE_EXPRESSION = 90;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTIPLICATIVE_EXPRESSION__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Operand</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTIPLICATIVE_EXPRESSION__OPERAND = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTIPLICATIVE_EXPRESSION__RIGHT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Multiplicative Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTIPLICATIVE_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.ExpressionSimpleImpl <em>Expression Simple</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.ExpressionSimpleImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getExpressionSimple()
   * @generated
   */
  int EXPRESSION_SIMPLE = 91;

  /**
   * The feature id for the '<em><b>Exp</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_SIMPLE__EXP = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Next</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_SIMPLE__NEXT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Expression Simple</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_SIMPLE_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.ExpressionComplexImpl <em>Expression Complex</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.ExpressionComplexImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getExpressionComplex()
   * @generated
   */
  int EXPRESSION_COMPLEX = 92;

  /**
   * The feature id for the '<em><b>Exps</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_COMPLEX__EXPS = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Next</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_COMPLEX__NEXT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Expression Complex</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_COMPLEX_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.ExpressionIncrementImpl <em>Expression Increment</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.ExpressionIncrementImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getExpressionIncrement()
   * @generated
   */
  int EXPRESSION_INCREMENT = 93;

  /**
   * The feature id for the '<em><b>Content</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_INCREMENT__CONTENT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Expression Increment</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_INCREMENT_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.ExpressionDecrementImpl <em>Expression Decrement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.ExpressionDecrementImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getExpressionDecrement()
   * @generated
   */
  int EXPRESSION_DECREMENT = 94;

  /**
   * The feature id for the '<em><b>Content</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_DECREMENT__CONTENT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Expression Decrement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_DECREMENT_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.ExpressionSizeofImpl <em>Expression Sizeof</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.ExpressionSizeofImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getExpressionSizeof()
   * @generated
   */
  int EXPRESSION_SIZEOF = 95;

  /**
   * The feature id for the '<em><b>Content</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_SIZEOF__CONTENT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_SIZEOF__TYPE = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Expression Sizeof</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_SIZEOF_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.ExpressionUnaryOpImpl <em>Expression Unary Op</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.ExpressionUnaryOpImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getExpressionUnaryOp()
   * @generated
   */
  int EXPRESSION_UNARY_OP = 96;

  /**
   * The feature id for the '<em><b>Op</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_UNARY_OP__OP = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Content</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_UNARY_OP__CONTENT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Expression Unary Op</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_UNARY_OP_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.ExpressionAlignImpl <em>Expression Align</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.ExpressionAlignImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getExpressionAlign()
   * @generated
   */
  int EXPRESSION_ALIGN = 97;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_ALIGN__TYPE = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Expression Align</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_ALIGN_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.ExpressionCastImpl <em>Expression Cast</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.ExpressionCastImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getExpressionCast()
   * @generated
   */
  int EXPRESSION_CAST = 98;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_CAST__TYPE = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Exp</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_CAST__EXP = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Expression Cast</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_CAST_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.ExpressionIdentifierImpl <em>Expression Identifier</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.ExpressionIdentifierImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getExpressionIdentifier()
   * @generated
   */
  int EXPRESSION_IDENTIFIER = 99;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_IDENTIFIER__NAME = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Expression Identifier</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_IDENTIFIER_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.ExpressionStringImpl <em>Expression String</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.ExpressionStringImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getExpressionString()
   * @generated
   */
  int EXPRESSION_STRING = 100;

  /**
   * The feature id for the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_STRING__VALUE = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Expression String</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_STRING_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.ExpressionIntegerImpl <em>Expression Integer</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.ExpressionIntegerImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getExpressionInteger()
   * @generated
   */
  int EXPRESSION_INTEGER = 101;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_INTEGER__VALUE = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Expression Integer</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_INTEGER_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.ExpressionFloatImpl <em>Expression Float</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.ExpressionFloatImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getExpressionFloat()
   * @generated
   */
  int EXPRESSION_FLOAT = 102;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_FLOAT__VALUE = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Expression Float</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_FLOAT_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.ExpressionSubExpressionImpl <em>Expression Sub Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.ExpressionSubExpressionImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getExpressionSubExpression()
   * @generated
   */
  int EXPRESSION_SUB_EXPRESSION = 103;

  /**
   * The feature id for the '<em><b>Par</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_SUB_EXPRESSION__PAR = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Expression Sub Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_SUB_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.ExpressionGenericSelectionImpl <em>Expression Generic Selection</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.ExpressionGenericSelectionImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getExpressionGenericSelection()
   * @generated
   */
  int EXPRESSION_GENERIC_SELECTION = 104;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_GENERIC_SELECTION__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_GENERIC_SELECTION__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Expression Generic Selection</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_GENERIC_SELECTION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.GenericAssociationCaseImpl <em>Generic Association Case</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.GenericAssociationCaseImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getGenericAssociationCase()
   * @generated
   */
  int GENERIC_ASSOCIATION_CASE = 105;

  /**
   * The feature id for the '<em><b>Exp</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_ASSOCIATION_CASE__EXP = GENERIC_ASSOCIATION__EXP;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_ASSOCIATION_CASE__TYPE = GENERIC_ASSOCIATION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Generic Association Case</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_ASSOCIATION_CASE_FEATURE_COUNT = GENERIC_ASSOCIATION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.GenericAssociationDefaultImpl <em>Generic Association Default</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.GenericAssociationDefaultImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getGenericAssociationDefault()
   * @generated
   */
  int GENERIC_ASSOCIATION_DEFAULT = 106;

  /**
   * The feature id for the '<em><b>Exp</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_ASSOCIATION_DEFAULT__EXP = GENERIC_ASSOCIATION__EXP;

  /**
   * The number of structural features of the '<em>Generic Association Default</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_ASSOCIATION_DEFAULT_FEATURE_COUNT = GENERIC_ASSOCIATION_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.PostfixExpressionPostfixExpressionImpl <em>Postfix Expression Postfix Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.PostfixExpressionPostfixExpressionImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getPostfixExpressionPostfixExpression()
   * @generated
   */
  int POSTFIX_EXPRESSION_POSTFIX_EXPRESSION = 107;

  /**
   * The feature id for the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int POSTFIX_EXPRESSION_POSTFIX_EXPRESSION__VALUE = POSTFIX_EXPRESSION_POSTFIX_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Postfix Expression Postfix Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int POSTFIX_EXPRESSION_POSTFIX_EXPRESSION_FEATURE_COUNT = POSTFIX_EXPRESSION_POSTFIX_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.PostfixExpressionPostfixArgumentListImpl <em>Postfix Expression Postfix Argument List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.PostfixExpressionPostfixArgumentListImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getPostfixExpressionPostfixArgumentList()
   * @generated
   */
  int POSTFIX_EXPRESSION_POSTFIX_ARGUMENT_LIST = 108;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int POSTFIX_EXPRESSION_POSTFIX_ARGUMENT_LIST__LEFT = POSTFIX_EXPRESSION_POSTFIX_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Next</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int POSTFIX_EXPRESSION_POSTFIX_ARGUMENT_LIST__NEXT = POSTFIX_EXPRESSION_POSTFIX_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Postfix Expression Postfix Argument List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int POSTFIX_EXPRESSION_POSTFIX_ARGUMENT_LIST_FEATURE_COUNT = POSTFIX_EXPRESSION_POSTFIX_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.PostfixExpressionPostfixAccessImpl <em>Postfix Expression Postfix Access</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.PostfixExpressionPostfixAccessImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getPostfixExpressionPostfixAccess()
   * @generated
   */
  int POSTFIX_EXPRESSION_POSTFIX_ACCESS = 109;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int POSTFIX_EXPRESSION_POSTFIX_ACCESS__VALUE = POSTFIX_EXPRESSION_POSTFIX_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Postfix Expression Postfix Access</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int POSTFIX_EXPRESSION_POSTFIX_ACCESS_FEATURE_COUNT = POSTFIX_EXPRESSION_POSTFIX_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.PostfixExpressionPostfixAccessPtrImpl <em>Postfix Expression Postfix Access Ptr</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.PostfixExpressionPostfixAccessPtrImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getPostfixExpressionPostfixAccessPtr()
   * @generated
   */
  int POSTFIX_EXPRESSION_POSTFIX_ACCESS_PTR = 110;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int POSTFIX_EXPRESSION_POSTFIX_ACCESS_PTR__VALUE = POSTFIX_EXPRESSION_POSTFIX_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Postfix Expression Postfix Access Ptr</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int POSTFIX_EXPRESSION_POSTFIX_ACCESS_PTR_FEATURE_COUNT = POSTFIX_EXPRESSION_POSTFIX_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.PostfixExpressionPostfixIncrementImpl <em>Postfix Expression Postfix Increment</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.PostfixExpressionPostfixIncrementImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getPostfixExpressionPostfixIncrement()
   * @generated
   */
  int POSTFIX_EXPRESSION_POSTFIX_INCREMENT = 111;

  /**
   * The number of structural features of the '<em>Postfix Expression Postfix Increment</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int POSTFIX_EXPRESSION_POSTFIX_INCREMENT_FEATURE_COUNT = POSTFIX_EXPRESSION_POSTFIX_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.PostfixExpressionPostfixDecrementImpl <em>Postfix Expression Postfix Decrement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.PostfixExpressionPostfixDecrementImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getPostfixExpressionPostfixDecrement()
   * @generated
   */
  int POSTFIX_EXPRESSION_POSTFIX_DECREMENT = 112;

  /**
   * The number of structural features of the '<em>Postfix Expression Postfix Decrement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int POSTFIX_EXPRESSION_POSTFIX_DECREMENT_FEATURE_COUNT = POSTFIX_EXPRESSION_POSTFIX_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.StatementDeclarationImpl <em>Statement Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.StatementDeclarationImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getStatementDeclaration()
   * @generated
   */
  int STATEMENT_DECLARATION = 113;

  /**
   * The feature id for the '<em><b>Decl</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATEMENT_DECLARATION__DECL = BLOCK_ITEM_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Statement Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATEMENT_DECLARATION_FEATURE_COUNT = BLOCK_ITEM_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.StatementInnerStatementImpl <em>Statement Inner Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.StatementInnerStatementImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getStatementInnerStatement()
   * @generated
   */
  int STATEMENT_INNER_STATEMENT = 114;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATEMENT_INNER_STATEMENT__STATEMENT = BLOCK_ITEM_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Statement Inner Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATEMENT_INNER_STATEMENT_FEATURE_COUNT = BLOCK_ITEM_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.LabeledStatementCaseImpl <em>Labeled Statement Case</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.LabeledStatementCaseImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getLabeledStatementCase()
   * @generated
   */
  int LABELED_STATEMENT_CASE = 115;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LABELED_STATEMENT_CASE__STATEMENT = LABELED_STATEMENT__STATEMENT;

  /**
   * The feature id for the '<em><b>Exp</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LABELED_STATEMENT_CASE__EXP = LABELED_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Labeled Statement Case</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LABELED_STATEMENT_CASE_FEATURE_COUNT = LABELED_STATEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.LabeledStatementDefaultImpl <em>Labeled Statement Default</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.LabeledStatementDefaultImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getLabeledStatementDefault()
   * @generated
   */
  int LABELED_STATEMENT_DEFAULT = 116;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LABELED_STATEMENT_DEFAULT__STATEMENT = LABELED_STATEMENT__STATEMENT;

  /**
   * The number of structural features of the '<em>Labeled Statement Default</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LABELED_STATEMENT_DEFAULT_FEATURE_COUNT = LABELED_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.GoToPrefixStatementImpl <em>Go To Prefix Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.GoToPrefixStatementImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getGoToPrefixStatement()
   * @generated
   */
  int GO_TO_PREFIX_STATEMENT = 117;

  /**
   * The feature id for the '<em><b>Goto</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GO_TO_PREFIX_STATEMENT__GOTO = STATEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GO_TO_PREFIX_STATEMENT__STATEMENT = STATEMENT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Go To Prefix Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GO_TO_PREFIX_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.SelectionStatementIfImpl <em>Selection Statement If</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.SelectionStatementIfImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getSelectionStatementIf()
   * @generated
   */
  int SELECTION_STATEMENT_IF = 118;

  /**
   * The feature id for the '<em><b>Exp</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECTION_STATEMENT_IF__EXP = SELECTION_STATEMENT__EXP;

  /**
   * The feature id for the '<em><b>Then statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECTION_STATEMENT_IF__THEN_STATEMENT = SELECTION_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Else statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECTION_STATEMENT_IF__ELSE_STATEMENT = SELECTION_STATEMENT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Selection Statement If</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECTION_STATEMENT_IF_FEATURE_COUNT = SELECTION_STATEMENT_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.SelectionStatementSwitchImpl <em>Selection Statement Switch</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.SelectionStatementSwitchImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getSelectionStatementSwitch()
   * @generated
   */
  int SELECTION_STATEMENT_SWITCH = 119;

  /**
   * The feature id for the '<em><b>Exp</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECTION_STATEMENT_SWITCH__EXP = SELECTION_STATEMENT__EXP;

  /**
   * The feature id for the '<em><b>Content</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECTION_STATEMENT_SWITCH__CONTENT = SELECTION_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Selection Statement Switch</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECTION_STATEMENT_SWITCH_FEATURE_COUNT = SELECTION_STATEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.IterationStatementWhileImpl <em>Iteration Statement While</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.IterationStatementWhileImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getIterationStatementWhile()
   * @generated
   */
  int ITERATION_STATEMENT_WHILE = 120;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ITERATION_STATEMENT_WHILE__STATEMENT = ITERATION_STATEMENT__STATEMENT;

  /**
   * The feature id for the '<em><b>Exp</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ITERATION_STATEMENT_WHILE__EXP = ITERATION_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Iteration Statement While</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ITERATION_STATEMENT_WHILE_FEATURE_COUNT = ITERATION_STATEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.IterationStatementDoImpl <em>Iteration Statement Do</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.IterationStatementDoImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getIterationStatementDo()
   * @generated
   */
  int ITERATION_STATEMENT_DO = 121;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ITERATION_STATEMENT_DO__STATEMENT = ITERATION_STATEMENT__STATEMENT;

  /**
   * The feature id for the '<em><b>Exp</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ITERATION_STATEMENT_DO__EXP = ITERATION_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Iteration Statement Do</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ITERATION_STATEMENT_DO_FEATURE_COUNT = ITERATION_STATEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.IterationStatementForImpl <em>Iteration Statement For</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.IterationStatementForImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getIterationStatementFor()
   * @generated
   */
  int ITERATION_STATEMENT_FOR = 122;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ITERATION_STATEMENT_FOR__STATEMENT = ITERATION_STATEMENT__STATEMENT;

  /**
   * The feature id for the '<em><b>Field1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ITERATION_STATEMENT_FOR__FIELD1 = ITERATION_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Decl</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ITERATION_STATEMENT_FOR__DECL = ITERATION_STATEMENT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Field2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ITERATION_STATEMENT_FOR__FIELD2 = ITERATION_STATEMENT_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Field3</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ITERATION_STATEMENT_FOR__FIELD3 = ITERATION_STATEMENT_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Next</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ITERATION_STATEMENT_FOR__NEXT = ITERATION_STATEMENT_FEATURE_COUNT + 4;

  /**
   * The number of structural features of the '<em>Iteration Statement For</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ITERATION_STATEMENT_FOR_FEATURE_COUNT = ITERATION_STATEMENT_FEATURE_COUNT + 5;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.JumpStatementGotoImpl <em>Jump Statement Goto</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.JumpStatementGotoImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getJumpStatementGoto()
   * @generated
   */
  int JUMP_STATEMENT_GOTO = 123;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JUMP_STATEMENT_GOTO__NAME = JUMP_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Jump Statement Goto</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JUMP_STATEMENT_GOTO_FEATURE_COUNT = JUMP_STATEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.JumpStatementContinueImpl <em>Jump Statement Continue</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.JumpStatementContinueImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getJumpStatementContinue()
   * @generated
   */
  int JUMP_STATEMENT_CONTINUE = 124;

  /**
   * The number of structural features of the '<em>Jump Statement Continue</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JUMP_STATEMENT_CONTINUE_FEATURE_COUNT = JUMP_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.JumpStatementBreakImpl <em>Jump Statement Break</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.JumpStatementBreakImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getJumpStatementBreak()
   * @generated
   */
  int JUMP_STATEMENT_BREAK = 125;

  /**
   * The number of structural features of the '<em>Jump Statement Break</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JUMP_STATEMENT_BREAK_FEATURE_COUNT = JUMP_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.clang.clang.impl.JumpStatementReturnImpl <em>Jump Statement Return</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.clang.clang.impl.JumpStatementReturnImpl
   * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getJumpStatementReturn()
   * @generated
   */
  int JUMP_STATEMENT_RETURN = 126;

  /**
   * The feature id for the '<em><b>Exp</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JUMP_STATEMENT_RETURN__EXP = JUMP_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Jump Statement Return</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JUMP_STATEMENT_RETURN_FEATURE_COUNT = JUMP_STATEMENT_FEATURE_COUNT + 1;


  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.Model <em>Model</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Model</em>'.
   * @see org.xtext.dop.clang.clang.Model
   * @generated
   */
  EClass getModel();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.dop.clang.clang.Model#getIncludesStd <em>Includes Std</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Includes Std</em>'.
   * @see org.xtext.dop.clang.clang.Model#getIncludesStd()
   * @see #getModel()
   * @generated
   */
  EReference getModel_IncludesStd();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.dop.clang.clang.Model#getIncludesFile <em>Includes File</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Includes File</em>'.
   * @see org.xtext.dop.clang.clang.Model#getIncludesFile()
   * @see #getModel()
   * @generated
   */
  EReference getModel_IncludesFile();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.dop.clang.clang.Model#getDeclarations <em>Declarations</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Declarations</em>'.
   * @see org.xtext.dop.clang.clang.Model#getDeclarations()
   * @see #getModel()
   * @generated
   */
  EReference getModel_Declarations();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.StandardInclude <em>Standard Include</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Standard Include</em>'.
   * @see org.xtext.dop.clang.clang.StandardInclude
   * @generated
   */
  EClass getStandardInclude();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.StandardInclude#getStd <em>Std</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Std</em>'.
   * @see org.xtext.dop.clang.clang.StandardInclude#getStd()
   * @see #getStandardInclude()
   * @generated
   */
  EReference getStandardInclude_Std();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.dop.clang.clang.StandardInclude#isWild <em>Wild</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Wild</em>'.
   * @see org.xtext.dop.clang.clang.StandardInclude#isWild()
   * @see #getStandardInclude()
   * @generated
   */
  EAttribute getStandardInclude_Wild();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.FileInclude <em>File Include</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>File Include</em>'.
   * @see org.xtext.dop.clang.clang.FileInclude
   * @generated
   */
  EClass getFileInclude();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.dop.clang.clang.FileInclude#getFile <em>File</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>File</em>'.
   * @see org.xtext.dop.clang.clang.FileInclude#getFile()
   * @see #getFileInclude()
   * @generated
   */
  EAttribute getFileInclude_File();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.Source <em>Source</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Source</em>'.
   * @see org.xtext.dop.clang.clang.Source
   * @generated
   */
  EClass getSource();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.dop.clang.clang.Source#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.xtext.dop.clang.clang.Source#getName()
   * @see #getSource()
   * @generated
   */
  EAttribute getSource_Name();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.Declaration <em>Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Declaration</em>'.
   * @see org.xtext.dop.clang.clang.Declaration
   * @generated
   */
  EClass getDeclaration();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.TypeAliasDeclaration <em>Type Alias Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Type Alias Declaration</em>'.
   * @see org.xtext.dop.clang.clang.TypeAliasDeclaration
   * @generated
   */
  EClass getTypeAliasDeclaration();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.TypeAliasDeclaration#getTdecl <em>Tdecl</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Tdecl</em>'.
   * @see org.xtext.dop.clang.clang.TypeAliasDeclaration#getTdecl()
   * @see #getTypeAliasDeclaration()
   * @generated
   */
  EReference getTypeAliasDeclaration_Tdecl();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.GenericSpecifiedDeclaration <em>Generic Specified Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Generic Specified Declaration</em>'.
   * @see org.xtext.dop.clang.clang.GenericSpecifiedDeclaration
   * @generated
   */
  EClass getGenericSpecifiedDeclaration();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.dop.clang.clang.GenericSpecifiedDeclaration#getSpecs <em>Specs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Specs</em>'.
   * @see org.xtext.dop.clang.clang.GenericSpecifiedDeclaration#getSpecs()
   * @see #getGenericSpecifiedDeclaration()
   * @generated
   */
  EReference getGenericSpecifiedDeclaration_Specs();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.dop.clang.clang.GenericSpecifiedDeclaration#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Type</em>'.
   * @see org.xtext.dop.clang.clang.GenericSpecifiedDeclaration#getType()
   * @see #getGenericSpecifiedDeclaration()
   * @generated
   */
  EReference getGenericSpecifiedDeclaration_Type();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.GenericSpecifiedDeclaration#getDecl <em>Decl</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Decl</em>'.
   * @see org.xtext.dop.clang.clang.GenericSpecifiedDeclaration#getDecl()
   * @see #getGenericSpecifiedDeclaration()
   * @generated
   */
  EReference getGenericSpecifiedDeclaration_Decl();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.GenericDeclarationElements <em>Generic Declaration Elements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Generic Declaration Elements</em>'.
   * @see org.xtext.dop.clang.clang.GenericDeclarationElements
   * @generated
   */
  EClass getGenericDeclarationElements();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.dop.clang.clang.GenericDeclarationElements#getEls <em>Els</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Els</em>'.
   * @see org.xtext.dop.clang.clang.GenericDeclarationElements#getEls()
   * @see #getGenericDeclarationElements()
   * @generated
   */
  EReference getGenericDeclarationElements_Els();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.GenericDeclarationElements#getNext <em>Next</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Next</em>'.
   * @see org.xtext.dop.clang.clang.GenericDeclarationElements#getNext()
   * @see #getGenericDeclarationElements()
   * @generated
   */
  EReference getGenericDeclarationElements_Next();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.GenericDeclarationElement <em>Generic Declaration Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Generic Declaration Element</em>'.
   * @see org.xtext.dop.clang.clang.GenericDeclarationElement
   * @generated
   */
  EClass getGenericDeclarationElement();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.GenericDeclarationElement#getId <em>Id</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Id</em>'.
   * @see org.xtext.dop.clang.clang.GenericDeclarationElement#getId()
   * @see #getGenericDeclarationElement()
   * @generated
   */
  EReference getGenericDeclarationElement_Id();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.GenericDeclarationElement#getInit <em>Init</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Init</em>'.
   * @see org.xtext.dop.clang.clang.GenericDeclarationElement#getInit()
   * @see #getGenericDeclarationElement()
   * @generated
   */
  EReference getGenericDeclarationElement_Init();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.GenericDeclarationId <em>Generic Declaration Id</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Generic Declaration Id</em>'.
   * @see org.xtext.dop.clang.clang.GenericDeclarationId
   * @generated
   */
  EClass getGenericDeclarationId();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.GenericDeclarationId#getInner <em>Inner</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Inner</em>'.
   * @see org.xtext.dop.clang.clang.GenericDeclarationId#getInner()
   * @see #getGenericDeclarationId()
   * @generated
   */
  EReference getGenericDeclarationId_Inner();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.dop.clang.clang.GenericDeclarationId#getPosts <em>Posts</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Posts</em>'.
   * @see org.xtext.dop.clang.clang.GenericDeclarationId#getPosts()
   * @see #getGenericDeclarationId()
   * @generated
   */
  EReference getGenericDeclarationId_Posts();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.GenericDeclarationInnerId <em>Generic Declaration Inner Id</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Generic Declaration Inner Id</em>'.
   * @see org.xtext.dop.clang.clang.GenericDeclarationInnerId
   * @generated
   */
  EClass getGenericDeclarationInnerId();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.GenericDeclarationInnerId#getInner <em>Inner</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Inner</em>'.
   * @see org.xtext.dop.clang.clang.GenericDeclarationInnerId#getInner()
   * @see #getGenericDeclarationInnerId()
   * @generated
   */
  EReference getGenericDeclarationInnerId_Inner();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.GenericDeclarationIdPostfix <em>Generic Declaration Id Postfix</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Generic Declaration Id Postfix</em>'.
   * @see org.xtext.dop.clang.clang.GenericDeclarationIdPostfix
   * @generated
   */
  EClass getGenericDeclarationIdPostfix();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.GenericDeclarationIdPostfix#getArray <em>Array</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Array</em>'.
   * @see org.xtext.dop.clang.clang.GenericDeclarationIdPostfix#getArray()
   * @see #getGenericDeclarationIdPostfix()
   * @generated
   */
  EReference getGenericDeclarationIdPostfix_Array();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.GenericDeclarationIdPostfix#getFunction <em>Function</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Function</em>'.
   * @see org.xtext.dop.clang.clang.GenericDeclarationIdPostfix#getFunction()
   * @see #getGenericDeclarationIdPostfix()
   * @generated
   */
  EReference getGenericDeclarationIdPostfix_Function();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.ArrayPostfix <em>Array Postfix</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Array Postfix</em>'.
   * @see org.xtext.dop.clang.clang.ArrayPostfix
   * @generated
   */
  EClass getArrayPostfix();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.dop.clang.clang.ArrayPostfix#getStar <em>Star</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Star</em>'.
   * @see org.xtext.dop.clang.clang.ArrayPostfix#getStar()
   * @see #getArrayPostfix()
   * @generated
   */
  EAttribute getArrayPostfix_Star();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.ArrayPostfix#getExp <em>Exp</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Exp</em>'.
   * @see org.xtext.dop.clang.clang.ArrayPostfix#getExp()
   * @see #getArrayPostfix()
   * @generated
   */
  EReference getArrayPostfix_Exp();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.ParametersPostfix <em>Parameters Postfix</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Parameters Postfix</em>'.
   * @see org.xtext.dop.clang.clang.ParametersPostfix
   * @generated
   */
  EClass getParametersPostfix();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.dop.clang.clang.ParametersPostfix#getEls <em>Els</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Els</em>'.
   * @see org.xtext.dop.clang.clang.ParametersPostfix#getEls()
   * @see #getParametersPostfix()
   * @generated
   */
  EReference getParametersPostfix_Els();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.dop.clang.clang.ParametersPostfix#getNext <em>Next</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Next</em>'.
   * @see org.xtext.dop.clang.clang.ParametersPostfix#getNext()
   * @see #getParametersPostfix()
   * @generated
   */
  EReference getParametersPostfix_Next();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.dop.clang.clang.ParametersPostfix#getMore <em>More</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>More</em>'.
   * @see org.xtext.dop.clang.clang.ParametersPostfix#getMore()
   * @see #getParametersPostfix()
   * @generated
   */
  EAttribute getParametersPostfix_More();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.NextParameter <em>Next Parameter</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Next Parameter</em>'.
   * @see org.xtext.dop.clang.clang.NextParameter
   * @generated
   */
  EClass getNextParameter();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.dop.clang.clang.NextParameter#getEls <em>Els</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Els</em>'.
   * @see org.xtext.dop.clang.clang.NextParameter#getEls()
   * @see #getNextParameter()
   * @generated
   */
  EReference getNextParameter_Els();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.Parameter <em>Parameter</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Parameter</em>'.
   * @see org.xtext.dop.clang.clang.Parameter
   * @generated
   */
  EClass getParameter();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.dop.clang.clang.Parameter#getSpecs <em>Specs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Specs</em>'.
   * @see org.xtext.dop.clang.clang.Parameter#getSpecs()
   * @see #getParameter()
   * @generated
   */
  EReference getParameter_Specs();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.dop.clang.clang.Parameter#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Type</em>'.
   * @see org.xtext.dop.clang.clang.Parameter#getType()
   * @see #getParameter()
   * @generated
   */
  EReference getParameter_Type();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.Parameter#getId <em>Id</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Id</em>'.
   * @see org.xtext.dop.clang.clang.Parameter#getId()
   * @see #getParameter()
   * @generated
   */
  EReference getParameter_Id();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.GenericDeclarationInitializer <em>Generic Declaration Initializer</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Generic Declaration Initializer</em>'.
   * @see org.xtext.dop.clang.clang.GenericDeclarationInitializer
   * @generated
   */
  EClass getGenericDeclarationInitializer();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.SpecifiedType <em>Specified Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Specified Type</em>'.
   * @see org.xtext.dop.clang.clang.SpecifiedType
   * @generated
   */
  EClass getSpecifiedType();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.SpecifiedType#getPar <em>Par</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Par</em>'.
   * @see org.xtext.dop.clang.clang.SpecifiedType#getPar()
   * @see #getSpecifiedType()
   * @generated
   */
  EReference getSpecifiedType_Par();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.StaticAssertDeclaration <em>Static Assert Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Static Assert Declaration</em>'.
   * @see org.xtext.dop.clang.clang.StaticAssertDeclaration
   * @generated
   */
  EClass getStaticAssertDeclaration();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.StaticAssertDeclaration#getExp <em>Exp</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Exp</em>'.
   * @see org.xtext.dop.clang.clang.StaticAssertDeclaration#getExp()
   * @see #getStaticAssertDeclaration()
   * @generated
   */
  EReference getStaticAssertDeclaration_Exp();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.dop.clang.clang.StaticAssertDeclaration#getString <em>String</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>String</em>'.
   * @see org.xtext.dop.clang.clang.StaticAssertDeclaration#getString()
   * @see #getStaticAssertDeclaration()
   * @generated
   */
  EAttribute getStaticAssertDeclaration_String();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.SimpleType <em>Simple Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Simple Type</em>'.
   * @see org.xtext.dop.clang.clang.SimpleType
   * @generated
   */
  EClass getSimpleType();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.BaseType <em>Base Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Base Type</em>'.
   * @see org.xtext.dop.clang.clang.BaseType
   * @generated
   */
  EClass getBaseType();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.StructOrUnionType <em>Struct Or Union Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Struct Or Union Type</em>'.
   * @see org.xtext.dop.clang.clang.StructOrUnionType
   * @generated
   */
  EClass getStructOrUnionType();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.dop.clang.clang.StructOrUnionType#getKind <em>Kind</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Kind</em>'.
   * @see org.xtext.dop.clang.clang.StructOrUnionType#getKind()
   * @see #getStructOrUnionType()
   * @generated
   */
  EAttribute getStructOrUnionType_Kind();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.dop.clang.clang.StructOrUnionType#getContent <em>Content</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Content</em>'.
   * @see org.xtext.dop.clang.clang.StructOrUnionType#getContent()
   * @see #getStructOrUnionType()
   * @generated
   */
  EReference getStructOrUnionType_Content();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.dop.clang.clang.StructOrUnionType#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.xtext.dop.clang.clang.StructOrUnionType#getName()
   * @see #getStructOrUnionType()
   * @generated
   */
  EAttribute getStructOrUnionType_Name();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.EnumType <em>Enum Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Enum Type</em>'.
   * @see org.xtext.dop.clang.clang.EnumType
   * @generated
   */
  EClass getEnumType();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.EnumType#getContent <em>Content</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Content</em>'.
   * @see org.xtext.dop.clang.clang.EnumType#getContent()
   * @see #getEnumType()
   * @generated
   */
  EReference getEnumType_Content();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.dop.clang.clang.EnumType#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.xtext.dop.clang.clang.EnumType#getName()
   * @see #getEnumType()
   * @generated
   */
  EAttribute getEnumType_Name();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.EnumeratorList <em>Enumerator List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Enumerator List</em>'.
   * @see org.xtext.dop.clang.clang.EnumeratorList
   * @generated
   */
  EClass getEnumeratorList();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.dop.clang.clang.EnumeratorList#getElements <em>Elements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Elements</em>'.
   * @see org.xtext.dop.clang.clang.EnumeratorList#getElements()
   * @see #getEnumeratorList()
   * @generated
   */
  EReference getEnumeratorList_Elements();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.Enumerator <em>Enumerator</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Enumerator</em>'.
   * @see org.xtext.dop.clang.clang.Enumerator
   * @generated
   */
  EClass getEnumerator();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.Enumerator#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Name</em>'.
   * @see org.xtext.dop.clang.clang.Enumerator#getName()
   * @see #getEnumerator()
   * @generated
   */
  EReference getEnumerator_Name();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.Enumerator#getExp <em>Exp</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Exp</em>'.
   * @see org.xtext.dop.clang.clang.Enumerator#getExp()
   * @see #getEnumerator()
   * @generated
   */
  EReference getEnumerator_Exp();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.EnumerationConstant <em>Enumeration Constant</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Enumeration Constant</em>'.
   * @see org.xtext.dop.clang.clang.EnumerationConstant
   * @generated
   */
  EClass getEnumerationConstant();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.dop.clang.clang.EnumerationConstant#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.xtext.dop.clang.clang.EnumerationConstant#getName()
   * @see #getEnumerationConstant()
   * @generated
   */
  EAttribute getEnumerationConstant_Name();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.DeclarationSpecifier <em>Declaration Specifier</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Declaration Specifier</em>'.
   * @see org.xtext.dop.clang.clang.DeclarationSpecifier
   * @generated
   */
  EClass getDeclarationSpecifier();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.StorageClassSpecifier <em>Storage Class Specifier</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Storage Class Specifier</em>'.
   * @see org.xtext.dop.clang.clang.StorageClassSpecifier
   * @generated
   */
  EClass getStorageClassSpecifier();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.TypeQualifier <em>Type Qualifier</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Type Qualifier</em>'.
   * @see org.xtext.dop.clang.clang.TypeQualifier
   * @generated
   */
  EClass getTypeQualifier();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.FunctionSpecifier <em>Function Specifier</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Function Specifier</em>'.
   * @see org.xtext.dop.clang.clang.FunctionSpecifier
   * @generated
   */
  EClass getFunctionSpecifier();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.AlignmentSpecifier <em>Alignment Specifier</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Alignment Specifier</em>'.
   * @see org.xtext.dop.clang.clang.AlignmentSpecifier
   * @generated
   */
  EClass getAlignmentSpecifier();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.AlignmentSpecifier#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see org.xtext.dop.clang.clang.AlignmentSpecifier#getType()
   * @see #getAlignmentSpecifier()
   * @generated
   */
  EReference getAlignmentSpecifier_Type();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.AlignmentSpecifier#getExp <em>Exp</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Exp</em>'.
   * @see org.xtext.dop.clang.clang.AlignmentSpecifier#getExp()
   * @see #getAlignmentSpecifier()
   * @generated
   */
  EReference getAlignmentSpecifier_Exp();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.Expression <em>Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Expression</em>'.
   * @see org.xtext.dop.clang.clang.Expression
   * @generated
   */
  EClass getExpression();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.AssignmentExpressionElement <em>Assignment Expression Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Assignment Expression Element</em>'.
   * @see org.xtext.dop.clang.clang.AssignmentExpressionElement
   * @generated
   */
  EClass getAssignmentExpressionElement();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.dop.clang.clang.AssignmentExpressionElement#getOp <em>Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Op</em>'.
   * @see org.xtext.dop.clang.clang.AssignmentExpressionElement#getOp()
   * @see #getAssignmentExpressionElement()
   * @generated
   */
  EAttribute getAssignmentExpressionElement_Op();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.AssignmentExpressionElement#getExp <em>Exp</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Exp</em>'.
   * @see org.xtext.dop.clang.clang.AssignmentExpressionElement#getExp()
   * @see #getAssignmentExpressionElement()
   * @generated
   */
  EReference getAssignmentExpressionElement_Exp();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.StringExpression <em>String Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>String Expression</em>'.
   * @see org.xtext.dop.clang.clang.StringExpression
   * @generated
   */
  EClass getStringExpression();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.dop.clang.clang.StringExpression#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.xtext.dop.clang.clang.StringExpression#getName()
   * @see #getStringExpression()
   * @generated
   */
  EAttribute getStringExpression_Name();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.GenericAssociation <em>Generic Association</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Generic Association</em>'.
   * @see org.xtext.dop.clang.clang.GenericAssociation
   * @generated
   */
  EClass getGenericAssociation();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.GenericAssociation#getExp <em>Exp</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Exp</em>'.
   * @see org.xtext.dop.clang.clang.GenericAssociation#getExp()
   * @see #getGenericAssociation()
   * @generated
   */
  EReference getGenericAssociation_Exp();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.PostfixExpressionPostfix <em>Postfix Expression Postfix</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Postfix Expression Postfix</em>'.
   * @see org.xtext.dop.clang.clang.PostfixExpressionPostfix
   * @generated
   */
  EClass getPostfixExpressionPostfix();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.NextAsignement <em>Next Asignement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Next Asignement</em>'.
   * @see org.xtext.dop.clang.clang.NextAsignement
   * @generated
   */
  EClass getNextAsignement();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.dop.clang.clang.NextAsignement#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Right</em>'.
   * @see org.xtext.dop.clang.clang.NextAsignement#getRight()
   * @see #getNextAsignement()
   * @generated
   */
  EReference getNextAsignement_Right();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.CompoundStatement <em>Compound Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Compound Statement</em>'.
   * @see org.xtext.dop.clang.clang.CompoundStatement
   * @generated
   */
  EClass getCompoundStatement();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.dop.clang.clang.CompoundStatement#getInner <em>Inner</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Inner</em>'.
   * @see org.xtext.dop.clang.clang.CompoundStatement#getInner()
   * @see #getCompoundStatement()
   * @generated
   */
  EReference getCompoundStatement_Inner();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.BlockItem <em>Block Item</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Block Item</em>'.
   * @see org.xtext.dop.clang.clang.BlockItem
   * @generated
   */
  EClass getBlockItem();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.Statements <em>Statements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Statements</em>'.
   * @see org.xtext.dop.clang.clang.Statements
   * @generated
   */
  EClass getStatements();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.LabeledStatement <em>Labeled Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Labeled Statement</em>'.
   * @see org.xtext.dop.clang.clang.LabeledStatement
   * @generated
   */
  EClass getLabeledStatement();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.LabeledStatement#getStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statement</em>'.
   * @see org.xtext.dop.clang.clang.LabeledStatement#getStatement()
   * @see #getLabeledStatement()
   * @generated
   */
  EReference getLabeledStatement_Statement();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.Statement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Statement</em>'.
   * @see org.xtext.dop.clang.clang.Statement
   * @generated
   */
  EClass getStatement();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.ExpressionStatement <em>Expression Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Expression Statement</em>'.
   * @see org.xtext.dop.clang.clang.ExpressionStatement
   * @generated
   */
  EClass getExpressionStatement();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.ExpressionStatement#getExp <em>Exp</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Exp</em>'.
   * @see org.xtext.dop.clang.clang.ExpressionStatement#getExp()
   * @see #getExpressionStatement()
   * @generated
   */
  EReference getExpressionStatement_Exp();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.SelectionStatement <em>Selection Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Selection Statement</em>'.
   * @see org.xtext.dop.clang.clang.SelectionStatement
   * @generated
   */
  EClass getSelectionStatement();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.SelectionStatement#getExp <em>Exp</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Exp</em>'.
   * @see org.xtext.dop.clang.clang.SelectionStatement#getExp()
   * @see #getSelectionStatement()
   * @generated
   */
  EReference getSelectionStatement_Exp();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.IterationStatement <em>Iteration Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Iteration Statement</em>'.
   * @see org.xtext.dop.clang.clang.IterationStatement
   * @generated
   */
  EClass getIterationStatement();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.IterationStatement#getStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statement</em>'.
   * @see org.xtext.dop.clang.clang.IterationStatement#getStatement()
   * @see #getIterationStatement()
   * @generated
   */
  EReference getIterationStatement_Statement();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.NextField <em>Next Field</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Next Field</em>'.
   * @see org.xtext.dop.clang.clang.NextField
   * @generated
   */
  EClass getNextField();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.NextField#getNext <em>Next</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Next</em>'.
   * @see org.xtext.dop.clang.clang.NextField#getNext()
   * @see #getNextField()
   * @generated
   */
  EReference getNextField_Next();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.JumpStatement <em>Jump Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Jump Statement</em>'.
   * @see org.xtext.dop.clang.clang.JumpStatement
   * @generated
   */
  EClass getJumpStatement();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.CompleteName <em>Complete Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Complete Name</em>'.
   * @see org.xtext.dop.clang.clang.CompleteName
   * @generated
   */
  EClass getCompleteName();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.dop.clang.clang.CompleteName#getExt <em>Ext</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ext</em>'.
   * @see org.xtext.dop.clang.clang.CompleteName#getExt()
   * @see #getCompleteName()
   * @generated
   */
  EAttribute getCompleteName_Ext();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.SimpleName <em>Simple Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Simple Name</em>'.
   * @see org.xtext.dop.clang.clang.SimpleName
   * @generated
   */
  EClass getSimpleName();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.GenericDeclaration0 <em>Generic Declaration0</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Generic Declaration0</em>'.
   * @see org.xtext.dop.clang.clang.GenericDeclaration0
   * @generated
   */
  EClass getGenericDeclaration0();

  /**
   * Returns the meta object for the attribute list '{@link org.xtext.dop.clang.clang.GenericDeclaration0#getPointers <em>Pointers</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Pointers</em>'.
   * @see org.xtext.dop.clang.clang.GenericDeclaration0#getPointers()
   * @see #getGenericDeclaration0()
   * @generated
   */
  EAttribute getGenericDeclaration0_Pointers();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.dop.clang.clang.GenericDeclaration0#getRestrict <em>Restrict</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Restrict</em>'.
   * @see org.xtext.dop.clang.clang.GenericDeclaration0#getRestrict()
   * @see #getGenericDeclaration0()
   * @generated
   */
  EAttribute getGenericDeclaration0_Restrict();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.GenericDeclaration1 <em>Generic Declaration1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Generic Declaration1</em>'.
   * @see org.xtext.dop.clang.clang.GenericDeclaration1
   * @generated
   */
  EClass getGenericDeclaration1();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.GenericDeclaration2 <em>Generic Declaration2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Generic Declaration2</em>'.
   * @see org.xtext.dop.clang.clang.GenericDeclaration2
   * @generated
   */
  EClass getGenericDeclaration2();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.GlobalName <em>Global Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Global Name</em>'.
   * @see org.xtext.dop.clang.clang.GlobalName
   * @generated
   */
  EClass getGlobalName();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.dop.clang.clang.GlobalName#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.xtext.dop.clang.clang.GlobalName#getName()
   * @see #getGlobalName()
   * @generated
   */
  EAttribute getGlobalName_Name();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.ExpressionInitializer <em>Expression Initializer</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Expression Initializer</em>'.
   * @see org.xtext.dop.clang.clang.ExpressionInitializer
   * @generated
   */
  EClass getExpressionInitializer();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.ExpressionInitializer#getExp <em>Exp</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Exp</em>'.
   * @see org.xtext.dop.clang.clang.ExpressionInitializer#getExp()
   * @see #getExpressionInitializer()
   * @generated
   */
  EReference getExpressionInitializer_Exp();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.FunctionInitializer <em>Function Initializer</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Function Initializer</em>'.
   * @see org.xtext.dop.clang.clang.FunctionInitializer
   * @generated
   */
  EClass getFunctionInitializer();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.FunctionInitializer#getBlock <em>Block</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Block</em>'.
   * @see org.xtext.dop.clang.clang.FunctionInitializer#getBlock()
   * @see #getFunctionInitializer()
   * @generated
   */
  EReference getFunctionInitializer_Block();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.Atomic <em>Atomic</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Atomic</em>'.
   * @see org.xtext.dop.clang.clang.Atomic
   * @generated
   */
  EClass getAtomic();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.Atomic#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see org.xtext.dop.clang.clang.Atomic#getType()
   * @see #getAtomic()
   * @generated
   */
  EReference getAtomic_Type();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.BaseTypeVoid <em>Base Type Void</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Base Type Void</em>'.
   * @see org.xtext.dop.clang.clang.BaseTypeVoid
   * @generated
   */
  EClass getBaseTypeVoid();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.Custom <em>Custom</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Custom</em>'.
   * @see org.xtext.dop.clang.clang.Custom
   * @generated
   */
  EClass getCustom();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.dop.clang.clang.Custom#getId <em>Id</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Id</em>'.
   * @see org.xtext.dop.clang.clang.Custom#getId()
   * @see #getCustom()
   * @generated
   */
  EAttribute getCustom_Id();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.BaseTypeChar <em>Base Type Char</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Base Type Char</em>'.
   * @see org.xtext.dop.clang.clang.BaseTypeChar
   * @generated
   */
  EClass getBaseTypeChar();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.BaseTypeShort <em>Base Type Short</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Base Type Short</em>'.
   * @see org.xtext.dop.clang.clang.BaseTypeShort
   * @generated
   */
  EClass getBaseTypeShort();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.BaseTypeInt <em>Base Type Int</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Base Type Int</em>'.
   * @see org.xtext.dop.clang.clang.BaseTypeInt
   * @generated
   */
  EClass getBaseTypeInt();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.BaseTypeLong <em>Base Type Long</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Base Type Long</em>'.
   * @see org.xtext.dop.clang.clang.BaseTypeLong
   * @generated
   */
  EClass getBaseTypeLong();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.BaseTypeFloat <em>Base Type Float</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Base Type Float</em>'.
   * @see org.xtext.dop.clang.clang.BaseTypeFloat
   * @generated
   */
  EClass getBaseTypeFloat();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.BaseTypeDouble <em>Base Type Double</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Base Type Double</em>'.
   * @see org.xtext.dop.clang.clang.BaseTypeDouble
   * @generated
   */
  EClass getBaseTypeDouble();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.BaseTypeSigned <em>Base Type Signed</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Base Type Signed</em>'.
   * @see org.xtext.dop.clang.clang.BaseTypeSigned
   * @generated
   */
  EClass getBaseTypeSigned();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.BaseTypeUnsigned <em>Base Type Unsigned</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Base Type Unsigned</em>'.
   * @see org.xtext.dop.clang.clang.BaseTypeUnsigned
   * @generated
   */
  EClass getBaseTypeUnsigned();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.BaseTypeBool <em>Base Type Bool</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Base Type Bool</em>'.
   * @see org.xtext.dop.clang.clang.BaseTypeBool
   * @generated
   */
  EClass getBaseTypeBool();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.BaseTypeComplex <em>Base Type Complex</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Base Type Complex</em>'.
   * @see org.xtext.dop.clang.clang.BaseTypeComplex
   * @generated
   */
  EClass getBaseTypeComplex();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.BaseTypeImaginary <em>Base Type Imaginary</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Base Type Imaginary</em>'.
   * @see org.xtext.dop.clang.clang.BaseTypeImaginary
   * @generated
   */
  EClass getBaseTypeImaginary();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.StorageClassSpecifierExtern <em>Storage Class Specifier Extern</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Storage Class Specifier Extern</em>'.
   * @see org.xtext.dop.clang.clang.StorageClassSpecifierExtern
   * @generated
   */
  EClass getStorageClassSpecifierExtern();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.StorageClassSpecifierStatic <em>Storage Class Specifier Static</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Storage Class Specifier Static</em>'.
   * @see org.xtext.dop.clang.clang.StorageClassSpecifierStatic
   * @generated
   */
  EClass getStorageClassSpecifierStatic();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.StorageClassSpecifierThreadLocal <em>Storage Class Specifier Thread Local</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Storage Class Specifier Thread Local</em>'.
   * @see org.xtext.dop.clang.clang.StorageClassSpecifierThreadLocal
   * @generated
   */
  EClass getStorageClassSpecifierThreadLocal();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.StorageClassSpecifierAuto <em>Storage Class Specifier Auto</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Storage Class Specifier Auto</em>'.
   * @see org.xtext.dop.clang.clang.StorageClassSpecifierAuto
   * @generated
   */
  EClass getStorageClassSpecifierAuto();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.StorageClassSpecifierRegister <em>Storage Class Specifier Register</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Storage Class Specifier Register</em>'.
   * @see org.xtext.dop.clang.clang.StorageClassSpecifierRegister
   * @generated
   */
  EClass getStorageClassSpecifierRegister();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.TypeQualifierConst <em>Type Qualifier Const</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Type Qualifier Const</em>'.
   * @see org.xtext.dop.clang.clang.TypeQualifierConst
   * @generated
   */
  EClass getTypeQualifierConst();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.TypeQualifierVolatile <em>Type Qualifier Volatile</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Type Qualifier Volatile</em>'.
   * @see org.xtext.dop.clang.clang.TypeQualifierVolatile
   * @generated
   */
  EClass getTypeQualifierVolatile();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.TypeQualifierAtomic <em>Type Qualifier Atomic</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Type Qualifier Atomic</em>'.
   * @see org.xtext.dop.clang.clang.TypeQualifierAtomic
   * @generated
   */
  EClass getTypeQualifierAtomic();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.FunctionSpecifierInline <em>Function Specifier Inline</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Function Specifier Inline</em>'.
   * @see org.xtext.dop.clang.clang.FunctionSpecifierInline
   * @generated
   */
  EClass getFunctionSpecifierInline();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.FunctionSpecifierNoReturn <em>Function Specifier No Return</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Function Specifier No Return</em>'.
   * @see org.xtext.dop.clang.clang.FunctionSpecifierNoReturn
   * @generated
   */
  EClass getFunctionSpecifierNoReturn();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.AssignmentExpression <em>Assignment Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Assignment Expression</em>'.
   * @see org.xtext.dop.clang.clang.AssignmentExpression
   * @generated
   */
  EClass getAssignmentExpression();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.AssignmentExpression#getExp <em>Exp</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Exp</em>'.
   * @see org.xtext.dop.clang.clang.AssignmentExpression#getExp()
   * @see #getAssignmentExpression()
   * @generated
   */
  EReference getAssignmentExpression_Exp();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.dop.clang.clang.AssignmentExpression#getList <em>List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>List</em>'.
   * @see org.xtext.dop.clang.clang.AssignmentExpression#getList()
   * @see #getAssignmentExpression()
   * @generated
   */
  EReference getAssignmentExpression_List();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.ConditionalExpression <em>Conditional Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Conditional Expression</em>'.
   * @see org.xtext.dop.clang.clang.ConditionalExpression
   * @generated
   */
  EClass getConditionalExpression();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.ConditionalExpression#getIf <em>If</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>If</em>'.
   * @see org.xtext.dop.clang.clang.ConditionalExpression#getIf()
   * @see #getConditionalExpression()
   * @generated
   */
  EReference getConditionalExpression_If();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.ConditionalExpression#getYes <em>Yes</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Yes</em>'.
   * @see org.xtext.dop.clang.clang.ConditionalExpression#getYes()
   * @see #getConditionalExpression()
   * @generated
   */
  EReference getConditionalExpression_Yes();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.ConditionalExpression#getNo <em>No</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>No</em>'.
   * @see org.xtext.dop.clang.clang.ConditionalExpression#getNo()
   * @see #getConditionalExpression()
   * @generated
   */
  EReference getConditionalExpression_No();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.LogicalOrExpression <em>Logical Or Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Logical Or Expression</em>'.
   * @see org.xtext.dop.clang.clang.LogicalOrExpression
   * @generated
   */
  EClass getLogicalOrExpression();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.LogicalOrExpression#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see org.xtext.dop.clang.clang.LogicalOrExpression#getLeft()
   * @see #getLogicalOrExpression()
   * @generated
   */
  EReference getLogicalOrExpression_Left();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.dop.clang.clang.LogicalOrExpression#getOperand <em>Operand</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Operand</em>'.
   * @see org.xtext.dop.clang.clang.LogicalOrExpression#getOperand()
   * @see #getLogicalOrExpression()
   * @generated
   */
  EAttribute getLogicalOrExpression_Operand();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.LogicalOrExpression#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see org.xtext.dop.clang.clang.LogicalOrExpression#getRight()
   * @see #getLogicalOrExpression()
   * @generated
   */
  EReference getLogicalOrExpression_Right();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.LogicalAndExpression <em>Logical And Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Logical And Expression</em>'.
   * @see org.xtext.dop.clang.clang.LogicalAndExpression
   * @generated
   */
  EClass getLogicalAndExpression();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.LogicalAndExpression#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see org.xtext.dop.clang.clang.LogicalAndExpression#getLeft()
   * @see #getLogicalAndExpression()
   * @generated
   */
  EReference getLogicalAndExpression_Left();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.dop.clang.clang.LogicalAndExpression#getOperand <em>Operand</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Operand</em>'.
   * @see org.xtext.dop.clang.clang.LogicalAndExpression#getOperand()
   * @see #getLogicalAndExpression()
   * @generated
   */
  EAttribute getLogicalAndExpression_Operand();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.LogicalAndExpression#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see org.xtext.dop.clang.clang.LogicalAndExpression#getRight()
   * @see #getLogicalAndExpression()
   * @generated
   */
  EReference getLogicalAndExpression_Right();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.InclusiveOrExpression <em>Inclusive Or Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Inclusive Or Expression</em>'.
   * @see org.xtext.dop.clang.clang.InclusiveOrExpression
   * @generated
   */
  EClass getInclusiveOrExpression();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.InclusiveOrExpression#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see org.xtext.dop.clang.clang.InclusiveOrExpression#getLeft()
   * @see #getInclusiveOrExpression()
   * @generated
   */
  EReference getInclusiveOrExpression_Left();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.dop.clang.clang.InclusiveOrExpression#getOperand <em>Operand</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Operand</em>'.
   * @see org.xtext.dop.clang.clang.InclusiveOrExpression#getOperand()
   * @see #getInclusiveOrExpression()
   * @generated
   */
  EAttribute getInclusiveOrExpression_Operand();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.InclusiveOrExpression#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see org.xtext.dop.clang.clang.InclusiveOrExpression#getRight()
   * @see #getInclusiveOrExpression()
   * @generated
   */
  EReference getInclusiveOrExpression_Right();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.ExclusiveOrExpression <em>Exclusive Or Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Exclusive Or Expression</em>'.
   * @see org.xtext.dop.clang.clang.ExclusiveOrExpression
   * @generated
   */
  EClass getExclusiveOrExpression();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.ExclusiveOrExpression#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see org.xtext.dop.clang.clang.ExclusiveOrExpression#getLeft()
   * @see #getExclusiveOrExpression()
   * @generated
   */
  EReference getExclusiveOrExpression_Left();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.dop.clang.clang.ExclusiveOrExpression#getOperand <em>Operand</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Operand</em>'.
   * @see org.xtext.dop.clang.clang.ExclusiveOrExpression#getOperand()
   * @see #getExclusiveOrExpression()
   * @generated
   */
  EAttribute getExclusiveOrExpression_Operand();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.ExclusiveOrExpression#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see org.xtext.dop.clang.clang.ExclusiveOrExpression#getRight()
   * @see #getExclusiveOrExpression()
   * @generated
   */
  EReference getExclusiveOrExpression_Right();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.AndExpression <em>And Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>And Expression</em>'.
   * @see org.xtext.dop.clang.clang.AndExpression
   * @generated
   */
  EClass getAndExpression();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.AndExpression#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see org.xtext.dop.clang.clang.AndExpression#getLeft()
   * @see #getAndExpression()
   * @generated
   */
  EReference getAndExpression_Left();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.dop.clang.clang.AndExpression#getOperand <em>Operand</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Operand</em>'.
   * @see org.xtext.dop.clang.clang.AndExpression#getOperand()
   * @see #getAndExpression()
   * @generated
   */
  EAttribute getAndExpression_Operand();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.AndExpression#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see org.xtext.dop.clang.clang.AndExpression#getRight()
   * @see #getAndExpression()
   * @generated
   */
  EReference getAndExpression_Right();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.EqualityExpression <em>Equality Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Equality Expression</em>'.
   * @see org.xtext.dop.clang.clang.EqualityExpression
   * @generated
   */
  EClass getEqualityExpression();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.EqualityExpression#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see org.xtext.dop.clang.clang.EqualityExpression#getLeft()
   * @see #getEqualityExpression()
   * @generated
   */
  EReference getEqualityExpression_Left();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.dop.clang.clang.EqualityExpression#getOperand <em>Operand</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Operand</em>'.
   * @see org.xtext.dop.clang.clang.EqualityExpression#getOperand()
   * @see #getEqualityExpression()
   * @generated
   */
  EAttribute getEqualityExpression_Operand();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.EqualityExpression#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see org.xtext.dop.clang.clang.EqualityExpression#getRight()
   * @see #getEqualityExpression()
   * @generated
   */
  EReference getEqualityExpression_Right();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.RelationalExpression <em>Relational Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Relational Expression</em>'.
   * @see org.xtext.dop.clang.clang.RelationalExpression
   * @generated
   */
  EClass getRelationalExpression();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.RelationalExpression#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see org.xtext.dop.clang.clang.RelationalExpression#getLeft()
   * @see #getRelationalExpression()
   * @generated
   */
  EReference getRelationalExpression_Left();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.dop.clang.clang.RelationalExpression#getOperand <em>Operand</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Operand</em>'.
   * @see org.xtext.dop.clang.clang.RelationalExpression#getOperand()
   * @see #getRelationalExpression()
   * @generated
   */
  EAttribute getRelationalExpression_Operand();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.RelationalExpression#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see org.xtext.dop.clang.clang.RelationalExpression#getRight()
   * @see #getRelationalExpression()
   * @generated
   */
  EReference getRelationalExpression_Right();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.ShiftExpression <em>Shift Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Shift Expression</em>'.
   * @see org.xtext.dop.clang.clang.ShiftExpression
   * @generated
   */
  EClass getShiftExpression();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.ShiftExpression#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see org.xtext.dop.clang.clang.ShiftExpression#getLeft()
   * @see #getShiftExpression()
   * @generated
   */
  EReference getShiftExpression_Left();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.dop.clang.clang.ShiftExpression#getOperand <em>Operand</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Operand</em>'.
   * @see org.xtext.dop.clang.clang.ShiftExpression#getOperand()
   * @see #getShiftExpression()
   * @generated
   */
  EAttribute getShiftExpression_Operand();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.ShiftExpression#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see org.xtext.dop.clang.clang.ShiftExpression#getRight()
   * @see #getShiftExpression()
   * @generated
   */
  EReference getShiftExpression_Right();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.AdditiveExpression <em>Additive Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Additive Expression</em>'.
   * @see org.xtext.dop.clang.clang.AdditiveExpression
   * @generated
   */
  EClass getAdditiveExpression();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.AdditiveExpression#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see org.xtext.dop.clang.clang.AdditiveExpression#getLeft()
   * @see #getAdditiveExpression()
   * @generated
   */
  EReference getAdditiveExpression_Left();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.dop.clang.clang.AdditiveExpression#getOperand <em>Operand</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Operand</em>'.
   * @see org.xtext.dop.clang.clang.AdditiveExpression#getOperand()
   * @see #getAdditiveExpression()
   * @generated
   */
  EAttribute getAdditiveExpression_Operand();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.AdditiveExpression#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see org.xtext.dop.clang.clang.AdditiveExpression#getRight()
   * @see #getAdditiveExpression()
   * @generated
   */
  EReference getAdditiveExpression_Right();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.MultiplicativeExpression <em>Multiplicative Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Multiplicative Expression</em>'.
   * @see org.xtext.dop.clang.clang.MultiplicativeExpression
   * @generated
   */
  EClass getMultiplicativeExpression();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.MultiplicativeExpression#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see org.xtext.dop.clang.clang.MultiplicativeExpression#getLeft()
   * @see #getMultiplicativeExpression()
   * @generated
   */
  EReference getMultiplicativeExpression_Left();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.dop.clang.clang.MultiplicativeExpression#getOperand <em>Operand</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Operand</em>'.
   * @see org.xtext.dop.clang.clang.MultiplicativeExpression#getOperand()
   * @see #getMultiplicativeExpression()
   * @generated
   */
  EAttribute getMultiplicativeExpression_Operand();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.MultiplicativeExpression#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see org.xtext.dop.clang.clang.MultiplicativeExpression#getRight()
   * @see #getMultiplicativeExpression()
   * @generated
   */
  EReference getMultiplicativeExpression_Right();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.ExpressionSimple <em>Expression Simple</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Expression Simple</em>'.
   * @see org.xtext.dop.clang.clang.ExpressionSimple
   * @generated
   */
  EClass getExpressionSimple();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.ExpressionSimple#getExp <em>Exp</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Exp</em>'.
   * @see org.xtext.dop.clang.clang.ExpressionSimple#getExp()
   * @see #getExpressionSimple()
   * @generated
   */
  EReference getExpressionSimple_Exp();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.dop.clang.clang.ExpressionSimple#getNext <em>Next</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Next</em>'.
   * @see org.xtext.dop.clang.clang.ExpressionSimple#getNext()
   * @see #getExpressionSimple()
   * @generated
   */
  EReference getExpressionSimple_Next();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.ExpressionComplex <em>Expression Complex</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Expression Complex</em>'.
   * @see org.xtext.dop.clang.clang.ExpressionComplex
   * @generated
   */
  EClass getExpressionComplex();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.dop.clang.clang.ExpressionComplex#getExps <em>Exps</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Exps</em>'.
   * @see org.xtext.dop.clang.clang.ExpressionComplex#getExps()
   * @see #getExpressionComplex()
   * @generated
   */
  EReference getExpressionComplex_Exps();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.dop.clang.clang.ExpressionComplex#getNext <em>Next</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Next</em>'.
   * @see org.xtext.dop.clang.clang.ExpressionComplex#getNext()
   * @see #getExpressionComplex()
   * @generated
   */
  EReference getExpressionComplex_Next();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.ExpressionIncrement <em>Expression Increment</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Expression Increment</em>'.
   * @see org.xtext.dop.clang.clang.ExpressionIncrement
   * @generated
   */
  EClass getExpressionIncrement();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.ExpressionIncrement#getContent <em>Content</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Content</em>'.
   * @see org.xtext.dop.clang.clang.ExpressionIncrement#getContent()
   * @see #getExpressionIncrement()
   * @generated
   */
  EReference getExpressionIncrement_Content();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.ExpressionDecrement <em>Expression Decrement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Expression Decrement</em>'.
   * @see org.xtext.dop.clang.clang.ExpressionDecrement
   * @generated
   */
  EClass getExpressionDecrement();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.ExpressionDecrement#getContent <em>Content</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Content</em>'.
   * @see org.xtext.dop.clang.clang.ExpressionDecrement#getContent()
   * @see #getExpressionDecrement()
   * @generated
   */
  EReference getExpressionDecrement_Content();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.ExpressionSizeof <em>Expression Sizeof</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Expression Sizeof</em>'.
   * @see org.xtext.dop.clang.clang.ExpressionSizeof
   * @generated
   */
  EClass getExpressionSizeof();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.ExpressionSizeof#getContent <em>Content</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Content</em>'.
   * @see org.xtext.dop.clang.clang.ExpressionSizeof#getContent()
   * @see #getExpressionSizeof()
   * @generated
   */
  EReference getExpressionSizeof_Content();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.ExpressionSizeof#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see org.xtext.dop.clang.clang.ExpressionSizeof#getType()
   * @see #getExpressionSizeof()
   * @generated
   */
  EReference getExpressionSizeof_Type();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.ExpressionUnaryOp <em>Expression Unary Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Expression Unary Op</em>'.
   * @see org.xtext.dop.clang.clang.ExpressionUnaryOp
   * @generated
   */
  EClass getExpressionUnaryOp();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.dop.clang.clang.ExpressionUnaryOp#getOp <em>Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Op</em>'.
   * @see org.xtext.dop.clang.clang.ExpressionUnaryOp#getOp()
   * @see #getExpressionUnaryOp()
   * @generated
   */
  EAttribute getExpressionUnaryOp_Op();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.ExpressionUnaryOp#getContent <em>Content</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Content</em>'.
   * @see org.xtext.dop.clang.clang.ExpressionUnaryOp#getContent()
   * @see #getExpressionUnaryOp()
   * @generated
   */
  EReference getExpressionUnaryOp_Content();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.ExpressionAlign <em>Expression Align</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Expression Align</em>'.
   * @see org.xtext.dop.clang.clang.ExpressionAlign
   * @generated
   */
  EClass getExpressionAlign();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.ExpressionAlign#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see org.xtext.dop.clang.clang.ExpressionAlign#getType()
   * @see #getExpressionAlign()
   * @generated
   */
  EReference getExpressionAlign_Type();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.ExpressionCast <em>Expression Cast</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Expression Cast</em>'.
   * @see org.xtext.dop.clang.clang.ExpressionCast
   * @generated
   */
  EClass getExpressionCast();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.ExpressionCast#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see org.xtext.dop.clang.clang.ExpressionCast#getType()
   * @see #getExpressionCast()
   * @generated
   */
  EReference getExpressionCast_Type();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.ExpressionCast#getExp <em>Exp</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Exp</em>'.
   * @see org.xtext.dop.clang.clang.ExpressionCast#getExp()
   * @see #getExpressionCast()
   * @generated
   */
  EReference getExpressionCast_Exp();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.ExpressionIdentifier <em>Expression Identifier</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Expression Identifier</em>'.
   * @see org.xtext.dop.clang.clang.ExpressionIdentifier
   * @generated
   */
  EClass getExpressionIdentifier();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.dop.clang.clang.ExpressionIdentifier#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.xtext.dop.clang.clang.ExpressionIdentifier#getName()
   * @see #getExpressionIdentifier()
   * @generated
   */
  EAttribute getExpressionIdentifier_Name();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.ExpressionString <em>Expression String</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Expression String</em>'.
   * @see org.xtext.dop.clang.clang.ExpressionString
   * @generated
   */
  EClass getExpressionString();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.ExpressionString#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Value</em>'.
   * @see org.xtext.dop.clang.clang.ExpressionString#getValue()
   * @see #getExpressionString()
   * @generated
   */
  EReference getExpressionString_Value();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.ExpressionInteger <em>Expression Integer</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Expression Integer</em>'.
   * @see org.xtext.dop.clang.clang.ExpressionInteger
   * @generated
   */
  EClass getExpressionInteger();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.dop.clang.clang.ExpressionInteger#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.xtext.dop.clang.clang.ExpressionInteger#getValue()
   * @see #getExpressionInteger()
   * @generated
   */
  EAttribute getExpressionInteger_Value();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.ExpressionFloat <em>Expression Float</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Expression Float</em>'.
   * @see org.xtext.dop.clang.clang.ExpressionFloat
   * @generated
   */
  EClass getExpressionFloat();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.dop.clang.clang.ExpressionFloat#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.xtext.dop.clang.clang.ExpressionFloat#getValue()
   * @see #getExpressionFloat()
   * @generated
   */
  EAttribute getExpressionFloat_Value();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.ExpressionSubExpression <em>Expression Sub Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Expression Sub Expression</em>'.
   * @see org.xtext.dop.clang.clang.ExpressionSubExpression
   * @generated
   */
  EClass getExpressionSubExpression();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.ExpressionSubExpression#getPar <em>Par</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Par</em>'.
   * @see org.xtext.dop.clang.clang.ExpressionSubExpression#getPar()
   * @see #getExpressionSubExpression()
   * @generated
   */
  EReference getExpressionSubExpression_Par();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.ExpressionGenericSelection <em>Expression Generic Selection</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Expression Generic Selection</em>'.
   * @see org.xtext.dop.clang.clang.ExpressionGenericSelection
   * @generated
   */
  EClass getExpressionGenericSelection();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.ExpressionGenericSelection#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see org.xtext.dop.clang.clang.ExpressionGenericSelection#getLeft()
   * @see #getExpressionGenericSelection()
   * @generated
   */
  EReference getExpressionGenericSelection_Left();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.dop.clang.clang.ExpressionGenericSelection#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Right</em>'.
   * @see org.xtext.dop.clang.clang.ExpressionGenericSelection#getRight()
   * @see #getExpressionGenericSelection()
   * @generated
   */
  EReference getExpressionGenericSelection_Right();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.GenericAssociationCase <em>Generic Association Case</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Generic Association Case</em>'.
   * @see org.xtext.dop.clang.clang.GenericAssociationCase
   * @generated
   */
  EClass getGenericAssociationCase();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.GenericAssociationCase#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see org.xtext.dop.clang.clang.GenericAssociationCase#getType()
   * @see #getGenericAssociationCase()
   * @generated
   */
  EReference getGenericAssociationCase_Type();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.GenericAssociationDefault <em>Generic Association Default</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Generic Association Default</em>'.
   * @see org.xtext.dop.clang.clang.GenericAssociationDefault
   * @generated
   */
  EClass getGenericAssociationDefault();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.PostfixExpressionPostfixExpression <em>Postfix Expression Postfix Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Postfix Expression Postfix Expression</em>'.
   * @see org.xtext.dop.clang.clang.PostfixExpressionPostfixExpression
   * @generated
   */
  EClass getPostfixExpressionPostfixExpression();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.PostfixExpressionPostfixExpression#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Value</em>'.
   * @see org.xtext.dop.clang.clang.PostfixExpressionPostfixExpression#getValue()
   * @see #getPostfixExpressionPostfixExpression()
   * @generated
   */
  EReference getPostfixExpressionPostfixExpression_Value();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.PostfixExpressionPostfixArgumentList <em>Postfix Expression Postfix Argument List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Postfix Expression Postfix Argument List</em>'.
   * @see org.xtext.dop.clang.clang.PostfixExpressionPostfixArgumentList
   * @generated
   */
  EClass getPostfixExpressionPostfixArgumentList();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.dop.clang.clang.PostfixExpressionPostfixArgumentList#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Left</em>'.
   * @see org.xtext.dop.clang.clang.PostfixExpressionPostfixArgumentList#getLeft()
   * @see #getPostfixExpressionPostfixArgumentList()
   * @generated
   */
  EReference getPostfixExpressionPostfixArgumentList_Left();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.dop.clang.clang.PostfixExpressionPostfixArgumentList#getNext <em>Next</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Next</em>'.
   * @see org.xtext.dop.clang.clang.PostfixExpressionPostfixArgumentList#getNext()
   * @see #getPostfixExpressionPostfixArgumentList()
   * @generated
   */
  EReference getPostfixExpressionPostfixArgumentList_Next();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.PostfixExpressionPostfixAccess <em>Postfix Expression Postfix Access</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Postfix Expression Postfix Access</em>'.
   * @see org.xtext.dop.clang.clang.PostfixExpressionPostfixAccess
   * @generated
   */
  EClass getPostfixExpressionPostfixAccess();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.dop.clang.clang.PostfixExpressionPostfixAccess#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.xtext.dop.clang.clang.PostfixExpressionPostfixAccess#getValue()
   * @see #getPostfixExpressionPostfixAccess()
   * @generated
   */
  EAttribute getPostfixExpressionPostfixAccess_Value();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.PostfixExpressionPostfixAccessPtr <em>Postfix Expression Postfix Access Ptr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Postfix Expression Postfix Access Ptr</em>'.
   * @see org.xtext.dop.clang.clang.PostfixExpressionPostfixAccessPtr
   * @generated
   */
  EClass getPostfixExpressionPostfixAccessPtr();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.dop.clang.clang.PostfixExpressionPostfixAccessPtr#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.xtext.dop.clang.clang.PostfixExpressionPostfixAccessPtr#getValue()
   * @see #getPostfixExpressionPostfixAccessPtr()
   * @generated
   */
  EAttribute getPostfixExpressionPostfixAccessPtr_Value();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.PostfixExpressionPostfixIncrement <em>Postfix Expression Postfix Increment</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Postfix Expression Postfix Increment</em>'.
   * @see org.xtext.dop.clang.clang.PostfixExpressionPostfixIncrement
   * @generated
   */
  EClass getPostfixExpressionPostfixIncrement();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.PostfixExpressionPostfixDecrement <em>Postfix Expression Postfix Decrement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Postfix Expression Postfix Decrement</em>'.
   * @see org.xtext.dop.clang.clang.PostfixExpressionPostfixDecrement
   * @generated
   */
  EClass getPostfixExpressionPostfixDecrement();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.StatementDeclaration <em>Statement Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Statement Declaration</em>'.
   * @see org.xtext.dop.clang.clang.StatementDeclaration
   * @generated
   */
  EClass getStatementDeclaration();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.StatementDeclaration#getDecl <em>Decl</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Decl</em>'.
   * @see org.xtext.dop.clang.clang.StatementDeclaration#getDecl()
   * @see #getStatementDeclaration()
   * @generated
   */
  EReference getStatementDeclaration_Decl();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.StatementInnerStatement <em>Statement Inner Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Statement Inner Statement</em>'.
   * @see org.xtext.dop.clang.clang.StatementInnerStatement
   * @generated
   */
  EClass getStatementInnerStatement();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.StatementInnerStatement#getStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statement</em>'.
   * @see org.xtext.dop.clang.clang.StatementInnerStatement#getStatement()
   * @see #getStatementInnerStatement()
   * @generated
   */
  EReference getStatementInnerStatement_Statement();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.LabeledStatementCase <em>Labeled Statement Case</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Labeled Statement Case</em>'.
   * @see org.xtext.dop.clang.clang.LabeledStatementCase
   * @generated
   */
  EClass getLabeledStatementCase();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.LabeledStatementCase#getExp <em>Exp</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Exp</em>'.
   * @see org.xtext.dop.clang.clang.LabeledStatementCase#getExp()
   * @see #getLabeledStatementCase()
   * @generated
   */
  EReference getLabeledStatementCase_Exp();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.LabeledStatementDefault <em>Labeled Statement Default</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Labeled Statement Default</em>'.
   * @see org.xtext.dop.clang.clang.LabeledStatementDefault
   * @generated
   */
  EClass getLabeledStatementDefault();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.GoToPrefixStatement <em>Go To Prefix Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Go To Prefix Statement</em>'.
   * @see org.xtext.dop.clang.clang.GoToPrefixStatement
   * @generated
   */
  EClass getGoToPrefixStatement();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.dop.clang.clang.GoToPrefixStatement#getGoto <em>Goto</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Goto</em>'.
   * @see org.xtext.dop.clang.clang.GoToPrefixStatement#getGoto()
   * @see #getGoToPrefixStatement()
   * @generated
   */
  EAttribute getGoToPrefixStatement_Goto();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.GoToPrefixStatement#getStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statement</em>'.
   * @see org.xtext.dop.clang.clang.GoToPrefixStatement#getStatement()
   * @see #getGoToPrefixStatement()
   * @generated
   */
  EReference getGoToPrefixStatement_Statement();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.SelectionStatementIf <em>Selection Statement If</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Selection Statement If</em>'.
   * @see org.xtext.dop.clang.clang.SelectionStatementIf
   * @generated
   */
  EClass getSelectionStatementIf();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.SelectionStatementIf#getThen_statement <em>Then statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Then statement</em>'.
   * @see org.xtext.dop.clang.clang.SelectionStatementIf#getThen_statement()
   * @see #getSelectionStatementIf()
   * @generated
   */
  EReference getSelectionStatementIf_Then_statement();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.SelectionStatementIf#getElse_statement <em>Else statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Else statement</em>'.
   * @see org.xtext.dop.clang.clang.SelectionStatementIf#getElse_statement()
   * @see #getSelectionStatementIf()
   * @generated
   */
  EReference getSelectionStatementIf_Else_statement();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.SelectionStatementSwitch <em>Selection Statement Switch</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Selection Statement Switch</em>'.
   * @see org.xtext.dop.clang.clang.SelectionStatementSwitch
   * @generated
   */
  EClass getSelectionStatementSwitch();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.dop.clang.clang.SelectionStatementSwitch#getContent <em>Content</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Content</em>'.
   * @see org.xtext.dop.clang.clang.SelectionStatementSwitch#getContent()
   * @see #getSelectionStatementSwitch()
   * @generated
   */
  EReference getSelectionStatementSwitch_Content();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.IterationStatementWhile <em>Iteration Statement While</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Iteration Statement While</em>'.
   * @see org.xtext.dop.clang.clang.IterationStatementWhile
   * @generated
   */
  EClass getIterationStatementWhile();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.IterationStatementWhile#getExp <em>Exp</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Exp</em>'.
   * @see org.xtext.dop.clang.clang.IterationStatementWhile#getExp()
   * @see #getIterationStatementWhile()
   * @generated
   */
  EReference getIterationStatementWhile_Exp();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.IterationStatementDo <em>Iteration Statement Do</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Iteration Statement Do</em>'.
   * @see org.xtext.dop.clang.clang.IterationStatementDo
   * @generated
   */
  EClass getIterationStatementDo();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.IterationStatementDo#getExp <em>Exp</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Exp</em>'.
   * @see org.xtext.dop.clang.clang.IterationStatementDo#getExp()
   * @see #getIterationStatementDo()
   * @generated
   */
  EReference getIterationStatementDo_Exp();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.IterationStatementFor <em>Iteration Statement For</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Iteration Statement For</em>'.
   * @see org.xtext.dop.clang.clang.IterationStatementFor
   * @generated
   */
  EClass getIterationStatementFor();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.IterationStatementFor#getField1 <em>Field1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Field1</em>'.
   * @see org.xtext.dop.clang.clang.IterationStatementFor#getField1()
   * @see #getIterationStatementFor()
   * @generated
   */
  EReference getIterationStatementFor_Field1();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.IterationStatementFor#getDecl <em>Decl</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Decl</em>'.
   * @see org.xtext.dop.clang.clang.IterationStatementFor#getDecl()
   * @see #getIterationStatementFor()
   * @generated
   */
  EReference getIterationStatementFor_Decl();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.IterationStatementFor#getField2 <em>Field2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Field2</em>'.
   * @see org.xtext.dop.clang.clang.IterationStatementFor#getField2()
   * @see #getIterationStatementFor()
   * @generated
   */
  EReference getIterationStatementFor_Field2();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.IterationStatementFor#getField3 <em>Field3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Field3</em>'.
   * @see org.xtext.dop.clang.clang.IterationStatementFor#getField3()
   * @see #getIterationStatementFor()
   * @generated
   */
  EReference getIterationStatementFor_Field3();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.dop.clang.clang.IterationStatementFor#getNext <em>Next</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Next</em>'.
   * @see org.xtext.dop.clang.clang.IterationStatementFor#getNext()
   * @see #getIterationStatementFor()
   * @generated
   */
  EReference getIterationStatementFor_Next();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.JumpStatementGoto <em>Jump Statement Goto</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Jump Statement Goto</em>'.
   * @see org.xtext.dop.clang.clang.JumpStatementGoto
   * @generated
   */
  EClass getJumpStatementGoto();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.dop.clang.clang.JumpStatementGoto#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.xtext.dop.clang.clang.JumpStatementGoto#getName()
   * @see #getJumpStatementGoto()
   * @generated
   */
  EAttribute getJumpStatementGoto_Name();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.JumpStatementContinue <em>Jump Statement Continue</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Jump Statement Continue</em>'.
   * @see org.xtext.dop.clang.clang.JumpStatementContinue
   * @generated
   */
  EClass getJumpStatementContinue();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.JumpStatementBreak <em>Jump Statement Break</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Jump Statement Break</em>'.
   * @see org.xtext.dop.clang.clang.JumpStatementBreak
   * @generated
   */
  EClass getJumpStatementBreak();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.clang.clang.JumpStatementReturn <em>Jump Statement Return</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Jump Statement Return</em>'.
   * @see org.xtext.dop.clang.clang.JumpStatementReturn
   * @generated
   */
  EClass getJumpStatementReturn();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.clang.clang.JumpStatementReturn#getExp <em>Exp</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Exp</em>'.
   * @see org.xtext.dop.clang.clang.JumpStatementReturn#getExp()
   * @see #getJumpStatementReturn()
   * @generated
   */
  EReference getJumpStatementReturn_Exp();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  ClangFactory getClangFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.ModelImpl <em>Model</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.ModelImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getModel()
     * @generated
     */
    EClass MODEL = eINSTANCE.getModel();

    /**
     * The meta object literal for the '<em><b>Includes Std</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MODEL__INCLUDES_STD = eINSTANCE.getModel_IncludesStd();

    /**
     * The meta object literal for the '<em><b>Includes File</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MODEL__INCLUDES_FILE = eINSTANCE.getModel_IncludesFile();

    /**
     * The meta object literal for the '<em><b>Declarations</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MODEL__DECLARATIONS = eINSTANCE.getModel_Declarations();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.StandardIncludeImpl <em>Standard Include</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.StandardIncludeImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getStandardInclude()
     * @generated
     */
    EClass STANDARD_INCLUDE = eINSTANCE.getStandardInclude();

    /**
     * The meta object literal for the '<em><b>Std</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference STANDARD_INCLUDE__STD = eINSTANCE.getStandardInclude_Std();

    /**
     * The meta object literal for the '<em><b>Wild</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute STANDARD_INCLUDE__WILD = eINSTANCE.getStandardInclude_Wild();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.FileIncludeImpl <em>File Include</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.FileIncludeImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getFileInclude()
     * @generated
     */
    EClass FILE_INCLUDE = eINSTANCE.getFileInclude();

    /**
     * The meta object literal for the '<em><b>File</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute FILE_INCLUDE__FILE = eINSTANCE.getFileInclude_File();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.SourceImpl <em>Source</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.SourceImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getSource()
     * @generated
     */
    EClass SOURCE = eINSTANCE.getSource();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SOURCE__NAME = eINSTANCE.getSource_Name();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.DeclarationImpl <em>Declaration</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.DeclarationImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getDeclaration()
     * @generated
     */
    EClass DECLARATION = eINSTANCE.getDeclaration();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.TypeAliasDeclarationImpl <em>Type Alias Declaration</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.TypeAliasDeclarationImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getTypeAliasDeclaration()
     * @generated
     */
    EClass TYPE_ALIAS_DECLARATION = eINSTANCE.getTypeAliasDeclaration();

    /**
     * The meta object literal for the '<em><b>Tdecl</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TYPE_ALIAS_DECLARATION__TDECL = eINSTANCE.getTypeAliasDeclaration_Tdecl();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.GenericSpecifiedDeclarationImpl <em>Generic Specified Declaration</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.GenericSpecifiedDeclarationImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getGenericSpecifiedDeclaration()
     * @generated
     */
    EClass GENERIC_SPECIFIED_DECLARATION = eINSTANCE.getGenericSpecifiedDeclaration();

    /**
     * The meta object literal for the '<em><b>Specs</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference GENERIC_SPECIFIED_DECLARATION__SPECS = eINSTANCE.getGenericSpecifiedDeclaration_Specs();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference GENERIC_SPECIFIED_DECLARATION__TYPE = eINSTANCE.getGenericSpecifiedDeclaration_Type();

    /**
     * The meta object literal for the '<em><b>Decl</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference GENERIC_SPECIFIED_DECLARATION__DECL = eINSTANCE.getGenericSpecifiedDeclaration_Decl();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.GenericDeclarationElementsImpl <em>Generic Declaration Elements</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.GenericDeclarationElementsImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getGenericDeclarationElements()
     * @generated
     */
    EClass GENERIC_DECLARATION_ELEMENTS = eINSTANCE.getGenericDeclarationElements();

    /**
     * The meta object literal for the '<em><b>Els</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference GENERIC_DECLARATION_ELEMENTS__ELS = eINSTANCE.getGenericDeclarationElements_Els();

    /**
     * The meta object literal for the '<em><b>Next</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference GENERIC_DECLARATION_ELEMENTS__NEXT = eINSTANCE.getGenericDeclarationElements_Next();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.GenericDeclarationElementImpl <em>Generic Declaration Element</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.GenericDeclarationElementImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getGenericDeclarationElement()
     * @generated
     */
    EClass GENERIC_DECLARATION_ELEMENT = eINSTANCE.getGenericDeclarationElement();

    /**
     * The meta object literal for the '<em><b>Id</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference GENERIC_DECLARATION_ELEMENT__ID = eINSTANCE.getGenericDeclarationElement_Id();

    /**
     * The meta object literal for the '<em><b>Init</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference GENERIC_DECLARATION_ELEMENT__INIT = eINSTANCE.getGenericDeclarationElement_Init();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.GenericDeclarationIdImpl <em>Generic Declaration Id</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.GenericDeclarationIdImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getGenericDeclarationId()
     * @generated
     */
    EClass GENERIC_DECLARATION_ID = eINSTANCE.getGenericDeclarationId();

    /**
     * The meta object literal for the '<em><b>Inner</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference GENERIC_DECLARATION_ID__INNER = eINSTANCE.getGenericDeclarationId_Inner();

    /**
     * The meta object literal for the '<em><b>Posts</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference GENERIC_DECLARATION_ID__POSTS = eINSTANCE.getGenericDeclarationId_Posts();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.GenericDeclarationInnerIdImpl <em>Generic Declaration Inner Id</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.GenericDeclarationInnerIdImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getGenericDeclarationInnerId()
     * @generated
     */
    EClass GENERIC_DECLARATION_INNER_ID = eINSTANCE.getGenericDeclarationInnerId();

    /**
     * The meta object literal for the '<em><b>Inner</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference GENERIC_DECLARATION_INNER_ID__INNER = eINSTANCE.getGenericDeclarationInnerId_Inner();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.GenericDeclarationIdPostfixImpl <em>Generic Declaration Id Postfix</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.GenericDeclarationIdPostfixImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getGenericDeclarationIdPostfix()
     * @generated
     */
    EClass GENERIC_DECLARATION_ID_POSTFIX = eINSTANCE.getGenericDeclarationIdPostfix();

    /**
     * The meta object literal for the '<em><b>Array</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference GENERIC_DECLARATION_ID_POSTFIX__ARRAY = eINSTANCE.getGenericDeclarationIdPostfix_Array();

    /**
     * The meta object literal for the '<em><b>Function</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference GENERIC_DECLARATION_ID_POSTFIX__FUNCTION = eINSTANCE.getGenericDeclarationIdPostfix_Function();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.ArrayPostfixImpl <em>Array Postfix</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.ArrayPostfixImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getArrayPostfix()
     * @generated
     */
    EClass ARRAY_POSTFIX = eINSTANCE.getArrayPostfix();

    /**
     * The meta object literal for the '<em><b>Star</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ARRAY_POSTFIX__STAR = eINSTANCE.getArrayPostfix_Star();

    /**
     * The meta object literal for the '<em><b>Exp</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ARRAY_POSTFIX__EXP = eINSTANCE.getArrayPostfix_Exp();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.ParametersPostfixImpl <em>Parameters Postfix</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.ParametersPostfixImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getParametersPostfix()
     * @generated
     */
    EClass PARAMETERS_POSTFIX = eINSTANCE.getParametersPostfix();

    /**
     * The meta object literal for the '<em><b>Els</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PARAMETERS_POSTFIX__ELS = eINSTANCE.getParametersPostfix_Els();

    /**
     * The meta object literal for the '<em><b>Next</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PARAMETERS_POSTFIX__NEXT = eINSTANCE.getParametersPostfix_Next();

    /**
     * The meta object literal for the '<em><b>More</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PARAMETERS_POSTFIX__MORE = eINSTANCE.getParametersPostfix_More();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.NextParameterImpl <em>Next Parameter</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.NextParameterImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getNextParameter()
     * @generated
     */
    EClass NEXT_PARAMETER = eINSTANCE.getNextParameter();

    /**
     * The meta object literal for the '<em><b>Els</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference NEXT_PARAMETER__ELS = eINSTANCE.getNextParameter_Els();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.ParameterImpl <em>Parameter</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.ParameterImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getParameter()
     * @generated
     */
    EClass PARAMETER = eINSTANCE.getParameter();

    /**
     * The meta object literal for the '<em><b>Specs</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PARAMETER__SPECS = eINSTANCE.getParameter_Specs();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PARAMETER__TYPE = eINSTANCE.getParameter_Type();

    /**
     * The meta object literal for the '<em><b>Id</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PARAMETER__ID = eINSTANCE.getParameter_Id();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.GenericDeclarationInitializerImpl <em>Generic Declaration Initializer</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.GenericDeclarationInitializerImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getGenericDeclarationInitializer()
     * @generated
     */
    EClass GENERIC_DECLARATION_INITIALIZER = eINSTANCE.getGenericDeclarationInitializer();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.SpecifiedTypeImpl <em>Specified Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.SpecifiedTypeImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getSpecifiedType()
     * @generated
     */
    EClass SPECIFIED_TYPE = eINSTANCE.getSpecifiedType();

    /**
     * The meta object literal for the '<em><b>Par</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SPECIFIED_TYPE__PAR = eINSTANCE.getSpecifiedType_Par();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.StaticAssertDeclarationImpl <em>Static Assert Declaration</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.StaticAssertDeclarationImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getStaticAssertDeclaration()
     * @generated
     */
    EClass STATIC_ASSERT_DECLARATION = eINSTANCE.getStaticAssertDeclaration();

    /**
     * The meta object literal for the '<em><b>Exp</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference STATIC_ASSERT_DECLARATION__EXP = eINSTANCE.getStaticAssertDeclaration_Exp();

    /**
     * The meta object literal for the '<em><b>String</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute STATIC_ASSERT_DECLARATION__STRING = eINSTANCE.getStaticAssertDeclaration_String();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.SimpleTypeImpl <em>Simple Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.SimpleTypeImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getSimpleType()
     * @generated
     */
    EClass SIMPLE_TYPE = eINSTANCE.getSimpleType();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.BaseTypeImpl <em>Base Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.BaseTypeImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getBaseType()
     * @generated
     */
    EClass BASE_TYPE = eINSTANCE.getBaseType();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.StructOrUnionTypeImpl <em>Struct Or Union Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.StructOrUnionTypeImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getStructOrUnionType()
     * @generated
     */
    EClass STRUCT_OR_UNION_TYPE = eINSTANCE.getStructOrUnionType();

    /**
     * The meta object literal for the '<em><b>Kind</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute STRUCT_OR_UNION_TYPE__KIND = eINSTANCE.getStructOrUnionType_Kind();

    /**
     * The meta object literal for the '<em><b>Content</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference STRUCT_OR_UNION_TYPE__CONTENT = eINSTANCE.getStructOrUnionType_Content();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute STRUCT_OR_UNION_TYPE__NAME = eINSTANCE.getStructOrUnionType_Name();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.EnumTypeImpl <em>Enum Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.EnumTypeImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getEnumType()
     * @generated
     */
    EClass ENUM_TYPE = eINSTANCE.getEnumType();

    /**
     * The meta object literal for the '<em><b>Content</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ENUM_TYPE__CONTENT = eINSTANCE.getEnumType_Content();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ENUM_TYPE__NAME = eINSTANCE.getEnumType_Name();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.EnumeratorListImpl <em>Enumerator List</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.EnumeratorListImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getEnumeratorList()
     * @generated
     */
    EClass ENUMERATOR_LIST = eINSTANCE.getEnumeratorList();

    /**
     * The meta object literal for the '<em><b>Elements</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ENUMERATOR_LIST__ELEMENTS = eINSTANCE.getEnumeratorList_Elements();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.EnumeratorImpl <em>Enumerator</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.EnumeratorImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getEnumerator()
     * @generated
     */
    EClass ENUMERATOR = eINSTANCE.getEnumerator();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ENUMERATOR__NAME = eINSTANCE.getEnumerator_Name();

    /**
     * The meta object literal for the '<em><b>Exp</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ENUMERATOR__EXP = eINSTANCE.getEnumerator_Exp();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.EnumerationConstantImpl <em>Enumeration Constant</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.EnumerationConstantImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getEnumerationConstant()
     * @generated
     */
    EClass ENUMERATION_CONSTANT = eINSTANCE.getEnumerationConstant();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ENUMERATION_CONSTANT__NAME = eINSTANCE.getEnumerationConstant_Name();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.DeclarationSpecifierImpl <em>Declaration Specifier</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.DeclarationSpecifierImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getDeclarationSpecifier()
     * @generated
     */
    EClass DECLARATION_SPECIFIER = eINSTANCE.getDeclarationSpecifier();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.StorageClassSpecifierImpl <em>Storage Class Specifier</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.StorageClassSpecifierImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getStorageClassSpecifier()
     * @generated
     */
    EClass STORAGE_CLASS_SPECIFIER = eINSTANCE.getStorageClassSpecifier();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.TypeQualifierImpl <em>Type Qualifier</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.TypeQualifierImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getTypeQualifier()
     * @generated
     */
    EClass TYPE_QUALIFIER = eINSTANCE.getTypeQualifier();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.FunctionSpecifierImpl <em>Function Specifier</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.FunctionSpecifierImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getFunctionSpecifier()
     * @generated
     */
    EClass FUNCTION_SPECIFIER = eINSTANCE.getFunctionSpecifier();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.AlignmentSpecifierImpl <em>Alignment Specifier</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.AlignmentSpecifierImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getAlignmentSpecifier()
     * @generated
     */
    EClass ALIGNMENT_SPECIFIER = eINSTANCE.getAlignmentSpecifier();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ALIGNMENT_SPECIFIER__TYPE = eINSTANCE.getAlignmentSpecifier_Type();

    /**
     * The meta object literal for the '<em><b>Exp</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ALIGNMENT_SPECIFIER__EXP = eINSTANCE.getAlignmentSpecifier_Exp();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.ExpressionImpl <em>Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.ExpressionImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getExpression()
     * @generated
     */
    EClass EXPRESSION = eINSTANCE.getExpression();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.AssignmentExpressionElementImpl <em>Assignment Expression Element</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.AssignmentExpressionElementImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getAssignmentExpressionElement()
     * @generated
     */
    EClass ASSIGNMENT_EXPRESSION_ELEMENT = eINSTANCE.getAssignmentExpressionElement();

    /**
     * The meta object literal for the '<em><b>Op</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ASSIGNMENT_EXPRESSION_ELEMENT__OP = eINSTANCE.getAssignmentExpressionElement_Op();

    /**
     * The meta object literal for the '<em><b>Exp</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ASSIGNMENT_EXPRESSION_ELEMENT__EXP = eINSTANCE.getAssignmentExpressionElement_Exp();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.StringExpressionImpl <em>String Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.StringExpressionImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getStringExpression()
     * @generated
     */
    EClass STRING_EXPRESSION = eINSTANCE.getStringExpression();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute STRING_EXPRESSION__NAME = eINSTANCE.getStringExpression_Name();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.GenericAssociationImpl <em>Generic Association</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.GenericAssociationImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getGenericAssociation()
     * @generated
     */
    EClass GENERIC_ASSOCIATION = eINSTANCE.getGenericAssociation();

    /**
     * The meta object literal for the '<em><b>Exp</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference GENERIC_ASSOCIATION__EXP = eINSTANCE.getGenericAssociation_Exp();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.PostfixExpressionPostfixImpl <em>Postfix Expression Postfix</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.PostfixExpressionPostfixImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getPostfixExpressionPostfix()
     * @generated
     */
    EClass POSTFIX_EXPRESSION_POSTFIX = eINSTANCE.getPostfixExpressionPostfix();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.NextAsignementImpl <em>Next Asignement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.NextAsignementImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getNextAsignement()
     * @generated
     */
    EClass NEXT_ASIGNEMENT = eINSTANCE.getNextAsignement();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference NEXT_ASIGNEMENT__RIGHT = eINSTANCE.getNextAsignement_Right();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.CompoundStatementImpl <em>Compound Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.CompoundStatementImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getCompoundStatement()
     * @generated
     */
    EClass COMPOUND_STATEMENT = eINSTANCE.getCompoundStatement();

    /**
     * The meta object literal for the '<em><b>Inner</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference COMPOUND_STATEMENT__INNER = eINSTANCE.getCompoundStatement_Inner();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.BlockItemImpl <em>Block Item</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.BlockItemImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getBlockItem()
     * @generated
     */
    EClass BLOCK_ITEM = eINSTANCE.getBlockItem();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.StatementsImpl <em>Statements</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.StatementsImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getStatements()
     * @generated
     */
    EClass STATEMENTS = eINSTANCE.getStatements();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.LabeledStatementImpl <em>Labeled Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.LabeledStatementImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getLabeledStatement()
     * @generated
     */
    EClass LABELED_STATEMENT = eINSTANCE.getLabeledStatement();

    /**
     * The meta object literal for the '<em><b>Statement</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LABELED_STATEMENT__STATEMENT = eINSTANCE.getLabeledStatement_Statement();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.StatementImpl <em>Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.StatementImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getStatement()
     * @generated
     */
    EClass STATEMENT = eINSTANCE.getStatement();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.ExpressionStatementImpl <em>Expression Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.ExpressionStatementImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getExpressionStatement()
     * @generated
     */
    EClass EXPRESSION_STATEMENT = eINSTANCE.getExpressionStatement();

    /**
     * The meta object literal for the '<em><b>Exp</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPRESSION_STATEMENT__EXP = eINSTANCE.getExpressionStatement_Exp();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.SelectionStatementImpl <em>Selection Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.SelectionStatementImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getSelectionStatement()
     * @generated
     */
    EClass SELECTION_STATEMENT = eINSTANCE.getSelectionStatement();

    /**
     * The meta object literal for the '<em><b>Exp</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SELECTION_STATEMENT__EXP = eINSTANCE.getSelectionStatement_Exp();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.IterationStatementImpl <em>Iteration Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.IterationStatementImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getIterationStatement()
     * @generated
     */
    EClass ITERATION_STATEMENT = eINSTANCE.getIterationStatement();

    /**
     * The meta object literal for the '<em><b>Statement</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ITERATION_STATEMENT__STATEMENT = eINSTANCE.getIterationStatement_Statement();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.NextFieldImpl <em>Next Field</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.NextFieldImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getNextField()
     * @generated
     */
    EClass NEXT_FIELD = eINSTANCE.getNextField();

    /**
     * The meta object literal for the '<em><b>Next</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference NEXT_FIELD__NEXT = eINSTANCE.getNextField_Next();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.JumpStatementImpl <em>Jump Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.JumpStatementImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getJumpStatement()
     * @generated
     */
    EClass JUMP_STATEMENT = eINSTANCE.getJumpStatement();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.CompleteNameImpl <em>Complete Name</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.CompleteNameImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getCompleteName()
     * @generated
     */
    EClass COMPLETE_NAME = eINSTANCE.getCompleteName();

    /**
     * The meta object literal for the '<em><b>Ext</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute COMPLETE_NAME__EXT = eINSTANCE.getCompleteName_Ext();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.SimpleNameImpl <em>Simple Name</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.SimpleNameImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getSimpleName()
     * @generated
     */
    EClass SIMPLE_NAME = eINSTANCE.getSimpleName();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.GenericDeclaration0Impl <em>Generic Declaration0</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.GenericDeclaration0Impl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getGenericDeclaration0()
     * @generated
     */
    EClass GENERIC_DECLARATION0 = eINSTANCE.getGenericDeclaration0();

    /**
     * The meta object literal for the '<em><b>Pointers</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute GENERIC_DECLARATION0__POINTERS = eINSTANCE.getGenericDeclaration0_Pointers();

    /**
     * The meta object literal for the '<em><b>Restrict</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute GENERIC_DECLARATION0__RESTRICT = eINSTANCE.getGenericDeclaration0_Restrict();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.GenericDeclaration1Impl <em>Generic Declaration1</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.GenericDeclaration1Impl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getGenericDeclaration1()
     * @generated
     */
    EClass GENERIC_DECLARATION1 = eINSTANCE.getGenericDeclaration1();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.GenericDeclaration2Impl <em>Generic Declaration2</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.GenericDeclaration2Impl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getGenericDeclaration2()
     * @generated
     */
    EClass GENERIC_DECLARATION2 = eINSTANCE.getGenericDeclaration2();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.GlobalNameImpl <em>Global Name</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.GlobalNameImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getGlobalName()
     * @generated
     */
    EClass GLOBAL_NAME = eINSTANCE.getGlobalName();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute GLOBAL_NAME__NAME = eINSTANCE.getGlobalName_Name();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.ExpressionInitializerImpl <em>Expression Initializer</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.ExpressionInitializerImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getExpressionInitializer()
     * @generated
     */
    EClass EXPRESSION_INITIALIZER = eINSTANCE.getExpressionInitializer();

    /**
     * The meta object literal for the '<em><b>Exp</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPRESSION_INITIALIZER__EXP = eINSTANCE.getExpressionInitializer_Exp();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.FunctionInitializerImpl <em>Function Initializer</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.FunctionInitializerImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getFunctionInitializer()
     * @generated
     */
    EClass FUNCTION_INITIALIZER = eINSTANCE.getFunctionInitializer();

    /**
     * The meta object literal for the '<em><b>Block</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FUNCTION_INITIALIZER__BLOCK = eINSTANCE.getFunctionInitializer_Block();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.AtomicImpl <em>Atomic</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.AtomicImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getAtomic()
     * @generated
     */
    EClass ATOMIC = eINSTANCE.getAtomic();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ATOMIC__TYPE = eINSTANCE.getAtomic_Type();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.BaseTypeVoidImpl <em>Base Type Void</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.BaseTypeVoidImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getBaseTypeVoid()
     * @generated
     */
    EClass BASE_TYPE_VOID = eINSTANCE.getBaseTypeVoid();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.CustomImpl <em>Custom</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.CustomImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getCustom()
     * @generated
     */
    EClass CUSTOM = eINSTANCE.getCustom();

    /**
     * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute CUSTOM__ID = eINSTANCE.getCustom_Id();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.BaseTypeCharImpl <em>Base Type Char</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.BaseTypeCharImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getBaseTypeChar()
     * @generated
     */
    EClass BASE_TYPE_CHAR = eINSTANCE.getBaseTypeChar();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.BaseTypeShortImpl <em>Base Type Short</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.BaseTypeShortImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getBaseTypeShort()
     * @generated
     */
    EClass BASE_TYPE_SHORT = eINSTANCE.getBaseTypeShort();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.BaseTypeIntImpl <em>Base Type Int</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.BaseTypeIntImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getBaseTypeInt()
     * @generated
     */
    EClass BASE_TYPE_INT = eINSTANCE.getBaseTypeInt();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.BaseTypeLongImpl <em>Base Type Long</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.BaseTypeLongImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getBaseTypeLong()
     * @generated
     */
    EClass BASE_TYPE_LONG = eINSTANCE.getBaseTypeLong();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.BaseTypeFloatImpl <em>Base Type Float</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.BaseTypeFloatImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getBaseTypeFloat()
     * @generated
     */
    EClass BASE_TYPE_FLOAT = eINSTANCE.getBaseTypeFloat();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.BaseTypeDoubleImpl <em>Base Type Double</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.BaseTypeDoubleImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getBaseTypeDouble()
     * @generated
     */
    EClass BASE_TYPE_DOUBLE = eINSTANCE.getBaseTypeDouble();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.BaseTypeSignedImpl <em>Base Type Signed</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.BaseTypeSignedImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getBaseTypeSigned()
     * @generated
     */
    EClass BASE_TYPE_SIGNED = eINSTANCE.getBaseTypeSigned();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.BaseTypeUnsignedImpl <em>Base Type Unsigned</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.BaseTypeUnsignedImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getBaseTypeUnsigned()
     * @generated
     */
    EClass BASE_TYPE_UNSIGNED = eINSTANCE.getBaseTypeUnsigned();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.BaseTypeBoolImpl <em>Base Type Bool</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.BaseTypeBoolImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getBaseTypeBool()
     * @generated
     */
    EClass BASE_TYPE_BOOL = eINSTANCE.getBaseTypeBool();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.BaseTypeComplexImpl <em>Base Type Complex</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.BaseTypeComplexImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getBaseTypeComplex()
     * @generated
     */
    EClass BASE_TYPE_COMPLEX = eINSTANCE.getBaseTypeComplex();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.BaseTypeImaginaryImpl <em>Base Type Imaginary</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.BaseTypeImaginaryImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getBaseTypeImaginary()
     * @generated
     */
    EClass BASE_TYPE_IMAGINARY = eINSTANCE.getBaseTypeImaginary();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.StorageClassSpecifierExternImpl <em>Storage Class Specifier Extern</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.StorageClassSpecifierExternImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getStorageClassSpecifierExtern()
     * @generated
     */
    EClass STORAGE_CLASS_SPECIFIER_EXTERN = eINSTANCE.getStorageClassSpecifierExtern();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.StorageClassSpecifierStaticImpl <em>Storage Class Specifier Static</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.StorageClassSpecifierStaticImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getStorageClassSpecifierStatic()
     * @generated
     */
    EClass STORAGE_CLASS_SPECIFIER_STATIC = eINSTANCE.getStorageClassSpecifierStatic();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.StorageClassSpecifierThreadLocalImpl <em>Storage Class Specifier Thread Local</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.StorageClassSpecifierThreadLocalImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getStorageClassSpecifierThreadLocal()
     * @generated
     */
    EClass STORAGE_CLASS_SPECIFIER_THREAD_LOCAL = eINSTANCE.getStorageClassSpecifierThreadLocal();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.StorageClassSpecifierAutoImpl <em>Storage Class Specifier Auto</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.StorageClassSpecifierAutoImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getStorageClassSpecifierAuto()
     * @generated
     */
    EClass STORAGE_CLASS_SPECIFIER_AUTO = eINSTANCE.getStorageClassSpecifierAuto();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.StorageClassSpecifierRegisterImpl <em>Storage Class Specifier Register</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.StorageClassSpecifierRegisterImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getStorageClassSpecifierRegister()
     * @generated
     */
    EClass STORAGE_CLASS_SPECIFIER_REGISTER = eINSTANCE.getStorageClassSpecifierRegister();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.TypeQualifierConstImpl <em>Type Qualifier Const</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.TypeQualifierConstImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getTypeQualifierConst()
     * @generated
     */
    EClass TYPE_QUALIFIER_CONST = eINSTANCE.getTypeQualifierConst();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.TypeQualifierVolatileImpl <em>Type Qualifier Volatile</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.TypeQualifierVolatileImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getTypeQualifierVolatile()
     * @generated
     */
    EClass TYPE_QUALIFIER_VOLATILE = eINSTANCE.getTypeQualifierVolatile();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.TypeQualifierAtomicImpl <em>Type Qualifier Atomic</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.TypeQualifierAtomicImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getTypeQualifierAtomic()
     * @generated
     */
    EClass TYPE_QUALIFIER_ATOMIC = eINSTANCE.getTypeQualifierAtomic();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.FunctionSpecifierInlineImpl <em>Function Specifier Inline</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.FunctionSpecifierInlineImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getFunctionSpecifierInline()
     * @generated
     */
    EClass FUNCTION_SPECIFIER_INLINE = eINSTANCE.getFunctionSpecifierInline();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.FunctionSpecifierNoReturnImpl <em>Function Specifier No Return</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.FunctionSpecifierNoReturnImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getFunctionSpecifierNoReturn()
     * @generated
     */
    EClass FUNCTION_SPECIFIER_NO_RETURN = eINSTANCE.getFunctionSpecifierNoReturn();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.AssignmentExpressionImpl <em>Assignment Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.AssignmentExpressionImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getAssignmentExpression()
     * @generated
     */
    EClass ASSIGNMENT_EXPRESSION = eINSTANCE.getAssignmentExpression();

    /**
     * The meta object literal for the '<em><b>Exp</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ASSIGNMENT_EXPRESSION__EXP = eINSTANCE.getAssignmentExpression_Exp();

    /**
     * The meta object literal for the '<em><b>List</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ASSIGNMENT_EXPRESSION__LIST = eINSTANCE.getAssignmentExpression_List();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.ConditionalExpressionImpl <em>Conditional Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.ConditionalExpressionImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getConditionalExpression()
     * @generated
     */
    EClass CONDITIONAL_EXPRESSION = eINSTANCE.getConditionalExpression();

    /**
     * The meta object literal for the '<em><b>If</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CONDITIONAL_EXPRESSION__IF = eINSTANCE.getConditionalExpression_If();

    /**
     * The meta object literal for the '<em><b>Yes</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CONDITIONAL_EXPRESSION__YES = eINSTANCE.getConditionalExpression_Yes();

    /**
     * The meta object literal for the '<em><b>No</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CONDITIONAL_EXPRESSION__NO = eINSTANCE.getConditionalExpression_No();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.LogicalOrExpressionImpl <em>Logical Or Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.LogicalOrExpressionImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getLogicalOrExpression()
     * @generated
     */
    EClass LOGICAL_OR_EXPRESSION = eINSTANCE.getLogicalOrExpression();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LOGICAL_OR_EXPRESSION__LEFT = eINSTANCE.getLogicalOrExpression_Left();

    /**
     * The meta object literal for the '<em><b>Operand</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute LOGICAL_OR_EXPRESSION__OPERAND = eINSTANCE.getLogicalOrExpression_Operand();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LOGICAL_OR_EXPRESSION__RIGHT = eINSTANCE.getLogicalOrExpression_Right();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.LogicalAndExpressionImpl <em>Logical And Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.LogicalAndExpressionImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getLogicalAndExpression()
     * @generated
     */
    EClass LOGICAL_AND_EXPRESSION = eINSTANCE.getLogicalAndExpression();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LOGICAL_AND_EXPRESSION__LEFT = eINSTANCE.getLogicalAndExpression_Left();

    /**
     * The meta object literal for the '<em><b>Operand</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute LOGICAL_AND_EXPRESSION__OPERAND = eINSTANCE.getLogicalAndExpression_Operand();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LOGICAL_AND_EXPRESSION__RIGHT = eINSTANCE.getLogicalAndExpression_Right();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.InclusiveOrExpressionImpl <em>Inclusive Or Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.InclusiveOrExpressionImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getInclusiveOrExpression()
     * @generated
     */
    EClass INCLUSIVE_OR_EXPRESSION = eINSTANCE.getInclusiveOrExpression();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference INCLUSIVE_OR_EXPRESSION__LEFT = eINSTANCE.getInclusiveOrExpression_Left();

    /**
     * The meta object literal for the '<em><b>Operand</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute INCLUSIVE_OR_EXPRESSION__OPERAND = eINSTANCE.getInclusiveOrExpression_Operand();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference INCLUSIVE_OR_EXPRESSION__RIGHT = eINSTANCE.getInclusiveOrExpression_Right();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.ExclusiveOrExpressionImpl <em>Exclusive Or Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.ExclusiveOrExpressionImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getExclusiveOrExpression()
     * @generated
     */
    EClass EXCLUSIVE_OR_EXPRESSION = eINSTANCE.getExclusiveOrExpression();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXCLUSIVE_OR_EXPRESSION__LEFT = eINSTANCE.getExclusiveOrExpression_Left();

    /**
     * The meta object literal for the '<em><b>Operand</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute EXCLUSIVE_OR_EXPRESSION__OPERAND = eINSTANCE.getExclusiveOrExpression_Operand();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXCLUSIVE_OR_EXPRESSION__RIGHT = eINSTANCE.getExclusiveOrExpression_Right();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.AndExpressionImpl <em>And Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.AndExpressionImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getAndExpression()
     * @generated
     */
    EClass AND_EXPRESSION = eINSTANCE.getAndExpression();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference AND_EXPRESSION__LEFT = eINSTANCE.getAndExpression_Left();

    /**
     * The meta object literal for the '<em><b>Operand</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute AND_EXPRESSION__OPERAND = eINSTANCE.getAndExpression_Operand();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference AND_EXPRESSION__RIGHT = eINSTANCE.getAndExpression_Right();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.EqualityExpressionImpl <em>Equality Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.EqualityExpressionImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getEqualityExpression()
     * @generated
     */
    EClass EQUALITY_EXPRESSION = eINSTANCE.getEqualityExpression();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EQUALITY_EXPRESSION__LEFT = eINSTANCE.getEqualityExpression_Left();

    /**
     * The meta object literal for the '<em><b>Operand</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute EQUALITY_EXPRESSION__OPERAND = eINSTANCE.getEqualityExpression_Operand();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EQUALITY_EXPRESSION__RIGHT = eINSTANCE.getEqualityExpression_Right();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.RelationalExpressionImpl <em>Relational Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.RelationalExpressionImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getRelationalExpression()
     * @generated
     */
    EClass RELATIONAL_EXPRESSION = eINSTANCE.getRelationalExpression();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference RELATIONAL_EXPRESSION__LEFT = eINSTANCE.getRelationalExpression_Left();

    /**
     * The meta object literal for the '<em><b>Operand</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute RELATIONAL_EXPRESSION__OPERAND = eINSTANCE.getRelationalExpression_Operand();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference RELATIONAL_EXPRESSION__RIGHT = eINSTANCE.getRelationalExpression_Right();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.ShiftExpressionImpl <em>Shift Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.ShiftExpressionImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getShiftExpression()
     * @generated
     */
    EClass SHIFT_EXPRESSION = eINSTANCE.getShiftExpression();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SHIFT_EXPRESSION__LEFT = eINSTANCE.getShiftExpression_Left();

    /**
     * The meta object literal for the '<em><b>Operand</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SHIFT_EXPRESSION__OPERAND = eINSTANCE.getShiftExpression_Operand();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SHIFT_EXPRESSION__RIGHT = eINSTANCE.getShiftExpression_Right();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.AdditiveExpressionImpl <em>Additive Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.AdditiveExpressionImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getAdditiveExpression()
     * @generated
     */
    EClass ADDITIVE_EXPRESSION = eINSTANCE.getAdditiveExpression();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ADDITIVE_EXPRESSION__LEFT = eINSTANCE.getAdditiveExpression_Left();

    /**
     * The meta object literal for the '<em><b>Operand</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ADDITIVE_EXPRESSION__OPERAND = eINSTANCE.getAdditiveExpression_Operand();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ADDITIVE_EXPRESSION__RIGHT = eINSTANCE.getAdditiveExpression_Right();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.MultiplicativeExpressionImpl <em>Multiplicative Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.MultiplicativeExpressionImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getMultiplicativeExpression()
     * @generated
     */
    EClass MULTIPLICATIVE_EXPRESSION = eINSTANCE.getMultiplicativeExpression();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MULTIPLICATIVE_EXPRESSION__LEFT = eINSTANCE.getMultiplicativeExpression_Left();

    /**
     * The meta object literal for the '<em><b>Operand</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute MULTIPLICATIVE_EXPRESSION__OPERAND = eINSTANCE.getMultiplicativeExpression_Operand();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MULTIPLICATIVE_EXPRESSION__RIGHT = eINSTANCE.getMultiplicativeExpression_Right();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.ExpressionSimpleImpl <em>Expression Simple</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.ExpressionSimpleImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getExpressionSimple()
     * @generated
     */
    EClass EXPRESSION_SIMPLE = eINSTANCE.getExpressionSimple();

    /**
     * The meta object literal for the '<em><b>Exp</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPRESSION_SIMPLE__EXP = eINSTANCE.getExpressionSimple_Exp();

    /**
     * The meta object literal for the '<em><b>Next</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPRESSION_SIMPLE__NEXT = eINSTANCE.getExpressionSimple_Next();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.ExpressionComplexImpl <em>Expression Complex</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.ExpressionComplexImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getExpressionComplex()
     * @generated
     */
    EClass EXPRESSION_COMPLEX = eINSTANCE.getExpressionComplex();

    /**
     * The meta object literal for the '<em><b>Exps</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPRESSION_COMPLEX__EXPS = eINSTANCE.getExpressionComplex_Exps();

    /**
     * The meta object literal for the '<em><b>Next</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPRESSION_COMPLEX__NEXT = eINSTANCE.getExpressionComplex_Next();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.ExpressionIncrementImpl <em>Expression Increment</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.ExpressionIncrementImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getExpressionIncrement()
     * @generated
     */
    EClass EXPRESSION_INCREMENT = eINSTANCE.getExpressionIncrement();

    /**
     * The meta object literal for the '<em><b>Content</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPRESSION_INCREMENT__CONTENT = eINSTANCE.getExpressionIncrement_Content();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.ExpressionDecrementImpl <em>Expression Decrement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.ExpressionDecrementImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getExpressionDecrement()
     * @generated
     */
    EClass EXPRESSION_DECREMENT = eINSTANCE.getExpressionDecrement();

    /**
     * The meta object literal for the '<em><b>Content</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPRESSION_DECREMENT__CONTENT = eINSTANCE.getExpressionDecrement_Content();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.ExpressionSizeofImpl <em>Expression Sizeof</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.ExpressionSizeofImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getExpressionSizeof()
     * @generated
     */
    EClass EXPRESSION_SIZEOF = eINSTANCE.getExpressionSizeof();

    /**
     * The meta object literal for the '<em><b>Content</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPRESSION_SIZEOF__CONTENT = eINSTANCE.getExpressionSizeof_Content();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPRESSION_SIZEOF__TYPE = eINSTANCE.getExpressionSizeof_Type();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.ExpressionUnaryOpImpl <em>Expression Unary Op</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.ExpressionUnaryOpImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getExpressionUnaryOp()
     * @generated
     */
    EClass EXPRESSION_UNARY_OP = eINSTANCE.getExpressionUnaryOp();

    /**
     * The meta object literal for the '<em><b>Op</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute EXPRESSION_UNARY_OP__OP = eINSTANCE.getExpressionUnaryOp_Op();

    /**
     * The meta object literal for the '<em><b>Content</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPRESSION_UNARY_OP__CONTENT = eINSTANCE.getExpressionUnaryOp_Content();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.ExpressionAlignImpl <em>Expression Align</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.ExpressionAlignImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getExpressionAlign()
     * @generated
     */
    EClass EXPRESSION_ALIGN = eINSTANCE.getExpressionAlign();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPRESSION_ALIGN__TYPE = eINSTANCE.getExpressionAlign_Type();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.ExpressionCastImpl <em>Expression Cast</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.ExpressionCastImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getExpressionCast()
     * @generated
     */
    EClass EXPRESSION_CAST = eINSTANCE.getExpressionCast();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPRESSION_CAST__TYPE = eINSTANCE.getExpressionCast_Type();

    /**
     * The meta object literal for the '<em><b>Exp</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPRESSION_CAST__EXP = eINSTANCE.getExpressionCast_Exp();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.ExpressionIdentifierImpl <em>Expression Identifier</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.ExpressionIdentifierImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getExpressionIdentifier()
     * @generated
     */
    EClass EXPRESSION_IDENTIFIER = eINSTANCE.getExpressionIdentifier();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute EXPRESSION_IDENTIFIER__NAME = eINSTANCE.getExpressionIdentifier_Name();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.ExpressionStringImpl <em>Expression String</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.ExpressionStringImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getExpressionString()
     * @generated
     */
    EClass EXPRESSION_STRING = eINSTANCE.getExpressionString();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPRESSION_STRING__VALUE = eINSTANCE.getExpressionString_Value();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.ExpressionIntegerImpl <em>Expression Integer</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.ExpressionIntegerImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getExpressionInteger()
     * @generated
     */
    EClass EXPRESSION_INTEGER = eINSTANCE.getExpressionInteger();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute EXPRESSION_INTEGER__VALUE = eINSTANCE.getExpressionInteger_Value();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.ExpressionFloatImpl <em>Expression Float</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.ExpressionFloatImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getExpressionFloat()
     * @generated
     */
    EClass EXPRESSION_FLOAT = eINSTANCE.getExpressionFloat();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute EXPRESSION_FLOAT__VALUE = eINSTANCE.getExpressionFloat_Value();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.ExpressionSubExpressionImpl <em>Expression Sub Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.ExpressionSubExpressionImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getExpressionSubExpression()
     * @generated
     */
    EClass EXPRESSION_SUB_EXPRESSION = eINSTANCE.getExpressionSubExpression();

    /**
     * The meta object literal for the '<em><b>Par</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPRESSION_SUB_EXPRESSION__PAR = eINSTANCE.getExpressionSubExpression_Par();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.ExpressionGenericSelectionImpl <em>Expression Generic Selection</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.ExpressionGenericSelectionImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getExpressionGenericSelection()
     * @generated
     */
    EClass EXPRESSION_GENERIC_SELECTION = eINSTANCE.getExpressionGenericSelection();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPRESSION_GENERIC_SELECTION__LEFT = eINSTANCE.getExpressionGenericSelection_Left();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPRESSION_GENERIC_SELECTION__RIGHT = eINSTANCE.getExpressionGenericSelection_Right();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.GenericAssociationCaseImpl <em>Generic Association Case</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.GenericAssociationCaseImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getGenericAssociationCase()
     * @generated
     */
    EClass GENERIC_ASSOCIATION_CASE = eINSTANCE.getGenericAssociationCase();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference GENERIC_ASSOCIATION_CASE__TYPE = eINSTANCE.getGenericAssociationCase_Type();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.GenericAssociationDefaultImpl <em>Generic Association Default</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.GenericAssociationDefaultImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getGenericAssociationDefault()
     * @generated
     */
    EClass GENERIC_ASSOCIATION_DEFAULT = eINSTANCE.getGenericAssociationDefault();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.PostfixExpressionPostfixExpressionImpl <em>Postfix Expression Postfix Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.PostfixExpressionPostfixExpressionImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getPostfixExpressionPostfixExpression()
     * @generated
     */
    EClass POSTFIX_EXPRESSION_POSTFIX_EXPRESSION = eINSTANCE.getPostfixExpressionPostfixExpression();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference POSTFIX_EXPRESSION_POSTFIX_EXPRESSION__VALUE = eINSTANCE.getPostfixExpressionPostfixExpression_Value();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.PostfixExpressionPostfixArgumentListImpl <em>Postfix Expression Postfix Argument List</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.PostfixExpressionPostfixArgumentListImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getPostfixExpressionPostfixArgumentList()
     * @generated
     */
    EClass POSTFIX_EXPRESSION_POSTFIX_ARGUMENT_LIST = eINSTANCE.getPostfixExpressionPostfixArgumentList();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference POSTFIX_EXPRESSION_POSTFIX_ARGUMENT_LIST__LEFT = eINSTANCE.getPostfixExpressionPostfixArgumentList_Left();

    /**
     * The meta object literal for the '<em><b>Next</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference POSTFIX_EXPRESSION_POSTFIX_ARGUMENT_LIST__NEXT = eINSTANCE.getPostfixExpressionPostfixArgumentList_Next();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.PostfixExpressionPostfixAccessImpl <em>Postfix Expression Postfix Access</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.PostfixExpressionPostfixAccessImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getPostfixExpressionPostfixAccess()
     * @generated
     */
    EClass POSTFIX_EXPRESSION_POSTFIX_ACCESS = eINSTANCE.getPostfixExpressionPostfixAccess();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute POSTFIX_EXPRESSION_POSTFIX_ACCESS__VALUE = eINSTANCE.getPostfixExpressionPostfixAccess_Value();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.PostfixExpressionPostfixAccessPtrImpl <em>Postfix Expression Postfix Access Ptr</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.PostfixExpressionPostfixAccessPtrImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getPostfixExpressionPostfixAccessPtr()
     * @generated
     */
    EClass POSTFIX_EXPRESSION_POSTFIX_ACCESS_PTR = eINSTANCE.getPostfixExpressionPostfixAccessPtr();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute POSTFIX_EXPRESSION_POSTFIX_ACCESS_PTR__VALUE = eINSTANCE.getPostfixExpressionPostfixAccessPtr_Value();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.PostfixExpressionPostfixIncrementImpl <em>Postfix Expression Postfix Increment</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.PostfixExpressionPostfixIncrementImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getPostfixExpressionPostfixIncrement()
     * @generated
     */
    EClass POSTFIX_EXPRESSION_POSTFIX_INCREMENT = eINSTANCE.getPostfixExpressionPostfixIncrement();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.PostfixExpressionPostfixDecrementImpl <em>Postfix Expression Postfix Decrement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.PostfixExpressionPostfixDecrementImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getPostfixExpressionPostfixDecrement()
     * @generated
     */
    EClass POSTFIX_EXPRESSION_POSTFIX_DECREMENT = eINSTANCE.getPostfixExpressionPostfixDecrement();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.StatementDeclarationImpl <em>Statement Declaration</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.StatementDeclarationImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getStatementDeclaration()
     * @generated
     */
    EClass STATEMENT_DECLARATION = eINSTANCE.getStatementDeclaration();

    /**
     * The meta object literal for the '<em><b>Decl</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference STATEMENT_DECLARATION__DECL = eINSTANCE.getStatementDeclaration_Decl();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.StatementInnerStatementImpl <em>Statement Inner Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.StatementInnerStatementImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getStatementInnerStatement()
     * @generated
     */
    EClass STATEMENT_INNER_STATEMENT = eINSTANCE.getStatementInnerStatement();

    /**
     * The meta object literal for the '<em><b>Statement</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference STATEMENT_INNER_STATEMENT__STATEMENT = eINSTANCE.getStatementInnerStatement_Statement();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.LabeledStatementCaseImpl <em>Labeled Statement Case</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.LabeledStatementCaseImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getLabeledStatementCase()
     * @generated
     */
    EClass LABELED_STATEMENT_CASE = eINSTANCE.getLabeledStatementCase();

    /**
     * The meta object literal for the '<em><b>Exp</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LABELED_STATEMENT_CASE__EXP = eINSTANCE.getLabeledStatementCase_Exp();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.LabeledStatementDefaultImpl <em>Labeled Statement Default</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.LabeledStatementDefaultImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getLabeledStatementDefault()
     * @generated
     */
    EClass LABELED_STATEMENT_DEFAULT = eINSTANCE.getLabeledStatementDefault();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.GoToPrefixStatementImpl <em>Go To Prefix Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.GoToPrefixStatementImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getGoToPrefixStatement()
     * @generated
     */
    EClass GO_TO_PREFIX_STATEMENT = eINSTANCE.getGoToPrefixStatement();

    /**
     * The meta object literal for the '<em><b>Goto</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute GO_TO_PREFIX_STATEMENT__GOTO = eINSTANCE.getGoToPrefixStatement_Goto();

    /**
     * The meta object literal for the '<em><b>Statement</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference GO_TO_PREFIX_STATEMENT__STATEMENT = eINSTANCE.getGoToPrefixStatement_Statement();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.SelectionStatementIfImpl <em>Selection Statement If</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.SelectionStatementIfImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getSelectionStatementIf()
     * @generated
     */
    EClass SELECTION_STATEMENT_IF = eINSTANCE.getSelectionStatementIf();

    /**
     * The meta object literal for the '<em><b>Then statement</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SELECTION_STATEMENT_IF__THEN_STATEMENT = eINSTANCE.getSelectionStatementIf_Then_statement();

    /**
     * The meta object literal for the '<em><b>Else statement</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SELECTION_STATEMENT_IF__ELSE_STATEMENT = eINSTANCE.getSelectionStatementIf_Else_statement();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.SelectionStatementSwitchImpl <em>Selection Statement Switch</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.SelectionStatementSwitchImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getSelectionStatementSwitch()
     * @generated
     */
    EClass SELECTION_STATEMENT_SWITCH = eINSTANCE.getSelectionStatementSwitch();

    /**
     * The meta object literal for the '<em><b>Content</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SELECTION_STATEMENT_SWITCH__CONTENT = eINSTANCE.getSelectionStatementSwitch_Content();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.IterationStatementWhileImpl <em>Iteration Statement While</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.IterationStatementWhileImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getIterationStatementWhile()
     * @generated
     */
    EClass ITERATION_STATEMENT_WHILE = eINSTANCE.getIterationStatementWhile();

    /**
     * The meta object literal for the '<em><b>Exp</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ITERATION_STATEMENT_WHILE__EXP = eINSTANCE.getIterationStatementWhile_Exp();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.IterationStatementDoImpl <em>Iteration Statement Do</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.IterationStatementDoImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getIterationStatementDo()
     * @generated
     */
    EClass ITERATION_STATEMENT_DO = eINSTANCE.getIterationStatementDo();

    /**
     * The meta object literal for the '<em><b>Exp</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ITERATION_STATEMENT_DO__EXP = eINSTANCE.getIterationStatementDo_Exp();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.IterationStatementForImpl <em>Iteration Statement For</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.IterationStatementForImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getIterationStatementFor()
     * @generated
     */
    EClass ITERATION_STATEMENT_FOR = eINSTANCE.getIterationStatementFor();

    /**
     * The meta object literal for the '<em><b>Field1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ITERATION_STATEMENT_FOR__FIELD1 = eINSTANCE.getIterationStatementFor_Field1();

    /**
     * The meta object literal for the '<em><b>Decl</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ITERATION_STATEMENT_FOR__DECL = eINSTANCE.getIterationStatementFor_Decl();

    /**
     * The meta object literal for the '<em><b>Field2</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ITERATION_STATEMENT_FOR__FIELD2 = eINSTANCE.getIterationStatementFor_Field2();

    /**
     * The meta object literal for the '<em><b>Field3</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ITERATION_STATEMENT_FOR__FIELD3 = eINSTANCE.getIterationStatementFor_Field3();

    /**
     * The meta object literal for the '<em><b>Next</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ITERATION_STATEMENT_FOR__NEXT = eINSTANCE.getIterationStatementFor_Next();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.JumpStatementGotoImpl <em>Jump Statement Goto</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.JumpStatementGotoImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getJumpStatementGoto()
     * @generated
     */
    EClass JUMP_STATEMENT_GOTO = eINSTANCE.getJumpStatementGoto();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute JUMP_STATEMENT_GOTO__NAME = eINSTANCE.getJumpStatementGoto_Name();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.JumpStatementContinueImpl <em>Jump Statement Continue</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.JumpStatementContinueImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getJumpStatementContinue()
     * @generated
     */
    EClass JUMP_STATEMENT_CONTINUE = eINSTANCE.getJumpStatementContinue();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.JumpStatementBreakImpl <em>Jump Statement Break</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.JumpStatementBreakImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getJumpStatementBreak()
     * @generated
     */
    EClass JUMP_STATEMENT_BREAK = eINSTANCE.getJumpStatementBreak();

    /**
     * The meta object literal for the '{@link org.xtext.dop.clang.clang.impl.JumpStatementReturnImpl <em>Jump Statement Return</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.clang.clang.impl.JumpStatementReturnImpl
     * @see org.xtext.dop.clang.clang.impl.ClangPackageImpl#getJumpStatementReturn()
     * @generated
     */
    EClass JUMP_STATEMENT_RETURN = eINSTANCE.getJumpStatementReturn();

    /**
     * The meta object literal for the '<em><b>Exp</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference JUMP_STATEMENT_RETURN__EXP = eINSTANCE.getJumpStatementReturn_Exp();

  }

} //ClangPackage
