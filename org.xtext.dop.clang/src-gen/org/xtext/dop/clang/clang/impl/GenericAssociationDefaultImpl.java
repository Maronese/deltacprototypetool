/**
 */
package org.xtext.dop.clang.clang.impl;

import org.eclipse.emf.ecore.EClass;

import org.xtext.dop.clang.clang.ClangPackage;
import org.xtext.dop.clang.clang.GenericAssociationDefault;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Generic Association Default</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class GenericAssociationDefaultImpl extends GenericAssociationImpl implements GenericAssociationDefault
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected GenericAssociationDefaultImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ClangPackage.Literals.GENERIC_ASSOCIATION_DEFAULT;
  }

} //GenericAssociationDefaultImpl
