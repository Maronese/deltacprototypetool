/**
 */
package org.xtext.dop.clang.clang;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Storage Class Specifier Static</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getStorageClassSpecifierStatic()
 * @model
 * @generated
 */
public interface StorageClassSpecifierStatic extends StorageClassSpecifier
{
} // StorageClassSpecifierStatic
