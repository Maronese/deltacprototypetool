/**
 */
package org.xtext.dop.productdeclaration.productDeclaration.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.xtext.dop.productdeclaration.productDeclaration.Core;
import org.xtext.dop.productdeclaration.productDeclaration.Deltas;
import org.xtext.dop.productdeclaration.productDeclaration.Features;
import org.xtext.dop.productdeclaration.productDeclaration.Partitions;
import org.xtext.dop.productdeclaration.productDeclaration.ProductDeclaration;
import org.xtext.dop.productdeclaration.productDeclaration.ProductDeclarationPackage;
import org.xtext.dop.productdeclaration.productDeclaration.Products;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Product Declaration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationImpl#getFeatures <em>Features</em>}</li>
 *   <li>{@link org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationImpl#getDeltas <em>Deltas</em>}</li>
 *   <li>{@link org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationImpl#getConstraints <em>Constraints</em>}</li>
 *   <li>{@link org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationImpl#getPartitions <em>Partitions</em>}</li>
 *   <li>{@link org.xtext.dop.productdeclaration.productDeclaration.impl.ProductDeclarationImpl#getProducts <em>Products</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ProductDeclarationImpl extends MinimalEObjectImpl.Container implements ProductDeclaration
{
  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The cached value of the '{@link #getFeatures() <em>Features</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFeatures()
   * @generated
   * @ordered
   */
  protected Features features;

  /**
   * The cached value of the '{@link #getDeltas() <em>Deltas</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDeltas()
   * @generated
   * @ordered
   */
  protected Deltas deltas;

  /**
   * The cached value of the '{@link #getConstraints() <em>Constraints</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getConstraints()
   * @generated
   * @ordered
   */
  protected Core constraints;

  /**
   * The cached value of the '{@link #getPartitions() <em>Partitions</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPartitions()
   * @generated
   * @ordered
   */
  protected Partitions partitions;

  /**
   * The cached value of the '{@link #getProducts() <em>Products</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getProducts()
   * @generated
   * @ordered
   */
  protected Products products;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ProductDeclarationImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ProductDeclarationPackage.Literals.PRODUCT_DECLARATION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName)
  {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ProductDeclarationPackage.PRODUCT_DECLARATION__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Features getFeatures()
  {
    return features;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetFeatures(Features newFeatures, NotificationChain msgs)
  {
    Features oldFeatures = features;
    features = newFeatures;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ProductDeclarationPackage.PRODUCT_DECLARATION__FEATURES, oldFeatures, newFeatures);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFeatures(Features newFeatures)
  {
    if (newFeatures != features)
    {
      NotificationChain msgs = null;
      if (features != null)
        msgs = ((InternalEObject)features).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ProductDeclarationPackage.PRODUCT_DECLARATION__FEATURES, null, msgs);
      if (newFeatures != null)
        msgs = ((InternalEObject)newFeatures).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ProductDeclarationPackage.PRODUCT_DECLARATION__FEATURES, null, msgs);
      msgs = basicSetFeatures(newFeatures, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ProductDeclarationPackage.PRODUCT_DECLARATION__FEATURES, newFeatures, newFeatures));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Deltas getDeltas()
  {
    return deltas;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDeltas(Deltas newDeltas, NotificationChain msgs)
  {
    Deltas oldDeltas = deltas;
    deltas = newDeltas;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ProductDeclarationPackage.PRODUCT_DECLARATION__DELTAS, oldDeltas, newDeltas);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDeltas(Deltas newDeltas)
  {
    if (newDeltas != deltas)
    {
      NotificationChain msgs = null;
      if (deltas != null)
        msgs = ((InternalEObject)deltas).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ProductDeclarationPackage.PRODUCT_DECLARATION__DELTAS, null, msgs);
      if (newDeltas != null)
        msgs = ((InternalEObject)newDeltas).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ProductDeclarationPackage.PRODUCT_DECLARATION__DELTAS, null, msgs);
      msgs = basicSetDeltas(newDeltas, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ProductDeclarationPackage.PRODUCT_DECLARATION__DELTAS, newDeltas, newDeltas));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Core getConstraints()
  {
    return constraints;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetConstraints(Core newConstraints, NotificationChain msgs)
  {
    Core oldConstraints = constraints;
    constraints = newConstraints;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ProductDeclarationPackage.PRODUCT_DECLARATION__CONSTRAINTS, oldConstraints, newConstraints);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setConstraints(Core newConstraints)
  {
    if (newConstraints != constraints)
    {
      NotificationChain msgs = null;
      if (constraints != null)
        msgs = ((InternalEObject)constraints).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ProductDeclarationPackage.PRODUCT_DECLARATION__CONSTRAINTS, null, msgs);
      if (newConstraints != null)
        msgs = ((InternalEObject)newConstraints).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ProductDeclarationPackage.PRODUCT_DECLARATION__CONSTRAINTS, null, msgs);
      msgs = basicSetConstraints(newConstraints, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ProductDeclarationPackage.PRODUCT_DECLARATION__CONSTRAINTS, newConstraints, newConstraints));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Partitions getPartitions()
  {
    return partitions;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetPartitions(Partitions newPartitions, NotificationChain msgs)
  {
    Partitions oldPartitions = partitions;
    partitions = newPartitions;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ProductDeclarationPackage.PRODUCT_DECLARATION__PARTITIONS, oldPartitions, newPartitions);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPartitions(Partitions newPartitions)
  {
    if (newPartitions != partitions)
    {
      NotificationChain msgs = null;
      if (partitions != null)
        msgs = ((InternalEObject)partitions).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ProductDeclarationPackage.PRODUCT_DECLARATION__PARTITIONS, null, msgs);
      if (newPartitions != null)
        msgs = ((InternalEObject)newPartitions).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ProductDeclarationPackage.PRODUCT_DECLARATION__PARTITIONS, null, msgs);
      msgs = basicSetPartitions(newPartitions, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ProductDeclarationPackage.PRODUCT_DECLARATION__PARTITIONS, newPartitions, newPartitions));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Products getProducts()
  {
    return products;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetProducts(Products newProducts, NotificationChain msgs)
  {
    Products oldProducts = products;
    products = newProducts;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ProductDeclarationPackage.PRODUCT_DECLARATION__PRODUCTS, oldProducts, newProducts);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setProducts(Products newProducts)
  {
    if (newProducts != products)
    {
      NotificationChain msgs = null;
      if (products != null)
        msgs = ((InternalEObject)products).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ProductDeclarationPackage.PRODUCT_DECLARATION__PRODUCTS, null, msgs);
      if (newProducts != null)
        msgs = ((InternalEObject)newProducts).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ProductDeclarationPackage.PRODUCT_DECLARATION__PRODUCTS, null, msgs);
      msgs = basicSetProducts(newProducts, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ProductDeclarationPackage.PRODUCT_DECLARATION__PRODUCTS, newProducts, newProducts));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case ProductDeclarationPackage.PRODUCT_DECLARATION__FEATURES:
        return basicSetFeatures(null, msgs);
      case ProductDeclarationPackage.PRODUCT_DECLARATION__DELTAS:
        return basicSetDeltas(null, msgs);
      case ProductDeclarationPackage.PRODUCT_DECLARATION__CONSTRAINTS:
        return basicSetConstraints(null, msgs);
      case ProductDeclarationPackage.PRODUCT_DECLARATION__PARTITIONS:
        return basicSetPartitions(null, msgs);
      case ProductDeclarationPackage.PRODUCT_DECLARATION__PRODUCTS:
        return basicSetProducts(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case ProductDeclarationPackage.PRODUCT_DECLARATION__NAME:
        return getName();
      case ProductDeclarationPackage.PRODUCT_DECLARATION__FEATURES:
        return getFeatures();
      case ProductDeclarationPackage.PRODUCT_DECLARATION__DELTAS:
        return getDeltas();
      case ProductDeclarationPackage.PRODUCT_DECLARATION__CONSTRAINTS:
        return getConstraints();
      case ProductDeclarationPackage.PRODUCT_DECLARATION__PARTITIONS:
        return getPartitions();
      case ProductDeclarationPackage.PRODUCT_DECLARATION__PRODUCTS:
        return getProducts();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case ProductDeclarationPackage.PRODUCT_DECLARATION__NAME:
        setName((String)newValue);
        return;
      case ProductDeclarationPackage.PRODUCT_DECLARATION__FEATURES:
        setFeatures((Features)newValue);
        return;
      case ProductDeclarationPackage.PRODUCT_DECLARATION__DELTAS:
        setDeltas((Deltas)newValue);
        return;
      case ProductDeclarationPackage.PRODUCT_DECLARATION__CONSTRAINTS:
        setConstraints((Core)newValue);
        return;
      case ProductDeclarationPackage.PRODUCT_DECLARATION__PARTITIONS:
        setPartitions((Partitions)newValue);
        return;
      case ProductDeclarationPackage.PRODUCT_DECLARATION__PRODUCTS:
        setProducts((Products)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case ProductDeclarationPackage.PRODUCT_DECLARATION__NAME:
        setName(NAME_EDEFAULT);
        return;
      case ProductDeclarationPackage.PRODUCT_DECLARATION__FEATURES:
        setFeatures((Features)null);
        return;
      case ProductDeclarationPackage.PRODUCT_DECLARATION__DELTAS:
        setDeltas((Deltas)null);
        return;
      case ProductDeclarationPackage.PRODUCT_DECLARATION__CONSTRAINTS:
        setConstraints((Core)null);
        return;
      case ProductDeclarationPackage.PRODUCT_DECLARATION__PARTITIONS:
        setPartitions((Partitions)null);
        return;
      case ProductDeclarationPackage.PRODUCT_DECLARATION__PRODUCTS:
        setProducts((Products)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case ProductDeclarationPackage.PRODUCT_DECLARATION__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case ProductDeclarationPackage.PRODUCT_DECLARATION__FEATURES:
        return features != null;
      case ProductDeclarationPackage.PRODUCT_DECLARATION__DELTAS:
        return deltas != null;
      case ProductDeclarationPackage.PRODUCT_DECLARATION__CONSTRAINTS:
        return constraints != null;
      case ProductDeclarationPackage.PRODUCT_DECLARATION__PARTITIONS:
        return partitions != null;
      case ProductDeclarationPackage.PRODUCT_DECLARATION__PRODUCTS:
        return products != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (name: ");
    result.append(name);
    result.append(')');
    return result.toString();
  }

} //ProductDeclarationImpl
