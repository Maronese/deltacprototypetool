package org.xtext.dop.productdeclaration.interpreter

import org.xtext.dop.productdeclaration.productDeclaration.ProductDeclaration
import java.util.List
import com.google.inject.Inject
import org.xtext.dop.rappresentation.Project
import java.util.Set
import org.xtext.dop.productdeclaration.productDeclaration.Product
import org.eclipse.emf.common.util.EList
import java.util.Observer
import java.util.Observable
import org.xtext.dop.productdeclaration.productDeclaration.Partition
import org.xtext.dop.productdeclaration.productDeclaration.Constraint
import org.xtext.dop.productdeclaration.productDeclaration.Expression
import org.xtext.dop.productdeclaration.productDeclaration.Or
import org.xtext.dop.productdeclaration.productDeclaration.And
import org.xtext.dop.productdeclaration.productDeclaration.Not
import org.xtext.dop.productdeclaration.productDeclaration.Element
import java.util.LinkedList
import org.xtext.dop.productdeclaration.productDeclaration.Sub

class Interpreter {
	
	Product target
	ProductDeclaration decl
	List<String> deltas
	
	new(ProductDeclaration decl) {
		this.decl = decl
		this.deltas = new LinkedList<String>
	}
	
	def interpret(Product target) {
		
		setTarget(target)
		
		for(Partition part : decl.partitions.list){
			
			for(Constraint constr : part.constr) {
				
				if(constr.expr.value) deltas.add(constr.delta.name)
				
			}
			
		}
		
		deltas
		
	}
	
	def private boolean value(Expression expr) {
		switch expr {
			Or:
				value((expr as Or).left) || value((expr as Or).right)
			And:
				value((expr as And).left) && value((expr as And).right)
			Not:
				!value((expr as Not).value)
			Element:
				target.list.map[elem.name].contains((expr as Element).name)
			Sub:
				value((expr as Sub).expr)
			default:
				throw new Error
		}
	}
	
	def guiSelection(String name, org.xtext.dop.codebase.generator.Main codebase) {
		new Panel(name, decl.products.list, this, codebase)
	}
	
	
	def setTarget(Product target) {
		this.target = target
	}
	
}