/**
 */
package org.xtext.dop.clang.clang;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type Alias Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.clang.clang.TypeAliasDeclaration#getTdecl <em>Tdecl</em>}</li>
 * </ul>
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getTypeAliasDeclaration()
 * @model
 * @generated
 */
public interface TypeAliasDeclaration extends Declaration
{
  /**
   * Returns the value of the '<em><b>Tdecl</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Tdecl</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Tdecl</em>' containment reference.
   * @see #setTdecl(SpecifiedType)
   * @see org.xtext.dop.clang.clang.ClangPackage#getTypeAliasDeclaration_Tdecl()
   * @model containment="true"
   * @generated
   */
  SpecifiedType getTdecl();

  /**
   * Sets the value of the '{@link org.xtext.dop.clang.clang.TypeAliasDeclaration#getTdecl <em>Tdecl</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Tdecl</em>' containment reference.
   * @see #getTdecl()
   * @generated
   */
  void setTdecl(SpecifiedType value);

} // TypeAliasDeclaration
