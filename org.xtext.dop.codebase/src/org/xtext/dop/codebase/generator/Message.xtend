package org.xtext.dop.codebase.generator

import java.util.List
import org.xtext.dop.rappresentation.Project

class Message {
	List<String> deltas
	Project project
	
	new(List<String> deltas, Project project) {
		this.deltas = deltas
		this.project = project
	}
	
	def getDeltas() {
		deltas
	}
	
	def getProject() {
		project
	}
	
}