/**
 */
package org.xtext.dop.clang.clang;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Iteration Statement For</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.clang.clang.IterationStatementFor#getField1 <em>Field1</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.IterationStatementFor#getDecl <em>Decl</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.IterationStatementFor#getField2 <em>Field2</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.IterationStatementFor#getField3 <em>Field3</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.IterationStatementFor#getNext <em>Next</em>}</li>
 * </ul>
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getIterationStatementFor()
 * @model
 * @generated
 */
public interface IterationStatementFor extends IterationStatement
{
  /**
   * Returns the value of the '<em><b>Field1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Field1</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Field1</em>' containment reference.
   * @see #setField1(ExpressionStatement)
   * @see org.xtext.dop.clang.clang.ClangPackage#getIterationStatementFor_Field1()
   * @model containment="true"
   * @generated
   */
  ExpressionStatement getField1();

  /**
   * Sets the value of the '{@link org.xtext.dop.clang.clang.IterationStatementFor#getField1 <em>Field1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Field1</em>' containment reference.
   * @see #getField1()
   * @generated
   */
  void setField1(ExpressionStatement value);

  /**
   * Returns the value of the '<em><b>Decl</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Decl</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Decl</em>' containment reference.
   * @see #setDecl(Declaration)
   * @see org.xtext.dop.clang.clang.ClangPackage#getIterationStatementFor_Decl()
   * @model containment="true"
   * @generated
   */
  Declaration getDecl();

  /**
   * Sets the value of the '{@link org.xtext.dop.clang.clang.IterationStatementFor#getDecl <em>Decl</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Decl</em>' containment reference.
   * @see #getDecl()
   * @generated
   */
  void setDecl(Declaration value);

  /**
   * Returns the value of the '<em><b>Field2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Field2</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Field2</em>' containment reference.
   * @see #setField2(ExpressionStatement)
   * @see org.xtext.dop.clang.clang.ClangPackage#getIterationStatementFor_Field2()
   * @model containment="true"
   * @generated
   */
  ExpressionStatement getField2();

  /**
   * Sets the value of the '{@link org.xtext.dop.clang.clang.IterationStatementFor#getField2 <em>Field2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Field2</em>' containment reference.
   * @see #getField2()
   * @generated
   */
  void setField2(ExpressionStatement value);

  /**
   * Returns the value of the '<em><b>Field3</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Field3</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Field3</em>' containment reference.
   * @see #setField3(Expression)
   * @see org.xtext.dop.clang.clang.ClangPackage#getIterationStatementFor_Field3()
   * @model containment="true"
   * @generated
   */
  Expression getField3();

  /**
   * Sets the value of the '{@link org.xtext.dop.clang.clang.IterationStatementFor#getField3 <em>Field3</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Field3</em>' containment reference.
   * @see #getField3()
   * @generated
   */
  void setField3(Expression value);

  /**
   * Returns the value of the '<em><b>Next</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.dop.clang.clang.NextField}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Next</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Next</em>' containment reference list.
   * @see org.xtext.dop.clang.clang.ClangPackage#getIterationStatementFor_Next()
   * @model containment="true"
   * @generated
   */
  EList<NextField> getNext();

} // IterationStatementFor
