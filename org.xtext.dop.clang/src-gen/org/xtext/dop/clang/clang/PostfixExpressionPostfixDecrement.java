/**
 */
package org.xtext.dop.clang.clang;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Postfix Expression Postfix Decrement</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getPostfixExpressionPostfixDecrement()
 * @model
 * @generated
 */
public interface PostfixExpressionPostfixDecrement extends PostfixExpressionPostfix
{
} // PostfixExpressionPostfixDecrement
