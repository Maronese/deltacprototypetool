package org.xtext.dop.main;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.inject.Injector;

public class InitMain {

	public static void main(String[] args) {
		Injector injectorConf = new org.xtext.dop.productdeclaration.ProductDeclarationStandaloneSetup().createInjectorAndDoEMFRegistration();
		org.xtext.dop.productdeclaration.generator.Main productDeclaration = injectorConf.getInstance(org.xtext.dop.productdeclaration.generator.Main.class);
		
		Injector injectorDelta = new org.xtext.dop.codebase.CodeBaseStandaloneSetup().createInjectorAndDoEMFRegistration();
		org.xtext.dop.codebase.generator.Main codeBase = injectorDelta.getInstance(org.xtext.dop.codebase.generator.Main.class);
		
		codeBase.loadResources("deltas");
		productDeclaration.load("configuration/PESPL.conf");
		
		Map<String, Set<String>> map = codeBase.inspect();
		
		if(!productDeclaration.validate(map)) {
			System.err.print("error");
			System.exit(0);
		}
		
		if(!codeBase.validate()) {
			System.err.print("error");
			System.exit(0);
		}
		
		productDeclaration.generate(codeBase);
		
	}

}
