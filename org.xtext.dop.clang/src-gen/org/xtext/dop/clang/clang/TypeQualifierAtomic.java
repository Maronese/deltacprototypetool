/**
 */
package org.xtext.dop.clang.clang;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type Qualifier Atomic</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getTypeQualifierAtomic()
 * @model
 * @generated
 */
public interface TypeQualifierAtomic extends TypeQualifier
{
} // TypeQualifierAtomic
