package org.xtext.dop.codebase.parser.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.parser.antlr.Lexer;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalCodeBaseLexer extends Lexer {
    public static final int RULE_A=109;
    public static final int RULE_STRUCT=42;
    public static final int RULE_DEC_OP=83;
    public static final int RULE_ATOMIC=28;
    public static final int RULE_REGISTER=49;
    public static final int RULE_G_OP=15;
    public static final int RULE_WHILE=96;
    public static final int RULE_NOT_OP=104;
    public static final int RULE_FUNC_NAME=89;
    public static final int RULE_RETURN=102;
    public static final int RULE_LAND_OP=58;
    public static final int RULE_INLINE=52;
    public static final int RULE_SUB_ASSIGN=66;
    public static final int RULE_ASSIGN=26;
    public static final int RULE_CUSTOM=30;
    public static final int RULE_COLON=56;
    public static final int RULE_L_OP=13;
    public static final int RULE_TILDE=103;
    public static final int RULE_HP=111;
    public static final int RULE_AUTO=48;
    public static final int RULE_INT=33;
    public static final int RULE_ML_COMMENT=119;
    public static final int RULE_RESTRICT=20;
    public static final int RULE_LBRACKET=23;
    public static final int RULE_MUL_ASSIGN=62;
    public static final int RULE_MODIFIES=10;
    public static final int RULE_IMAGINARY=41;
    public static final int RULE_ENUM=44;
    public static final int RULE_MINUS_OP=77;
    public static final int RULE_MULT_OP=14;
    public static final int RULE_I_CONSTANT=86;
    public static final int RULE_COMPLEX=40;
    public static final int RULE_IS=115;
    public static final int RULE_CONST=50;
    public static final int RULE_VOLATILE=51;
    public static final int RULE_DEFAULT=90;
    public static final int RULE_IF=93;
    public static final int RULE_ELLIPSIS=25;
    public static final int RULE_DOT=17;
    public static final int RULE_DIV_OP=78;
    public static final int RULE_AND_OP=61;
    public static final int RULE_SIGNED=37;
    public static final int RULE_LEFT_ASSIGN=67;
    public static final int RULE_STATIC_ASSERT=27;
    public static final int RULE_UNSIGNED=38;
    public static final int RULE_ALIGNAS=54;
    public static final int RULE_EQ_OP=72;
    public static final int RULE_STATIC=46;
    public static final int RULE_LEFT_OP=80;
    public static final int RULE_CONTINUE=100;
    public static final int RULE_BOOL=39;
    public static final int RULE_AND_ASSIGN=69;
    public static final int RULE_O=105;
    public static final int RULE_SWITCH=95;
    public static final int RULE_P=113;
    public static final int RULE_ADDS=8;
    public static final int RULE_L=108;
    public static final int RULE_OR_OP=59;
    public static final int RULE_FLOAT=35;
    public static final int RULE_F_CONSTANT=87;
    public static final int RULE_H=110;
    public static final int RULE_E=112;
    public static final int RULE_INCLUDE=12;
    public static final int RULE_D=106;
    public static final int RULE_THREAD_LOCAL=47;
    public static final int RULE_NORETURN=53;
    public static final int RULE_DQUOTE=122;
    public static final int RULE_REMOVES=9;
    public static final int RULE_CP=116;
    public static final int RULE_LPAREN=21;
    public static final int RULE_EXTERN=45;
    public static final int RULE_GE_OP=75;
    public static final int RULE_CHAR=31;
    public static final int RULE_SP=117;
    public static final int RULE_COMMA=19;
    public static final int RULE_MOD_ASSIGN=64;
    public static final int RULE_SHORT=32;
    public static final int RULE_RCBRACKET=7;
    public static final int RULE_CASE=92;
    public static final int RULE_DELTA=4;
    public static final int RULE_XOR_ASSIGN=70;
    public static final int RULE_DO=97;
    public static final int RULE_INC_OP=82;
    public static final int RULE_UNION=43;
    public static final int RULE_SEMICOLON=11;
    public static final int RULE_ELSE=94;
    public static final int RULE_TYPEDEF=18;
    public static final int RULE_LCBRACKET=6;
    public static final int RULE_IDENTIFIER=5;
    public static final int RULE_XOR_OP=60;
    public static final int RULE_OR_ASSIGN=71;
    public static final int RULE_NE_OP=73;
    public static final int RULE_ALIGNOF=85;
    public static final int RULE_PTR_OP=91;
    public static final int RULE_ES=118;
    public static final int RULE_STRING_LITERAL=16;
    public static final int RULE_SL_COMMENT=120;
    public static final int RULE_LOR_OP=57;
    public static final int RULE_DOUBLE=36;
    public static final int RULE_BREAK=101;
    public static final int RULE_ADD_ASSIGN=65;
    public static final int RULE_Q_OP=55;
    public static final int RULE_FOR=98;
    public static final int EOF=-1;
    public static final int RULE_VOID=29;
    public static final int RULE_ADD_OP=76;
    public static final int RULE_SIZEOF=84;
    public static final int RULE_GENERIC=88;
    public static final int RULE_GOTO=99;
    public static final int RULE_WS=121;
    public static final int RULE_RIGHT_ASSIGN=68;
    public static final int RULE_FS=114;
    public static final int RULE_DIV_ASSIGN=63;
    public static final int RULE_RIGHT_OP=81;
    public static final int RULE_RPAREN=22;
    public static final int RULE_MOD_OP=79;
    public static final int RULE_NZ=107;
    public static final int RULE_LE_OP=74;
    public static final int RULE_LONG=34;
    public static final int RULE_RBRACKET=24;

    // delegates
    // delegators

    public InternalCodeBaseLexer() {;} 
    public InternalCodeBaseLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public InternalCodeBaseLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g"; }

    // $ANTLR start "RULE_DELTA"
    public final void mRULE_DELTA() throws RecognitionException {
        try {
            int _type = RULE_DELTA;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6353:12: ( 'delta' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6353:14: 'delta'
            {
            match("delta"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_DELTA"

    // $ANTLR start "RULE_ADDS"
    public final void mRULE_ADDS() throws RecognitionException {
        try {
            int _type = RULE_ADDS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6355:11: ( 'adds' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6355:13: 'adds'
            {
            match("adds"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ADDS"

    // $ANTLR start "RULE_REMOVES"
    public final void mRULE_REMOVES() throws RecognitionException {
        try {
            int _type = RULE_REMOVES;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6357:14: ( 'removes' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6357:16: 'removes'
            {
            match("removes"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_REMOVES"

    // $ANTLR start "RULE_MODIFIES"
    public final void mRULE_MODIFIES() throws RecognitionException {
        try {
            int _type = RULE_MODIFIES;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6359:15: ( 'modifies' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6359:17: 'modifies'
            {
            match("modifies"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_MODIFIES"

    // $ANTLR start "RULE_O"
    public final void mRULE_O() throws RecognitionException {
        try {
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6361:17: ( '0' .. '7' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6361:19: '0' .. '7'
            {
            matchRange('0','7'); 

            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_O"

    // $ANTLR start "RULE_D"
    public final void mRULE_D() throws RecognitionException {
        try {
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6363:17: ( '0' .. '9' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6363:19: '0' .. '9'
            {
            matchRange('0','9'); 

            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_D"

    // $ANTLR start "RULE_NZ"
    public final void mRULE_NZ() throws RecognitionException {
        try {
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6365:18: ( '1' .. '9' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6365:20: '1' .. '9'
            {
            matchRange('1','9'); 

            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_NZ"

    // $ANTLR start "RULE_L"
    public final void mRULE_L() throws RecognitionException {
        try {
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6367:17: ( ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6367:19: ( 'a' .. 'z' | 'A' .. 'Z' | '_' )
            {
            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_L"

    // $ANTLR start "RULE_A"
    public final void mRULE_A() throws RecognitionException {
        try {
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6369:17: ( ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' ) )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6369:19: ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )
            {
            if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_A"

    // $ANTLR start "RULE_H"
    public final void mRULE_H() throws RecognitionException {
        try {
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6371:17: ( ( 'a' .. 'f' | 'A' .. 'F' | '0' .. '9' ) )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6371:19: ( 'a' .. 'f' | 'A' .. 'F' | '0' .. '9' )
            {
            if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='F')||(input.LA(1)>='a' && input.LA(1)<='f') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_H"

    // $ANTLR start "RULE_HP"
    public final void mRULE_HP() throws RecognitionException {
        try {
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6373:18: ( ( '0x' | '0X' ) )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6373:20: ( '0x' | '0X' )
            {
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6373:20: ( '0x' | '0X' )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0=='0') ) {
                int LA1_1 = input.LA(2);

                if ( (LA1_1=='x') ) {
                    alt1=1;
                }
                else if ( (LA1_1=='X') ) {
                    alt1=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 1, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6373:21: '0x'
                    {
                    match("0x"); 


                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6373:26: '0X'
                    {
                    match("0X"); 


                    }
                    break;

            }


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_HP"

    // $ANTLR start "RULE_E"
    public final void mRULE_E() throws RecognitionException {
        try {
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6375:17: ( ( 'E' | 'e' ) ( '+' | '-' )? ( RULE_D )+ )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6375:19: ( 'E' | 'e' ) ( '+' | '-' )? ( RULE_D )+
            {
            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6375:29: ( '+' | '-' )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0=='+'||LA2_0=='-') ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:
                    {
                    if ( input.LA(1)=='+'||input.LA(1)=='-' ) {
                        input.consume();

                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;}


                    }
                    break;

            }

            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6375:40: ( RULE_D )+
            int cnt3=0;
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( ((LA3_0>='0' && LA3_0<='9')) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6375:40: RULE_D
            	    {
            	    mRULE_D(); 

            	    }
            	    break;

            	default :
            	    if ( cnt3 >= 1 ) break loop3;
                        EarlyExitException eee =
                            new EarlyExitException(3, input);
                        throw eee;
                }
                cnt3++;
            } while (true);


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_E"

    // $ANTLR start "RULE_P"
    public final void mRULE_P() throws RecognitionException {
        try {
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6377:17: ( ( 'P' | 'p' ) ( '+' | '-' )? ( RULE_D )+ )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6377:19: ( 'P' | 'p' ) ( '+' | '-' )? ( RULE_D )+
            {
            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6377:29: ( '+' | '-' )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0=='+'||LA4_0=='-') ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:
                    {
                    if ( input.LA(1)=='+'||input.LA(1)=='-' ) {
                        input.consume();

                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;}


                    }
                    break;

            }

            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6377:40: ( RULE_D )+
            int cnt5=0;
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( ((LA5_0>='0' && LA5_0<='9')) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6377:40: RULE_D
            	    {
            	    mRULE_D(); 

            	    }
            	    break;

            	default :
            	    if ( cnt5 >= 1 ) break loop5;
                        EarlyExitException eee =
                            new EarlyExitException(5, input);
                        throw eee;
                }
                cnt5++;
            } while (true);


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_P"

    // $ANTLR start "RULE_FS"
    public final void mRULE_FS() throws RecognitionException {
        try {
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6379:18: ( ( 'f' | 'F' | 'l' | 'L' ) )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6379:20: ( 'f' | 'F' | 'l' | 'L' )
            {
            if ( input.LA(1)=='F'||input.LA(1)=='L'||input.LA(1)=='f'||input.LA(1)=='l' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_FS"

    // $ANTLR start "RULE_IS"
    public final void mRULE_IS() throws RecognitionException {
        try {
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6381:18: ( ( ( 'u' | 'U' ) ( 'l' | 'L' | 'll' | 'LL' )? | ( 'l' | 'L' | 'll' | 'LL' ) ( 'u' | 'U' )? ) )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6381:20: ( ( 'u' | 'U' ) ( 'l' | 'L' | 'll' | 'LL' )? | ( 'l' | 'L' | 'll' | 'LL' ) ( 'u' | 'U' )? )
            {
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6381:20: ( ( 'u' | 'U' ) ( 'l' | 'L' | 'll' | 'LL' )? | ( 'l' | 'L' | 'll' | 'LL' ) ( 'u' | 'U' )? )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0=='U'||LA9_0=='u') ) {
                alt9=1;
            }
            else if ( (LA9_0=='L'||LA9_0=='l') ) {
                alt9=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }
            switch (alt9) {
                case 1 :
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6381:21: ( 'u' | 'U' ) ( 'l' | 'L' | 'll' | 'LL' )?
                    {
                    if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                        input.consume();

                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;}

                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6381:31: ( 'l' | 'L' | 'll' | 'LL' )?
                    int alt6=5;
                    int LA6_0 = input.LA(1);

                    if ( (LA6_0=='l') ) {
                        int LA6_1 = input.LA(2);

                        if ( (LA6_1=='l') ) {
                            alt6=3;
                        }
                    }
                    else if ( (LA6_0=='L') ) {
                        int LA6_2 = input.LA(2);

                        if ( (LA6_2=='L') ) {
                            alt6=4;
                        }
                    }
                    switch (alt6) {
                        case 1 :
                            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6381:32: 'l'
                            {
                            match('l'); 

                            }
                            break;
                        case 2 :
                            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6381:36: 'L'
                            {
                            match('L'); 

                            }
                            break;
                        case 3 :
                            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6381:40: 'll'
                            {
                            match("ll"); 


                            }
                            break;
                        case 4 :
                            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6381:45: 'LL'
                            {
                            match("LL"); 


                            }
                            break;

                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6381:52: ( 'l' | 'L' | 'll' | 'LL' ) ( 'u' | 'U' )?
                    {
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6381:52: ( 'l' | 'L' | 'll' | 'LL' )
                    int alt7=4;
                    int LA7_0 = input.LA(1);

                    if ( (LA7_0=='l') ) {
                        int LA7_1 = input.LA(2);

                        if ( (LA7_1=='l') ) {
                            alt7=3;
                        }
                        else {
                            alt7=1;}
                    }
                    else if ( (LA7_0=='L') ) {
                        int LA7_2 = input.LA(2);

                        if ( (LA7_2=='L') ) {
                            alt7=4;
                        }
                        else {
                            alt7=2;}
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 7, 0, input);

                        throw nvae;
                    }
                    switch (alt7) {
                        case 1 :
                            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6381:53: 'l'
                            {
                            match('l'); 

                            }
                            break;
                        case 2 :
                            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6381:57: 'L'
                            {
                            match('L'); 

                            }
                            break;
                        case 3 :
                            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6381:61: 'll'
                            {
                            match("ll"); 


                            }
                            break;
                        case 4 :
                            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6381:66: 'LL'
                            {
                            match("LL"); 


                            }
                            break;

                    }

                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6381:72: ( 'u' | 'U' )?
                    int alt8=2;
                    int LA8_0 = input.LA(1);

                    if ( (LA8_0=='U'||LA8_0=='u') ) {
                        alt8=1;
                    }
                    switch (alt8) {
                        case 1 :
                            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:
                            {
                            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                                input.consume();

                            }
                            else {
                                MismatchedSetException mse = new MismatchedSetException(null,input);
                                recover(mse);
                                throw mse;}


                            }
                            break;

                    }


                    }
                    break;

            }


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_IS"

    // $ANTLR start "RULE_CP"
    public final void mRULE_CP() throws RecognitionException {
        try {
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6383:18: ( ( 'u' | 'U' | 'L' ) )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6383:20: ( 'u' | 'U' | 'L' )
            {
            if ( input.LA(1)=='L'||input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_CP"

    // $ANTLR start "RULE_SP"
    public final void mRULE_SP() throws RecognitionException {
        try {
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6385:18: ( ( 'u8' | 'u' | 'U' | 'L' ) )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6385:20: ( 'u8' | 'u' | 'U' | 'L' )
            {
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6385:20: ( 'u8' | 'u' | 'U' | 'L' )
            int alt10=4;
            switch ( input.LA(1) ) {
            case 'u':
                {
                int LA10_1 = input.LA(2);

                if ( (LA10_1=='8') ) {
                    alt10=1;
                }
                else {
                    alt10=2;}
                }
                break;
            case 'U':
                {
                alt10=3;
                }
                break;
            case 'L':
                {
                alt10=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }

            switch (alt10) {
                case 1 :
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6385:21: 'u8'
                    {
                    match("u8"); 


                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6385:26: 'u'
                    {
                    match('u'); 

                    }
                    break;
                case 3 :
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6385:30: 'U'
                    {
                    match('U'); 

                    }
                    break;
                case 4 :
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6385:34: 'L'
                    {
                    match('L'); 

                    }
                    break;

            }


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_SP"

    // $ANTLR start "RULE_ES"
    public final void mRULE_ES() throws RecognitionException {
        try {
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6387:18: ( '\\\\' ( '\\'' | '\"' | '?' | '\\\\' | 'a' | 'b' | 'f' | 'n' | 'r' | 't' | 'v' | ( RULE_O | RULE_O RULE_O | RULE_O RULE_O RULE_O ) | 'x' ( RULE_H )+ ) )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6387:20: '\\\\' ( '\\'' | '\"' | '?' | '\\\\' | 'a' | 'b' | 'f' | 'n' | 'r' | 't' | 'v' | ( RULE_O | RULE_O RULE_O | RULE_O RULE_O RULE_O ) | 'x' ( RULE_H )+ )
            {
            match('\\'); 
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6387:25: ( '\\'' | '\"' | '?' | '\\\\' | 'a' | 'b' | 'f' | 'n' | 'r' | 't' | 'v' | ( RULE_O | RULE_O RULE_O | RULE_O RULE_O RULE_O ) | 'x' ( RULE_H )+ )
            int alt13=13;
            switch ( input.LA(1) ) {
            case '\'':
                {
                alt13=1;
                }
                break;
            case '\"':
                {
                alt13=2;
                }
                break;
            case '?':
                {
                alt13=3;
                }
                break;
            case '\\':
                {
                alt13=4;
                }
                break;
            case 'a':
                {
                alt13=5;
                }
                break;
            case 'b':
                {
                alt13=6;
                }
                break;
            case 'f':
                {
                alt13=7;
                }
                break;
            case 'n':
                {
                alt13=8;
                }
                break;
            case 'r':
                {
                alt13=9;
                }
                break;
            case 't':
                {
                alt13=10;
                }
                break;
            case 'v':
                {
                alt13=11;
                }
                break;
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
                {
                alt13=12;
                }
                break;
            case 'x':
                {
                alt13=13;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;
            }

            switch (alt13) {
                case 1 :
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6387:26: '\\''
                    {
                    match('\''); 

                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6387:31: '\"'
                    {
                    match('\"'); 

                    }
                    break;
                case 3 :
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6387:35: '?'
                    {
                    match('?'); 

                    }
                    break;
                case 4 :
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6387:39: '\\\\'
                    {
                    match('\\'); 

                    }
                    break;
                case 5 :
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6387:44: 'a'
                    {
                    match('a'); 

                    }
                    break;
                case 6 :
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6387:48: 'b'
                    {
                    match('b'); 

                    }
                    break;
                case 7 :
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6387:52: 'f'
                    {
                    match('f'); 

                    }
                    break;
                case 8 :
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6387:56: 'n'
                    {
                    match('n'); 

                    }
                    break;
                case 9 :
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6387:60: 'r'
                    {
                    match('r'); 

                    }
                    break;
                case 10 :
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6387:64: 't'
                    {
                    match('t'); 

                    }
                    break;
                case 11 :
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6387:68: 'v'
                    {
                    match('v'); 

                    }
                    break;
                case 12 :
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6387:72: ( RULE_O | RULE_O RULE_O | RULE_O RULE_O RULE_O )
                    {
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6387:72: ( RULE_O | RULE_O RULE_O | RULE_O RULE_O RULE_O )
                    int alt11=3;
                    int LA11_0 = input.LA(1);

                    if ( ((LA11_0>='0' && LA11_0<='7')) ) {
                        int LA11_1 = input.LA(2);

                        if ( ((LA11_1>='0' && LA11_1<='7')) ) {
                            int LA11_3 = input.LA(3);

                            if ( ((LA11_3>='0' && LA11_3<='7')) ) {
                                alt11=3;
                            }
                            else {
                                alt11=2;}
                        }
                        else {
                            alt11=1;}
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 11, 0, input);

                        throw nvae;
                    }
                    switch (alt11) {
                        case 1 :
                            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6387:73: RULE_O
                            {
                            mRULE_O(); 

                            }
                            break;
                        case 2 :
                            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6387:80: RULE_O RULE_O
                            {
                            mRULE_O(); 
                            mRULE_O(); 

                            }
                            break;
                        case 3 :
                            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6387:94: RULE_O RULE_O RULE_O
                            {
                            mRULE_O(); 
                            mRULE_O(); 
                            mRULE_O(); 

                            }
                            break;

                    }


                    }
                    break;
                case 13 :
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6387:116: 'x' ( RULE_H )+
                    {
                    match('x'); 
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6387:120: ( RULE_H )+
                    int cnt12=0;
                    loop12:
                    do {
                        int alt12=2;
                        int LA12_0 = input.LA(1);

                        if ( ((LA12_0>='0' && LA12_0<='9')||(LA12_0>='A' && LA12_0<='F')||(LA12_0>='a' && LA12_0<='f')) ) {
                            alt12=1;
                        }


                        switch (alt12) {
                    	case 1 :
                    	    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6387:120: RULE_H
                    	    {
                    	    mRULE_H(); 

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt12 >= 1 ) break loop12;
                                EarlyExitException eee =
                                    new EarlyExitException(12, input);
                                throw eee;
                        }
                        cnt12++;
                    } while (true);


                    }
                    break;

            }


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_ES"

    // $ANTLR start "RULE_ML_COMMENT"
    public final void mRULE_ML_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_ML_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6389:17: ( '/*' ( options {greedy=false; } : . )* '*/' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6389:19: '/*' ( options {greedy=false; } : . )* '*/'
            {
            match("/*"); 

            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6389:24: ( options {greedy=false; } : . )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( (LA14_0=='*') ) {
                    int LA14_1 = input.LA(2);

                    if ( (LA14_1=='/') ) {
                        alt14=2;
                    }
                    else if ( ((LA14_1>='\u0000' && LA14_1<='.')||(LA14_1>='0' && LA14_1<='\uFFFF')) ) {
                        alt14=1;
                    }


                }
                else if ( ((LA14_0>='\u0000' && LA14_0<=')')||(LA14_0>='+' && LA14_0<='\uFFFF')) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6389:52: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);

            match("*/"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ML_COMMENT"

    // $ANTLR start "RULE_SL_COMMENT"
    public final void mRULE_SL_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_SL_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6391:17: ( '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )? )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6391:19: '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )?
            {
            match("//"); 

            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6391:24: (~ ( ( '\\n' | '\\r' ) ) )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( ((LA15_0>='\u0000' && LA15_0<='\t')||(LA15_0>='\u000B' && LA15_0<='\f')||(LA15_0>='\u000E' && LA15_0<='\uFFFF')) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6391:24: ~ ( ( '\\n' | '\\r' ) )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='\uFFFF') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6391:40: ( ( '\\r' )? '\\n' )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0=='\n'||LA17_0=='\r') ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6391:41: ( '\\r' )? '\\n'
                    {
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6391:41: ( '\\r' )?
                    int alt16=2;
                    int LA16_0 = input.LA(1);

                    if ( (LA16_0=='\r') ) {
                        alt16=1;
                    }
                    switch (alt16) {
                        case 1 :
                            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6391:41: '\\r'
                            {
                            match('\r'); 

                            }
                            break;

                    }

                    match('\n'); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SL_COMMENT"

    // $ANTLR start "RULE_WS"
    public final void mRULE_WS() throws RecognitionException {
        try {
            int _type = RULE_WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6393:9: ( ( ' ' | '\\t' | '\\b' | '\\n' | '\\f' | '\\r' ) )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6393:11: ( ' ' | '\\t' | '\\b' | '\\n' | '\\f' | '\\r' )
            {
            if ( (input.LA(1)>='\b' && input.LA(1)<='\n')||(input.LA(1)>='\f' && input.LA(1)<='\r')||input.LA(1)==' ' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_WS"

    // $ANTLR start "RULE_AUTO"
    public final void mRULE_AUTO() throws RecognitionException {
        try {
            int _type = RULE_AUTO;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6395:11: ( 'auto' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6395:13: 'auto'
            {
            match("auto"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_AUTO"

    // $ANTLR start "RULE_BREAK"
    public final void mRULE_BREAK() throws RecognitionException {
        try {
            int _type = RULE_BREAK;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6397:12: ( 'break' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6397:14: 'break'
            {
            match("break"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_BREAK"

    // $ANTLR start "RULE_CASE"
    public final void mRULE_CASE() throws RecognitionException {
        try {
            int _type = RULE_CASE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6399:11: ( 'case' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6399:13: 'case'
            {
            match("case"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_CASE"

    // $ANTLR start "RULE_CHAR"
    public final void mRULE_CHAR() throws RecognitionException {
        try {
            int _type = RULE_CHAR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6401:11: ( 'char' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6401:13: 'char'
            {
            match("char"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_CHAR"

    // $ANTLR start "RULE_CONST"
    public final void mRULE_CONST() throws RecognitionException {
        try {
            int _type = RULE_CONST;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6403:12: ( 'const' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6403:14: 'const'
            {
            match("const"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_CONST"

    // $ANTLR start "RULE_CONTINUE"
    public final void mRULE_CONTINUE() throws RecognitionException {
        try {
            int _type = RULE_CONTINUE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6405:15: ( 'continue' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6405:17: 'continue'
            {
            match("continue"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_CONTINUE"

    // $ANTLR start "RULE_DEFAULT"
    public final void mRULE_DEFAULT() throws RecognitionException {
        try {
            int _type = RULE_DEFAULT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6407:14: ( 'default' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6407:16: 'default'
            {
            match("default"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_DEFAULT"

    // $ANTLR start "RULE_DO"
    public final void mRULE_DO() throws RecognitionException {
        try {
            int _type = RULE_DO;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6409:9: ( 'do' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6409:11: 'do'
            {
            match("do"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_DO"

    // $ANTLR start "RULE_DOUBLE"
    public final void mRULE_DOUBLE() throws RecognitionException {
        try {
            int _type = RULE_DOUBLE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6411:13: ( 'double' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6411:15: 'double'
            {
            match("double"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_DOUBLE"

    // $ANTLR start "RULE_ELSE"
    public final void mRULE_ELSE() throws RecognitionException {
        try {
            int _type = RULE_ELSE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6413:11: ( 'else' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6413:13: 'else'
            {
            match("else"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ELSE"

    // $ANTLR start "RULE_ENUM"
    public final void mRULE_ENUM() throws RecognitionException {
        try {
            int _type = RULE_ENUM;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6415:11: ( 'enum' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6415:13: 'enum'
            {
            match("enum"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ENUM"

    // $ANTLR start "RULE_EXTERN"
    public final void mRULE_EXTERN() throws RecognitionException {
        try {
            int _type = RULE_EXTERN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6417:13: ( 'extern' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6417:15: 'extern'
            {
            match("extern"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_EXTERN"

    // $ANTLR start "RULE_FLOAT"
    public final void mRULE_FLOAT() throws RecognitionException {
        try {
            int _type = RULE_FLOAT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6419:12: ( 'float' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6419:14: 'float'
            {
            match("float"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_FLOAT"

    // $ANTLR start "RULE_FOR"
    public final void mRULE_FOR() throws RecognitionException {
        try {
            int _type = RULE_FOR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6421:10: ( 'for' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6421:12: 'for'
            {
            match("for"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_FOR"

    // $ANTLR start "RULE_GOTO"
    public final void mRULE_GOTO() throws RecognitionException {
        try {
            int _type = RULE_GOTO;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6423:11: ( 'goto' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6423:13: 'goto'
            {
            match("goto"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_GOTO"

    // $ANTLR start "RULE_IF"
    public final void mRULE_IF() throws RecognitionException {
        try {
            int _type = RULE_IF;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6425:9: ( 'if' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6425:11: 'if'
            {
            match("if"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_IF"

    // $ANTLR start "RULE_INLINE"
    public final void mRULE_INLINE() throws RecognitionException {
        try {
            int _type = RULE_INLINE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6427:13: ( 'inline' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6427:15: 'inline'
            {
            match("inline"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_INLINE"

    // $ANTLR start "RULE_INT"
    public final void mRULE_INT() throws RecognitionException {
        try {
            int _type = RULE_INT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6429:10: ( 'int' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6429:12: 'int'
            {
            match("int"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_INT"

    // $ANTLR start "RULE_LONG"
    public final void mRULE_LONG() throws RecognitionException {
        try {
            int _type = RULE_LONG;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6431:11: ( 'long' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6431:13: 'long'
            {
            match("long"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_LONG"

    // $ANTLR start "RULE_REGISTER"
    public final void mRULE_REGISTER() throws RecognitionException {
        try {
            int _type = RULE_REGISTER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6433:15: ( 'register' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6433:17: 'register'
            {
            match("register"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_REGISTER"

    // $ANTLR start "RULE_RESTRICT"
    public final void mRULE_RESTRICT() throws RecognitionException {
        try {
            int _type = RULE_RESTRICT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6435:15: ( 'restrict' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6435:17: 'restrict'
            {
            match("restrict"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_RESTRICT"

    // $ANTLR start "RULE_RETURN"
    public final void mRULE_RETURN() throws RecognitionException {
        try {
            int _type = RULE_RETURN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6437:13: ( 'return' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6437:15: 'return'
            {
            match("return"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_RETURN"

    // $ANTLR start "RULE_SHORT"
    public final void mRULE_SHORT() throws RecognitionException {
        try {
            int _type = RULE_SHORT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6439:12: ( 'short' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6439:14: 'short'
            {
            match("short"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SHORT"

    // $ANTLR start "RULE_SIGNED"
    public final void mRULE_SIGNED() throws RecognitionException {
        try {
            int _type = RULE_SIGNED;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6441:13: ( 'signed' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6441:15: 'signed'
            {
            match("signed"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SIGNED"

    // $ANTLR start "RULE_SIZEOF"
    public final void mRULE_SIZEOF() throws RecognitionException {
        try {
            int _type = RULE_SIZEOF;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6443:13: ( 'sizeof' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6443:15: 'sizeof'
            {
            match("sizeof"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SIZEOF"

    // $ANTLR start "RULE_STATIC"
    public final void mRULE_STATIC() throws RecognitionException {
        try {
            int _type = RULE_STATIC;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6445:13: ( 'static' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6445:15: 'static'
            {
            match("static"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_STATIC"

    // $ANTLR start "RULE_STRUCT"
    public final void mRULE_STRUCT() throws RecognitionException {
        try {
            int _type = RULE_STRUCT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6447:13: ( 'struct' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6447:15: 'struct'
            {
            match("struct"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_STRUCT"

    // $ANTLR start "RULE_SWITCH"
    public final void mRULE_SWITCH() throws RecognitionException {
        try {
            int _type = RULE_SWITCH;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6449:13: ( 'switch' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6449:15: 'switch'
            {
            match("switch"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SWITCH"

    // $ANTLR start "RULE_TYPEDEF"
    public final void mRULE_TYPEDEF() throws RecognitionException {
        try {
            int _type = RULE_TYPEDEF;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6451:14: ( 'typedef' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6451:16: 'typedef'
            {
            match("typedef"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_TYPEDEF"

    // $ANTLR start "RULE_UNION"
    public final void mRULE_UNION() throws RecognitionException {
        try {
            int _type = RULE_UNION;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6453:12: ( 'union' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6453:14: 'union'
            {
            match("union"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_UNION"

    // $ANTLR start "RULE_UNSIGNED"
    public final void mRULE_UNSIGNED() throws RecognitionException {
        try {
            int _type = RULE_UNSIGNED;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6455:15: ( 'unsigned' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6455:17: 'unsigned'
            {
            match("unsigned"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_UNSIGNED"

    // $ANTLR start "RULE_VOID"
    public final void mRULE_VOID() throws RecognitionException {
        try {
            int _type = RULE_VOID;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6457:11: ( 'void' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6457:13: 'void'
            {
            match("void"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_VOID"

    // $ANTLR start "RULE_VOLATILE"
    public final void mRULE_VOLATILE() throws RecognitionException {
        try {
            int _type = RULE_VOLATILE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6459:15: ( 'volatile' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6459:17: 'volatile'
            {
            match("volatile"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_VOLATILE"

    // $ANTLR start "RULE_WHILE"
    public final void mRULE_WHILE() throws RecognitionException {
        try {
            int _type = RULE_WHILE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6461:12: ( 'while' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6461:14: 'while'
            {
            match("while"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_WHILE"

    // $ANTLR start "RULE_ALIGNAS"
    public final void mRULE_ALIGNAS() throws RecognitionException {
        try {
            int _type = RULE_ALIGNAS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6463:14: ( '_Alignas' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6463:16: '_Alignas'
            {
            match("_Alignas"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ALIGNAS"

    // $ANTLR start "RULE_ALIGNOF"
    public final void mRULE_ALIGNOF() throws RecognitionException {
        try {
            int _type = RULE_ALIGNOF;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6465:14: ( '_Alignof' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6465:16: '_Alignof'
            {
            match("_Alignof"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ALIGNOF"

    // $ANTLR start "RULE_ATOMIC"
    public final void mRULE_ATOMIC() throws RecognitionException {
        try {
            int _type = RULE_ATOMIC;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6467:13: ( '_Atomic' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6467:15: '_Atomic'
            {
            match("_Atomic"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ATOMIC"

    // $ANTLR start "RULE_BOOL"
    public final void mRULE_BOOL() throws RecognitionException {
        try {
            int _type = RULE_BOOL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6469:11: ( '_Bool' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6469:13: '_Bool'
            {
            match("_Bool"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_BOOL"

    // $ANTLR start "RULE_COMPLEX"
    public final void mRULE_COMPLEX() throws RecognitionException {
        try {
            int _type = RULE_COMPLEX;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6471:14: ( '_Complex' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6471:16: '_Complex'
            {
            match("_Complex"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_COMPLEX"

    // $ANTLR start "RULE_GENERIC"
    public final void mRULE_GENERIC() throws RecognitionException {
        try {
            int _type = RULE_GENERIC;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6473:14: ( '_Generic' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6473:16: '_Generic'
            {
            match("_Generic"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_GENERIC"

    // $ANTLR start "RULE_IMAGINARY"
    public final void mRULE_IMAGINARY() throws RecognitionException {
        try {
            int _type = RULE_IMAGINARY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6475:16: ( '_Imaginary' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6475:18: '_Imaginary'
            {
            match("_Imaginary"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_IMAGINARY"

    // $ANTLR start "RULE_NORETURN"
    public final void mRULE_NORETURN() throws RecognitionException {
        try {
            int _type = RULE_NORETURN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6477:15: ( '_Noreturn' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6477:17: '_Noreturn'
            {
            match("_Noreturn"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_NORETURN"

    // $ANTLR start "RULE_STATIC_ASSERT"
    public final void mRULE_STATIC_ASSERT() throws RecognitionException {
        try {
            int _type = RULE_STATIC_ASSERT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6479:20: ( '_Static_assert' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6479:22: '_Static_assert'
            {
            match("_Static_assert"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_STATIC_ASSERT"

    // $ANTLR start "RULE_THREAD_LOCAL"
    public final void mRULE_THREAD_LOCAL() throws RecognitionException {
        try {
            int _type = RULE_THREAD_LOCAL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6481:19: ( '_Thread_local' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6481:21: '_Thread_local'
            {
            match("_Thread_local"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_THREAD_LOCAL"

    // $ANTLR start "RULE_FUNC_NAME"
    public final void mRULE_FUNC_NAME() throws RecognitionException {
        try {
            int _type = RULE_FUNC_NAME;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6483:16: ( '__func__' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6483:18: '__func__'
            {
            match("__func__"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_FUNC_NAME"

    // $ANTLR start "RULE_IDENTIFIER"
    public final void mRULE_IDENTIFIER() throws RecognitionException {
        try {
            int _type = RULE_IDENTIFIER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6485:17: ( RULE_L ( RULE_A )* )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6485:19: RULE_L ( RULE_A )*
            {
            mRULE_L(); 
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6485:26: ( RULE_A )*
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( ((LA18_0>='0' && LA18_0<='9')||(LA18_0>='A' && LA18_0<='Z')||LA18_0=='_'||(LA18_0>='a' && LA18_0<='z')) ) {
                    alt18=1;
                }


                switch (alt18) {
            	case 1 :
            	    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6485:26: RULE_A
            	    {
            	    mRULE_A(); 

            	    }
            	    break;

            	default :
            	    break loop18;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_IDENTIFIER"

    // $ANTLR start "RULE_CUSTOM"
    public final void mRULE_CUSTOM() throws RecognitionException {
        try {
            int _type = RULE_CUSTOM;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6487:13: ( '$' RULE_L ( RULE_A )* )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6487:15: '$' RULE_L ( RULE_A )*
            {
            match('$'); 
            mRULE_L(); 
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6487:26: ( RULE_A )*
            loop19:
            do {
                int alt19=2;
                int LA19_0 = input.LA(1);

                if ( ((LA19_0>='0' && LA19_0<='9')||(LA19_0>='A' && LA19_0<='Z')||LA19_0=='_'||(LA19_0>='a' && LA19_0<='z')) ) {
                    alt19=1;
                }


                switch (alt19) {
            	case 1 :
            	    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6487:26: RULE_A
            	    {
            	    mRULE_A(); 

            	    }
            	    break;

            	default :
            	    break loop19;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_CUSTOM"

    // $ANTLR start "RULE_I_CONSTANT"
    public final void mRULE_I_CONSTANT() throws RecognitionException {
        try {
            int _type = RULE_I_CONSTANT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6489:17: ( ( RULE_HP ( RULE_H )+ ( RULE_IS )? | RULE_NZ ( RULE_D )* ( RULE_IS )? | '0' ( RULE_O )* ( RULE_IS )? | ( RULE_CP )? '\\'' (~ ( ( '\\'' | '\\\\' | '\\n' ) ) | RULE_ES )+ '\\'' ) )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6489:19: ( RULE_HP ( RULE_H )+ ( RULE_IS )? | RULE_NZ ( RULE_D )* ( RULE_IS )? | '0' ( RULE_O )* ( RULE_IS )? | ( RULE_CP )? '\\'' (~ ( ( '\\'' | '\\\\' | '\\n' ) ) | RULE_ES )+ '\\'' )
            {
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6489:19: ( RULE_HP ( RULE_H )+ ( RULE_IS )? | RULE_NZ ( RULE_D )* ( RULE_IS )? | '0' ( RULE_O )* ( RULE_IS )? | ( RULE_CP )? '\\'' (~ ( ( '\\'' | '\\\\' | '\\n' ) ) | RULE_ES )+ '\\'' )
            int alt28=4;
            switch ( input.LA(1) ) {
            case '0':
                {
                int LA28_1 = input.LA(2);

                if ( (LA28_1=='X'||LA28_1=='x') ) {
                    alt28=1;
                }
                else {
                    alt28=3;}
                }
                break;
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                {
                alt28=2;
                }
                break;
            case '\'':
            case 'L':
            case 'U':
            case 'u':
                {
                alt28=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 28, 0, input);

                throw nvae;
            }

            switch (alt28) {
                case 1 :
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6489:20: RULE_HP ( RULE_H )+ ( RULE_IS )?
                    {
                    mRULE_HP(); 
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6489:28: ( RULE_H )+
                    int cnt20=0;
                    loop20:
                    do {
                        int alt20=2;
                        int LA20_0 = input.LA(1);

                        if ( ((LA20_0>='0' && LA20_0<='9')||(LA20_0>='A' && LA20_0<='F')||(LA20_0>='a' && LA20_0<='f')) ) {
                            alt20=1;
                        }


                        switch (alt20) {
                    	case 1 :
                    	    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6489:28: RULE_H
                    	    {
                    	    mRULE_H(); 

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt20 >= 1 ) break loop20;
                                EarlyExitException eee =
                                    new EarlyExitException(20, input);
                                throw eee;
                        }
                        cnt20++;
                    } while (true);

                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6489:36: ( RULE_IS )?
                    int alt21=2;
                    int LA21_0 = input.LA(1);

                    if ( (LA21_0=='L'||LA21_0=='U'||LA21_0=='l'||LA21_0=='u') ) {
                        alt21=1;
                    }
                    switch (alt21) {
                        case 1 :
                            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6489:36: RULE_IS
                            {
                            mRULE_IS(); 

                            }
                            break;

                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6489:45: RULE_NZ ( RULE_D )* ( RULE_IS )?
                    {
                    mRULE_NZ(); 
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6489:53: ( RULE_D )*
                    loop22:
                    do {
                        int alt22=2;
                        int LA22_0 = input.LA(1);

                        if ( ((LA22_0>='0' && LA22_0<='9')) ) {
                            alt22=1;
                        }


                        switch (alt22) {
                    	case 1 :
                    	    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6489:53: RULE_D
                    	    {
                    	    mRULE_D(); 

                    	    }
                    	    break;

                    	default :
                    	    break loop22;
                        }
                    } while (true);

                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6489:61: ( RULE_IS )?
                    int alt23=2;
                    int LA23_0 = input.LA(1);

                    if ( (LA23_0=='L'||LA23_0=='U'||LA23_0=='l'||LA23_0=='u') ) {
                        alt23=1;
                    }
                    switch (alt23) {
                        case 1 :
                            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6489:61: RULE_IS
                            {
                            mRULE_IS(); 

                            }
                            break;

                    }


                    }
                    break;
                case 3 :
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6489:70: '0' ( RULE_O )* ( RULE_IS )?
                    {
                    match('0'); 
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6489:74: ( RULE_O )*
                    loop24:
                    do {
                        int alt24=2;
                        int LA24_0 = input.LA(1);

                        if ( ((LA24_0>='0' && LA24_0<='7')) ) {
                            alt24=1;
                        }


                        switch (alt24) {
                    	case 1 :
                    	    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6489:74: RULE_O
                    	    {
                    	    mRULE_O(); 

                    	    }
                    	    break;

                    	default :
                    	    break loop24;
                        }
                    } while (true);

                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6489:82: ( RULE_IS )?
                    int alt25=2;
                    int LA25_0 = input.LA(1);

                    if ( (LA25_0=='L'||LA25_0=='U'||LA25_0=='l'||LA25_0=='u') ) {
                        alt25=1;
                    }
                    switch (alt25) {
                        case 1 :
                            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6489:82: RULE_IS
                            {
                            mRULE_IS(); 

                            }
                            break;

                    }


                    }
                    break;
                case 4 :
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6489:91: ( RULE_CP )? '\\'' (~ ( ( '\\'' | '\\\\' | '\\n' ) ) | RULE_ES )+ '\\''
                    {
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6489:91: ( RULE_CP )?
                    int alt26=2;
                    int LA26_0 = input.LA(1);

                    if ( (LA26_0=='L'||LA26_0=='U'||LA26_0=='u') ) {
                        alt26=1;
                    }
                    switch (alt26) {
                        case 1 :
                            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6489:91: RULE_CP
                            {
                            mRULE_CP(); 

                            }
                            break;

                    }

                    match('\''); 
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6489:105: (~ ( ( '\\'' | '\\\\' | '\\n' ) ) | RULE_ES )+
                    int cnt27=0;
                    loop27:
                    do {
                        int alt27=3;
                        int LA27_0 = input.LA(1);

                        if ( ((LA27_0>='\u0000' && LA27_0<='\t')||(LA27_0>='\u000B' && LA27_0<='&')||(LA27_0>='(' && LA27_0<='[')||(LA27_0>=']' && LA27_0<='\uFFFF')) ) {
                            alt27=1;
                        }
                        else if ( (LA27_0=='\\') ) {
                            alt27=2;
                        }


                        switch (alt27) {
                    	case 1 :
                    	    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6489:106: ~ ( ( '\\'' | '\\\\' | '\\n' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='&')||(input.LA(1)>='(' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;
                    	case 2 :
                    	    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6489:126: RULE_ES
                    	    {
                    	    mRULE_ES(); 

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt27 >= 1 ) break loop27;
                                EarlyExitException eee =
                                    new EarlyExitException(27, input);
                                throw eee;
                        }
                        cnt27++;
                    } while (true);

                    match('\''); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_I_CONSTANT"

    // $ANTLR start "RULE_F_CONSTANT"
    public final void mRULE_F_CONSTANT() throws RecognitionException {
        try {
            int _type = RULE_F_CONSTANT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6491:17: ( ( ( RULE_D )+ RULE_E ( RULE_FS )? | ( RULE_D )* '.' ( RULE_D )+ ( RULE_E )? ( RULE_FS )? | ( RULE_D )+ '.' ( RULE_E )? ( RULE_FS )? | RULE_HP ( RULE_H )+ RULE_P ( RULE_FS )? | RULE_HP ( RULE_H )* '.' ( RULE_H )+ RULE_P ( RULE_FS )? | RULE_HP ( RULE_H )+ '.' RULE_P ( RULE_FS )? ) )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6491:19: ( ( RULE_D )+ RULE_E ( RULE_FS )? | ( RULE_D )* '.' ( RULE_D )+ ( RULE_E )? ( RULE_FS )? | ( RULE_D )+ '.' ( RULE_E )? ( RULE_FS )? | RULE_HP ( RULE_H )+ RULE_P ( RULE_FS )? | RULE_HP ( RULE_H )* '.' ( RULE_H )+ RULE_P ( RULE_FS )? | RULE_HP ( RULE_H )+ '.' RULE_P ( RULE_FS )? )
            {
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6491:19: ( ( RULE_D )+ RULE_E ( RULE_FS )? | ( RULE_D )* '.' ( RULE_D )+ ( RULE_E )? ( RULE_FS )? | ( RULE_D )+ '.' ( RULE_E )? ( RULE_FS )? | RULE_HP ( RULE_H )+ RULE_P ( RULE_FS )? | RULE_HP ( RULE_H )* '.' ( RULE_H )+ RULE_P ( RULE_FS )? | RULE_HP ( RULE_H )+ '.' RULE_P ( RULE_FS )? )
            int alt45=6;
            alt45 = dfa45.predict(input);
            switch (alt45) {
                case 1 :
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6491:20: ( RULE_D )+ RULE_E ( RULE_FS )?
                    {
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6491:20: ( RULE_D )+
                    int cnt29=0;
                    loop29:
                    do {
                        int alt29=2;
                        int LA29_0 = input.LA(1);

                        if ( ((LA29_0>='0' && LA29_0<='9')) ) {
                            alt29=1;
                        }


                        switch (alt29) {
                    	case 1 :
                    	    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6491:20: RULE_D
                    	    {
                    	    mRULE_D(); 

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt29 >= 1 ) break loop29;
                                EarlyExitException eee =
                                    new EarlyExitException(29, input);
                                throw eee;
                        }
                        cnt29++;
                    } while (true);

                    mRULE_E(); 
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6491:35: ( RULE_FS )?
                    int alt30=2;
                    int LA30_0 = input.LA(1);

                    if ( (LA30_0=='F'||LA30_0=='L'||LA30_0=='f'||LA30_0=='l') ) {
                        alt30=1;
                    }
                    switch (alt30) {
                        case 1 :
                            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6491:35: RULE_FS
                            {
                            mRULE_FS(); 

                            }
                            break;

                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6491:44: ( RULE_D )* '.' ( RULE_D )+ ( RULE_E )? ( RULE_FS )?
                    {
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6491:44: ( RULE_D )*
                    loop31:
                    do {
                        int alt31=2;
                        int LA31_0 = input.LA(1);

                        if ( ((LA31_0>='0' && LA31_0<='9')) ) {
                            alt31=1;
                        }


                        switch (alt31) {
                    	case 1 :
                    	    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6491:44: RULE_D
                    	    {
                    	    mRULE_D(); 

                    	    }
                    	    break;

                    	default :
                    	    break loop31;
                        }
                    } while (true);

                    match('.'); 
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6491:56: ( RULE_D )+
                    int cnt32=0;
                    loop32:
                    do {
                        int alt32=2;
                        int LA32_0 = input.LA(1);

                        if ( ((LA32_0>='0' && LA32_0<='9')) ) {
                            alt32=1;
                        }


                        switch (alt32) {
                    	case 1 :
                    	    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6491:56: RULE_D
                    	    {
                    	    mRULE_D(); 

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt32 >= 1 ) break loop32;
                                EarlyExitException eee =
                                    new EarlyExitException(32, input);
                                throw eee;
                        }
                        cnt32++;
                    } while (true);

                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6491:64: ( RULE_E )?
                    int alt33=2;
                    int LA33_0 = input.LA(1);

                    if ( (LA33_0=='E'||LA33_0=='e') ) {
                        alt33=1;
                    }
                    switch (alt33) {
                        case 1 :
                            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6491:64: RULE_E
                            {
                            mRULE_E(); 

                            }
                            break;

                    }

                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6491:72: ( RULE_FS )?
                    int alt34=2;
                    int LA34_0 = input.LA(1);

                    if ( (LA34_0=='F'||LA34_0=='L'||LA34_0=='f'||LA34_0=='l') ) {
                        alt34=1;
                    }
                    switch (alt34) {
                        case 1 :
                            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6491:72: RULE_FS
                            {
                            mRULE_FS(); 

                            }
                            break;

                    }


                    }
                    break;
                case 3 :
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6491:81: ( RULE_D )+ '.' ( RULE_E )? ( RULE_FS )?
                    {
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6491:81: ( RULE_D )+
                    int cnt35=0;
                    loop35:
                    do {
                        int alt35=2;
                        int LA35_0 = input.LA(1);

                        if ( ((LA35_0>='0' && LA35_0<='9')) ) {
                            alt35=1;
                        }


                        switch (alt35) {
                    	case 1 :
                    	    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6491:81: RULE_D
                    	    {
                    	    mRULE_D(); 

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt35 >= 1 ) break loop35;
                                EarlyExitException eee =
                                    new EarlyExitException(35, input);
                                throw eee;
                        }
                        cnt35++;
                    } while (true);

                    match('.'); 
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6491:93: ( RULE_E )?
                    int alt36=2;
                    int LA36_0 = input.LA(1);

                    if ( (LA36_0=='E'||LA36_0=='e') ) {
                        alt36=1;
                    }
                    switch (alt36) {
                        case 1 :
                            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6491:93: RULE_E
                            {
                            mRULE_E(); 

                            }
                            break;

                    }

                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6491:101: ( RULE_FS )?
                    int alt37=2;
                    int LA37_0 = input.LA(1);

                    if ( (LA37_0=='F'||LA37_0=='L'||LA37_0=='f'||LA37_0=='l') ) {
                        alt37=1;
                    }
                    switch (alt37) {
                        case 1 :
                            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6491:101: RULE_FS
                            {
                            mRULE_FS(); 

                            }
                            break;

                    }


                    }
                    break;
                case 4 :
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6491:110: RULE_HP ( RULE_H )+ RULE_P ( RULE_FS )?
                    {
                    mRULE_HP(); 
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6491:118: ( RULE_H )+
                    int cnt38=0;
                    loop38:
                    do {
                        int alt38=2;
                        int LA38_0 = input.LA(1);

                        if ( ((LA38_0>='0' && LA38_0<='9')||(LA38_0>='A' && LA38_0<='F')||(LA38_0>='a' && LA38_0<='f')) ) {
                            alt38=1;
                        }


                        switch (alt38) {
                    	case 1 :
                    	    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6491:118: RULE_H
                    	    {
                    	    mRULE_H(); 

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt38 >= 1 ) break loop38;
                                EarlyExitException eee =
                                    new EarlyExitException(38, input);
                                throw eee;
                        }
                        cnt38++;
                    } while (true);

                    mRULE_P(); 
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6491:133: ( RULE_FS )?
                    int alt39=2;
                    int LA39_0 = input.LA(1);

                    if ( (LA39_0=='F'||LA39_0=='L'||LA39_0=='f'||LA39_0=='l') ) {
                        alt39=1;
                    }
                    switch (alt39) {
                        case 1 :
                            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6491:133: RULE_FS
                            {
                            mRULE_FS(); 

                            }
                            break;

                    }


                    }
                    break;
                case 5 :
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6491:142: RULE_HP ( RULE_H )* '.' ( RULE_H )+ RULE_P ( RULE_FS )?
                    {
                    mRULE_HP(); 
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6491:150: ( RULE_H )*
                    loop40:
                    do {
                        int alt40=2;
                        int LA40_0 = input.LA(1);

                        if ( ((LA40_0>='0' && LA40_0<='9')||(LA40_0>='A' && LA40_0<='F')||(LA40_0>='a' && LA40_0<='f')) ) {
                            alt40=1;
                        }


                        switch (alt40) {
                    	case 1 :
                    	    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6491:150: RULE_H
                    	    {
                    	    mRULE_H(); 

                    	    }
                    	    break;

                    	default :
                    	    break loop40;
                        }
                    } while (true);

                    match('.'); 
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6491:162: ( RULE_H )+
                    int cnt41=0;
                    loop41:
                    do {
                        int alt41=2;
                        int LA41_0 = input.LA(1);

                        if ( ((LA41_0>='0' && LA41_0<='9')||(LA41_0>='A' && LA41_0<='F')||(LA41_0>='a' && LA41_0<='f')) ) {
                            alt41=1;
                        }


                        switch (alt41) {
                    	case 1 :
                    	    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6491:162: RULE_H
                    	    {
                    	    mRULE_H(); 

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt41 >= 1 ) break loop41;
                                EarlyExitException eee =
                                    new EarlyExitException(41, input);
                                throw eee;
                        }
                        cnt41++;
                    } while (true);

                    mRULE_P(); 
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6491:177: ( RULE_FS )?
                    int alt42=2;
                    int LA42_0 = input.LA(1);

                    if ( (LA42_0=='F'||LA42_0=='L'||LA42_0=='f'||LA42_0=='l') ) {
                        alt42=1;
                    }
                    switch (alt42) {
                        case 1 :
                            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6491:177: RULE_FS
                            {
                            mRULE_FS(); 

                            }
                            break;

                    }


                    }
                    break;
                case 6 :
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6491:186: RULE_HP ( RULE_H )+ '.' RULE_P ( RULE_FS )?
                    {
                    mRULE_HP(); 
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6491:194: ( RULE_H )+
                    int cnt43=0;
                    loop43:
                    do {
                        int alt43=2;
                        int LA43_0 = input.LA(1);

                        if ( ((LA43_0>='0' && LA43_0<='9')||(LA43_0>='A' && LA43_0<='F')||(LA43_0>='a' && LA43_0<='f')) ) {
                            alt43=1;
                        }


                        switch (alt43) {
                    	case 1 :
                    	    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6491:194: RULE_H
                    	    {
                    	    mRULE_H(); 

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt43 >= 1 ) break loop43;
                                EarlyExitException eee =
                                    new EarlyExitException(43, input);
                                throw eee;
                        }
                        cnt43++;
                    } while (true);

                    match('.'); 
                    mRULE_P(); 
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6491:213: ( RULE_FS )?
                    int alt44=2;
                    int LA44_0 = input.LA(1);

                    if ( (LA44_0=='F'||LA44_0=='L'||LA44_0=='f'||LA44_0=='l') ) {
                        alt44=1;
                    }
                    switch (alt44) {
                        case 1 :
                            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6491:213: RULE_FS
                            {
                            mRULE_FS(); 

                            }
                            break;

                    }


                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_F_CONSTANT"

    // $ANTLR start "RULE_STRING_LITERAL"
    public final void mRULE_STRING_LITERAL() throws RecognitionException {
        try {
            int _type = RULE_STRING_LITERAL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6493:21: ( ( ( RULE_SP )? '\"' (~ ( ( '\"' | '\\\\' | '\\n' ) ) | RULE_ES )* '\"' )+ )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6493:23: ( ( RULE_SP )? '\"' (~ ( ( '\"' | '\\\\' | '\\n' ) ) | RULE_ES )* '\"' )+
            {
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6493:23: ( ( RULE_SP )? '\"' (~ ( ( '\"' | '\\\\' | '\\n' ) ) | RULE_ES )* '\"' )+
            int cnt48=0;
            loop48:
            do {
                int alt48=2;
                int LA48_0 = input.LA(1);

                if ( (LA48_0=='\"'||LA48_0=='L'||LA48_0=='U'||LA48_0=='u') ) {
                    alt48=1;
                }


                switch (alt48) {
            	case 1 :
            	    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6493:24: ( RULE_SP )? '\"' (~ ( ( '\"' | '\\\\' | '\\n' ) ) | RULE_ES )* '\"'
            	    {
            	    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6493:24: ( RULE_SP )?
            	    int alt46=2;
            	    int LA46_0 = input.LA(1);

            	    if ( (LA46_0=='L'||LA46_0=='U'||LA46_0=='u') ) {
            	        alt46=1;
            	    }
            	    switch (alt46) {
            	        case 1 :
            	            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6493:24: RULE_SP
            	            {
            	            mRULE_SP(); 

            	            }
            	            break;

            	    }

            	    match('\"'); 
            	    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6493:37: (~ ( ( '\"' | '\\\\' | '\\n' ) ) | RULE_ES )*
            	    loop47:
            	    do {
            	        int alt47=3;
            	        int LA47_0 = input.LA(1);

            	        if ( ((LA47_0>='\u0000' && LA47_0<='\t')||(LA47_0>='\u000B' && LA47_0<='!')||(LA47_0>='#' && LA47_0<='[')||(LA47_0>=']' && LA47_0<='\uFFFF')) ) {
            	            alt47=1;
            	        }
            	        else if ( (LA47_0=='\\') ) {
            	            alt47=2;
            	        }


            	        switch (alt47) {
            	    	case 1 :
            	    	    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6493:38: ~ ( ( '\"' | '\\\\' | '\\n' ) )
            	    	    {
            	    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='!')||(input.LA(1)>='#' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
            	    	        input.consume();

            	    	    }
            	    	    else {
            	    	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	    	        recover(mse);
            	    	        throw mse;}


            	    	    }
            	    	    break;
            	    	case 2 :
            	    	    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6493:57: RULE_ES
            	    	    {
            	    	    mRULE_ES(); 

            	    	    }
            	    	    break;

            	    	default :
            	    	    break loop47;
            	        }
            	    } while (true);

            	    match('\"'); 

            	    }
            	    break;

            	default :
            	    if ( cnt48 >= 1 ) break loop48;
                        EarlyExitException eee =
                            new EarlyExitException(48, input);
                        throw eee;
                }
                cnt48++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_STRING_LITERAL"

    // $ANTLR start "RULE_ELLIPSIS"
    public final void mRULE_ELLIPSIS() throws RecognitionException {
        try {
            int _type = RULE_ELLIPSIS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6495:15: ( '...' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6495:17: '...'
            {
            match("..."); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ELLIPSIS"

    // $ANTLR start "RULE_ASSIGN"
    public final void mRULE_ASSIGN() throws RecognitionException {
        try {
            int _type = RULE_ASSIGN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6497:13: ( '=' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6497:15: '='
            {
            match('='); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ASSIGN"

    // $ANTLR start "RULE_RIGHT_ASSIGN"
    public final void mRULE_RIGHT_ASSIGN() throws RecognitionException {
        try {
            int _type = RULE_RIGHT_ASSIGN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6499:19: ( '>>=' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6499:21: '>>='
            {
            match(">>="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_RIGHT_ASSIGN"

    // $ANTLR start "RULE_LEFT_ASSIGN"
    public final void mRULE_LEFT_ASSIGN() throws RecognitionException {
        try {
            int _type = RULE_LEFT_ASSIGN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6501:18: ( '<<=' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6501:20: '<<='
            {
            match("<<="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_LEFT_ASSIGN"

    // $ANTLR start "RULE_ADD_ASSIGN"
    public final void mRULE_ADD_ASSIGN() throws RecognitionException {
        try {
            int _type = RULE_ADD_ASSIGN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6503:17: ( '+=' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6503:19: '+='
            {
            match("+="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ADD_ASSIGN"

    // $ANTLR start "RULE_SUB_ASSIGN"
    public final void mRULE_SUB_ASSIGN() throws RecognitionException {
        try {
            int _type = RULE_SUB_ASSIGN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6505:17: ( '-=' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6505:19: '-='
            {
            match("-="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SUB_ASSIGN"

    // $ANTLR start "RULE_MUL_ASSIGN"
    public final void mRULE_MUL_ASSIGN() throws RecognitionException {
        try {
            int _type = RULE_MUL_ASSIGN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6507:17: ( '*=' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6507:19: '*='
            {
            match("*="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_MUL_ASSIGN"

    // $ANTLR start "RULE_DIV_ASSIGN"
    public final void mRULE_DIV_ASSIGN() throws RecognitionException {
        try {
            int _type = RULE_DIV_ASSIGN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6509:17: ( '/=' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6509:19: '/='
            {
            match("/="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_DIV_ASSIGN"

    // $ANTLR start "RULE_MOD_ASSIGN"
    public final void mRULE_MOD_ASSIGN() throws RecognitionException {
        try {
            int _type = RULE_MOD_ASSIGN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6511:17: ( '%=' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6511:19: '%='
            {
            match("%="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_MOD_ASSIGN"

    // $ANTLR start "RULE_AND_ASSIGN"
    public final void mRULE_AND_ASSIGN() throws RecognitionException {
        try {
            int _type = RULE_AND_ASSIGN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6513:17: ( '&=' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6513:19: '&='
            {
            match("&="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_AND_ASSIGN"

    // $ANTLR start "RULE_XOR_ASSIGN"
    public final void mRULE_XOR_ASSIGN() throws RecognitionException {
        try {
            int _type = RULE_XOR_ASSIGN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6515:17: ( '^=' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6515:19: '^='
            {
            match("^="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_XOR_ASSIGN"

    // $ANTLR start "RULE_OR_ASSIGN"
    public final void mRULE_OR_ASSIGN() throws RecognitionException {
        try {
            int _type = RULE_OR_ASSIGN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6517:16: ( '|=' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6517:18: '|='
            {
            match("|="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_OR_ASSIGN"

    // $ANTLR start "RULE_RIGHT_OP"
    public final void mRULE_RIGHT_OP() throws RecognitionException {
        try {
            int _type = RULE_RIGHT_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6519:15: ( '>>' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6519:17: '>>'
            {
            match(">>"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_RIGHT_OP"

    // $ANTLR start "RULE_LEFT_OP"
    public final void mRULE_LEFT_OP() throws RecognitionException {
        try {
            int _type = RULE_LEFT_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6521:14: ( '<<' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6521:16: '<<'
            {
            match("<<"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_LEFT_OP"

    // $ANTLR start "RULE_INC_OP"
    public final void mRULE_INC_OP() throws RecognitionException {
        try {
            int _type = RULE_INC_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6523:13: ( '++' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6523:15: '++'
            {
            match("++"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_INC_OP"

    // $ANTLR start "RULE_DEC_OP"
    public final void mRULE_DEC_OP() throws RecognitionException {
        try {
            int _type = RULE_DEC_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6525:13: ( '--' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6525:15: '--'
            {
            match("--"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_DEC_OP"

    // $ANTLR start "RULE_PTR_OP"
    public final void mRULE_PTR_OP() throws RecognitionException {
        try {
            int _type = RULE_PTR_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6527:13: ( '->' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6527:15: '->'
            {
            match("->"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_PTR_OP"

    // $ANTLR start "RULE_LAND_OP"
    public final void mRULE_LAND_OP() throws RecognitionException {
        try {
            int _type = RULE_LAND_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6529:14: ( '&&' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6529:16: '&&'
            {
            match("&&"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_LAND_OP"

    // $ANTLR start "RULE_LOR_OP"
    public final void mRULE_LOR_OP() throws RecognitionException {
        try {
            int _type = RULE_LOR_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6531:13: ( '||' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6531:15: '||'
            {
            match("||"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_LOR_OP"

    // $ANTLR start "RULE_LE_OP"
    public final void mRULE_LE_OP() throws RecognitionException {
        try {
            int _type = RULE_LE_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6533:12: ( '<=' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6533:14: '<='
            {
            match("<="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_LE_OP"

    // $ANTLR start "RULE_GE_OP"
    public final void mRULE_GE_OP() throws RecognitionException {
        try {
            int _type = RULE_GE_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6535:12: ( '>=' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6535:14: '>='
            {
            match(">="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_GE_OP"

    // $ANTLR start "RULE_EQ_OP"
    public final void mRULE_EQ_OP() throws RecognitionException {
        try {
            int _type = RULE_EQ_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6537:12: ( '==' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6537:14: '=='
            {
            match("=="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_EQ_OP"

    // $ANTLR start "RULE_NE_OP"
    public final void mRULE_NE_OP() throws RecognitionException {
        try {
            int _type = RULE_NE_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6539:12: ( '!=' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6539:14: '!='
            {
            match("!="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_NE_OP"

    // $ANTLR start "RULE_SEMICOLON"
    public final void mRULE_SEMICOLON() throws RecognitionException {
        try {
            int _type = RULE_SEMICOLON;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6541:16: ( ';' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6541:18: ';'
            {
            match(';'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SEMICOLON"

    // $ANTLR start "RULE_LCBRACKET"
    public final void mRULE_LCBRACKET() throws RecognitionException {
        try {
            int _type = RULE_LCBRACKET;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6543:16: ( ( '{' | '<%' ) )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6543:18: ( '{' | '<%' )
            {
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6543:18: ( '{' | '<%' )
            int alt49=2;
            int LA49_0 = input.LA(1);

            if ( (LA49_0=='{') ) {
                alt49=1;
            }
            else if ( (LA49_0=='<') ) {
                alt49=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 49, 0, input);

                throw nvae;
            }
            switch (alt49) {
                case 1 :
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6543:19: '{'
                    {
                    match('{'); 

                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6543:23: '<%'
                    {
                    match("<%"); 


                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_LCBRACKET"

    // $ANTLR start "RULE_RCBRACKET"
    public final void mRULE_RCBRACKET() throws RecognitionException {
        try {
            int _type = RULE_RCBRACKET;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6545:16: ( ( '}' | '%>' ) )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6545:18: ( '}' | '%>' )
            {
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6545:18: ( '}' | '%>' )
            int alt50=2;
            int LA50_0 = input.LA(1);

            if ( (LA50_0=='}') ) {
                alt50=1;
            }
            else if ( (LA50_0=='%') ) {
                alt50=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 50, 0, input);

                throw nvae;
            }
            switch (alt50) {
                case 1 :
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6545:19: '}'
                    {
                    match('}'); 

                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6545:23: '%>'
                    {
                    match("%>"); 


                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_RCBRACKET"

    // $ANTLR start "RULE_COMMA"
    public final void mRULE_COMMA() throws RecognitionException {
        try {
            int _type = RULE_COMMA;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6547:12: ( ',' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6547:14: ','
            {
            match(','); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_COMMA"

    // $ANTLR start "RULE_COLON"
    public final void mRULE_COLON() throws RecognitionException {
        try {
            int _type = RULE_COLON;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6549:12: ( ':' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6549:14: ':'
            {
            match(':'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_COLON"

    // $ANTLR start "RULE_LPAREN"
    public final void mRULE_LPAREN() throws RecognitionException {
        try {
            int _type = RULE_LPAREN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6551:13: ( '(' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6551:15: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_LPAREN"

    // $ANTLR start "RULE_RPAREN"
    public final void mRULE_RPAREN() throws RecognitionException {
        try {
            int _type = RULE_RPAREN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6553:13: ( ')' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6553:15: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_RPAREN"

    // $ANTLR start "RULE_LBRACKET"
    public final void mRULE_LBRACKET() throws RecognitionException {
        try {
            int _type = RULE_LBRACKET;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6555:15: ( ( '[' | '<:' ) )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6555:17: ( '[' | '<:' )
            {
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6555:17: ( '[' | '<:' )
            int alt51=2;
            int LA51_0 = input.LA(1);

            if ( (LA51_0=='[') ) {
                alt51=1;
            }
            else if ( (LA51_0=='<') ) {
                alt51=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 51, 0, input);

                throw nvae;
            }
            switch (alt51) {
                case 1 :
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6555:18: '['
                    {
                    match('['); 

                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6555:22: '<:'
                    {
                    match("<:"); 


                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_LBRACKET"

    // $ANTLR start "RULE_RBRACKET"
    public final void mRULE_RBRACKET() throws RecognitionException {
        try {
            int _type = RULE_RBRACKET;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6557:15: ( ( ']' | ':>' ) )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6557:17: ( ']' | ':>' )
            {
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6557:17: ( ']' | ':>' )
            int alt52=2;
            int LA52_0 = input.LA(1);

            if ( (LA52_0==']') ) {
                alt52=1;
            }
            else if ( (LA52_0==':') ) {
                alt52=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 52, 0, input);

                throw nvae;
            }
            switch (alt52) {
                case 1 :
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6557:18: ']'
                    {
                    match(']'); 

                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6557:22: ':>'
                    {
                    match(":>"); 


                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_RBRACKET"

    // $ANTLR start "RULE_DOT"
    public final void mRULE_DOT() throws RecognitionException {
        try {
            int _type = RULE_DOT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6559:10: ( '.' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6559:12: '.'
            {
            match('.'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_DOT"

    // $ANTLR start "RULE_AND_OP"
    public final void mRULE_AND_OP() throws RecognitionException {
        try {
            int _type = RULE_AND_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6561:13: ( '&' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6561:15: '&'
            {
            match('&'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_AND_OP"

    // $ANTLR start "RULE_NOT_OP"
    public final void mRULE_NOT_OP() throws RecognitionException {
        try {
            int _type = RULE_NOT_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6563:13: ( '!' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6563:15: '!'
            {
            match('!'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_NOT_OP"

    // $ANTLR start "RULE_TILDE"
    public final void mRULE_TILDE() throws RecognitionException {
        try {
            int _type = RULE_TILDE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6565:12: ( '~' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6565:14: '~'
            {
            match('~'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_TILDE"

    // $ANTLR start "RULE_MINUS_OP"
    public final void mRULE_MINUS_OP() throws RecognitionException {
        try {
            int _type = RULE_MINUS_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6567:15: ( '-' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6567:17: '-'
            {
            match('-'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_MINUS_OP"

    // $ANTLR start "RULE_ADD_OP"
    public final void mRULE_ADD_OP() throws RecognitionException {
        try {
            int _type = RULE_ADD_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6569:13: ( '+' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6569:15: '+'
            {
            match('+'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ADD_OP"

    // $ANTLR start "RULE_MULT_OP"
    public final void mRULE_MULT_OP() throws RecognitionException {
        try {
            int _type = RULE_MULT_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6571:14: ( '*' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6571:16: '*'
            {
            match('*'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_MULT_OP"

    // $ANTLR start "RULE_DIV_OP"
    public final void mRULE_DIV_OP() throws RecognitionException {
        try {
            int _type = RULE_DIV_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6573:13: ( '/' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6573:15: '/'
            {
            match('/'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_DIV_OP"

    // $ANTLR start "RULE_MOD_OP"
    public final void mRULE_MOD_OP() throws RecognitionException {
        try {
            int _type = RULE_MOD_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6575:13: ( '%' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6575:15: '%'
            {
            match('%'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_MOD_OP"

    // $ANTLR start "RULE_L_OP"
    public final void mRULE_L_OP() throws RecognitionException {
        try {
            int _type = RULE_L_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6577:11: ( '<' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6577:13: '<'
            {
            match('<'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_L_OP"

    // $ANTLR start "RULE_G_OP"
    public final void mRULE_G_OP() throws RecognitionException {
        try {
            int _type = RULE_G_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6579:11: ( '>' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6579:13: '>'
            {
            match('>'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_G_OP"

    // $ANTLR start "RULE_XOR_OP"
    public final void mRULE_XOR_OP() throws RecognitionException {
        try {
            int _type = RULE_XOR_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6581:13: ( '^' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6581:15: '^'
            {
            match('^'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_XOR_OP"

    // $ANTLR start "RULE_OR_OP"
    public final void mRULE_OR_OP() throws RecognitionException {
        try {
            int _type = RULE_OR_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6583:12: ( '|' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6583:14: '|'
            {
            match('|'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_OR_OP"

    // $ANTLR start "RULE_Q_OP"
    public final void mRULE_Q_OP() throws RecognitionException {
        try {
            int _type = RULE_Q_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6585:11: ( '?' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6585:13: '?'
            {
            match('?'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_Q_OP"

    // $ANTLR start "RULE_INCLUDE"
    public final void mRULE_INCLUDE() throws RecognitionException {
        try {
            int _type = RULE_INCLUDE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6587:14: ( '#include' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6587:16: '#include'
            {
            match("#include"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_INCLUDE"

    // $ANTLR start "RULE_DQUOTE"
    public final void mRULE_DQUOTE() throws RecognitionException {
        try {
            int _type = RULE_DQUOTE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6589:13: ( '\"' )
            // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:6589:15: '\"'
            {
            match('\"'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_DQUOTE"

    public void mTokens() throws RecognitionException {
        // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:8: ( RULE_DELTA | RULE_ADDS | RULE_REMOVES | RULE_MODIFIES | RULE_ML_COMMENT | RULE_SL_COMMENT | RULE_WS | RULE_AUTO | RULE_BREAK | RULE_CASE | RULE_CHAR | RULE_CONST | RULE_CONTINUE | RULE_DEFAULT | RULE_DO | RULE_DOUBLE | RULE_ELSE | RULE_ENUM | RULE_EXTERN | RULE_FLOAT | RULE_FOR | RULE_GOTO | RULE_IF | RULE_INLINE | RULE_INT | RULE_LONG | RULE_REGISTER | RULE_RESTRICT | RULE_RETURN | RULE_SHORT | RULE_SIGNED | RULE_SIZEOF | RULE_STATIC | RULE_STRUCT | RULE_SWITCH | RULE_TYPEDEF | RULE_UNION | RULE_UNSIGNED | RULE_VOID | RULE_VOLATILE | RULE_WHILE | RULE_ALIGNAS | RULE_ALIGNOF | RULE_ATOMIC | RULE_BOOL | RULE_COMPLEX | RULE_GENERIC | RULE_IMAGINARY | RULE_NORETURN | RULE_STATIC_ASSERT | RULE_THREAD_LOCAL | RULE_FUNC_NAME | RULE_IDENTIFIER | RULE_CUSTOM | RULE_I_CONSTANT | RULE_F_CONSTANT | RULE_STRING_LITERAL | RULE_ELLIPSIS | RULE_ASSIGN | RULE_RIGHT_ASSIGN | RULE_LEFT_ASSIGN | RULE_ADD_ASSIGN | RULE_SUB_ASSIGN | RULE_MUL_ASSIGN | RULE_DIV_ASSIGN | RULE_MOD_ASSIGN | RULE_AND_ASSIGN | RULE_XOR_ASSIGN | RULE_OR_ASSIGN | RULE_RIGHT_OP | RULE_LEFT_OP | RULE_INC_OP | RULE_DEC_OP | RULE_PTR_OP | RULE_LAND_OP | RULE_LOR_OP | RULE_LE_OP | RULE_GE_OP | RULE_EQ_OP | RULE_NE_OP | RULE_SEMICOLON | RULE_LCBRACKET | RULE_RCBRACKET | RULE_COMMA | RULE_COLON | RULE_LPAREN | RULE_RPAREN | RULE_LBRACKET | RULE_RBRACKET | RULE_DOT | RULE_AND_OP | RULE_NOT_OP | RULE_TILDE | RULE_MINUS_OP | RULE_ADD_OP | RULE_MULT_OP | RULE_DIV_OP | RULE_MOD_OP | RULE_L_OP | RULE_G_OP | RULE_XOR_OP | RULE_OR_OP | RULE_Q_OP | RULE_INCLUDE | RULE_DQUOTE )
        int alt53=105;
        alt53 = dfa53.predict(input);
        switch (alt53) {
            case 1 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:10: RULE_DELTA
                {
                mRULE_DELTA(); 

                }
                break;
            case 2 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:21: RULE_ADDS
                {
                mRULE_ADDS(); 

                }
                break;
            case 3 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:31: RULE_REMOVES
                {
                mRULE_REMOVES(); 

                }
                break;
            case 4 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:44: RULE_MODIFIES
                {
                mRULE_MODIFIES(); 

                }
                break;
            case 5 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:58: RULE_ML_COMMENT
                {
                mRULE_ML_COMMENT(); 

                }
                break;
            case 6 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:74: RULE_SL_COMMENT
                {
                mRULE_SL_COMMENT(); 

                }
                break;
            case 7 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:90: RULE_WS
                {
                mRULE_WS(); 

                }
                break;
            case 8 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:98: RULE_AUTO
                {
                mRULE_AUTO(); 

                }
                break;
            case 9 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:108: RULE_BREAK
                {
                mRULE_BREAK(); 

                }
                break;
            case 10 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:119: RULE_CASE
                {
                mRULE_CASE(); 

                }
                break;
            case 11 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:129: RULE_CHAR
                {
                mRULE_CHAR(); 

                }
                break;
            case 12 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:139: RULE_CONST
                {
                mRULE_CONST(); 

                }
                break;
            case 13 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:150: RULE_CONTINUE
                {
                mRULE_CONTINUE(); 

                }
                break;
            case 14 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:164: RULE_DEFAULT
                {
                mRULE_DEFAULT(); 

                }
                break;
            case 15 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:177: RULE_DO
                {
                mRULE_DO(); 

                }
                break;
            case 16 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:185: RULE_DOUBLE
                {
                mRULE_DOUBLE(); 

                }
                break;
            case 17 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:197: RULE_ELSE
                {
                mRULE_ELSE(); 

                }
                break;
            case 18 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:207: RULE_ENUM
                {
                mRULE_ENUM(); 

                }
                break;
            case 19 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:217: RULE_EXTERN
                {
                mRULE_EXTERN(); 

                }
                break;
            case 20 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:229: RULE_FLOAT
                {
                mRULE_FLOAT(); 

                }
                break;
            case 21 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:240: RULE_FOR
                {
                mRULE_FOR(); 

                }
                break;
            case 22 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:249: RULE_GOTO
                {
                mRULE_GOTO(); 

                }
                break;
            case 23 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:259: RULE_IF
                {
                mRULE_IF(); 

                }
                break;
            case 24 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:267: RULE_INLINE
                {
                mRULE_INLINE(); 

                }
                break;
            case 25 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:279: RULE_INT
                {
                mRULE_INT(); 

                }
                break;
            case 26 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:288: RULE_LONG
                {
                mRULE_LONG(); 

                }
                break;
            case 27 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:298: RULE_REGISTER
                {
                mRULE_REGISTER(); 

                }
                break;
            case 28 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:312: RULE_RESTRICT
                {
                mRULE_RESTRICT(); 

                }
                break;
            case 29 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:326: RULE_RETURN
                {
                mRULE_RETURN(); 

                }
                break;
            case 30 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:338: RULE_SHORT
                {
                mRULE_SHORT(); 

                }
                break;
            case 31 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:349: RULE_SIGNED
                {
                mRULE_SIGNED(); 

                }
                break;
            case 32 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:361: RULE_SIZEOF
                {
                mRULE_SIZEOF(); 

                }
                break;
            case 33 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:373: RULE_STATIC
                {
                mRULE_STATIC(); 

                }
                break;
            case 34 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:385: RULE_STRUCT
                {
                mRULE_STRUCT(); 

                }
                break;
            case 35 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:397: RULE_SWITCH
                {
                mRULE_SWITCH(); 

                }
                break;
            case 36 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:409: RULE_TYPEDEF
                {
                mRULE_TYPEDEF(); 

                }
                break;
            case 37 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:422: RULE_UNION
                {
                mRULE_UNION(); 

                }
                break;
            case 38 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:433: RULE_UNSIGNED
                {
                mRULE_UNSIGNED(); 

                }
                break;
            case 39 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:447: RULE_VOID
                {
                mRULE_VOID(); 

                }
                break;
            case 40 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:457: RULE_VOLATILE
                {
                mRULE_VOLATILE(); 

                }
                break;
            case 41 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:471: RULE_WHILE
                {
                mRULE_WHILE(); 

                }
                break;
            case 42 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:482: RULE_ALIGNAS
                {
                mRULE_ALIGNAS(); 

                }
                break;
            case 43 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:495: RULE_ALIGNOF
                {
                mRULE_ALIGNOF(); 

                }
                break;
            case 44 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:508: RULE_ATOMIC
                {
                mRULE_ATOMIC(); 

                }
                break;
            case 45 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:520: RULE_BOOL
                {
                mRULE_BOOL(); 

                }
                break;
            case 46 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:530: RULE_COMPLEX
                {
                mRULE_COMPLEX(); 

                }
                break;
            case 47 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:543: RULE_GENERIC
                {
                mRULE_GENERIC(); 

                }
                break;
            case 48 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:556: RULE_IMAGINARY
                {
                mRULE_IMAGINARY(); 

                }
                break;
            case 49 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:571: RULE_NORETURN
                {
                mRULE_NORETURN(); 

                }
                break;
            case 50 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:585: RULE_STATIC_ASSERT
                {
                mRULE_STATIC_ASSERT(); 

                }
                break;
            case 51 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:604: RULE_THREAD_LOCAL
                {
                mRULE_THREAD_LOCAL(); 

                }
                break;
            case 52 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:622: RULE_FUNC_NAME
                {
                mRULE_FUNC_NAME(); 

                }
                break;
            case 53 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:637: RULE_IDENTIFIER
                {
                mRULE_IDENTIFIER(); 

                }
                break;
            case 54 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:653: RULE_CUSTOM
                {
                mRULE_CUSTOM(); 

                }
                break;
            case 55 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:665: RULE_I_CONSTANT
                {
                mRULE_I_CONSTANT(); 

                }
                break;
            case 56 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:681: RULE_F_CONSTANT
                {
                mRULE_F_CONSTANT(); 

                }
                break;
            case 57 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:697: RULE_STRING_LITERAL
                {
                mRULE_STRING_LITERAL(); 

                }
                break;
            case 58 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:717: RULE_ELLIPSIS
                {
                mRULE_ELLIPSIS(); 

                }
                break;
            case 59 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:731: RULE_ASSIGN
                {
                mRULE_ASSIGN(); 

                }
                break;
            case 60 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:743: RULE_RIGHT_ASSIGN
                {
                mRULE_RIGHT_ASSIGN(); 

                }
                break;
            case 61 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:761: RULE_LEFT_ASSIGN
                {
                mRULE_LEFT_ASSIGN(); 

                }
                break;
            case 62 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:778: RULE_ADD_ASSIGN
                {
                mRULE_ADD_ASSIGN(); 

                }
                break;
            case 63 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:794: RULE_SUB_ASSIGN
                {
                mRULE_SUB_ASSIGN(); 

                }
                break;
            case 64 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:810: RULE_MUL_ASSIGN
                {
                mRULE_MUL_ASSIGN(); 

                }
                break;
            case 65 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:826: RULE_DIV_ASSIGN
                {
                mRULE_DIV_ASSIGN(); 

                }
                break;
            case 66 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:842: RULE_MOD_ASSIGN
                {
                mRULE_MOD_ASSIGN(); 

                }
                break;
            case 67 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:858: RULE_AND_ASSIGN
                {
                mRULE_AND_ASSIGN(); 

                }
                break;
            case 68 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:874: RULE_XOR_ASSIGN
                {
                mRULE_XOR_ASSIGN(); 

                }
                break;
            case 69 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:890: RULE_OR_ASSIGN
                {
                mRULE_OR_ASSIGN(); 

                }
                break;
            case 70 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:905: RULE_RIGHT_OP
                {
                mRULE_RIGHT_OP(); 

                }
                break;
            case 71 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:919: RULE_LEFT_OP
                {
                mRULE_LEFT_OP(); 

                }
                break;
            case 72 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:932: RULE_INC_OP
                {
                mRULE_INC_OP(); 

                }
                break;
            case 73 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:944: RULE_DEC_OP
                {
                mRULE_DEC_OP(); 

                }
                break;
            case 74 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:956: RULE_PTR_OP
                {
                mRULE_PTR_OP(); 

                }
                break;
            case 75 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:968: RULE_LAND_OP
                {
                mRULE_LAND_OP(); 

                }
                break;
            case 76 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:981: RULE_LOR_OP
                {
                mRULE_LOR_OP(); 

                }
                break;
            case 77 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:993: RULE_LE_OP
                {
                mRULE_LE_OP(); 

                }
                break;
            case 78 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:1004: RULE_GE_OP
                {
                mRULE_GE_OP(); 

                }
                break;
            case 79 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:1015: RULE_EQ_OP
                {
                mRULE_EQ_OP(); 

                }
                break;
            case 80 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:1026: RULE_NE_OP
                {
                mRULE_NE_OP(); 

                }
                break;
            case 81 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:1037: RULE_SEMICOLON
                {
                mRULE_SEMICOLON(); 

                }
                break;
            case 82 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:1052: RULE_LCBRACKET
                {
                mRULE_LCBRACKET(); 

                }
                break;
            case 83 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:1067: RULE_RCBRACKET
                {
                mRULE_RCBRACKET(); 

                }
                break;
            case 84 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:1082: RULE_COMMA
                {
                mRULE_COMMA(); 

                }
                break;
            case 85 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:1093: RULE_COLON
                {
                mRULE_COLON(); 

                }
                break;
            case 86 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:1104: RULE_LPAREN
                {
                mRULE_LPAREN(); 

                }
                break;
            case 87 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:1116: RULE_RPAREN
                {
                mRULE_RPAREN(); 

                }
                break;
            case 88 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:1128: RULE_LBRACKET
                {
                mRULE_LBRACKET(); 

                }
                break;
            case 89 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:1142: RULE_RBRACKET
                {
                mRULE_RBRACKET(); 

                }
                break;
            case 90 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:1156: RULE_DOT
                {
                mRULE_DOT(); 

                }
                break;
            case 91 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:1165: RULE_AND_OP
                {
                mRULE_AND_OP(); 

                }
                break;
            case 92 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:1177: RULE_NOT_OP
                {
                mRULE_NOT_OP(); 

                }
                break;
            case 93 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:1189: RULE_TILDE
                {
                mRULE_TILDE(); 

                }
                break;
            case 94 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:1200: RULE_MINUS_OP
                {
                mRULE_MINUS_OP(); 

                }
                break;
            case 95 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:1214: RULE_ADD_OP
                {
                mRULE_ADD_OP(); 

                }
                break;
            case 96 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:1226: RULE_MULT_OP
                {
                mRULE_MULT_OP(); 

                }
                break;
            case 97 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:1239: RULE_DIV_OP
                {
                mRULE_DIV_OP(); 

                }
                break;
            case 98 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:1251: RULE_MOD_OP
                {
                mRULE_MOD_OP(); 

                }
                break;
            case 99 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:1263: RULE_L_OP
                {
                mRULE_L_OP(); 

                }
                break;
            case 100 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:1273: RULE_G_OP
                {
                mRULE_G_OP(); 

                }
                break;
            case 101 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:1283: RULE_XOR_OP
                {
                mRULE_XOR_OP(); 

                }
                break;
            case 102 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:1295: RULE_OR_OP
                {
                mRULE_OR_OP(); 

                }
                break;
            case 103 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:1306: RULE_Q_OP
                {
                mRULE_Q_OP(); 

                }
                break;
            case 104 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:1316: RULE_INCLUDE
                {
                mRULE_INCLUDE(); 

                }
                break;
            case 105 :
                // ../org.xtext.dop.codebase/src-gen/org/xtext/dop/codebase/parser/antlr/internal/InternalCodeBase.g:1:1329: RULE_DQUOTE
                {
                mRULE_DQUOTE(); 

                }
                break;

        }

    }


    protected DFA45 dfa45 = new DFA45(this);
    protected DFA53 dfa53 = new DFA53(this);
    static final String DFA45_eotS =
        "\6\uffff\1\12\7\uffff";
    static final String DFA45_eofS =
        "\16\uffff";
    static final String DFA45_minS =
        "\2\56\1\uffff\3\56\1\60\1\uffff\1\56\2\uffff\1\60\2\uffff";
    static final String DFA45_maxS =
        "\1\71\1\170\1\uffff\1\145\2\146\1\71\1\uffff\1\160\2\uffff\1\160"+
        "\2\uffff";
    static final String DFA45_acceptS =
        "\2\uffff\1\2\4\uffff\1\1\1\uffff\1\5\1\3\1\uffff\1\4\1\6";
    static final String DFA45_specialS =
        "\16\uffff}>";
    static final String[] DFA45_transitionS = {
            "\1\2\1\uffff\1\1\11\3",
            "\1\6\1\uffff\12\3\13\uffff\1\7\22\uffff\1\5\14\uffff\1\7\22"+
            "\uffff\1\4",
            "",
            "\1\6\1\uffff\12\3\13\uffff\1\7\37\uffff\1\7",
            "\1\11\1\uffff\12\10\7\uffff\6\10\32\uffff\6\10",
            "\1\11\1\uffff\12\10\7\uffff\6\10\32\uffff\6\10",
            "\12\2",
            "",
            "\1\13\1\uffff\12\10\7\uffff\6\10\11\uffff\1\14\20\uffff\6"+
            "\10\11\uffff\1\14",
            "",
            "",
            "\12\11\7\uffff\6\11\11\uffff\1\15\20\uffff\6\11\11\uffff\1"+
            "\15",
            "",
            ""
    };

    static final short[] DFA45_eot = DFA.unpackEncodedString(DFA45_eotS);
    static final short[] DFA45_eof = DFA.unpackEncodedString(DFA45_eofS);
    static final char[] DFA45_min = DFA.unpackEncodedStringToUnsignedChars(DFA45_minS);
    static final char[] DFA45_max = DFA.unpackEncodedStringToUnsignedChars(DFA45_maxS);
    static final short[] DFA45_accept = DFA.unpackEncodedString(DFA45_acceptS);
    static final short[] DFA45_special = DFA.unpackEncodedString(DFA45_specialS);
    static final short[][] DFA45_transition;

    static {
        int numStates = DFA45_transitionS.length;
        DFA45_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA45_transition[i] = DFA.unpackEncodedString(DFA45_transitionS[i]);
        }
    }

    class DFA45 extends DFA {

        public DFA45(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 45;
            this.eot = DFA45_eot;
            this.eof = DFA45_eof;
            this.min = DFA45_min;
            this.max = DFA45_max;
            this.accept = DFA45_accept;
            this.special = DFA45_special;
            this.transition = DFA45_transition;
        }
        public String getDescription() {
            return "6491:19: ( ( RULE_D )+ RULE_E ( RULE_FS )? | ( RULE_D )* '.' ( RULE_D )+ ( RULE_E )? ( RULE_FS )? | ( RULE_D )+ '.' ( RULE_E )? ( RULE_FS )? | RULE_HP ( RULE_H )+ RULE_P ( RULE_FS )? | RULE_HP ( RULE_H )* '.' ( RULE_H )+ RULE_P ( RULE_FS )? | RULE_HP ( RULE_H )+ '.' RULE_P ( RULE_FS )? )";
        }
    }
    static final String DFA53_eotS =
        "\1\uffff\4\30\1\75\1\uffff\16\30\1\uffff\2\31\2\uffff\1\144\1\30"+
        "\1\145\1\147\1\152\1\155\1\160\1\164\1\166\1\170\1\173\1\175\1\u0080"+
        "\1\u0082\4\uffff\1\u0083\7\uffff\1\30\1\u0087\4\30\4\uffff\12\30"+
        "\1\u0099\11\30\1\uffff\13\30\2\uffff\1\31\1\uffff\1\31\5\uffff\1"+
        "\u00b5\2\uffff\1\u00b7\30\uffff\3\30\1\uffff\17\30\1\u00cb\1\30"+
        "\1\uffff\1\30\1\u00ce\27\30\1\31\4\uffff\3\30\1\u00e9\1\u00ea\6"+
        "\30\1\u00f1\1\u00f2\2\30\1\u00f5\1\u00f6\2\30\1\uffff\1\u00f9\1"+
        "\30\1\uffff\1\u00fb\11\30\1\u0105\14\30\1\u0112\2\30\2\uffff\5\30"+
        "\1\u011a\2\uffff\1\u011b\1\30\2\uffff\1\30\1\u011e\1\uffff\1\30"+
        "\1\uffff\1\u0120\6\30\1\u0127\1\30\1\uffff\1\30\1\u012a\2\30\1\u012d"+
        "\7\30\1\uffff\1\30\1\u0136\3\30\1\u013a\1\30\2\uffff\1\30\1\u013d"+
        "\1\uffff\1\u013e\1\uffff\1\u013f\1\u0140\1\u0141\1\u0142\1\u0143"+
        "\1\30\1\uffff\2\30\1\uffff\2\30\1\uffff\7\30\1\u0151\1\uffff\1\u0152"+
        "\2\30\1\uffff\2\30\7\uffff\1\u0157\4\30\1\u015c\7\30\2\uffff\1\u0164"+
        "\1\u0165\1\u0166\1\u0167\1\uffff\1\u0168\1\u0169\1\u016a\1\u016b"+
        "\1\uffff\1\u016c\1\u016d\4\30\1\u0172\12\uffff\1\30\1\u0174\2\30"+
        "\1\uffff\1\u0177\1\uffff\2\30\1\uffff\5\30\1\u017f\1\u0180\2\uffff";
    static final String DFA53_eofS =
        "\u0181\uffff";
    static final String DFA53_minS =
        "\1\10\1\145\1\144\1\145\1\157\1\52\1\uffff\1\162\1\141\2\154\1"+
        "\157\1\146\1\157\1\150\1\171\1\42\1\157\1\150\1\101\1\42\1\uffff"+
        "\2\56\2\uffff\1\56\1\42\1\0\2\75\1\45\1\53\1\55\2\75\1\46\3\75\4"+
        "\uffff\1\76\7\uffff\1\146\1\60\1\144\1\164\1\147\1\144\4\uffff\1"+
        "\145\1\163\1\141\1\156\1\163\1\165\1\164\1\157\1\162\1\164\1\60"+
        "\1\154\1\156\1\157\1\147\1\141\1\151\1\160\1\151\1\42\1\uffff\2"+
        "\151\1\154\2\157\1\145\1\155\1\157\1\164\1\150\1\146\3\56\1\uffff"+
        "\1\56\5\uffff\1\75\2\uffff\1\75\30\uffff\1\164\1\141\1\142\1\uffff"+
        "\1\163\2\157\1\151\1\164\1\165\1\151\1\141\1\145\1\162\1\163\1\145"+
        "\1\155\1\145\1\141\1\60\1\157\1\uffff\1\151\1\60\1\147\1\162\1\156"+
        "\1\145\1\164\1\165\1\164\1\145\1\157\1\151\1\144\1\141\1\154\1\151"+
        "\2\157\1\155\1\156\1\141\1\162\1\141\1\162\1\165\1\56\4\uffff\1"+
        "\141\1\165\1\154\2\60\1\166\1\163\2\162\1\146\1\153\2\60\1\164\1"+
        "\151\2\60\1\162\1\164\1\uffff\1\60\1\156\1\uffff\1\60\1\164\1\145"+
        "\1\157\1\151\2\143\1\144\1\156\1\147\1\60\1\164\1\145\1\147\1\155"+
        "\1\154\1\160\1\145\1\147\1\145\1\164\1\145\1\156\1\60\1\154\1\145"+
        "\2\uffff\1\145\1\164\1\151\1\156\1\151\1\60\2\uffff\1\60\1\156\2"+
        "\uffff\1\156\1\60\1\uffff\1\145\1\uffff\1\60\1\144\1\146\1\143\1"+
        "\164\1\150\1\145\1\60\1\156\1\uffff\1\151\1\60\1\156\1\151\1\60"+
        "\1\154\1\162\1\151\1\164\1\151\1\141\1\143\1\uffff\1\164\1\60\1"+
        "\163\1\145\1\143\1\60\1\145\2\uffff\1\165\1\60\1\uffff\1\60\1\uffff"+
        "\5\60\1\146\1\uffff\1\145\1\154\1\uffff\1\141\1\143\1\uffff\1\145"+
        "\1\151\1\156\1\165\1\143\1\144\1\137\1\60\1\uffff\1\60\1\162\1\164"+
        "\1\uffff\1\163\1\145\7\uffff\1\60\1\144\1\145\1\163\1\146\1\60\1"+
        "\170\1\143\1\141\1\162\3\137\2\uffff\4\60\1\uffff\4\60\1\uffff\2"+
        "\60\1\162\1\156\1\141\1\154\1\60\12\uffff\1\171\1\60\1\163\1\157"+
        "\1\uffff\1\60\1\uffff\1\163\1\143\1\uffff\1\145\1\141\1\162\1\154"+
        "\1\164\2\60\2\uffff";
    static final String DFA53_maxS =
        "\1\176\1\157\1\165\1\145\1\157\1\75\1\uffff\1\162\1\157\1\170\2"+
        "\157\1\156\1\157\1\167\1\171\1\156\1\157\1\150\1\137\1\47\1\uffff"+
        "\1\170\1\145\2\uffff\1\71\1\47\1\uffff\1\75\1\76\2\75\1\76\1\75"+
        "\1\76\2\75\1\174\1\75\4\uffff\1\76\7\uffff\1\154\1\172\1\144\2\164"+
        "\1\144\4\uffff\1\145\1\163\1\141\1\156\1\163\1\165\1\164\1\157\1"+
        "\162\1\164\1\172\1\164\1\156\1\157\1\172\1\162\1\151\1\160\1\163"+
        "\1\42\1\uffff\1\154\1\151\1\164\2\157\1\145\1\155\1\157\1\164\1"+
        "\150\3\146\1\145\1\uffff\1\145\5\uffff\1\75\2\uffff\1\75\30\uffff"+
        "\1\164\1\141\1\142\1\uffff\1\163\2\157\1\151\1\164\1\165\1\151\1"+
        "\141\1\145\1\162\1\164\1\145\1\155\1\145\1\141\1\172\1\157\1\uffff"+
        "\1\151\1\172\1\147\1\162\1\156\1\145\1\164\1\165\1\164\1\145\1\157"+
        "\1\151\1\144\1\141\1\154\1\151\2\157\1\155\1\156\1\141\1\162\1\141"+
        "\1\162\1\165\1\160\4\uffff\1\141\1\165\1\154\2\172\1\166\1\163\2"+
        "\162\1\146\1\153\2\172\1\164\1\151\2\172\1\162\1\164\1\uffff\1\172"+
        "\1\156\1\uffff\1\172\1\164\1\145\1\157\1\151\2\143\1\144\1\156\1"+
        "\147\1\172\1\164\1\145\1\147\1\155\1\154\1\160\1\145\1\147\1\145"+
        "\1\164\1\145\1\156\1\172\1\154\1\145\2\uffff\1\145\1\164\1\151\1"+
        "\156\1\151\1\172\2\uffff\1\172\1\156\2\uffff\1\156\1\172\1\uffff"+
        "\1\145\1\uffff\1\172\1\144\1\146\1\143\1\164\1\150\1\145\1\172\1"+
        "\156\1\uffff\1\151\1\172\1\156\1\151\1\172\1\154\1\162\1\151\1\164"+
        "\1\151\1\141\1\143\1\uffff\1\164\1\172\1\163\1\145\1\143\1\172\1"+
        "\145\2\uffff\1\165\1\172\1\uffff\1\172\1\uffff\5\172\1\146\1\uffff"+
        "\1\145\1\154\1\uffff\1\157\1\143\1\uffff\1\145\1\151\1\156\1\165"+
        "\1\143\1\144\1\137\1\172\1\uffff\1\172\1\162\1\164\1\uffff\1\163"+
        "\1\145\7\uffff\1\172\1\144\1\145\1\163\1\146\1\172\1\170\1\143\1"+
        "\141\1\162\3\137\2\uffff\4\172\1\uffff\4\172\1\uffff\2\172\1\162"+
        "\1\156\1\141\1\154\1\172\12\uffff\1\171\1\172\1\163\1\157\1\uffff"+
        "\1\172\1\uffff\1\163\1\143\1\uffff\1\145\1\141\1\162\1\154\1\164"+
        "\2\172\2\uffff";
    static final String DFA53_acceptS =
        "\6\uffff\1\7\16\uffff\1\66\2\uffff\1\65\1\67\16\uffff\1\121\1\122"+
        "\1\123\1\124\1\uffff\1\126\1\127\1\130\1\131\1\135\1\147\1\150\6"+
        "\uffff\1\5\1\6\1\101\1\141\24\uffff\1\71\16\uffff\1\70\1\uffff\1"+
        "\72\1\132\1\151\1\117\1\73\1\uffff\1\116\1\144\1\uffff\1\115\1\143"+
        "\1\76\1\110\1\137\1\77\1\111\1\112\1\136\1\100\1\140\1\102\1\142"+
        "\1\103\1\113\1\133\1\104\1\145\1\105\1\114\1\146\1\120\1\134\1\125"+
        "\3\uffff\1\17\21\uffff\1\27\32\uffff\1\74\1\106\1\75\1\107\23\uffff"+
        "\1\25\2\uffff\1\31\32\uffff\1\2\1\10\6\uffff\1\12\1\13\2\uffff\1"+
        "\21\1\22\2\uffff\1\26\1\uffff\1\32\11\uffff\1\47\14\uffff\1\1\7"+
        "\uffff\1\11\1\14\2\uffff\1\24\1\uffff\1\36\6\uffff\1\45\2\uffff"+
        "\1\51\2\uffff\1\55\10\uffff\1\20\3\uffff\1\35\2\uffff\1\23\1\30"+
        "\1\37\1\40\1\41\1\42\1\43\15\uffff\1\16\1\3\4\uffff\1\44\4\uffff"+
        "\1\54\7\uffff\1\33\1\34\1\4\1\15\1\46\1\50\1\52\1\53\1\56\1\57\4"+
        "\uffff\1\64\1\uffff\1\61\2\uffff\1\60\7\uffff\1\63\1\62";
    static final String DFA53_specialS =
        "\34\uffff\1\0\u0164\uffff}>";
    static final String[] DFA53_transitionS = {
            "\3\6\1\uffff\2\6\22\uffff\1\6\1\47\1\34\1\63\1\25\1\43\1\44"+
            "\1\31\1\55\1\56\1\42\1\40\1\53\1\41\1\32\1\5\1\26\11\27\1\54"+
            "\1\50\1\37\1\35\1\36\1\62\1\uffff\13\30\1\33\10\30\1\24\5\30"+
            "\1\57\1\uffff\1\60\1\45\1\23\1\uffff\1\2\1\7\1\10\1\1\1\11\1"+
            "\12\1\13\1\30\1\14\2\30\1\15\1\4\4\30\1\3\1\16\1\17\1\20\1\21"+
            "\1\22\3\30\1\51\1\46\1\52\1\61",
            "\1\64\11\uffff\1\65",
            "\1\66\20\uffff\1\67",
            "\1\70",
            "\1\71",
            "\1\72\4\uffff\1\73\15\uffff\1\74",
            "",
            "\1\76",
            "\1\77\6\uffff\1\100\6\uffff\1\101",
            "\1\102\1\uffff\1\103\11\uffff\1\104",
            "\1\105\2\uffff\1\106",
            "\1\107",
            "\1\110\7\uffff\1\111",
            "\1\112",
            "\1\113\1\114\12\uffff\1\115\2\uffff\1\116",
            "\1\117",
            "\1\122\4\uffff\1\31\20\uffff\1\121\65\uffff\1\120",
            "\1\123",
            "\1\124",
            "\1\125\1\126\1\127\3\uffff\1\130\1\uffff\1\131\4\uffff\1\132"+
            "\4\uffff\1\133\1\134\12\uffff\1\135",
            "\1\122\4\uffff\1\31",
            "",
            "\1\141\1\uffff\10\140\2\141\13\uffff\1\141\22\uffff\1\137"+
            "\14\uffff\1\141\22\uffff\1\136",
            "\1\141\1\uffff\12\142\13\uffff\1\141\37\uffff\1\141",
            "",
            "",
            "\1\143\1\uffff\12\141",
            "\1\122\4\uffff\1\31",
            "\12\122\1\uffff\ufff5\122",
            "\1\146",
            "\1\151\1\150",
            "\1\51\24\uffff\1\57\1\uffff\1\153\1\154",
            "\1\157\21\uffff\1\156",
            "\1\162\17\uffff\1\161\1\163",
            "\1\165",
            "\1\167\1\52",
            "\1\172\26\uffff\1\171",
            "\1\174",
            "\1\176\76\uffff\1\177",
            "\1\u0081",
            "",
            "",
            "",
            "",
            "\1\60",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\u0085\5\uffff\1\u0084",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\24\30\1\u0086\5"+
            "\30",
            "\1\u0088",
            "\1\u0089",
            "\1\u008b\5\uffff\1\u008a\5\uffff\1\u008c\1\u008d",
            "\1\u008e",
            "",
            "",
            "",
            "",
            "\1\u008f",
            "\1\u0090",
            "\1\u0091",
            "\1\u0092",
            "\1\u0093",
            "\1\u0094",
            "\1\u0095",
            "\1\u0096",
            "\1\u0097",
            "\1\u0098",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "\1\u009a\7\uffff\1\u009b",
            "\1\u009c",
            "\1\u009d",
            "\1\u009e\22\uffff\1\u009f",
            "\1\u00a0\20\uffff\1\u00a1",
            "\1\u00a2",
            "\1\u00a3",
            "\1\u00a4\11\uffff\1\u00a5",
            "\1\122",
            "",
            "\1\u00a6\2\uffff\1\u00a7",
            "\1\u00a8",
            "\1\u00a9\7\uffff\1\u00aa",
            "\1\u00ab",
            "\1\u00ac",
            "\1\u00ad",
            "\1\u00ae",
            "\1\u00af",
            "\1\u00b0",
            "\1\u00b1",
            "\1\u00b2",
            "\1\141\1\uffff\12\u00b3\7\uffff\6\u00b3\32\uffff\6\u00b3",
            "\1\141\1\uffff\12\u00b3\7\uffff\6\u00b3\32\uffff\6\u00b3",
            "\1\141\1\uffff\10\140\2\141\13\uffff\1\141\37\uffff\1\141",
            "",
            "\1\141\1\uffff\12\142\13\uffff\1\141\37\uffff\1\141",
            "",
            "",
            "",
            "",
            "",
            "\1\u00b4",
            "",
            "",
            "\1\u00b6",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\u00b8",
            "\1\u00b9",
            "\1\u00ba",
            "",
            "\1\u00bb",
            "\1\u00bc",
            "\1\u00bd",
            "\1\u00be",
            "\1\u00bf",
            "\1\u00c0",
            "\1\u00c1",
            "\1\u00c2",
            "\1\u00c3",
            "\1\u00c4",
            "\1\u00c5\1\u00c6",
            "\1\u00c7",
            "\1\u00c8",
            "\1\u00c9",
            "\1\u00ca",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "\1\u00cc",
            "",
            "\1\u00cd",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "\1\u00cf",
            "\1\u00d0",
            "\1\u00d1",
            "\1\u00d2",
            "\1\u00d3",
            "\1\u00d4",
            "\1\u00d5",
            "\1\u00d6",
            "\1\u00d7",
            "\1\u00d8",
            "\1\u00d9",
            "\1\u00da",
            "\1\u00db",
            "\1\u00dc",
            "\1\u00dd",
            "\1\u00de",
            "\1\u00df",
            "\1\u00e0",
            "\1\u00e1",
            "\1\u00e2",
            "\1\u00e3",
            "\1\u00e4",
            "\1\u00e5",
            "\1\141\1\uffff\12\u00b3\7\uffff\6\u00b3\11\uffff\1\141\20"+
            "\uffff\6\u00b3\11\uffff\1\141",
            "",
            "",
            "",
            "",
            "\1\u00e6",
            "\1\u00e7",
            "\1\u00e8",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "\1\u00eb",
            "\1\u00ec",
            "\1\u00ed",
            "\1\u00ee",
            "\1\u00ef",
            "\1\u00f0",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "\1\u00f3",
            "\1\u00f4",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "\1\u00f7",
            "\1\u00f8",
            "",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "\1\u00fa",
            "",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "\1\u00fc",
            "\1\u00fd",
            "\1\u00fe",
            "\1\u00ff",
            "\1\u0100",
            "\1\u0101",
            "\1\u0102",
            "\1\u0103",
            "\1\u0104",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "\1\u0106",
            "\1\u0107",
            "\1\u0108",
            "\1\u0109",
            "\1\u010a",
            "\1\u010b",
            "\1\u010c",
            "\1\u010d",
            "\1\u010e",
            "\1\u010f",
            "\1\u0110",
            "\1\u0111",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "\1\u0113",
            "\1\u0114",
            "",
            "",
            "\1\u0115",
            "\1\u0116",
            "\1\u0117",
            "\1\u0118",
            "\1\u0119",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "",
            "",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "\1\u011c",
            "",
            "",
            "\1\u011d",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "",
            "\1\u011f",
            "",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "\1\u0121",
            "\1\u0122",
            "\1\u0123",
            "\1\u0124",
            "\1\u0125",
            "\1\u0126",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "\1\u0128",
            "",
            "\1\u0129",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "\1\u012b",
            "\1\u012c",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "\1\u012e",
            "\1\u012f",
            "\1\u0130",
            "\1\u0131",
            "\1\u0132",
            "\1\u0133",
            "\1\u0134",
            "",
            "\1\u0135",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "\1\u0137",
            "\1\u0138",
            "\1\u0139",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "\1\u013b",
            "",
            "",
            "\1\u013c",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "\1\u0144",
            "",
            "\1\u0145",
            "\1\u0146",
            "",
            "\1\u0147\15\uffff\1\u0148",
            "\1\u0149",
            "",
            "\1\u014a",
            "\1\u014b",
            "\1\u014c",
            "\1\u014d",
            "\1\u014e",
            "\1\u014f",
            "\1\u0150",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "\1\u0153",
            "\1\u0154",
            "",
            "\1\u0155",
            "\1\u0156",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "\1\u0158",
            "\1\u0159",
            "\1\u015a",
            "\1\u015b",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "\1\u015d",
            "\1\u015e",
            "\1\u015f",
            "\1\u0160",
            "\1\u0161",
            "\1\u0162",
            "\1\u0163",
            "",
            "",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "\1\u016e",
            "\1\u016f",
            "\1\u0170",
            "\1\u0171",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\u0173",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "\1\u0175",
            "\1\u0176",
            "",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "",
            "\1\u0178",
            "\1\u0179",
            "",
            "\1\u017a",
            "\1\u017b",
            "\1\u017c",
            "\1\u017d",
            "\1\u017e",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "",
            ""
    };

    static final short[] DFA53_eot = DFA.unpackEncodedString(DFA53_eotS);
    static final short[] DFA53_eof = DFA.unpackEncodedString(DFA53_eofS);
    static final char[] DFA53_min = DFA.unpackEncodedStringToUnsignedChars(DFA53_minS);
    static final char[] DFA53_max = DFA.unpackEncodedStringToUnsignedChars(DFA53_maxS);
    static final short[] DFA53_accept = DFA.unpackEncodedString(DFA53_acceptS);
    static final short[] DFA53_special = DFA.unpackEncodedString(DFA53_specialS);
    static final short[][] DFA53_transition;

    static {
        int numStates = DFA53_transitionS.length;
        DFA53_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA53_transition[i] = DFA.unpackEncodedString(DFA53_transitionS[i]);
        }
    }

    class DFA53 extends DFA {

        public DFA53(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 53;
            this.eot = DFA53_eot;
            this.eof = DFA53_eof;
            this.min = DFA53_min;
            this.max = DFA53_max;
            this.accept = DFA53_accept;
            this.special = DFA53_special;
            this.transition = DFA53_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( RULE_DELTA | RULE_ADDS | RULE_REMOVES | RULE_MODIFIES | RULE_ML_COMMENT | RULE_SL_COMMENT | RULE_WS | RULE_AUTO | RULE_BREAK | RULE_CASE | RULE_CHAR | RULE_CONST | RULE_CONTINUE | RULE_DEFAULT | RULE_DO | RULE_DOUBLE | RULE_ELSE | RULE_ENUM | RULE_EXTERN | RULE_FLOAT | RULE_FOR | RULE_GOTO | RULE_IF | RULE_INLINE | RULE_INT | RULE_LONG | RULE_REGISTER | RULE_RESTRICT | RULE_RETURN | RULE_SHORT | RULE_SIGNED | RULE_SIZEOF | RULE_STATIC | RULE_STRUCT | RULE_SWITCH | RULE_TYPEDEF | RULE_UNION | RULE_UNSIGNED | RULE_VOID | RULE_VOLATILE | RULE_WHILE | RULE_ALIGNAS | RULE_ALIGNOF | RULE_ATOMIC | RULE_BOOL | RULE_COMPLEX | RULE_GENERIC | RULE_IMAGINARY | RULE_NORETURN | RULE_STATIC_ASSERT | RULE_THREAD_LOCAL | RULE_FUNC_NAME | RULE_IDENTIFIER | RULE_CUSTOM | RULE_I_CONSTANT | RULE_F_CONSTANT | RULE_STRING_LITERAL | RULE_ELLIPSIS | RULE_ASSIGN | RULE_RIGHT_ASSIGN | RULE_LEFT_ASSIGN | RULE_ADD_ASSIGN | RULE_SUB_ASSIGN | RULE_MUL_ASSIGN | RULE_DIV_ASSIGN | RULE_MOD_ASSIGN | RULE_AND_ASSIGN | RULE_XOR_ASSIGN | RULE_OR_ASSIGN | RULE_RIGHT_OP | RULE_LEFT_OP | RULE_INC_OP | RULE_DEC_OP | RULE_PTR_OP | RULE_LAND_OP | RULE_LOR_OP | RULE_LE_OP | RULE_GE_OP | RULE_EQ_OP | RULE_NE_OP | RULE_SEMICOLON | RULE_LCBRACKET | RULE_RCBRACKET | RULE_COMMA | RULE_COLON | RULE_LPAREN | RULE_RPAREN | RULE_LBRACKET | RULE_RBRACKET | RULE_DOT | RULE_AND_OP | RULE_NOT_OP | RULE_TILDE | RULE_MINUS_OP | RULE_ADD_OP | RULE_MULT_OP | RULE_DIV_OP | RULE_MOD_OP | RULE_L_OP | RULE_G_OP | RULE_XOR_OP | RULE_OR_OP | RULE_Q_OP | RULE_INCLUDE | RULE_DQUOTE );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            IntStream input = _input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA53_28 = input.LA(1);

                        s = -1;
                        if ( ((LA53_28>='\u0000' && LA53_28<='\t')||(LA53_28>='\u000B' && LA53_28<='\uFFFF')) ) {s = 82;}

                        else s = 101;

                        if ( s>=0 ) return s;
                        break;
            }
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 53, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

}