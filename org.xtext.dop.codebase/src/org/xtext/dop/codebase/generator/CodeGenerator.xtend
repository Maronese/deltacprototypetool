package org.xtext.dop.codebase.generator

import org.xtext.dop.rappresentation.Project
import org.xtext.dop.codebase.printer.Printer
import org.xtext.dop.rappresentation.Resource
import java.io.File
import java.io.PrintWriter
import java.io.IOException

class CodeGenerator {
	
	Project project
	extension Printer printer
	String path
	
	new(Project project) {
		this.project = project
		this.path = "spl_product_"+project.projectName
	}
	
	def private createFile(Resource resource) throws IOException {
		printer = new Printer(resource)
		//val model = resource.source
		val name = resource.name
		val file = new File(path+'/'+name+".c")
		val writer = new PrintWriter(file)
		
		if(file.exists()) file.delete();
		writer.print(print)
		writer.close
		return file
	}
	
	def generateProject() {
		val folder = new File(path)
		folder.mkdir
		for(Resource resource : project.values) {
			resource.createFile.createNewFile
		}
	}
}