/**
 */
package org.xtext.dop.productdeclaration.productDeclaration;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Deltas</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.productdeclaration.productDeclaration.Deltas#getList <em>List</em>}</li>
 * </ul>
 *
 * @see org.xtext.dop.productdeclaration.productDeclaration.ProductDeclarationPackage#getDeltas()
 * @model
 * @generated
 */
public interface Deltas extends EObject
{
  /**
   * Returns the value of the '<em><b>List</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.dop.productdeclaration.productDeclaration.ListElem}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>List</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>List</em>' containment reference list.
   * @see org.xtext.dop.productdeclaration.productDeclaration.ProductDeclarationPackage#getDeltas_List()
   * @model containment="true"
   * @generated
   */
  EList<ListElem> getList();

} // Deltas
