package org.xtext.dop.productdeclaration.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import org.xtext.dop.productdeclaration.services.ProductDeclarationGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalProductDeclarationParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'SPL'", "'{'", "'Features'", "'='", "'}'", "'Deltas'", "'Constraints'", "'Partitions'", "'Products'", "';'", "'when'", "'('", "')'", "'|'", "'&'", "'!'", "','"
    };
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int RULE_INT=5;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalProductDeclarationParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalProductDeclarationParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalProductDeclarationParser.tokenNames; }
    public String getGrammarFileName() { return "../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g"; }


     
     	private ProductDeclarationGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(ProductDeclarationGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRuleProductDeclaration"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:60:1: entryRuleProductDeclaration : ruleProductDeclaration EOF ;
    public final void entryRuleProductDeclaration() throws RecognitionException {
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:61:1: ( ruleProductDeclaration EOF )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:62:1: ruleProductDeclaration EOF
            {
             before(grammarAccess.getProductDeclarationRule()); 
            pushFollow(FOLLOW_ruleProductDeclaration_in_entryRuleProductDeclaration61);
            ruleProductDeclaration();

            state._fsp--;

             after(grammarAccess.getProductDeclarationRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleProductDeclaration68); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleProductDeclaration"


    // $ANTLR start "ruleProductDeclaration"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:69:1: ruleProductDeclaration : ( ( rule__ProductDeclaration__Group__0 ) ) ;
    public final void ruleProductDeclaration() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:73:2: ( ( ( rule__ProductDeclaration__Group__0 ) ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:74:1: ( ( rule__ProductDeclaration__Group__0 ) )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:74:1: ( ( rule__ProductDeclaration__Group__0 ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:75:1: ( rule__ProductDeclaration__Group__0 )
            {
             before(grammarAccess.getProductDeclarationAccess().getGroup()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:76:1: ( rule__ProductDeclaration__Group__0 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:76:2: rule__ProductDeclaration__Group__0
            {
            pushFollow(FOLLOW_rule__ProductDeclaration__Group__0_in_ruleProductDeclaration94);
            rule__ProductDeclaration__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getProductDeclarationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleProductDeclaration"


    // $ANTLR start "entryRuleFeatures"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:88:1: entryRuleFeatures : ruleFeatures EOF ;
    public final void entryRuleFeatures() throws RecognitionException {
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:89:1: ( ruleFeatures EOF )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:90:1: ruleFeatures EOF
            {
             before(grammarAccess.getFeaturesRule()); 
            pushFollow(FOLLOW_ruleFeatures_in_entryRuleFeatures121);
            ruleFeatures();

            state._fsp--;

             after(grammarAccess.getFeaturesRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleFeatures128); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFeatures"


    // $ANTLR start "ruleFeatures"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:97:1: ruleFeatures : ( ( ( rule__Features__ListAssignment ) ) ( ( rule__Features__ListAssignment )* ) ) ;
    public final void ruleFeatures() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:101:2: ( ( ( ( rule__Features__ListAssignment ) ) ( ( rule__Features__ListAssignment )* ) ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:102:1: ( ( ( rule__Features__ListAssignment ) ) ( ( rule__Features__ListAssignment )* ) )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:102:1: ( ( ( rule__Features__ListAssignment ) ) ( ( rule__Features__ListAssignment )* ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:103:1: ( ( rule__Features__ListAssignment ) ) ( ( rule__Features__ListAssignment )* )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:103:1: ( ( rule__Features__ListAssignment ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:104:1: ( rule__Features__ListAssignment )
            {
             before(grammarAccess.getFeaturesAccess().getListAssignment()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:105:1: ( rule__Features__ListAssignment )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:105:2: rule__Features__ListAssignment
            {
            pushFollow(FOLLOW_rule__Features__ListAssignment_in_ruleFeatures156);
            rule__Features__ListAssignment();

            state._fsp--;


            }

             after(grammarAccess.getFeaturesAccess().getListAssignment()); 

            }

            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:108:1: ( ( rule__Features__ListAssignment )* )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:109:1: ( rule__Features__ListAssignment )*
            {
             before(grammarAccess.getFeaturesAccess().getListAssignment()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:110:1: ( rule__Features__ListAssignment )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==RULE_ID) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:110:2: rule__Features__ListAssignment
            	    {
            	    pushFollow(FOLLOW_rule__Features__ListAssignment_in_ruleFeatures168);
            	    rule__Features__ListAssignment();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getFeaturesAccess().getListAssignment()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFeatures"


    // $ANTLR start "entryRuleDeltas"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:123:1: entryRuleDeltas : ruleDeltas EOF ;
    public final void entryRuleDeltas() throws RecognitionException {
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:124:1: ( ruleDeltas EOF )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:125:1: ruleDeltas EOF
            {
             before(grammarAccess.getDeltasRule()); 
            pushFollow(FOLLOW_ruleDeltas_in_entryRuleDeltas198);
            ruleDeltas();

            state._fsp--;

             after(grammarAccess.getDeltasRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleDeltas205); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDeltas"


    // $ANTLR start "ruleDeltas"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:132:1: ruleDeltas : ( ( ( rule__Deltas__ListAssignment ) ) ( ( rule__Deltas__ListAssignment )* ) ) ;
    public final void ruleDeltas() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:136:2: ( ( ( ( rule__Deltas__ListAssignment ) ) ( ( rule__Deltas__ListAssignment )* ) ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:137:1: ( ( ( rule__Deltas__ListAssignment ) ) ( ( rule__Deltas__ListAssignment )* ) )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:137:1: ( ( ( rule__Deltas__ListAssignment ) ) ( ( rule__Deltas__ListAssignment )* ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:138:1: ( ( rule__Deltas__ListAssignment ) ) ( ( rule__Deltas__ListAssignment )* )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:138:1: ( ( rule__Deltas__ListAssignment ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:139:1: ( rule__Deltas__ListAssignment )
            {
             before(grammarAccess.getDeltasAccess().getListAssignment()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:140:1: ( rule__Deltas__ListAssignment )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:140:2: rule__Deltas__ListAssignment
            {
            pushFollow(FOLLOW_rule__Deltas__ListAssignment_in_ruleDeltas233);
            rule__Deltas__ListAssignment();

            state._fsp--;


            }

             after(grammarAccess.getDeltasAccess().getListAssignment()); 

            }

            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:143:1: ( ( rule__Deltas__ListAssignment )* )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:144:1: ( rule__Deltas__ListAssignment )*
            {
             before(grammarAccess.getDeltasAccess().getListAssignment()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:145:1: ( rule__Deltas__ListAssignment )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==RULE_ID) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:145:2: rule__Deltas__ListAssignment
            	    {
            	    pushFollow(FOLLOW_rule__Deltas__ListAssignment_in_ruleDeltas245);
            	    rule__Deltas__ListAssignment();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

             after(grammarAccess.getDeltasAccess().getListAssignment()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDeltas"


    // $ANTLR start "entryRuleProducts"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:158:1: entryRuleProducts : ruleProducts EOF ;
    public final void entryRuleProducts() throws RecognitionException {
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:159:1: ( ruleProducts EOF )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:160:1: ruleProducts EOF
            {
             before(grammarAccess.getProductsRule()); 
            pushFollow(FOLLOW_ruleProducts_in_entryRuleProducts275);
            ruleProducts();

            state._fsp--;

             after(grammarAccess.getProductsRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleProducts282); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleProducts"


    // $ANTLR start "ruleProducts"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:167:1: ruleProducts : ( ( ( rule__Products__ListAssignment ) ) ( ( rule__Products__ListAssignment )* ) ) ;
    public final void ruleProducts() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:171:2: ( ( ( ( rule__Products__ListAssignment ) ) ( ( rule__Products__ListAssignment )* ) ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:172:1: ( ( ( rule__Products__ListAssignment ) ) ( ( rule__Products__ListAssignment )* ) )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:172:1: ( ( ( rule__Products__ListAssignment ) ) ( ( rule__Products__ListAssignment )* ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:173:1: ( ( rule__Products__ListAssignment ) ) ( ( rule__Products__ListAssignment )* )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:173:1: ( ( rule__Products__ListAssignment ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:174:1: ( rule__Products__ListAssignment )
            {
             before(grammarAccess.getProductsAccess().getListAssignment()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:175:1: ( rule__Products__ListAssignment )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:175:2: rule__Products__ListAssignment
            {
            pushFollow(FOLLOW_rule__Products__ListAssignment_in_ruleProducts310);
            rule__Products__ListAssignment();

            state._fsp--;


            }

             after(grammarAccess.getProductsAccess().getListAssignment()); 

            }

            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:178:1: ( ( rule__Products__ListAssignment )* )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:179:1: ( rule__Products__ListAssignment )*
            {
             before(grammarAccess.getProductsAccess().getListAssignment()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:180:1: ( rule__Products__ListAssignment )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==RULE_ID) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:180:2: rule__Products__ListAssignment
            	    {
            	    pushFollow(FOLLOW_rule__Products__ListAssignment_in_ruleProducts322);
            	    rule__Products__ListAssignment();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

             after(grammarAccess.getProductsAccess().getListAssignment()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleProducts"


    // $ANTLR start "entryRuleProduct"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:193:1: entryRuleProduct : ruleProduct EOF ;
    public final void entryRuleProduct() throws RecognitionException {
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:194:1: ( ruleProduct EOF )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:195:1: ruleProduct EOF
            {
             before(grammarAccess.getProductRule()); 
            pushFollow(FOLLOW_ruleProduct_in_entryRuleProduct352);
            ruleProduct();

            state._fsp--;

             after(grammarAccess.getProductRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleProduct359); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleProduct"


    // $ANTLR start "ruleProduct"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:202:1: ruleProduct : ( ( rule__Product__Group__0 ) ) ;
    public final void ruleProduct() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:206:2: ( ( ( rule__Product__Group__0 ) ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:207:1: ( ( rule__Product__Group__0 ) )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:207:1: ( ( rule__Product__Group__0 ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:208:1: ( rule__Product__Group__0 )
            {
             before(grammarAccess.getProductAccess().getGroup()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:209:1: ( rule__Product__Group__0 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:209:2: rule__Product__Group__0
            {
            pushFollow(FOLLOW_rule__Product__Group__0_in_ruleProduct385);
            rule__Product__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getProductAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleProduct"


    // $ANTLR start "entryRulePartitions"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:221:1: entryRulePartitions : rulePartitions EOF ;
    public final void entryRulePartitions() throws RecognitionException {
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:222:1: ( rulePartitions EOF )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:223:1: rulePartitions EOF
            {
             before(grammarAccess.getPartitionsRule()); 
            pushFollow(FOLLOW_rulePartitions_in_entryRulePartitions412);
            rulePartitions();

            state._fsp--;

             after(grammarAccess.getPartitionsRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulePartitions419); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePartitions"


    // $ANTLR start "rulePartitions"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:230:1: rulePartitions : ( ( ( rule__Partitions__ListAssignment ) ) ( ( rule__Partitions__ListAssignment )* ) ) ;
    public final void rulePartitions() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:234:2: ( ( ( ( rule__Partitions__ListAssignment ) ) ( ( rule__Partitions__ListAssignment )* ) ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:235:1: ( ( ( rule__Partitions__ListAssignment ) ) ( ( rule__Partitions__ListAssignment )* ) )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:235:1: ( ( ( rule__Partitions__ListAssignment ) ) ( ( rule__Partitions__ListAssignment )* ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:236:1: ( ( rule__Partitions__ListAssignment ) ) ( ( rule__Partitions__ListAssignment )* )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:236:1: ( ( rule__Partitions__ListAssignment ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:237:1: ( rule__Partitions__ListAssignment )
            {
             before(grammarAccess.getPartitionsAccess().getListAssignment()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:238:1: ( rule__Partitions__ListAssignment )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:238:2: rule__Partitions__ListAssignment
            {
            pushFollow(FOLLOW_rule__Partitions__ListAssignment_in_rulePartitions447);
            rule__Partitions__ListAssignment();

            state._fsp--;


            }

             after(grammarAccess.getPartitionsAccess().getListAssignment()); 

            }

            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:241:1: ( ( rule__Partitions__ListAssignment )* )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:242:1: ( rule__Partitions__ListAssignment )*
            {
             before(grammarAccess.getPartitionsAccess().getListAssignment()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:243:1: ( rule__Partitions__ListAssignment )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==12) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:243:2: rule__Partitions__ListAssignment
            	    {
            	    pushFollow(FOLLOW_rule__Partitions__ListAssignment_in_rulePartitions459);
            	    rule__Partitions__ListAssignment();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

             after(grammarAccess.getPartitionsAccess().getListAssignment()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePartitions"


    // $ANTLR start "entryRulePartition"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:256:1: entryRulePartition : rulePartition EOF ;
    public final void entryRulePartition() throws RecognitionException {
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:257:1: ( rulePartition EOF )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:258:1: rulePartition EOF
            {
             before(grammarAccess.getPartitionRule()); 
            pushFollow(FOLLOW_rulePartition_in_entryRulePartition489);
            rulePartition();

            state._fsp--;

             after(grammarAccess.getPartitionRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulePartition496); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePartition"


    // $ANTLR start "rulePartition"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:265:1: rulePartition : ( ( rule__Partition__Group__0 ) ) ;
    public final void rulePartition() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:269:2: ( ( ( rule__Partition__Group__0 ) ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:270:1: ( ( rule__Partition__Group__0 ) )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:270:1: ( ( rule__Partition__Group__0 ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:271:1: ( rule__Partition__Group__0 )
            {
             before(grammarAccess.getPartitionAccess().getGroup()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:272:1: ( rule__Partition__Group__0 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:272:2: rule__Partition__Group__0
            {
            pushFollow(FOLLOW_rule__Partition__Group__0_in_rulePartition522);
            rule__Partition__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPartitionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePartition"


    // $ANTLR start "entryRuleConstraint"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:284:1: entryRuleConstraint : ruleConstraint EOF ;
    public final void entryRuleConstraint() throws RecognitionException {
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:285:1: ( ruleConstraint EOF )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:286:1: ruleConstraint EOF
            {
             before(grammarAccess.getConstraintRule()); 
            pushFollow(FOLLOW_ruleConstraint_in_entryRuleConstraint549);
            ruleConstraint();

            state._fsp--;

             after(grammarAccess.getConstraintRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleConstraint556); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleConstraint"


    // $ANTLR start "ruleConstraint"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:293:1: ruleConstraint : ( ( rule__Constraint__Group__0 ) ) ;
    public final void ruleConstraint() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:297:2: ( ( ( rule__Constraint__Group__0 ) ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:298:1: ( ( rule__Constraint__Group__0 ) )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:298:1: ( ( rule__Constraint__Group__0 ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:299:1: ( rule__Constraint__Group__0 )
            {
             before(grammarAccess.getConstraintAccess().getGroup()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:300:1: ( rule__Constraint__Group__0 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:300:2: rule__Constraint__Group__0
            {
            pushFollow(FOLLOW_rule__Constraint__Group__0_in_ruleConstraint582);
            rule__Constraint__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getConstraintAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleConstraint"


    // $ANTLR start "entryRuleListElem"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:312:1: entryRuleListElem : ruleListElem EOF ;
    public final void entryRuleListElem() throws RecognitionException {
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:313:1: ( ruleListElem EOF )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:314:1: ruleListElem EOF
            {
             before(grammarAccess.getListElemRule()); 
            pushFollow(FOLLOW_ruleListElem_in_entryRuleListElem609);
            ruleListElem();

            state._fsp--;

             after(grammarAccess.getListElemRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleListElem616); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleListElem"


    // $ANTLR start "ruleListElem"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:321:1: ruleListElem : ( ( rule__ListElem__Group__0 ) ) ;
    public final void ruleListElem() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:325:2: ( ( ( rule__ListElem__Group__0 ) ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:326:1: ( ( rule__ListElem__Group__0 ) )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:326:1: ( ( rule__ListElem__Group__0 ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:327:1: ( rule__ListElem__Group__0 )
            {
             before(grammarAccess.getListElemAccess().getGroup()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:328:1: ( rule__ListElem__Group__0 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:328:2: rule__ListElem__Group__0
            {
            pushFollow(FOLLOW_rule__ListElem__Group__0_in_ruleListElem642);
            rule__ListElem__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getListElemAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleListElem"


    // $ANTLR start "entryRuleElement"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:340:1: entryRuleElement : ruleElement EOF ;
    public final void entryRuleElement() throws RecognitionException {
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:341:1: ( ruleElement EOF )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:342:1: ruleElement EOF
            {
             before(grammarAccess.getElementRule()); 
            pushFollow(FOLLOW_ruleElement_in_entryRuleElement669);
            ruleElement();

            state._fsp--;

             after(grammarAccess.getElementRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleElement676); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleElement"


    // $ANTLR start "ruleElement"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:349:1: ruleElement : ( ( rule__Element__NameAssignment ) ) ;
    public final void ruleElement() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:353:2: ( ( ( rule__Element__NameAssignment ) ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:354:1: ( ( rule__Element__NameAssignment ) )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:354:1: ( ( rule__Element__NameAssignment ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:355:1: ( rule__Element__NameAssignment )
            {
             before(grammarAccess.getElementAccess().getNameAssignment()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:356:1: ( rule__Element__NameAssignment )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:356:2: rule__Element__NameAssignment
            {
            pushFollow(FOLLOW_rule__Element__NameAssignment_in_ruleElement702);
            rule__Element__NameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getElementAccess().getNameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleElement"


    // $ANTLR start "entryRuleCore"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:368:1: entryRuleCore : ruleCore EOF ;
    public final void entryRuleCore() throws RecognitionException {
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:369:1: ( ruleCore EOF )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:370:1: ruleCore EOF
            {
             before(grammarAccess.getCoreRule()); 
            pushFollow(FOLLOW_ruleCore_in_entryRuleCore729);
            ruleCore();

            state._fsp--;

             after(grammarAccess.getCoreRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleCore736); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCore"


    // $ANTLR start "ruleCore"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:377:1: ruleCore : ( ( rule__Core__Group__0 ) ) ;
    public final void ruleCore() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:381:2: ( ( ( rule__Core__Group__0 ) ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:382:1: ( ( rule__Core__Group__0 ) )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:382:1: ( ( rule__Core__Group__0 ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:383:1: ( rule__Core__Group__0 )
            {
             before(grammarAccess.getCoreAccess().getGroup()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:384:1: ( rule__Core__Group__0 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:384:2: rule__Core__Group__0
            {
            pushFollow(FOLLOW_rule__Core__Group__0_in_ruleCore762);
            rule__Core__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCoreAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCore"


    // $ANTLR start "entryRuleExpression"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:396:1: entryRuleExpression : ruleExpression EOF ;
    public final void entryRuleExpression() throws RecognitionException {
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:397:1: ( ruleExpression EOF )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:398:1: ruleExpression EOF
            {
             before(grammarAccess.getExpressionRule()); 
            pushFollow(FOLLOW_ruleExpression_in_entryRuleExpression789);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getExpressionRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleExpression796); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExpression"


    // $ANTLR start "ruleExpression"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:405:1: ruleExpression : ( ruleOr ) ;
    public final void ruleExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:409:2: ( ( ruleOr ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:410:1: ( ruleOr )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:410:1: ( ruleOr )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:411:1: ruleOr
            {
             before(grammarAccess.getExpressionAccess().getOrParserRuleCall()); 
            pushFollow(FOLLOW_ruleOr_in_ruleExpression822);
            ruleOr();

            state._fsp--;

             after(grammarAccess.getExpressionAccess().getOrParserRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExpression"


    // $ANTLR start "entryRuleOr"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:424:1: entryRuleOr : ruleOr EOF ;
    public final void entryRuleOr() throws RecognitionException {
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:425:1: ( ruleOr EOF )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:426:1: ruleOr EOF
            {
             before(grammarAccess.getOrRule()); 
            pushFollow(FOLLOW_ruleOr_in_entryRuleOr848);
            ruleOr();

            state._fsp--;

             after(grammarAccess.getOrRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleOr855); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOr"


    // $ANTLR start "ruleOr"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:433:1: ruleOr : ( ( rule__Or__Group__0 ) ) ;
    public final void ruleOr() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:437:2: ( ( ( rule__Or__Group__0 ) ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:438:1: ( ( rule__Or__Group__0 ) )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:438:1: ( ( rule__Or__Group__0 ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:439:1: ( rule__Or__Group__0 )
            {
             before(grammarAccess.getOrAccess().getGroup()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:440:1: ( rule__Or__Group__0 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:440:2: rule__Or__Group__0
            {
            pushFollow(FOLLOW_rule__Or__Group__0_in_ruleOr881);
            rule__Or__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOrAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOr"


    // $ANTLR start "entryRuleAnd"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:452:1: entryRuleAnd : ruleAnd EOF ;
    public final void entryRuleAnd() throws RecognitionException {
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:453:1: ( ruleAnd EOF )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:454:1: ruleAnd EOF
            {
             before(grammarAccess.getAndRule()); 
            pushFollow(FOLLOW_ruleAnd_in_entryRuleAnd908);
            ruleAnd();

            state._fsp--;

             after(grammarAccess.getAndRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleAnd915); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAnd"


    // $ANTLR start "ruleAnd"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:461:1: ruleAnd : ( ( rule__And__Group__0 ) ) ;
    public final void ruleAnd() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:465:2: ( ( ( rule__And__Group__0 ) ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:466:1: ( ( rule__And__Group__0 ) )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:466:1: ( ( rule__And__Group__0 ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:467:1: ( rule__And__Group__0 )
            {
             before(grammarAccess.getAndAccess().getGroup()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:468:1: ( rule__And__Group__0 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:468:2: rule__And__Group__0
            {
            pushFollow(FOLLOW_rule__And__Group__0_in_ruleAnd941);
            rule__And__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAndAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAnd"


    // $ANTLR start "entryRulePrimary"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:480:1: entryRulePrimary : rulePrimary EOF ;
    public final void entryRulePrimary() throws RecognitionException {
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:481:1: ( rulePrimary EOF )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:482:1: rulePrimary EOF
            {
             before(grammarAccess.getPrimaryRule()); 
            pushFollow(FOLLOW_rulePrimary_in_entryRulePrimary968);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getPrimaryRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulePrimary975); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePrimary"


    // $ANTLR start "rulePrimary"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:489:1: rulePrimary : ( ( rule__Primary__Alternatives ) ) ;
    public final void rulePrimary() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:493:2: ( ( ( rule__Primary__Alternatives ) ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:494:1: ( ( rule__Primary__Alternatives ) )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:494:1: ( ( rule__Primary__Alternatives ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:495:1: ( rule__Primary__Alternatives )
            {
             before(grammarAccess.getPrimaryAccess().getAlternatives()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:496:1: ( rule__Primary__Alternatives )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:496:2: rule__Primary__Alternatives
            {
            pushFollow(FOLLOW_rule__Primary__Alternatives_in_rulePrimary1001);
            rule__Primary__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getPrimaryAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePrimary"


    // $ANTLR start "entryRuleAtomic"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:508:1: entryRuleAtomic : ruleAtomic EOF ;
    public final void entryRuleAtomic() throws RecognitionException {
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:509:1: ( ruleAtomic EOF )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:510:1: ruleAtomic EOF
            {
             before(grammarAccess.getAtomicRule()); 
            pushFollow(FOLLOW_ruleAtomic_in_entryRuleAtomic1028);
            ruleAtomic();

            state._fsp--;

             after(grammarAccess.getAtomicRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleAtomic1035); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAtomic"


    // $ANTLR start "ruleAtomic"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:517:1: ruleAtomic : ( ruleElement ) ;
    public final void ruleAtomic() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:521:2: ( ( ruleElement ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:522:1: ( ruleElement )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:522:1: ( ruleElement )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:523:1: ruleElement
            {
             before(grammarAccess.getAtomicAccess().getElementParserRuleCall()); 
            pushFollow(FOLLOW_ruleElement_in_ruleAtomic1061);
            ruleElement();

            state._fsp--;

             after(grammarAccess.getAtomicAccess().getElementParserRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAtomic"


    // $ANTLR start "rule__Primary__Alternatives"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:536:1: rule__Primary__Alternatives : ( ( ruleAtomic ) | ( ( rule__Primary__Group_1__0 ) ) | ( ( rule__Primary__Group_2__0 ) ) );
    public final void rule__Primary__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:540:1: ( ( ruleAtomic ) | ( ( rule__Primary__Group_1__0 ) ) | ( ( rule__Primary__Group_2__0 ) ) )
            int alt5=3;
            switch ( input.LA(1) ) {
            case RULE_ID:
                {
                alt5=1;
                }
                break;
            case 22:
                {
                alt5=2;
                }
                break;
            case 26:
                {
                alt5=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }

            switch (alt5) {
                case 1 :
                    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:541:1: ( ruleAtomic )
                    {
                    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:541:1: ( ruleAtomic )
                    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:542:1: ruleAtomic
                    {
                     before(grammarAccess.getPrimaryAccess().getAtomicParserRuleCall_0()); 
                    pushFollow(FOLLOW_ruleAtomic_in_rule__Primary__Alternatives1096);
                    ruleAtomic();

                    state._fsp--;

                     after(grammarAccess.getPrimaryAccess().getAtomicParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:547:6: ( ( rule__Primary__Group_1__0 ) )
                    {
                    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:547:6: ( ( rule__Primary__Group_1__0 ) )
                    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:548:1: ( rule__Primary__Group_1__0 )
                    {
                     before(grammarAccess.getPrimaryAccess().getGroup_1()); 
                    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:549:1: ( rule__Primary__Group_1__0 )
                    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:549:2: rule__Primary__Group_1__0
                    {
                    pushFollow(FOLLOW_rule__Primary__Group_1__0_in_rule__Primary__Alternatives1113);
                    rule__Primary__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getPrimaryAccess().getGroup_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:553:6: ( ( rule__Primary__Group_2__0 ) )
                    {
                    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:553:6: ( ( rule__Primary__Group_2__0 ) )
                    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:554:1: ( rule__Primary__Group_2__0 )
                    {
                     before(grammarAccess.getPrimaryAccess().getGroup_2()); 
                    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:555:1: ( rule__Primary__Group_2__0 )
                    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:555:2: rule__Primary__Group_2__0
                    {
                    pushFollow(FOLLOW_rule__Primary__Group_2__0_in_rule__Primary__Alternatives1131);
                    rule__Primary__Group_2__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getPrimaryAccess().getGroup_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Alternatives"


    // $ANTLR start "rule__ProductDeclaration__Group__0"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:566:1: rule__ProductDeclaration__Group__0 : rule__ProductDeclaration__Group__0__Impl rule__ProductDeclaration__Group__1 ;
    public final void rule__ProductDeclaration__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:570:1: ( rule__ProductDeclaration__Group__0__Impl rule__ProductDeclaration__Group__1 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:571:2: rule__ProductDeclaration__Group__0__Impl rule__ProductDeclaration__Group__1
            {
            pushFollow(FOLLOW_rule__ProductDeclaration__Group__0__Impl_in_rule__ProductDeclaration__Group__01162);
            rule__ProductDeclaration__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ProductDeclaration__Group__1_in_rule__ProductDeclaration__Group__01165);
            rule__ProductDeclaration__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__0"


    // $ANTLR start "rule__ProductDeclaration__Group__0__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:578:1: rule__ProductDeclaration__Group__0__Impl : ( 'SPL' ) ;
    public final void rule__ProductDeclaration__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:582:1: ( ( 'SPL' ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:583:1: ( 'SPL' )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:583:1: ( 'SPL' )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:584:1: 'SPL'
            {
             before(grammarAccess.getProductDeclarationAccess().getSPLKeyword_0()); 
            match(input,11,FOLLOW_11_in_rule__ProductDeclaration__Group__0__Impl1193); 
             after(grammarAccess.getProductDeclarationAccess().getSPLKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__0__Impl"


    // $ANTLR start "rule__ProductDeclaration__Group__1"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:597:1: rule__ProductDeclaration__Group__1 : rule__ProductDeclaration__Group__1__Impl rule__ProductDeclaration__Group__2 ;
    public final void rule__ProductDeclaration__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:601:1: ( rule__ProductDeclaration__Group__1__Impl rule__ProductDeclaration__Group__2 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:602:2: rule__ProductDeclaration__Group__1__Impl rule__ProductDeclaration__Group__2
            {
            pushFollow(FOLLOW_rule__ProductDeclaration__Group__1__Impl_in_rule__ProductDeclaration__Group__11224);
            rule__ProductDeclaration__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ProductDeclaration__Group__2_in_rule__ProductDeclaration__Group__11227);
            rule__ProductDeclaration__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__1"


    // $ANTLR start "rule__ProductDeclaration__Group__1__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:609:1: rule__ProductDeclaration__Group__1__Impl : ( ( rule__ProductDeclaration__NameAssignment_1 ) ) ;
    public final void rule__ProductDeclaration__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:613:1: ( ( ( rule__ProductDeclaration__NameAssignment_1 ) ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:614:1: ( ( rule__ProductDeclaration__NameAssignment_1 ) )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:614:1: ( ( rule__ProductDeclaration__NameAssignment_1 ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:615:1: ( rule__ProductDeclaration__NameAssignment_1 )
            {
             before(grammarAccess.getProductDeclarationAccess().getNameAssignment_1()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:616:1: ( rule__ProductDeclaration__NameAssignment_1 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:616:2: rule__ProductDeclaration__NameAssignment_1
            {
            pushFollow(FOLLOW_rule__ProductDeclaration__NameAssignment_1_in_rule__ProductDeclaration__Group__1__Impl1254);
            rule__ProductDeclaration__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getProductDeclarationAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__1__Impl"


    // $ANTLR start "rule__ProductDeclaration__Group__2"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:626:1: rule__ProductDeclaration__Group__2 : rule__ProductDeclaration__Group__2__Impl rule__ProductDeclaration__Group__3 ;
    public final void rule__ProductDeclaration__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:630:1: ( rule__ProductDeclaration__Group__2__Impl rule__ProductDeclaration__Group__3 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:631:2: rule__ProductDeclaration__Group__2__Impl rule__ProductDeclaration__Group__3
            {
            pushFollow(FOLLOW_rule__ProductDeclaration__Group__2__Impl_in_rule__ProductDeclaration__Group__21284);
            rule__ProductDeclaration__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ProductDeclaration__Group__3_in_rule__ProductDeclaration__Group__21287);
            rule__ProductDeclaration__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__2"


    // $ANTLR start "rule__ProductDeclaration__Group__2__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:638:1: rule__ProductDeclaration__Group__2__Impl : ( '{' ) ;
    public final void rule__ProductDeclaration__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:642:1: ( ( '{' ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:643:1: ( '{' )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:643:1: ( '{' )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:644:1: '{'
            {
             before(grammarAccess.getProductDeclarationAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,12,FOLLOW_12_in_rule__ProductDeclaration__Group__2__Impl1315); 
             after(grammarAccess.getProductDeclarationAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__2__Impl"


    // $ANTLR start "rule__ProductDeclaration__Group__3"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:657:1: rule__ProductDeclaration__Group__3 : rule__ProductDeclaration__Group__3__Impl rule__ProductDeclaration__Group__4 ;
    public final void rule__ProductDeclaration__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:661:1: ( rule__ProductDeclaration__Group__3__Impl rule__ProductDeclaration__Group__4 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:662:2: rule__ProductDeclaration__Group__3__Impl rule__ProductDeclaration__Group__4
            {
            pushFollow(FOLLOW_rule__ProductDeclaration__Group__3__Impl_in_rule__ProductDeclaration__Group__31346);
            rule__ProductDeclaration__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ProductDeclaration__Group__4_in_rule__ProductDeclaration__Group__31349);
            rule__ProductDeclaration__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__3"


    // $ANTLR start "rule__ProductDeclaration__Group__3__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:669:1: rule__ProductDeclaration__Group__3__Impl : ( 'Features' ) ;
    public final void rule__ProductDeclaration__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:673:1: ( ( 'Features' ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:674:1: ( 'Features' )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:674:1: ( 'Features' )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:675:1: 'Features'
            {
             before(grammarAccess.getProductDeclarationAccess().getFeaturesKeyword_3()); 
            match(input,13,FOLLOW_13_in_rule__ProductDeclaration__Group__3__Impl1377); 
             after(grammarAccess.getProductDeclarationAccess().getFeaturesKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__3__Impl"


    // $ANTLR start "rule__ProductDeclaration__Group__4"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:688:1: rule__ProductDeclaration__Group__4 : rule__ProductDeclaration__Group__4__Impl rule__ProductDeclaration__Group__5 ;
    public final void rule__ProductDeclaration__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:692:1: ( rule__ProductDeclaration__Group__4__Impl rule__ProductDeclaration__Group__5 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:693:2: rule__ProductDeclaration__Group__4__Impl rule__ProductDeclaration__Group__5
            {
            pushFollow(FOLLOW_rule__ProductDeclaration__Group__4__Impl_in_rule__ProductDeclaration__Group__41408);
            rule__ProductDeclaration__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ProductDeclaration__Group__5_in_rule__ProductDeclaration__Group__41411);
            rule__ProductDeclaration__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__4"


    // $ANTLR start "rule__ProductDeclaration__Group__4__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:700:1: rule__ProductDeclaration__Group__4__Impl : ( '=' ) ;
    public final void rule__ProductDeclaration__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:704:1: ( ( '=' ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:705:1: ( '=' )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:705:1: ( '=' )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:706:1: '='
            {
             before(grammarAccess.getProductDeclarationAccess().getEqualsSignKeyword_4()); 
            match(input,14,FOLLOW_14_in_rule__ProductDeclaration__Group__4__Impl1439); 
             after(grammarAccess.getProductDeclarationAccess().getEqualsSignKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__4__Impl"


    // $ANTLR start "rule__ProductDeclaration__Group__5"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:719:1: rule__ProductDeclaration__Group__5 : rule__ProductDeclaration__Group__5__Impl rule__ProductDeclaration__Group__6 ;
    public final void rule__ProductDeclaration__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:723:1: ( rule__ProductDeclaration__Group__5__Impl rule__ProductDeclaration__Group__6 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:724:2: rule__ProductDeclaration__Group__5__Impl rule__ProductDeclaration__Group__6
            {
            pushFollow(FOLLOW_rule__ProductDeclaration__Group__5__Impl_in_rule__ProductDeclaration__Group__51470);
            rule__ProductDeclaration__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ProductDeclaration__Group__6_in_rule__ProductDeclaration__Group__51473);
            rule__ProductDeclaration__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__5"


    // $ANTLR start "rule__ProductDeclaration__Group__5__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:731:1: rule__ProductDeclaration__Group__5__Impl : ( '{' ) ;
    public final void rule__ProductDeclaration__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:735:1: ( ( '{' ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:736:1: ( '{' )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:736:1: ( '{' )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:737:1: '{'
            {
             before(grammarAccess.getProductDeclarationAccess().getLeftCurlyBracketKeyword_5()); 
            match(input,12,FOLLOW_12_in_rule__ProductDeclaration__Group__5__Impl1501); 
             after(grammarAccess.getProductDeclarationAccess().getLeftCurlyBracketKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__5__Impl"


    // $ANTLR start "rule__ProductDeclaration__Group__6"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:750:1: rule__ProductDeclaration__Group__6 : rule__ProductDeclaration__Group__6__Impl rule__ProductDeclaration__Group__7 ;
    public final void rule__ProductDeclaration__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:754:1: ( rule__ProductDeclaration__Group__6__Impl rule__ProductDeclaration__Group__7 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:755:2: rule__ProductDeclaration__Group__6__Impl rule__ProductDeclaration__Group__7
            {
            pushFollow(FOLLOW_rule__ProductDeclaration__Group__6__Impl_in_rule__ProductDeclaration__Group__61532);
            rule__ProductDeclaration__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ProductDeclaration__Group__7_in_rule__ProductDeclaration__Group__61535);
            rule__ProductDeclaration__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__6"


    // $ANTLR start "rule__ProductDeclaration__Group__6__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:762:1: rule__ProductDeclaration__Group__6__Impl : ( ( rule__ProductDeclaration__FeaturesAssignment_6 )? ) ;
    public final void rule__ProductDeclaration__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:766:1: ( ( ( rule__ProductDeclaration__FeaturesAssignment_6 )? ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:767:1: ( ( rule__ProductDeclaration__FeaturesAssignment_6 )? )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:767:1: ( ( rule__ProductDeclaration__FeaturesAssignment_6 )? )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:768:1: ( rule__ProductDeclaration__FeaturesAssignment_6 )?
            {
             before(grammarAccess.getProductDeclarationAccess().getFeaturesAssignment_6()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:769:1: ( rule__ProductDeclaration__FeaturesAssignment_6 )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==RULE_ID) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:769:2: rule__ProductDeclaration__FeaturesAssignment_6
                    {
                    pushFollow(FOLLOW_rule__ProductDeclaration__FeaturesAssignment_6_in_rule__ProductDeclaration__Group__6__Impl1562);
                    rule__ProductDeclaration__FeaturesAssignment_6();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getProductDeclarationAccess().getFeaturesAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__6__Impl"


    // $ANTLR start "rule__ProductDeclaration__Group__7"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:779:1: rule__ProductDeclaration__Group__7 : rule__ProductDeclaration__Group__7__Impl rule__ProductDeclaration__Group__8 ;
    public final void rule__ProductDeclaration__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:783:1: ( rule__ProductDeclaration__Group__7__Impl rule__ProductDeclaration__Group__8 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:784:2: rule__ProductDeclaration__Group__7__Impl rule__ProductDeclaration__Group__8
            {
            pushFollow(FOLLOW_rule__ProductDeclaration__Group__7__Impl_in_rule__ProductDeclaration__Group__71593);
            rule__ProductDeclaration__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ProductDeclaration__Group__8_in_rule__ProductDeclaration__Group__71596);
            rule__ProductDeclaration__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__7"


    // $ANTLR start "rule__ProductDeclaration__Group__7__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:791:1: rule__ProductDeclaration__Group__7__Impl : ( '}' ) ;
    public final void rule__ProductDeclaration__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:795:1: ( ( '}' ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:796:1: ( '}' )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:796:1: ( '}' )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:797:1: '}'
            {
             before(grammarAccess.getProductDeclarationAccess().getRightCurlyBracketKeyword_7()); 
            match(input,15,FOLLOW_15_in_rule__ProductDeclaration__Group__7__Impl1624); 
             after(grammarAccess.getProductDeclarationAccess().getRightCurlyBracketKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__7__Impl"


    // $ANTLR start "rule__ProductDeclaration__Group__8"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:810:1: rule__ProductDeclaration__Group__8 : rule__ProductDeclaration__Group__8__Impl rule__ProductDeclaration__Group__9 ;
    public final void rule__ProductDeclaration__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:814:1: ( rule__ProductDeclaration__Group__8__Impl rule__ProductDeclaration__Group__9 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:815:2: rule__ProductDeclaration__Group__8__Impl rule__ProductDeclaration__Group__9
            {
            pushFollow(FOLLOW_rule__ProductDeclaration__Group__8__Impl_in_rule__ProductDeclaration__Group__81655);
            rule__ProductDeclaration__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ProductDeclaration__Group__9_in_rule__ProductDeclaration__Group__81658);
            rule__ProductDeclaration__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__8"


    // $ANTLR start "rule__ProductDeclaration__Group__8__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:822:1: rule__ProductDeclaration__Group__8__Impl : ( 'Deltas' ) ;
    public final void rule__ProductDeclaration__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:826:1: ( ( 'Deltas' ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:827:1: ( 'Deltas' )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:827:1: ( 'Deltas' )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:828:1: 'Deltas'
            {
             before(grammarAccess.getProductDeclarationAccess().getDeltasKeyword_8()); 
            match(input,16,FOLLOW_16_in_rule__ProductDeclaration__Group__8__Impl1686); 
             after(grammarAccess.getProductDeclarationAccess().getDeltasKeyword_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__8__Impl"


    // $ANTLR start "rule__ProductDeclaration__Group__9"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:841:1: rule__ProductDeclaration__Group__9 : rule__ProductDeclaration__Group__9__Impl rule__ProductDeclaration__Group__10 ;
    public final void rule__ProductDeclaration__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:845:1: ( rule__ProductDeclaration__Group__9__Impl rule__ProductDeclaration__Group__10 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:846:2: rule__ProductDeclaration__Group__9__Impl rule__ProductDeclaration__Group__10
            {
            pushFollow(FOLLOW_rule__ProductDeclaration__Group__9__Impl_in_rule__ProductDeclaration__Group__91717);
            rule__ProductDeclaration__Group__9__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ProductDeclaration__Group__10_in_rule__ProductDeclaration__Group__91720);
            rule__ProductDeclaration__Group__10();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__9"


    // $ANTLR start "rule__ProductDeclaration__Group__9__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:853:1: rule__ProductDeclaration__Group__9__Impl : ( '=' ) ;
    public final void rule__ProductDeclaration__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:857:1: ( ( '=' ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:858:1: ( '=' )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:858:1: ( '=' )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:859:1: '='
            {
             before(grammarAccess.getProductDeclarationAccess().getEqualsSignKeyword_9()); 
            match(input,14,FOLLOW_14_in_rule__ProductDeclaration__Group__9__Impl1748); 
             after(grammarAccess.getProductDeclarationAccess().getEqualsSignKeyword_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__9__Impl"


    // $ANTLR start "rule__ProductDeclaration__Group__10"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:872:1: rule__ProductDeclaration__Group__10 : rule__ProductDeclaration__Group__10__Impl rule__ProductDeclaration__Group__11 ;
    public final void rule__ProductDeclaration__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:876:1: ( rule__ProductDeclaration__Group__10__Impl rule__ProductDeclaration__Group__11 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:877:2: rule__ProductDeclaration__Group__10__Impl rule__ProductDeclaration__Group__11
            {
            pushFollow(FOLLOW_rule__ProductDeclaration__Group__10__Impl_in_rule__ProductDeclaration__Group__101779);
            rule__ProductDeclaration__Group__10__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ProductDeclaration__Group__11_in_rule__ProductDeclaration__Group__101782);
            rule__ProductDeclaration__Group__11();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__10"


    // $ANTLR start "rule__ProductDeclaration__Group__10__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:884:1: rule__ProductDeclaration__Group__10__Impl : ( '{' ) ;
    public final void rule__ProductDeclaration__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:888:1: ( ( '{' ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:889:1: ( '{' )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:889:1: ( '{' )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:890:1: '{'
            {
             before(grammarAccess.getProductDeclarationAccess().getLeftCurlyBracketKeyword_10()); 
            match(input,12,FOLLOW_12_in_rule__ProductDeclaration__Group__10__Impl1810); 
             after(grammarAccess.getProductDeclarationAccess().getLeftCurlyBracketKeyword_10()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__10__Impl"


    // $ANTLR start "rule__ProductDeclaration__Group__11"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:903:1: rule__ProductDeclaration__Group__11 : rule__ProductDeclaration__Group__11__Impl rule__ProductDeclaration__Group__12 ;
    public final void rule__ProductDeclaration__Group__11() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:907:1: ( rule__ProductDeclaration__Group__11__Impl rule__ProductDeclaration__Group__12 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:908:2: rule__ProductDeclaration__Group__11__Impl rule__ProductDeclaration__Group__12
            {
            pushFollow(FOLLOW_rule__ProductDeclaration__Group__11__Impl_in_rule__ProductDeclaration__Group__111841);
            rule__ProductDeclaration__Group__11__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ProductDeclaration__Group__12_in_rule__ProductDeclaration__Group__111844);
            rule__ProductDeclaration__Group__12();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__11"


    // $ANTLR start "rule__ProductDeclaration__Group__11__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:915:1: rule__ProductDeclaration__Group__11__Impl : ( ( rule__ProductDeclaration__DeltasAssignment_11 )? ) ;
    public final void rule__ProductDeclaration__Group__11__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:919:1: ( ( ( rule__ProductDeclaration__DeltasAssignment_11 )? ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:920:1: ( ( rule__ProductDeclaration__DeltasAssignment_11 )? )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:920:1: ( ( rule__ProductDeclaration__DeltasAssignment_11 )? )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:921:1: ( rule__ProductDeclaration__DeltasAssignment_11 )?
            {
             before(grammarAccess.getProductDeclarationAccess().getDeltasAssignment_11()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:922:1: ( rule__ProductDeclaration__DeltasAssignment_11 )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==RULE_ID) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:922:2: rule__ProductDeclaration__DeltasAssignment_11
                    {
                    pushFollow(FOLLOW_rule__ProductDeclaration__DeltasAssignment_11_in_rule__ProductDeclaration__Group__11__Impl1871);
                    rule__ProductDeclaration__DeltasAssignment_11();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getProductDeclarationAccess().getDeltasAssignment_11()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__11__Impl"


    // $ANTLR start "rule__ProductDeclaration__Group__12"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:932:1: rule__ProductDeclaration__Group__12 : rule__ProductDeclaration__Group__12__Impl rule__ProductDeclaration__Group__13 ;
    public final void rule__ProductDeclaration__Group__12() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:936:1: ( rule__ProductDeclaration__Group__12__Impl rule__ProductDeclaration__Group__13 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:937:2: rule__ProductDeclaration__Group__12__Impl rule__ProductDeclaration__Group__13
            {
            pushFollow(FOLLOW_rule__ProductDeclaration__Group__12__Impl_in_rule__ProductDeclaration__Group__121902);
            rule__ProductDeclaration__Group__12__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ProductDeclaration__Group__13_in_rule__ProductDeclaration__Group__121905);
            rule__ProductDeclaration__Group__13();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__12"


    // $ANTLR start "rule__ProductDeclaration__Group__12__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:944:1: rule__ProductDeclaration__Group__12__Impl : ( '}' ) ;
    public final void rule__ProductDeclaration__Group__12__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:948:1: ( ( '}' ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:949:1: ( '}' )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:949:1: ( '}' )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:950:1: '}'
            {
             before(grammarAccess.getProductDeclarationAccess().getRightCurlyBracketKeyword_12()); 
            match(input,15,FOLLOW_15_in_rule__ProductDeclaration__Group__12__Impl1933); 
             after(grammarAccess.getProductDeclarationAccess().getRightCurlyBracketKeyword_12()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__12__Impl"


    // $ANTLR start "rule__ProductDeclaration__Group__13"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:963:1: rule__ProductDeclaration__Group__13 : rule__ProductDeclaration__Group__13__Impl rule__ProductDeclaration__Group__14 ;
    public final void rule__ProductDeclaration__Group__13() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:967:1: ( rule__ProductDeclaration__Group__13__Impl rule__ProductDeclaration__Group__14 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:968:2: rule__ProductDeclaration__Group__13__Impl rule__ProductDeclaration__Group__14
            {
            pushFollow(FOLLOW_rule__ProductDeclaration__Group__13__Impl_in_rule__ProductDeclaration__Group__131964);
            rule__ProductDeclaration__Group__13__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ProductDeclaration__Group__14_in_rule__ProductDeclaration__Group__131967);
            rule__ProductDeclaration__Group__14();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__13"


    // $ANTLR start "rule__ProductDeclaration__Group__13__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:975:1: rule__ProductDeclaration__Group__13__Impl : ( 'Constraints' ) ;
    public final void rule__ProductDeclaration__Group__13__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:979:1: ( ( 'Constraints' ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:980:1: ( 'Constraints' )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:980:1: ( 'Constraints' )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:981:1: 'Constraints'
            {
             before(grammarAccess.getProductDeclarationAccess().getConstraintsKeyword_13()); 
            match(input,17,FOLLOW_17_in_rule__ProductDeclaration__Group__13__Impl1995); 
             after(grammarAccess.getProductDeclarationAccess().getConstraintsKeyword_13()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__13__Impl"


    // $ANTLR start "rule__ProductDeclaration__Group__14"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:994:1: rule__ProductDeclaration__Group__14 : rule__ProductDeclaration__Group__14__Impl rule__ProductDeclaration__Group__15 ;
    public final void rule__ProductDeclaration__Group__14() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:998:1: ( rule__ProductDeclaration__Group__14__Impl rule__ProductDeclaration__Group__15 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:999:2: rule__ProductDeclaration__Group__14__Impl rule__ProductDeclaration__Group__15
            {
            pushFollow(FOLLOW_rule__ProductDeclaration__Group__14__Impl_in_rule__ProductDeclaration__Group__142026);
            rule__ProductDeclaration__Group__14__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ProductDeclaration__Group__15_in_rule__ProductDeclaration__Group__142029);
            rule__ProductDeclaration__Group__15();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__14"


    // $ANTLR start "rule__ProductDeclaration__Group__14__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1006:1: rule__ProductDeclaration__Group__14__Impl : ( '{' ) ;
    public final void rule__ProductDeclaration__Group__14__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1010:1: ( ( '{' ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1011:1: ( '{' )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1011:1: ( '{' )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1012:1: '{'
            {
             before(grammarAccess.getProductDeclarationAccess().getLeftCurlyBracketKeyword_14()); 
            match(input,12,FOLLOW_12_in_rule__ProductDeclaration__Group__14__Impl2057); 
             after(grammarAccess.getProductDeclarationAccess().getLeftCurlyBracketKeyword_14()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__14__Impl"


    // $ANTLR start "rule__ProductDeclaration__Group__15"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1025:1: rule__ProductDeclaration__Group__15 : rule__ProductDeclaration__Group__15__Impl rule__ProductDeclaration__Group__16 ;
    public final void rule__ProductDeclaration__Group__15() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1029:1: ( rule__ProductDeclaration__Group__15__Impl rule__ProductDeclaration__Group__16 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1030:2: rule__ProductDeclaration__Group__15__Impl rule__ProductDeclaration__Group__16
            {
            pushFollow(FOLLOW_rule__ProductDeclaration__Group__15__Impl_in_rule__ProductDeclaration__Group__152088);
            rule__ProductDeclaration__Group__15__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ProductDeclaration__Group__16_in_rule__ProductDeclaration__Group__152091);
            rule__ProductDeclaration__Group__16();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__15"


    // $ANTLR start "rule__ProductDeclaration__Group__15__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1037:1: rule__ProductDeclaration__Group__15__Impl : ( ( rule__ProductDeclaration__ConstraintsAssignment_15 )? ) ;
    public final void rule__ProductDeclaration__Group__15__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1041:1: ( ( ( rule__ProductDeclaration__ConstraintsAssignment_15 )? ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1042:1: ( ( rule__ProductDeclaration__ConstraintsAssignment_15 )? )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1042:1: ( ( rule__ProductDeclaration__ConstraintsAssignment_15 )? )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1043:1: ( rule__ProductDeclaration__ConstraintsAssignment_15 )?
            {
             before(grammarAccess.getProductDeclarationAccess().getConstraintsAssignment_15()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1044:1: ( rule__ProductDeclaration__ConstraintsAssignment_15 )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==RULE_ID||LA8_0==22||LA8_0==26) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1044:2: rule__ProductDeclaration__ConstraintsAssignment_15
                    {
                    pushFollow(FOLLOW_rule__ProductDeclaration__ConstraintsAssignment_15_in_rule__ProductDeclaration__Group__15__Impl2118);
                    rule__ProductDeclaration__ConstraintsAssignment_15();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getProductDeclarationAccess().getConstraintsAssignment_15()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__15__Impl"


    // $ANTLR start "rule__ProductDeclaration__Group__16"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1054:1: rule__ProductDeclaration__Group__16 : rule__ProductDeclaration__Group__16__Impl rule__ProductDeclaration__Group__17 ;
    public final void rule__ProductDeclaration__Group__16() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1058:1: ( rule__ProductDeclaration__Group__16__Impl rule__ProductDeclaration__Group__17 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1059:2: rule__ProductDeclaration__Group__16__Impl rule__ProductDeclaration__Group__17
            {
            pushFollow(FOLLOW_rule__ProductDeclaration__Group__16__Impl_in_rule__ProductDeclaration__Group__162149);
            rule__ProductDeclaration__Group__16__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ProductDeclaration__Group__17_in_rule__ProductDeclaration__Group__162152);
            rule__ProductDeclaration__Group__17();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__16"


    // $ANTLR start "rule__ProductDeclaration__Group__16__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1066:1: rule__ProductDeclaration__Group__16__Impl : ( '}' ) ;
    public final void rule__ProductDeclaration__Group__16__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1070:1: ( ( '}' ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1071:1: ( '}' )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1071:1: ( '}' )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1072:1: '}'
            {
             before(grammarAccess.getProductDeclarationAccess().getRightCurlyBracketKeyword_16()); 
            match(input,15,FOLLOW_15_in_rule__ProductDeclaration__Group__16__Impl2180); 
             after(grammarAccess.getProductDeclarationAccess().getRightCurlyBracketKeyword_16()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__16__Impl"


    // $ANTLR start "rule__ProductDeclaration__Group__17"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1085:1: rule__ProductDeclaration__Group__17 : rule__ProductDeclaration__Group__17__Impl rule__ProductDeclaration__Group__18 ;
    public final void rule__ProductDeclaration__Group__17() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1089:1: ( rule__ProductDeclaration__Group__17__Impl rule__ProductDeclaration__Group__18 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1090:2: rule__ProductDeclaration__Group__17__Impl rule__ProductDeclaration__Group__18
            {
            pushFollow(FOLLOW_rule__ProductDeclaration__Group__17__Impl_in_rule__ProductDeclaration__Group__172211);
            rule__ProductDeclaration__Group__17__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ProductDeclaration__Group__18_in_rule__ProductDeclaration__Group__172214);
            rule__ProductDeclaration__Group__18();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__17"


    // $ANTLR start "rule__ProductDeclaration__Group__17__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1097:1: rule__ProductDeclaration__Group__17__Impl : ( 'Partitions' ) ;
    public final void rule__ProductDeclaration__Group__17__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1101:1: ( ( 'Partitions' ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1102:1: ( 'Partitions' )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1102:1: ( 'Partitions' )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1103:1: 'Partitions'
            {
             before(grammarAccess.getProductDeclarationAccess().getPartitionsKeyword_17()); 
            match(input,18,FOLLOW_18_in_rule__ProductDeclaration__Group__17__Impl2242); 
             after(grammarAccess.getProductDeclarationAccess().getPartitionsKeyword_17()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__17__Impl"


    // $ANTLR start "rule__ProductDeclaration__Group__18"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1116:1: rule__ProductDeclaration__Group__18 : rule__ProductDeclaration__Group__18__Impl rule__ProductDeclaration__Group__19 ;
    public final void rule__ProductDeclaration__Group__18() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1120:1: ( rule__ProductDeclaration__Group__18__Impl rule__ProductDeclaration__Group__19 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1121:2: rule__ProductDeclaration__Group__18__Impl rule__ProductDeclaration__Group__19
            {
            pushFollow(FOLLOW_rule__ProductDeclaration__Group__18__Impl_in_rule__ProductDeclaration__Group__182273);
            rule__ProductDeclaration__Group__18__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ProductDeclaration__Group__19_in_rule__ProductDeclaration__Group__182276);
            rule__ProductDeclaration__Group__19();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__18"


    // $ANTLR start "rule__ProductDeclaration__Group__18__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1128:1: rule__ProductDeclaration__Group__18__Impl : ( '{' ) ;
    public final void rule__ProductDeclaration__Group__18__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1132:1: ( ( '{' ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1133:1: ( '{' )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1133:1: ( '{' )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1134:1: '{'
            {
             before(grammarAccess.getProductDeclarationAccess().getLeftCurlyBracketKeyword_18()); 
            match(input,12,FOLLOW_12_in_rule__ProductDeclaration__Group__18__Impl2304); 
             after(grammarAccess.getProductDeclarationAccess().getLeftCurlyBracketKeyword_18()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__18__Impl"


    // $ANTLR start "rule__ProductDeclaration__Group__19"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1147:1: rule__ProductDeclaration__Group__19 : rule__ProductDeclaration__Group__19__Impl rule__ProductDeclaration__Group__20 ;
    public final void rule__ProductDeclaration__Group__19() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1151:1: ( rule__ProductDeclaration__Group__19__Impl rule__ProductDeclaration__Group__20 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1152:2: rule__ProductDeclaration__Group__19__Impl rule__ProductDeclaration__Group__20
            {
            pushFollow(FOLLOW_rule__ProductDeclaration__Group__19__Impl_in_rule__ProductDeclaration__Group__192335);
            rule__ProductDeclaration__Group__19__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ProductDeclaration__Group__20_in_rule__ProductDeclaration__Group__192338);
            rule__ProductDeclaration__Group__20();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__19"


    // $ANTLR start "rule__ProductDeclaration__Group__19__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1159:1: rule__ProductDeclaration__Group__19__Impl : ( ( rule__ProductDeclaration__PartitionsAssignment_19 )? ) ;
    public final void rule__ProductDeclaration__Group__19__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1163:1: ( ( ( rule__ProductDeclaration__PartitionsAssignment_19 )? ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1164:1: ( ( rule__ProductDeclaration__PartitionsAssignment_19 )? )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1164:1: ( ( rule__ProductDeclaration__PartitionsAssignment_19 )? )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1165:1: ( rule__ProductDeclaration__PartitionsAssignment_19 )?
            {
             before(grammarAccess.getProductDeclarationAccess().getPartitionsAssignment_19()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1166:1: ( rule__ProductDeclaration__PartitionsAssignment_19 )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==12) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1166:2: rule__ProductDeclaration__PartitionsAssignment_19
                    {
                    pushFollow(FOLLOW_rule__ProductDeclaration__PartitionsAssignment_19_in_rule__ProductDeclaration__Group__19__Impl2365);
                    rule__ProductDeclaration__PartitionsAssignment_19();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getProductDeclarationAccess().getPartitionsAssignment_19()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__19__Impl"


    // $ANTLR start "rule__ProductDeclaration__Group__20"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1176:1: rule__ProductDeclaration__Group__20 : rule__ProductDeclaration__Group__20__Impl rule__ProductDeclaration__Group__21 ;
    public final void rule__ProductDeclaration__Group__20() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1180:1: ( rule__ProductDeclaration__Group__20__Impl rule__ProductDeclaration__Group__21 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1181:2: rule__ProductDeclaration__Group__20__Impl rule__ProductDeclaration__Group__21
            {
            pushFollow(FOLLOW_rule__ProductDeclaration__Group__20__Impl_in_rule__ProductDeclaration__Group__202396);
            rule__ProductDeclaration__Group__20__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ProductDeclaration__Group__21_in_rule__ProductDeclaration__Group__202399);
            rule__ProductDeclaration__Group__21();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__20"


    // $ANTLR start "rule__ProductDeclaration__Group__20__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1188:1: rule__ProductDeclaration__Group__20__Impl : ( '}' ) ;
    public final void rule__ProductDeclaration__Group__20__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1192:1: ( ( '}' ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1193:1: ( '}' )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1193:1: ( '}' )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1194:1: '}'
            {
             before(grammarAccess.getProductDeclarationAccess().getRightCurlyBracketKeyword_20()); 
            match(input,15,FOLLOW_15_in_rule__ProductDeclaration__Group__20__Impl2427); 
             after(grammarAccess.getProductDeclarationAccess().getRightCurlyBracketKeyword_20()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__20__Impl"


    // $ANTLR start "rule__ProductDeclaration__Group__21"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1207:1: rule__ProductDeclaration__Group__21 : rule__ProductDeclaration__Group__21__Impl rule__ProductDeclaration__Group__22 ;
    public final void rule__ProductDeclaration__Group__21() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1211:1: ( rule__ProductDeclaration__Group__21__Impl rule__ProductDeclaration__Group__22 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1212:2: rule__ProductDeclaration__Group__21__Impl rule__ProductDeclaration__Group__22
            {
            pushFollow(FOLLOW_rule__ProductDeclaration__Group__21__Impl_in_rule__ProductDeclaration__Group__212458);
            rule__ProductDeclaration__Group__21__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ProductDeclaration__Group__22_in_rule__ProductDeclaration__Group__212461);
            rule__ProductDeclaration__Group__22();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__21"


    // $ANTLR start "rule__ProductDeclaration__Group__21__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1219:1: rule__ProductDeclaration__Group__21__Impl : ( 'Products' ) ;
    public final void rule__ProductDeclaration__Group__21__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1223:1: ( ( 'Products' ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1224:1: ( 'Products' )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1224:1: ( 'Products' )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1225:1: 'Products'
            {
             before(grammarAccess.getProductDeclarationAccess().getProductsKeyword_21()); 
            match(input,19,FOLLOW_19_in_rule__ProductDeclaration__Group__21__Impl2489); 
             after(grammarAccess.getProductDeclarationAccess().getProductsKeyword_21()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__21__Impl"


    // $ANTLR start "rule__ProductDeclaration__Group__22"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1238:1: rule__ProductDeclaration__Group__22 : rule__ProductDeclaration__Group__22__Impl rule__ProductDeclaration__Group__23 ;
    public final void rule__ProductDeclaration__Group__22() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1242:1: ( rule__ProductDeclaration__Group__22__Impl rule__ProductDeclaration__Group__23 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1243:2: rule__ProductDeclaration__Group__22__Impl rule__ProductDeclaration__Group__23
            {
            pushFollow(FOLLOW_rule__ProductDeclaration__Group__22__Impl_in_rule__ProductDeclaration__Group__222520);
            rule__ProductDeclaration__Group__22__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ProductDeclaration__Group__23_in_rule__ProductDeclaration__Group__222523);
            rule__ProductDeclaration__Group__23();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__22"


    // $ANTLR start "rule__ProductDeclaration__Group__22__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1250:1: rule__ProductDeclaration__Group__22__Impl : ( '{' ) ;
    public final void rule__ProductDeclaration__Group__22__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1254:1: ( ( '{' ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1255:1: ( '{' )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1255:1: ( '{' )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1256:1: '{'
            {
             before(grammarAccess.getProductDeclarationAccess().getLeftCurlyBracketKeyword_22()); 
            match(input,12,FOLLOW_12_in_rule__ProductDeclaration__Group__22__Impl2551); 
             after(grammarAccess.getProductDeclarationAccess().getLeftCurlyBracketKeyword_22()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__22__Impl"


    // $ANTLR start "rule__ProductDeclaration__Group__23"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1269:1: rule__ProductDeclaration__Group__23 : rule__ProductDeclaration__Group__23__Impl rule__ProductDeclaration__Group__24 ;
    public final void rule__ProductDeclaration__Group__23() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1273:1: ( rule__ProductDeclaration__Group__23__Impl rule__ProductDeclaration__Group__24 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1274:2: rule__ProductDeclaration__Group__23__Impl rule__ProductDeclaration__Group__24
            {
            pushFollow(FOLLOW_rule__ProductDeclaration__Group__23__Impl_in_rule__ProductDeclaration__Group__232582);
            rule__ProductDeclaration__Group__23__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ProductDeclaration__Group__24_in_rule__ProductDeclaration__Group__232585);
            rule__ProductDeclaration__Group__24();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__23"


    // $ANTLR start "rule__ProductDeclaration__Group__23__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1281:1: rule__ProductDeclaration__Group__23__Impl : ( ( rule__ProductDeclaration__ProductsAssignment_23 )? ) ;
    public final void rule__ProductDeclaration__Group__23__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1285:1: ( ( ( rule__ProductDeclaration__ProductsAssignment_23 )? ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1286:1: ( ( rule__ProductDeclaration__ProductsAssignment_23 )? )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1286:1: ( ( rule__ProductDeclaration__ProductsAssignment_23 )? )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1287:1: ( rule__ProductDeclaration__ProductsAssignment_23 )?
            {
             before(grammarAccess.getProductDeclarationAccess().getProductsAssignment_23()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1288:1: ( rule__ProductDeclaration__ProductsAssignment_23 )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==RULE_ID) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1288:2: rule__ProductDeclaration__ProductsAssignment_23
                    {
                    pushFollow(FOLLOW_rule__ProductDeclaration__ProductsAssignment_23_in_rule__ProductDeclaration__Group__23__Impl2612);
                    rule__ProductDeclaration__ProductsAssignment_23();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getProductDeclarationAccess().getProductsAssignment_23()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__23__Impl"


    // $ANTLR start "rule__ProductDeclaration__Group__24"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1298:1: rule__ProductDeclaration__Group__24 : rule__ProductDeclaration__Group__24__Impl rule__ProductDeclaration__Group__25 ;
    public final void rule__ProductDeclaration__Group__24() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1302:1: ( rule__ProductDeclaration__Group__24__Impl rule__ProductDeclaration__Group__25 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1303:2: rule__ProductDeclaration__Group__24__Impl rule__ProductDeclaration__Group__25
            {
            pushFollow(FOLLOW_rule__ProductDeclaration__Group__24__Impl_in_rule__ProductDeclaration__Group__242643);
            rule__ProductDeclaration__Group__24__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ProductDeclaration__Group__25_in_rule__ProductDeclaration__Group__242646);
            rule__ProductDeclaration__Group__25();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__24"


    // $ANTLR start "rule__ProductDeclaration__Group__24__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1310:1: rule__ProductDeclaration__Group__24__Impl : ( '}' ) ;
    public final void rule__ProductDeclaration__Group__24__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1314:1: ( ( '}' ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1315:1: ( '}' )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1315:1: ( '}' )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1316:1: '}'
            {
             before(grammarAccess.getProductDeclarationAccess().getRightCurlyBracketKeyword_24()); 
            match(input,15,FOLLOW_15_in_rule__ProductDeclaration__Group__24__Impl2674); 
             after(grammarAccess.getProductDeclarationAccess().getRightCurlyBracketKeyword_24()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__24__Impl"


    // $ANTLR start "rule__ProductDeclaration__Group__25"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1329:1: rule__ProductDeclaration__Group__25 : rule__ProductDeclaration__Group__25__Impl ;
    public final void rule__ProductDeclaration__Group__25() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1333:1: ( rule__ProductDeclaration__Group__25__Impl )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1334:2: rule__ProductDeclaration__Group__25__Impl
            {
            pushFollow(FOLLOW_rule__ProductDeclaration__Group__25__Impl_in_rule__ProductDeclaration__Group__252705);
            rule__ProductDeclaration__Group__25__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__25"


    // $ANTLR start "rule__ProductDeclaration__Group__25__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1340:1: rule__ProductDeclaration__Group__25__Impl : ( '}' ) ;
    public final void rule__ProductDeclaration__Group__25__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1344:1: ( ( '}' ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1345:1: ( '}' )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1345:1: ( '}' )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1346:1: '}'
            {
             before(grammarAccess.getProductDeclarationAccess().getRightCurlyBracketKeyword_25()); 
            match(input,15,FOLLOW_15_in_rule__ProductDeclaration__Group__25__Impl2733); 
             after(grammarAccess.getProductDeclarationAccess().getRightCurlyBracketKeyword_25()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__Group__25__Impl"


    // $ANTLR start "rule__Product__Group__0"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1411:1: rule__Product__Group__0 : rule__Product__Group__0__Impl rule__Product__Group__1 ;
    public final void rule__Product__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1415:1: ( rule__Product__Group__0__Impl rule__Product__Group__1 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1416:2: rule__Product__Group__0__Impl rule__Product__Group__1
            {
            pushFollow(FOLLOW_rule__Product__Group__0__Impl_in_rule__Product__Group__02816);
            rule__Product__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Product__Group__1_in_rule__Product__Group__02819);
            rule__Product__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Product__Group__0"


    // $ANTLR start "rule__Product__Group__0__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1423:1: rule__Product__Group__0__Impl : ( ( rule__Product__NameAssignment_0 ) ) ;
    public final void rule__Product__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1427:1: ( ( ( rule__Product__NameAssignment_0 ) ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1428:1: ( ( rule__Product__NameAssignment_0 ) )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1428:1: ( ( rule__Product__NameAssignment_0 ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1429:1: ( rule__Product__NameAssignment_0 )
            {
             before(grammarAccess.getProductAccess().getNameAssignment_0()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1430:1: ( rule__Product__NameAssignment_0 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1430:2: rule__Product__NameAssignment_0
            {
            pushFollow(FOLLOW_rule__Product__NameAssignment_0_in_rule__Product__Group__0__Impl2846);
            rule__Product__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getProductAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Product__Group__0__Impl"


    // $ANTLR start "rule__Product__Group__1"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1440:1: rule__Product__Group__1 : rule__Product__Group__1__Impl rule__Product__Group__2 ;
    public final void rule__Product__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1444:1: ( rule__Product__Group__1__Impl rule__Product__Group__2 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1445:2: rule__Product__Group__1__Impl rule__Product__Group__2
            {
            pushFollow(FOLLOW_rule__Product__Group__1__Impl_in_rule__Product__Group__12876);
            rule__Product__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Product__Group__2_in_rule__Product__Group__12879);
            rule__Product__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Product__Group__1"


    // $ANTLR start "rule__Product__Group__1__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1452:1: rule__Product__Group__1__Impl : ( '=' ) ;
    public final void rule__Product__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1456:1: ( ( '=' ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1457:1: ( '=' )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1457:1: ( '=' )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1458:1: '='
            {
             before(grammarAccess.getProductAccess().getEqualsSignKeyword_1()); 
            match(input,14,FOLLOW_14_in_rule__Product__Group__1__Impl2907); 
             after(grammarAccess.getProductAccess().getEqualsSignKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Product__Group__1__Impl"


    // $ANTLR start "rule__Product__Group__2"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1471:1: rule__Product__Group__2 : rule__Product__Group__2__Impl rule__Product__Group__3 ;
    public final void rule__Product__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1475:1: ( rule__Product__Group__2__Impl rule__Product__Group__3 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1476:2: rule__Product__Group__2__Impl rule__Product__Group__3
            {
            pushFollow(FOLLOW_rule__Product__Group__2__Impl_in_rule__Product__Group__22938);
            rule__Product__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Product__Group__3_in_rule__Product__Group__22941);
            rule__Product__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Product__Group__2"


    // $ANTLR start "rule__Product__Group__2__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1483:1: rule__Product__Group__2__Impl : ( '{' ) ;
    public final void rule__Product__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1487:1: ( ( '{' ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1488:1: ( '{' )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1488:1: ( '{' )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1489:1: '{'
            {
             before(grammarAccess.getProductAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,12,FOLLOW_12_in_rule__Product__Group__2__Impl2969); 
             after(grammarAccess.getProductAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Product__Group__2__Impl"


    // $ANTLR start "rule__Product__Group__3"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1502:1: rule__Product__Group__3 : rule__Product__Group__3__Impl rule__Product__Group__4 ;
    public final void rule__Product__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1506:1: ( rule__Product__Group__3__Impl rule__Product__Group__4 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1507:2: rule__Product__Group__3__Impl rule__Product__Group__4
            {
            pushFollow(FOLLOW_rule__Product__Group__3__Impl_in_rule__Product__Group__33000);
            rule__Product__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Product__Group__4_in_rule__Product__Group__33003);
            rule__Product__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Product__Group__3"


    // $ANTLR start "rule__Product__Group__3__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1514:1: rule__Product__Group__3__Impl : ( ( rule__Product__ListAssignment_3 )* ) ;
    public final void rule__Product__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1518:1: ( ( ( rule__Product__ListAssignment_3 )* ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1519:1: ( ( rule__Product__ListAssignment_3 )* )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1519:1: ( ( rule__Product__ListAssignment_3 )* )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1520:1: ( rule__Product__ListAssignment_3 )*
            {
             before(grammarAccess.getProductAccess().getListAssignment_3()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1521:1: ( rule__Product__ListAssignment_3 )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==RULE_ID) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1521:2: rule__Product__ListAssignment_3
            	    {
            	    pushFollow(FOLLOW_rule__Product__ListAssignment_3_in_rule__Product__Group__3__Impl3030);
            	    rule__Product__ListAssignment_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

             after(grammarAccess.getProductAccess().getListAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Product__Group__3__Impl"


    // $ANTLR start "rule__Product__Group__4"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1531:1: rule__Product__Group__4 : rule__Product__Group__4__Impl rule__Product__Group__5 ;
    public final void rule__Product__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1535:1: ( rule__Product__Group__4__Impl rule__Product__Group__5 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1536:2: rule__Product__Group__4__Impl rule__Product__Group__5
            {
            pushFollow(FOLLOW_rule__Product__Group__4__Impl_in_rule__Product__Group__43061);
            rule__Product__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Product__Group__5_in_rule__Product__Group__43064);
            rule__Product__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Product__Group__4"


    // $ANTLR start "rule__Product__Group__4__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1543:1: rule__Product__Group__4__Impl : ( '}' ) ;
    public final void rule__Product__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1547:1: ( ( '}' ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1548:1: ( '}' )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1548:1: ( '}' )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1549:1: '}'
            {
             before(grammarAccess.getProductAccess().getRightCurlyBracketKeyword_4()); 
            match(input,15,FOLLOW_15_in_rule__Product__Group__4__Impl3092); 
             after(grammarAccess.getProductAccess().getRightCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Product__Group__4__Impl"


    // $ANTLR start "rule__Product__Group__5"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1562:1: rule__Product__Group__5 : rule__Product__Group__5__Impl ;
    public final void rule__Product__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1566:1: ( rule__Product__Group__5__Impl )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1567:2: rule__Product__Group__5__Impl
            {
            pushFollow(FOLLOW_rule__Product__Group__5__Impl_in_rule__Product__Group__53123);
            rule__Product__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Product__Group__5"


    // $ANTLR start "rule__Product__Group__5__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1573:1: rule__Product__Group__5__Impl : ( ';' ) ;
    public final void rule__Product__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1577:1: ( ( ';' ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1578:1: ( ';' )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1578:1: ( ';' )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1579:1: ';'
            {
             before(grammarAccess.getProductAccess().getSemicolonKeyword_5()); 
            match(input,20,FOLLOW_20_in_rule__Product__Group__5__Impl3151); 
             after(grammarAccess.getProductAccess().getSemicolonKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Product__Group__5__Impl"


    // $ANTLR start "rule__Partition__Group__0"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1604:1: rule__Partition__Group__0 : rule__Partition__Group__0__Impl rule__Partition__Group__1 ;
    public final void rule__Partition__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1608:1: ( rule__Partition__Group__0__Impl rule__Partition__Group__1 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1609:2: rule__Partition__Group__0__Impl rule__Partition__Group__1
            {
            pushFollow(FOLLOW_rule__Partition__Group__0__Impl_in_rule__Partition__Group__03194);
            rule__Partition__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Partition__Group__1_in_rule__Partition__Group__03197);
            rule__Partition__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Partition__Group__0"


    // $ANTLR start "rule__Partition__Group__0__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1616:1: rule__Partition__Group__0__Impl : ( ( ( rule__Partition__ConstrAssignment_0 ) ) ( ( rule__Partition__ConstrAssignment_0 )* ) ) ;
    public final void rule__Partition__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1620:1: ( ( ( ( rule__Partition__ConstrAssignment_0 ) ) ( ( rule__Partition__ConstrAssignment_0 )* ) ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1621:1: ( ( ( rule__Partition__ConstrAssignment_0 ) ) ( ( rule__Partition__ConstrAssignment_0 )* ) )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1621:1: ( ( ( rule__Partition__ConstrAssignment_0 ) ) ( ( rule__Partition__ConstrAssignment_0 )* ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1622:1: ( ( rule__Partition__ConstrAssignment_0 ) ) ( ( rule__Partition__ConstrAssignment_0 )* )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1622:1: ( ( rule__Partition__ConstrAssignment_0 ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1623:1: ( rule__Partition__ConstrAssignment_0 )
            {
             before(grammarAccess.getPartitionAccess().getConstrAssignment_0()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1624:1: ( rule__Partition__ConstrAssignment_0 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1624:2: rule__Partition__ConstrAssignment_0
            {
            pushFollow(FOLLOW_rule__Partition__ConstrAssignment_0_in_rule__Partition__Group__0__Impl3226);
            rule__Partition__ConstrAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getPartitionAccess().getConstrAssignment_0()); 

            }

            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1627:1: ( ( rule__Partition__ConstrAssignment_0 )* )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1628:1: ( rule__Partition__ConstrAssignment_0 )*
            {
             before(grammarAccess.getPartitionAccess().getConstrAssignment_0()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1629:1: ( rule__Partition__ConstrAssignment_0 )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==12) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1629:2: rule__Partition__ConstrAssignment_0
            	    {
            	    pushFollow(FOLLOW_rule__Partition__ConstrAssignment_0_in_rule__Partition__Group__0__Impl3238);
            	    rule__Partition__ConstrAssignment_0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);

             after(grammarAccess.getPartitionAccess().getConstrAssignment_0()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Partition__Group__0__Impl"


    // $ANTLR start "rule__Partition__Group__1"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1640:1: rule__Partition__Group__1 : rule__Partition__Group__1__Impl ;
    public final void rule__Partition__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1644:1: ( rule__Partition__Group__1__Impl )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1645:2: rule__Partition__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Partition__Group__1__Impl_in_rule__Partition__Group__13271);
            rule__Partition__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Partition__Group__1"


    // $ANTLR start "rule__Partition__Group__1__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1651:1: rule__Partition__Group__1__Impl : ( ';' ) ;
    public final void rule__Partition__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1655:1: ( ( ';' ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1656:1: ( ';' )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1656:1: ( ';' )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1657:1: ';'
            {
             before(grammarAccess.getPartitionAccess().getSemicolonKeyword_1()); 
            match(input,20,FOLLOW_20_in_rule__Partition__Group__1__Impl3299); 
             after(grammarAccess.getPartitionAccess().getSemicolonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Partition__Group__1__Impl"


    // $ANTLR start "rule__Constraint__Group__0"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1674:1: rule__Constraint__Group__0 : rule__Constraint__Group__0__Impl rule__Constraint__Group__1 ;
    public final void rule__Constraint__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1678:1: ( rule__Constraint__Group__0__Impl rule__Constraint__Group__1 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1679:2: rule__Constraint__Group__0__Impl rule__Constraint__Group__1
            {
            pushFollow(FOLLOW_rule__Constraint__Group__0__Impl_in_rule__Constraint__Group__03334);
            rule__Constraint__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Constraint__Group__1_in_rule__Constraint__Group__03337);
            rule__Constraint__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__0"


    // $ANTLR start "rule__Constraint__Group__0__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1686:1: rule__Constraint__Group__0__Impl : ( '{' ) ;
    public final void rule__Constraint__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1690:1: ( ( '{' ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1691:1: ( '{' )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1691:1: ( '{' )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1692:1: '{'
            {
             before(grammarAccess.getConstraintAccess().getLeftCurlyBracketKeyword_0()); 
            match(input,12,FOLLOW_12_in_rule__Constraint__Group__0__Impl3365); 
             after(grammarAccess.getConstraintAccess().getLeftCurlyBracketKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__0__Impl"


    // $ANTLR start "rule__Constraint__Group__1"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1705:1: rule__Constraint__Group__1 : rule__Constraint__Group__1__Impl rule__Constraint__Group__2 ;
    public final void rule__Constraint__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1709:1: ( rule__Constraint__Group__1__Impl rule__Constraint__Group__2 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1710:2: rule__Constraint__Group__1__Impl rule__Constraint__Group__2
            {
            pushFollow(FOLLOW_rule__Constraint__Group__1__Impl_in_rule__Constraint__Group__13396);
            rule__Constraint__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Constraint__Group__2_in_rule__Constraint__Group__13399);
            rule__Constraint__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__1"


    // $ANTLR start "rule__Constraint__Group__1__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1717:1: rule__Constraint__Group__1__Impl : ( ( rule__Constraint__DeltaAssignment_1 ) ) ;
    public final void rule__Constraint__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1721:1: ( ( ( rule__Constraint__DeltaAssignment_1 ) ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1722:1: ( ( rule__Constraint__DeltaAssignment_1 ) )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1722:1: ( ( rule__Constraint__DeltaAssignment_1 ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1723:1: ( rule__Constraint__DeltaAssignment_1 )
            {
             before(grammarAccess.getConstraintAccess().getDeltaAssignment_1()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1724:1: ( rule__Constraint__DeltaAssignment_1 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1724:2: rule__Constraint__DeltaAssignment_1
            {
            pushFollow(FOLLOW_rule__Constraint__DeltaAssignment_1_in_rule__Constraint__Group__1__Impl3426);
            rule__Constraint__DeltaAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getConstraintAccess().getDeltaAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__1__Impl"


    // $ANTLR start "rule__Constraint__Group__2"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1734:1: rule__Constraint__Group__2 : rule__Constraint__Group__2__Impl rule__Constraint__Group__3 ;
    public final void rule__Constraint__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1738:1: ( rule__Constraint__Group__2__Impl rule__Constraint__Group__3 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1739:2: rule__Constraint__Group__2__Impl rule__Constraint__Group__3
            {
            pushFollow(FOLLOW_rule__Constraint__Group__2__Impl_in_rule__Constraint__Group__23456);
            rule__Constraint__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Constraint__Group__3_in_rule__Constraint__Group__23459);
            rule__Constraint__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__2"


    // $ANTLR start "rule__Constraint__Group__2__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1746:1: rule__Constraint__Group__2__Impl : ( '}' ) ;
    public final void rule__Constraint__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1750:1: ( ( '}' ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1751:1: ( '}' )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1751:1: ( '}' )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1752:1: '}'
            {
             before(grammarAccess.getConstraintAccess().getRightCurlyBracketKeyword_2()); 
            match(input,15,FOLLOW_15_in_rule__Constraint__Group__2__Impl3487); 
             after(grammarAccess.getConstraintAccess().getRightCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__2__Impl"


    // $ANTLR start "rule__Constraint__Group__3"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1765:1: rule__Constraint__Group__3 : rule__Constraint__Group__3__Impl rule__Constraint__Group__4 ;
    public final void rule__Constraint__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1769:1: ( rule__Constraint__Group__3__Impl rule__Constraint__Group__4 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1770:2: rule__Constraint__Group__3__Impl rule__Constraint__Group__4
            {
            pushFollow(FOLLOW_rule__Constraint__Group__3__Impl_in_rule__Constraint__Group__33518);
            rule__Constraint__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Constraint__Group__4_in_rule__Constraint__Group__33521);
            rule__Constraint__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__3"


    // $ANTLR start "rule__Constraint__Group__3__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1777:1: rule__Constraint__Group__3__Impl : ( 'when' ) ;
    public final void rule__Constraint__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1781:1: ( ( 'when' ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1782:1: ( 'when' )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1782:1: ( 'when' )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1783:1: 'when'
            {
             before(grammarAccess.getConstraintAccess().getWhenKeyword_3()); 
            match(input,21,FOLLOW_21_in_rule__Constraint__Group__3__Impl3549); 
             after(grammarAccess.getConstraintAccess().getWhenKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__3__Impl"


    // $ANTLR start "rule__Constraint__Group__4"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1796:1: rule__Constraint__Group__4 : rule__Constraint__Group__4__Impl rule__Constraint__Group__5 ;
    public final void rule__Constraint__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1800:1: ( rule__Constraint__Group__4__Impl rule__Constraint__Group__5 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1801:2: rule__Constraint__Group__4__Impl rule__Constraint__Group__5
            {
            pushFollow(FOLLOW_rule__Constraint__Group__4__Impl_in_rule__Constraint__Group__43580);
            rule__Constraint__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Constraint__Group__5_in_rule__Constraint__Group__43583);
            rule__Constraint__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__4"


    // $ANTLR start "rule__Constraint__Group__4__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1808:1: rule__Constraint__Group__4__Impl : ( '(' ) ;
    public final void rule__Constraint__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1812:1: ( ( '(' ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1813:1: ( '(' )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1813:1: ( '(' )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1814:1: '('
            {
             before(grammarAccess.getConstraintAccess().getLeftParenthesisKeyword_4()); 
            match(input,22,FOLLOW_22_in_rule__Constraint__Group__4__Impl3611); 
             after(grammarAccess.getConstraintAccess().getLeftParenthesisKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__4__Impl"


    // $ANTLR start "rule__Constraint__Group__5"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1827:1: rule__Constraint__Group__5 : rule__Constraint__Group__5__Impl rule__Constraint__Group__6 ;
    public final void rule__Constraint__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1831:1: ( rule__Constraint__Group__5__Impl rule__Constraint__Group__6 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1832:2: rule__Constraint__Group__5__Impl rule__Constraint__Group__6
            {
            pushFollow(FOLLOW_rule__Constraint__Group__5__Impl_in_rule__Constraint__Group__53642);
            rule__Constraint__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Constraint__Group__6_in_rule__Constraint__Group__53645);
            rule__Constraint__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__5"


    // $ANTLR start "rule__Constraint__Group__5__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1839:1: rule__Constraint__Group__5__Impl : ( ( rule__Constraint__ExprAssignment_5 ) ) ;
    public final void rule__Constraint__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1843:1: ( ( ( rule__Constraint__ExprAssignment_5 ) ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1844:1: ( ( rule__Constraint__ExprAssignment_5 ) )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1844:1: ( ( rule__Constraint__ExprAssignment_5 ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1845:1: ( rule__Constraint__ExprAssignment_5 )
            {
             before(grammarAccess.getConstraintAccess().getExprAssignment_5()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1846:1: ( rule__Constraint__ExprAssignment_5 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1846:2: rule__Constraint__ExprAssignment_5
            {
            pushFollow(FOLLOW_rule__Constraint__ExprAssignment_5_in_rule__Constraint__Group__5__Impl3672);
            rule__Constraint__ExprAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getConstraintAccess().getExprAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__5__Impl"


    // $ANTLR start "rule__Constraint__Group__6"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1856:1: rule__Constraint__Group__6 : rule__Constraint__Group__6__Impl rule__Constraint__Group__7 ;
    public final void rule__Constraint__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1860:1: ( rule__Constraint__Group__6__Impl rule__Constraint__Group__7 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1861:2: rule__Constraint__Group__6__Impl rule__Constraint__Group__7
            {
            pushFollow(FOLLOW_rule__Constraint__Group__6__Impl_in_rule__Constraint__Group__63702);
            rule__Constraint__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Constraint__Group__7_in_rule__Constraint__Group__63705);
            rule__Constraint__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__6"


    // $ANTLR start "rule__Constraint__Group__6__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1868:1: rule__Constraint__Group__6__Impl : ( ')' ) ;
    public final void rule__Constraint__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1872:1: ( ( ')' ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1873:1: ( ')' )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1873:1: ( ')' )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1874:1: ')'
            {
             before(grammarAccess.getConstraintAccess().getRightParenthesisKeyword_6()); 
            match(input,23,FOLLOW_23_in_rule__Constraint__Group__6__Impl3733); 
             after(grammarAccess.getConstraintAccess().getRightParenthesisKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__6__Impl"


    // $ANTLR start "rule__Constraint__Group__7"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1887:1: rule__Constraint__Group__7 : rule__Constraint__Group__7__Impl ;
    public final void rule__Constraint__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1891:1: ( rule__Constraint__Group__7__Impl )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1892:2: rule__Constraint__Group__7__Impl
            {
            pushFollow(FOLLOW_rule__Constraint__Group__7__Impl_in_rule__Constraint__Group__73764);
            rule__Constraint__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__7"


    // $ANTLR start "rule__Constraint__Group__7__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1898:1: rule__Constraint__Group__7__Impl : ( ( rule__Constraint__LastAssignment_7 )? ) ;
    public final void rule__Constraint__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1902:1: ( ( ( rule__Constraint__LastAssignment_7 )? ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1903:1: ( ( rule__Constraint__LastAssignment_7 )? )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1903:1: ( ( rule__Constraint__LastAssignment_7 )? )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1904:1: ( rule__Constraint__LastAssignment_7 )?
            {
             before(grammarAccess.getConstraintAccess().getLastAssignment_7()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1905:1: ( rule__Constraint__LastAssignment_7 )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==27) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1905:2: rule__Constraint__LastAssignment_7
                    {
                    pushFollow(FOLLOW_rule__Constraint__LastAssignment_7_in_rule__Constraint__Group__7__Impl3791);
                    rule__Constraint__LastAssignment_7();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getConstraintAccess().getLastAssignment_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__7__Impl"


    // $ANTLR start "rule__ListElem__Group__0"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1931:1: rule__ListElem__Group__0 : rule__ListElem__Group__0__Impl rule__ListElem__Group__1 ;
    public final void rule__ListElem__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1935:1: ( rule__ListElem__Group__0__Impl rule__ListElem__Group__1 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1936:2: rule__ListElem__Group__0__Impl rule__ListElem__Group__1
            {
            pushFollow(FOLLOW_rule__ListElem__Group__0__Impl_in_rule__ListElem__Group__03838);
            rule__ListElem__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ListElem__Group__1_in_rule__ListElem__Group__03841);
            rule__ListElem__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ListElem__Group__0"


    // $ANTLR start "rule__ListElem__Group__0__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1943:1: rule__ListElem__Group__0__Impl : ( ( rule__ListElem__ElemAssignment_0 ) ) ;
    public final void rule__ListElem__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1947:1: ( ( ( rule__ListElem__ElemAssignment_0 ) ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1948:1: ( ( rule__ListElem__ElemAssignment_0 ) )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1948:1: ( ( rule__ListElem__ElemAssignment_0 ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1949:1: ( rule__ListElem__ElemAssignment_0 )
            {
             before(grammarAccess.getListElemAccess().getElemAssignment_0()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1950:1: ( rule__ListElem__ElemAssignment_0 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1950:2: rule__ListElem__ElemAssignment_0
            {
            pushFollow(FOLLOW_rule__ListElem__ElemAssignment_0_in_rule__ListElem__Group__0__Impl3868);
            rule__ListElem__ElemAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getListElemAccess().getElemAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ListElem__Group__0__Impl"


    // $ANTLR start "rule__ListElem__Group__1"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1960:1: rule__ListElem__Group__1 : rule__ListElem__Group__1__Impl ;
    public final void rule__ListElem__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1964:1: ( rule__ListElem__Group__1__Impl )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1965:2: rule__ListElem__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__ListElem__Group__1__Impl_in_rule__ListElem__Group__13898);
            rule__ListElem__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ListElem__Group__1"


    // $ANTLR start "rule__ListElem__Group__1__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1971:1: rule__ListElem__Group__1__Impl : ( ( rule__ListElem__LastAssignment_1 )? ) ;
    public final void rule__ListElem__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1975:1: ( ( ( rule__ListElem__LastAssignment_1 )? ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1976:1: ( ( rule__ListElem__LastAssignment_1 )? )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1976:1: ( ( rule__ListElem__LastAssignment_1 )? )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1977:1: ( rule__ListElem__LastAssignment_1 )?
            {
             before(grammarAccess.getListElemAccess().getLastAssignment_1()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1978:1: ( rule__ListElem__LastAssignment_1 )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==27) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1978:2: rule__ListElem__LastAssignment_1
                    {
                    pushFollow(FOLLOW_rule__ListElem__LastAssignment_1_in_rule__ListElem__Group__1__Impl3925);
                    rule__ListElem__LastAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getListElemAccess().getLastAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ListElem__Group__1__Impl"


    // $ANTLR start "rule__Core__Group__0"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1992:1: rule__Core__Group__0 : rule__Core__Group__0__Impl rule__Core__Group__1 ;
    public final void rule__Core__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1996:1: ( rule__Core__Group__0__Impl rule__Core__Group__1 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:1997:2: rule__Core__Group__0__Impl rule__Core__Group__1
            {
            pushFollow(FOLLOW_rule__Core__Group__0__Impl_in_rule__Core__Group__03960);
            rule__Core__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Core__Group__1_in_rule__Core__Group__03963);
            rule__Core__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Core__Group__0"


    // $ANTLR start "rule__Core__Group__0__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2004:1: rule__Core__Group__0__Impl : ( ( rule__Core__ExprAssignment_0 ) ) ;
    public final void rule__Core__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2008:1: ( ( ( rule__Core__ExprAssignment_0 ) ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2009:1: ( ( rule__Core__ExprAssignment_0 ) )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2009:1: ( ( rule__Core__ExprAssignment_0 ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2010:1: ( rule__Core__ExprAssignment_0 )
            {
             before(grammarAccess.getCoreAccess().getExprAssignment_0()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2011:1: ( rule__Core__ExprAssignment_0 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2011:2: rule__Core__ExprAssignment_0
            {
            pushFollow(FOLLOW_rule__Core__ExprAssignment_0_in_rule__Core__Group__0__Impl3990);
            rule__Core__ExprAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getCoreAccess().getExprAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Core__Group__0__Impl"


    // $ANTLR start "rule__Core__Group__1"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2021:1: rule__Core__Group__1 : rule__Core__Group__1__Impl ;
    public final void rule__Core__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2025:1: ( rule__Core__Group__1__Impl )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2026:2: rule__Core__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Core__Group__1__Impl_in_rule__Core__Group__14020);
            rule__Core__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Core__Group__1"


    // $ANTLR start "rule__Core__Group__1__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2032:1: rule__Core__Group__1__Impl : ( ';' ) ;
    public final void rule__Core__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2036:1: ( ( ';' ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2037:1: ( ';' )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2037:1: ( ';' )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2038:1: ';'
            {
             before(grammarAccess.getCoreAccess().getSemicolonKeyword_1()); 
            match(input,20,FOLLOW_20_in_rule__Core__Group__1__Impl4048); 
             after(grammarAccess.getCoreAccess().getSemicolonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Core__Group__1__Impl"


    // $ANTLR start "rule__Or__Group__0"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2055:1: rule__Or__Group__0 : rule__Or__Group__0__Impl rule__Or__Group__1 ;
    public final void rule__Or__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2059:1: ( rule__Or__Group__0__Impl rule__Or__Group__1 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2060:2: rule__Or__Group__0__Impl rule__Or__Group__1
            {
            pushFollow(FOLLOW_rule__Or__Group__0__Impl_in_rule__Or__Group__04083);
            rule__Or__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Or__Group__1_in_rule__Or__Group__04086);
            rule__Or__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__0"


    // $ANTLR start "rule__Or__Group__0__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2067:1: rule__Or__Group__0__Impl : ( ruleAnd ) ;
    public final void rule__Or__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2071:1: ( ( ruleAnd ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2072:1: ( ruleAnd )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2072:1: ( ruleAnd )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2073:1: ruleAnd
            {
             before(grammarAccess.getOrAccess().getAndParserRuleCall_0()); 
            pushFollow(FOLLOW_ruleAnd_in_rule__Or__Group__0__Impl4113);
            ruleAnd();

            state._fsp--;

             after(grammarAccess.getOrAccess().getAndParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__0__Impl"


    // $ANTLR start "rule__Or__Group__1"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2084:1: rule__Or__Group__1 : rule__Or__Group__1__Impl ;
    public final void rule__Or__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2088:1: ( rule__Or__Group__1__Impl )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2089:2: rule__Or__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Or__Group__1__Impl_in_rule__Or__Group__14142);
            rule__Or__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__1"


    // $ANTLR start "rule__Or__Group__1__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2095:1: rule__Or__Group__1__Impl : ( ( rule__Or__Group_1__0 )? ) ;
    public final void rule__Or__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2099:1: ( ( ( rule__Or__Group_1__0 )? ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2100:1: ( ( rule__Or__Group_1__0 )? )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2100:1: ( ( rule__Or__Group_1__0 )? )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2101:1: ( rule__Or__Group_1__0 )?
            {
             before(grammarAccess.getOrAccess().getGroup_1()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2102:1: ( rule__Or__Group_1__0 )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==24) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2102:2: rule__Or__Group_1__0
                    {
                    pushFollow(FOLLOW_rule__Or__Group_1__0_in_rule__Or__Group__1__Impl4169);
                    rule__Or__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getOrAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__1__Impl"


    // $ANTLR start "rule__Or__Group_1__0"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2116:1: rule__Or__Group_1__0 : rule__Or__Group_1__0__Impl rule__Or__Group_1__1 ;
    public final void rule__Or__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2120:1: ( rule__Or__Group_1__0__Impl rule__Or__Group_1__1 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2121:2: rule__Or__Group_1__0__Impl rule__Or__Group_1__1
            {
            pushFollow(FOLLOW_rule__Or__Group_1__0__Impl_in_rule__Or__Group_1__04204);
            rule__Or__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Or__Group_1__1_in_rule__Or__Group_1__04207);
            rule__Or__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__0"


    // $ANTLR start "rule__Or__Group_1__0__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2128:1: rule__Or__Group_1__0__Impl : ( () ) ;
    public final void rule__Or__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2132:1: ( ( () ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2133:1: ( () )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2133:1: ( () )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2134:1: ()
            {
             before(grammarAccess.getOrAccess().getOrLeftAction_1_0()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2135:1: ()
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2137:1: 
            {
            }

             after(grammarAccess.getOrAccess().getOrLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__0__Impl"


    // $ANTLR start "rule__Or__Group_1__1"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2147:1: rule__Or__Group_1__1 : rule__Or__Group_1__1__Impl rule__Or__Group_1__2 ;
    public final void rule__Or__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2151:1: ( rule__Or__Group_1__1__Impl rule__Or__Group_1__2 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2152:2: rule__Or__Group_1__1__Impl rule__Or__Group_1__2
            {
            pushFollow(FOLLOW_rule__Or__Group_1__1__Impl_in_rule__Or__Group_1__14265);
            rule__Or__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Or__Group_1__2_in_rule__Or__Group_1__14268);
            rule__Or__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__1"


    // $ANTLR start "rule__Or__Group_1__1__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2159:1: rule__Or__Group_1__1__Impl : ( '|' ) ;
    public final void rule__Or__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2163:1: ( ( '|' ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2164:1: ( '|' )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2164:1: ( '|' )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2165:1: '|'
            {
             before(grammarAccess.getOrAccess().getVerticalLineKeyword_1_1()); 
            match(input,24,FOLLOW_24_in_rule__Or__Group_1__1__Impl4296); 
             after(grammarAccess.getOrAccess().getVerticalLineKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__1__Impl"


    // $ANTLR start "rule__Or__Group_1__2"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2178:1: rule__Or__Group_1__2 : rule__Or__Group_1__2__Impl ;
    public final void rule__Or__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2182:1: ( rule__Or__Group_1__2__Impl )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2183:2: rule__Or__Group_1__2__Impl
            {
            pushFollow(FOLLOW_rule__Or__Group_1__2__Impl_in_rule__Or__Group_1__24327);
            rule__Or__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__2"


    // $ANTLR start "rule__Or__Group_1__2__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2189:1: rule__Or__Group_1__2__Impl : ( ( rule__Or__RightAssignment_1_2 ) ) ;
    public final void rule__Or__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2193:1: ( ( ( rule__Or__RightAssignment_1_2 ) ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2194:1: ( ( rule__Or__RightAssignment_1_2 ) )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2194:1: ( ( rule__Or__RightAssignment_1_2 ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2195:1: ( rule__Or__RightAssignment_1_2 )
            {
             before(grammarAccess.getOrAccess().getRightAssignment_1_2()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2196:1: ( rule__Or__RightAssignment_1_2 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2196:2: rule__Or__RightAssignment_1_2
            {
            pushFollow(FOLLOW_rule__Or__RightAssignment_1_2_in_rule__Or__Group_1__2__Impl4354);
            rule__Or__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getOrAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__2__Impl"


    // $ANTLR start "rule__And__Group__0"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2212:1: rule__And__Group__0 : rule__And__Group__0__Impl rule__And__Group__1 ;
    public final void rule__And__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2216:1: ( rule__And__Group__0__Impl rule__And__Group__1 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2217:2: rule__And__Group__0__Impl rule__And__Group__1
            {
            pushFollow(FOLLOW_rule__And__Group__0__Impl_in_rule__And__Group__04390);
            rule__And__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__And__Group__1_in_rule__And__Group__04393);
            rule__And__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__0"


    // $ANTLR start "rule__And__Group__0__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2224:1: rule__And__Group__0__Impl : ( rulePrimary ) ;
    public final void rule__And__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2228:1: ( ( rulePrimary ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2229:1: ( rulePrimary )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2229:1: ( rulePrimary )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2230:1: rulePrimary
            {
             before(grammarAccess.getAndAccess().getPrimaryParserRuleCall_0()); 
            pushFollow(FOLLOW_rulePrimary_in_rule__And__Group__0__Impl4420);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getAndAccess().getPrimaryParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__0__Impl"


    // $ANTLR start "rule__And__Group__1"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2241:1: rule__And__Group__1 : rule__And__Group__1__Impl ;
    public final void rule__And__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2245:1: ( rule__And__Group__1__Impl )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2246:2: rule__And__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__And__Group__1__Impl_in_rule__And__Group__14449);
            rule__And__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__1"


    // $ANTLR start "rule__And__Group__1__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2252:1: rule__And__Group__1__Impl : ( ( rule__And__Group_1__0 )? ) ;
    public final void rule__And__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2256:1: ( ( ( rule__And__Group_1__0 )? ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2257:1: ( ( rule__And__Group_1__0 )? )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2257:1: ( ( rule__And__Group_1__0 )? )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2258:1: ( rule__And__Group_1__0 )?
            {
             before(grammarAccess.getAndAccess().getGroup_1()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2259:1: ( rule__And__Group_1__0 )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==25) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2259:2: rule__And__Group_1__0
                    {
                    pushFollow(FOLLOW_rule__And__Group_1__0_in_rule__And__Group__1__Impl4476);
                    rule__And__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAndAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__1__Impl"


    // $ANTLR start "rule__And__Group_1__0"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2273:1: rule__And__Group_1__0 : rule__And__Group_1__0__Impl rule__And__Group_1__1 ;
    public final void rule__And__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2277:1: ( rule__And__Group_1__0__Impl rule__And__Group_1__1 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2278:2: rule__And__Group_1__0__Impl rule__And__Group_1__1
            {
            pushFollow(FOLLOW_rule__And__Group_1__0__Impl_in_rule__And__Group_1__04511);
            rule__And__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__And__Group_1__1_in_rule__And__Group_1__04514);
            rule__And__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__0"


    // $ANTLR start "rule__And__Group_1__0__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2285:1: rule__And__Group_1__0__Impl : ( () ) ;
    public final void rule__And__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2289:1: ( ( () ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2290:1: ( () )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2290:1: ( () )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2291:1: ()
            {
             before(grammarAccess.getAndAccess().getAndLeftAction_1_0()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2292:1: ()
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2294:1: 
            {
            }

             after(grammarAccess.getAndAccess().getAndLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__0__Impl"


    // $ANTLR start "rule__And__Group_1__1"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2304:1: rule__And__Group_1__1 : rule__And__Group_1__1__Impl rule__And__Group_1__2 ;
    public final void rule__And__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2308:1: ( rule__And__Group_1__1__Impl rule__And__Group_1__2 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2309:2: rule__And__Group_1__1__Impl rule__And__Group_1__2
            {
            pushFollow(FOLLOW_rule__And__Group_1__1__Impl_in_rule__And__Group_1__14572);
            rule__And__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__And__Group_1__2_in_rule__And__Group_1__14575);
            rule__And__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__1"


    // $ANTLR start "rule__And__Group_1__1__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2316:1: rule__And__Group_1__1__Impl : ( '&' ) ;
    public final void rule__And__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2320:1: ( ( '&' ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2321:1: ( '&' )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2321:1: ( '&' )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2322:1: '&'
            {
             before(grammarAccess.getAndAccess().getAmpersandKeyword_1_1()); 
            match(input,25,FOLLOW_25_in_rule__And__Group_1__1__Impl4603); 
             after(grammarAccess.getAndAccess().getAmpersandKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__1__Impl"


    // $ANTLR start "rule__And__Group_1__2"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2335:1: rule__And__Group_1__2 : rule__And__Group_1__2__Impl ;
    public final void rule__And__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2339:1: ( rule__And__Group_1__2__Impl )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2340:2: rule__And__Group_1__2__Impl
            {
            pushFollow(FOLLOW_rule__And__Group_1__2__Impl_in_rule__And__Group_1__24634);
            rule__And__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__2"


    // $ANTLR start "rule__And__Group_1__2__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2346:1: rule__And__Group_1__2__Impl : ( ( rule__And__RightAssignment_1_2 ) ) ;
    public final void rule__And__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2350:1: ( ( ( rule__And__RightAssignment_1_2 ) ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2351:1: ( ( rule__And__RightAssignment_1_2 ) )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2351:1: ( ( rule__And__RightAssignment_1_2 ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2352:1: ( rule__And__RightAssignment_1_2 )
            {
             before(grammarAccess.getAndAccess().getRightAssignment_1_2()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2353:1: ( rule__And__RightAssignment_1_2 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2353:2: rule__And__RightAssignment_1_2
            {
            pushFollow(FOLLOW_rule__And__RightAssignment_1_2_in_rule__And__Group_1__2__Impl4661);
            rule__And__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getAndAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__2__Impl"


    // $ANTLR start "rule__Primary__Group_1__0"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2369:1: rule__Primary__Group_1__0 : rule__Primary__Group_1__0__Impl rule__Primary__Group_1__1 ;
    public final void rule__Primary__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2373:1: ( rule__Primary__Group_1__0__Impl rule__Primary__Group_1__1 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2374:2: rule__Primary__Group_1__0__Impl rule__Primary__Group_1__1
            {
            pushFollow(FOLLOW_rule__Primary__Group_1__0__Impl_in_rule__Primary__Group_1__04697);
            rule__Primary__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Primary__Group_1__1_in_rule__Primary__Group_1__04700);
            rule__Primary__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_1__0"


    // $ANTLR start "rule__Primary__Group_1__0__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2381:1: rule__Primary__Group_1__0__Impl : ( () ) ;
    public final void rule__Primary__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2385:1: ( ( () ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2386:1: ( () )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2386:1: ( () )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2387:1: ()
            {
             before(grammarAccess.getPrimaryAccess().getSubAction_1_0()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2388:1: ()
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2390:1: 
            {
            }

             after(grammarAccess.getPrimaryAccess().getSubAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_1__0__Impl"


    // $ANTLR start "rule__Primary__Group_1__1"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2400:1: rule__Primary__Group_1__1 : rule__Primary__Group_1__1__Impl rule__Primary__Group_1__2 ;
    public final void rule__Primary__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2404:1: ( rule__Primary__Group_1__1__Impl rule__Primary__Group_1__2 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2405:2: rule__Primary__Group_1__1__Impl rule__Primary__Group_1__2
            {
            pushFollow(FOLLOW_rule__Primary__Group_1__1__Impl_in_rule__Primary__Group_1__14758);
            rule__Primary__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Primary__Group_1__2_in_rule__Primary__Group_1__14761);
            rule__Primary__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_1__1"


    // $ANTLR start "rule__Primary__Group_1__1__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2412:1: rule__Primary__Group_1__1__Impl : ( '(' ) ;
    public final void rule__Primary__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2416:1: ( ( '(' ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2417:1: ( '(' )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2417:1: ( '(' )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2418:1: '('
            {
             before(grammarAccess.getPrimaryAccess().getLeftParenthesisKeyword_1_1()); 
            match(input,22,FOLLOW_22_in_rule__Primary__Group_1__1__Impl4789); 
             after(grammarAccess.getPrimaryAccess().getLeftParenthesisKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_1__1__Impl"


    // $ANTLR start "rule__Primary__Group_1__2"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2431:1: rule__Primary__Group_1__2 : rule__Primary__Group_1__2__Impl rule__Primary__Group_1__3 ;
    public final void rule__Primary__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2435:1: ( rule__Primary__Group_1__2__Impl rule__Primary__Group_1__3 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2436:2: rule__Primary__Group_1__2__Impl rule__Primary__Group_1__3
            {
            pushFollow(FOLLOW_rule__Primary__Group_1__2__Impl_in_rule__Primary__Group_1__24820);
            rule__Primary__Group_1__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Primary__Group_1__3_in_rule__Primary__Group_1__24823);
            rule__Primary__Group_1__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_1__2"


    // $ANTLR start "rule__Primary__Group_1__2__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2443:1: rule__Primary__Group_1__2__Impl : ( ( rule__Primary__ExprAssignment_1_2 ) ) ;
    public final void rule__Primary__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2447:1: ( ( ( rule__Primary__ExprAssignment_1_2 ) ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2448:1: ( ( rule__Primary__ExprAssignment_1_2 ) )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2448:1: ( ( rule__Primary__ExprAssignment_1_2 ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2449:1: ( rule__Primary__ExprAssignment_1_2 )
            {
             before(grammarAccess.getPrimaryAccess().getExprAssignment_1_2()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2450:1: ( rule__Primary__ExprAssignment_1_2 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2450:2: rule__Primary__ExprAssignment_1_2
            {
            pushFollow(FOLLOW_rule__Primary__ExprAssignment_1_2_in_rule__Primary__Group_1__2__Impl4850);
            rule__Primary__ExprAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getPrimaryAccess().getExprAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_1__2__Impl"


    // $ANTLR start "rule__Primary__Group_1__3"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2460:1: rule__Primary__Group_1__3 : rule__Primary__Group_1__3__Impl ;
    public final void rule__Primary__Group_1__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2464:1: ( rule__Primary__Group_1__3__Impl )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2465:2: rule__Primary__Group_1__3__Impl
            {
            pushFollow(FOLLOW_rule__Primary__Group_1__3__Impl_in_rule__Primary__Group_1__34880);
            rule__Primary__Group_1__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_1__3"


    // $ANTLR start "rule__Primary__Group_1__3__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2471:1: rule__Primary__Group_1__3__Impl : ( ')' ) ;
    public final void rule__Primary__Group_1__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2475:1: ( ( ')' ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2476:1: ( ')' )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2476:1: ( ')' )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2477:1: ')'
            {
             before(grammarAccess.getPrimaryAccess().getRightParenthesisKeyword_1_3()); 
            match(input,23,FOLLOW_23_in_rule__Primary__Group_1__3__Impl4908); 
             after(grammarAccess.getPrimaryAccess().getRightParenthesisKeyword_1_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_1__3__Impl"


    // $ANTLR start "rule__Primary__Group_2__0"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2498:1: rule__Primary__Group_2__0 : rule__Primary__Group_2__0__Impl rule__Primary__Group_2__1 ;
    public final void rule__Primary__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2502:1: ( rule__Primary__Group_2__0__Impl rule__Primary__Group_2__1 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2503:2: rule__Primary__Group_2__0__Impl rule__Primary__Group_2__1
            {
            pushFollow(FOLLOW_rule__Primary__Group_2__0__Impl_in_rule__Primary__Group_2__04947);
            rule__Primary__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Primary__Group_2__1_in_rule__Primary__Group_2__04950);
            rule__Primary__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_2__0"


    // $ANTLR start "rule__Primary__Group_2__0__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2510:1: rule__Primary__Group_2__0__Impl : ( () ) ;
    public final void rule__Primary__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2514:1: ( ( () ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2515:1: ( () )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2515:1: ( () )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2516:1: ()
            {
             before(grammarAccess.getPrimaryAccess().getNotAction_2_0()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2517:1: ()
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2519:1: 
            {
            }

             after(grammarAccess.getPrimaryAccess().getNotAction_2_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_2__0__Impl"


    // $ANTLR start "rule__Primary__Group_2__1"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2529:1: rule__Primary__Group_2__1 : rule__Primary__Group_2__1__Impl rule__Primary__Group_2__2 ;
    public final void rule__Primary__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2533:1: ( rule__Primary__Group_2__1__Impl rule__Primary__Group_2__2 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2534:2: rule__Primary__Group_2__1__Impl rule__Primary__Group_2__2
            {
            pushFollow(FOLLOW_rule__Primary__Group_2__1__Impl_in_rule__Primary__Group_2__15008);
            rule__Primary__Group_2__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Primary__Group_2__2_in_rule__Primary__Group_2__15011);
            rule__Primary__Group_2__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_2__1"


    // $ANTLR start "rule__Primary__Group_2__1__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2541:1: rule__Primary__Group_2__1__Impl : ( '!' ) ;
    public final void rule__Primary__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2545:1: ( ( '!' ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2546:1: ( '!' )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2546:1: ( '!' )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2547:1: '!'
            {
             before(grammarAccess.getPrimaryAccess().getExclamationMarkKeyword_2_1()); 
            match(input,26,FOLLOW_26_in_rule__Primary__Group_2__1__Impl5039); 
             after(grammarAccess.getPrimaryAccess().getExclamationMarkKeyword_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_2__1__Impl"


    // $ANTLR start "rule__Primary__Group_2__2"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2560:1: rule__Primary__Group_2__2 : rule__Primary__Group_2__2__Impl ;
    public final void rule__Primary__Group_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2564:1: ( rule__Primary__Group_2__2__Impl )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2565:2: rule__Primary__Group_2__2__Impl
            {
            pushFollow(FOLLOW_rule__Primary__Group_2__2__Impl_in_rule__Primary__Group_2__25070);
            rule__Primary__Group_2__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_2__2"


    // $ANTLR start "rule__Primary__Group_2__2__Impl"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2571:1: rule__Primary__Group_2__2__Impl : ( ( rule__Primary__ValueAssignment_2_2 ) ) ;
    public final void rule__Primary__Group_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2575:1: ( ( ( rule__Primary__ValueAssignment_2_2 ) ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2576:1: ( ( rule__Primary__ValueAssignment_2_2 ) )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2576:1: ( ( rule__Primary__ValueAssignment_2_2 ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2577:1: ( rule__Primary__ValueAssignment_2_2 )
            {
             before(grammarAccess.getPrimaryAccess().getValueAssignment_2_2()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2578:1: ( rule__Primary__ValueAssignment_2_2 )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2578:2: rule__Primary__ValueAssignment_2_2
            {
            pushFollow(FOLLOW_rule__Primary__ValueAssignment_2_2_in_rule__Primary__Group_2__2__Impl5097);
            rule__Primary__ValueAssignment_2_2();

            state._fsp--;


            }

             after(grammarAccess.getPrimaryAccess().getValueAssignment_2_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_2__2__Impl"


    // $ANTLR start "rule__ProductDeclaration__NameAssignment_1"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2595:1: rule__ProductDeclaration__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__ProductDeclaration__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2599:1: ( ( RULE_ID ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2600:1: ( RULE_ID )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2600:1: ( RULE_ID )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2601:1: RULE_ID
            {
             before(grammarAccess.getProductDeclarationAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__ProductDeclaration__NameAssignment_15138); 
             after(grammarAccess.getProductDeclarationAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__NameAssignment_1"


    // $ANTLR start "rule__ProductDeclaration__FeaturesAssignment_6"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2610:1: rule__ProductDeclaration__FeaturesAssignment_6 : ( ruleFeatures ) ;
    public final void rule__ProductDeclaration__FeaturesAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2614:1: ( ( ruleFeatures ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2615:1: ( ruleFeatures )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2615:1: ( ruleFeatures )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2616:1: ruleFeatures
            {
             before(grammarAccess.getProductDeclarationAccess().getFeaturesFeaturesParserRuleCall_6_0()); 
            pushFollow(FOLLOW_ruleFeatures_in_rule__ProductDeclaration__FeaturesAssignment_65169);
            ruleFeatures();

            state._fsp--;

             after(grammarAccess.getProductDeclarationAccess().getFeaturesFeaturesParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__FeaturesAssignment_6"


    // $ANTLR start "rule__ProductDeclaration__DeltasAssignment_11"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2625:1: rule__ProductDeclaration__DeltasAssignment_11 : ( ruleDeltas ) ;
    public final void rule__ProductDeclaration__DeltasAssignment_11() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2629:1: ( ( ruleDeltas ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2630:1: ( ruleDeltas )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2630:1: ( ruleDeltas )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2631:1: ruleDeltas
            {
             before(grammarAccess.getProductDeclarationAccess().getDeltasDeltasParserRuleCall_11_0()); 
            pushFollow(FOLLOW_ruleDeltas_in_rule__ProductDeclaration__DeltasAssignment_115200);
            ruleDeltas();

            state._fsp--;

             after(grammarAccess.getProductDeclarationAccess().getDeltasDeltasParserRuleCall_11_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__DeltasAssignment_11"


    // $ANTLR start "rule__ProductDeclaration__ConstraintsAssignment_15"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2640:1: rule__ProductDeclaration__ConstraintsAssignment_15 : ( ruleCore ) ;
    public final void rule__ProductDeclaration__ConstraintsAssignment_15() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2644:1: ( ( ruleCore ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2645:1: ( ruleCore )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2645:1: ( ruleCore )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2646:1: ruleCore
            {
             before(grammarAccess.getProductDeclarationAccess().getConstraintsCoreParserRuleCall_15_0()); 
            pushFollow(FOLLOW_ruleCore_in_rule__ProductDeclaration__ConstraintsAssignment_155231);
            ruleCore();

            state._fsp--;

             after(grammarAccess.getProductDeclarationAccess().getConstraintsCoreParserRuleCall_15_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__ConstraintsAssignment_15"


    // $ANTLR start "rule__ProductDeclaration__PartitionsAssignment_19"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2655:1: rule__ProductDeclaration__PartitionsAssignment_19 : ( rulePartitions ) ;
    public final void rule__ProductDeclaration__PartitionsAssignment_19() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2659:1: ( ( rulePartitions ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2660:1: ( rulePartitions )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2660:1: ( rulePartitions )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2661:1: rulePartitions
            {
             before(grammarAccess.getProductDeclarationAccess().getPartitionsPartitionsParserRuleCall_19_0()); 
            pushFollow(FOLLOW_rulePartitions_in_rule__ProductDeclaration__PartitionsAssignment_195262);
            rulePartitions();

            state._fsp--;

             after(grammarAccess.getProductDeclarationAccess().getPartitionsPartitionsParserRuleCall_19_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__PartitionsAssignment_19"


    // $ANTLR start "rule__ProductDeclaration__ProductsAssignment_23"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2670:1: rule__ProductDeclaration__ProductsAssignment_23 : ( ruleProducts ) ;
    public final void rule__ProductDeclaration__ProductsAssignment_23() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2674:1: ( ( ruleProducts ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2675:1: ( ruleProducts )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2675:1: ( ruleProducts )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2676:1: ruleProducts
            {
             before(grammarAccess.getProductDeclarationAccess().getProductsProductsParserRuleCall_23_0()); 
            pushFollow(FOLLOW_ruleProducts_in_rule__ProductDeclaration__ProductsAssignment_235293);
            ruleProducts();

            state._fsp--;

             after(grammarAccess.getProductDeclarationAccess().getProductsProductsParserRuleCall_23_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProductDeclaration__ProductsAssignment_23"


    // $ANTLR start "rule__Features__ListAssignment"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2685:1: rule__Features__ListAssignment : ( ruleListElem ) ;
    public final void rule__Features__ListAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2689:1: ( ( ruleListElem ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2690:1: ( ruleListElem )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2690:1: ( ruleListElem )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2691:1: ruleListElem
            {
             before(grammarAccess.getFeaturesAccess().getListListElemParserRuleCall_0()); 
            pushFollow(FOLLOW_ruleListElem_in_rule__Features__ListAssignment5324);
            ruleListElem();

            state._fsp--;

             after(grammarAccess.getFeaturesAccess().getListListElemParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Features__ListAssignment"


    // $ANTLR start "rule__Deltas__ListAssignment"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2700:1: rule__Deltas__ListAssignment : ( ruleListElem ) ;
    public final void rule__Deltas__ListAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2704:1: ( ( ruleListElem ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2705:1: ( ruleListElem )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2705:1: ( ruleListElem )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2706:1: ruleListElem
            {
             before(grammarAccess.getDeltasAccess().getListListElemParserRuleCall_0()); 
            pushFollow(FOLLOW_ruleListElem_in_rule__Deltas__ListAssignment5355);
            ruleListElem();

            state._fsp--;

             after(grammarAccess.getDeltasAccess().getListListElemParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Deltas__ListAssignment"


    // $ANTLR start "rule__Products__ListAssignment"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2715:1: rule__Products__ListAssignment : ( ruleProduct ) ;
    public final void rule__Products__ListAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2719:1: ( ( ruleProduct ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2720:1: ( ruleProduct )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2720:1: ( ruleProduct )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2721:1: ruleProduct
            {
             before(grammarAccess.getProductsAccess().getListProductParserRuleCall_0()); 
            pushFollow(FOLLOW_ruleProduct_in_rule__Products__ListAssignment5386);
            ruleProduct();

            state._fsp--;

             after(grammarAccess.getProductsAccess().getListProductParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Products__ListAssignment"


    // $ANTLR start "rule__Product__NameAssignment_0"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2730:1: rule__Product__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__Product__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2734:1: ( ( RULE_ID ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2735:1: ( RULE_ID )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2735:1: ( RULE_ID )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2736:1: RULE_ID
            {
             before(grammarAccess.getProductAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Product__NameAssignment_05417); 
             after(grammarAccess.getProductAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Product__NameAssignment_0"


    // $ANTLR start "rule__Product__ListAssignment_3"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2745:1: rule__Product__ListAssignment_3 : ( ruleListElem ) ;
    public final void rule__Product__ListAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2749:1: ( ( ruleListElem ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2750:1: ( ruleListElem )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2750:1: ( ruleListElem )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2751:1: ruleListElem
            {
             before(grammarAccess.getProductAccess().getListListElemParserRuleCall_3_0()); 
            pushFollow(FOLLOW_ruleListElem_in_rule__Product__ListAssignment_35448);
            ruleListElem();

            state._fsp--;

             after(grammarAccess.getProductAccess().getListListElemParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Product__ListAssignment_3"


    // $ANTLR start "rule__Partitions__ListAssignment"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2760:1: rule__Partitions__ListAssignment : ( rulePartition ) ;
    public final void rule__Partitions__ListAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2764:1: ( ( rulePartition ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2765:1: ( rulePartition )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2765:1: ( rulePartition )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2766:1: rulePartition
            {
             before(grammarAccess.getPartitionsAccess().getListPartitionParserRuleCall_0()); 
            pushFollow(FOLLOW_rulePartition_in_rule__Partitions__ListAssignment5479);
            rulePartition();

            state._fsp--;

             after(grammarAccess.getPartitionsAccess().getListPartitionParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Partitions__ListAssignment"


    // $ANTLR start "rule__Partition__ConstrAssignment_0"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2775:1: rule__Partition__ConstrAssignment_0 : ( ruleConstraint ) ;
    public final void rule__Partition__ConstrAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2779:1: ( ( ruleConstraint ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2780:1: ( ruleConstraint )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2780:1: ( ruleConstraint )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2781:1: ruleConstraint
            {
             before(grammarAccess.getPartitionAccess().getConstrConstraintParserRuleCall_0_0()); 
            pushFollow(FOLLOW_ruleConstraint_in_rule__Partition__ConstrAssignment_05510);
            ruleConstraint();

            state._fsp--;

             after(grammarAccess.getPartitionAccess().getConstrConstraintParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Partition__ConstrAssignment_0"


    // $ANTLR start "rule__Constraint__DeltaAssignment_1"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2790:1: rule__Constraint__DeltaAssignment_1 : ( ruleElement ) ;
    public final void rule__Constraint__DeltaAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2794:1: ( ( ruleElement ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2795:1: ( ruleElement )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2795:1: ( ruleElement )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2796:1: ruleElement
            {
             before(grammarAccess.getConstraintAccess().getDeltaElementParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleElement_in_rule__Constraint__DeltaAssignment_15541);
            ruleElement();

            state._fsp--;

             after(grammarAccess.getConstraintAccess().getDeltaElementParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__DeltaAssignment_1"


    // $ANTLR start "rule__Constraint__ExprAssignment_5"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2805:1: rule__Constraint__ExprAssignment_5 : ( ruleExpression ) ;
    public final void rule__Constraint__ExprAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2809:1: ( ( ruleExpression ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2810:1: ( ruleExpression )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2810:1: ( ruleExpression )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2811:1: ruleExpression
            {
             before(grammarAccess.getConstraintAccess().getExprExpressionParserRuleCall_5_0()); 
            pushFollow(FOLLOW_ruleExpression_in_rule__Constraint__ExprAssignment_55572);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getConstraintAccess().getExprExpressionParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__ExprAssignment_5"


    // $ANTLR start "rule__Constraint__LastAssignment_7"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2820:1: rule__Constraint__LastAssignment_7 : ( ( ',' ) ) ;
    public final void rule__Constraint__LastAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2824:1: ( ( ( ',' ) ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2825:1: ( ( ',' ) )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2825:1: ( ( ',' ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2826:1: ( ',' )
            {
             before(grammarAccess.getConstraintAccess().getLastCommaKeyword_7_0()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2827:1: ( ',' )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2828:1: ','
            {
             before(grammarAccess.getConstraintAccess().getLastCommaKeyword_7_0()); 
            match(input,27,FOLLOW_27_in_rule__Constraint__LastAssignment_75608); 
             after(grammarAccess.getConstraintAccess().getLastCommaKeyword_7_0()); 

            }

             after(grammarAccess.getConstraintAccess().getLastCommaKeyword_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__LastAssignment_7"


    // $ANTLR start "rule__ListElem__ElemAssignment_0"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2843:1: rule__ListElem__ElemAssignment_0 : ( ruleElement ) ;
    public final void rule__ListElem__ElemAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2847:1: ( ( ruleElement ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2848:1: ( ruleElement )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2848:1: ( ruleElement )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2849:1: ruleElement
            {
             before(grammarAccess.getListElemAccess().getElemElementParserRuleCall_0_0()); 
            pushFollow(FOLLOW_ruleElement_in_rule__ListElem__ElemAssignment_05647);
            ruleElement();

            state._fsp--;

             after(grammarAccess.getListElemAccess().getElemElementParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ListElem__ElemAssignment_0"


    // $ANTLR start "rule__ListElem__LastAssignment_1"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2858:1: rule__ListElem__LastAssignment_1 : ( ( ',' ) ) ;
    public final void rule__ListElem__LastAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2862:1: ( ( ( ',' ) ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2863:1: ( ( ',' ) )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2863:1: ( ( ',' ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2864:1: ( ',' )
            {
             before(grammarAccess.getListElemAccess().getLastCommaKeyword_1_0()); 
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2865:1: ( ',' )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2866:1: ','
            {
             before(grammarAccess.getListElemAccess().getLastCommaKeyword_1_0()); 
            match(input,27,FOLLOW_27_in_rule__ListElem__LastAssignment_15683); 
             after(grammarAccess.getListElemAccess().getLastCommaKeyword_1_0()); 

            }

             after(grammarAccess.getListElemAccess().getLastCommaKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ListElem__LastAssignment_1"


    // $ANTLR start "rule__Element__NameAssignment"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2881:1: rule__Element__NameAssignment : ( RULE_ID ) ;
    public final void rule__Element__NameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2885:1: ( ( RULE_ID ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2886:1: ( RULE_ID )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2886:1: ( RULE_ID )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2887:1: RULE_ID
            {
             before(grammarAccess.getElementAccess().getNameIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Element__NameAssignment5722); 
             after(grammarAccess.getElementAccess().getNameIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Element__NameAssignment"


    // $ANTLR start "rule__Core__ExprAssignment_0"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2896:1: rule__Core__ExprAssignment_0 : ( ruleExpression ) ;
    public final void rule__Core__ExprAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2900:1: ( ( ruleExpression ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2901:1: ( ruleExpression )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2901:1: ( ruleExpression )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2902:1: ruleExpression
            {
             before(grammarAccess.getCoreAccess().getExprExpressionParserRuleCall_0_0()); 
            pushFollow(FOLLOW_ruleExpression_in_rule__Core__ExprAssignment_05753);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getCoreAccess().getExprExpressionParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Core__ExprAssignment_0"


    // $ANTLR start "rule__Or__RightAssignment_1_2"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2911:1: rule__Or__RightAssignment_1_2 : ( ruleOr ) ;
    public final void rule__Or__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2915:1: ( ( ruleOr ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2916:1: ( ruleOr )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2916:1: ( ruleOr )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2917:1: ruleOr
            {
             before(grammarAccess.getOrAccess().getRightOrParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_ruleOr_in_rule__Or__RightAssignment_1_25784);
            ruleOr();

            state._fsp--;

             after(grammarAccess.getOrAccess().getRightOrParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__RightAssignment_1_2"


    // $ANTLR start "rule__And__RightAssignment_1_2"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2926:1: rule__And__RightAssignment_1_2 : ( ruleAnd ) ;
    public final void rule__And__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2930:1: ( ( ruleAnd ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2931:1: ( ruleAnd )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2931:1: ( ruleAnd )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2932:1: ruleAnd
            {
             before(grammarAccess.getAndAccess().getRightAndParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_ruleAnd_in_rule__And__RightAssignment_1_25815);
            ruleAnd();

            state._fsp--;

             after(grammarAccess.getAndAccess().getRightAndParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__RightAssignment_1_2"


    // $ANTLR start "rule__Primary__ExprAssignment_1_2"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2941:1: rule__Primary__ExprAssignment_1_2 : ( ruleExpression ) ;
    public final void rule__Primary__ExprAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2945:1: ( ( ruleExpression ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2946:1: ( ruleExpression )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2946:1: ( ruleExpression )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2947:1: ruleExpression
            {
             before(grammarAccess.getPrimaryAccess().getExprExpressionParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_ruleExpression_in_rule__Primary__ExprAssignment_1_25846);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getPrimaryAccess().getExprExpressionParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__ExprAssignment_1_2"


    // $ANTLR start "rule__Primary__ValueAssignment_2_2"
    // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2956:1: rule__Primary__ValueAssignment_2_2 : ( rulePrimary ) ;
    public final void rule__Primary__ValueAssignment_2_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2960:1: ( ( rulePrimary ) )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2961:1: ( rulePrimary )
            {
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2961:1: ( rulePrimary )
            // ../org.xtext.dop.productdeclaration.ui/src-gen/org/xtext/dop/productdeclaration/ui/contentassist/antlr/internal/InternalProductDeclaration.g:2962:1: rulePrimary
            {
             before(grammarAccess.getPrimaryAccess().getValuePrimaryParserRuleCall_2_2_0()); 
            pushFollow(FOLLOW_rulePrimary_in_rule__Primary__ValueAssignment_2_25877);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getPrimaryAccess().getValuePrimaryParserRuleCall_2_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__ValueAssignment_2_2"

    // Delegated rules


 

    public static final BitSet FOLLOW_ruleProductDeclaration_in_entryRuleProductDeclaration61 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleProductDeclaration68 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__0_in_ruleProductDeclaration94 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFeatures_in_entryRuleFeatures121 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFeatures128 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Features__ListAssignment_in_ruleFeatures156 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_rule__Features__ListAssignment_in_ruleFeatures168 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_ruleDeltas_in_entryRuleDeltas198 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDeltas205 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Deltas__ListAssignment_in_ruleDeltas233 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_rule__Deltas__ListAssignment_in_ruleDeltas245 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_ruleProducts_in_entryRuleProducts275 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleProducts282 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Products__ListAssignment_in_ruleProducts310 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_rule__Products__ListAssignment_in_ruleProducts322 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_ruleProduct_in_entryRuleProduct352 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleProduct359 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Product__Group__0_in_ruleProduct385 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePartitions_in_entryRulePartitions412 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePartitions419 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Partitions__ListAssignment_in_rulePartitions447 = new BitSet(new long[]{0x0000000000001002L});
    public static final BitSet FOLLOW_rule__Partitions__ListAssignment_in_rulePartitions459 = new BitSet(new long[]{0x0000000000001002L});
    public static final BitSet FOLLOW_rulePartition_in_entryRulePartition489 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePartition496 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Partition__Group__0_in_rulePartition522 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleConstraint_in_entryRuleConstraint549 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleConstraint556 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Constraint__Group__0_in_ruleConstraint582 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleListElem_in_entryRuleListElem609 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleListElem616 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ListElem__Group__0_in_ruleListElem642 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleElement_in_entryRuleElement669 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleElement676 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Element__NameAssignment_in_ruleElement702 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCore_in_entryRuleCore729 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCore736 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Core__Group__0_in_ruleCore762 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExpression_in_entryRuleExpression789 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleExpression796 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOr_in_ruleExpression822 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOr_in_entryRuleOr848 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOr855 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Or__Group__0_in_ruleOr881 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAnd_in_entryRuleAnd908 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAnd915 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__And__Group__0_in_ruleAnd941 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePrimary_in_entryRulePrimary968 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePrimary975 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Primary__Alternatives_in_rulePrimary1001 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAtomic_in_entryRuleAtomic1028 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAtomic1035 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleElement_in_ruleAtomic1061 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAtomic_in_rule__Primary__Alternatives1096 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Primary__Group_1__0_in_rule__Primary__Alternatives1113 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Primary__Group_2__0_in_rule__Primary__Alternatives1131 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__0__Impl_in_rule__ProductDeclaration__Group__01162 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__1_in_rule__ProductDeclaration__Group__01165 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_rule__ProductDeclaration__Group__0__Impl1193 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__1__Impl_in_rule__ProductDeclaration__Group__11224 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__2_in_rule__ProductDeclaration__Group__11227 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__NameAssignment_1_in_rule__ProductDeclaration__Group__1__Impl1254 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__2__Impl_in_rule__ProductDeclaration__Group__21284 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__3_in_rule__ProductDeclaration__Group__21287 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__ProductDeclaration__Group__2__Impl1315 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__3__Impl_in_rule__ProductDeclaration__Group__31346 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__4_in_rule__ProductDeclaration__Group__31349 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__ProductDeclaration__Group__3__Impl1377 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__4__Impl_in_rule__ProductDeclaration__Group__41408 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__5_in_rule__ProductDeclaration__Group__41411 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_rule__ProductDeclaration__Group__4__Impl1439 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__5__Impl_in_rule__ProductDeclaration__Group__51470 = new BitSet(new long[]{0x0000000000008010L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__6_in_rule__ProductDeclaration__Group__51473 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__ProductDeclaration__Group__5__Impl1501 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__6__Impl_in_rule__ProductDeclaration__Group__61532 = new BitSet(new long[]{0x0000000000008010L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__7_in_rule__ProductDeclaration__Group__61535 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__FeaturesAssignment_6_in_rule__ProductDeclaration__Group__6__Impl1562 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__7__Impl_in_rule__ProductDeclaration__Group__71593 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__8_in_rule__ProductDeclaration__Group__71596 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__ProductDeclaration__Group__7__Impl1624 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__8__Impl_in_rule__ProductDeclaration__Group__81655 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__9_in_rule__ProductDeclaration__Group__81658 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__ProductDeclaration__Group__8__Impl1686 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__9__Impl_in_rule__ProductDeclaration__Group__91717 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__10_in_rule__ProductDeclaration__Group__91720 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_rule__ProductDeclaration__Group__9__Impl1748 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__10__Impl_in_rule__ProductDeclaration__Group__101779 = new BitSet(new long[]{0x0000000000008010L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__11_in_rule__ProductDeclaration__Group__101782 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__ProductDeclaration__Group__10__Impl1810 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__11__Impl_in_rule__ProductDeclaration__Group__111841 = new BitSet(new long[]{0x0000000000008010L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__12_in_rule__ProductDeclaration__Group__111844 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__DeltasAssignment_11_in_rule__ProductDeclaration__Group__11__Impl1871 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__12__Impl_in_rule__ProductDeclaration__Group__121902 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__13_in_rule__ProductDeclaration__Group__121905 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__ProductDeclaration__Group__12__Impl1933 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__13__Impl_in_rule__ProductDeclaration__Group__131964 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__14_in_rule__ProductDeclaration__Group__131967 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__ProductDeclaration__Group__13__Impl1995 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__14__Impl_in_rule__ProductDeclaration__Group__142026 = new BitSet(new long[]{0x0000000004408010L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__15_in_rule__ProductDeclaration__Group__142029 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__ProductDeclaration__Group__14__Impl2057 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__15__Impl_in_rule__ProductDeclaration__Group__152088 = new BitSet(new long[]{0x0000000004408010L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__16_in_rule__ProductDeclaration__Group__152091 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__ConstraintsAssignment_15_in_rule__ProductDeclaration__Group__15__Impl2118 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__16__Impl_in_rule__ProductDeclaration__Group__162149 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__17_in_rule__ProductDeclaration__Group__162152 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__ProductDeclaration__Group__16__Impl2180 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__17__Impl_in_rule__ProductDeclaration__Group__172211 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__18_in_rule__ProductDeclaration__Group__172214 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rule__ProductDeclaration__Group__17__Impl2242 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__18__Impl_in_rule__ProductDeclaration__Group__182273 = new BitSet(new long[]{0x0000000000009000L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__19_in_rule__ProductDeclaration__Group__182276 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__ProductDeclaration__Group__18__Impl2304 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__19__Impl_in_rule__ProductDeclaration__Group__192335 = new BitSet(new long[]{0x0000000000009000L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__20_in_rule__ProductDeclaration__Group__192338 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__PartitionsAssignment_19_in_rule__ProductDeclaration__Group__19__Impl2365 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__20__Impl_in_rule__ProductDeclaration__Group__202396 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__21_in_rule__ProductDeclaration__Group__202399 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__ProductDeclaration__Group__20__Impl2427 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__21__Impl_in_rule__ProductDeclaration__Group__212458 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__22_in_rule__ProductDeclaration__Group__212461 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_rule__ProductDeclaration__Group__21__Impl2489 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__22__Impl_in_rule__ProductDeclaration__Group__222520 = new BitSet(new long[]{0x0000000000008010L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__23_in_rule__ProductDeclaration__Group__222523 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__ProductDeclaration__Group__22__Impl2551 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__23__Impl_in_rule__ProductDeclaration__Group__232582 = new BitSet(new long[]{0x0000000000008010L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__24_in_rule__ProductDeclaration__Group__232585 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__ProductsAssignment_23_in_rule__ProductDeclaration__Group__23__Impl2612 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__24__Impl_in_rule__ProductDeclaration__Group__242643 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__25_in_rule__ProductDeclaration__Group__242646 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__ProductDeclaration__Group__24__Impl2674 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ProductDeclaration__Group__25__Impl_in_rule__ProductDeclaration__Group__252705 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__ProductDeclaration__Group__25__Impl2733 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Product__Group__0__Impl_in_rule__Product__Group__02816 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_rule__Product__Group__1_in_rule__Product__Group__02819 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Product__NameAssignment_0_in_rule__Product__Group__0__Impl2846 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Product__Group__1__Impl_in_rule__Product__Group__12876 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_rule__Product__Group__2_in_rule__Product__Group__12879 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_rule__Product__Group__1__Impl2907 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Product__Group__2__Impl_in_rule__Product__Group__22938 = new BitSet(new long[]{0x0000000000008010L});
    public static final BitSet FOLLOW_rule__Product__Group__3_in_rule__Product__Group__22941 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__Product__Group__2__Impl2969 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Product__Group__3__Impl_in_rule__Product__Group__33000 = new BitSet(new long[]{0x0000000000008010L});
    public static final BitSet FOLLOW_rule__Product__Group__4_in_rule__Product__Group__33003 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Product__ListAssignment_3_in_rule__Product__Group__3__Impl3030 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_rule__Product__Group__4__Impl_in_rule__Product__Group__43061 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_rule__Product__Group__5_in_rule__Product__Group__43064 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__Product__Group__4__Impl3092 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Product__Group__5__Impl_in_rule__Product__Group__53123 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_rule__Product__Group__5__Impl3151 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Partition__Group__0__Impl_in_rule__Partition__Group__03194 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_rule__Partition__Group__1_in_rule__Partition__Group__03197 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Partition__ConstrAssignment_0_in_rule__Partition__Group__0__Impl3226 = new BitSet(new long[]{0x0000000000001002L});
    public static final BitSet FOLLOW_rule__Partition__ConstrAssignment_0_in_rule__Partition__Group__0__Impl3238 = new BitSet(new long[]{0x0000000000001002L});
    public static final BitSet FOLLOW_rule__Partition__Group__1__Impl_in_rule__Partition__Group__13271 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_rule__Partition__Group__1__Impl3299 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Constraint__Group__0__Impl_in_rule__Constraint__Group__03334 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Constraint__Group__1_in_rule__Constraint__Group__03337 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__Constraint__Group__0__Impl3365 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Constraint__Group__1__Impl_in_rule__Constraint__Group__13396 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_rule__Constraint__Group__2_in_rule__Constraint__Group__13399 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Constraint__DeltaAssignment_1_in_rule__Constraint__Group__1__Impl3426 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Constraint__Group__2__Impl_in_rule__Constraint__Group__23456 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_rule__Constraint__Group__3_in_rule__Constraint__Group__23459 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__Constraint__Group__2__Impl3487 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Constraint__Group__3__Impl_in_rule__Constraint__Group__33518 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_rule__Constraint__Group__4_in_rule__Constraint__Group__33521 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_rule__Constraint__Group__3__Impl3549 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Constraint__Group__4__Impl_in_rule__Constraint__Group__43580 = new BitSet(new long[]{0x0000000004400010L});
    public static final BitSet FOLLOW_rule__Constraint__Group__5_in_rule__Constraint__Group__43583 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_rule__Constraint__Group__4__Impl3611 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Constraint__Group__5__Impl_in_rule__Constraint__Group__53642 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_rule__Constraint__Group__6_in_rule__Constraint__Group__53645 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Constraint__ExprAssignment_5_in_rule__Constraint__Group__5__Impl3672 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Constraint__Group__6__Impl_in_rule__Constraint__Group__63702 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_rule__Constraint__Group__7_in_rule__Constraint__Group__63705 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_rule__Constraint__Group__6__Impl3733 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Constraint__Group__7__Impl_in_rule__Constraint__Group__73764 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Constraint__LastAssignment_7_in_rule__Constraint__Group__7__Impl3791 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ListElem__Group__0__Impl_in_rule__ListElem__Group__03838 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_rule__ListElem__Group__1_in_rule__ListElem__Group__03841 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ListElem__ElemAssignment_0_in_rule__ListElem__Group__0__Impl3868 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ListElem__Group__1__Impl_in_rule__ListElem__Group__13898 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ListElem__LastAssignment_1_in_rule__ListElem__Group__1__Impl3925 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Core__Group__0__Impl_in_rule__Core__Group__03960 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_rule__Core__Group__1_in_rule__Core__Group__03963 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Core__ExprAssignment_0_in_rule__Core__Group__0__Impl3990 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Core__Group__1__Impl_in_rule__Core__Group__14020 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_rule__Core__Group__1__Impl4048 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Or__Group__0__Impl_in_rule__Or__Group__04083 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_rule__Or__Group__1_in_rule__Or__Group__04086 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAnd_in_rule__Or__Group__0__Impl4113 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Or__Group__1__Impl_in_rule__Or__Group__14142 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Or__Group_1__0_in_rule__Or__Group__1__Impl4169 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Or__Group_1__0__Impl_in_rule__Or__Group_1__04204 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_rule__Or__Group_1__1_in_rule__Or__Group_1__04207 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Or__Group_1__1__Impl_in_rule__Or__Group_1__14265 = new BitSet(new long[]{0x0000000004400010L});
    public static final BitSet FOLLOW_rule__Or__Group_1__2_in_rule__Or__Group_1__14268 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_rule__Or__Group_1__1__Impl4296 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Or__Group_1__2__Impl_in_rule__Or__Group_1__24327 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Or__RightAssignment_1_2_in_rule__Or__Group_1__2__Impl4354 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__And__Group__0__Impl_in_rule__And__Group__04390 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_rule__And__Group__1_in_rule__And__Group__04393 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePrimary_in_rule__And__Group__0__Impl4420 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__And__Group__1__Impl_in_rule__And__Group__14449 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__And__Group_1__0_in_rule__And__Group__1__Impl4476 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__And__Group_1__0__Impl_in_rule__And__Group_1__04511 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_rule__And__Group_1__1_in_rule__And__Group_1__04514 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__And__Group_1__1__Impl_in_rule__And__Group_1__14572 = new BitSet(new long[]{0x0000000004400010L});
    public static final BitSet FOLLOW_rule__And__Group_1__2_in_rule__And__Group_1__14575 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_25_in_rule__And__Group_1__1__Impl4603 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__And__Group_1__2__Impl_in_rule__And__Group_1__24634 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__And__RightAssignment_1_2_in_rule__And__Group_1__2__Impl4661 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Primary__Group_1__0__Impl_in_rule__Primary__Group_1__04697 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_rule__Primary__Group_1__1_in_rule__Primary__Group_1__04700 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Primary__Group_1__1__Impl_in_rule__Primary__Group_1__14758 = new BitSet(new long[]{0x0000000004400010L});
    public static final BitSet FOLLOW_rule__Primary__Group_1__2_in_rule__Primary__Group_1__14761 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_rule__Primary__Group_1__1__Impl4789 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Primary__Group_1__2__Impl_in_rule__Primary__Group_1__24820 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_rule__Primary__Group_1__3_in_rule__Primary__Group_1__24823 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Primary__ExprAssignment_1_2_in_rule__Primary__Group_1__2__Impl4850 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Primary__Group_1__3__Impl_in_rule__Primary__Group_1__34880 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_rule__Primary__Group_1__3__Impl4908 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Primary__Group_2__0__Impl_in_rule__Primary__Group_2__04947 = new BitSet(new long[]{0x0000000004400010L});
    public static final BitSet FOLLOW_rule__Primary__Group_2__1_in_rule__Primary__Group_2__04950 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Primary__Group_2__1__Impl_in_rule__Primary__Group_2__15008 = new BitSet(new long[]{0x0000000004400010L});
    public static final BitSet FOLLOW_rule__Primary__Group_2__2_in_rule__Primary__Group_2__15011 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_26_in_rule__Primary__Group_2__1__Impl5039 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Primary__Group_2__2__Impl_in_rule__Primary__Group_2__25070 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Primary__ValueAssignment_2_2_in_rule__Primary__Group_2__2__Impl5097 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__ProductDeclaration__NameAssignment_15138 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFeatures_in_rule__ProductDeclaration__FeaturesAssignment_65169 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDeltas_in_rule__ProductDeclaration__DeltasAssignment_115200 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCore_in_rule__ProductDeclaration__ConstraintsAssignment_155231 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePartitions_in_rule__ProductDeclaration__PartitionsAssignment_195262 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleProducts_in_rule__ProductDeclaration__ProductsAssignment_235293 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleListElem_in_rule__Features__ListAssignment5324 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleListElem_in_rule__Deltas__ListAssignment5355 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleProduct_in_rule__Products__ListAssignment5386 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Product__NameAssignment_05417 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleListElem_in_rule__Product__ListAssignment_35448 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePartition_in_rule__Partitions__ListAssignment5479 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleConstraint_in_rule__Partition__ConstrAssignment_05510 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleElement_in_rule__Constraint__DeltaAssignment_15541 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExpression_in_rule__Constraint__ExprAssignment_55572 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_rule__Constraint__LastAssignment_75608 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleElement_in_rule__ListElem__ElemAssignment_05647 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_rule__ListElem__LastAssignment_15683 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Element__NameAssignment5722 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExpression_in_rule__Core__ExprAssignment_05753 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOr_in_rule__Or__RightAssignment_1_25784 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAnd_in_rule__And__RightAssignment_1_25815 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExpression_in_rule__Primary__ExprAssignment_1_25846 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePrimary_in_rule__Primary__ValueAssignment_2_25877 = new BitSet(new long[]{0x0000000000000002L});

}