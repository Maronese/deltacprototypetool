/**
 */
package org.xtext.dop.clang.clang.impl;

import org.eclipse.emf.ecore.EClass;

import org.xtext.dop.clang.clang.ClangPackage;
import org.xtext.dop.clang.clang.TypeQualifier;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Type Qualifier</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TypeQualifierImpl extends DeclarationSpecifierImpl implements TypeQualifier
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TypeQualifierImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ClangPackage.Literals.TYPE_QUALIFIER;
  }

} //TypeQualifierImpl
