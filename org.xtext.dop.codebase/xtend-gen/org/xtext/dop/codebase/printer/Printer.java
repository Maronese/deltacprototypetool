package org.xtext.dop.codebase.printer;

import com.google.common.base.Objects;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.xtext.dop.clang.clang.AdditiveExpression;
import org.xtext.dop.clang.clang.AlignmentSpecifier;
import org.xtext.dop.clang.clang.AndExpression;
import org.xtext.dop.clang.clang.ArrayPostfix;
import org.xtext.dop.clang.clang.AssignmentExpression;
import org.xtext.dop.clang.clang.AssignmentExpressionElement;
import org.xtext.dop.clang.clang.Atomic;
import org.xtext.dop.clang.clang.BaseTypeBool;
import org.xtext.dop.clang.clang.BaseTypeChar;
import org.xtext.dop.clang.clang.BaseTypeComplex;
import org.xtext.dop.clang.clang.BaseTypeDouble;
import org.xtext.dop.clang.clang.BaseTypeFloat;
import org.xtext.dop.clang.clang.BaseTypeImaginary;
import org.xtext.dop.clang.clang.BaseTypeInt;
import org.xtext.dop.clang.clang.BaseTypeLong;
import org.xtext.dop.clang.clang.BaseTypeShort;
import org.xtext.dop.clang.clang.BaseTypeSigned;
import org.xtext.dop.clang.clang.BaseTypeUnsigned;
import org.xtext.dop.clang.clang.BaseTypeVoid;
import org.xtext.dop.clang.clang.BlockItem;
import org.xtext.dop.clang.clang.CompleteName;
import org.xtext.dop.clang.clang.CompoundStatement;
import org.xtext.dop.clang.clang.ConditionalExpression;
import org.xtext.dop.clang.clang.Custom;
import org.xtext.dop.clang.clang.Declaration;
import org.xtext.dop.clang.clang.DeclarationSpecifier;
import org.xtext.dop.clang.clang.EnumType;
import org.xtext.dop.clang.clang.EqualityExpression;
import org.xtext.dop.clang.clang.ExclusiveOrExpression;
import org.xtext.dop.clang.clang.Expression;
import org.xtext.dop.clang.clang.ExpressionAlign;
import org.xtext.dop.clang.clang.ExpressionCast;
import org.xtext.dop.clang.clang.ExpressionComplex;
import org.xtext.dop.clang.clang.ExpressionDecrement;
import org.xtext.dop.clang.clang.ExpressionFloat;
import org.xtext.dop.clang.clang.ExpressionGenericSelection;
import org.xtext.dop.clang.clang.ExpressionIdentifier;
import org.xtext.dop.clang.clang.ExpressionIncrement;
import org.xtext.dop.clang.clang.ExpressionInitializer;
import org.xtext.dop.clang.clang.ExpressionInteger;
import org.xtext.dop.clang.clang.ExpressionSimple;
import org.xtext.dop.clang.clang.ExpressionSizeof;
import org.xtext.dop.clang.clang.ExpressionStatement;
import org.xtext.dop.clang.clang.ExpressionString;
import org.xtext.dop.clang.clang.ExpressionSubExpression;
import org.xtext.dop.clang.clang.ExpressionUnaryOp;
import org.xtext.dop.clang.clang.FileInclude;
import org.xtext.dop.clang.clang.FunctionInitializer;
import org.xtext.dop.clang.clang.FunctionSpecifierInline;
import org.xtext.dop.clang.clang.FunctionSpecifierNoReturn;
import org.xtext.dop.clang.clang.GenericAssociation;
import org.xtext.dop.clang.clang.GenericAssociationCase;
import org.xtext.dop.clang.clang.GenericAssociationDefault;
import org.xtext.dop.clang.clang.GenericDeclaration0;
import org.xtext.dop.clang.clang.GenericDeclaration1;
import org.xtext.dop.clang.clang.GenericDeclaration2;
import org.xtext.dop.clang.clang.GenericDeclarationElement;
import org.xtext.dop.clang.clang.GenericDeclarationElements;
import org.xtext.dop.clang.clang.GenericDeclarationId;
import org.xtext.dop.clang.clang.GenericDeclarationIdPostfix;
import org.xtext.dop.clang.clang.GenericDeclarationInitializer;
import org.xtext.dop.clang.clang.GenericDeclarationInnerId;
import org.xtext.dop.clang.clang.GenericSpecifiedDeclaration;
import org.xtext.dop.clang.clang.GlobalName;
import org.xtext.dop.clang.clang.GoToPrefixStatement;
import org.xtext.dop.clang.clang.InclusiveOrExpression;
import org.xtext.dop.clang.clang.IterationStatementDo;
import org.xtext.dop.clang.clang.IterationStatementFor;
import org.xtext.dop.clang.clang.IterationStatementWhile;
import org.xtext.dop.clang.clang.JumpStatementBreak;
import org.xtext.dop.clang.clang.JumpStatementContinue;
import org.xtext.dop.clang.clang.JumpStatementGoto;
import org.xtext.dop.clang.clang.JumpStatementReturn;
import org.xtext.dop.clang.clang.LabeledStatement;
import org.xtext.dop.clang.clang.LabeledStatementCase;
import org.xtext.dop.clang.clang.LabeledStatementDefault;
import org.xtext.dop.clang.clang.LogicalAndExpression;
import org.xtext.dop.clang.clang.LogicalOrExpression;
import org.xtext.dop.clang.clang.Model;
import org.xtext.dop.clang.clang.MultiplicativeExpression;
import org.xtext.dop.clang.clang.NextAsignement;
import org.xtext.dop.clang.clang.NextField;
import org.xtext.dop.clang.clang.NextParameter;
import org.xtext.dop.clang.clang.Parameter;
import org.xtext.dop.clang.clang.ParametersPostfix;
import org.xtext.dop.clang.clang.PostfixExpressionPostfix;
import org.xtext.dop.clang.clang.PostfixExpressionPostfixAccess;
import org.xtext.dop.clang.clang.PostfixExpressionPostfixAccessPtr;
import org.xtext.dop.clang.clang.PostfixExpressionPostfixArgumentList;
import org.xtext.dop.clang.clang.PostfixExpressionPostfixDecrement;
import org.xtext.dop.clang.clang.PostfixExpressionPostfixExpression;
import org.xtext.dop.clang.clang.PostfixExpressionPostfixIncrement;
import org.xtext.dop.clang.clang.RelationalExpression;
import org.xtext.dop.clang.clang.SelectionStatementIf;
import org.xtext.dop.clang.clang.SelectionStatementSwitch;
import org.xtext.dop.clang.clang.ShiftExpression;
import org.xtext.dop.clang.clang.SimpleName;
import org.xtext.dop.clang.clang.SimpleType;
import org.xtext.dop.clang.clang.Source;
import org.xtext.dop.clang.clang.SpecifiedType;
import org.xtext.dop.clang.clang.StandardInclude;
import org.xtext.dop.clang.clang.Statement;
import org.xtext.dop.clang.clang.StatementDeclaration;
import org.xtext.dop.clang.clang.StatementInnerStatement;
import org.xtext.dop.clang.clang.StaticAssertDeclaration;
import org.xtext.dop.clang.clang.StorageClassSpecifierAuto;
import org.xtext.dop.clang.clang.StorageClassSpecifierExtern;
import org.xtext.dop.clang.clang.StorageClassSpecifierRegister;
import org.xtext.dop.clang.clang.StorageClassSpecifierStatic;
import org.xtext.dop.clang.clang.StorageClassSpecifierThreadLocal;
import org.xtext.dop.clang.clang.StringExpression;
import org.xtext.dop.clang.clang.StructOrUnionType;
import org.xtext.dop.clang.clang.TypeAliasDeclaration;
import org.xtext.dop.clang.clang.TypeQualifierAtomic;
import org.xtext.dop.clang.clang.TypeQualifierConst;
import org.xtext.dop.clang.clang.TypeQualifierVolatile;
import org.xtext.dop.rappresentation.Resource;

@SuppressWarnings("all")
public class Printer {
  private int tab;
  
  private Resource resource;
  
  public Printer(final Resource resource) {
    this.resource = resource;
  }
  
  public String print() {
    final Model model = this.resource.getSource();
    EList<StandardInclude> _includesStd = model.getIncludesStd();
    final Function1<StandardInclude, String> _function = (StandardInclude it) -> {
      return this.print(it);
    };
    List<String> _map = ListExtensions.<StandardInclude, String>map(_includesStd, _function);
    String res = IterableExtensions.join(_map, "\n");
    String _res = res;
    EList<FileInclude> _includesFile = model.getIncludesFile();
    final Function1<FileInclude, String> _function_1 = (FileInclude it) -> {
      return this.print(it);
    };
    List<String> _map_1 = ListExtensions.<FileInclude, String>map(_includesFile, _function_1);
    String _join = IterableExtensions.join(_map_1, "\n");
    String _plus = ("\n\n" + _join);
    res = (_res + _plus);
    String _res_1 = res;
    String _name = this.resource.getName();
    String _plus_1 = ("#include \"" + _name);
    String _plus_2 = (_plus_1 + "_prototypes.h\"");
    res = (_res_1 + _plus_2);
    String _res_2 = res;
    EList<Declaration> _declarations = model.getDeclarations();
    final Function1<Declaration, String> _function_2 = (Declaration it) -> {
      return this.print(it);
    };
    List<String> _map_2 = ListExtensions.<Declaration, String>map(_declarations, _function_2);
    String _join_1 = IterableExtensions.join(_map_2, "\n");
    String _plus_3 = ("\n\n" + _join_1);
    res = (_res_2 + _plus_3);
    return res;
  }
  
  public String print(final StandardInclude include) {
    Source _std = include.getStd();
    String _print = this.print(_std);
    String _plus = ("#include <" + _print);
    return (_plus + ">");
  }
  
  public String print(final FileInclude include) {
    String _file = include.getFile();
    return ("#include " + _file);
  }
  
  public String print(final Source source) {
    String _switchResult = null;
    boolean _matched = false;
    if (!_matched) {
      if (source instanceof SimpleName) {
        _matched=true;
        _switchResult = ((SimpleName)source).getName();
      }
    }
    if (!_matched) {
      if (source instanceof CompleteName) {
        _matched=true;
        String _name = ((CompleteName)source).getName();
        String _plus = (_name + ".");
        String _ext = ((CompleteName) source).getExt();
        _switchResult = (_plus + _ext);
      }
    }
    return _switchResult;
  }
  
  public String print(final Declaration decl) {
    String _switchResult = null;
    boolean _matched = false;
    if (!_matched) {
      if (decl instanceof GenericSpecifiedDeclaration) {
        _matched=true;
        final GenericSpecifiedDeclaration item = ((GenericSpecifiedDeclaration) decl);
        String res = "";
        EList<DeclarationSpecifier> _specs = item.getSpecs();
        boolean _isEmpty = _specs.isEmpty();
        boolean _not = (!_isEmpty);
        if (_not) {
          String _res = res;
          EList<DeclarationSpecifier> _specs_1 = item.getSpecs();
          final Function1<DeclarationSpecifier, String> _function = (DeclarationSpecifier it) -> {
            return this.print(it);
          };
          List<String> _map = ListExtensions.<DeclarationSpecifier, String>map(_specs_1, _function);
          String _join = IterableExtensions.join(_map, " ");
          String _plus = (_join + " ");
          res = (_res + _plus);
        }
        EList<SimpleType> _type = item.getType();
        final Function1<SimpleType, String> _function_1 = (SimpleType it) -> {
          return this.print(it);
        };
        List<String> _map_1 = ListExtensions.<SimpleType, String>map(_type, _function_1);
        String _join_1 = IterableExtensions.join(_map_1, " ");
        String _plus_1 = (res + _join_1);
        String _plus_2 = (_plus_1 + " ");
        GenericDeclarationElements _decl = item.getDecl();
        String _print = this.print(_decl);
        String _plus_3 = (_plus_2 + _print);
        return (_plus_3 + ";");
      }
    }
    if (!_matched) {
      if (decl instanceof TypeAliasDeclaration) {
        _matched=true;
        String _xblockexpression = null;
        {
          final TypeAliasDeclaration item = ((TypeAliasDeclaration) decl);
          SpecifiedType _tdecl = item.getTdecl();
          String _print = this.print(_tdecl);
          String _plus = ("typedef " + _print);
          _xblockexpression = (_plus + ";");
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (decl instanceof StaticAssertDeclaration) {
        _matched=true;
        String _xblockexpression = null;
        {
          final StaticAssertDeclaration item = ((StaticAssertDeclaration) decl);
          Expression _exp = item.getExp();
          String _print = this.print(_exp);
          String _plus = ("_Static_assert (" + _print);
          String _plus_1 = (_plus + ", ");
          String _string = item.getString();
          String _plus_2 = (_plus_1 + _string);
          _xblockexpression = (_plus_2 + ");");
        }
        _switchResult = _xblockexpression;
      }
    }
    return _switchResult;
  }
  
  public String print(final GenericDeclarationElements gen) {
    EList<GenericDeclarationElement> _els = gen.getEls();
    final Function1<GenericDeclarationElement, String> _function = (GenericDeclarationElement it) -> {
      return this.print(it);
    };
    List<String> _map = ListExtensions.<GenericDeclarationElement, String>map(_els, _function);
    final String res = IterableExtensions.join(_map, " ");
    GenericDeclarationElements _next = gen.getNext();
    boolean _notEquals = (!Objects.equal(_next, null));
    if (_notEquals) {
      GenericDeclarationElements _next_1 = gen.getNext();
      String _print = this.print(_next_1);
      return ((res + ", ") + _print);
    }
    return res;
  }
  
  public String print(final GenericDeclarationElement gen) {
    GenericDeclarationId _id = gen.getId();
    final String res = this.print(_id);
    GenericDeclarationInitializer _init = gen.getInit();
    boolean _notEquals = (!Objects.equal(_init, null));
    if (_notEquals) {
      GenericDeclarationInitializer _init_1 = gen.getInit();
      String _print = this.print(_init_1);
      return ((res + " ") + _print);
    } else {
      return res;
    }
  }
  
  public String print(final SpecifiedType spec) {
    Parameter _par = spec.getPar();
    return this.print(_par);
  }
  
  public String print(final GenericDeclarationId gen) {
    String _switchResult = null;
    boolean _matched = false;
    if (!_matched) {
      if (gen instanceof GenericDeclaration0) {
        _matched=true;
        String _xblockexpression = null;
        {
          final GenericDeclaration0 item = ((GenericDeclaration0) gen);
          EList<String> _pointers = item.getPointers();
          String _join = IterableExtensions.join(_pointers);
          String res = (_join + " ");
          String _restrict = item.getRestrict();
          boolean _notEquals = (!Objects.equal(_restrict, null));
          if (_notEquals) {
            String _res = res;
            String _restrict_1 = item.getRestrict();
            String _plus = (_restrict_1 + " ");
            res = (_res + _plus);
          }
          GenericDeclarationInnerId _inner = item.getInner();
          boolean _notEquals_1 = (!Objects.equal(_inner, null));
          if (_notEquals_1) {
            String _res_1 = res;
            GenericDeclarationInnerId _inner_1 = item.getInner();
            String _print = this.print(_inner_1);
            String _plus_1 = (_print + " ");
            res = (_res_1 + _plus_1);
          }
          EList<GenericDeclarationIdPostfix> _posts = item.getPosts();
          final Function1<GenericDeclarationIdPostfix, String> _function = (GenericDeclarationIdPostfix it) -> {
            return this.print(it);
          };
          List<String> _map = ListExtensions.<GenericDeclarationIdPostfix, String>map(_posts, _function);
          String _join_1 = IterableExtensions.join(_map);
          _xblockexpression = (res + _join_1);
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (gen instanceof GenericDeclaration1) {
        _matched=true;
        String _xblockexpression = null;
        {
          final GenericDeclaration1 item = ((GenericDeclaration1) gen);
          GenericDeclarationInnerId _inner = item.getInner();
          String _print = this.print(_inner);
          String _plus = (_print + " ");
          EList<GenericDeclarationIdPostfix> _posts = item.getPosts();
          final Function1<GenericDeclarationIdPostfix, String> _function = (GenericDeclarationIdPostfix it) -> {
            return this.print(it);
          };
          List<String> _map = ListExtensions.<GenericDeclarationIdPostfix, String>map(_posts, _function);
          String _join = IterableExtensions.join(_map);
          _xblockexpression = (_plus + _join);
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (gen instanceof GenericDeclaration2) {
        _matched=true;
        String _xblockexpression = null;
        {
          final GenericDeclaration2 item = ((GenericDeclaration2) gen);
          EList<GenericDeclarationIdPostfix> _posts = item.getPosts();
          final Function1<GenericDeclarationIdPostfix, String> _function = (GenericDeclarationIdPostfix it) -> {
            return this.print(it);
          };
          List<String> _map = ListExtensions.<GenericDeclarationIdPostfix, String>map(_posts, _function);
          String _join = IterableExtensions.join(_map);
          String _plus = (_join + " ");
          GenericDeclarationInnerId _inner = item.getInner();
          String _print = this.print(_inner);
          _xblockexpression = (_plus + _print);
        }
        _switchResult = _xblockexpression;
      }
    }
    return _switchResult;
  }
  
  public String print(final GenericDeclarationInnerId gen) {
    if ((gen instanceof GlobalName)) {
      return ((GlobalName)gen).getName();
    } else {
      GenericDeclarationId _inner = gen.getInner();
      String _print = this.print(_inner);
      String _plus = ("(" + _print);
      return (_plus + ")");
    }
  }
  
  public String print(final GenericDeclarationIdPostfix gen) {
    ArrayPostfix _array = gen.getArray();
    boolean _notEquals = (!Objects.equal(_array, null));
    if (_notEquals) {
      ArrayPostfix _array_1 = gen.getArray();
      return this.print(_array_1);
    } else {
      ParametersPostfix _function = gen.getFunction();
      return this.print(_function);
    }
  }
  
  public String print(final ArrayPostfix arr) {
    String res = "[";
    String _star = arr.getStar();
    boolean _notEquals = (!Objects.equal(_star, null));
    if (_notEquals) {
      String _res = res;
      String _star_1 = arr.getStar();
      res = (_res + _star_1);
    }
    Expression _exp = arr.getExp();
    boolean _notEquals_1 = (!Objects.equal(_exp, null));
    if (_notEquals_1) {
      Expression _exp_1 = arr.getExp();
      String _print = this.print(_exp_1);
      String _plus = (res + _print);
      return (_plus + "]");
    } else {
      return (res + "]");
    }
  }
  
  public String print(final ParametersPostfix par) {
    String ret = "(";
    EList<Parameter> _els = par.getEls();
    boolean _isEmpty = _els.isEmpty();
    boolean _not = (!_isEmpty);
    if (_not) {
      String _ret = ret;
      EList<Parameter> _els_1 = par.getEls();
      final Function1<Parameter, String> _function = (Parameter it) -> {
        return this.print(it);
      };
      List<String> _map = ListExtensions.<Parameter, String>map(_els_1, _function);
      String _join = IterableExtensions.join(_map, " ");
      ret = (_ret + _join);
    }
    EList<NextParameter> _next = par.getNext();
    boolean _isEmpty_1 = _next.isEmpty();
    boolean _not_1 = (!_isEmpty_1);
    if (_not_1) {
      String _ret_1 = ret;
      EList<NextParameter> _next_1 = par.getNext();
      final Function1<NextParameter, String> _function_1 = (NextParameter it) -> {
        return this.print(it);
      };
      List<String> _map_1 = ListExtensions.<NextParameter, String>map(_next_1, _function_1);
      String _join_1 = IterableExtensions.join(_map_1);
      ret = (_ret_1 + _join_1);
    }
    String _more = par.getMore();
    boolean _notEquals = (!Objects.equal(_more, null));
    if (_notEquals) {
      String _ret_2 = ret;
      ret = (_ret_2 + "...");
    }
    return (ret + ")");
  }
  
  public String print(final NextParameter next) {
    EList<Parameter> _els = next.getEls();
    final Function1<Parameter, String> _function = (Parameter it) -> {
      return this.print(it);
    };
    List<String> _map = ListExtensions.<Parameter, String>map(_els, _function);
    String _join = IterableExtensions.join(_map);
    return (", " + _join);
  }
  
  public String print(final Parameter par) {
    EList<DeclarationSpecifier> _specs = par.getSpecs();
    final Function1<DeclarationSpecifier, String> _function = (DeclarationSpecifier it) -> {
      return this.print(it);
    };
    List<String> _map = ListExtensions.<DeclarationSpecifier, String>map(_specs, _function);
    String _join = IterableExtensions.join(_map, " ");
    String _plus = (_join + " ");
    EList<SimpleType> _type = par.getType();
    final Function1<SimpleType, String> _function_1 = (SimpleType it) -> {
      return this.print(it);
    };
    List<String> _map_1 = ListExtensions.<SimpleType, String>map(_type, _function_1);
    String _join_1 = IterableExtensions.join(_map_1, " ");
    String res = (_plus + _join_1);
    GenericDeclarationId _id = par.getId();
    boolean _notEquals = (!Objects.equal(_id, null));
    if (_notEquals) {
      GenericDeclarationId _id_1 = par.getId();
      String _print = this.print(_id_1);
      return ((res + " ") + _print);
    } else {
      return res;
    }
  }
  
  public String print(final GenericDeclarationInitializer gen) {
    String _switchResult = null;
    boolean _matched = false;
    if (!_matched) {
      if (gen instanceof ExpressionInitializer) {
        _matched=true;
        String _xblockexpression = null;
        {
          final ExpressionInitializer item = ((ExpressionInitializer) gen);
          Expression _exp = item.getExp();
          String _print = this.print(_exp);
          _xblockexpression = (" = " + _print);
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (gen instanceof FunctionInitializer) {
        _matched=true;
        final FunctionInitializer item = ((FunctionInitializer) gen);
        CompoundStatement _block = item.getBlock();
        boolean _notEquals = (!Objects.equal(_block, null));
        if (_notEquals) {
          CompoundStatement _block_1 = item.getBlock();
          String _print = this.print(_block_1);
          return (" " + _print);
        } else {
          return "";
        }
      }
    }
    return _switchResult;
  }
  
  public String print(final SimpleType type) {
    String _switchResult = null;
    boolean _matched = false;
    if (!_matched) {
      if (type instanceof BaseTypeVoid) {
        _matched=true;
        _switchResult = "void";
      }
    }
    if (!_matched) {
      if (type instanceof Custom) {
        _matched=true;
        String _xblockexpression = null;
        {
          final Custom item = ((Custom) type);
          String _id = item.getId();
          _xblockexpression = _id.substring(1);
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (type instanceof BaseTypeChar) {
        _matched=true;
        _switchResult = "char";
      }
    }
    if (!_matched) {
      if (type instanceof BaseTypeShort) {
        _matched=true;
        _switchResult = "shot";
      }
    }
    if (!_matched) {
      if (type instanceof BaseTypeInt) {
        _matched=true;
        _switchResult = "int";
      }
    }
    if (!_matched) {
      if (type instanceof BaseTypeLong) {
        _matched=true;
        _switchResult = "long";
      }
    }
    if (!_matched) {
      if (type instanceof BaseTypeFloat) {
        _matched=true;
        _switchResult = "float";
      }
    }
    if (!_matched) {
      if (type instanceof BaseTypeDouble) {
        _matched=true;
        _switchResult = "double";
      }
    }
    if (!_matched) {
      if (type instanceof BaseTypeSigned) {
        _matched=true;
        _switchResult = "signed";
      }
    }
    if (!_matched) {
      if (type instanceof BaseTypeUnsigned) {
        _matched=true;
        _switchResult = "unsigned";
      }
    }
    if (!_matched) {
      if (type instanceof BaseTypeBool) {
        _matched=true;
        _switchResult = "_Bool";
      }
    }
    if (!_matched) {
      if (type instanceof BaseTypeComplex) {
        _matched=true;
        _switchResult = "_Complex";
      }
    }
    if (!_matched) {
      if (type instanceof BaseTypeImaginary) {
        _matched=true;
        _switchResult = "_Immaginary";
      }
    }
    if (!_matched) {
      if (type instanceof StructOrUnionType) {
        _matched=true;
        String _xblockexpression = null;
        {
          final StructOrUnionType item = ((StructOrUnionType) type);
          String _kind = item.getKind();
          String res = (_kind + " ");
          EList<GenericSpecifiedDeclaration> _content = item.getContent();
          boolean _equals = Objects.equal(_content, null);
          if (_equals) {
            String _name = item.getName();
            return (res + _name);
          }
          String _name_1 = item.getName();
          boolean _notEquals = (!Objects.equal(_name_1, null));
          if (_notEquals) {
            String _res = res;
            String _name_2 = item.getName();
            String _plus = (_name_2 + " ");
            res = (_res + _plus);
          }
          EList<GenericSpecifiedDeclaration> _content_1 = item.getContent();
          final Function1<GenericSpecifiedDeclaration, String> _function = (GenericSpecifiedDeclaration it) -> {
            return this.print(it);
          };
          List<String> _map = ListExtensions.<GenericSpecifiedDeclaration, String>map(_content_1, _function);
          String _join = IterableExtensions.join(_map);
          String _plus_1 = ("{\n" + _join);
          _xblockexpression = (_plus_1 + "\n}\n");
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (type instanceof EnumType) {
        _matched=true;
        String _xblockexpression = null;
        {
          final EnumType item = ((EnumType) type);
          String res = "enum ";
          _xblockexpression = "";
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (type instanceof Atomic) {
        _matched=true;
        String _xblockexpression = null;
        {
          final Atomic item = ((Atomic) type);
          SimpleType _type = item.getType();
          String _plus = ("_Atomic(" + _type);
          _xblockexpression = (_plus + ")");
        }
        _switchResult = _xblockexpression;
      }
    }
    return _switchResult;
  }
  
  public String print(final DeclarationSpecifier spec) {
    String _switchResult = null;
    boolean _matched = false;
    if (!_matched) {
      if (spec instanceof StorageClassSpecifierExtern) {
        _matched=true;
        _switchResult = "extern";
      }
    }
    if (!_matched) {
      if (spec instanceof StorageClassSpecifierStatic) {
        _matched=true;
        _switchResult = "static";
      }
    }
    if (!_matched) {
      if (spec instanceof StorageClassSpecifierThreadLocal) {
        _matched=true;
        _switchResult = "_Thread_local";
      }
    }
    if (!_matched) {
      if (spec instanceof StorageClassSpecifierAuto) {
        _matched=true;
        _switchResult = "auto";
      }
    }
    if (!_matched) {
      if (spec instanceof StorageClassSpecifierRegister) {
        _matched=true;
        _switchResult = "register";
      }
    }
    if (!_matched) {
      if (spec instanceof TypeQualifierConst) {
        _matched=true;
        _switchResult = "const";
      }
    }
    if (!_matched) {
      if (spec instanceof TypeQualifierVolatile) {
        _matched=true;
        _switchResult = "volatile";
      }
    }
    if (!_matched) {
      if (spec instanceof TypeQualifierAtomic) {
        _matched=true;
        _switchResult = "_Atomic";
      }
    }
    if (!_matched) {
      if (spec instanceof FunctionSpecifierInline) {
        _matched=true;
        _switchResult = "inline";
      }
    }
    if (!_matched) {
      if (spec instanceof FunctionSpecifierNoReturn) {
        _matched=true;
        _switchResult = "_Noreturn";
      }
    }
    if (!_matched) {
      if (spec instanceof AlignmentSpecifier) {
        _matched=true;
        final AlignmentSpecifier item = ((AlignmentSpecifier) spec);
        SimpleType _type = item.getType();
        boolean _notEquals = (!Objects.equal(_type, null));
        if (_notEquals) {
          SimpleType _type_1 = item.getType();
          String _print = this.print(_type_1);
          String _plus = ("_Alignas(" + _print);
          return (_plus + ")");
        } else {
          Expression _exp = item.getExp();
          String _print_1 = this.print(_exp);
          String _plus_1 = ("_Alignas(" + _print_1);
          return (_plus_1 + ")");
        }
      }
    }
    return _switchResult;
  }
  
  public String print(final Expression expr) {
    String _switchResult = null;
    boolean _matched = false;
    if (!_matched) {
      if (expr instanceof AssignmentExpression) {
        _matched=true;
        Expression _exp = ((AssignmentExpression)expr).getExp();
        String _print = this.print(_exp);
        EList<AssignmentExpressionElement> _list = ((AssignmentExpression)expr).getList();
        final Function1<AssignmentExpressionElement, String> _function = (AssignmentExpressionElement it) -> {
          return this.print(it);
        };
        List<String> _map = ListExtensions.<AssignmentExpressionElement, String>map(_list, _function);
        String _join = IterableExtensions.join(_map);
        _switchResult = (_print + _join);
      }
    }
    if (!_matched) {
      if (expr instanceof ConditionalExpression) {
        _matched=true;
        Expression _if = ((ConditionalExpression)expr).getIf();
        String _print = this.print(_if);
        String _plus = (_print + "? ");
        Expression _yes = ((ConditionalExpression)expr).getYes();
        String _print_1 = this.print(_yes);
        String _plus_1 = (_plus + _print_1);
        String _plus_2 = (_plus_1 + ": ");
        Expression _no = ((ConditionalExpression)expr).getNo();
        String _print_2 = this.print(_no);
        _switchResult = (_plus_2 + _print_2);
      }
    }
    if (!_matched) {
      if (expr instanceof LogicalOrExpression) {
        _matched=true;
        Expression _left = ((LogicalOrExpression)expr).getLeft();
        String _print = this.print(_left);
        String _plus = (_print + "||");
        Expression _right = ((LogicalOrExpression)expr).getRight();
        String _print_1 = this.print(_right);
        _switchResult = (_plus + _print_1);
      }
    }
    if (!_matched) {
      if (expr instanceof LogicalAndExpression) {
        _matched=true;
        Expression _left = ((LogicalAndExpression)expr).getLeft();
        String _print = this.print(_left);
        String _plus = (_print + "&&");
        Expression _right = ((LogicalAndExpression)expr).getRight();
        String _print_1 = this.print(_right);
        _switchResult = (_plus + _print_1);
      }
    }
    if (!_matched) {
      if (expr instanceof InclusiveOrExpression) {
        _matched=true;
        Expression _left = ((InclusiveOrExpression)expr).getLeft();
        String _print = this.print(_left);
        String _plus = (_print + "|");
        Expression _right = ((InclusiveOrExpression)expr).getRight();
        String _print_1 = this.print(_right);
        _switchResult = (_plus + _print_1);
      }
    }
    if (!_matched) {
      if (expr instanceof ExclusiveOrExpression) {
        _matched=true;
        Expression _left = ((ExclusiveOrExpression)expr).getLeft();
        String _print = this.print(_left);
        String _plus = (_print + "^");
        Expression _right = ((ExclusiveOrExpression)expr).getRight();
        String _print_1 = this.print(_right);
        _switchResult = (_plus + _print_1);
      }
    }
    if (!_matched) {
      if (expr instanceof AndExpression) {
        _matched=true;
        Expression _left = ((AndExpression)expr).getLeft();
        String _print = this.print(_left);
        String _plus = (_print + "&");
        Expression _right = ((AndExpression)expr).getRight();
        String _print_1 = this.print(_right);
        _switchResult = (_plus + _print_1);
      }
    }
    if (!_matched) {
      if (expr instanceof EqualityExpression) {
        _matched=true;
        Expression _left = ((EqualityExpression)expr).getLeft();
        String _print = this.print(_left);
        String _operand = ((EqualityExpression)expr).getOperand();
        String _plus = (_print + _operand);
        Expression _right = ((EqualityExpression)expr).getRight();
        String _print_1 = this.print(_right);
        _switchResult = (_plus + _print_1);
      }
    }
    if (!_matched) {
      if (expr instanceof RelationalExpression) {
        _matched=true;
        Expression _left = ((RelationalExpression)expr).getLeft();
        String _print = this.print(_left);
        String _operand = ((RelationalExpression)expr).getOperand();
        String _plus = (_print + _operand);
        Expression _right = ((RelationalExpression)expr).getRight();
        String _print_1 = this.print(_right);
        _switchResult = (_plus + _print_1);
      }
    }
    if (!_matched) {
      if (expr instanceof ShiftExpression) {
        _matched=true;
        Expression _left = ((ShiftExpression)expr).getLeft();
        String _print = this.print(_left);
        String _operand = ((ShiftExpression)expr).getOperand();
        String _plus = (_print + _operand);
        Expression _right = ((ShiftExpression)expr).getRight();
        String _print_1 = this.print(_right);
        _switchResult = (_plus + _print_1);
      }
    }
    if (!_matched) {
      if (expr instanceof AdditiveExpression) {
        _matched=true;
        Expression _left = ((AdditiveExpression)expr).getLeft();
        String _print = this.print(_left);
        String _operand = ((AdditiveExpression)expr).getOperand();
        String _plus = (_print + _operand);
        Expression _right = ((AdditiveExpression)expr).getRight();
        String _print_1 = this.print(_right);
        _switchResult = (_plus + _print_1);
      }
    }
    if (!_matched) {
      if (expr instanceof MultiplicativeExpression) {
        _matched=true;
        Expression _left = ((MultiplicativeExpression)expr).getLeft();
        String _print = this.print(_left);
        String _operand = ((MultiplicativeExpression)expr).getOperand();
        String _plus = (_print + _operand);
        Expression _right = ((MultiplicativeExpression)expr).getRight();
        String _print_1 = this.print(_right);
        _switchResult = (_plus + _print_1);
      }
    }
    if (!_matched) {
      if (expr instanceof ExpressionSimple) {
        _matched=true;
        String _xblockexpression = null;
        {
          final ExpressionSimple item = ((ExpressionSimple) expr);
          Expression _exp = item.getExp();
          String _print = this.print(_exp);
          EList<PostfixExpressionPostfix> _next = item.getNext();
          final Function1<PostfixExpressionPostfix, String> _function = (PostfixExpressionPostfix it) -> {
            return this.print(it);
          };
          List<String> _map = ListExtensions.<PostfixExpressionPostfix, String>map(_next, _function);
          String _join = IterableExtensions.join(_map);
          _xblockexpression = (_print + _join);
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (expr instanceof ExpressionComplex) {
        _matched=true;
        String _xblockexpression = null;
        {
          final ExpressionComplex item = ((ExpressionComplex) expr);
          EList<Expression> _exps = item.getExps();
          final Function1<Expression, String> _function = (Expression it) -> {
            return this.print(it);
          };
          List<String> _map = ListExtensions.<Expression, String>map(_exps, _function);
          String _join = IterableExtensions.join(_map, ",");
          String _plus = ("{" + _join);
          String _plus_1 = (_plus + "}");
          EList<PostfixExpressionPostfix> _next = item.getNext();
          final Function1<PostfixExpressionPostfix, String> _function_1 = (PostfixExpressionPostfix it) -> {
            return this.print(it);
          };
          List<String> _map_1 = ListExtensions.<PostfixExpressionPostfix, String>map(_next, _function_1);
          String _join_1 = IterableExtensions.join(_map_1);
          _xblockexpression = (_plus_1 + _join_1);
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (expr instanceof ExpressionIncrement) {
        _matched=true;
        String _xblockexpression = null;
        {
          final ExpressionIncrement item = ((ExpressionIncrement) expr);
          Expression _content = item.getContent();
          String _print = this.print(_content);
          _xblockexpression = ("++" + _print);
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (expr instanceof ExpressionDecrement) {
        _matched=true;
        String _xblockexpression = null;
        {
          final ExpressionDecrement item = ((ExpressionDecrement) expr);
          Expression _content = item.getContent();
          String _print = this.print(_content);
          _xblockexpression = ("--" + _print);
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (expr instanceof ExpressionSizeof) {
        _matched=true;
        String _xblockexpression = null;
        {
          final ExpressionSizeof item = ((ExpressionSizeof) expr);
          Expression _content = item.getContent();
          String _print = this.print(_content);
          String _plus = ("sizeof(" + _print);
          SpecifiedType _type = item.getType();
          String _print_1 = this.print(_type);
          String _plus_1 = (_plus + _print_1);
          _xblockexpression = (_plus_1 + ")");
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (expr instanceof ExpressionUnaryOp) {
        _matched=true;
        String _xblockexpression = null;
        {
          final ExpressionUnaryOp item = ((ExpressionUnaryOp) expr);
          String _op = item.getOp();
          Expression _content = item.getContent();
          String _print = this.print(_content);
          _xblockexpression = (_op + _print);
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (expr instanceof ExpressionAlign) {
        _matched=true;
        String _xblockexpression = null;
        {
          final ExpressionAlign item = ((ExpressionAlign) expr);
          SpecifiedType _type = item.getType();
          String _print = this.print(_type);
          String _plus = ("alignof(" + _print);
          _xblockexpression = (_plus + ")");
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (expr instanceof ExpressionCast) {
        _matched=true;
        String _xblockexpression = null;
        {
          final ExpressionCast item = ((ExpressionCast) expr);
          SpecifiedType _type = item.getType();
          String _print = this.print(_type);
          String _plus = ("(" + _print);
          String _plus_1 = (_plus + ")");
          Expression _exp = item.getExp();
          String _print_1 = this.print(_exp);
          _xblockexpression = (_plus_1 + _print_1);
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (expr instanceof ExpressionIdentifier) {
        _matched=true;
        String _xblockexpression = null;
        {
          final ExpressionIdentifier item = ((ExpressionIdentifier) expr);
          _xblockexpression = item.getName();
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (expr instanceof ExpressionString) {
        _matched=true;
        String _xblockexpression = null;
        {
          final ExpressionString item = ((ExpressionString) expr);
          StringExpression _value = item.getValue();
          _xblockexpression = this.print(_value);
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (expr instanceof ExpressionInteger) {
        _matched=true;
        String _xblockexpression = null;
        {
          final ExpressionInteger item = ((ExpressionInteger) expr);
          _xblockexpression = item.getValue();
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (expr instanceof ExpressionFloat) {
        _matched=true;
        String _xblockexpression = null;
        {
          final ExpressionFloat item = ((ExpressionFloat) expr);
          _xblockexpression = item.getValue();
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (expr instanceof ExpressionSubExpression) {
        _matched=true;
        String _xblockexpression = null;
        {
          final ExpressionSubExpression item = ((ExpressionSubExpression) expr);
          Expression _par = item.getPar();
          String _print = this.print(_par);
          String _plus = ("(" + _print);
          _xblockexpression = (_plus + ")");
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (expr instanceof ExpressionGenericSelection) {
        _matched=true;
        String _xblockexpression = null;
        {
          final ExpressionGenericSelection item = ((ExpressionGenericSelection) expr);
          _xblockexpression = "";
        }
        _switchResult = _xblockexpression;
      }
    }
    return _switchResult;
  }
  
  public String print(final AssignmentExpressionElement expr) {
    String _op = expr.getOp();
    Expression _exp = expr.getExp();
    String _print = this.print(_exp);
    return (_op + _print);
  }
  
  public String print(final NextAsignement expr) {
    EList<Expression> _right = expr.getRight();
    final Function1<Expression, String> _function = (Expression it) -> {
      return this.print(it);
    };
    List<String> _map = ListExtensions.<Expression, String>map(_right, _function);
    return IterableExtensions.join(_map);
  }
  
  public String print(final StringExpression expr) {
    return expr.getName();
  }
  
  public String print(final GenericAssociation expr) {
    String _switchResult = null;
    boolean _matched = false;
    if (!_matched) {
      if (expr instanceof GenericAssociationCase) {
        _matched=true;
        String _xblockexpression = null;
        {
          final GenericAssociationCase item = ((GenericAssociationCase) expr);
          SpecifiedType _type = item.getType();
          String _print = this.print(_type);
          String _plus = (_print + ": ");
          Expression _exp = item.getExp();
          String _print_1 = this.print(_exp);
          _xblockexpression = (_plus + _print_1);
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (expr instanceof GenericAssociationDefault) {
        _matched=true;
        Expression _exp = ((GenericAssociationDefault)expr).getExp();
        String _print = this.print(_exp);
        _switchResult = ("default: " + _print);
      }
    }
    return _switchResult;
  }
  
  public String print(final PostfixExpressionPostfix expr) {
    String _switchResult = null;
    boolean _matched = false;
    if (!_matched) {
      if (expr instanceof PostfixExpressionPostfixExpression) {
        _matched=true;
        String _xblockexpression = null;
        {
          final PostfixExpressionPostfixExpression item = ((PostfixExpressionPostfixExpression) expr);
          Expression _value = item.getValue();
          String _print = this.print(_value);
          String _plus = ("[" + _print);
          _xblockexpression = (_plus + "]");
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (expr instanceof PostfixExpressionPostfixArgumentList) {
        _matched=true;
        String _xblockexpression = null;
        {
          final PostfixExpressionPostfixArgumentList item = ((PostfixExpressionPostfixArgumentList) expr);
          _xblockexpression = "";
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (expr instanceof PostfixExpressionPostfixAccess) {
        _matched=true;
        String _xblockexpression = null;
        {
          final PostfixExpressionPostfixAccess item = ((PostfixExpressionPostfixAccess) expr);
          String _value = item.getValue();
          _xblockexpression = ("." + _value);
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (expr instanceof PostfixExpressionPostfixAccessPtr) {
        _matched=true;
        String _xblockexpression = null;
        {
          final PostfixExpressionPostfixAccessPtr item = ((PostfixExpressionPostfixAccessPtr) expr);
          String _value = item.getValue();
          _xblockexpression = ("->" + _value);
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (expr instanceof PostfixExpressionPostfixIncrement) {
        _matched=true;
        _switchResult = "++";
      }
    }
    if (!_matched) {
      if (expr instanceof PostfixExpressionPostfixDecrement) {
        _matched=true;
        _switchResult = "--";
      }
    }
    return _switchResult;
  }
  
  public String print(final Statement stat) {
    String _switchResult = null;
    boolean _matched = false;
    if (!_matched) {
      if (stat instanceof GoToPrefixStatement) {
        _matched=true;
        String _xblockexpression = null;
        {
          final GoToPrefixStatement item = ((GoToPrefixStatement) stat);
          String _goto = item.getGoto();
          String _plus = (_goto + ": ");
          Statement _statement = item.getStatement();
          String _print = this.print(_statement);
          _xblockexpression = (_plus + _print);
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (stat instanceof CompoundStatement) {
        _matched=true;
        String _xblockexpression = null;
        {
          final CompoundStatement item = ((CompoundStatement) stat);
          String ret = " {\n";
          this.tab++;
          String _ret = ret;
          EList<BlockItem> _inner = item.getInner();
          final Function1<BlockItem, String> _function = (BlockItem it) -> {
            return this.print(it);
          };
          List<String> _map = ListExtensions.<BlockItem, String>map(_inner, _function);
          String _join = IterableExtensions.join(_map);
          ret = (_ret + _join);
          this.tab--;
          String _ret_1 = ret;
          _xblockexpression = ret = (_ret_1 + "}\n");
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (stat instanceof LabeledStatementCase) {
        _matched=true;
        String _xblockexpression = null;
        {
          final LabeledStatementCase item = ((LabeledStatementCase) stat);
          Expression _exp = item.getExp();
          String _print = this.print(_exp);
          String _plus = ("case " + _print);
          String _plus_1 = (_plus + ": ");
          Statement _statement = item.getStatement();
          String _print_1 = this.print(_statement);
          _xblockexpression = (_plus_1 + _print_1);
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (stat instanceof LabeledStatementDefault) {
        _matched=true;
        String _xblockexpression = null;
        {
          final LabeledStatementDefault item = ((LabeledStatementDefault) stat);
          Statement _statement = item.getStatement();
          String _print = this.print(_statement);
          _xblockexpression = ("default: " + _print);
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (stat instanceof ExpressionStatement) {
        _matched=true;
        String _xblockexpression = null;
        {
          final ExpressionStatement item = ((ExpressionStatement) stat);
          String _xifexpression = null;
          Expression _exp = item.getExp();
          boolean _equals = Objects.equal(_exp, null);
          if (_equals) {
            return ";\n";
          } else {
            Expression _exp_1 = item.getExp();
            String _print = this.print(_exp_1);
            _xifexpression = (_print + ";\n");
          }
          _xblockexpression = _xifexpression;
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (stat instanceof SelectionStatementIf) {
        _matched=true;
        String _xblockexpression = null;
        {
          final SelectionStatementIf item = ((SelectionStatementIf) stat);
          Expression _exp = item.getExp();
          String _print = this.print(_exp);
          String _plus = ("if(" + _print);
          String _plus_1 = (_plus + ")");
          Statement _then_statement = item.getThen_statement();
          String _print_1 = this.print(_then_statement);
          String _plus_2 = (_plus_1 + _print_1);
          String _plus_3 = (_plus_2 + "\nelse");
          Statement _else_statement = item.getElse_statement();
          String _print_2 = this.print(_else_statement);
          _xblockexpression = (_plus_3 + _print_2);
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (stat instanceof SelectionStatementSwitch) {
        _matched=true;
        String _xblockexpression = null;
        {
          final SelectionStatementSwitch item = ((SelectionStatementSwitch) stat);
          Expression _exp = item.getExp();
          String _print = this.print(_exp);
          String _plus = ("switch(" + _print);
          /* (_plus + ") {"); */
          EList<LabeledStatement> _content = item.getContent();
          final Function1<LabeledStatement, String> _function = (LabeledStatement it) -> {
            return this.print(it);
          };
          List<String> _map = ListExtensions.<LabeledStatement, String>map(_content, _function);
          _xblockexpression = IterableExtensions.join(_map);
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (stat instanceof IterationStatementWhile) {
        _matched=true;
        String _xblockexpression = null;
        {
          final IterationStatementWhile item = ((IterationStatementWhile) stat);
          Expression _exp = item.getExp();
          String _print = this.print(_exp);
          String _plus = ("while(" + _print);
          String _plus_1 = (_plus + ") ");
          Statement _statement = item.getStatement();
          String _print_1 = this.print(_statement);
          _xblockexpression = (_plus_1 + _print_1);
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (stat instanceof IterationStatementDo) {
        _matched=true;
        String _xblockexpression = null;
        {
          final IterationStatementDo item = ((IterationStatementDo) stat);
          Statement _statement = item.getStatement();
          String _print = this.print(_statement);
          String _plus = ("do" + _print);
          String _plus_1 = (_plus + "while(");
          Expression _exp = item.getExp();
          String _print_1 = this.print(_exp);
          String _plus_2 = (_plus_1 + _print_1);
          _xblockexpression = (_plus_2 + ")");
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (stat instanceof IterationStatementFor) {
        _matched=true;
        String _xblockexpression = null;
        {
          final IterationStatementFor item = ((IterationStatementFor) stat);
          String res = "for(";
          boolean _and = false;
          ExpressionStatement _field1 = item.getField1();
          boolean _equals = Objects.equal(_field1, null);
          if (!_equals) {
            _and = false;
          } else {
            Declaration _decl = item.getDecl();
            boolean _notEquals = (!Objects.equal(_decl, null));
            _and = _notEquals;
          }
          if (_and) {
            String _res = res;
            Declaration _decl_1 = item.getDecl();
            String _print = this.print(_decl_1);
            res = (_res + _print);
          } else {
            boolean _and_1 = false;
            ExpressionStatement _field1_1 = item.getField1();
            boolean _notEquals_1 = (!Objects.equal(_field1_1, null));
            if (!_notEquals_1) {
              _and_1 = false;
            } else {
              Declaration _decl_2 = item.getDecl();
              boolean _equals_1 = Objects.equal(_decl_2, null);
              _and_1 = _equals_1;
            }
            if (_and_1) {
              String _res_1 = res;
              ExpressionStatement _field1_2 = item.getField1();
              String _print_1 = this.print(_field1_2);
              res = (_res_1 + _print_1);
            }
          }
          ExpressionStatement _field2 = item.getField2();
          boolean _notEquals_2 = (!Objects.equal(_field2, null));
          if (_notEquals_2) {
            String _res_2 = res;
            ExpressionStatement _field2_1 = item.getField2();
            String _print_2 = this.print(_field2_1);
            res = (_res_2 + _print_2);
          }
          Expression _field3 = item.getField3();
          boolean _notEquals_3 = (!Objects.equal(_field3, null));
          if (_notEquals_3) {
            String _res_3 = res;
            Expression _field3_1 = item.getField3();
            String _print_3 = this.print(_field3_1);
            res = (_res_3 + _print_3);
          }
          EList<NextField> _next = item.getNext();
          boolean _notEquals_4 = (!Objects.equal(_next, null));
          if (_notEquals_4) {
            String _res_4 = res;
            EList<NextField> _next_1 = item.getNext();
            final Function1<NextField, String> _function = (NextField it) -> {
              return this.print(it);
            };
            List<String> _map = ListExtensions.<NextField, String>map(_next_1, _function);
            String _join = IterableExtensions.join(_map, ",");
            res = (_res_4 + _join);
          }
          String _res_5 = res;
          Statement _statement = item.getStatement();
          String _print_4 = this.print(_statement);
          String _plus = (")" + _print_4);
          _xblockexpression = res = (_res_5 + _plus);
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (stat instanceof JumpStatementGoto) {
        _matched=true;
        String _xblockexpression = null;
        {
          final JumpStatementGoto item = ((JumpStatementGoto) stat);
          String _name = item.getName();
          String _plus = ("goto " + _name);
          _xblockexpression = (_plus + ";\n");
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (stat instanceof JumpStatementReturn) {
        _matched=true;
        final JumpStatementReturn item = ((JumpStatementReturn) stat);
        Expression _exp = item.getExp();
        boolean _equals = Objects.equal(_exp, null);
        if (_equals) {
          return "return;\n";
        } else {
          Expression _exp_1 = item.getExp();
          String _print = this.print(_exp_1);
          String _plus = ("return " + _print);
          return (_plus + ";\n");
        }
      }
    }
    if (!_matched) {
      if (stat instanceof JumpStatementContinue) {
        _matched=true;
        _switchResult = "continue;\n";
      }
    }
    if (!_matched) {
      if (stat instanceof JumpStatementBreak) {
        _matched=true;
        _switchResult = "break;\n";
      }
    }
    return _switchResult;
  }
  
  public String print(final NextField field) {
    Expression _next = field.getNext();
    return this.print(_next);
  }
  
  public String print(final BlockItem block) {
    String _switchResult = null;
    boolean _matched = false;
    if (!_matched) {
      if (block instanceof StatementDeclaration) {
        _matched=true;
        String _xblockexpression = null;
        {
          final StatementDeclaration item = ((StatementDeclaration) block);
          Declaration _decl = item.getDecl();
          String _print = this.print(_decl);
          String _indent = this.indent("\n");
          _xblockexpression = (_print + _indent);
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (block instanceof StatementInnerStatement) {
        _matched=true;
        String _xblockexpression = null;
        {
          final StatementInnerStatement item = ((StatementInnerStatement) block);
          Statement _statement = item.getStatement();
          _xblockexpression = this.print(_statement);
        }
        _switchResult = _xblockexpression;
      }
    }
    return _switchResult;
  }
  
  public String indent(final String str) {
    String _xblockexpression = null;
    {
      String res = "";
      for (int i = 0; (i < this.tab); i++) {
        String _res = res;
        res = (_res + "\t");
      }
      _xblockexpression = (res + str);
    }
    return _xblockexpression;
  }
}
