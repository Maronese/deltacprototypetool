/**
 */
package org.xtext.dop.productdeclaration.productDeclaration.impl;

import org.eclipse.emf.ecore.EClass;

import org.xtext.dop.productdeclaration.productDeclaration.Espression;
import org.xtext.dop.productdeclaration.productDeclaration.ProductDeclarationPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Espression</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class EspressionImpl extends ExpressionImpl implements Espression
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected EspressionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ProductDeclarationPackage.Literals.ESPRESSION;
  }

} //EspressionImpl
