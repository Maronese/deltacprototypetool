/**
 */
package org.xtext.dop.codebase.codeBase;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Remove Global</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.codebase.codeBase.RemoveGlobal#getGlobal <em>Global</em>}</li>
 * </ul>
 *
 * @see org.xtext.dop.codebase.codeBase.CodeBasePackage#getRemoveGlobal()
 * @model
 * @generated
 */
public interface RemoveGlobal extends SourceOperation
{
  /**
   * Returns the value of the '<em><b>Global</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Global</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Global</em>' attribute.
   * @see #setGlobal(String)
   * @see org.xtext.dop.codebase.codeBase.CodeBasePackage#getRemoveGlobal_Global()
   * @model
   * @generated
   */
  String getGlobal();

  /**
   * Sets the value of the '{@link org.xtext.dop.codebase.codeBase.RemoveGlobal#getGlobal <em>Global</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Global</em>' attribute.
   * @see #getGlobal()
   * @generated
   */
  void setGlobal(String value);

} // RemoveGlobal
