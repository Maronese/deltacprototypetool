/**
 */
package org.xtext.dop.clang.clang;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Conditional Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.clang.clang.ConditionalExpression#getIf <em>If</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.ConditionalExpression#getYes <em>Yes</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.ConditionalExpression#getNo <em>No</em>}</li>
 * </ul>
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getConditionalExpression()
 * @model
 * @generated
 */
public interface ConditionalExpression extends Expression
{
  /**
   * Returns the value of the '<em><b>If</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>If</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>If</em>' containment reference.
   * @see #setIf(Expression)
   * @see org.xtext.dop.clang.clang.ClangPackage#getConditionalExpression_If()
   * @model containment="true"
   * @generated
   */
  Expression getIf();

  /**
   * Sets the value of the '{@link org.xtext.dop.clang.clang.ConditionalExpression#getIf <em>If</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>If</em>' containment reference.
   * @see #getIf()
   * @generated
   */
  void setIf(Expression value);

  /**
   * Returns the value of the '<em><b>Yes</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Yes</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Yes</em>' containment reference.
   * @see #setYes(Expression)
   * @see org.xtext.dop.clang.clang.ClangPackage#getConditionalExpression_Yes()
   * @model containment="true"
   * @generated
   */
  Expression getYes();

  /**
   * Sets the value of the '{@link org.xtext.dop.clang.clang.ConditionalExpression#getYes <em>Yes</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Yes</em>' containment reference.
   * @see #getYes()
   * @generated
   */
  void setYes(Expression value);

  /**
   * Returns the value of the '<em><b>No</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>No</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>No</em>' containment reference.
   * @see #setNo(Expression)
   * @see org.xtext.dop.clang.clang.ClangPackage#getConditionalExpression_No()
   * @model containment="true"
   * @generated
   */
  Expression getNo();

  /**
   * Sets the value of the '{@link org.xtext.dop.clang.clang.ConditionalExpression#getNo <em>No</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>No</em>' containment reference.
   * @see #getNo()
   * @generated
   */
  void setNo(Expression value);

} // ConditionalExpression
