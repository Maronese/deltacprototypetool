/**
 */
package org.xtext.dop.clang.clang.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.xtext.dop.clang.clang.ClangPackage;
import org.xtext.dop.clang.clang.PostfixExpressionPostfix;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Postfix Expression Postfix</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class PostfixExpressionPostfixImpl extends MinimalEObjectImpl.Container implements PostfixExpressionPostfix
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected PostfixExpressionPostfixImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ClangPackage.Literals.POSTFIX_EXPRESSION_POSTFIX;
  }

} //PostfixExpressionPostfixImpl
