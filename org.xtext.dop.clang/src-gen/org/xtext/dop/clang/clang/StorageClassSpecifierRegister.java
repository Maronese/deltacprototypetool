/**
 */
package org.xtext.dop.clang.clang;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Storage Class Specifier Register</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getStorageClassSpecifierRegister()
 * @model
 * @generated
 */
public interface StorageClassSpecifierRegister extends StorageClassSpecifier
{
} // StorageClassSpecifierRegister
