/**
 */
package org.xtext.dop.clang.clang.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.xtext.dop.clang.clang.ClangPackage;
import org.xtext.dop.clang.clang.Declaration;
import org.xtext.dop.clang.clang.FileInclude;
import org.xtext.dop.clang.clang.Model;
import org.xtext.dop.clang.clang.StandardInclude;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.clang.clang.impl.ModelImpl#getIncludesStd <em>Includes Std</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.impl.ModelImpl#getIncludesFile <em>Includes File</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.impl.ModelImpl#getDeclarations <em>Declarations</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ModelImpl extends MinimalEObjectImpl.Container implements Model
{
  /**
   * The cached value of the '{@link #getIncludesStd() <em>Includes Std</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIncludesStd()
   * @generated
   * @ordered
   */
  protected EList<StandardInclude> includesStd;

  /**
   * The cached value of the '{@link #getIncludesFile() <em>Includes File</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIncludesFile()
   * @generated
   * @ordered
   */
  protected EList<FileInclude> includesFile;

  /**
   * The cached value of the '{@link #getDeclarations() <em>Declarations</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDeclarations()
   * @generated
   * @ordered
   */
  protected EList<Declaration> declarations;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ModelImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ClangPackage.Literals.MODEL;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<StandardInclude> getIncludesStd()
  {
    if (includesStd == null)
    {
      includesStd = new EObjectContainmentEList<StandardInclude>(StandardInclude.class, this, ClangPackage.MODEL__INCLUDES_STD);
    }
    return includesStd;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<FileInclude> getIncludesFile()
  {
    if (includesFile == null)
    {
      includesFile = new EObjectContainmentEList<FileInclude>(FileInclude.class, this, ClangPackage.MODEL__INCLUDES_FILE);
    }
    return includesFile;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Declaration> getDeclarations()
  {
    if (declarations == null)
    {
      declarations = new EObjectContainmentEList<Declaration>(Declaration.class, this, ClangPackage.MODEL__DECLARATIONS);
    }
    return declarations;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case ClangPackage.MODEL__INCLUDES_STD:
        return ((InternalEList<?>)getIncludesStd()).basicRemove(otherEnd, msgs);
      case ClangPackage.MODEL__INCLUDES_FILE:
        return ((InternalEList<?>)getIncludesFile()).basicRemove(otherEnd, msgs);
      case ClangPackage.MODEL__DECLARATIONS:
        return ((InternalEList<?>)getDeclarations()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case ClangPackage.MODEL__INCLUDES_STD:
        return getIncludesStd();
      case ClangPackage.MODEL__INCLUDES_FILE:
        return getIncludesFile();
      case ClangPackage.MODEL__DECLARATIONS:
        return getDeclarations();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case ClangPackage.MODEL__INCLUDES_STD:
        getIncludesStd().clear();
        getIncludesStd().addAll((Collection<? extends StandardInclude>)newValue);
        return;
      case ClangPackage.MODEL__INCLUDES_FILE:
        getIncludesFile().clear();
        getIncludesFile().addAll((Collection<? extends FileInclude>)newValue);
        return;
      case ClangPackage.MODEL__DECLARATIONS:
        getDeclarations().clear();
        getDeclarations().addAll((Collection<? extends Declaration>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case ClangPackage.MODEL__INCLUDES_STD:
        getIncludesStd().clear();
        return;
      case ClangPackage.MODEL__INCLUDES_FILE:
        getIncludesFile().clear();
        return;
      case ClangPackage.MODEL__DECLARATIONS:
        getDeclarations().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case ClangPackage.MODEL__INCLUDES_STD:
        return includesStd != null && !includesStd.isEmpty();
      case ClangPackage.MODEL__INCLUDES_FILE:
        return includesFile != null && !includesFile.isEmpty();
      case ClangPackage.MODEL__DECLARATIONS:
        return declarations != null && !declarations.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //ModelImpl
