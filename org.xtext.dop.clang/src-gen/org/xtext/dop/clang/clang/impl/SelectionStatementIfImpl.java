/**
 */
package org.xtext.dop.clang.clang.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.xtext.dop.clang.clang.ClangPackage;
import org.xtext.dop.clang.clang.SelectionStatementIf;
import org.xtext.dop.clang.clang.Statement;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Selection Statement If</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.clang.clang.impl.SelectionStatementIfImpl#getThen_statement <em>Then statement</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.impl.SelectionStatementIfImpl#getElse_statement <em>Else statement</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SelectionStatementIfImpl extends SelectionStatementImpl implements SelectionStatementIf
{
  /**
   * The cached value of the '{@link #getThen_statement() <em>Then statement</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getThen_statement()
   * @generated
   * @ordered
   */
  protected Statement then_statement;

  /**
   * The cached value of the '{@link #getElse_statement() <em>Else statement</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getElse_statement()
   * @generated
   * @ordered
   */
  protected Statement else_statement;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SelectionStatementIfImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ClangPackage.Literals.SELECTION_STATEMENT_IF;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Statement getThen_statement()
  {
    return then_statement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetThen_statement(Statement newThen_statement, NotificationChain msgs)
  {
    Statement oldThen_statement = then_statement;
    then_statement = newThen_statement;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ClangPackage.SELECTION_STATEMENT_IF__THEN_STATEMENT, oldThen_statement, newThen_statement);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setThen_statement(Statement newThen_statement)
  {
    if (newThen_statement != then_statement)
    {
      NotificationChain msgs = null;
      if (then_statement != null)
        msgs = ((InternalEObject)then_statement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ClangPackage.SELECTION_STATEMENT_IF__THEN_STATEMENT, null, msgs);
      if (newThen_statement != null)
        msgs = ((InternalEObject)newThen_statement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ClangPackage.SELECTION_STATEMENT_IF__THEN_STATEMENT, null, msgs);
      msgs = basicSetThen_statement(newThen_statement, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ClangPackage.SELECTION_STATEMENT_IF__THEN_STATEMENT, newThen_statement, newThen_statement));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Statement getElse_statement()
  {
    return else_statement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetElse_statement(Statement newElse_statement, NotificationChain msgs)
  {
    Statement oldElse_statement = else_statement;
    else_statement = newElse_statement;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ClangPackage.SELECTION_STATEMENT_IF__ELSE_STATEMENT, oldElse_statement, newElse_statement);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setElse_statement(Statement newElse_statement)
  {
    if (newElse_statement != else_statement)
    {
      NotificationChain msgs = null;
      if (else_statement != null)
        msgs = ((InternalEObject)else_statement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ClangPackage.SELECTION_STATEMENT_IF__ELSE_STATEMENT, null, msgs);
      if (newElse_statement != null)
        msgs = ((InternalEObject)newElse_statement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ClangPackage.SELECTION_STATEMENT_IF__ELSE_STATEMENT, null, msgs);
      msgs = basicSetElse_statement(newElse_statement, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ClangPackage.SELECTION_STATEMENT_IF__ELSE_STATEMENT, newElse_statement, newElse_statement));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case ClangPackage.SELECTION_STATEMENT_IF__THEN_STATEMENT:
        return basicSetThen_statement(null, msgs);
      case ClangPackage.SELECTION_STATEMENT_IF__ELSE_STATEMENT:
        return basicSetElse_statement(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case ClangPackage.SELECTION_STATEMENT_IF__THEN_STATEMENT:
        return getThen_statement();
      case ClangPackage.SELECTION_STATEMENT_IF__ELSE_STATEMENT:
        return getElse_statement();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case ClangPackage.SELECTION_STATEMENT_IF__THEN_STATEMENT:
        setThen_statement((Statement)newValue);
        return;
      case ClangPackage.SELECTION_STATEMENT_IF__ELSE_STATEMENT:
        setElse_statement((Statement)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case ClangPackage.SELECTION_STATEMENT_IF__THEN_STATEMENT:
        setThen_statement((Statement)null);
        return;
      case ClangPackage.SELECTION_STATEMENT_IF__ELSE_STATEMENT:
        setElse_statement((Statement)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case ClangPackage.SELECTION_STATEMENT_IF__THEN_STATEMENT:
        return then_statement != null;
      case ClangPackage.SELECTION_STATEMENT_IF__ELSE_STATEMENT:
        return else_statement != null;
    }
    return super.eIsSet(featureID);
  }

} //SelectionStatementIfImpl
