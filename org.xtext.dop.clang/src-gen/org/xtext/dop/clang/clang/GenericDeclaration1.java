/**
 */
package org.xtext.dop.clang.clang;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Generic Declaration1</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getGenericDeclaration1()
 * @model
 * @generated
 */
public interface GenericDeclaration1 extends GenericDeclarationId
{
} // GenericDeclaration1
