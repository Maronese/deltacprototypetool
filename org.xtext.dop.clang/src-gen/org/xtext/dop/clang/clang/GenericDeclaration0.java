/**
 */
package org.xtext.dop.clang.clang;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Generic Declaration0</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.clang.clang.GenericDeclaration0#getPointers <em>Pointers</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.GenericDeclaration0#getRestrict <em>Restrict</em>}</li>
 * </ul>
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getGenericDeclaration0()
 * @model
 * @generated
 */
public interface GenericDeclaration0 extends GenericDeclarationId
{
  /**
   * Returns the value of the '<em><b>Pointers</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Pointers</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Pointers</em>' attribute list.
   * @see org.xtext.dop.clang.clang.ClangPackage#getGenericDeclaration0_Pointers()
   * @model unique="false"
   * @generated
   */
  EList<String> getPointers();

  /**
   * Returns the value of the '<em><b>Restrict</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Restrict</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Restrict</em>' attribute.
   * @see #setRestrict(String)
   * @see org.xtext.dop.clang.clang.ClangPackage#getGenericDeclaration0_Restrict()
   * @model
   * @generated
   */
  String getRestrict();

  /**
   * Sets the value of the '{@link org.xtext.dop.clang.clang.GenericDeclaration0#getRestrict <em>Restrict</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Restrict</em>' attribute.
   * @see #getRestrict()
   * @generated
   */
  void setRestrict(String value);

} // GenericDeclaration0
