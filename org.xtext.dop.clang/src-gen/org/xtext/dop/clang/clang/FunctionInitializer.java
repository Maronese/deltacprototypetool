/**
 */
package org.xtext.dop.clang.clang;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Function Initializer</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.clang.clang.FunctionInitializer#getBlock <em>Block</em>}</li>
 * </ul>
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getFunctionInitializer()
 * @model
 * @generated
 */
public interface FunctionInitializer extends GenericDeclarationInitializer
{
  /**
   * Returns the value of the '<em><b>Block</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Block</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Block</em>' containment reference.
   * @see #setBlock(CompoundStatement)
   * @see org.xtext.dop.clang.clang.ClangPackage#getFunctionInitializer_Block()
   * @model containment="true"
   * @generated
   */
  CompoundStatement getBlock();

  /**
   * Sets the value of the '{@link org.xtext.dop.clang.clang.FunctionInitializer#getBlock <em>Block</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Block</em>' containment reference.
   * @see #getBlock()
   * @generated
   */
  void setBlock(CompoundStatement value);

} // FunctionInitializer
