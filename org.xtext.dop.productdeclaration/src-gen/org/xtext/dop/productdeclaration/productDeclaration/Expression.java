/**
 */
package org.xtext.dop.productdeclaration.productDeclaration;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.dop.productdeclaration.productDeclaration.ProductDeclarationPackage#getExpression()
 * @model
 * @generated
 */
public interface Expression extends EObject
{
} // Expression
