/**
 */
package org.xtext.dop.clang.clang.impl;

import org.eclipse.emf.ecore.EClass;

import org.xtext.dop.clang.clang.ClangPackage;
import org.xtext.dop.clang.clang.StorageClassSpecifierAuto;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Storage Class Specifier Auto</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class StorageClassSpecifierAutoImpl extends StorageClassSpecifierImpl implements StorageClassSpecifierAuto
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected StorageClassSpecifierAutoImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ClangPackage.Literals.STORAGE_CLASS_SPECIFIER_AUTO;
  }

} //StorageClassSpecifierAutoImpl
