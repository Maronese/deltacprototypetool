/**
 */
package org.xtext.dop.clang.clang.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.xtext.dop.clang.clang.ClangPackage;
import org.xtext.dop.clang.clang.DeclarationSpecifier;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Declaration Specifier</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DeclarationSpecifierImpl extends MinimalEObjectImpl.Container implements DeclarationSpecifier
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected DeclarationSpecifierImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ClangPackage.Literals.DECLARATION_SPECIFIER;
  }

} //DeclarationSpecifierImpl
