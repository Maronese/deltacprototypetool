package org.xtext.dop.productdeclaration.interpreter

import java.util.Observable
import org.xtext.dop.productdeclaration.productDeclaration.Product
import java.util.List
import org.xtext.dop.rappresentation.Project
import org.xtext.dop.rappresentation.DopProject
import org.xtext.dop.codebase.generator.Message

class GenerateListObservable extends Observable {	
	
	Interpreter interpreter
	
	new(Interpreter interpreter) {
		super()
		this.interpreter = interpreter
	}
	
	def sendSelection(Object selection) {
		
		val project = new DopProject((selection as Product).name)
		
		val deltas = interpreter.interpret(selection as Product)
		
		println(deltas.join(', '))
		
		setChanged()
		notifyObservers(new Message(deltas, project))
	}
	

}