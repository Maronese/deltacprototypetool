/**
 */
package org.xtext.dop.codebase.codeBase;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.xtext.dop.codebase.codeBase.CodeBaseFactory
 * @model kind="package"
 * @generated
 */
public interface CodeBasePackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "codeBase";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.xtext.org/dop/codebase/CodeBase";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "codeBase";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  CodeBasePackage eINSTANCE = org.xtext.dop.codebase.codeBase.impl.CodeBasePackageImpl.init();

  /**
   * The meta object id for the '{@link org.xtext.dop.codebase.codeBase.impl.DeltaModuleImpl <em>Delta Module</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.codebase.codeBase.impl.DeltaModuleImpl
   * @see org.xtext.dop.codebase.codeBase.impl.CodeBasePackageImpl#getDeltaModule()
   * @generated
   */
  int DELTA_MODULE = 0;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DELTA_MODULE__NAME = 0;

  /**
   * The feature id for the '<em><b>Body</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DELTA_MODULE__BODY = 1;

  /**
   * The number of structural features of the '<em>Delta Module</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DELTA_MODULE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.xtext.dop.codebase.codeBase.impl.FileOperationImpl <em>File Operation</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.codebase.codeBase.impl.FileOperationImpl
   * @see org.xtext.dop.codebase.codeBase.impl.CodeBasePackageImpl#getFileOperation()
   * @generated
   */
  int FILE_OPERATION = 1;

  /**
   * The number of structural features of the '<em>File Operation</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FILE_OPERATION_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.codebase.codeBase.impl.AddSourceImpl <em>Add Source</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.codebase.codeBase.impl.AddSourceImpl
   * @see org.xtext.dop.codebase.codeBase.impl.CodeBasePackageImpl#getAddSource()
   * @generated
   */
  int ADD_SOURCE = 2;

  /**
   * The feature id for the '<em><b>File Name</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADD_SOURCE__FILE_NAME = 0;

  /**
   * The feature id for the '<em><b>Code</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADD_SOURCE__CODE = 1;

  /**
   * The number of structural features of the '<em>Add Source</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADD_SOURCE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.xtext.dop.codebase.codeBase.impl.RemoveSourceImpl <em>Remove Source</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.codebase.codeBase.impl.RemoveSourceImpl
   * @see org.xtext.dop.codebase.codeBase.impl.CodeBasePackageImpl#getRemoveSource()
   * @generated
   */
  int REMOVE_SOURCE = 3;

  /**
   * The feature id for the '<em><b>File Name</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REMOVE_SOURCE__FILE_NAME = 0;

  /**
   * The number of structural features of the '<em>Remove Source</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REMOVE_SOURCE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.codebase.codeBase.impl.SourceOperationImpl <em>Source Operation</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.codebase.codeBase.impl.SourceOperationImpl
   * @see org.xtext.dop.codebase.codeBase.impl.CodeBasePackageImpl#getSourceOperation()
   * @generated
   */
  int SOURCE_OPERATION = 4;

  /**
   * The number of structural features of the '<em>Source Operation</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOURCE_OPERATION_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.xtext.dop.codebase.codeBase.impl.AddsImpl <em>Adds</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.codebase.codeBase.impl.AddsImpl
   * @see org.xtext.dop.codebase.codeBase.impl.CodeBasePackageImpl#getAdds()
   * @generated
   */
  int ADDS = 5;

  /**
   * The feature id for the '<em><b>Files</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADDS__FILES = FILE_OPERATION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Adds</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADDS_FEATURE_COUNT = FILE_OPERATION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.codebase.codeBase.impl.RemovesImpl <em>Removes</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.codebase.codeBase.impl.RemovesImpl
   * @see org.xtext.dop.codebase.codeBase.impl.CodeBasePackageImpl#getRemoves()
   * @generated
   */
  int REMOVES = 6;

  /**
   * The feature id for the '<em><b>Files</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REMOVES__FILES = FILE_OPERATION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Removes</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REMOVES_FEATURE_COUNT = FILE_OPERATION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.codebase.codeBase.impl.ModifiesImpl <em>Modifies</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.codebase.codeBase.impl.ModifiesImpl
   * @see org.xtext.dop.codebase.codeBase.impl.CodeBasePackageImpl#getModifies()
   * @generated
   */
  int MODIFIES = 7;

  /**
   * The feature id for the '<em><b>File Name</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODIFIES__FILE_NAME = FILE_OPERATION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Action</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODIFIES__ACTION = FILE_OPERATION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Modifies</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODIFIES_FEATURE_COUNT = FILE_OPERATION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.xtext.dop.codebase.codeBase.impl.AddDeclarationImpl <em>Add Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.codebase.codeBase.impl.AddDeclarationImpl
   * @see org.xtext.dop.codebase.codeBase.impl.CodeBasePackageImpl#getAddDeclaration()
   * @generated
   */
  int ADD_DECLARATION = 8;

  /**
   * The feature id for the '<em><b>Decl</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADD_DECLARATION__DECL = SOURCE_OPERATION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Add Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADD_DECLARATION_FEATURE_COUNT = SOURCE_OPERATION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.codebase.codeBase.impl.AddStandardIncludeImpl <em>Add Standard Include</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.codebase.codeBase.impl.AddStandardIncludeImpl
   * @see org.xtext.dop.codebase.codeBase.impl.CodeBasePackageImpl#getAddStandardInclude()
   * @generated
   */
  int ADD_STANDARD_INCLUDE = 9;

  /**
   * The feature id for the '<em><b>Incl</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADD_STANDARD_INCLUDE__INCL = SOURCE_OPERATION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Add Standard Include</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADD_STANDARD_INCLUDE_FEATURE_COUNT = SOURCE_OPERATION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.codebase.codeBase.impl.AddFileIncludeImpl <em>Add File Include</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.codebase.codeBase.impl.AddFileIncludeImpl
   * @see org.xtext.dop.codebase.codeBase.impl.CodeBasePackageImpl#getAddFileInclude()
   * @generated
   */
  int ADD_FILE_INCLUDE = 10;

  /**
   * The feature id for the '<em><b>Incl</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADD_FILE_INCLUDE__INCL = SOURCE_OPERATION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Add File Include</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADD_FILE_INCLUDE_FEATURE_COUNT = SOURCE_OPERATION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.codebase.codeBase.impl.RemoveGlobalImpl <em>Remove Global</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.codebase.codeBase.impl.RemoveGlobalImpl
   * @see org.xtext.dop.codebase.codeBase.impl.CodeBasePackageImpl#getRemoveGlobal()
   * @generated
   */
  int REMOVE_GLOBAL = 11;

  /**
   * The feature id for the '<em><b>Global</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REMOVE_GLOBAL__GLOBAL = SOURCE_OPERATION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Remove Global</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REMOVE_GLOBAL_FEATURE_COUNT = SOURCE_OPERATION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.codebase.codeBase.impl.RemoveStandardIncludeImpl <em>Remove Standard Include</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.codebase.codeBase.impl.RemoveStandardIncludeImpl
   * @see org.xtext.dop.codebase.codeBase.impl.CodeBasePackageImpl#getRemoveStandardInclude()
   * @generated
   */
  int REMOVE_STANDARD_INCLUDE = 12;

  /**
   * The feature id for the '<em><b>Incl</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REMOVE_STANDARD_INCLUDE__INCL = SOURCE_OPERATION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Remove Standard Include</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REMOVE_STANDARD_INCLUDE_FEATURE_COUNT = SOURCE_OPERATION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.codebase.codeBase.impl.RemoveFileIncludeImpl <em>Remove File Include</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.codebase.codeBase.impl.RemoveFileIncludeImpl
   * @see org.xtext.dop.codebase.codeBase.impl.CodeBasePackageImpl#getRemoveFileInclude()
   * @generated
   */
  int REMOVE_FILE_INCLUDE = 13;

  /**
   * The feature id for the '<em><b>Incl</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REMOVE_FILE_INCLUDE__INCL = SOURCE_OPERATION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Remove File Include</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REMOVE_FILE_INCLUDE_FEATURE_COUNT = SOURCE_OPERATION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.dop.codebase.codeBase.impl.ModifyFunctionImpl <em>Modify Function</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.dop.codebase.codeBase.impl.ModifyFunctionImpl
   * @see org.xtext.dop.codebase.codeBase.impl.CodeBasePackageImpl#getModifyFunction()
   * @generated
   */
  int MODIFY_FUNCTION = 14;

  /**
   * The feature id for the '<em><b>Decl</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODIFY_FUNCTION__DECL = SOURCE_OPERATION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Modify Function</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODIFY_FUNCTION_FEATURE_COUNT = SOURCE_OPERATION_FEATURE_COUNT + 1;


  /**
   * Returns the meta object for class '{@link org.xtext.dop.codebase.codeBase.DeltaModule <em>Delta Module</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Delta Module</em>'.
   * @see org.xtext.dop.codebase.codeBase.DeltaModule
   * @generated
   */
  EClass getDeltaModule();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.dop.codebase.codeBase.DeltaModule#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.xtext.dop.codebase.codeBase.DeltaModule#getName()
   * @see #getDeltaModule()
   * @generated
   */
  EAttribute getDeltaModule_Name();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.dop.codebase.codeBase.DeltaModule#getBody <em>Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Body</em>'.
   * @see org.xtext.dop.codebase.codeBase.DeltaModule#getBody()
   * @see #getDeltaModule()
   * @generated
   */
  EReference getDeltaModule_Body();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.codebase.codeBase.FileOperation <em>File Operation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>File Operation</em>'.
   * @see org.xtext.dop.codebase.codeBase.FileOperation
   * @generated
   */
  EClass getFileOperation();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.codebase.codeBase.AddSource <em>Add Source</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Add Source</em>'.
   * @see org.xtext.dop.codebase.codeBase.AddSource
   * @generated
   */
  EClass getAddSource();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.codebase.codeBase.AddSource#getFileName <em>File Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>File Name</em>'.
   * @see org.xtext.dop.codebase.codeBase.AddSource#getFileName()
   * @see #getAddSource()
   * @generated
   */
  EReference getAddSource_FileName();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.codebase.codeBase.AddSource#getCode <em>Code</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Code</em>'.
   * @see org.xtext.dop.codebase.codeBase.AddSource#getCode()
   * @see #getAddSource()
   * @generated
   */
  EReference getAddSource_Code();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.codebase.codeBase.RemoveSource <em>Remove Source</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Remove Source</em>'.
   * @see org.xtext.dop.codebase.codeBase.RemoveSource
   * @generated
   */
  EClass getRemoveSource();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.codebase.codeBase.RemoveSource#getFileName <em>File Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>File Name</em>'.
   * @see org.xtext.dop.codebase.codeBase.RemoveSource#getFileName()
   * @see #getRemoveSource()
   * @generated
   */
  EReference getRemoveSource_FileName();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.codebase.codeBase.SourceOperation <em>Source Operation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Source Operation</em>'.
   * @see org.xtext.dop.codebase.codeBase.SourceOperation
   * @generated
   */
  EClass getSourceOperation();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.codebase.codeBase.Adds <em>Adds</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Adds</em>'.
   * @see org.xtext.dop.codebase.codeBase.Adds
   * @generated
   */
  EClass getAdds();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.dop.codebase.codeBase.Adds#getFiles <em>Files</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Files</em>'.
   * @see org.xtext.dop.codebase.codeBase.Adds#getFiles()
   * @see #getAdds()
   * @generated
   */
  EReference getAdds_Files();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.codebase.codeBase.Removes <em>Removes</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Removes</em>'.
   * @see org.xtext.dop.codebase.codeBase.Removes
   * @generated
   */
  EClass getRemoves();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.dop.codebase.codeBase.Removes#getFiles <em>Files</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Files</em>'.
   * @see org.xtext.dop.codebase.codeBase.Removes#getFiles()
   * @see #getRemoves()
   * @generated
   */
  EReference getRemoves_Files();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.codebase.codeBase.Modifies <em>Modifies</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Modifies</em>'.
   * @see org.xtext.dop.codebase.codeBase.Modifies
   * @generated
   */
  EClass getModifies();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.codebase.codeBase.Modifies#getFileName <em>File Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>File Name</em>'.
   * @see org.xtext.dop.codebase.codeBase.Modifies#getFileName()
   * @see #getModifies()
   * @generated
   */
  EReference getModifies_FileName();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.dop.codebase.codeBase.Modifies#getAction <em>Action</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Action</em>'.
   * @see org.xtext.dop.codebase.codeBase.Modifies#getAction()
   * @see #getModifies()
   * @generated
   */
  EReference getModifies_Action();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.codebase.codeBase.AddDeclaration <em>Add Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Add Declaration</em>'.
   * @see org.xtext.dop.codebase.codeBase.AddDeclaration
   * @generated
   */
  EClass getAddDeclaration();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.codebase.codeBase.AddDeclaration#getDecl <em>Decl</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Decl</em>'.
   * @see org.xtext.dop.codebase.codeBase.AddDeclaration#getDecl()
   * @see #getAddDeclaration()
   * @generated
   */
  EReference getAddDeclaration_Decl();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.codebase.codeBase.AddStandardInclude <em>Add Standard Include</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Add Standard Include</em>'.
   * @see org.xtext.dop.codebase.codeBase.AddStandardInclude
   * @generated
   */
  EClass getAddStandardInclude();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.codebase.codeBase.AddStandardInclude#getIncl <em>Incl</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Incl</em>'.
   * @see org.xtext.dop.codebase.codeBase.AddStandardInclude#getIncl()
   * @see #getAddStandardInclude()
   * @generated
   */
  EReference getAddStandardInclude_Incl();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.codebase.codeBase.AddFileInclude <em>Add File Include</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Add File Include</em>'.
   * @see org.xtext.dop.codebase.codeBase.AddFileInclude
   * @generated
   */
  EClass getAddFileInclude();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.codebase.codeBase.AddFileInclude#getIncl <em>Incl</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Incl</em>'.
   * @see org.xtext.dop.codebase.codeBase.AddFileInclude#getIncl()
   * @see #getAddFileInclude()
   * @generated
   */
  EReference getAddFileInclude_Incl();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.codebase.codeBase.RemoveGlobal <em>Remove Global</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Remove Global</em>'.
   * @see org.xtext.dop.codebase.codeBase.RemoveGlobal
   * @generated
   */
  EClass getRemoveGlobal();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.dop.codebase.codeBase.RemoveGlobal#getGlobal <em>Global</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Global</em>'.
   * @see org.xtext.dop.codebase.codeBase.RemoveGlobal#getGlobal()
   * @see #getRemoveGlobal()
   * @generated
   */
  EAttribute getRemoveGlobal_Global();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.codebase.codeBase.RemoveStandardInclude <em>Remove Standard Include</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Remove Standard Include</em>'.
   * @see org.xtext.dop.codebase.codeBase.RemoveStandardInclude
   * @generated
   */
  EClass getRemoveStandardInclude();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.codebase.codeBase.RemoveStandardInclude#getIncl <em>Incl</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Incl</em>'.
   * @see org.xtext.dop.codebase.codeBase.RemoveStandardInclude#getIncl()
   * @see #getRemoveStandardInclude()
   * @generated
   */
  EReference getRemoveStandardInclude_Incl();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.codebase.codeBase.RemoveFileInclude <em>Remove File Include</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Remove File Include</em>'.
   * @see org.xtext.dop.codebase.codeBase.RemoveFileInclude
   * @generated
   */
  EClass getRemoveFileInclude();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.codebase.codeBase.RemoveFileInclude#getIncl <em>Incl</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Incl</em>'.
   * @see org.xtext.dop.codebase.codeBase.RemoveFileInclude#getIncl()
   * @see #getRemoveFileInclude()
   * @generated
   */
  EReference getRemoveFileInclude_Incl();

  /**
   * Returns the meta object for class '{@link org.xtext.dop.codebase.codeBase.ModifyFunction <em>Modify Function</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Modify Function</em>'.
   * @see org.xtext.dop.codebase.codeBase.ModifyFunction
   * @generated
   */
  EClass getModifyFunction();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.dop.codebase.codeBase.ModifyFunction#getDecl <em>Decl</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Decl</em>'.
   * @see org.xtext.dop.codebase.codeBase.ModifyFunction#getDecl()
   * @see #getModifyFunction()
   * @generated
   */
  EReference getModifyFunction_Decl();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  CodeBaseFactory getCodeBaseFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link org.xtext.dop.codebase.codeBase.impl.DeltaModuleImpl <em>Delta Module</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.codebase.codeBase.impl.DeltaModuleImpl
     * @see org.xtext.dop.codebase.codeBase.impl.CodeBasePackageImpl#getDeltaModule()
     * @generated
     */
    EClass DELTA_MODULE = eINSTANCE.getDeltaModule();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DELTA_MODULE__NAME = eINSTANCE.getDeltaModule_Name();

    /**
     * The meta object literal for the '<em><b>Body</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DELTA_MODULE__BODY = eINSTANCE.getDeltaModule_Body();

    /**
     * The meta object literal for the '{@link org.xtext.dop.codebase.codeBase.impl.FileOperationImpl <em>File Operation</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.codebase.codeBase.impl.FileOperationImpl
     * @see org.xtext.dop.codebase.codeBase.impl.CodeBasePackageImpl#getFileOperation()
     * @generated
     */
    EClass FILE_OPERATION = eINSTANCE.getFileOperation();

    /**
     * The meta object literal for the '{@link org.xtext.dop.codebase.codeBase.impl.AddSourceImpl <em>Add Source</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.codebase.codeBase.impl.AddSourceImpl
     * @see org.xtext.dop.codebase.codeBase.impl.CodeBasePackageImpl#getAddSource()
     * @generated
     */
    EClass ADD_SOURCE = eINSTANCE.getAddSource();

    /**
     * The meta object literal for the '<em><b>File Name</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ADD_SOURCE__FILE_NAME = eINSTANCE.getAddSource_FileName();

    /**
     * The meta object literal for the '<em><b>Code</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ADD_SOURCE__CODE = eINSTANCE.getAddSource_Code();

    /**
     * The meta object literal for the '{@link org.xtext.dop.codebase.codeBase.impl.RemoveSourceImpl <em>Remove Source</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.codebase.codeBase.impl.RemoveSourceImpl
     * @see org.xtext.dop.codebase.codeBase.impl.CodeBasePackageImpl#getRemoveSource()
     * @generated
     */
    EClass REMOVE_SOURCE = eINSTANCE.getRemoveSource();

    /**
     * The meta object literal for the '<em><b>File Name</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference REMOVE_SOURCE__FILE_NAME = eINSTANCE.getRemoveSource_FileName();

    /**
     * The meta object literal for the '{@link org.xtext.dop.codebase.codeBase.impl.SourceOperationImpl <em>Source Operation</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.codebase.codeBase.impl.SourceOperationImpl
     * @see org.xtext.dop.codebase.codeBase.impl.CodeBasePackageImpl#getSourceOperation()
     * @generated
     */
    EClass SOURCE_OPERATION = eINSTANCE.getSourceOperation();

    /**
     * The meta object literal for the '{@link org.xtext.dop.codebase.codeBase.impl.AddsImpl <em>Adds</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.codebase.codeBase.impl.AddsImpl
     * @see org.xtext.dop.codebase.codeBase.impl.CodeBasePackageImpl#getAdds()
     * @generated
     */
    EClass ADDS = eINSTANCE.getAdds();

    /**
     * The meta object literal for the '<em><b>Files</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ADDS__FILES = eINSTANCE.getAdds_Files();

    /**
     * The meta object literal for the '{@link org.xtext.dop.codebase.codeBase.impl.RemovesImpl <em>Removes</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.codebase.codeBase.impl.RemovesImpl
     * @see org.xtext.dop.codebase.codeBase.impl.CodeBasePackageImpl#getRemoves()
     * @generated
     */
    EClass REMOVES = eINSTANCE.getRemoves();

    /**
     * The meta object literal for the '<em><b>Files</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference REMOVES__FILES = eINSTANCE.getRemoves_Files();

    /**
     * The meta object literal for the '{@link org.xtext.dop.codebase.codeBase.impl.ModifiesImpl <em>Modifies</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.codebase.codeBase.impl.ModifiesImpl
     * @see org.xtext.dop.codebase.codeBase.impl.CodeBasePackageImpl#getModifies()
     * @generated
     */
    EClass MODIFIES = eINSTANCE.getModifies();

    /**
     * The meta object literal for the '<em><b>File Name</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MODIFIES__FILE_NAME = eINSTANCE.getModifies_FileName();

    /**
     * The meta object literal for the '<em><b>Action</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MODIFIES__ACTION = eINSTANCE.getModifies_Action();

    /**
     * The meta object literal for the '{@link org.xtext.dop.codebase.codeBase.impl.AddDeclarationImpl <em>Add Declaration</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.codebase.codeBase.impl.AddDeclarationImpl
     * @see org.xtext.dop.codebase.codeBase.impl.CodeBasePackageImpl#getAddDeclaration()
     * @generated
     */
    EClass ADD_DECLARATION = eINSTANCE.getAddDeclaration();

    /**
     * The meta object literal for the '<em><b>Decl</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ADD_DECLARATION__DECL = eINSTANCE.getAddDeclaration_Decl();

    /**
     * The meta object literal for the '{@link org.xtext.dop.codebase.codeBase.impl.AddStandardIncludeImpl <em>Add Standard Include</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.codebase.codeBase.impl.AddStandardIncludeImpl
     * @see org.xtext.dop.codebase.codeBase.impl.CodeBasePackageImpl#getAddStandardInclude()
     * @generated
     */
    EClass ADD_STANDARD_INCLUDE = eINSTANCE.getAddStandardInclude();

    /**
     * The meta object literal for the '<em><b>Incl</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ADD_STANDARD_INCLUDE__INCL = eINSTANCE.getAddStandardInclude_Incl();

    /**
     * The meta object literal for the '{@link org.xtext.dop.codebase.codeBase.impl.AddFileIncludeImpl <em>Add File Include</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.codebase.codeBase.impl.AddFileIncludeImpl
     * @see org.xtext.dop.codebase.codeBase.impl.CodeBasePackageImpl#getAddFileInclude()
     * @generated
     */
    EClass ADD_FILE_INCLUDE = eINSTANCE.getAddFileInclude();

    /**
     * The meta object literal for the '<em><b>Incl</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ADD_FILE_INCLUDE__INCL = eINSTANCE.getAddFileInclude_Incl();

    /**
     * The meta object literal for the '{@link org.xtext.dop.codebase.codeBase.impl.RemoveGlobalImpl <em>Remove Global</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.codebase.codeBase.impl.RemoveGlobalImpl
     * @see org.xtext.dop.codebase.codeBase.impl.CodeBasePackageImpl#getRemoveGlobal()
     * @generated
     */
    EClass REMOVE_GLOBAL = eINSTANCE.getRemoveGlobal();

    /**
     * The meta object literal for the '<em><b>Global</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute REMOVE_GLOBAL__GLOBAL = eINSTANCE.getRemoveGlobal_Global();

    /**
     * The meta object literal for the '{@link org.xtext.dop.codebase.codeBase.impl.RemoveStandardIncludeImpl <em>Remove Standard Include</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.codebase.codeBase.impl.RemoveStandardIncludeImpl
     * @see org.xtext.dop.codebase.codeBase.impl.CodeBasePackageImpl#getRemoveStandardInclude()
     * @generated
     */
    EClass REMOVE_STANDARD_INCLUDE = eINSTANCE.getRemoveStandardInclude();

    /**
     * The meta object literal for the '<em><b>Incl</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference REMOVE_STANDARD_INCLUDE__INCL = eINSTANCE.getRemoveStandardInclude_Incl();

    /**
     * The meta object literal for the '{@link org.xtext.dop.codebase.codeBase.impl.RemoveFileIncludeImpl <em>Remove File Include</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.codebase.codeBase.impl.RemoveFileIncludeImpl
     * @see org.xtext.dop.codebase.codeBase.impl.CodeBasePackageImpl#getRemoveFileInclude()
     * @generated
     */
    EClass REMOVE_FILE_INCLUDE = eINSTANCE.getRemoveFileInclude();

    /**
     * The meta object literal for the '<em><b>Incl</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference REMOVE_FILE_INCLUDE__INCL = eINSTANCE.getRemoveFileInclude_Incl();

    /**
     * The meta object literal for the '{@link org.xtext.dop.codebase.codeBase.impl.ModifyFunctionImpl <em>Modify Function</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.dop.codebase.codeBase.impl.ModifyFunctionImpl
     * @see org.xtext.dop.codebase.codeBase.impl.CodeBasePackageImpl#getModifyFunction()
     * @generated
     */
    EClass MODIFY_FUNCTION = eINSTANCE.getModifyFunction();

    /**
     * The meta object literal for the '<em><b>Decl</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MODIFY_FUNCTION__DECL = eINSTANCE.getModifyFunction_Decl();

  }

} //CodeBasePackage
