/**
 */
package org.xtext.dop.clang.clang;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type Qualifier Volatile</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getTypeQualifierVolatile()
 * @model
 * @generated
 */
public interface TypeQualifierVolatile extends TypeQualifier
{
} // TypeQualifierVolatile
