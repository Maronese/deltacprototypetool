/**
 */
package org.xtext.dop.clang.clang;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Base Type Float</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getBaseTypeFloat()
 * @model
 * @generated
 */
public interface BaseTypeFloat extends BaseType
{
} // BaseTypeFloat
