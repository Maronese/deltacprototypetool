/**
 */
package org.xtext.dop.codebase.codeBase.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.xtext.dop.codebase.codeBase.CodeBasePackage;
import org.xtext.dop.codebase.codeBase.FileOperation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>File Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class FileOperationImpl extends MinimalEObjectImpl.Container implements FileOperation
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected FileOperationImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return CodeBasePackage.Literals.FILE_OPERATION;
  }

} //FileOperationImpl
