/**
 */
package org.xtext.dop.clang.clang.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.xtext.dop.clang.clang.ClangPackage;
import org.xtext.dop.clang.clang.ConditionalExpression;
import org.xtext.dop.clang.clang.Expression;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Conditional Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.clang.clang.impl.ConditionalExpressionImpl#getIf <em>If</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.impl.ConditionalExpressionImpl#getYes <em>Yes</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.impl.ConditionalExpressionImpl#getNo <em>No</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConditionalExpressionImpl extends ExpressionImpl implements ConditionalExpression
{
  /**
   * The cached value of the '{@link #getIf() <em>If</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIf()
   * @generated
   * @ordered
   */
  protected Expression if_;

  /**
   * The cached value of the '{@link #getYes() <em>Yes</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getYes()
   * @generated
   * @ordered
   */
  protected Expression yes;

  /**
   * The cached value of the '{@link #getNo() <em>No</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNo()
   * @generated
   * @ordered
   */
  protected Expression no;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ConditionalExpressionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ClangPackage.Literals.CONDITIONAL_EXPRESSION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Expression getIf()
  {
    return if_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetIf(Expression newIf, NotificationChain msgs)
  {
    Expression oldIf = if_;
    if_ = newIf;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ClangPackage.CONDITIONAL_EXPRESSION__IF, oldIf, newIf);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setIf(Expression newIf)
  {
    if (newIf != if_)
    {
      NotificationChain msgs = null;
      if (if_ != null)
        msgs = ((InternalEObject)if_).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ClangPackage.CONDITIONAL_EXPRESSION__IF, null, msgs);
      if (newIf != null)
        msgs = ((InternalEObject)newIf).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ClangPackage.CONDITIONAL_EXPRESSION__IF, null, msgs);
      msgs = basicSetIf(newIf, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ClangPackage.CONDITIONAL_EXPRESSION__IF, newIf, newIf));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Expression getYes()
  {
    return yes;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetYes(Expression newYes, NotificationChain msgs)
  {
    Expression oldYes = yes;
    yes = newYes;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ClangPackage.CONDITIONAL_EXPRESSION__YES, oldYes, newYes);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setYes(Expression newYes)
  {
    if (newYes != yes)
    {
      NotificationChain msgs = null;
      if (yes != null)
        msgs = ((InternalEObject)yes).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ClangPackage.CONDITIONAL_EXPRESSION__YES, null, msgs);
      if (newYes != null)
        msgs = ((InternalEObject)newYes).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ClangPackage.CONDITIONAL_EXPRESSION__YES, null, msgs);
      msgs = basicSetYes(newYes, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ClangPackage.CONDITIONAL_EXPRESSION__YES, newYes, newYes));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Expression getNo()
  {
    return no;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetNo(Expression newNo, NotificationChain msgs)
  {
    Expression oldNo = no;
    no = newNo;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ClangPackage.CONDITIONAL_EXPRESSION__NO, oldNo, newNo);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNo(Expression newNo)
  {
    if (newNo != no)
    {
      NotificationChain msgs = null;
      if (no != null)
        msgs = ((InternalEObject)no).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ClangPackage.CONDITIONAL_EXPRESSION__NO, null, msgs);
      if (newNo != null)
        msgs = ((InternalEObject)newNo).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ClangPackage.CONDITIONAL_EXPRESSION__NO, null, msgs);
      msgs = basicSetNo(newNo, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ClangPackage.CONDITIONAL_EXPRESSION__NO, newNo, newNo));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case ClangPackage.CONDITIONAL_EXPRESSION__IF:
        return basicSetIf(null, msgs);
      case ClangPackage.CONDITIONAL_EXPRESSION__YES:
        return basicSetYes(null, msgs);
      case ClangPackage.CONDITIONAL_EXPRESSION__NO:
        return basicSetNo(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case ClangPackage.CONDITIONAL_EXPRESSION__IF:
        return getIf();
      case ClangPackage.CONDITIONAL_EXPRESSION__YES:
        return getYes();
      case ClangPackage.CONDITIONAL_EXPRESSION__NO:
        return getNo();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case ClangPackage.CONDITIONAL_EXPRESSION__IF:
        setIf((Expression)newValue);
        return;
      case ClangPackage.CONDITIONAL_EXPRESSION__YES:
        setYes((Expression)newValue);
        return;
      case ClangPackage.CONDITIONAL_EXPRESSION__NO:
        setNo((Expression)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case ClangPackage.CONDITIONAL_EXPRESSION__IF:
        setIf((Expression)null);
        return;
      case ClangPackage.CONDITIONAL_EXPRESSION__YES:
        setYes((Expression)null);
        return;
      case ClangPackage.CONDITIONAL_EXPRESSION__NO:
        setNo((Expression)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case ClangPackage.CONDITIONAL_EXPRESSION__IF:
        return if_ != null;
      case ClangPackage.CONDITIONAL_EXPRESSION__YES:
        return yes != null;
      case ClangPackage.CONDITIONAL_EXPRESSION__NO:
        return no != null;
    }
    return super.eIsSet(featureID);
  }

} //ConditionalExpressionImpl
