/**
 */
package org.xtext.dop.clang.clang.impl;

import org.eclipse.emf.ecore.EClass;

import org.xtext.dop.clang.clang.ClangPackage;
import org.xtext.dop.clang.clang.JumpStatementContinue;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Jump Statement Continue</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class JumpStatementContinueImpl extends JumpStatementImpl implements JumpStatementContinue
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected JumpStatementContinueImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ClangPackage.Literals.JUMP_STATEMENT_CONTINUE;
  }

} //JumpStatementContinueImpl
