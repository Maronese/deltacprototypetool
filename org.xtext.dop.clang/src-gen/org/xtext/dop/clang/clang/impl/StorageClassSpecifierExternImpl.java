/**
 */
package org.xtext.dop.clang.clang.impl;

import org.eclipse.emf.ecore.EClass;

import org.xtext.dop.clang.clang.ClangPackage;
import org.xtext.dop.clang.clang.StorageClassSpecifierExtern;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Storage Class Specifier Extern</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class StorageClassSpecifierExternImpl extends StorageClassSpecifierImpl implements StorageClassSpecifierExtern
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected StorageClassSpecifierExternImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ClangPackage.Literals.STORAGE_CLASS_SPECIFIER_EXTERN;
  }

} //StorageClassSpecifierExternImpl
