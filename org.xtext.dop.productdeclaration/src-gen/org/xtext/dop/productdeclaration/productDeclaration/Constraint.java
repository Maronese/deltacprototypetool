/**
 */
package org.xtext.dop.productdeclaration.productDeclaration;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.productdeclaration.productDeclaration.Constraint#getDelta <em>Delta</em>}</li>
 *   <li>{@link org.xtext.dop.productdeclaration.productDeclaration.Constraint#getExpr <em>Expr</em>}</li>
 *   <li>{@link org.xtext.dop.productdeclaration.productDeclaration.Constraint#isLast <em>Last</em>}</li>
 * </ul>
 *
 * @see org.xtext.dop.productdeclaration.productDeclaration.ProductDeclarationPackage#getConstraint()
 * @model
 * @generated
 */
public interface Constraint extends EObject
{
  /**
   * Returns the value of the '<em><b>Delta</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Delta</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Delta</em>' containment reference.
   * @see #setDelta(Element)
   * @see org.xtext.dop.productdeclaration.productDeclaration.ProductDeclarationPackage#getConstraint_Delta()
   * @model containment="true"
   * @generated
   */
  Element getDelta();

  /**
   * Sets the value of the '{@link org.xtext.dop.productdeclaration.productDeclaration.Constraint#getDelta <em>Delta</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Delta</em>' containment reference.
   * @see #getDelta()
   * @generated
   */
  void setDelta(Element value);

  /**
   * Returns the value of the '<em><b>Expr</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Expr</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Expr</em>' containment reference.
   * @see #setExpr(Expression)
   * @see org.xtext.dop.productdeclaration.productDeclaration.ProductDeclarationPackage#getConstraint_Expr()
   * @model containment="true"
   * @generated
   */
  Expression getExpr();

  /**
   * Sets the value of the '{@link org.xtext.dop.productdeclaration.productDeclaration.Constraint#getExpr <em>Expr</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Expr</em>' containment reference.
   * @see #getExpr()
   * @generated
   */
  void setExpr(Expression value);

  /**
   * Returns the value of the '<em><b>Last</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Last</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Last</em>' attribute.
   * @see #setLast(boolean)
   * @see org.xtext.dop.productdeclaration.productDeclaration.ProductDeclarationPackage#getConstraint_Last()
   * @model
   * @generated
   */
  boolean isLast();

  /**
   * Sets the value of the '{@link org.xtext.dop.productdeclaration.productDeclaration.Constraint#isLast <em>Last</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Last</em>' attribute.
   * @see #isLast()
   * @generated
   */
  void setLast(boolean value);

} // Constraint
