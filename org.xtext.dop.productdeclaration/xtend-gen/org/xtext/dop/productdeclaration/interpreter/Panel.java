package org.xtext.dop.productdeclaration.interpreter;

import com.google.common.base.Objects;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.xtext.dop.codebase.generator.Main;
import org.xtext.dop.productdeclaration.interpreter.GenerateListObservable;
import org.xtext.dop.productdeclaration.interpreter.Interpreter;
import org.xtext.dop.productdeclaration.productDeclaration.Element;
import org.xtext.dop.productdeclaration.productDeclaration.ListElem;
import org.xtext.dop.productdeclaration.productDeclaration.Product;

@SuppressWarnings("all")
public class Panel extends JFrame {
  /**
   * def public static void main(String[] args) {
   * var list = new ArrayList<String>
   * list.addAll("alpha", "beta", "gamma", "delta", "eta", "beta")
   * new Panel("test product line gui", list)
   * }
   */
  public static class SelectionListener implements ActionListener {
    private List<Product> product;
    
    private JTextArea text;
    
    public SelectionListener(final List<Product> product, final JTextArea text) {
      this.product = product;
      this.text = text;
    }
    
    @Override
    public void actionPerformed(final ActionEvent e) {
      Object _source = e.getSource();
      final JRadioButton button = ((JRadioButton) _source);
      final Function1<Product, Boolean> _function = (Product it) -> {
        String _name = it.getName();
        String _text = button.getText();
        return Boolean.valueOf(Objects.equal(_name, _text));
      };
      Product _findFirst = IterableExtensions.<Product>findFirst(this.product, _function);
      EList<ListElem> _list = _findFirst.getList();
      final Function1<ListElem, String> _function_1 = (ListElem it) -> {
        Element _elem = it.getElem();
        return _elem.getName();
      };
      List<String> _map = ListExtensions.<ListElem, String>map(_list, _function_1);
      String _join = IterableExtensions.join(_map, ", ");
      this.text.setText(_join);
    }
  }
  
  public static class ExitListener implements ActionListener {
    @Override
    public void actionPerformed(final ActionEvent arg0) {
      System.exit(0);
    }
  }
  
  public static class GenerateListener implements ActionListener {
    private ArrayList<JRadioButton> radioButtons;
    
    private List<Product> product;
    
    private Interpreter interpreter;
    
    private Main codebase;
    
    public GenerateListener(final ArrayList<JRadioButton> radioButtons, final List<Product> product, final Interpreter interpreter, final Main codebase) {
      this.radioButtons = radioButtons;
      this.product = product;
      this.interpreter = interpreter;
      this.codebase = codebase;
    }
    
    @Override
    public void actionPerformed(final ActionEvent arg0) {
      final Function1<JRadioButton, Boolean> _function = (JRadioButton it) -> {
        return Boolean.valueOf(it.isSelected());
      };
      final JRadioButton button = IterableExtensions.<JRadioButton>findFirst(this.radioButtons, _function);
      boolean _equals = Objects.equal(button, null);
      if (_equals) {
        return;
      }
      final GenerateListObservable obs = new GenerateListObservable(this.interpreter);
      obs.addObserver(this.codebase);
      final Function1<Product, Boolean> _function_1 = (Product it) -> {
        String _name = it.getName();
        String _text = button.getText();
        return Boolean.valueOf(Objects.equal(_name, _text));
      };
      Product _findFirst = IterableExtensions.<Product>findFirst(this.product, _function_1);
      obs.sendSelection(_findFirst);
      System.exit(0);
    }
  }
  
  private final Container c = this.getContentPane();
  
  private JPanel productPanel;
  
  private JPanel featurePanel;
  
  private JPanel optionPanel;
  
  private JPanel radioPanel;
  
  private JPanel textPanel;
  
  private JLabel productTitle;
  
  private JLabel featureTitle;
  
  private JTextArea features;
  
  private ButtonGroup group;
  
  private ArrayList<JRadioButton> radioButtons;
  
  private JButton exitButton;
  
  private JButton generateButton;
  
  private List<Product> products;
  
  private Interpreter interpreter;
  
  private Main codebase;
  
  public Panel(final String name, final List<Product> list, final Interpreter interpreter, final Main codebase) {
    super(name);
    this.products = list;
    this.interpreter = interpreter;
    this.codebase = codebase;
    this.setup(list);
  }
  
  public void setup(final List<Product> list) {
    ButtonGroup _buttonGroup = new ButtonGroup();
    this.group = _buttonGroup;
    JPanel _jPanel = new JPanel();
    this.productPanel = _jPanel;
    JPanel _jPanel_1 = new JPanel();
    this.featurePanel = _jPanel_1;
    JPanel _jPanel_2 = new JPanel();
    this.optionPanel = _jPanel_2;
    JPanel _jPanel_3 = new JPanel();
    this.radioPanel = _jPanel_3;
    JPanel _jPanel_4 = new JPanel();
    this.textPanel = _jPanel_4;
    JLabel _jLabel = new JLabel("       Products viable:");
    this.productTitle = _jLabel;
    JLabel _jLabel_1 = new JLabel("       Features selected:");
    this.featureTitle = _jLabel_1;
    JTextArea _jTextArea = new JTextArea();
    this.features = _jTextArea;
    ArrayList<JRadioButton> _arrayList = new ArrayList<JRadioButton>();
    this.radioButtons = _arrayList;
    JButton _jButton = new JButton("Close");
    this.exitButton = _jButton;
    JButton _jButton_1 = new JButton("Generate");
    this.generateButton = _jButton_1;
    for (final Product prod : list) {
      {
        String _name = prod.getName();
        final JRadioButton bt = new JRadioButton(_name);
        this.radioButtons.add(bt);
        this.group.add(bt);
        this.radioPanel.add(bt);
        Panel.SelectionListener _selectionListener = new Panel.SelectionListener(this.products, this.features);
        bt.addActionListener(_selectionListener);
      }
    }
    GridLayout _gridLayout = new GridLayout(2, 1);
    this.productPanel.setLayout(_gridLayout);
    this.productPanel.add(this.productTitle);
    this.productPanel.add(this.radioPanel);
    GridLayout _gridLayout_1 = new GridLayout(2, 1);
    this.featurePanel.setLayout(_gridLayout_1);
    this.features.setColumns(40);
    this.features.setLineWrap(true);
    this.features.setRows(5);
    this.features.setWrapStyleWord(true);
    this.features.setEditable(false);
    this.textPanel.add(this.features);
    this.featurePanel.add(this.featureTitle);
    this.featurePanel.add(this.textPanel);
    this.optionPanel.add(this.exitButton);
    Panel.ExitListener _exitListener = new Panel.ExitListener();
    this.exitButton.addActionListener(_exitListener);
    this.optionPanel.add(this.generateButton);
    Panel.GenerateListener _generateListener = new Panel.GenerateListener(this.radioButtons, this.products, this.interpreter, this.codebase);
    this.generateButton.addActionListener(_generateListener);
    BorderLayout _borderLayout = new BorderLayout();
    this.c.setLayout(_borderLayout);
    this.c.add(this.productPanel, BorderLayout.NORTH);
    this.c.add(this.featurePanel, BorderLayout.CENTER);
    this.c.add(this.optionPanel, BorderLayout.SOUTH);
    this.setSize(500, 250);
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    this.setVisible(true);
  }
}
