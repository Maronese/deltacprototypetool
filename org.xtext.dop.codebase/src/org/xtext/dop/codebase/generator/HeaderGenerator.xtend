package org.xtext.dop.codebase.generator

import org.xtext.dop.rappresentation.Project
import org.xtext.dop.clang.clang.Model
import org.xtext.dop.clang.inspector.Inspector
import org.xtext.dop.clang.clang.Declaration
import org.xtext.dop.clang.clang.FunctionInitializer
import org.xtext.dop.codebase.printer.Printer
import org.xtext.dop.rappresentation.Resource
import java.io.File
import java.io.PrintWriter

class HeaderGenerator {
	
	Project project
	extension Inspector inspector
	extension Printer printer
	String path
	
	new(Project project) {
		this.project = project
		this.inspector = new Inspector
		this.path = "spl_product_"+project.projectName
	}
	
	def generateHeaders() {
		for(Resource resource : project.values) {
			resource.generateHeader.createNewFile
		}
	}
	
	def private generateHeader(Resource resource){
		printer = new Printer(resource)
		val model = resource.source
		val name = resource.name
		val file = new File(path+'/'+name+"_prototypes.h")
		val writer = new PrintWriter(file)
		
		if(file.exists()) file.delete();
		writer.print(model.generateHeader)
		writer.close
		return file
	}
	
	def private generateHeader(Model model) {
		var string = ""
		val index = model.indexFirstFunction
		var Declaration funct 
		for(var i=index;i<model.declarations.size();i++) {
			funct = model.declarations.get(i)
			funct.eAllContents.filter(typeof(FunctionInitializer)).next.block = null
			string += funct.print+"\n"
		}
		return string
	}
}