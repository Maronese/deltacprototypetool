/**
 */
package org.xtext.dop.clang.clang.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.xtext.dop.clang.clang.ClangPackage;
import org.xtext.dop.clang.clang.GenericDeclarationId;
import org.xtext.dop.clang.clang.GenericDeclarationIdPostfix;
import org.xtext.dop.clang.clang.GenericDeclarationInnerId;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Generic Declaration Id</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.clang.clang.impl.GenericDeclarationIdImpl#getInner <em>Inner</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.impl.GenericDeclarationIdImpl#getPosts <em>Posts</em>}</li>
 * </ul>
 *
 * @generated
 */
public class GenericDeclarationIdImpl extends MinimalEObjectImpl.Container implements GenericDeclarationId
{
  /**
   * The cached value of the '{@link #getInner() <em>Inner</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getInner()
   * @generated
   * @ordered
   */
  protected GenericDeclarationInnerId inner;

  /**
   * The cached value of the '{@link #getPosts() <em>Posts</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPosts()
   * @generated
   * @ordered
   */
  protected EList<GenericDeclarationIdPostfix> posts;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected GenericDeclarationIdImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ClangPackage.Literals.GENERIC_DECLARATION_ID;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GenericDeclarationInnerId getInner()
  {
    return inner;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetInner(GenericDeclarationInnerId newInner, NotificationChain msgs)
  {
    GenericDeclarationInnerId oldInner = inner;
    inner = newInner;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ClangPackage.GENERIC_DECLARATION_ID__INNER, oldInner, newInner);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setInner(GenericDeclarationInnerId newInner)
  {
    if (newInner != inner)
    {
      NotificationChain msgs = null;
      if (inner != null)
        msgs = ((InternalEObject)inner).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ClangPackage.GENERIC_DECLARATION_ID__INNER, null, msgs);
      if (newInner != null)
        msgs = ((InternalEObject)newInner).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ClangPackage.GENERIC_DECLARATION_ID__INNER, null, msgs);
      msgs = basicSetInner(newInner, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ClangPackage.GENERIC_DECLARATION_ID__INNER, newInner, newInner));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<GenericDeclarationIdPostfix> getPosts()
  {
    if (posts == null)
    {
      posts = new EObjectContainmentEList<GenericDeclarationIdPostfix>(GenericDeclarationIdPostfix.class, this, ClangPackage.GENERIC_DECLARATION_ID__POSTS);
    }
    return posts;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case ClangPackage.GENERIC_DECLARATION_ID__INNER:
        return basicSetInner(null, msgs);
      case ClangPackage.GENERIC_DECLARATION_ID__POSTS:
        return ((InternalEList<?>)getPosts()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case ClangPackage.GENERIC_DECLARATION_ID__INNER:
        return getInner();
      case ClangPackage.GENERIC_DECLARATION_ID__POSTS:
        return getPosts();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case ClangPackage.GENERIC_DECLARATION_ID__INNER:
        setInner((GenericDeclarationInnerId)newValue);
        return;
      case ClangPackage.GENERIC_DECLARATION_ID__POSTS:
        getPosts().clear();
        getPosts().addAll((Collection<? extends GenericDeclarationIdPostfix>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case ClangPackage.GENERIC_DECLARATION_ID__INNER:
        setInner((GenericDeclarationInnerId)null);
        return;
      case ClangPackage.GENERIC_DECLARATION_ID__POSTS:
        getPosts().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case ClangPackage.GENERIC_DECLARATION_ID__INNER:
        return inner != null;
      case ClangPackage.GENERIC_DECLARATION_ID__POSTS:
        return posts != null && !posts.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //GenericDeclarationIdImpl
