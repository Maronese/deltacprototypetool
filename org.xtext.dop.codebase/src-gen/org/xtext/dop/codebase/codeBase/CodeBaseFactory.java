/**
 */
package org.xtext.dop.codebase.codeBase;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.xtext.dop.codebase.codeBase.CodeBasePackage
 * @generated
 */
public interface CodeBaseFactory extends EFactory
{
  /**
   * The singleton instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  CodeBaseFactory eINSTANCE = org.xtext.dop.codebase.codeBase.impl.CodeBaseFactoryImpl.init();

  /**
   * Returns a new object of class '<em>Delta Module</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Delta Module</em>'.
   * @generated
   */
  DeltaModule createDeltaModule();

  /**
   * Returns a new object of class '<em>File Operation</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>File Operation</em>'.
   * @generated
   */
  FileOperation createFileOperation();

  /**
   * Returns a new object of class '<em>Add Source</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Add Source</em>'.
   * @generated
   */
  AddSource createAddSource();

  /**
   * Returns a new object of class '<em>Remove Source</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Remove Source</em>'.
   * @generated
   */
  RemoveSource createRemoveSource();

  /**
   * Returns a new object of class '<em>Source Operation</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Source Operation</em>'.
   * @generated
   */
  SourceOperation createSourceOperation();

  /**
   * Returns a new object of class '<em>Adds</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Adds</em>'.
   * @generated
   */
  Adds createAdds();

  /**
   * Returns a new object of class '<em>Removes</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Removes</em>'.
   * @generated
   */
  Removes createRemoves();

  /**
   * Returns a new object of class '<em>Modifies</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Modifies</em>'.
   * @generated
   */
  Modifies createModifies();

  /**
   * Returns a new object of class '<em>Add Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Add Declaration</em>'.
   * @generated
   */
  AddDeclaration createAddDeclaration();

  /**
   * Returns a new object of class '<em>Add Standard Include</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Add Standard Include</em>'.
   * @generated
   */
  AddStandardInclude createAddStandardInclude();

  /**
   * Returns a new object of class '<em>Add File Include</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Add File Include</em>'.
   * @generated
   */
  AddFileInclude createAddFileInclude();

  /**
   * Returns a new object of class '<em>Remove Global</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Remove Global</em>'.
   * @generated
   */
  RemoveGlobal createRemoveGlobal();

  /**
   * Returns a new object of class '<em>Remove Standard Include</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Remove Standard Include</em>'.
   * @generated
   */
  RemoveStandardInclude createRemoveStandardInclude();

  /**
   * Returns a new object of class '<em>Remove File Include</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Remove File Include</em>'.
   * @generated
   */
  RemoveFileInclude createRemoveFileInclude();

  /**
   * Returns a new object of class '<em>Modify Function</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Modify Function</em>'.
   * @generated
   */
  ModifyFunction createModifyFunction();

  /**
   * Returns the package supported by this factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the package supported by this factory.
   * @generated
   */
  CodeBasePackage getCodeBasePackage();

} //CodeBaseFactory
