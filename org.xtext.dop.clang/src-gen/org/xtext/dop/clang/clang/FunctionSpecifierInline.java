/**
 */
package org.xtext.dop.clang.clang;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Function Specifier Inline</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getFunctionSpecifierInline()
 * @model
 * @generated
 */
public interface FunctionSpecifierInline extends FunctionSpecifier
{
} // FunctionSpecifierInline
