/**
 */
package org.xtext.dop.codebase.codeBase;

import org.eclipse.emf.common.util.EList;

import org.xtext.dop.clang.clang.Source;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Modifies</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.codebase.codeBase.Modifies#getFileName <em>File Name</em>}</li>
 *   <li>{@link org.xtext.dop.codebase.codeBase.Modifies#getAction <em>Action</em>}</li>
 * </ul>
 *
 * @see org.xtext.dop.codebase.codeBase.CodeBasePackage#getModifies()
 * @model
 * @generated
 */
public interface Modifies extends FileOperation
{
  /**
   * Returns the value of the '<em><b>File Name</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>File Name</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>File Name</em>' containment reference.
   * @see #setFileName(Source)
   * @see org.xtext.dop.codebase.codeBase.CodeBasePackage#getModifies_FileName()
   * @model containment="true"
   * @generated
   */
  Source getFileName();

  /**
   * Sets the value of the '{@link org.xtext.dop.codebase.codeBase.Modifies#getFileName <em>File Name</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>File Name</em>' containment reference.
   * @see #getFileName()
   * @generated
   */
  void setFileName(Source value);

  /**
   * Returns the value of the '<em><b>Action</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.dop.codebase.codeBase.SourceOperation}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Action</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Action</em>' containment reference list.
   * @see org.xtext.dop.codebase.codeBase.CodeBasePackage#getModifies_Action()
   * @model containment="true"
   * @generated
   */
  EList<SourceOperation> getAction();

} // Modifies
