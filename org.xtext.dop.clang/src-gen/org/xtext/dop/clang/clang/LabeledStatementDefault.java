/**
 */
package org.xtext.dop.clang.clang;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Labeled Statement Default</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getLabeledStatementDefault()
 * @model
 * @generated
 */
public interface LabeledStatementDefault extends LabeledStatement
{
} // LabeledStatementDefault
