/**
 */
package org.xtext.dop.clang.clang;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Compound Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.clang.clang.CompoundStatement#getInner <em>Inner</em>}</li>
 * </ul>
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getCompoundStatement()
 * @model
 * @generated
 */
public interface CompoundStatement extends Statements
{
  /**
   * Returns the value of the '<em><b>Inner</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.dop.clang.clang.BlockItem}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Inner</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Inner</em>' containment reference list.
   * @see org.xtext.dop.clang.clang.ClangPackage#getCompoundStatement_Inner()
   * @model containment="true"
   * @generated
   */
  EList<BlockItem> getInner();

} // CompoundStatement
