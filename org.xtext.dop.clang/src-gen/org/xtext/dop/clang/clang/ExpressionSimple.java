/**
 */
package org.xtext.dop.clang.clang;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Expression Simple</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.clang.clang.ExpressionSimple#getExp <em>Exp</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.ExpressionSimple#getNext <em>Next</em>}</li>
 * </ul>
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getExpressionSimple()
 * @model
 * @generated
 */
public interface ExpressionSimple extends Expression
{
  /**
   * Returns the value of the '<em><b>Exp</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Exp</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Exp</em>' containment reference.
   * @see #setExp(Expression)
   * @see org.xtext.dop.clang.clang.ClangPackage#getExpressionSimple_Exp()
   * @model containment="true"
   * @generated
   */
  Expression getExp();

  /**
   * Sets the value of the '{@link org.xtext.dop.clang.clang.ExpressionSimple#getExp <em>Exp</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Exp</em>' containment reference.
   * @see #getExp()
   * @generated
   */
  void setExp(Expression value);

  /**
   * Returns the value of the '<em><b>Next</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.dop.clang.clang.PostfixExpressionPostfix}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Next</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Next</em>' containment reference list.
   * @see org.xtext.dop.clang.clang.ClangPackage#getExpressionSimple_Next()
   * @model containment="true"
   * @generated
   */
  EList<PostfixExpressionPostfix> getNext();

} // ExpressionSimple
