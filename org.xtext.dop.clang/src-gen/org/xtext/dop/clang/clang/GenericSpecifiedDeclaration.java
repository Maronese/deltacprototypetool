/**
 */
package org.xtext.dop.clang.clang;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Generic Specified Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.clang.clang.GenericSpecifiedDeclaration#getSpecs <em>Specs</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.GenericSpecifiedDeclaration#getType <em>Type</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.GenericSpecifiedDeclaration#getDecl <em>Decl</em>}</li>
 * </ul>
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getGenericSpecifiedDeclaration()
 * @model
 * @generated
 */
public interface GenericSpecifiedDeclaration extends Declaration
{
  /**
   * Returns the value of the '<em><b>Specs</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.dop.clang.clang.DeclarationSpecifier}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Specs</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Specs</em>' containment reference list.
   * @see org.xtext.dop.clang.clang.ClangPackage#getGenericSpecifiedDeclaration_Specs()
   * @model containment="true"
   * @generated
   */
  EList<DeclarationSpecifier> getSpecs();

  /**
   * Returns the value of the '<em><b>Type</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.dop.clang.clang.SimpleType}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' containment reference list.
   * @see org.xtext.dop.clang.clang.ClangPackage#getGenericSpecifiedDeclaration_Type()
   * @model containment="true"
   * @generated
   */
  EList<SimpleType> getType();

  /**
   * Returns the value of the '<em><b>Decl</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Decl</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Decl</em>' containment reference.
   * @see #setDecl(GenericDeclarationElements)
   * @see org.xtext.dop.clang.clang.ClangPackage#getGenericSpecifiedDeclaration_Decl()
   * @model containment="true"
   * @generated
   */
  GenericDeclarationElements getDecl();

  /**
   * Sets the value of the '{@link org.xtext.dop.clang.clang.GenericSpecifiedDeclaration#getDecl <em>Decl</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Decl</em>' containment reference.
   * @see #getDecl()
   * @generated
   */
  void setDecl(GenericDeclarationElements value);

} // GenericSpecifiedDeclaration
