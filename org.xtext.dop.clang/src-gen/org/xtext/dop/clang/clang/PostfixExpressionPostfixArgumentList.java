/**
 */
package org.xtext.dop.clang.clang;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Postfix Expression Postfix Argument List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.clang.clang.PostfixExpressionPostfixArgumentList#getLeft <em>Left</em>}</li>
 *   <li>{@link org.xtext.dop.clang.clang.PostfixExpressionPostfixArgumentList#getNext <em>Next</em>}</li>
 * </ul>
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getPostfixExpressionPostfixArgumentList()
 * @model
 * @generated
 */
public interface PostfixExpressionPostfixArgumentList extends PostfixExpressionPostfix
{
  /**
   * Returns the value of the '<em><b>Left</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.dop.clang.clang.Expression}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Left</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Left</em>' containment reference list.
   * @see org.xtext.dop.clang.clang.ClangPackage#getPostfixExpressionPostfixArgumentList_Left()
   * @model containment="true"
   * @generated
   */
  EList<Expression> getLeft();

  /**
   * Returns the value of the '<em><b>Next</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.dop.clang.clang.NextAsignement}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Next</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Next</em>' containment reference list.
   * @see org.xtext.dop.clang.clang.ClangPackage#getPostfixExpressionPostfixArgumentList_Next()
   * @model containment="true"
   * @generated
   */
  EList<NextAsignement> getNext();

} // PostfixExpressionPostfixArgumentList
