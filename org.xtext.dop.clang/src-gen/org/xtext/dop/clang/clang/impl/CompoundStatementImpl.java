/**
 */
package org.xtext.dop.clang.clang.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.xtext.dop.clang.clang.BlockItem;
import org.xtext.dop.clang.clang.ClangPackage;
import org.xtext.dop.clang.clang.CompoundStatement;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Compound Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.clang.clang.impl.CompoundStatementImpl#getInner <em>Inner</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CompoundStatementImpl extends StatementsImpl implements CompoundStatement
{
  /**
   * The cached value of the '{@link #getInner() <em>Inner</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getInner()
   * @generated
   * @ordered
   */
  protected EList<BlockItem> inner;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected CompoundStatementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ClangPackage.Literals.COMPOUND_STATEMENT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<BlockItem> getInner()
  {
    if (inner == null)
    {
      inner = new EObjectContainmentEList<BlockItem>(BlockItem.class, this, ClangPackage.COMPOUND_STATEMENT__INNER);
    }
    return inner;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case ClangPackage.COMPOUND_STATEMENT__INNER:
        return ((InternalEList<?>)getInner()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case ClangPackage.COMPOUND_STATEMENT__INNER:
        return getInner();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case ClangPackage.COMPOUND_STATEMENT__INNER:
        getInner().clear();
        getInner().addAll((Collection<? extends BlockItem>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case ClangPackage.COMPOUND_STATEMENT__INNER:
        getInner().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case ClangPackage.COMPOUND_STATEMENT__INNER:
        return inner != null && !inner.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //CompoundStatementImpl
