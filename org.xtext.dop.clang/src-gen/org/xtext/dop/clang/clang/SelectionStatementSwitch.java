/**
 */
package org.xtext.dop.clang.clang;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Selection Statement Switch</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.clang.clang.SelectionStatementSwitch#getContent <em>Content</em>}</li>
 * </ul>
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getSelectionStatementSwitch()
 * @model
 * @generated
 */
public interface SelectionStatementSwitch extends SelectionStatement
{
  /**
   * Returns the value of the '<em><b>Content</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.dop.clang.clang.LabeledStatement}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Content</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Content</em>' containment reference list.
   * @see org.xtext.dop.clang.clang.ClangPackage#getSelectionStatementSwitch_Content()
   * @model containment="true"
   * @generated
   */
  EList<LabeledStatement> getContent();

} // SelectionStatementSwitch
