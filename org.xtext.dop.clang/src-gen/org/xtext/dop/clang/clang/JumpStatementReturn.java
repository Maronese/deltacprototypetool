/**
 */
package org.xtext.dop.clang.clang;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Jump Statement Return</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.clang.clang.JumpStatementReturn#getExp <em>Exp</em>}</li>
 * </ul>
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getJumpStatementReturn()
 * @model
 * @generated
 */
public interface JumpStatementReturn extends JumpStatement
{
  /**
   * Returns the value of the '<em><b>Exp</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Exp</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Exp</em>' containment reference.
   * @see #setExp(Expression)
   * @see org.xtext.dop.clang.clang.ClangPackage#getJumpStatementReturn_Exp()
   * @model containment="true"
   * @generated
   */
  Expression getExp();

  /**
   * Sets the value of the '{@link org.xtext.dop.clang.clang.JumpStatementReturn#getExp <em>Exp</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Exp</em>' containment reference.
   * @see #getExp()
   * @generated
   */
  void setExp(Expression value);

} // JumpStatementReturn
