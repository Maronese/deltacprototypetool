package org.xtext.dop.rappresentation;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.xtext.dop.clang.clang.Model;

import com.google.inject.Inject;


public class DopResource implements Resource {
	
	private String name;
	private Model source;
	private String createdBy;
	private HashMap<String, List<String>> log;
	
	public DopResource(String name, Model source) {
		this.source = source;
		this.name = name;
		this.log = new HashMap<>();
	}
	
	@Override
	public Model getSource() {
		return source;
	}
	
	@Override
	public List<String> getLogGlobalName(String global) {
		return log.get(global);
	}

	@Override
	public boolean addLogGlobalName(String global) {
		return log.put(global, new LinkedList<String>()) == null;
	}

	@Override
	public boolean removeLogGlobalName(String global) {
		return log.remove(global) != null;
	}

	@Override
	public boolean updateLogGlobalName(String global, String update) {
		return log.containsKey(global)? log.get(global).add(update) : false;
	}

	@Override
	public void setCreator(String delta) {
		this.createdBy = delta;
	}

	@Override
	public String getCreator() {
		return createdBy;
	}
	
	@Override
	public String getName() {
		return name;
	}
}
