/**
 */
package org.xtext.dop.clang.clang.impl;

import org.eclipse.emf.ecore.EClass;

import org.xtext.dop.clang.clang.ClangPackage;
import org.xtext.dop.clang.clang.TypeQualifierConst;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Type Qualifier Const</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TypeQualifierConstImpl extends TypeQualifierImpl implements TypeQualifierConst
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TypeQualifierConstImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ClangPackage.Literals.TYPE_QUALIFIER_CONST;
  }

} //TypeQualifierConstImpl
