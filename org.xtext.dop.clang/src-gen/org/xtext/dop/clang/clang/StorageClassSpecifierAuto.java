/**
 */
package org.xtext.dop.clang.clang;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Storage Class Specifier Auto</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getStorageClassSpecifierAuto()
 * @model
 * @generated
 */
public interface StorageClassSpecifierAuto extends StorageClassSpecifier
{
} // StorageClassSpecifierAuto
