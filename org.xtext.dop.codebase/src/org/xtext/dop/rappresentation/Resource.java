package org.xtext.dop.rappresentation;

import java.util.List;

import org.xtext.dop.clang.clang.Model;

public interface Resource {
	
	//ottieni albero parsificazione
	public Model getSource();
	
	//ottieni lista log per nome globale
	public List<String> getLogGlobalName(String global);
	
	//aggiungi nome globale al log
	public boolean addLogGlobalName(String global);
	
	//togli nome globale al log
	public boolean removeLogGlobalName(String global);
	
	//aggiorna log per nome globale
	public boolean updateLogGlobalName(String global, String update);
	
	//imposta ceratore risorsa
	public void setCreator(String delta);
	
	public String getCreator();
	
	public String getName();
}
