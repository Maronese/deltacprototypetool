/**
 */
package org.xtext.dop.clang.clang.impl;

import org.eclipse.emf.ecore.EClass;

import org.xtext.dop.clang.clang.ClangPackage;
import org.xtext.dop.clang.clang.GenericDeclaration2;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Generic Declaration2</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class GenericDeclaration2Impl extends GenericDeclarationIdImpl implements GenericDeclaration2
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected GenericDeclaration2Impl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ClangPackage.Literals.GENERIC_DECLARATION2;
  }

} //GenericDeclaration2Impl
