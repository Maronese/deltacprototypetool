/**
 */
package org.xtext.dop.clang.clang.impl;

import org.eclipse.emf.ecore.EClass;

import org.xtext.dop.clang.clang.BaseTypeComplex;
import org.xtext.dop.clang.clang.ClangPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Base Type Complex</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BaseTypeComplexImpl extends BaseTypeImpl implements BaseTypeComplex
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected BaseTypeComplexImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ClangPackage.Literals.BASE_TYPE_COMPLEX;
  }

} //BaseTypeComplexImpl
