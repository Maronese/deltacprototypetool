/*
 * generated by Xtext
 */
package org.xtext.dop.productdeclaration.ui.contentassist.antlr;

import java.util.Collection;
import java.util.Map;
import java.util.HashMap;

import org.antlr.runtime.RecognitionException;
import org.eclipse.xtext.AbstractElement;
import org.eclipse.xtext.ui.editor.contentassist.antlr.AbstractContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.FollowElement;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;

import com.google.inject.Inject;

import org.xtext.dop.productdeclaration.services.ProductDeclarationGrammarAccess;

public class ProductDeclarationParser extends AbstractContentAssistParser {
	
	@Inject
	private ProductDeclarationGrammarAccess grammarAccess;
	
	private Map<AbstractElement, String> nameMappings;
	
	@Override
	protected org.xtext.dop.productdeclaration.ui.contentassist.antlr.internal.InternalProductDeclarationParser createParser() {
		org.xtext.dop.productdeclaration.ui.contentassist.antlr.internal.InternalProductDeclarationParser result = new org.xtext.dop.productdeclaration.ui.contentassist.antlr.internal.InternalProductDeclarationParser(null);
		result.setGrammarAccess(grammarAccess);
		return result;
	}
	
	@Override
	protected String getRuleName(AbstractElement element) {
		if (nameMappings == null) {
			nameMappings = new HashMap<AbstractElement, String>() {
				private static final long serialVersionUID = 1L;
				{
					put(grammarAccess.getPrimaryAccess().getAlternatives(), "rule__Primary__Alternatives");
					put(grammarAccess.getProductDeclarationAccess().getGroup(), "rule__ProductDeclaration__Group__0");
					put(grammarAccess.getProductAccess().getGroup(), "rule__Product__Group__0");
					put(grammarAccess.getPartitionAccess().getGroup(), "rule__Partition__Group__0");
					put(grammarAccess.getConstraintAccess().getGroup(), "rule__Constraint__Group__0");
					put(grammarAccess.getListElemAccess().getGroup(), "rule__ListElem__Group__0");
					put(grammarAccess.getCoreAccess().getGroup(), "rule__Core__Group__0");
					put(grammarAccess.getOrAccess().getGroup(), "rule__Or__Group__0");
					put(grammarAccess.getOrAccess().getGroup_1(), "rule__Or__Group_1__0");
					put(grammarAccess.getAndAccess().getGroup(), "rule__And__Group__0");
					put(grammarAccess.getAndAccess().getGroup_1(), "rule__And__Group_1__0");
					put(grammarAccess.getPrimaryAccess().getGroup_1(), "rule__Primary__Group_1__0");
					put(grammarAccess.getPrimaryAccess().getGroup_2(), "rule__Primary__Group_2__0");
					put(grammarAccess.getProductDeclarationAccess().getNameAssignment_1(), "rule__ProductDeclaration__NameAssignment_1");
					put(grammarAccess.getProductDeclarationAccess().getFeaturesAssignment_6(), "rule__ProductDeclaration__FeaturesAssignment_6");
					put(grammarAccess.getProductDeclarationAccess().getDeltasAssignment_11(), "rule__ProductDeclaration__DeltasAssignment_11");
					put(grammarAccess.getProductDeclarationAccess().getConstraintsAssignment_15(), "rule__ProductDeclaration__ConstraintsAssignment_15");
					put(grammarAccess.getProductDeclarationAccess().getPartitionsAssignment_19(), "rule__ProductDeclaration__PartitionsAssignment_19");
					put(grammarAccess.getProductDeclarationAccess().getProductsAssignment_23(), "rule__ProductDeclaration__ProductsAssignment_23");
					put(grammarAccess.getFeaturesAccess().getListAssignment(), "rule__Features__ListAssignment");
					put(grammarAccess.getDeltasAccess().getListAssignment(), "rule__Deltas__ListAssignment");
					put(grammarAccess.getProductsAccess().getListAssignment(), "rule__Products__ListAssignment");
					put(grammarAccess.getProductAccess().getNameAssignment_0(), "rule__Product__NameAssignment_0");
					put(grammarAccess.getProductAccess().getListAssignment_3(), "rule__Product__ListAssignment_3");
					put(grammarAccess.getPartitionsAccess().getListAssignment(), "rule__Partitions__ListAssignment");
					put(grammarAccess.getPartitionAccess().getConstrAssignment_0(), "rule__Partition__ConstrAssignment_0");
					put(grammarAccess.getConstraintAccess().getDeltaAssignment_1(), "rule__Constraint__DeltaAssignment_1");
					put(grammarAccess.getConstraintAccess().getExprAssignment_5(), "rule__Constraint__ExprAssignment_5");
					put(grammarAccess.getConstraintAccess().getLastAssignment_7(), "rule__Constraint__LastAssignment_7");
					put(grammarAccess.getListElemAccess().getElemAssignment_0(), "rule__ListElem__ElemAssignment_0");
					put(grammarAccess.getListElemAccess().getLastAssignment_1(), "rule__ListElem__LastAssignment_1");
					put(grammarAccess.getElementAccess().getNameAssignment(), "rule__Element__NameAssignment");
					put(grammarAccess.getCoreAccess().getExprAssignment_0(), "rule__Core__ExprAssignment_0");
					put(grammarAccess.getOrAccess().getRightAssignment_1_2(), "rule__Or__RightAssignment_1_2");
					put(grammarAccess.getAndAccess().getRightAssignment_1_2(), "rule__And__RightAssignment_1_2");
					put(grammarAccess.getPrimaryAccess().getExprAssignment_1_2(), "rule__Primary__ExprAssignment_1_2");
					put(grammarAccess.getPrimaryAccess().getValueAssignment_2_2(), "rule__Primary__ValueAssignment_2_2");
				}
			};
		}
		return nameMappings.get(element);
	}
	
	@Override
	protected Collection<FollowElement> getFollowElements(AbstractInternalContentAssistParser parser) {
		try {
			org.xtext.dop.productdeclaration.ui.contentassist.antlr.internal.InternalProductDeclarationParser typedParser = (org.xtext.dop.productdeclaration.ui.contentassist.antlr.internal.InternalProductDeclarationParser) parser;
			typedParser.entryRuleProductDeclaration();
			return typedParser.getFollowElements();
		} catch(RecognitionException ex) {
			throw new RuntimeException(ex);
		}		
	}
	
	@Override
	protected String[] getInitialHiddenTokens() {
		return new String[] { "RULE_WS", "RULE_ML_COMMENT", "RULE_SL_COMMENT" };
	}
	
	public ProductDeclarationGrammarAccess getGrammarAccess() {
		return this.grammarAccess;
	}
	
	public void setGrammarAccess(ProductDeclarationGrammarAccess grammarAccess) {
		this.grammarAccess = grammarAccess;
	}
}
