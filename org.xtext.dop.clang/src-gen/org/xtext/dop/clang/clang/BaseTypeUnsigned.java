/**
 */
package org.xtext.dop.clang.clang;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Base Type Unsigned</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getBaseTypeUnsigned()
 * @model
 * @generated
 */
public interface BaseTypeUnsigned extends BaseType
{
} // BaseTypeUnsigned
