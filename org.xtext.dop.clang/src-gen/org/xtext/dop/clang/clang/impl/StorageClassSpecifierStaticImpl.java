/**
 */
package org.xtext.dop.clang.clang.impl;

import org.eclipse.emf.ecore.EClass;

import org.xtext.dop.clang.clang.ClangPackage;
import org.xtext.dop.clang.clang.StorageClassSpecifierStatic;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Storage Class Specifier Static</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class StorageClassSpecifierStaticImpl extends StorageClassSpecifierImpl implements StorageClassSpecifierStatic
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected StorageClassSpecifierStaticImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ClangPackage.Literals.STORAGE_CLASS_SPECIFIER_STATIC;
  }

} //StorageClassSpecifierStaticImpl
