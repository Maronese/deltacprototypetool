/**
 */
package org.xtext.dop.clang.clang;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Jump Statement Continue</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getJumpStatementContinue()
 * @model
 * @generated
 */
public interface JumpStatementContinue extends JumpStatement
{
} // JumpStatementContinue
