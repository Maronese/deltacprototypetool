/**
 */
package org.xtext.dop.clang.clang;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Generic Declaration Initializer</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getGenericDeclarationInitializer()
 * @model
 * @generated
 */
public interface GenericDeclarationInitializer extends EObject
{
} // GenericDeclarationInitializer
