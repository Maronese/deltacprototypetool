package org.xtext.dop.rappresentation;

import java.util.Collection;

public interface Project {

	//aggiungi risorsa
	public boolean addResource(String path, Resource fileContent);
	
	//rimuovi risorsa
	public boolean removeResource(String path);
	
	//ottieni risorsa
	public Resource getResource(String path);
	
	//imposta nome progetto
	public void setProjectName(String name);
	
	public String getProjectName();
	
	public Collection<Resource> values();
}
