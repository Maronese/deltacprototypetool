/**
 */
package org.xtext.dop.clang.clang;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Specified Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.clang.clang.SpecifiedType#getPar <em>Par</em>}</li>
 * </ul>
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getSpecifiedType()
 * @model
 * @generated
 */
public interface SpecifiedType extends EObject
{
  /**
   * Returns the value of the '<em><b>Par</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Par</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Par</em>' containment reference.
   * @see #setPar(Parameter)
   * @see org.xtext.dop.clang.clang.ClangPackage#getSpecifiedType_Par()
   * @model containment="true"
   * @generated
   */
  Parameter getPar();

  /**
   * Sets the value of the '{@link org.xtext.dop.clang.clang.SpecifiedType#getPar <em>Par</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Par</em>' containment reference.
   * @see #getPar()
   * @generated
   */
  void setPar(Parameter value);

} // SpecifiedType
