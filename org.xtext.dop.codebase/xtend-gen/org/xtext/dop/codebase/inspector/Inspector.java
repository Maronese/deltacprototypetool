package org.xtext.dop.codebase.inspector;

import com.google.common.collect.Iterators;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.xtext.dop.clang.clang.CompleteName;
import org.xtext.dop.clang.clang.SimpleName;
import org.xtext.dop.clang.clang.Source;
import org.xtext.dop.codebase.codeBase.AddSource;
import org.xtext.dop.codebase.codeBase.Adds;
import org.xtext.dop.codebase.codeBase.DeltaModule;
import org.xtext.dop.codebase.codeBase.FileOperation;
import org.xtext.dop.codebase.codeBase.Modifies;
import org.xtext.dop.codebase.codeBase.RemoveSource;
import org.xtext.dop.codebase.codeBase.Removes;

@SuppressWarnings("all")
public class Inspector {
  public Set<String> inspect(final Resource resource) {
    TreeIterator<EObject> _allContents = resource.getAllContents();
    Iterator<DeltaModule> _filter = Iterators.<DeltaModule>filter(_allContents, DeltaModule.class);
    DeltaModule _next = _filter.next();
    return this.deals(_next);
  }
  
  private Set<String> deals(final DeltaModule delta) {
    Set<String> _xblockexpression = null;
    {
      final Set<String> set = new HashSet<String>();
      EList<FileOperation> _body = delta.getBody();
      for (final FileOperation op : _body) {
        boolean _matched = false;
        if (!_matched) {
          if (op instanceof Adds) {
            _matched=true;
            EList<AddSource> _files = ((Adds) op).getFiles();
            for (final AddSource source : _files) {
              Source _fileName = source.getFileName();
              String _source = this.getSource(_fileName);
              set.add(_source);
            }
          }
        }
        if (!_matched) {
          if (op instanceof Removes) {
            _matched=true;
            EList<RemoveSource> _files = ((Removes) op).getFiles();
            for (final RemoveSource source : _files) {
              Source _fileName = source.getFileName();
              String _source = this.getSource(_fileName);
              set.add(_source);
            }
          }
        }
        if (!_matched) {
          if (op instanceof Modifies) {
            _matched=true;
            Source _fileName = ((Modifies) op).getFileName();
            String _source = this.getSource(_fileName);
            set.add(_source);
          }
        }
      }
      _xblockexpression = set;
    }
    return _xblockexpression;
  }
  
  private String getSource(final Source s) {
    String _switchResult = null;
    boolean _matched = false;
    if (!_matched) {
      if (s instanceof SimpleName) {
        _matched=true;
        _switchResult = ((SimpleName)s).getName();
      }
    }
    if (!_matched) {
      if (s instanceof CompleteName) {
        _matched=true;
        String _name = ((CompleteName)s).getName();
        String _plus = (_name + ".");
        String _ext = ((CompleteName) s).getExt();
        _switchResult = (_plus + _ext);
      }
    }
    return _switchResult;
  }
}
