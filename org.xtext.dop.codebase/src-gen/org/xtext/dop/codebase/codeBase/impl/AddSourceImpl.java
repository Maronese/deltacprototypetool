/**
 */
package org.xtext.dop.codebase.codeBase.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.xtext.dop.clang.clang.Model;
import org.xtext.dop.clang.clang.Source;

import org.xtext.dop.codebase.codeBase.AddSource;
import org.xtext.dop.codebase.codeBase.CodeBasePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Add Source</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.codebase.codeBase.impl.AddSourceImpl#getFileName <em>File Name</em>}</li>
 *   <li>{@link org.xtext.dop.codebase.codeBase.impl.AddSourceImpl#getCode <em>Code</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AddSourceImpl extends MinimalEObjectImpl.Container implements AddSource
{
  /**
   * The cached value of the '{@link #getFileName() <em>File Name</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFileName()
   * @generated
   * @ordered
   */
  protected Source fileName;

  /**
   * The cached value of the '{@link #getCode() <em>Code</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCode()
   * @generated
   * @ordered
   */
  protected Model code;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected AddSourceImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return CodeBasePackage.Literals.ADD_SOURCE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Source getFileName()
  {
    return fileName;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetFileName(Source newFileName, NotificationChain msgs)
  {
    Source oldFileName = fileName;
    fileName = newFileName;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CodeBasePackage.ADD_SOURCE__FILE_NAME, oldFileName, newFileName);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFileName(Source newFileName)
  {
    if (newFileName != fileName)
    {
      NotificationChain msgs = null;
      if (fileName != null)
        msgs = ((InternalEObject)fileName).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CodeBasePackage.ADD_SOURCE__FILE_NAME, null, msgs);
      if (newFileName != null)
        msgs = ((InternalEObject)newFileName).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CodeBasePackage.ADD_SOURCE__FILE_NAME, null, msgs);
      msgs = basicSetFileName(newFileName, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CodeBasePackage.ADD_SOURCE__FILE_NAME, newFileName, newFileName));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Model getCode()
  {
    return code;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetCode(Model newCode, NotificationChain msgs)
  {
    Model oldCode = code;
    code = newCode;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CodeBasePackage.ADD_SOURCE__CODE, oldCode, newCode);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCode(Model newCode)
  {
    if (newCode != code)
    {
      NotificationChain msgs = null;
      if (code != null)
        msgs = ((InternalEObject)code).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CodeBasePackage.ADD_SOURCE__CODE, null, msgs);
      if (newCode != null)
        msgs = ((InternalEObject)newCode).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CodeBasePackage.ADD_SOURCE__CODE, null, msgs);
      msgs = basicSetCode(newCode, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CodeBasePackage.ADD_SOURCE__CODE, newCode, newCode));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case CodeBasePackage.ADD_SOURCE__FILE_NAME:
        return basicSetFileName(null, msgs);
      case CodeBasePackage.ADD_SOURCE__CODE:
        return basicSetCode(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case CodeBasePackage.ADD_SOURCE__FILE_NAME:
        return getFileName();
      case CodeBasePackage.ADD_SOURCE__CODE:
        return getCode();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case CodeBasePackage.ADD_SOURCE__FILE_NAME:
        setFileName((Source)newValue);
        return;
      case CodeBasePackage.ADD_SOURCE__CODE:
        setCode((Model)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case CodeBasePackage.ADD_SOURCE__FILE_NAME:
        setFileName((Source)null);
        return;
      case CodeBasePackage.ADD_SOURCE__CODE:
        setCode((Model)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case CodeBasePackage.ADD_SOURCE__FILE_NAME:
        return fileName != null;
      case CodeBasePackage.ADD_SOURCE__CODE:
        return code != null;
    }
    return super.eIsSet(featureID);
  }

} //AddSourceImpl
