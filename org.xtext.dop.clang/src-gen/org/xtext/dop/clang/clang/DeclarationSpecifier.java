/**
 */
package org.xtext.dop.clang.clang;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Declaration Specifier</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getDeclarationSpecifier()
 * @model
 * @generated
 */
public interface DeclarationSpecifier extends EObject
{
} // DeclarationSpecifier
