package org.xtext.dop.productdeclaration.interpreter

import java.awt.BorderLayout
import java.awt.GridLayout
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import java.util.ArrayList
import java.util.List
import javax.swing.ButtonGroup
import javax.swing.JButton
import javax.swing.JFrame
import javax.swing.JLabel
import javax.swing.JPanel
import javax.swing.JRadioButton
import javax.swing.JTextArea
import org.xtext.dop.productdeclaration.productDeclaration.Product
import org.xtext.dop.rappresentation.Project

class Panel extends JFrame {
	
	val c = getContentPane
	JPanel productPanel
	JPanel featurePanel
	JPanel optionPanel
	JPanel radioPanel
	JPanel textPanel
	JLabel productTitle
	JLabel featureTitle
	JTextArea features
	ButtonGroup group
	ArrayList<JRadioButton> radioButtons
	JButton exitButton
	JButton generateButton
	
	List<Product> products
	Interpreter interpreter
	org.xtext.dop.codebase.generator.Main codebase
	
	new(String name, List<Product> list, Interpreter interpreter, org.xtext.dop.codebase.generator.Main codebase) {
		super(name);
		products = list
		this.interpreter = interpreter
		this.codebase = codebase
		setup(list);
	}
	
	def setup(List<Product> list) {
		
		group = new ButtonGroup
		productPanel = new JPanel
		featurePanel = new JPanel
		optionPanel = new JPanel
		radioPanel = new JPanel
		textPanel = new JPanel
		productTitle = new JLabel("       Products viable:")
		featureTitle = new JLabel("       Features selected:")
		features = new JTextArea()
		radioButtons = new ArrayList<JRadioButton>
		exitButton = new JButton("Close")
		generateButton = new JButton("Generate")
		
		for(Product prod: list) {
			//println(prod)
			val bt = new JRadioButton(prod.name)
			radioButtons.add(bt)
			group.add(bt)
			radioPanel.add(bt)
			
			bt.addActionListener(new SelectionListener(products, features))
			
		}
		
		productPanel.setLayout(new GridLayout(2, 1))
		
		productPanel.add(productTitle)
		productPanel.add(radioPanel)
		
		featurePanel.setLayout(new GridLayout(2, 1))
		
		features.setColumns(40);
        features.setLineWrap(true);
        features.setRows(5);
        features.setWrapStyleWord(true);
        features.setEditable(false);
        //jScrollPane1 = new JScrollPane(textArea);
		textPanel.add(features)
		
		featurePanel.add(featureTitle)
		featurePanel.add(textPanel)
		
		optionPanel.add(exitButton)
		exitButton.addActionListener(new ExitListener)
		optionPanel.add(generateButton)
		generateButton.addActionListener(new GenerateListener(radioButtons, products, interpreter, codebase))
		
		c.setLayout(new BorderLayout)
		
		c.add(productPanel, BorderLayout.NORTH)
		c.add(featurePanel, BorderLayout.CENTER)
		c.add(optionPanel, BorderLayout.SOUTH)
		
		setSize(500,250);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	/*def public static void main(String[] args) {
		var list = new ArrayList<String>
		list.addAll("alpha", "beta", "gamma", "delta", "eta", "beta")
		new Panel("test product line gui", list)
	}*/
	
	static class SelectionListener implements ActionListener {
		
		List<Product> product
		JTextArea text
		
		new(List<Product> product, JTextArea text) {
			this.product = product
			this.text = text
		}
		
		override actionPerformed(ActionEvent e) {
			val button = e.getSource as JRadioButton
			text.setText(product.findFirst[it.name==button.getText].list.map[it.elem.name].join(', '))
		}
		
	}
	
	static class ExitListener implements ActionListener {
		
		override actionPerformed(ActionEvent arg0) {
			System.exit(0)
		}
		
	}
	
	static class GenerateListener implements ActionListener {
		
		ArrayList<JRadioButton> radioButtons
		List<Product> product
		Interpreter interpreter
		org.xtext.dop.codebase.generator.Main codebase
		
		new(ArrayList<JRadioButton> radioButtons, List<Product> product, Interpreter interpreter, org.xtext.dop.codebase.generator.Main codebase) {
			this.radioButtons = radioButtons
			this.product = product
			this.interpreter = interpreter
			this.codebase = codebase
		}
		
		override actionPerformed(ActionEvent arg0) {
			//println(text.getText)
			val button = radioButtons.findFirst[it.isSelected]
			if(button==null) return;
			val obs = new GenerateListObservable(interpreter)
			obs.addObserver(codebase)//da modificare con observer correto
			obs.sendSelection(product.findFirst[it.name==button.getText])
			System.exit(0)
		}
		
	}
	
}