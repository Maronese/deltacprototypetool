/**
 */
package org.xtext.dop.clang.clang;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Base Type Imaginary</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getBaseTypeImaginary()
 * @model
 * @generated
 */
public interface BaseTypeImaginary extends BaseType
{
} // BaseTypeImaginary
