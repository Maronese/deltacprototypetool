/**
 */
package org.xtext.dop.clang.clang;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.xtext.dop.clang.clang.ClangPackage
 * @generated
 */
public interface ClangFactory extends EFactory
{
  /**
   * The singleton instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  ClangFactory eINSTANCE = org.xtext.dop.clang.clang.impl.ClangFactoryImpl.init();

  /**
   * Returns a new object of class '<em>Model</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Model</em>'.
   * @generated
   */
  Model createModel();

  /**
   * Returns a new object of class '<em>Standard Include</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Standard Include</em>'.
   * @generated
   */
  StandardInclude createStandardInclude();

  /**
   * Returns a new object of class '<em>File Include</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>File Include</em>'.
   * @generated
   */
  FileInclude createFileInclude();

  /**
   * Returns a new object of class '<em>Source</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Source</em>'.
   * @generated
   */
  Source createSource();

  /**
   * Returns a new object of class '<em>Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Declaration</em>'.
   * @generated
   */
  Declaration createDeclaration();

  /**
   * Returns a new object of class '<em>Type Alias Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Type Alias Declaration</em>'.
   * @generated
   */
  TypeAliasDeclaration createTypeAliasDeclaration();

  /**
   * Returns a new object of class '<em>Generic Specified Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Generic Specified Declaration</em>'.
   * @generated
   */
  GenericSpecifiedDeclaration createGenericSpecifiedDeclaration();

  /**
   * Returns a new object of class '<em>Generic Declaration Elements</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Generic Declaration Elements</em>'.
   * @generated
   */
  GenericDeclarationElements createGenericDeclarationElements();

  /**
   * Returns a new object of class '<em>Generic Declaration Element</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Generic Declaration Element</em>'.
   * @generated
   */
  GenericDeclarationElement createGenericDeclarationElement();

  /**
   * Returns a new object of class '<em>Generic Declaration Id</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Generic Declaration Id</em>'.
   * @generated
   */
  GenericDeclarationId createGenericDeclarationId();

  /**
   * Returns a new object of class '<em>Generic Declaration Inner Id</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Generic Declaration Inner Id</em>'.
   * @generated
   */
  GenericDeclarationInnerId createGenericDeclarationInnerId();

  /**
   * Returns a new object of class '<em>Generic Declaration Id Postfix</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Generic Declaration Id Postfix</em>'.
   * @generated
   */
  GenericDeclarationIdPostfix createGenericDeclarationIdPostfix();

  /**
   * Returns a new object of class '<em>Array Postfix</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Array Postfix</em>'.
   * @generated
   */
  ArrayPostfix createArrayPostfix();

  /**
   * Returns a new object of class '<em>Parameters Postfix</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Parameters Postfix</em>'.
   * @generated
   */
  ParametersPostfix createParametersPostfix();

  /**
   * Returns a new object of class '<em>Next Parameter</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Next Parameter</em>'.
   * @generated
   */
  NextParameter createNextParameter();

  /**
   * Returns a new object of class '<em>Parameter</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Parameter</em>'.
   * @generated
   */
  Parameter createParameter();

  /**
   * Returns a new object of class '<em>Generic Declaration Initializer</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Generic Declaration Initializer</em>'.
   * @generated
   */
  GenericDeclarationInitializer createGenericDeclarationInitializer();

  /**
   * Returns a new object of class '<em>Specified Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Specified Type</em>'.
   * @generated
   */
  SpecifiedType createSpecifiedType();

  /**
   * Returns a new object of class '<em>Static Assert Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Static Assert Declaration</em>'.
   * @generated
   */
  StaticAssertDeclaration createStaticAssertDeclaration();

  /**
   * Returns a new object of class '<em>Simple Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Simple Type</em>'.
   * @generated
   */
  SimpleType createSimpleType();

  /**
   * Returns a new object of class '<em>Base Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Base Type</em>'.
   * @generated
   */
  BaseType createBaseType();

  /**
   * Returns a new object of class '<em>Struct Or Union Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Struct Or Union Type</em>'.
   * @generated
   */
  StructOrUnionType createStructOrUnionType();

  /**
   * Returns a new object of class '<em>Enum Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Enum Type</em>'.
   * @generated
   */
  EnumType createEnumType();

  /**
   * Returns a new object of class '<em>Enumerator List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Enumerator List</em>'.
   * @generated
   */
  EnumeratorList createEnumeratorList();

  /**
   * Returns a new object of class '<em>Enumerator</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Enumerator</em>'.
   * @generated
   */
  Enumerator createEnumerator();

  /**
   * Returns a new object of class '<em>Enumeration Constant</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Enumeration Constant</em>'.
   * @generated
   */
  EnumerationConstant createEnumerationConstant();

  /**
   * Returns a new object of class '<em>Declaration Specifier</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Declaration Specifier</em>'.
   * @generated
   */
  DeclarationSpecifier createDeclarationSpecifier();

  /**
   * Returns a new object of class '<em>Storage Class Specifier</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Storage Class Specifier</em>'.
   * @generated
   */
  StorageClassSpecifier createStorageClassSpecifier();

  /**
   * Returns a new object of class '<em>Type Qualifier</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Type Qualifier</em>'.
   * @generated
   */
  TypeQualifier createTypeQualifier();

  /**
   * Returns a new object of class '<em>Function Specifier</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Function Specifier</em>'.
   * @generated
   */
  FunctionSpecifier createFunctionSpecifier();

  /**
   * Returns a new object of class '<em>Alignment Specifier</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Alignment Specifier</em>'.
   * @generated
   */
  AlignmentSpecifier createAlignmentSpecifier();

  /**
   * Returns a new object of class '<em>Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Expression</em>'.
   * @generated
   */
  Expression createExpression();

  /**
   * Returns a new object of class '<em>Assignment Expression Element</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Assignment Expression Element</em>'.
   * @generated
   */
  AssignmentExpressionElement createAssignmentExpressionElement();

  /**
   * Returns a new object of class '<em>String Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>String Expression</em>'.
   * @generated
   */
  StringExpression createStringExpression();

  /**
   * Returns a new object of class '<em>Generic Association</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Generic Association</em>'.
   * @generated
   */
  GenericAssociation createGenericAssociation();

  /**
   * Returns a new object of class '<em>Postfix Expression Postfix</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Postfix Expression Postfix</em>'.
   * @generated
   */
  PostfixExpressionPostfix createPostfixExpressionPostfix();

  /**
   * Returns a new object of class '<em>Next Asignement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Next Asignement</em>'.
   * @generated
   */
  NextAsignement createNextAsignement();

  /**
   * Returns a new object of class '<em>Compound Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Compound Statement</em>'.
   * @generated
   */
  CompoundStatement createCompoundStatement();

  /**
   * Returns a new object of class '<em>Block Item</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Block Item</em>'.
   * @generated
   */
  BlockItem createBlockItem();

  /**
   * Returns a new object of class '<em>Statements</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Statements</em>'.
   * @generated
   */
  Statements createStatements();

  /**
   * Returns a new object of class '<em>Labeled Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Labeled Statement</em>'.
   * @generated
   */
  LabeledStatement createLabeledStatement();

  /**
   * Returns a new object of class '<em>Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Statement</em>'.
   * @generated
   */
  Statement createStatement();

  /**
   * Returns a new object of class '<em>Expression Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Expression Statement</em>'.
   * @generated
   */
  ExpressionStatement createExpressionStatement();

  /**
   * Returns a new object of class '<em>Selection Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Selection Statement</em>'.
   * @generated
   */
  SelectionStatement createSelectionStatement();

  /**
   * Returns a new object of class '<em>Iteration Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Iteration Statement</em>'.
   * @generated
   */
  IterationStatement createIterationStatement();

  /**
   * Returns a new object of class '<em>Next Field</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Next Field</em>'.
   * @generated
   */
  NextField createNextField();

  /**
   * Returns a new object of class '<em>Jump Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Jump Statement</em>'.
   * @generated
   */
  JumpStatement createJumpStatement();

  /**
   * Returns a new object of class '<em>Complete Name</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Complete Name</em>'.
   * @generated
   */
  CompleteName createCompleteName();

  /**
   * Returns a new object of class '<em>Simple Name</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Simple Name</em>'.
   * @generated
   */
  SimpleName createSimpleName();

  /**
   * Returns a new object of class '<em>Generic Declaration0</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Generic Declaration0</em>'.
   * @generated
   */
  GenericDeclaration0 createGenericDeclaration0();

  /**
   * Returns a new object of class '<em>Generic Declaration1</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Generic Declaration1</em>'.
   * @generated
   */
  GenericDeclaration1 createGenericDeclaration1();

  /**
   * Returns a new object of class '<em>Generic Declaration2</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Generic Declaration2</em>'.
   * @generated
   */
  GenericDeclaration2 createGenericDeclaration2();

  /**
   * Returns a new object of class '<em>Global Name</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Global Name</em>'.
   * @generated
   */
  GlobalName createGlobalName();

  /**
   * Returns a new object of class '<em>Expression Initializer</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Expression Initializer</em>'.
   * @generated
   */
  ExpressionInitializer createExpressionInitializer();

  /**
   * Returns a new object of class '<em>Function Initializer</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Function Initializer</em>'.
   * @generated
   */
  FunctionInitializer createFunctionInitializer();

  /**
   * Returns a new object of class '<em>Atomic</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Atomic</em>'.
   * @generated
   */
  Atomic createAtomic();

  /**
   * Returns a new object of class '<em>Base Type Void</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Base Type Void</em>'.
   * @generated
   */
  BaseTypeVoid createBaseTypeVoid();

  /**
   * Returns a new object of class '<em>Custom</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Custom</em>'.
   * @generated
   */
  Custom createCustom();

  /**
   * Returns a new object of class '<em>Base Type Char</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Base Type Char</em>'.
   * @generated
   */
  BaseTypeChar createBaseTypeChar();

  /**
   * Returns a new object of class '<em>Base Type Short</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Base Type Short</em>'.
   * @generated
   */
  BaseTypeShort createBaseTypeShort();

  /**
   * Returns a new object of class '<em>Base Type Int</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Base Type Int</em>'.
   * @generated
   */
  BaseTypeInt createBaseTypeInt();

  /**
   * Returns a new object of class '<em>Base Type Long</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Base Type Long</em>'.
   * @generated
   */
  BaseTypeLong createBaseTypeLong();

  /**
   * Returns a new object of class '<em>Base Type Float</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Base Type Float</em>'.
   * @generated
   */
  BaseTypeFloat createBaseTypeFloat();

  /**
   * Returns a new object of class '<em>Base Type Double</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Base Type Double</em>'.
   * @generated
   */
  BaseTypeDouble createBaseTypeDouble();

  /**
   * Returns a new object of class '<em>Base Type Signed</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Base Type Signed</em>'.
   * @generated
   */
  BaseTypeSigned createBaseTypeSigned();

  /**
   * Returns a new object of class '<em>Base Type Unsigned</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Base Type Unsigned</em>'.
   * @generated
   */
  BaseTypeUnsigned createBaseTypeUnsigned();

  /**
   * Returns a new object of class '<em>Base Type Bool</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Base Type Bool</em>'.
   * @generated
   */
  BaseTypeBool createBaseTypeBool();

  /**
   * Returns a new object of class '<em>Base Type Complex</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Base Type Complex</em>'.
   * @generated
   */
  BaseTypeComplex createBaseTypeComplex();

  /**
   * Returns a new object of class '<em>Base Type Imaginary</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Base Type Imaginary</em>'.
   * @generated
   */
  BaseTypeImaginary createBaseTypeImaginary();

  /**
   * Returns a new object of class '<em>Storage Class Specifier Extern</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Storage Class Specifier Extern</em>'.
   * @generated
   */
  StorageClassSpecifierExtern createStorageClassSpecifierExtern();

  /**
   * Returns a new object of class '<em>Storage Class Specifier Static</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Storage Class Specifier Static</em>'.
   * @generated
   */
  StorageClassSpecifierStatic createStorageClassSpecifierStatic();

  /**
   * Returns a new object of class '<em>Storage Class Specifier Thread Local</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Storage Class Specifier Thread Local</em>'.
   * @generated
   */
  StorageClassSpecifierThreadLocal createStorageClassSpecifierThreadLocal();

  /**
   * Returns a new object of class '<em>Storage Class Specifier Auto</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Storage Class Specifier Auto</em>'.
   * @generated
   */
  StorageClassSpecifierAuto createStorageClassSpecifierAuto();

  /**
   * Returns a new object of class '<em>Storage Class Specifier Register</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Storage Class Specifier Register</em>'.
   * @generated
   */
  StorageClassSpecifierRegister createStorageClassSpecifierRegister();

  /**
   * Returns a new object of class '<em>Type Qualifier Const</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Type Qualifier Const</em>'.
   * @generated
   */
  TypeQualifierConst createTypeQualifierConst();

  /**
   * Returns a new object of class '<em>Type Qualifier Volatile</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Type Qualifier Volatile</em>'.
   * @generated
   */
  TypeQualifierVolatile createTypeQualifierVolatile();

  /**
   * Returns a new object of class '<em>Type Qualifier Atomic</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Type Qualifier Atomic</em>'.
   * @generated
   */
  TypeQualifierAtomic createTypeQualifierAtomic();

  /**
   * Returns a new object of class '<em>Function Specifier Inline</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Function Specifier Inline</em>'.
   * @generated
   */
  FunctionSpecifierInline createFunctionSpecifierInline();

  /**
   * Returns a new object of class '<em>Function Specifier No Return</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Function Specifier No Return</em>'.
   * @generated
   */
  FunctionSpecifierNoReturn createFunctionSpecifierNoReturn();

  /**
   * Returns a new object of class '<em>Assignment Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Assignment Expression</em>'.
   * @generated
   */
  AssignmentExpression createAssignmentExpression();

  /**
   * Returns a new object of class '<em>Conditional Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Conditional Expression</em>'.
   * @generated
   */
  ConditionalExpression createConditionalExpression();

  /**
   * Returns a new object of class '<em>Logical Or Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Logical Or Expression</em>'.
   * @generated
   */
  LogicalOrExpression createLogicalOrExpression();

  /**
   * Returns a new object of class '<em>Logical And Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Logical And Expression</em>'.
   * @generated
   */
  LogicalAndExpression createLogicalAndExpression();

  /**
   * Returns a new object of class '<em>Inclusive Or Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Inclusive Or Expression</em>'.
   * @generated
   */
  InclusiveOrExpression createInclusiveOrExpression();

  /**
   * Returns a new object of class '<em>Exclusive Or Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Exclusive Or Expression</em>'.
   * @generated
   */
  ExclusiveOrExpression createExclusiveOrExpression();

  /**
   * Returns a new object of class '<em>And Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>And Expression</em>'.
   * @generated
   */
  AndExpression createAndExpression();

  /**
   * Returns a new object of class '<em>Equality Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Equality Expression</em>'.
   * @generated
   */
  EqualityExpression createEqualityExpression();

  /**
   * Returns a new object of class '<em>Relational Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Relational Expression</em>'.
   * @generated
   */
  RelationalExpression createRelationalExpression();

  /**
   * Returns a new object of class '<em>Shift Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Shift Expression</em>'.
   * @generated
   */
  ShiftExpression createShiftExpression();

  /**
   * Returns a new object of class '<em>Additive Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Additive Expression</em>'.
   * @generated
   */
  AdditiveExpression createAdditiveExpression();

  /**
   * Returns a new object of class '<em>Multiplicative Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Multiplicative Expression</em>'.
   * @generated
   */
  MultiplicativeExpression createMultiplicativeExpression();

  /**
   * Returns a new object of class '<em>Expression Simple</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Expression Simple</em>'.
   * @generated
   */
  ExpressionSimple createExpressionSimple();

  /**
   * Returns a new object of class '<em>Expression Complex</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Expression Complex</em>'.
   * @generated
   */
  ExpressionComplex createExpressionComplex();

  /**
   * Returns a new object of class '<em>Expression Increment</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Expression Increment</em>'.
   * @generated
   */
  ExpressionIncrement createExpressionIncrement();

  /**
   * Returns a new object of class '<em>Expression Decrement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Expression Decrement</em>'.
   * @generated
   */
  ExpressionDecrement createExpressionDecrement();

  /**
   * Returns a new object of class '<em>Expression Sizeof</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Expression Sizeof</em>'.
   * @generated
   */
  ExpressionSizeof createExpressionSizeof();

  /**
   * Returns a new object of class '<em>Expression Unary Op</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Expression Unary Op</em>'.
   * @generated
   */
  ExpressionUnaryOp createExpressionUnaryOp();

  /**
   * Returns a new object of class '<em>Expression Align</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Expression Align</em>'.
   * @generated
   */
  ExpressionAlign createExpressionAlign();

  /**
   * Returns a new object of class '<em>Expression Cast</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Expression Cast</em>'.
   * @generated
   */
  ExpressionCast createExpressionCast();

  /**
   * Returns a new object of class '<em>Expression Identifier</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Expression Identifier</em>'.
   * @generated
   */
  ExpressionIdentifier createExpressionIdentifier();

  /**
   * Returns a new object of class '<em>Expression String</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Expression String</em>'.
   * @generated
   */
  ExpressionString createExpressionString();

  /**
   * Returns a new object of class '<em>Expression Integer</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Expression Integer</em>'.
   * @generated
   */
  ExpressionInteger createExpressionInteger();

  /**
   * Returns a new object of class '<em>Expression Float</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Expression Float</em>'.
   * @generated
   */
  ExpressionFloat createExpressionFloat();

  /**
   * Returns a new object of class '<em>Expression Sub Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Expression Sub Expression</em>'.
   * @generated
   */
  ExpressionSubExpression createExpressionSubExpression();

  /**
   * Returns a new object of class '<em>Expression Generic Selection</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Expression Generic Selection</em>'.
   * @generated
   */
  ExpressionGenericSelection createExpressionGenericSelection();

  /**
   * Returns a new object of class '<em>Generic Association Case</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Generic Association Case</em>'.
   * @generated
   */
  GenericAssociationCase createGenericAssociationCase();

  /**
   * Returns a new object of class '<em>Generic Association Default</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Generic Association Default</em>'.
   * @generated
   */
  GenericAssociationDefault createGenericAssociationDefault();

  /**
   * Returns a new object of class '<em>Postfix Expression Postfix Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Postfix Expression Postfix Expression</em>'.
   * @generated
   */
  PostfixExpressionPostfixExpression createPostfixExpressionPostfixExpression();

  /**
   * Returns a new object of class '<em>Postfix Expression Postfix Argument List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Postfix Expression Postfix Argument List</em>'.
   * @generated
   */
  PostfixExpressionPostfixArgumentList createPostfixExpressionPostfixArgumentList();

  /**
   * Returns a new object of class '<em>Postfix Expression Postfix Access</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Postfix Expression Postfix Access</em>'.
   * @generated
   */
  PostfixExpressionPostfixAccess createPostfixExpressionPostfixAccess();

  /**
   * Returns a new object of class '<em>Postfix Expression Postfix Access Ptr</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Postfix Expression Postfix Access Ptr</em>'.
   * @generated
   */
  PostfixExpressionPostfixAccessPtr createPostfixExpressionPostfixAccessPtr();

  /**
   * Returns a new object of class '<em>Postfix Expression Postfix Increment</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Postfix Expression Postfix Increment</em>'.
   * @generated
   */
  PostfixExpressionPostfixIncrement createPostfixExpressionPostfixIncrement();

  /**
   * Returns a new object of class '<em>Postfix Expression Postfix Decrement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Postfix Expression Postfix Decrement</em>'.
   * @generated
   */
  PostfixExpressionPostfixDecrement createPostfixExpressionPostfixDecrement();

  /**
   * Returns a new object of class '<em>Statement Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Statement Declaration</em>'.
   * @generated
   */
  StatementDeclaration createStatementDeclaration();

  /**
   * Returns a new object of class '<em>Statement Inner Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Statement Inner Statement</em>'.
   * @generated
   */
  StatementInnerStatement createStatementInnerStatement();

  /**
   * Returns a new object of class '<em>Labeled Statement Case</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Labeled Statement Case</em>'.
   * @generated
   */
  LabeledStatementCase createLabeledStatementCase();

  /**
   * Returns a new object of class '<em>Labeled Statement Default</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Labeled Statement Default</em>'.
   * @generated
   */
  LabeledStatementDefault createLabeledStatementDefault();

  /**
   * Returns a new object of class '<em>Go To Prefix Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Go To Prefix Statement</em>'.
   * @generated
   */
  GoToPrefixStatement createGoToPrefixStatement();

  /**
   * Returns a new object of class '<em>Selection Statement If</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Selection Statement If</em>'.
   * @generated
   */
  SelectionStatementIf createSelectionStatementIf();

  /**
   * Returns a new object of class '<em>Selection Statement Switch</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Selection Statement Switch</em>'.
   * @generated
   */
  SelectionStatementSwitch createSelectionStatementSwitch();

  /**
   * Returns a new object of class '<em>Iteration Statement While</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Iteration Statement While</em>'.
   * @generated
   */
  IterationStatementWhile createIterationStatementWhile();

  /**
   * Returns a new object of class '<em>Iteration Statement Do</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Iteration Statement Do</em>'.
   * @generated
   */
  IterationStatementDo createIterationStatementDo();

  /**
   * Returns a new object of class '<em>Iteration Statement For</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Iteration Statement For</em>'.
   * @generated
   */
  IterationStatementFor createIterationStatementFor();

  /**
   * Returns a new object of class '<em>Jump Statement Goto</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Jump Statement Goto</em>'.
   * @generated
   */
  JumpStatementGoto createJumpStatementGoto();

  /**
   * Returns a new object of class '<em>Jump Statement Continue</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Jump Statement Continue</em>'.
   * @generated
   */
  JumpStatementContinue createJumpStatementContinue();

  /**
   * Returns a new object of class '<em>Jump Statement Break</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Jump Statement Break</em>'.
   * @generated
   */
  JumpStatementBreak createJumpStatementBreak();

  /**
   * Returns a new object of class '<em>Jump Statement Return</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Jump Statement Return</em>'.
   * @generated
   */
  JumpStatementReturn createJumpStatementReturn();

  /**
   * Returns the package supported by this factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the package supported by this factory.
   * @generated
   */
  ClangPackage getClangPackage();

} //ClangFactory
