/**
 */
package org.xtext.dop.clang.clang;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Base Type Long</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getBaseTypeLong()
 * @model
 * @generated
 */
public interface BaseTypeLong extends BaseType
{
} // BaseTypeLong
