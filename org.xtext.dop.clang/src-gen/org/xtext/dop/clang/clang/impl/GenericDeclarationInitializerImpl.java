/**
 */
package org.xtext.dop.clang.clang.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.xtext.dop.clang.clang.ClangPackage;
import org.xtext.dop.clang.clang.GenericDeclarationInitializer;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Generic Declaration Initializer</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class GenericDeclarationInitializerImpl extends MinimalEObjectImpl.Container implements GenericDeclarationInitializer
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected GenericDeclarationInitializerImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ClangPackage.Literals.GENERIC_DECLARATION_INITIALIZER;
  }

} //GenericDeclarationInitializerImpl
