/**
 */
package org.xtext.dop.clang.clang;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Function Specifier</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.dop.clang.clang.ClangPackage#getFunctionSpecifier()
 * @model
 * @generated
 */
public interface FunctionSpecifier extends DeclarationSpecifier
{
} // FunctionSpecifier
