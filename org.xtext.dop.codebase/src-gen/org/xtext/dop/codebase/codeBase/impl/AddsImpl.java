/**
 */
package org.xtext.dop.codebase.codeBase.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.xtext.dop.codebase.codeBase.AddSource;
import org.xtext.dop.codebase.codeBase.Adds;
import org.xtext.dop.codebase.codeBase.CodeBasePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Adds</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.dop.codebase.codeBase.impl.AddsImpl#getFiles <em>Files</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AddsImpl extends FileOperationImpl implements Adds
{
  /**
   * The cached value of the '{@link #getFiles() <em>Files</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFiles()
   * @generated
   * @ordered
   */
  protected EList<AddSource> files;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected AddsImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return CodeBasePackage.Literals.ADDS;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<AddSource> getFiles()
  {
    if (files == null)
    {
      files = new EObjectContainmentEList<AddSource>(AddSource.class, this, CodeBasePackage.ADDS__FILES);
    }
    return files;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case CodeBasePackage.ADDS__FILES:
        return ((InternalEList<?>)getFiles()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case CodeBasePackage.ADDS__FILES:
        return getFiles();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case CodeBasePackage.ADDS__FILES:
        getFiles().clear();
        getFiles().addAll((Collection<? extends AddSource>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case CodeBasePackage.ADDS__FILES:
        getFiles().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case CodeBasePackage.ADDS__FILES:
        return files != null && !files.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //AddsImpl
